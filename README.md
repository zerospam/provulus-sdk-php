# Provulus SDK [![Coverage Status](https://coveralls.io/repos/bitbucket/zerospam/provulus-sdk-php/badge.svg?branch=master)](https://coveralls.io/bitbucket/zerospam/provulus-sdk-php?branch=master)

This README would normally document whatever steps are necessary to get your application up and running.

### What is this repository for?

This repository contains the code for a PHP library to interact with Provulus API.
By using this SDK you'll be able to directly interact with the API of Cumulus to manage your clients
and more.

### Features
#### Create Clients
You can create new client 
```php
<?php
        $conf = new ProvulusConfiguration(ProvulusEndpoint::LOCAL(), ProvulusApiVersion::V1());
        $conf->setApiKey('test');
        $client = new ProvulusClient($conf, new Client());

        $createRequest = new CreateClientRequest();
        $createRequest->setFirstName('Test')
                      ->setLastName('Test')
                      ->setDomain('testaclient.com')
                      ->setOrganizationName('Test a client dot com')
                      ->setEmail('test@testaclient.com')
                      ->setTransport('mailcatcher')
                      ->setMailboxes('10')
                      ->setTransportPort(25)
                      ->setPhone('5146244520')
                      ->setLanguage(LocaleEnum::EN_US())
                      ->withAdmin(true)
                      ->setTestEmailDomain('test@testaclient.com');
        try {
            $response = $client->processRequest($createRequest);
        } catch (ValidationException $e) {
            var_dump($e->getValidationData());
            throw $e;
        }


        var_dump($response);
```

### How do I get set up?
```
composer require zerospam/provulus-sdk-php
```