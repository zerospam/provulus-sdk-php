#!/usr/bin/env bash

docker run -it --rm --name composer -v "$PWD":/opt/project -w /opt/project zerospam/php-composer:5.6 composer "$@"