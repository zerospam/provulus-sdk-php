<?php
/**
 * Created by PhpStorm.
 * User: pbb
 * Date: 10/02/17
 * Time: 3:41 PM
 */

namespace ProvulusSDK\Utils\Date;

use Carbon\Carbon;

/**
 * Class Date
 *
 * @package ProvulusSDK\Utils
 */
final class DateUtil
{
    /**
     * DateUtil constructor.
     */
    private function __construct()
    {
    }

    /**
     * Gets Date Format used by Zerospam
     *
     * @return string
     */
    public static function getZerospamDateFormat()
    {
        return 'Y-m-d H:i:s.u P';
    }

    /**
     * Check if the date is null, else format it
     *
     * @param null|Carbon $date
     *
     * @return null|string
     */
    public static function nullOrFormatted($date)
    {
        if (is_null($date)) {
            return null;
        }

        if (!$date instanceof Carbon) {
            throw new \InvalidArgumentException('Need to be a carbon date.');
        }

        return $date->format(self::getZerospamDateFormat());
    }
}