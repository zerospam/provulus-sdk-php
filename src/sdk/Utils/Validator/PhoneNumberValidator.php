<?php
/**
 * Created by PhpStorm.
 * User: pbb
 * Date: 15/12/16
 * Time: 3:14 PM
 */

namespace ProvulusSDK\Utils\Validator;

use libphonenumber\NumberParseException;
use libphonenumber\PhoneNumberUtil;

/**
 * Class PhoneNumberValidator
 * Validates Phone numbers
 *
 * @package ProvulusSDK\Utils\Validators
 */
final class PhoneNumberValidator
{
    /**
     * Returns if Phone number is valid or not
     *
     * @param string $phoneNumber
     *
     * @return bool
     * @throws NumberParseException
     */
    public static function isValidPhoneNumber($phoneNumber)
    {
        if (!is_string($phoneNumber)) {
            throw new \InvalidArgumentException('Phone Number argument must be a string');
        }

        $phoneUtil = PhoneNumberUtil::getInstance();
        try {
            $phoneNumber = $phoneUtil->parse($phoneNumber, 'US');
        } catch (NumberParseException $e) {
            throw $e;
        }

        return $phoneUtil->isValidNumber($phoneNumber);
    }
}