<?php
/**
 * Created by PhpStorm.
 * User: pbb
 * Date: 23/12/16
 * Time: 3:21 PM
 */

namespace ProvulusSDK\Utils\Validator;


final class DomainValidator
{

    /**
     * DomainValidator constructor.
     */
    private function __construct() { }

    /**
     * Check if the domain syntax is correct.
     *
     * @param string $domainName
     *
     */
    public static function checkValidDomainSyntax($domainName)
    {
        if (!preg_match('/(?=^.{4,253}$)(^((?!-)[a-zA-Z0-9-]{1,63}(?<!-)\\.)+[a-zA-Z]{2,63}$)/i', $domainName)) {
            throw new \InvalidArgumentException('Invalid Domain Syntax');
        }
    }
}