<?php
/**
 * Created by PhpStorm.
 * User: aaflalo
 * Date: 08/12/16
 * Time: 11:51 AM
 */

namespace ProvulusSDK\Utils\Validator;


final class EmailValidator
{

    /**
     * EmailValidator constructor.
     */
    private function __construct() { }

    /**
     * Check if the email is valid
     *
     * @param $email
     *
     * @throws  \InvalidArgumentException if the email is not valid
     */
    public static function checkValidEmailConstraint($email)
    {
        if (!filter_var($email, FILTER_VALIDATE_EMAIL)) {
            throw new \InvalidArgumentException('Invalid Email');
        }
    }
}