<?php
/**
 * Created by PhpStorm.
 * User: aaflalo
 * Date: 01/03/17
 * Time: 10:06 AM
 */

namespace ProvulusSDK\Utils\Relation;


class RelationContainer
{

    /**
     * @var string
     */
    private $type, $id;

    /**
     * RelationContainer constructor.
     *
     * @param $type
     * @param $id
     */
    public function __construct($type, $id)
    {
        $this->type = $type;
        $this->id   = $id;
    }

    /**
     * Key for array
     *
     * @return string
     */
    public function key()
    {
        return $this->type . '-' . $this->id;
    }

    /**
     * Getter for type
     *
     * @return string
     */
    public function getType()
    {
        return $this->type;
    }

    /**
     * Getter for id
     *
     * @return string
     */
    public function getId()
    {
        return $this->id;
    }


}