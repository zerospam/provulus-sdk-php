<?php
/**
 * Created by PhpStorm.
 * User: ycoutu
 * Date: 19/09/18
 * Time: 10:41 AM
 */

namespace ProvulusSDK\Utils\Organization\Billing;

use Carbon\Carbon;
use ProvulusSDK\Client\Request\HasNullableFields;
use ProvulusSDK\Client\Request\Resource\Arrayable;
use ProvulusSDK\Client\Request\Resource\NullableFieldsTrait;
use ProvulusSDK\Utils\ReflectionUtil;

class Rebate implements Arrayable, HasNullableFields
{
    use NullableFieldsTrait;

    /** @var int */
    private $id;

    /** @var Carbon|null */
    private $expiry;

    /**
     * @param int $id
     * @return $this
     */
    public function setId($id)
    {
        $this->id = $id;
        return $this;
    }

    /**
     * @param Carbon|null $expiry
     * @return $this
     */
    public function setExpiry($expiry)
    {
        $this->nullableChanged();
        $this->expiry = $expiry;
        return $this;
    }

    /**
     * Transform the object into an array
     *
     * @return array
     */
    public function toArray()
    {
        return ReflectionUtil::objToSnakeArray($this, ['nullableChanged']);
    }
}
