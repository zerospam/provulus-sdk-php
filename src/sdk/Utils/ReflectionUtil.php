<?php
/**
 * Created by PhpStorm.
 * User: aaflalo
 * Date: 06/12/16
 * Time: 3:00 PM
 */

namespace ProvulusSDK\Utils;

use Carbon\Carbon;
use ProvulusSDK\Client\Request\HasNullableFields;
use ProvulusSDK\Client\Request\Resource\Arrayable;
use ProvulusSDK\Client\Request\Resource\PrimalValued;

final class ReflectionUtil
{
    private function __construct()
    {
    }

    /**
     * Take an object and return an array using all it's properties
     *
     * Don't set the null one
     *
     * @param       $object    *
     *
     * @param array $ignored array containing name of properties to not serialize
     *
     * @return array
     */
    public static function objToSnakeArray($object, $ignored = [])
    {
        if (!is_object($object)) {
            throw new \InvalidArgumentException('Not an object');
        }

        return array_reduce(self::getAllProperties($object, $ignored),
            function (
                array $result,
                \ReflectionProperty $property
            ) use (
                $object
            ) {

                $property->setAccessible(true);

                $value = $property->getValue($object);


                if ($value instanceof PrimalValued) {
                    $value = $value->toPrimitive();
                } elseif ($value instanceof Arrayable) {
                    $value = $value->toArray();
                    if (empty($value)) {
                        $value = null;
                    }
                } elseif (is_array($value)) {
                    $value = Arr::transformArray($value);
                } elseif ($value instanceof \DateTimeZone) {
                    $value = $value->getName();
                } elseif ($value instanceof Carbon) {
                    $value = $value->toRfc3339String();
                }

                $field = $property->getName();

                if ($object instanceof HasNullableFields) {
                    if (is_null($value) && !$object->isNullable($field)) {
                        return $result;
                    }

                    if ($object->isNullable($field) && !$object->isValueChanged($field)) {
                        return $result;
                    }
                } elseif (is_null($value)) {
                    return $result;
                }

                $result[Str::snake($field)] = $value;

                $property->setAccessible(false);

                return $result;
            }, []);
    }

    /**
     * Get all the properties not ignored
     *
     * @param       $class
     * @param array $ignored
     *
     * @return array|\ReflectionProperty[]
     */
    public static function getAllProperties($class, $ignored = [])
    {
        $classReflection = new \ReflectionClass($class);

        $properties = $classReflection->getProperties();

        if ($parentClass = $classReflection->getParentClass()) {
            $parentProps = self::getAllProperties($parentClass->getName(), $ignored);
            if (!empty($parentProps)) {
                $properties = array_merge($parentProps, $properties);
            }
        }
        if (empty($ignored)) {
            return $properties;
        }

        return array_filter($properties, function (\ReflectionProperty $property) use ($ignored) {
            return !in_array($property->name, $ignored);
        });
    }
}
