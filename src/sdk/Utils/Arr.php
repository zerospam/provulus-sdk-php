<?php
/**
 * Created by PhpStorm.
 * User: aaflalo
 * Date: 15/12/16
 * Time: 1:21 PM
 */

namespace ProvulusSDK\Utils;

use Carbon\Carbon;
use ProvulusSDK\Client\Request\Resource\Arrayable;
use ProvulusSDK\Client\Request\Resource\PrimalValued;

final class Arr
{
    /**
     * Arr constructor.
     */
    private function __construct()
    {
    }


    /**
     * Transform an array
     *
     * @param array $array
     *
     * @return array
     */
    public static function transformArray(array $array)
    {
        return array_map(function ($item) {
            if ($item instanceof PrimalValued) {
                return $item->toPrimitive();
            }
            if ($item instanceof Arrayable) {
                $item = $item->toArray();
            }
            if ($item instanceof \DateTimeZone) {
                return $item->getName();
            }
            if ($item instanceof Carbon) {
                return $item->toRfc3339String();
            }

            if (is_array($item)) {
                return Arr::transformArray($item);
            }

            return $item;
        }, $array);
    }
}
