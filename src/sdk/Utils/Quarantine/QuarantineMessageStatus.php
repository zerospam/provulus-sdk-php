<?php
/**
 * Created by PhpStorm.
 * User: pbb
 * Date: 11/04/17
 * Time: 1:58 PM
 */

namespace ProvulusSDK\Utils\Quarantine;

/**
 * Class QuarantineMessageStatus
 *
 * @package ProvulusSDK\Utils\Quarantine
 */
class QuarantineMessageStatus
{

    /**
     * @var string
     */
    private $type;

    /**
     * @var string|null
     */
    private $changed, $userName, $organizationName;

    /**
     * QuarantineMessageStatus constructor.
     *
     * @param $type
     * @param $changed
     * @param $userName
     * @param $organizationName
     */
    public function __construct($type, $changed, $userName, $organizationName)
    {
        $this->type             = $type;
        $this->changed          = $changed;
        $this->userName         = $userName;
        $this->organizationName = $organizationName;
    }

    /**
     * @param $array
     *
     * @return QuarantineMessageStatus
     */
    public static function fromArray($array)
    {
        return new self(
            $array['type'],
            $array['changed'],
            $array['user']['name'],
            $array['user']['organization']
        );
    }

    /**
     * Getter for type
     *
     * @return string
     */
    public function getType()
    {
        return $this->type;
    }

    /**
     * Getter for changed
     *
     * @return null|string
     */
    public function getChanged()
    {
        return $this->changed;
    }

    /**
     * Getter for userName
     *
     * @return null|string
     */
    public function getUserName()
    {
        return $this->userName;
    }

    /**
     * Getter for organizationName
     *
     * @return null|string
     */
    public function getOrganizationName()
    {
        return $this->organizationName;
    }

    /**
     * Can this message be released
     *
     * @return bool
     */
    public function IsReleasable()
    {
        return $this->type != 'deleted' && $this->type != 'released' && $this->type != 'declared_as_spam';
    }

    /**
     * Can this message be restored
     *
     * @return bool
     */
    public function IsRestorable()
    {
        return $this->type == 'deleted';
    }

    /**
     * Can this message be declared as spam
     *
     * @return bool
     */
    public function IsDeclarableAsSpam()
    {
        return $this->type != 'declared_as_spam';
    }

}