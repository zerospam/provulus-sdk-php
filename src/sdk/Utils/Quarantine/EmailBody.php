<?php
/**
 * Created by PhpStorm.
 * User: pbb
 * Date: 21/04/17
 * Time: 11:27 AM
 */

namespace ProvulusSDK\Utils\Quarantine;

/**
 * Class EmailBody
 *
 * Body of an Email (QuarantineMessage)
 *
 * @package ProvulusSDK\Utils\Quarantine
 */
class EmailBody
{
    /**
     * @var string
     */
    private $html, $text;

    /**
     * EmailBody constructor.
     *
     * @param $text
     * @param $html
     */
    public function __construct($text, $html)
    {
        $this->text = is_null($text) ? null : base64_decode($text);
        $this->html = is_null($html) ? null : base64_decode($html);
    }

    /**
     * Create EmailBody from array
     *
     * @param $array
     *
     * @return static
     */
    public static function fromArray($array)
    {
        return new static(
            $array['plainText'],
            $array['html']
        );
    }

    /**
     * Getter for html
     *
     * @return string
     */
    public function getHtml()
    {
        return $this->html;
    }

    /**
     * Getter for text
     *
     * @return string
     */
    public function getText()
    {
        return $this->text;
    }
}
