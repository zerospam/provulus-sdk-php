<?php
/**
 * Created by PhpStorm.
 * User: pbb
 * Date: 21/04/17
 * Time: 11:34 AM
 */

namespace ProvulusSDK\Utils\Quarantine;

/**
 * Class RelatedEmail
 *
 * When mail is sent to multiple recipients,
 * RelatedEmail represents another MessageRecipient that the email was sent to
 *
 * @package ProvulusSDK\Utils\Quarantine
 */
class RelatedEmail
{

    /**
     * @var int
     */
    private $msgId;

    /**
     * @var EmailAddress
     */
    private $recipient;

    /**
     * @var QuarantineMessageStatus
     */
    private $status;

    /**
     * RelatedEmail constructor.
     *
     * @param $msgId
     * @param $recipient
     * @param $status
     */
    function __construct($msgId, EmailAddress $recipient, QuarantineMessageStatus $status)
    {
        $this->msgId     = $msgId;
        $this->recipient = $recipient;
        $this->status    = $status;
    }

    /**
     * @param $array
     *
     * @return static
     */
    public static function fromArray($array)
    {
        return new static(
            $array['msg_id'],
            EmailAddress::fromArray($array['recipient']),
            QuarantineMessageStatus::fromArray($array['status'])
        );
    }

    /**
     * Getter for msgId
     *
     * @return int
     */
    public function getMsgId()
    {
        return $this->msgId;
    }

    /**
     * Getter for recipient
     *
     * @return EmailAddress
     */
    public function getRecipient()
    {
        return $this->recipient;
    }

    /**
     * Getter for status
     *
     * @return QuarantineMessageStatus
     */
    public function getStatus()
    {
        return $this->status;
    }
}