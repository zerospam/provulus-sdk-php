<?php
/**
 * Created by PhpStorm.
 * User: pbb
 * Date: 15/02/17
 * Time: 4:40 PM
 */

namespace ProvulusSDK\Utils\Quarantine;

/**
 * Class AddressAndName
 *
 * Representation of an email address parts with the associated name
 *
 * @package ProvulusSDK\Utils
 */
class EmailAddress
{

    /**
     * @var string
     */
    private $localPart, $domainPart;

    /**
     * @var string|null
     */
    private $name;

    /**
     * AddressAndName constructor.
     *
     * @param string      $localPart
     * @param string      $domainPart
     * @param null|string $name
     */
    public function __construct($localPart, $domainPart, $name)
    {
        $this->localPart  = $localPart;
        $this->domainPart = $domainPart;
        $this->name       = $name;
    }


    /**
     * Create the container from an array
     *
     * @param string[] $attributes
     *
     * @return EmailAddress
     */
    public static function fromArray(array $attributes)
    {
        return new EmailAddress(
            $attributes['local_part'],
            $attributes['domain_part'],
            $attributes['name']
        );
    }

    /**
     * Getter for localPart
     *
     * @return string
     */
    public function getLocalPart()
    {
        return $this->localPart;
    }

    /**
     * Getter for domainPart
     *
     * @return string
     */
    public function getDomainPart()
    {
        return $this->domainPart;
    }

    /**
     * Getter for name
     *
     * @return string|null
     */
    public function getName()
    {
        return $this->name;
    }

    /**
     * Full email
     *
     * @return string|null
     */
    public function getEmail()
    {
        if (!$this->localPart && !$this->domainPart) {
            return null;
        }

        return $this->localPart . '@' . $this->domainPart;
    }


    /**
     * Get the instance as an array.
     *
     * @return array
     */
    public function toArray()
    {
        return [
            'local_part'  => $this->localPart,
            'domain_part' => $this->domainPart,
            'name'        => $this->name,
        ];
    }
}