<?php
/**
 * Created by PhpStorm.
 * User: pbb
 * Date: 21/04/17
 * Time: 11:14 AM
 */

namespace ProvulusSDK\Utils\Quarantine;

use Cake\Collection\Collection;

/**
 * Class EmailActor
 *
 * Represents an actor of the email transaction (either sender or recipient)
 *
 * @package ProvulusSDK\Utils\Quarantine
 */
class EmailActor
{

    /**
     * @var EmailAddress
     */
    private $envelope;
    /**
     * @var EmailAddress[]|Collection
     */
    private $header;

    public function __construct(EmailAddress $envelope, Collection $header)
    {
        $this->envelope = $envelope;
        $this->header   = $header;
    }

    /**
     * @param $array
     *
     * @return static
     */
    public static function fromArray($array)
    {
        $address = [];
        foreach ($array['header'] as $header) {
            $address[] = EmailAddress::fromArray($header);
        }

        return new static(
            EmailAddress::fromArray($array['envelope']),
            new Collection($address)
        );
    }

    /**
     * Getter for envelope
     *
     * @return EmailAddress
     */
    public function getEnvelope()
    {
        return $this->envelope;
    }

    /**
     * Getter for header
     *
     * @return EmailAddress[]|Collection
     */
    public function getHeader()
    {
        return $this->header;
    }
}