<?php
/**
 * Created by PhpStorm.
 * User: pbb
 * Date: 24/04/17
 * Time: 9:40 AM
 */

namespace ProvulusSDK\Utils\Quarantine;

/**
 * Class EmailAttachment
 *
 * Represents an attachment for a QuarantineMessage email
 *
 * @package ProvulusSDK\Utils\Quarantine
 */
class EmailAttachment
{
    /**
     * @var int
     */
    private $id;
    /**
     * @var int
     */
    private $msgId;
    /**
     * @var string
     */
    private $name;
    private $type;
    /**
     * @var int
     */
    private $size;
    /**
     * @var bool
     */
    private $isBanned;

    /**
     * EmailAttachment constructor.
     *
     * @param int    $id
     * @param int    $msgId
     * @param string $name
     * @param string $type
     * @param int    $size
     * @param bool   $isBanned
     */
    public function __construct($id, $msgId, $name, $type, $size, $isBanned)
    {

        $this->id       = $id;
        $this->msgId    = $msgId;
        $this->name     = $name;
        $this->type     = $type;
        $this->size     = $size;
        $this->isBanned = $isBanned;
    }


    /**
     * @param array $array
     *
     * @return static
     */
    public static function fromArray($array)
    {
        return new static (
            $array['id'],
            $array['msg_id'],
            $array['name'],
            $array['type'],
            $array['size'],
            $array['is_banned']
        );
    }

    /**
     * Getter for id
     *
     * @return int
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Getter for msgId
     *
     * @return int
     */
    public function getMsgId()
    {
        return $this->msgId;
    }

    /**
     * Getter for name
     *
     * @return string
     */
    public function getName()
    {
        return $this->name;
    }

    /**
     * Getter for type
     *
     * @return string
     */
    public function getType()
    {
        return $this->type;
    }

    /**
     * Getter for size
     *
     * @return int
     */
    public function getSize()
    {
        return $this->size;
    }

    /**
     * Getter for isBanned
     *
     * @return bool
     */
    public function isBanned()
    {
        return $this->isBanned;
    }


}