<?php
/**
 * Created by PhpStorm.
 * User: ycoutu
 * Date: 30/08/17
 * Time: 10:05 AM
 */

namespace ProvulusSDK\Utils\FilteringList;

use Carbon\Carbon;
use ProvulusSDK\Client\Request\Resource\Arrayable;
use ProvulusSDK\Client\Request\Resource\Domain\Filtering\FilteringList\FilteringListSetTrait;

class FilteringListObject implements Arrayable
{
    use FilteringListSetTrait;

    public function __construct()
    {
        $this->expiresAt = null;
        $this->description = -1;
    }

    /**
     * @param null|Carbon $expiresAt
     *
     * @return WhitelistSetTrait
     */
    public function setExpiresAt($expiresAt)
    {
        // Overwritten because no need for nullableChanged
        $this->expiresAt = $expiresAt;

        return $this;
    }

    /**
     * @param null|string $description
     *
     * @return FilteringListSetTrait
     */
    public function setDescription($description)
    {
        // Overwritten because no need for nullableChanged
        $this->description = $description;

        return $this;
    }

    /**
     * Transform the object into an array
     *
     * @return array
     */
    public function toArray()
    {
        $array = [
            'value' => $this->value
        ];
        if (isset($this->action)) {
            $array['action'] = $this->action->getValue();
        }
        if (isset($this->isEnabled)) {
            $array['is_enabled'] = $this->isEnabled;
        }
        $array['expires_at'] = $this->expiresAt; // ALWAYS SET EVEN IF NULL
        if ($this->description !== -1) {
            $array['description'] = $this->description;
        }

        return $array;
    }
}
