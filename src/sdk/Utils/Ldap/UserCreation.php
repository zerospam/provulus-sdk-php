<?php
/**
 * Created by PhpStorm.
 * User: pbb
 * Date: 05/06/17
 * Time: 3:05 PM
 */

namespace ProvulusSDK\Utils\Ldap;

use Carbon\Carbon;
use ProvulusSDK\Client\Request\HasNullableFields;
use ProvulusSDK\Client\Request\Resource\Arrayable;
use ProvulusSDK\Client\Request\Resource\ArrayableTrait;
use ProvulusSDK\Client\Request\Resource\NullableFieldsTrait;
use ProvulusSDK\Client\Request\Resource\Value\GetValueTrait;
use ProvulusSDK\Client\Request\Resource\Value\HasValue;

/**
 * Class UserCreation
 *
 * User creation wrapper object
 *
 * @package ProvulusSDK\Utils\LDAP
 */
class UserCreation implements Arrayable, HasNullableFields, HasValue
{
    use NullableFieldsTrait, ArrayableTrait, GetValueTrait;

    /** @var  bool */
    private $isCreateUsers;

    /** @var  Carbon|null */
    private $createdAt;

    /** @var  bool */
    private $hasDigest;

    /** @var  bool */
    private $sendEmailOnCreation;

    /** @var bool */
    private $isLdapAuthEnabled;

    /**
     * UserCreation constructor.
     *
     * @param bool|null   $isCreateUsers
     * @param bool|null   $hasDigest
     * @param bool|null   $isSendEmailOnCreation
     * @param bool|null   $isLdapAuthEnabled
     * @param Carbon|null $createdAt
     */
    public function __construct(
        $isCreateUsers = null,
        $hasDigest = null,
        $isSendEmailOnCreation = null,
        $isLdapAuthEnabled = null,
        $createdAt = null
    ) {
        $this->isCreateUsers       = $isCreateUsers;
        $this->createdAt           = $createdAt;
        $this->hasDigest           = $hasDigest;
        $this->sendEmailOnCreation = $isSendEmailOnCreation;
        $this->isLdapAuthEnabled   = $isLdapAuthEnabled;
    }

    /**
     * @return bool
     */
    public function isCreateUsers()
    {
        return $this->isCreateUsers;
    }

    /**
     * @return Carbon|null
     */
    public function getCreatedAt()
    {
        return $this->createdAt;
    }

    /**
     * @return bool
     */
    public function hasDigest()
    {
        return $this->hasDigest;
    }

    /**
     * @return bool
     */
    public function sendEmailOnCreation()
    {
        return $this->sendEmailOnCreation;
    }

    /**
     * @param bool $isCreateUsers
     *
     * @return UserCreation
     */
    public function setIsCreateUsers($isCreateUsers)
    {
        $this->isCreateUsers = $isCreateUsers;

        return $this;
    }

    /**
     * @param bool $hasDigest
     *
     * @return UserCreation
     */
    public function setHasDigest($hasDigest)
    {
        $this->hasDigest = $hasDigest;

        return $this;
    }

    /**
     * @param bool $sendEmailOnCreation
     *
     * @return UserCreation
     */
    public function setSendEmailOnCreation($sendEmailOnCreation)
    {
        $this->sendEmailOnCreation = $sendEmailOnCreation;

        return $this;
    }

    /**
     * @return bool
     */
    public function isLdapAuthEnabled()
    {
        return $this->isLdapAuthEnabled;
    }

    /**
     * @param bool $isLdapAuthEnabled
     *
     * @return UserCreation
     */
    public function setIsLdapAuthEnabled($isLdapAuthEnabled)
    {
        $this->isLdapAuthEnabled = $isLdapAuthEnabled;

        return $this;
    }

    /**
     * @param $array
     *
     * @return UserCreation
     */
    public static function fromArray($array)
    {
        if ($created_at = $array['created_at']) {
            $created_at = Carbon::parse($created_at);
        }

        return new UserCreation(
            $array['is_create_users'],
            $array['has_digest'],
            $array['send_email_on_creation'],
            $array['is_ldap_auth_enabled'],
            $created_at
        );
    }

    /**
     * Get default instance of UserCreation
     *
     * @return UserCreation
     */
    public static function defaultInstance()
    {
        return new UserCreation();
    }
}
