<?php
/**
 * Created by PhpStorm.
 * User: pbb
 * Date: 05/06/17
 * Time: 3:04 PM
 */

namespace ProvulusSDK\Utils\Ldap;

use function array_key_exists;
use ProvulusSDK\Client\Request\HasNullableFields;
use ProvulusSDK\Client\Request\Resource\Arrayable;
use ProvulusSDK\Client\Request\Resource\ArrayableTrait;
use ProvulusSDK\Client\Request\Resource\NullableFieldsTrait;
use ProvulusSDK\Client\Request\Resource\Value\GetValueTrait;
use ProvulusSDK\Client\Request\Resource\Value\HasValue;
use ProvulusSDK\Enum\Ldap\LdapConfigSyncFrequencyEnum;
use ProvulusSDK\Utils\Str;

/**
 * Class Synchronization
 *
 * Ldap Synchronization wrapper object
 *
 * @package ProvulusSDK\Utils\LDAP
 */
class Synchronization implements Arrayable, HasNullableFields, HasValue
{
    use NullableFieldsTrait, ArrayableTrait, GetValueTrait;

    /** @var boolean */
    private $isAutomatic;

    /** @var  LdapConfigSyncFrequencyEnum|null */
    private $frequency;

    /**
     * Synchronization constructor.
     *
     * @param bool|null                        $isAutomatic
     * @param LdapConfigSyncFrequencyEnum|null $frequency
     */
    public function __construct(
        $isAutomatic = null,
        $frequency = null
    ) {
        $this->isAutomatic                 = $isAutomatic;
        $this->frequency                   = $frequency;
    }

    /**
     * @return bool
     */
    public function isAutomatic()
    {
        return $this->isAutomatic;
    }

    /**
     * @return LdapConfigSyncFrequencyEnum|null
     */
    public function getFrequency()
    {
        return $this->frequency;
    }

    /**
     * @param bool $isAutomatic
     *
     * @return Synchronization
     */
    public function setIsAutomatic($isAutomatic)
    {
        $this->isAutomatic = $isAutomatic;

        return $this;
    }

    /**
     * @param null|LdapConfigSyncFrequencyEnum $frequency
     *
     * @return Synchronization
     */
    public function setFrequency($frequency)
    {
        $this->frequency = $frequency;

        return $this;
    }

    /**
     * @param $array
     *
     * @return Synchronization
     */
    public static function fromArray($array)
    {
        if ($frequency = $array['frequency']) {
            $frequency = LdapConfigSyncFrequencyEnum::byName(Str::upper($frequency));
        }

        return new Synchronization(
            $array['is_automatic'],
            $frequency
        );
    }

    /**
     * Default instance of Synchronization
     *
     * @return Synchronization
     */
    public static function defaultInstance()
    {
        return new Synchronization();
    }
}
