<?php
/**
 * Created by PhpStorm.
 * User: aaflalo
 * Date: 18-01-15
 * Time: 14:21
 */

namespace ProvulusSDK\Utils\Maintenance;

use Carbon\Carbon;
use Guzzle\Common\Collection;
use Psr\Http\Message\UriInterface;
use function GuzzleHttp\Psr7\uri_for;

class MaintenanceInformation
{

    /**  @var string */
    private $name;

    /** @var  Carbon|null */
    private $scheduledFor;

    /** @var Carbon|null */
    private $scheduledUntil;

    /** @var UriInterface */
    private $link;

    /** @var Carbon */
    private $createdAt;

    /** @var string|null */
    private $impact;

    /**
     * ScheduledIncident constructor.
     *
     * @param string       $name
     * @param Carbon       $createdAt
     * @param UriInterface $link
     * @param string       $impact
     * @param Carbon       $scheduledFor
     * @param Carbon       $scheduledUntil
     */
    private function __construct(
        $name,
        Carbon $createdAt,
        UriInterface $link,
        $impact,
        Carbon $scheduledFor = null,
        Carbon $scheduledUntil = null
    ) {
        $this->name = $name;
        $this->scheduledFor = $scheduledFor;
        $this->scheduledUntil = $scheduledUntil;
        $this->link = $link;
        $this->createdAt = $createdAt;
        $this->impact = $impact;
    }

    public static function fromArray(array $data)
    {
        return new static(
            $data['name'],
            self::parseDate($data['created_at']),
            uri_for($data['link']),
            $data['impact'],
            self::parseDate($data['scheduled_for']),
            self::parseDate($data['scheduled_until'])
        );
    }

    /**
     * Parse the date
     * @param string $data
     *
     * @return Carbon|null
     */
    private static function parseDate($data)
    {
        if (is_null($data) || empty($data)) {
            return null;
        }

        return Carbon::parse($data);
    }

    /**
     * @return string
     */
    public function name()
    {
        return $this->name;
    }

    /**
     * @return Carbon
     */
    public function scheduledFor()
    {
        return $this->scheduledFor;
    }

    /**
     * @return Carbon
     */
    public function scheduledUntil()
    {
        return $this->scheduledUntil;
    }

    /**
     * @return string
     */
    public function impact()
    {
        return $this->impact;
    }

    /**
     * @return UriInterface
     */
    public function link()
    {
        return $this->link;
    }

    /**
     * @return Carbon
     */
    public function createdAt()
    {
        return $this->createdAt;
    }
}
