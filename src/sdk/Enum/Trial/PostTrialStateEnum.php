<?php
/**
 * Created by PhpStorm.
 * User: pbb
 * Date: 26/01/18
 * Time: 3:04 PM
 */

namespace ProvulusSDK\Enum\Trial;


use MabeEnum\Enum;
use ProvulusSDK\Client\Request\Resource\PrimalValued;
use ProvulusSDK\Client\Request\Resource\PrimalValuedEnumTrait;

/**
 * Class PostTrialActionEnum
 *
 * @method static PostTrialStateEnum CONFIRM()
 * @method static PostTrialStateEnum DECLINE()
 *
 * @package ProvulusSDK\Enum\Trial
 */
class PostTrialStateEnum extends Enum implements PrimalValued
{
    use PrimalValuedEnumTrait;

    const DECLINE = 'decline';
    const CONFIRM = 'confirm';
}