<?php
/**
 * Created by PhpStorm.
 * User: pbb
 * Date: 05/07/17
 * Time: 11:13 AM
 */

namespace ProvulusSDK\Enum\Audit;


use MabeEnum\Enum;
use ProvulusSDK\Client\Request\Resource\PrimalValued;
use ProvulusSDK\Client\Request\Resource\PrimalValuedEnumTrait;

/**
 * Class AuditedModelsEnum
 *
 * Enumeration of the audited models (by Laravel audit logs)
 *
 * @method static AuditedModelsEnum API_KEY()
 * @method static AuditedModelsEnum CONTACT_PERSON()
 * @method static AuditedModelsEnum DOMAIN()
 * @method static AuditedModelsEnum FILTERING_POLICY()
 * @method static AuditedModelsEnum LDAP_CONFIG()
 * @method static AuditedModelsEnum ORGANIZATION()
 * @method static AuditedModelsEnum ORGANIZATION_MX()
 * @method static AuditedModelsEnum OUT_IDENTITY()
 * @method static AuditedModelsEnum USER()
 * @method static AuditedModelsEnum USER_EMAIL_ADDRESS()
 * @method static AuditedModelsEnum PAYLOAD_AUDITABLE()
 * @method static AuditedModelsEnum POST_TRIAL_ACTION()
 * @method static AuditedModelsEnum CANCELLATION_INFORMATION()
 *
 * @package ProvulusSDK\Enum\Audit
 */
class AuditedModelsEnum extends Enum implements PrimalValued
{
    use PrimalValuedEnumTrait;

    const API_KEY = 'api_key';
    const CONTACT_PERSON = 'contact_person';
    const DOMAIN = 'domain';
    const FILTERING_POLICY = 'filtering_policy';
    const FILTERING_POLICY_ITEM = 'filtering_policy_item';
    const LDAP_CONFIG = 'ldap_config';
    const ORGANIZATION = 'organization';
    const ORGANIZATION_MX = 'organization_mx';
    const OUT_IDENTITY = 'out_identity';
    const USER = 'user';
    const USER_EMAIL_ADDRESS = 'user_email_address';
    const PAYLOAD_AUDITABLE = 'payload_auditable';
    const DISTRIBUTOR_ITEM = 'distributor_item';
    const FILTERING_LIST_AUTHORIZED_ADDRESS = 'filtering_list_authorized_address';
    const FILTERING_LIST_FILE_EXTENSION = 'filtering_list_file_extension';
    const FILTERING_LIST_INBOUND_RECIPIENT = 'filtering_list_inbound_recipient';
    const FILTERING_LIST_INBOUND_SENDER = 'filtering_list_inbound_sender';
    const FILTERING_LIST_IP = 'filtering_list_ip';
    const FILTERING_LIST_MIME = 'filtering_list_mime';
    const FILTERING_LIST_OUTBOUND_RECIPIENT = 'filtering_list_outbound_recipient';
    const FILTERING_LIST_OUTBOUND_SENDER = 'filtering_list_outbound_sender';
    const FILTERING_LIST_SPF = 'filtering_list_spf';
    const PERSON_OF_INTEREST = 'person_of_interest';
    const TWO_FACTOR_AUTH = 'two_factor_auth';
    const POST_TRIAL_ACTION = 'post_trial_action';
    const CANCELLATION_INFORMATION = 'cancellation_information';
    const FILTERING_LIST_BANNED_FILE_SENDER = 'filtering_list_banned_file_sender';
    const LDAP_CONNECTION = 'ldap_connection';
    const REBATE = 'rebate';
    const PRICE_LIST = 'price_list';
    const TAX = 'tax';
    const BILLING = 'billing';
    const CONTENT_CATEGORY = 'content_category';
    const OUTBOUND = 'outbound';
    const OUTBOUND_CONFIGURATION = 'outbound_configuration';
    const PASSWORD_CONFIGURATION = 'password_configuration';
    const LOGIN_CIDR             = 'login_cidr';
}
