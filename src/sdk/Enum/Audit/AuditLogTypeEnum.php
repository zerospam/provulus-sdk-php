<?php
/**
 * Created by PhpStorm.
 * User: pbb
 * Date: 05/07/17
 * Time: 11:15 AM
 */

namespace ProvulusSDK\Enum\Audit;

use MabeEnum\Enum;
use ProvulusSDK\Client\Request\Resource\PrimalValued;
use ProvulusSDK\Client\Request\Resource\PrimalValuedEnumTrait;

/**
 * Class AuditLogTypeEnum
 *
 * Type of action taken on Model when audited
 *
 * @method static AuditLogTypeEnum CREATED()
 * @method static AuditLogTypeEnum UPDATED()
 * @method static AuditLogTypeEnum DELETED()
 * @method static AuditLogTypeEnum RESTORED()
 *
 * @package ProvulusSDK\Enum\Audit
 */
class AuditLogTypeEnum extends Enum implements PrimalValued
{
    use PrimalValuedEnumTrait;

    const CREATED  = 'created';
    const UPDATED  = 'updated';
    const DELETED  = 'deleted';
    const RESTORED = 'restored';
}