<?php
/**
 * Created by PhpStorm.
 * User: aaflalo
 * Date: 07/12/16
 * Time: 8:53 AM
 */

namespace ProvulusSDK\Enum\Locale;


use MabeEnum\Enum;
use ProvulusSDK\Client\Request\Resource\PrimalValued;
use ProvulusSDK\Client\Request\Resource\PrimalValuedEnumTrait;
use ProvulusSDK\Utils\Str;

/**
 * Class LocaleEnum
 * @method static LocaleEnum FR_CA()
 * @method static LocaleEnum EN_US()
 * @method static LocaleEnum ES_ES()
 * @package ProvulusSDK\Enum\Locale
 */
class LocaleEnum extends Enum implements PrimalValued
{
    use PrimalValuedEnumTrait;

    const FR_CA = 'fr_CA';
    const EN_US = 'en_US';
    const ES_ES = 'es_ES';

    /**
     * Return the language of the Locale
     *
     * @return string
     */
    public function getLanguage()
    {
        $value = explode('_', $this->getValue());

        return Str::lower($value[0]);
    }

    /**
     * Return the language of the Locale
     *
     * @return string
     */
    public function getCountry()
    {
        $value = explode('_', $this->getValue());

        return Str::lower($value[1]);
    }
}