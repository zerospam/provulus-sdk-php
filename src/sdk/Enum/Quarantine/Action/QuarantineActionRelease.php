<?php
/**
 * Created by PhpStorm.
 * User: ycoutu
 * Date: 10/11/17
 * Time: 11:00 AM
 */

namespace ProvulusSDK\Enum\Quarantine\Action;

class QuarantineActionRelease implements IQuarantineAction
{

    /**
     * Get the route suffix related to this action
     *
     * @return string
     */
    public function getRoute()
    {
        return 'release';
    }
}
