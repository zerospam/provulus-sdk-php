<?php
/**
 * Created by PhpStorm.
 * User: ycoutu
 * Date: 10/11/17
 * Time: 10:51 AM
 */

namespace ProvulusSDK\Enum\Quarantine\Action;

use MabeEnum\Enum;
use MabeEnum\EnumMap;
use ProvulusSDK\Client\Request\Resource\PrimalValued;
use ProvulusSDK\Client\Request\Resource\PrimalValuedEnumTrait;
use ReflectionClass;

/**
 * Class QuarantineActionEnum
 *
 * @method static QuarantineActionEnum RELEASE()
 * @method static QuarantineActionEnum DECLARE_AS_SPAM()
 * @method static QuarantineActionEnum RELEARN()
 * @method static QuarantineActionEnum DELETE()
 * @method static QuarantineActionEnum RESTORE()
 * @method static QuarantineActionEnum RELEASE_AND_ALLOW()
 *
 * @package ProvulusSDK\Enum\Quarantine\Action
 */
class QuarantineActionEnum extends Enum implements PrimalValued
{
    use PrimalValuedEnumTrait;

    const RELEASE               = 'release';
    const DECLARE_AS_SPAM       = 'declare_as_spam';
    const RELEARN               = 'relearn';
    const DELETE                = 'delete';
    const RESTORE               = 'restore';
    const RELEASE_AND_ALLOW = 'release_and_allow';

    /**
     * @var EnumMap
     */
    private static $map;

    /**
     * Map of type to class
     *
     * @return EnumMap
     */
    private static function map()
    {
        if (isset(self::$map)) {
            return self::$map;
        }
        $map = new EnumMap(self::class);
        $map->attach(self::RELEASE, QuarantineActionRelease::class);
        $map->attach(self::DECLARE_AS_SPAM, QuarantineActionDeclareSpam::class);
        $map->attach(self::RELEARN, QuarantineActionRelearn::class);
        $map->attach(self::DELETE, QuarantineActionDelete::class);
        $map->attach(self::RESTORE, QuarantineActionRestore::class);
        $map->attach(self::RELEASE_AND_ALLOW, QuarantineActionReleaseAllow::class);

        return self::$map = $map;
    }

    /**
     * @return IQuarantineAction
     */
    public function buildType()
    {
        $class = self::map()[$this];

        /** @var IQuarantineAction $object */
        $object = (new ReflectionClass($class))->newInstance();
        return $object;
    }
}
