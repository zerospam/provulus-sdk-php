<?php
/**
 * Created by PhpStorm.
 * User: ycoutu
 * Date: 10/11/17
 * Time: 10:58 AM
 */

namespace ProvulusSDK\Enum\Quarantine\Action;

interface IQuarantineAction
{
    /**
     * Get the route suffix related to this action
     *
     * @return string
     */
    public function getRoute();
}
