<?php
/**
 * Created by PhpStorm.
 * User: ycoutu
 * Date: 10/11/17
 * Time: 11:01 AM
 */

namespace ProvulusSDK\Enum\Quarantine\Action;

class QuarantineActionDelete implements IQuarantineAction
{

    /**
     * Get the route suffix related to this action
     *
     * @return string
     */
    public function getRoute()
    {
        return 'delete';
    }
}
