<?php
/**
 * Created by PhpStorm.
 * User: ycoutu
 * Date: 09/11/17
 * Time: 4:46 PM
 */

namespace ProvulusSDK\Enum\Quarantine\Type;

interface IQuarantineType
{
    /**
     * Returns the required bindings for this type
     *
     * @return string[]
     */
    public function getRequiredBindings();

    /**
     * Returns the route related to the quarantine type
     *
     * @return string
     */
    public function getRoute();
}
