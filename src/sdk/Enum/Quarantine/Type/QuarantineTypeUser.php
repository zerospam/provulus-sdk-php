<?php
/**
 * Created by PhpStorm.
 * User: ycoutu
 * Date: 09/11/17
 * Time: 4:46 PM
 */

namespace ProvulusSDK\Enum\Quarantine\Type;

class QuarantineTypeUser implements IQuarantineType
{

    /**
     * Returns the required bindings for this type
     *
     * @return string[]
     */
    public function getRequiredBindings()
    {
        return [
            'orgId',
            'userId'
        ];
    }

    /**
     * Returns the route related to the quarantine type
     *
     * @return string
     */
    public function getRoute()
    {
        return 'orgs/:orgId/users/:userId';
    }
}
