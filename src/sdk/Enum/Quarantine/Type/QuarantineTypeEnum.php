<?php
/**
 * Created by PhpStorm.
 * User: ycoutu
 * Date: 09/11/17
 * Time: 4:28 PM
 */

namespace ProvulusSDK\Enum\Quarantine\Type;

use MabeEnum\Enum;
use MabeEnum\EnumMap;
use ProvulusSDK\Client\Request\Resource\PrimalValued;
use ProvulusSDK\Client\Request\Resource\PrimalValuedEnumTrait;

/**
 * Class QuarantineTypeEnum
 *
 * @method static QuarantineTypeEnum SYSTEM()
 * @method static QuarantineTypeEnum ORGANIZATION()
 * @method static QuarantineTypeEnum USER()
 * @method static QuarantineTypeEnum AGGREGATED()
 *
 * @package ProvulusSDK\Enum\Quarantine
 */
class QuarantineTypeEnum extends Enum implements PrimalValued
{
    use PrimalValuedEnumTrait;

    const SYSTEM       = 'system';
    const ORGANIZATION = 'organization';
    const USER         = 'user';
    const AGGREGATED   = 'aggregated';

    /**
     * @var EnumMap
     */
    private static $map;

    /**
     * Map of type to class
     *
     * @return EnumMap
     */
    private static function map()
    {
        if (isset(self::$map)) {
            return self::$map;
        }
        $map = new EnumMap(self::class);
        $map->attach(self::SYSTEM, QuarantineTypeSystem::class);
        $map->attach(self::ORGANIZATION, QuarantineTypeOrganization::class);
        $map->attach(self::USER, QuarantineTypeUser::class);
        $map->attach(self::AGGREGATED, QuarantineTypeAggregated::class);

        return self::$map = $map;
    }

    /**
     * @return IQuarantineType
     */
    public function buildType()
    {
        $class = self::map()[$this];

        /** @var IQuarantineType $object */
        $object = new $class();

        return $object;
    }
}
