<?php
/**
 * Created by PhpStorm.
 * User: ycoutu
 * Date: 18/09/17
 * Time: 1:53 PM
 */

namespace ProvulusSDK\Enum\Domain\Identity;

use MabeEnum\Enum;
use MabeEnum\EnumMap;
use ProvulusSDK\Client\Request\Resource\PrimalValued;
use ProvulusSDK\Client\Request\Resource\PrimalValuedEnumTrait;
use ReflectionClass;

/**
 * Class IdentityAuthenticationTypeEnum
 *
 * @method static IdentityAuthenticationTypeEnum IDENTITY_IP();
 * @method static IdentityAuthenticationTypeEnum IDENTITY_LOGIN()
 * @method static IdentityAuthenticationTypeEnum IDENTITY_SHARED_IP()
 * @method static IdentityAuthenticationTypeEnum IDENTITY_OFFICE365()
 * @package ProvulusSDK\Enum\Domain\Identity
 */
class IdentityAuthenticationTypeEnum extends Enum implements PrimalValued
{
    use PrimalValuedEnumTrait;

    const IDENTITY_IP        = 'identity_ip';
    const IDENTITY_LOGIN     = 'identity_login';
    const IDENTITY_SHARED_IP = 'identity_shared_ip';
    const IDENTITY_OFFICE365 = 'identity_office365';

    private static $map;

    /**
     * @return EnumMap
     */
    private static function map()
    {
        if (isset(self::$map)) {
            return self::$map;
        }

        $map = new EnumMap(self::class);
        $map->attach(self::IDENTITY_IP, IdentityAuthenticationTypeIP::class);
        $map->attach(self::IDENTITY_LOGIN, IdentityAuthenticationTypeLogin::class);
        $map->attach(self::IDENTITY_SHARED_IP, IdentityAuthentificationTypeSharedIp::class);
        $map->attach(self::IDENTITY_OFFICE365, IdentityAuthenticationTypeOffice365::class);

        return self::$map = $map;
    }

    /**
     * Build the corresponding type from enum
     *
     * @return IIdentityAuthenticationType
     */
    public function buildType()
    {
        $class = self::map()[$this];

        /** @var IIdentityAuthenticationType $identityType */
        $identityType = (new ReflectionClass($class))->newInstance();

        return $identityType;
    }
}
