<?php
/**
 * Created by PhpStorm.
 * User: ycoutu
 * Date: 17/10/17
 * Time: 1:54 PM
 */

namespace ProvulusSDK\Enum\Domain\Identity;

interface IIdentityAuthenticationType
{
    /**
     * Returns callback that will be used to transform the details
     *
     * @return callable
     */
    public function getResponseCallback();

    /**
     * Tells if the details are a collection of responses or a single responses
     *
     * @return bool
     */
    public function isResponseCollection();
}
