<?php
/**
 * Created by PhpStorm.
 * User: ycoutu
 * Date: 17/10/17
 * Time: 1:54 PM
 */

namespace ProvulusSDK\Enum\Domain\Identity;

use ProvulusSDK\Client\Response\Domain\Identity\Type\OutIdentityIpResponse;

class IdentityAuthenticationTypeIP implements IIdentityAuthenticationType
{
    /**
     * Returns callback that will be used to transform the details
     *
     * @return callable
     */
    public function getResponseCallback()
    {
        return function (array $data) {
            return new OutIdentityIpResponse($data);
        };
    }

    /**
     * Tells if the details are a collection of responses or a single responses
     *
     * @return bool
     */
    public function isResponseCollection()
    {
        return true;
    }
}
