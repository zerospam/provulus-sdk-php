<?php
/**
 * Created by PhpStorm.
 * User: pbb
 * Date: 20/12/16
 * Time: 3:15 PM
 */

namespace ProvulusSDK\Enum\Domain;


use MabeEnum\Enum;
use ProvulusSDK\Client\Request\Resource\PrimalValued;
use ProvulusSDK\Client\Request\Resource\PrimalValuedEnumTrait;

/**
 * Class DomainTransportProtocol
 *
 * Different transfer protocol
 *
 * @method static DomainTransportProtocolEnum NOTRANSPORT();
 * @method static DomainTransportProtocolEnum SMTP()
 * @method static DomainTransportProtocolEnum SMTPNOV6()
 * @package Models\Domain\Transport
 */
class DomainTransportProtocolEnum extends Enum implements PrimalValued
{
    use PrimalValuedEnumTrait;
    const NOTRANSPORT = 'notransport';
    const SMTP        = 'smtp';
    const SMTPNOV6    = 'smtpnov6';
}