<?php
/**
 * Created by PhpStorm.
 * User: pbb
 * Date: 19/12/16
 * Time: 10:22 AM
 */

namespace ProvulusSDK\Enum\Domain;

use MabeEnum\Enum;
use ProvulusSDK\Client\Request\Resource\PrimalValued;
use ProvulusSDK\Client\Request\Resource\PrimalValuedEnumTrait;


/**
 * Class DomainTypeEnum
 *
 * Enum for domain types
 *
 * @method static DomainTypeEnum STANDARD()
 * @method static DomainTypeEnum MIRROR()
 * @method static DomainTypeEnum STUDENT()
 * @method static DomainTypeEnum STUDENT_FAKE()
 *
 * @package ProvulusSDK\Enum\Domain
 */
class DomainTypeEnum extends Enum implements PrimalValued
{
    use PrimalValuedEnumTrait;

    const STANDARD     = 'standard';
    const MIRROR       = 'mirror';
    const STUDENT      = 'student';
    const STUDENT_FAKE = 'student_fake';
}
