<?php
/**
 * Created by PhpStorm.
 * User: pbb
 * Date: 06/06/17
 * Time: 3:46 PM
 */

namespace ProvulusSDK\Enum\Ldap;

use MabeEnum\Enum;
use ProvulusSDK\Client\Request\Resource\PrimalValued;
use ProvulusSDK\Client\Request\Resource\PrimalValuedEnumTrait;

/**
 * Class LdapConfigSyncFrequencyEnum
 *
 * @method static LdapConfigSyncFrequencyEnum EVERY_DAY()
 * @method static LdapConfigSyncFrequencyEnum EVERY_TWELVE_HOURS()
 * @method static LdapConfigSyncFrequencyEnum EVERY_EIGHT_HOURS()
 * @method static LdapConfigSyncFrequencyEnum EVERY_SIX_HOURS()
 * @method static LdapConfigSyncFrequencyEnum EVERY_HOUR()
 * @method static LdapConfigSyncFrequencyEnum EVERY_TWO_HOURS()
 *
 * @package Cumulus\Models\Ldap\Enum
 */
class LdapConfigSyncFrequencyEnum extends Enum implements PrimalValued
{
    use PrimalValuedEnumTrait;

    const EVERY_DAY          = 'every_day';
    const EVERY_TWELVE_HOURS = 'every_twelve_hours';
    const EVERY_EIGHT_HOURS  = 'every_eight_hours';
    const EVERY_SIX_HOURS    = 'every_six_hours';
    const EVERY_TWO_HOURS    = 'every_two_hours';

}