<?php
/**
 * Created by PhpStorm.
 * User: pbb
 * Date: 13/06/17
 * Time: 1:28 PM
 */

namespace ProvulusSDK\Enum\Ldap;

use MabeEnum\Enum;
use ProvulusSDK\Client\Request\Resource\PrimalValued;
use ProvulusSDK\Client\Request\Resource\PrimalValuedEnumTrait;

/**
 * Class LdapDifferentialEnum
 *
 * @method static LdapDifferentialEnum ADDED()
 * @method static LdapDifferentialEnum DELETED()
 * @method static LdapDifferentialEnum MODIFIED()
 *
 * @package ProvulusSDK\Enum\LDAP
 */
class LdapDifferentialEnum extends Enum implements PrimalValued
{
    use PrimalValuedEnumTrait;

    const ADDED    = 'added';
    const DELETED  = 'deleted';
    const MODIFIED = 'modified';
}