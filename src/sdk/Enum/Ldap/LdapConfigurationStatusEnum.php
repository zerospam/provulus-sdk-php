<?php
/**
 * Created by PhpStorm.
 * User: ycoutu
 * Date: 04/09/18
 * Time: 2:46 PM
 */

namespace ProvulusSDK\Enum\Ldap;

use MabeEnum\Enum;

/**
 * Class LdapConfigurationStatusEnum
 *
 * @method static LdapConfigurationStatusEnum DONE()
 * @method static LdapConfigurationStatusEnum VALIDATE()
 * @method static LdapConfigurationStatusEnum SYNCHRONIZING()
 * @method static LdapConfigurationStatusEnum APPLY()
 * @method static LdapConfigurationStatusEnum CANCEL()
 * @package ProvulusSDK\Enum\LDAP
 */
class LdapConfigurationStatusEnum extends Enum
{
    const DONE          = 'done';
    const VALIDATE      = 'validate';
    const SYNCHRONIZING = 'synchronizing';
    const APPLY         = 'apply';
    const CANCEL        = 'cancel';
}
