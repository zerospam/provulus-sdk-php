<?php
/**
 * Created by PhpStorm.
 * User: pbb
 * Date: 13/06/17
 * Time: 1:06 PM
 */

namespace ProvulusSDK\Enum\Ldap;

use MabeEnum\Enum;
use ProvulusSDK\Client\Request\Resource\PrimalValued;
use ProvulusSDK\Client\Request\Resource\PrimalValuedEnumTrait;

/**
 * Class LdapSyncStatusEnum
 *
 * Synchronization Status of LdapSync
 *
 * @method static LdapSyncStatusEnum OK()
 * @method static LdapSyncStatusEnum ERROR()
 * @method static LdapSyncStatusEnum REQ()
 * @method static LdapSyncStatusEnum SYNCHRONIZING()
 * @method static LdapSyncStatusEnum AWAITING_CONFIRMATION()
 * @method static LdapSyncStatusEnum CANCELED()
 * @method static LdapSyncStatusEnum NOTHING()
 * @method static LdapSyncStatusEnum AUTO_REQ()
 * @method static LdapSyncStatusEnum DEAD()
 * @method static LdapSyncStatusEnum CONNECTION_FAILED()
 * @package ProvulusSDK\Enum\LDAP
 */
class LdapSyncStatusEnum extends Enum implements PrimalValued
{

    use PrimalValuedEnumTrait;

    const OK                    = 'ok';
    const ERROR                 = 'error';
    const REQ                   = 'req';
    const SYNCHRONIZING         = 'synchronizing';
    const AWAITING_CONFIRMATION = 'awaiting_confirmation';
    const CANCELED              = 'canceled';
    const NOTHING               = 'nothing';
    const AUTO_REQ              = 'auto_req';
    const DEAD                  = 'dead';
    const CONNECTION_FAILED     = 'connection_failed';
}