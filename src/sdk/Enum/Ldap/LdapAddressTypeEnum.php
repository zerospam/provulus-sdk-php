<?php
/**
 * Created by PhpStorm.
 * User: ycoutu
 * Date: 14/06/17
 * Time: 1:34 PM
*/

namespace ProvulusSDK\Enum\Ldap;

use MabeEnum\Enum;
use ProvulusSDK\Client\Request\Resource\PrimalValued;
use ProvulusSDK\Client\Request\Resource\PrimalValuedEnumTrait;

/**
 * Class LdapAddressTypeEnum
 *
 * Type of the LDAP address
 *
 * @method static LdapAddressTypeEnum PRIMARY_ADDRESS()
 * @method static LdapAddressTypeEnum ALIAS_ADDRESS()
 *
 * @package ProvulusSDK\Enum\LDAP
 */
class LdapAddressTypeEnum extends Enum implements PrimalValued
{
    use PrimalValuedEnumTrait;


    const PRIMARY_ADDRESS = "primary_address";
    const ALIAS_ADDRESS   = "alias_address";
}
