<?php

namespace ProvulusSDK\Enum\Billing;

use MabeEnum\Enum;
use ProvulusSDK\Client\Request\Resource\PrimalValued;
use ProvulusSDK\Client\Request\Resource\PrimalValuedEnumTrait;

/**
 * Class BillingPeriodEnum
 *
 * Different possible billing periods
 *
 * @method static BillingPeriodEnum MONTHLY()
 * @method static BillingPeriodEnum ANNUAL()
 * @method static BillingPeriodEnum BIENNIAL()
 * @method static BillingPeriodEnum TRIENNIAL()
 * @package ProvulusSDK\Enum\Billing
 */
class BillingPeriodEnum extends Enum implements PrimalValued
{
    use PrimalValuedEnumTrait;

    const MONTHLY = 'monthly';
    const ANNUAL = 'annual';
    const BIENNIAL = 'biennial';
    const TRIENNIAL = 'triennial';
}