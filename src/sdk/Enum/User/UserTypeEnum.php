<?php
/**
 * Created by PhpStorm.
 * User: pbb
 * Date: 27/02/17
 * Time: 2:19 PM
 */

namespace ProvulusSDK\Enum\User;

use MabeEnum\Enum;
use ProvulusSDK\Client\Request\Resource\PrimalValued;
use ProvulusSDK\Client\Request\Resource\PrimalValuedEnumTrait;

/**
 * Class UserTypeEnum
 *
 * Type of users
 * @method static UserTypeEnum ADMIN()
 * @method static UserTypeEnum REGULAR()
 * @method static UserTypeEnum LDAP()
 * @method static UserTypeEnum ADMIN_QUAR()
 * @method static UserTypeEnum SYSTEM()
 *
 * @package ProvulusSDK\Enum\User
 */
class UserTypeEnum extends Enum implements PrimalValued
{
    use PrimalValuedEnumTrait;

    const SYSTEM     = 'system';
    const ADMIN      = 'admin';
    const REGULAR    = 'regular';
    const ADMIN_QUAR = 'admin_quar';
    const LDAP       = 'ldap';
}