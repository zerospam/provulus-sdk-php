<?php
/**
 * Created by PhpStorm.
 * User: ycoutu
 * Date: 18/12/17
 * Time: 2:13 PM
 */

namespace ProvulusSDK\Enum\Login;

use MabeEnum\Enum;
use ProvulusSDK\Client\Request\Resource\PrimalValued;
use ProvulusSDK\Client\Request\Resource\PrimalValuedEnumTrait;

/**
 * Class TwoFactorAuthUsersEnum
 *
 * @method static TwoFactorAuthUsersEnum OPTIONAL()
 * @method static TwoFactorAuthUsersEnum ADMINISTRATOR()
 * @method static TwoFactorAuthUsersEnum ALL()
 *
 * @package ProvulusSDK\Enum\Login
 */
class TwoFactorAuthUsersEnum extends Enum implements PrimalValued
{
    use PrimalValuedEnumTrait;

    const OPTIONAL = 'optional';
    const ADMINISTRATOR = 'administrator';
    const ALL = 'all';
}
