<?php
/**
 * Created by PhpStorm.
 * User: pbb
 * Date: 09/04/18
 * Time: 2:38 PM
 */

namespace ProvulusSDK\Enum\Organization\PaymentState;

use MabeEnum\Enum;
use ProvulusSDK\Client\Request\Resource\PrimalValued;
use ProvulusSDK\Client\Request\Resource\PrimalValuedEnumTrait;

/**
 * Class PaymentStateEnum
 *
 * @method static PaymentStateEnum PAYING()
 * @method static PaymentStateEnum IN_TRIAL()
 *
 * @package ProvulusSDK\Enum\Organization\PaymentState
 */
class PaymentStateEnum extends Enum implements PrimalValued
{
    use PrimalValuedEnumTrait;

    const PAYING   = 'paying';
    const IN_TRIAL = 'in trial';
}