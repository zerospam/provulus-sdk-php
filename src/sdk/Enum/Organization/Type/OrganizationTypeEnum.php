<?php
/**
 * Created by PhpStorm.
 * User: ycoutu
 * Date: 26/09/18
 * Time: 10:01 AM
 */

namespace ProvulusSDK\Enum\Organization\Type;

use MabeEnum\Enum;
use ProvulusSDK\Client\Request\Resource\PrimalValued;
use ProvulusSDK\Client\Request\Resource\PrimalValuedEnumTrait;

/**
 * Class OrganizationTypeEnum
 *
 * @method static OrganizationTypeEnum OWNER_COMPANY()
 * @method static OrganizationTypeEnum SUPPORT()
 * @method static OrganizationTypeEnum BILLING()
 * @method static OrganizationTypeEnum SUPPORT_BILLING()
 * @method static OrganizationTypeEnum DISTRIBUTOR()
 * @method static OrganizationTypeEnum NORMAL()
 * @method static OrganizationTypeEnum DISTRIBUTED_RESELLER()
 * @method static OrganizationTypeEnum RESELLER_CLIENT()
 *
 * @package ProvulusSDK\Enum\Organization\Type
 */
class OrganizationTypeEnum extends Enum implements PrimalValued
{
    use PrimalValuedEnumTrait;

    const OWNER_COMPANY        = 'owner';
    const SUPPORT              = 'support';
    const BILLING              = 'billing';
    const SUPPORT_BILLING      = 'support&billing';
    const DISTRIBUTOR          = 'distributor';
    const NORMAL               = 'basic';
    const DISTRIBUTED_RESELLER = 'distributed_reseller';
    const RESELLER_CLIENT      = 'reseller_client';
    const DISTRIBUTED_CLIENT   = 'distributed_client';
}
