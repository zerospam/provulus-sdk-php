<?php


namespace ProvulusSDK\Enum\Organization;

use MabeEnum\Enum;
use ProvulusSDK\Client\Request\Resource\PrimalValued;
use ProvulusSDK\Client\Request\Resource\PrimalValuedEnumTrait;

/**
 * Class CountryEnum
 *
 * @method static CountryEnum AFGHANISTAN()
 * @method static CountryEnum ALAND_ISLANDS()
 * @method static CountryEnum ALBANIA()
 * @method static CountryEnum ALGERIA()
 * @method static CountryEnum AMERICAN_SAMOA()
 * @method static CountryEnum ANDORRA()
 * @method static CountryEnum ANGOLA()
 * @method static CountryEnum ANGUILLA()
 * @method static CountryEnum ANTARCTICA()
 * @method static CountryEnum ANTIGUA_BARBUDA()
 * @method static CountryEnum ARGENTINA()
 * @method static CountryEnum ARMENIA()
 * @method static CountryEnum ARUBA()
 * @method static CountryEnum ASCENSION_ISLAND()
 * @method static CountryEnum AUSTRALIA()
 * @method static CountryEnum AUSTRIA()
 * @method static CountryEnum AZERBAIJAN()
 * @method static CountryEnum BAHAMAS()
 * @method static CountryEnum BAHRAIN()
 * @method static CountryEnum BANGLADESH()
 * @method static CountryEnum BARBADOS()
 * @method static CountryEnum BELARUS()
 * @method static CountryEnum BELGIUM()
 * @method static CountryEnum BELIZE()
 * @method static CountryEnum BENIN()
 * @method static CountryEnum BERMUDA()
 * @method static CountryEnum BHUTAN()
 * @method static CountryEnum BOLIVIA()
 * @method static CountryEnum BOSNIA_HERZEGOVINA()
 * @method static CountryEnum BOTSWANA()
 * @method static CountryEnum BRAZIL()
 * @method static CountryEnum BRITISH_INDIAN_OCEAN_TERRITORY()
 * @method static CountryEnum BRITISH_VIRGIN_ISLANDS()
 * @method static CountryEnum BRUNEI()
 * @method static CountryEnum BULGARIA()
 * @method static CountryEnum BURKINA_FASO()
 * @method static CountryEnum BURUNDI()
 * @method static CountryEnum CAMBODIA()
 * @method static CountryEnum CAMEROON()
 * @method static CountryEnum CANADA()
 * @method static CountryEnum CANARY_ISLANDS()
 * @method static CountryEnum CAPE_VERDE()
 * @method static CountryEnum CARIBBEAN_NETHERLANDS()
 * @method static CountryEnum CAYMAN_ISLANDS()
 * @method static CountryEnum CENTRAL_AFRICAN_REPUBLIC()
 * @method static CountryEnum CEUTA_MELILLA()
 * @method static CountryEnum CHAD()
 * @method static CountryEnum CHILE()
 * @method static CountryEnum CHINA()
 * @method static CountryEnum CHRISTMAS_ISLAND()
 * @method static CountryEnum COCOS_ISLANDS()
 * @method static CountryEnum COLOMBIA()
 * @method static CountryEnum COMOROS()
 * @method static CountryEnum CONGO_BRAZZAVILLE()
 * @method static CountryEnum CONGO_KINSHASA()
 * @method static CountryEnum COOK_ISLANDS()
 * @method static CountryEnum COSTA_RICA()
 * @method static CountryEnum COTE_DIVOIRE()
 * @method static CountryEnum CROATIA()
 * @method static CountryEnum CUBA()
 * @method static CountryEnum CURACAO()
 * @method static CountryEnum CYPRUS()
 * @method static CountryEnum CZECHIA()
 * @method static CountryEnum DENMARK()
 * @method static CountryEnum DIEGO_GARCIA()
 * @method static CountryEnum DJIBOUTI()
 * @method static CountryEnum DOMINICA()
 * @method static CountryEnum DOMINICAN_REPUBLIC()
 * @method static CountryEnum ECUADOR()
 * @method static CountryEnum EGYPT()
 * @method static CountryEnum EL_SALVADOR()
 * @method static CountryEnum EQUATORIAL_GUINEA()
 * @method static CountryEnum ERITREA()
 * @method static CountryEnum ESTONIA()
 * @method static CountryEnum ETHIOPIA()
 * @method static CountryEnum EUROZONE()
 * @method static CountryEnum FALKLAND_ISLANDS()
 * @method static CountryEnum FAROE_ISLANDS()
 * @method static CountryEnum FIJI()
 * @method static CountryEnum FINLAND()
 * @method static CountryEnum FRANCE()
 * @method static CountryEnum FRENCH_GUIANA()
 * @method static CountryEnum FRENCH_POLYNESIA()
 * @method static CountryEnum FRENCH_SOUTHERN_TERRITORIES()
 * @method static CountryEnum GABON()
 * @method static CountryEnum GAMBIA()
 * @method static CountryEnum GEORGIA()
 * @method static CountryEnum GERMANY()
 * @method static CountryEnum GHANA()
 * @method static CountryEnum GIBRALTAR()
 * @method static CountryEnum GREECE()
 * @method static CountryEnum GREENLAND()
 * @method static CountryEnum GRENADA()
 * @method static CountryEnum GUADELOUPE()
 * @method static CountryEnum GUAM()
 * @method static CountryEnum GUATEMALA()
 * @method static CountryEnum GUERNSEY()
 * @method static CountryEnum GUINEA()
 * @method static CountryEnum GUINEABISSAU()
 * @method static CountryEnum GUYANA()
 * @method static CountryEnum HAITI()
 * @method static CountryEnum HONDURAS()
 * @method static CountryEnum HONG_KONG_SAR_CHINA()
 * @method static CountryEnum HUNGARY()
 * @method static CountryEnum ICELAND()
 * @method static CountryEnum INDIA()
 * @method static CountryEnum INDONESIA()
 * @method static CountryEnum IRAN()
 * @method static CountryEnum IRAQ()
 * @method static CountryEnum IRELAND()
 * @method static CountryEnum ISLE_OF_MAN()
 * @method static CountryEnum ISRAEL()
 * @method static CountryEnum ITALY()
 * @method static CountryEnum JAMAICA()
 * @method static CountryEnum JAPAN()
 * @method static CountryEnum JERSEY()
 * @method static CountryEnum JORDAN()
 * @method static CountryEnum KAZAKHSTAN()
 * @method static CountryEnum KENYA()
 * @method static CountryEnum KIRIBATI()
 * @method static CountryEnum KOSOVO()
 * @method static CountryEnum KUWAIT()
 * @method static CountryEnum KYRGYZSTAN()
 * @method static CountryEnum LAOS()
 * @method static CountryEnum LATVIA()
 * @method static CountryEnum LEBANON()
 * @method static CountryEnum LESOTHO()
 * @method static CountryEnum LIBERIA()
 * @method static CountryEnum LIBYA()
 * @method static CountryEnum LIECHTENSTEIN()
 * @method static CountryEnum LITHUANIA()
 * @method static CountryEnum LUXEMBOURG()
 * @method static CountryEnum MACAU_SAR_CHINA()
 * @method static CountryEnum MACEDONIA()
 * @method static CountryEnum MADAGASCAR()
 * @method static CountryEnum MALAWI()
 * @method static CountryEnum MALAYSIA()
 * @method static CountryEnum MALDIVES()
 * @method static CountryEnum MALI()
 * @method static CountryEnum MALTA()
 * @method static CountryEnum MARSHALL_ISLANDS()
 * @method static CountryEnum MARTINIQUE()
 * @method static CountryEnum MAURITANIA()
 * @method static CountryEnum MAURITIUS()
 * @method static CountryEnum MAYOTTE()
 * @method static CountryEnum MEXICO()
 * @method static CountryEnum MICRONESIA()
 * @method static CountryEnum MOLDOVA()
 * @method static CountryEnum MONACO()
 * @method static CountryEnum MONGOLIA()
 * @method static CountryEnum MONTENEGRO()
 * @method static CountryEnum MONTSERRAT()
 * @method static CountryEnum MOROCCO()
 * @method static CountryEnum MOZAMBIQUE()
 * @method static CountryEnum MYANMAR()
 * @method static CountryEnum NAMIBIA()
 * @method static CountryEnum NAURU()
 * @method static CountryEnum NEPAL()
 * @method static CountryEnum NETHERLANDS()
 * @method static CountryEnum NEW_CALEDONIA()
 * @method static CountryEnum NEW_ZEALAND()
 * @method static CountryEnum NICARAGUA()
 * @method static CountryEnum NIGER()
 * @method static CountryEnum NIGERIA()
 * @method static CountryEnum NIUE()
 * @method static CountryEnum NORFOLK_ISLAND()
 * @method static CountryEnum NORTH_KOREA()
 * @method static CountryEnum NORTHERN_MARIANA_ISLANDS()
 * @method static CountryEnum NORWAY()
 * @method static CountryEnum OMAN()
 * @method static CountryEnum PAKISTAN()
 * @method static CountryEnum PALAU()
 * @method static CountryEnum PALESTINIAN_TERRITORIES()
 * @method static CountryEnum PANAMA()
 * @method static CountryEnum PAPUA_NEW_GUINEA()
 * @method static CountryEnum PARAGUAY()
 * @method static CountryEnum PERU()
 * @method static CountryEnum PHILIPPINES()
 * @method static CountryEnum PITCAIRN_ISLANDS()
 * @method static CountryEnum POLAND()
 * @method static CountryEnum PORTUGAL()
 * @method static CountryEnum PUERTO_RICO()
 * @method static CountryEnum QATAR()
 * @method static CountryEnum REUNION()
 * @method static CountryEnum ROMANIA()
 * @method static CountryEnum RUSSIA()
 * @method static CountryEnum RWANDA()
 * @method static CountryEnum SAMOA()
 * @method static CountryEnum SAN_MARINO()
 * @method static CountryEnum SAO_TOME_PRINCIPE()
 * @method static CountryEnum SAUDI_ARABIA()
 * @method static CountryEnum SENEGAL()
 * @method static CountryEnum SERBIA()
 * @method static CountryEnum SEYCHELLES()
 * @method static CountryEnum SIERRA_LEONE()
 * @method static CountryEnum SINGAPORE()
 * @method static CountryEnum SINT_MAARTEN()
 * @method static CountryEnum SLOVAKIA()
 * @method static CountryEnum SLOVENIA()
 * @method static CountryEnum SOLOMON_ISLANDS()
 * @method static CountryEnum SOMALIA()
 * @method static CountryEnum SOUTH_AFRICA()
 * @method static CountryEnum SOUTH_GEORGIA_SOUTH_SANDWICH_ISLANDS()
 * @method static CountryEnum SOUTH_KOREA()
 * @method static CountryEnum SOUTH_SUDAN()
 * @method static CountryEnum SPAIN()
 * @method static CountryEnum SRI_LANKA()
 * @method static CountryEnum ST_BARTHELEMY()
 * @method static CountryEnum ST_HELENA()
 * @method static CountryEnum ST_KITTS_NEVIS()
 * @method static CountryEnum ST_LUCIA()
 * @method static CountryEnum ST_MARTIN()
 * @method static CountryEnum ST_PIERRE_MIQUELON()
 * @method static CountryEnum ST_VINCENT_GRENADINES()
 * @method static CountryEnum SUDAN()
 * @method static CountryEnum SURINAME()
 * @method static CountryEnum SVALBARD_JAN_MAYEN()
 * @method static CountryEnum SWAZILAND()
 * @method static CountryEnum SWEDEN()
 * @method static CountryEnum SWITZERLAND()
 * @method static CountryEnum SYRIA()
 * @method static CountryEnum TAIWAN()
 * @method static CountryEnum TAJIKISTAN()
 * @method static CountryEnum TANZANIA()
 * @method static CountryEnum THAILAND()
 * @method static CountryEnum TIMORLESTE()
 * @method static CountryEnum TOGO()
 * @method static CountryEnum TOKELAU()
 * @method static CountryEnum TONGA()
 * @method static CountryEnum TRINIDAD_TOBAGO()
 * @method static CountryEnum TRISTAN_DA_CUNHA()
 * @method static CountryEnum TUNISIA()
 * @method static CountryEnum TURKEY()
 * @method static CountryEnum TURKMENISTAN()
 * @method static CountryEnum TURKS_CAICOS_ISLANDS()
 * @method static CountryEnum TUVALU()
 * @method static CountryEnum US_OUTLYING_ISLANDS()
 * @method static CountryEnum US_VIRGIN_ISLANDS()
 * @method static CountryEnum UGANDA()
 * @method static CountryEnum UKRAINE()
 * @method static CountryEnum UNITED_ARAB_EMIRATES()
 * @method static CountryEnum UNITED_KINGDOM()
 * @method static CountryEnum UNITED_NATIONS()
 * @method static CountryEnum UNITED_STATES()
 * @method static CountryEnum URUGUAY()
 * @method static CountryEnum UZBEKISTAN()
 * @method static CountryEnum VANUATU()
 * @method static CountryEnum VATICAN_CITY()
 * @method static CountryEnum VENEZUELA()
 * @method static CountryEnum VIETNAM()
 * @method static CountryEnum WALLIS_FUTUNA()
 * @method static CountryEnum WESTERN_SAHARA()
 * @method static CountryEnum YEMEN()
 * @method static CountryEnum ZAMBIA()
 * @method static CountryEnum ZIMBABWE()
 *
 * @package ProvulusSDK\Enum\Organization
 */
class CountryEnum extends Enum implements PrimalValued
{
    use PrimalValuedEnumTrait;

    const AFGHANISTAN = 'AF';
    const ALAND_ISLANDS = 'AX';
    const ALBANIA = 'AL';
    const ALGERIA = 'DZ';
    const AMERICAN_SAMOA = 'AS';
    const ANDORRA = 'AD';
    const ANGOLA = 'AO';
    const ANGUILLA = 'AI';
    const ANTARCTICA = 'AQ';
    const ANTIGUA_BARBUDA = 'AG';
    const ARGENTINA = 'AR';
    const ARMENIA = 'AM';
    const ARUBA = 'AW';
    const ASCENSION_ISLAND = 'AC';
    const AUSTRALIA = 'AU';
    const AUSTRIA = 'AT';
    const AZERBAIJAN = 'AZ';
    const BAHAMAS = 'BS';
    const BAHRAIN = 'BH';
    const BANGLADESH = 'BD';
    const BARBADOS = 'BB';
    const BELARUS = 'BY';
    const BELGIUM = 'BE';
    const BELIZE = 'BZ';
    const BENIN = 'BJ';
    const BERMUDA = 'BM';
    const BHUTAN = 'BT';
    const BOLIVIA = 'BO';
    const BOSNIA_HERZEGOVINA = 'BA';
    const BOTSWANA = 'BW';
    const BRAZIL = 'BR';
    const BRITISH_INDIAN_OCEAN_TERRITORY = 'IO';
    const BRITISH_VIRGIN_ISLANDS = 'VG';
    const BRUNEI = 'BN';
    const BULGARIA = 'BG';
    const BURKINA_FASO = 'BF';
    const BURUNDI = 'BI';
    const CAMBODIA = 'KH';
    const CAMEROON = 'CM';
    const CANADA = 'CA';
    const CANARY_ISLANDS = 'IC';
    const CAPE_VERDE = 'CV';
    const CARIBBEAN_NETHERLANDS = 'BQ';
    const CAYMAN_ISLANDS = 'KY';
    const CENTRAL_AFRICAN_REPUBLIC = 'CF';
    const CEUTA_MELILLA = 'EA';
    const CHAD = 'TD';
    const CHILE = 'CL';
    const CHINA = 'CN';
    const CHRISTMAS_ISLAND = 'CX';
    const COCOS_ISLANDS = 'CC';
    const COLOMBIA = 'CO';
    const COMOROS = 'KM';
    const CONGO_BRAZZAVILLE = 'CG';
    const CONGO_KINSHASA = 'CD';
    const COOK_ISLANDS = 'CK';
    const COSTA_RICA = 'CR';
    const COTE_DIVOIRE = 'CI';
    const CROATIA = 'HR';
    const CUBA = 'CU';
    const CURACAO = 'CW';
    const CYPRUS = 'CY';
    const CZECHIA = 'CZ';
    const DENMARK = 'DK';
    const DIEGO_GARCIA = 'DG';
    const DJIBOUTI = 'DJ';
    const DOMINICA = 'DM';
    const DOMINICAN_REPUBLIC = 'DO';
    const ECUADOR = 'EC';
    const EGYPT = 'EG';
    const EL_SALVADOR = 'SV';
    const EQUATORIAL_GUINEA = 'GQ';
    const ERITREA = 'ER';
    const ESTONIA = 'EE';
    const ETHIOPIA = 'ET';
    const EUROZONE = 'EZ';
    const FALKLAND_ISLANDS = 'FK';
    const FAROE_ISLANDS = 'FO';
    const FIJI = 'FJ';
    const FINLAND = 'FI';
    const FRANCE = 'FR';
    const FRENCH_GUIANA = 'GF';
    const FRENCH_POLYNESIA = 'PF';
    const FRENCH_SOUTHERN_TERRITORIES = 'TF';
    const GABON = 'GA';
    const GAMBIA = 'GM';
    const GEORGIA = 'GE';
    const GERMANY = 'DE';
    const GHANA = 'GH';
    const GIBRALTAR = 'GI';
    const GREECE = 'GR';
    const GREENLAND = 'GL';
    const GRENADA = 'GD';
    const GUADELOUPE = 'GP';
    const GUAM = 'GU';
    const GUATEMALA = 'GT';
    const GUERNSEY = 'GG';
    const GUINEA = 'GN';
    const GUINEABISSAU = 'GW';
    const GUYANA = 'GY';
    const HAITI = 'HT';
    const HONDURAS = 'HN';
    const HONG_KONG_SAR_CHINA = 'HK';
    const HUNGARY = 'HU';
    const ICELAND = 'IS';
    const INDIA = 'IN';
    const INDONESIA = 'ID';
    const IRAN = 'IR';
    const IRAQ = 'IQ';
    const IRELAND = 'IE';
    const ISLE_OF_MAN = 'IM';
    const ISRAEL = 'IL';
    const ITALY = 'IT';
    const JAMAICA = 'JM';
    const JAPAN = 'JP';
    const JERSEY = 'JE';
    const JORDAN = 'JO';
    const KAZAKHSTAN = 'KZ';
    const KENYA = 'KE';
    const KIRIBATI = 'KI';
    const KOSOVO = 'XK';
    const KUWAIT = 'KW';
    const KYRGYZSTAN = 'KG';
    const LAOS = 'LA';
    const LATVIA = 'LV';
    const LEBANON = 'LB';
    const LESOTHO = 'LS';
    const LIBERIA = 'LR';
    const LIBYA = 'LY';
    const LIECHTENSTEIN = 'LI';
    const LITHUANIA = 'LT';
    const LUXEMBOURG = 'LU';
    const MACAU_SAR_CHINA = 'MO';
    const MACEDONIA = 'MK';
    const MADAGASCAR = 'MG';
    const MALAWI = 'MW';
    const MALAYSIA = 'MY';
    const MALDIVES = 'MV';
    const MALI = 'ML';
    const MALTA = 'MT';
    const MARSHALL_ISLANDS = 'MH';
    const MARTINIQUE = 'MQ';
    const MAURITANIA = 'MR';
    const MAURITIUS = 'MU';
    const MAYOTTE = 'YT';
    const MEXICO = 'MX';
    const MICRONESIA = 'FM';
    const MOLDOVA = 'MD';
    const MONACO = 'MC';
    const MONGOLIA = 'MN';
    const MONTENEGRO = 'ME';
    const MONTSERRAT = 'MS';
    const MOROCCO = 'MA';
    const MOZAMBIQUE = 'MZ';
    const MYANMAR = 'MM';
    const NAMIBIA = 'NA';
    const NAURU = 'NR';
    const NEPAL = 'NP';
    const NETHERLANDS = 'NL';
    const NEW_CALEDONIA = 'NC';
    const NEW_ZEALAND = 'NZ';
    const NICARAGUA = 'NI';
    const NIGER = 'NE';
    const NIGERIA = 'NG';
    const NIUE = 'NU';
    const NORFOLK_ISLAND = 'NF';
    const NORTH_KOREA = 'KP';
    const NORTHERN_MARIANA_ISLANDS = 'MP';
    const NORWAY = 'NO';
    const OMAN = 'OM';
    const PAKISTAN = 'PK';
    const PALAU = 'PW';
    const PALESTINIAN_TERRITORIES = 'PS';
    const PANAMA = 'PA';
    const PAPUA_NEW_GUINEA = 'PG';
    const PARAGUAY = 'PY';
    const PERU = 'PE';
    const PHILIPPINES = 'PH';
    const PITCAIRN_ISLANDS = 'PN';
    const POLAND = 'PL';
    const PORTUGAL = 'PT';
    const PUERTO_RICO = 'PR';
    const QATAR = 'QA';
    const REUNION = 'RE';
    const ROMANIA = 'RO';
    const RUSSIA = 'RU';
    const RWANDA = 'RW';
    const SAMOA = 'WS';
    const SAN_MARINO = 'SM';
    const SAO_TOME_PRINCIPE = 'ST';
    const SAUDI_ARABIA = 'SA';
    const SENEGAL = 'SN';
    const SERBIA = 'RS';
    const SEYCHELLES = 'SC';
    const SIERRA_LEONE = 'SL';
    const SINGAPORE = 'SG';
    const SINT_MAARTEN = 'SX';
    const SLOVAKIA = 'SK';
    const SLOVENIA = 'SI';
    const SOLOMON_ISLANDS = 'SB';
    const SOMALIA = 'SO';
    const SOUTH_AFRICA = 'ZA';
    const SOUTH_GEORGIA_SOUTH_SANDWICH_ISLANDS = 'GS';
    const SOUTH_KOREA = 'KR';
    const SOUTH_SUDAN = 'SS';
    const SPAIN = 'ES';
    const SRI_LANKA = 'LK';
    const ST_BARTHELEMY = 'BL';
    const ST_HELENA = 'SH';
    const ST_KITTS_NEVIS = 'KN';
    const ST_LUCIA = 'LC';
    const ST_MARTIN = 'MF';
    const ST_PIERRE_MIQUELON = 'PM';
    const ST_VINCENT_GRENADINES = 'VC';
    const SUDAN = 'SD';
    const SURINAME = 'SR';
    const SVALBARD_JAN_MAYEN = 'SJ';
    const SWAZILAND = 'SZ';
    const SWEDEN = 'SE';
    const SWITZERLAND = 'CH';
    const SYRIA = 'SY';
    const TAIWAN = 'TW';
    const TAJIKISTAN = 'TJ';
    const TANZANIA = 'TZ';
    const THAILAND = 'TH';
    const TIMORLESTE = 'TL';
    const TOGO = 'TG';
    const TOKELAU = 'TK';
    const TONGA = 'TO';
    const TRINIDAD_TOBAGO = 'TT';
    const TRISTAN_DA_CUNHA = 'TA';
    const TUNISIA = 'TN';
    const TURKEY = 'TR';
    const TURKMENISTAN = 'TM';
    const TURKS_CAICOS_ISLANDS = 'TC';
    const TUVALU = 'TV';
    const US_OUTLYING_ISLANDS = 'UM';
    const US_VIRGIN_ISLANDS = 'VI';
    const UGANDA = 'UG';
    const UKRAINE = 'UA';
    const UNITED_ARAB_EMIRATES = 'AE';
    const UNITED_KINGDOM = 'GB';
    const UNITED_NATIONS = 'UN';
    const UNITED_STATES = 'US';
    const URUGUAY = 'UY';
    const UZBEKISTAN = 'UZ';
    const VANUATU = 'VU';
    const VATICAN_CITY = 'VA';
    const VENEZUELA = 'VE';
    const VIETNAM = 'VN';
    const WALLIS_FUTUNA = 'WF';
    const WESTERN_SAHARA = 'EH';
    const YEMEN = 'YE';
    const ZAMBIA = 'ZM';
    const ZIMBABWE = 'ZW';
}
