<?php
/**
 * Created by PhpStorm.
 * User: pbb
 * Date: 10/09/18
 * Time: 2:44 PM
 */

namespace ProvulusSDK\Enum\Contact\Mailgroups;

use MabeEnum\Enum;
use ProvulusSDK\Client\Request\Resource\PrimalValued;
use ProvulusSDK\Client\Request\Resource\PrimalValuedEnumTrait;

/**
 * Class MailGroupEnum
 *
 * @method static MailGroupEnum TECHNICAL_SUPPORT()
 * @method static MailGroupEnum SALES_SUPPORT()
 * @method static MailGroupEnum BILLING_SUPPORT()
 * @method static MailGroupEnum NEW_CLIENT_CREATED()
 * @method static MailGroupEnum MAIL_DELIVERY_ISSUES()
 * @method static MailGroupEnum OUTBOUND_FILTERING_CONFIGURATION()
 * @method static MailGroupEnum OTHER()
 * @method static MailGroupEnum SERVICE_CONFIGURATION()
 * @method static MailGroupEnum SERVICE_ACTIVATION()
 * @method static MailGroupEnum SERVICE_DECONFIGURATION()
 * @method static MailGroupEnum TRIAL_EXPIRY()
 * @method static MailGroupEnum YEARLY_MAILBOX_COUNT_UPDATE()
 *
 * @package sdk\Enum\Contact\Mailgroups
 */
class MailGroupEnum extends Enum implements PrimalValued
{
    use PrimalValuedEnumTrait;

    const TECHNICAL_SUPPORT                = 'technical_support';
    const SALES_SUPPORT                    = 'sales_support';
    const BILLING_SUPPORT                  = 'billing_support';
    const NEW_CLIENT_CREATED               = 'new_client_created';
    const MAIL_DELIVERY_ISSUES             = 'mail_delivery_issues';
    const OTHER                            = 'other';
    const OUTBOUND_FILTERING_CONFIGURATION = 'outbound_filtering_configuration';
    const SERVICE_CONFIGURATION            = 'service_configuration';
    const SERVICE_ACTIVATION               = 'service_activation';
    const SERVICE_DECONFIGURATION          = 'service_deconfiguration';
    const TRIAL_EXPIRY                     = 'trial_expiry';
    const YEARLY_MAILBOX_COUNT_UPDATE      = 'yearly_mailbox_count_update';
}
