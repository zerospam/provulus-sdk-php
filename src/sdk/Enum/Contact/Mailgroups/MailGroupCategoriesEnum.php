<?php
/**
 * Created by PhpStorm.
 * User: pbb
 * Date: 12/09/18
 * Time: 11:01 AM
 */

namespace ProvulusSDK\Enum\Contact\Mailgroups;

use MabeEnum\Enum;
use ProvulusSDK\Client\Request\Resource\PrimalValued;
use ProvulusSDK\Client\Request\Resource\PrimalValuedEnumTrait;

/**
 * Class MailGroupCategoriesEnum
 *
 * @method static MailGroupCategoriesEnum TECHNICAL()
 * @method static MailGroupCategoriesEnum SALES()
 * @method static MailGroupCategoriesEnum BILLING()
 *
 * @package ProvulusSDK\Enum\Contact\Mailgroups
 */
class MailGroupCategoriesEnum extends Enum implements PrimalValued
{
    use PrimalValuedEnumTrait;

    const TECHNICAL = 'technical';
    const SALES     = 'sales';
    const BILLING   = 'billing';
}