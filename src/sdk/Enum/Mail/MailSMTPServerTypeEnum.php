<?php
/**
 * Created by PhpStorm.
 * User: pbb
 * Date: 20/12/16
 * Time: 3:30 PM
 */

namespace ProvulusSDK\Enum\Mail;


use MabeEnum\Enum;
use ProvulusSDK\Client\Request\Resource\PrimalValued;
use ProvulusSDK\Client\Request\Resource\PrimalValuedEnumTrait;

/**
 * Class SMTPServerTypeEnum
 *
 * Server type (google, exim, pix, etc ...)
 *
 * @package Cumulus\Business\Mail\SMTP\Server
 *
 * @method static MailSMTPServerTypeEnum EXIM()
 * @method static MailSMTPServerTypeEnum GOOGLE()
 * @method static MailSMTPServerTypeEnum PIX()
 * @method static MailSMTPServerTypeEnum BASIC()
 * @method static MailSMTPServerTypeEnum OFFICE()
 * @method static MailSMTPServerTypeEnum EXCHANGE()
 */
class MailSMTPServerTypeEnum extends Enum implements PrimalValued
{
    use PrimalValuedEnumTrait;
    const BASIC    = 'basic';
    const GOOGLE   = 'google';
    const PIX      = 'pix';
    const EXIM     = 'exim';
    const OFFICE   = 'office';
    const EXCHANGE = 'exchange';
}