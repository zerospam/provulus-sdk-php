<?php
/**
 * Created by PhpStorm.
 * User: pbb
 * Date: 17/10/17
 * Time: 1:29 PM
 */

namespace ProvulusSDK\Enum\PersonOfInterest;

use MabeEnum\Enum;
use ProvulusSDK\Client\Request\Resource\PrimalValued;
use ProvulusSDK\Client\Request\Resource\PrimalValuedEnumTrait;

/**
 * Class PersonOfInterestMatchingRuleEnum
 *
 * @method static PersonOfInterestMatchRuleEnum STRICT()
 * @method static PersonOfInterestMatchRuleEnum LOOSE()
 *
 * @package Cumulus\Models\PersonOfInterest
 */
class PersonOfInterestMatchRuleEnum extends Enum implements PrimalValued
{
    use PrimalValuedEnumTrait;

    const STRICT = 'strict';
    const LOOSE  = 'loose';
}
