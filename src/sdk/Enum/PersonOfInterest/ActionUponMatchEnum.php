<?php
/**
 * Created by PhpStorm.
 * User: pbb
 * Date: 17/10/17
 * Time: 3:40 PM
 */

namespace ProvulusSDK\Enum\PersonOfInterest;

use MabeEnum\Enum;
use ProvulusSDK\Client\Request\Resource\PrimalValued;
use ProvulusSDK\Client\Request\Resource\PrimalValuedEnumTrait;

/**
 * Class ActionUponMatchEnum
 *
 * Enum for action to take upon matching Person Of Interest information
 * with mail FROM header
 *
 * @method static ActionUponMatchEnum BLOCK()
 * @method static ActionUponMatchEnum ADD_HEADER()
 *
 * @package ProvulusSDK\Client\Response\PersonOfInterest
 */
class ActionUponMatchEnum extends Enum implements PrimalValued
{
    use PrimalValuedEnumTrait;

    const BLOCK      = 'block';
    const ADD_HEADER = 'add_header';
}