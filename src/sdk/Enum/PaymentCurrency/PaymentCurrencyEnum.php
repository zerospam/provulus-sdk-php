<?php

namespace ProvulusSDK\Enum\PaymentCurrency;

use MabeEnum\Enum;
use ProvulusSDK\Client\Request\Resource\PrimalValued;
use ProvulusSDK\Client\Request\Resource\PrimalValuedEnumTrait;

/**
 * Class PaymentCurrencyEnum
 *
 * Different currency used
 *
 * @method static PaymentCurrencyEnum CAD();
 * @method static PaymentCurrencyEnum USD()
 * @method static PaymentCurrencyEnum EUR()
 * @method static PaymentCurrencyEnum GBP()
 * @package ProvulusSDK\Enum\PaymentCurrency
 */
class PaymentCurrencyEnum extends Enum implements PrimalValued
{
    use PrimalValuedEnumTrait;

    const CAD = 'cad';
    const USD = 'usd';
    const EUR = 'eur';
    const GBP = 'gbp';
}