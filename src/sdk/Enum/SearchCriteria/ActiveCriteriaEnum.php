<?php
/**
 * Created by PhpStorm.
 * User: pbb
 * Date: 13/02/17
 * Time: 10:09 AM
 */

namespace ProvulusSDK\Enum\SearchCriteria;

use MabeEnum\Enum;
use ProvulusSDK\Client\Request\Resource\PrimalValued;
use ProvulusSDK\Client\Request\Resource\PrimalValuedEnumTrait;

/**
 * Class ActiveCriteriaEnum
 *
 * Enum for active criteria different cases
 *
 * @method static ActiveCriteriaEnum ALL()
 * @method static ActiveCriteriaEnum ACTIVE()
 * @method static ActiveCriteriaEnum INACTIVE()
 *
 * @package Cumulus\Repositories\Criteria\Organization
 */
class ActiveCriteriaEnum extends Enum implements PrimalValued
{
    use PrimalValuedEnumTrait;

    const ALL      = 'all';
    const ACTIVE   = 'active';
    const INACTIVE = 'inactive';
}