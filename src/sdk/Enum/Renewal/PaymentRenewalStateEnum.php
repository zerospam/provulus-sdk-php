<?php
/**
 * Created by PhpStorm.
 * User: pbb
 * Date: 21/02/17
 * Time: 3:09 PM
 */

namespace ProvulusSDK\Enum\Renewal;


use MabeEnum\Enum;
use ProvulusSDK\Client\Request\Resource\PrimalValued;
use ProvulusSDK\Client\Request\Resource\PrimalValuedEnumTrait;


/**
 * Class PaymentRenewalStateEnum
 *
 * Renewal state of the organization
 *
 * @method static PaymentRenewalStateEnum OK()
 * @method static PaymentRenewalStateEnum RENEWING()
 * @method static PaymentRenewalStateEnum ALERTED()
 * @method static PaymentRenewalStateEnum GRACEPERIOD()
 * @method static PaymentRenewalStateEnum BLOCKED()
 * @method static PaymentRenewalStateEnum PENDING()
 * @method static PaymentRenewalStateEnum MANUAL()
 * @method static PaymentRenewalStateEnum ALL_RENEWAL()
 * @method static PaymentRenewalStateEnum NO_DOMAIN()
 *
 * @package ProvulusSDK\Enum\Renewal
 */
class PaymentRenewalStateEnum extends Enum implements PrimalValued
{
    use PrimalValuedEnumTrait;

    const OK          = 'ok';
    const RENEWING    = 'renewing';
    const ALERTED     = 'alerted';
    const GRACEPERIOD = 'graceperiod';
    const BLOCKED     = 'blocked';
    const PENDING     = 'pending';
    const MANUAL      = 'manual';
    const ALL_RENEWAL = 'all_renewal';
    const NO_DOMAIN   = 'no_domain';
}