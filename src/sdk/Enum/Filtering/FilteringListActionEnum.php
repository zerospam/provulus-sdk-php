<?php
/**
 * Created by PhpStorm.
 * User: ycoutu
 * Date: 29/08/17
 * Time: 4:42 PM
 */

namespace ProvulusSDK\Enum\Filtering;

use MabeEnum\Enum;
use ProvulusSDK\Client\Request\Resource\PrimalValued;
use ProvulusSDK\Client\Request\Resource\PrimalValuedEnumTrait;

/**
 * Class FilteringListActionEnum
 *
 * @method static FilteringListActionEnum ALLOW()
 * @method static FilteringListActionEnum BLOCK()
 * @method static FilteringListActionEnum RESTRICT()
 *
 * @package ProvulusSDK\Enum\Filtering
 */
class FilteringListActionEnum extends Enum implements PrimalValued
{
    use PrimalValuedEnumTrait;

    const ALLOW = "allow";
    const BLOCK = "block";
    const RESTRICT = "restrict";
}
