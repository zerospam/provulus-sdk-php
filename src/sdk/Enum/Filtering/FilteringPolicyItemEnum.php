<?php
/**
 * Created by PhpStorm.
 * User: pbb
 * Date: 22/12/16
 * Time: 3:31 PM
 */

namespace ProvulusSDK\Enum\Filtering;


use MabeEnum\Enum;
use ProvulusSDK\Client\Request\Resource\PrimalValued;
use ProvulusSDK\Client\Request\Resource\PrimalValuedEnumTrait;

/**
 * Class FilteringPolicyItemEnum
 *
 * Define and map FilteringPolicyItem types
 *
 * @method static FilteringPolicyItemEnum DOMAIN()
 * @method static FilteringPolicyItemEnum ADDRESS()
 *
 * @package ProvulusSDK\Enum\Filtering
 */
class FilteringPolicyItemEnum extends Enum implements PrimalValued
{
    use PrimalValuedEnumTrait;

    const ADDRESS = 'address';
    const DOMAIN  = 'domain';
}