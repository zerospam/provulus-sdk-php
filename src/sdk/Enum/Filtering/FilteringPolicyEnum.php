<?php
/**
 * Created by PhpStorm.
 * User: pbb
 * Date: 21/12/16
 * Time: 2:34 PM
 */

namespace ProvulusSDK\Enum\Filtering;


use MabeEnum\Enum;
use ProvulusSDK\Client\Request\Resource\PrimalValued;
use ProvulusSDK\Client\Request\Resource\PrimalValuedEnumTrait;

/**
 * Class DomainFilteringTypeEnum
 *
 * @method static FilteringPolicyEnum INBOUND()
 * @method static FilteringPolicyEnum OUTBOUND()
 *
 * @package Cumulus\Models\Domain\Filtering
 */
class FilteringPolicyEnum extends Enum implements PrimalValued
{
    use PrimalValuedEnumTrait;

    const INBOUND  = 'inbound';
    const OUTBOUND = 'outbound';
}