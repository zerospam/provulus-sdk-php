<?php
/**
 * Created by PhpStorm.
 * User: ycoutu
 * Date: 11/01/18
 * Time: 3:31 PM
 */

namespace ProvulusSDK\Enum\Filtering;

use MabeEnum\Enum;
use ProvulusSDK\Client\Request\Resource\PrimalValued;
use ProvulusSDK\Client\Request\Resource\PrimalValuedEnumTrait;

/**
 * Class GreymailSensitivityEnum
 *
 * @method static GreymailSensitivityEnum STRICT()
 * @method static GreymailSensitivityEnum NORMAL()
 * @method static GreymailSensitivityEnum PERMISSIVE()
 *
 * @package ProvulusSDK\Enum\Filtering
 */
class GreymailSensitivityEnum extends Enum implements PrimalValued
{
    use PrimalValuedEnumTrait;

    const STRICT     = 'strict';
    const NORMAL     = 'normal';
    const PERMISSIVE = 'permissive';
}
