<?php
/**
 * Created by PhpStorm.
 * User: ycoutu
 * Date: 29/12/17
 * Time: 9:14 AM
 */

namespace ProvulusSDK\Exception\API;

/**
 * Class ForbiddenException
 *
 * The access is forbidden
 *
 * @package ProvulusSDK\Exception\API
 */
class ForbiddenException extends ProvulusException
{

}
