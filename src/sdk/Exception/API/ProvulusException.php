<?php
/**
 * Created by PhpStorm.
 * User: aaflalo
 * Date: 29/11/16
 * Time: 3:21 PM
 */

namespace ProvulusSDK\Exception\API;

/**
 * Class ProvulusException
 *
 * Basic Runtime exception of the SDK
 *
 * @package ProvulusSDK\Exception
 */
abstract class ProvulusException extends \RuntimeException
{


}