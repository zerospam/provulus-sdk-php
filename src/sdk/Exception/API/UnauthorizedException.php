<?php
/**
 * Created by PhpStorm.
 * User: aaflalo
 * Date: 29/11/16
 * Time: 3:23 PM
 */

namespace ProvulusSDK\Exception\API;


/**
 * Class UnauthorizedException
 *
 * You don't have access to this route
 *
 * @package ProvulusSDK\Exception
 */
class UnauthorizedException extends ProvulusException
{

}