<?php
/**
 * Created by PhpStorm.
 * User: aaflalo
 * Date: 30/11/16
 * Time: 3:12 PM
 */

namespace ProvulusSDK\Exception\API;

use GuzzleHttp\Exception\RequestException;
use GuzzleHttp\Psr7\Uri;
use ProvulusSDK\Client\Request\IProvulusRequest;
use Psr\Http\Message\UriInterface;


/**
 * Class NotFoundException
 *
 * Couldn't find the wanted object
 *
 * @package ProvulusSDK\Exception\API
 */
class NotFoundException extends ProvulusException
{

    /**
     * @var IProvulusRequest
     */
    private $request;


    public function __construct(IProvulusRequest $request, UriInterface $baseUri, RequestException $previous)
    {
        $this->request = $request;

        $message = sprintf('[%s] %s not found.',
            $request->httpType()->getValue(),
            Uri::resolve($baseUri, $request->toUri()));

        parent::__construct($message, 404, $previous);
    }

    /**
     * The request that couldn't be resolved
     *
     * @return IProvulusRequest
     */
    public function getRequest()
    {
        return $this->request;
    }


}