<?php
/**
 * Created by PhpStorm.
 * User: aaflalo
 * Date: 20/01/17
 * Time: 3:55 PM
 */

namespace ProvulusSDK\Exception\API;


use Cake\Collection\Collection;
use ProvulusSDK\Client\Errors\Precondition\PreconditionError;

class PreconditionException extends ProvulusException
{

    /**
     * @var Collection|PreconditionError[]
     */
    private $errors;

    /**
     * PreconditionException constructor.
     *
     * @param array $errors
     */
    public function __construct(array $errors)
    {
        $errorsArray = [];
        foreach ($errors as $error) {
            $errorsArray[] = PreconditionError::fromArray($error);
        }
        $this->errors = new Collection($errorsArray);
    }

    /**
     * Getter for errors
     *
     * @return Collection|PreconditionError[]
     */
    public function getErrors()
    {
        return $this->errors;
    }


}