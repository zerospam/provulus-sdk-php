<?php
/**
 * Created by PhpStorm.
 * User: aaflalo
 * Date: 15/12/16
 * Time: 11:43 AM
 */

namespace ProvulusSDK\Exception\API;


use ProvulusSDK\Client\RateLimit\IRateLimitData;

class HitRateLimitException extends ProvulusException
{

    /**
     * @var IRateLimitData
     */
    private $rateLimit;

    /**
     * HitRateLimitException constructor.
     *
     * @param IRateLimitData $rateLimit
     */
    public function __construct(IRateLimitData $rateLimit) { $this->rateLimit = $rateLimit; }

    /**
     * Getter for rateLimit
     *
     * @return IRateLimitData
     */
    public function getRateLimit()
    {
        return $this->rateLimit;
    }


}