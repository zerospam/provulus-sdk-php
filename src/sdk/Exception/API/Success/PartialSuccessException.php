<?php
/**
 * Created by PhpStorm.
 * User: aaflalo
 * Date: 17-08-25
 * Time: 09:29
 */

namespace ProvulusSDK\Exception\API\Success;

use ProvulusSDK\Client\Errors\Precondition\PreconditionError;
use ProvulusSDK\Exception\API\PreconditionException;

/**
 * Class PartialSuccessException
 *
 * When the request succeeded but only partially
 *
 * @package ProvulusSDK\Exception\API\Success
 */
class PartialSuccessException extends PreconditionException
{


    public function __construct(array $errors)
    {
        parent::__construct($errors);

        $messages = $this->getErrors()->map(
            function (PreconditionError $error) {
                return $error->getMessage();
            }
        )->toArray();

        $this->message = implode(PHP_EOL, $messages);
    }
}
