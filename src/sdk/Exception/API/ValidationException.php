<?php
/**
 * Created by PhpStorm.
 * User: aaflalo
 * Date: 29/11/16
 * Time: 3:20 PM
 */

namespace ProvulusSDK\Exception\API;

use ProvulusSDK\Client\Errors\Validation\ValidationFieldError;

/**
 * Class ValidationException
 *
 * Some of the data sent aren't following the validation rules
 *
 * @package ProvulusSDK\Exception
 */
class ValidationException extends ProvulusException
{

    /**
     * @var ValidationFieldError[]
     */
    private $validationData = [];

    /**
     * ValidationException constructor.
     *
     * @param array $validationData
     */
    public function __construct(array $validationData)
    {
        foreach ($validationData as $field => $errors) {
            $this->validationData[$field] = ValidationFieldError::fromArray($field, $errors);
        }
    }

    /**
     *Validation errors with key as field code and value as ValidationFieldError.
     *
     * @return  ValidationFieldError[]
     */
    public function getValidationData()
    {
        return $this->validationData;
    }


}