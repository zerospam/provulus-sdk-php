<?php
/**
 * Created by PhpStorm.
 * User: aaflalo
 * Date: 08/02/17
 * Time: 9:27 AM
 */
namespace ProvulusSDK\Config;

use GuzzleHttp\ClientInterface;
use ProvulusSDK\Config\JWT\IProvulusJWTTokenStorage;
use ProvulusSDK\Enum\Locale\LocaleEnum;
use Psr\Http\Message\UriInterface;


/**
 * Class ProvulusConfiguration
 *
 * Configuration for the client
 *
 * @package ProvulusSDK\Config
 */
interface IProvulusConfiguration
{
    /**
     * Return the base url for this configuration
     *
     * @return UriInterface
     */
    public function baseUrl();

    /**
     * Check if the configuration is valid
     *
     * @return bool
     */
    public function isValid();

    /**
     * Getter for apiKey
     *
     * @return string
     */
    public function getApiKey();

    /**
     * @param string $apiKey
     *
     * @return $this
     */
    public function setApiKey($apiKey);

    /**
     * Getter for jwtStorage
     *
     * @return IProvulusJWTTokenStorage
     */
    public function getJwtStorage();

    /**
     * @param IProvulusJWTTokenStorage $jwtStorage
     *
     * @return $this
     */
    public function setJwtStorage($jwtStorage);

    /**
     * ID of the organization that will be impersonated
     *
     * @return int
     */
    public function getOrganizationId();

    /**
     * Set the ID of the organization if you wish to impersonate this organization
     *
     * Useful for reseller to act as one of their client
     *
     * @param int $organizationId
     *
     * @return $this
     */
    public function setOrganizationId($organizationId);

    /**
     * Used as shared key with the server
     *
     * @return string
     */
    public function getEncryptionKey();

    /**
     * Set the shared key, if base64 encoded need to start with base64:
     *
     * @param string $encryptionKey
     *
     * @return $this
     */
    public function setEncryptionKey($encryptionKey);

    /**
     * Build the client for this configuration
     *
     * @return ClientInterface
     */
    public function buildClient();

    /**
     * Connection timeout before dropping the connection
     *
     * @return int
     */
    public function getConnectionTimeout();

    /**
     * Number of retries before giving up on the connection
     *
     * @return int
     */
    public function getRetries();

    /**
     * Connection timeout before dropping the connection
     *
     * @param int $connectionTimeout
     *
     * @return $this
     */
    public function setConnectionTimeout($connectionTimeout);

    /**
     * Number of retries before giving up on the connection
     *
     * @param int $retries
     *
     * @return $this
     */
    public function setRetries($retries);

    /**
     * @return int
     */
    public function getRequestTimeout();

    /**
     *  Request Timeout before dropping the connection
     *
     * @param int $requestTimeout
     *
     * @return $this
     */
    public function setRequestTimeout($requestTimeout);

    /**
     * @return null|LocaleEnum
     */
    public function getLocale();

    /**
     * @param null|LocaleEnum $locale
     *
     * @return $this
     */
    public function setLocale($locale);
}