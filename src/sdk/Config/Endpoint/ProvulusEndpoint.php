<?php
/**
 * Created by PhpStorm.
 * User: aaflalo
 * Date: 29/11/16
 * Time: 1:34 PM
 */

namespace ProvulusSDK\Config\Endpoint;


use MabeEnum\Enum;
use Psr\Http\Message\UriInterface;

/**
 * Class ProvulusEndpoint
 * @method static ProvulusEndpoint PROD()
 * @method static ProvulusEndpoint STAGING()
 * @method static ProvulusEndpoint LOCAL()
 *
 * @method static ProvulusEndpoint TESTING()
 * @package ProvulusSDK\Config\Endpoint
 */
class ProvulusEndpoint extends Enum
{

    const PROD    = 'https://api.cumulussmtp.com';
    const STAGING = 'https://staging.cumulussmtp.com';
    const LOCAL   = 'https://cumulus.local';

    const TESTING = 'http://127.0.0.1';

    /**
     * Endpoint to URL object
     *
     * @return UriInterface
     */
    public function toUrl()
    {
        return \GuzzleHttp\Psr7\uri_for($this->getValue());
    }
}