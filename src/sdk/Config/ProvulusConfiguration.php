<?php
/**
 * Created by PhpStorm.
 * User: aaflalo
 * Date: 29/11/16
 * Time: 1:26 PM
 */

namespace ProvulusSDK\Config;


use GuzzleHttp\Client;
use GuzzleHttp\ClientInterface;
use ProvulusSDK\Config\Endpoint\ProvulusEndpoint;
use ProvulusSDK\Config\JWT\IProvulusJWTTokenStorage;
use ProvulusSDK\Config\Version\ProvulusApiVersion;
use ProvulusSDK\Enum\Locale\LocaleEnum;
use ProvulusSDK\Utils\Str;
use Psr\Http\Message\UriInterface;

/**
 * Class ProvulusConfiguration
 *
 * Configuration for the client
 *
 * @package ProvulusSDK\Config
 */
class ProvulusConfiguration implements IProvulusConfiguration
{

    /**
     * @var ProvulusEndpoint
     */
    private $endpoint;
    /**
     * @var ProvulusApiVersion
     */
    private $version;

    /**
     * @var string
     */
    private $apiKey;

    /**
     * @var IProvulusJWTTokenStorage
     */
    private $jwtStorage;

    /**
     * @var int
     */
    private $organizationId;

    /**
     * @var string
     */
    private $encryptionKey;

    /**
     * Connection timeout in seconds
     *
     * @var int
     */
    private $connectionTimeout = 5;
    /**
     * @var int
     */
    private $retries = 3;

    /**
     * Request timeout in second.
     * The max time the request can take to process
     *
     * @var int
     */
    private $requestTimeout = 15;

    /**
     * The locale to use for the request
     *
     * @var LocaleEnum|null
     */
    private $locale;

    /**
     * ProvulusConfiguration constructor.
     *
     * @param ProvulusEndpoint   $endpoint
     * @param ProvulusApiVersion $version
     */
    public function __construct(ProvulusEndpoint $endpoint, ProvulusApiVersion $version)
    {
        $this->endpoint = $endpoint;
        $this->version  = $version;
    }

    /**
     * Return the base url for this configuration
     *
     * @return UriInterface
     */
    public function baseUrl()
    {
        return $this->endpoint->toUrl()->withPath('/api/' . $this->version->getValue() . '/');
    }

    /**
     * Check if the configuration is valid
     *
     * @return bool
     */
    public function isValid()
    {
        if (is_null($this->apiKey) && is_null($this->jwtStorage)) {
            return false;
        }

        return true;
    }

    /**
     * Getter for apiKey
     *
     * @return string
     */
    public function getApiKey()
    {
        return $this->apiKey;
    }

    /**
     * @param string $apiKey
     *
     * @return $this
     */
    public function setApiKey($apiKey)
    {
        $this->apiKey = $apiKey;

        return $this;
    }

    /**
     * Getter for jwtStorage
     *
     * @return IProvulusJWTTokenStorage
     */
    public function getJwtStorage()
    {
        return $this->jwtStorage;
    }

    /**
     * @param IProvulusJWTTokenStorage $jwtStorage
     *
     * @return $this
     */
    public function setJwtStorage($jwtStorage)
    {
        $this->jwtStorage = $jwtStorage;

        return $this;
    }

    /**
     * ID of the organization that will be impersonated
     *
     * @return int
     */
    public function getOrganizationId()
    {
        return $this->organizationId;
    }

    /**
     * Set the ID of the organization if you wish to impersonate this organization
     *
     * Useful for reseller to act as one of their client
     *
     * @param int $organizationId
     *
     * @return $this
     */
    public function setOrganizationId($organizationId)
    {
        $this->organizationId = $organizationId;

        return $this;
    }

    /**
     * Used as shared key with the server
     *
     * @return string
     */
    public function getEncryptionKey()
    {
        $key = $this->encryptionKey;
        if (Str::startsWith($key, 'base64:')) {
            $key = base64_decode(substr($key, 7));
        }

        return $key;
    }

    /**
     * Set the shared key, if base64 encoded need to start with base64:
     *
     * @param string $encryptionKey
     *
     * @return $this
     */
    public function setEncryptionKey($encryptionKey)
    {
        $this->encryptionKey = $encryptionKey;

        return $this;
    }

    /**
     * Build the client for this configuration
     *
     * @return ClientInterface
     */
    public function buildClient()
    {
        $options = ['base_uri' => $this->baseUrl()];
        if ($this->endpoint->is(ProvulusEndpoint::LOCAL())) {
            $options['verify'] = false;
        }

        return new Client($options);
    }

    /**
     * Timeout before dropping the connection
     *
     * @return int
     */
    public function getConnectionTimeout()
    {
        return $this->connectionTimeout;
    }

    /**
     * Number of retries before giving up on the connection
     *
     * @return int
     */
    public function getRetries()
    {
        return $this->retries;
    }

    /**
     * Connection timeout before dropping the connection
     *
     * @param int $connectionTimeout
     *
     * @return $this
     */
    public function setConnectionTimeout($connectionTimeout)
    {
        $this->connectionTimeout = $connectionTimeout;

        return $this;
    }

    /**
     * Number of retries before giving up on the connection
     *
     * @param int $retries
     *
     * @return $this
     */
    public function setRetries($retries)
    {
        $this->retries = $retries;

        return $this;
    }

    /**
     * @return int
     */
    public function getRequestTimeout()
    {
        return $this->requestTimeout;
    }

    /**
     *  Request Timeout before dropping the connection
     *
     * @param int $requestTimeout
     *
     * @return $this
     */
    public function setRequestTimeout($requestTimeout)
    {
        $this->requestTimeout = $requestTimeout;

        return $this;
    }

    /**
     * @return null|LocaleEnum
     */
    public function getLocale()
    {
        return $this->locale;
    }

    /**
     * @param null|LocaleEnum $locale
     *
     * @return $this
     */
    public function setLocale($locale)
    {
        $this->locale = $locale;

        return $this;
    }




}