<?php
/**
 * Created by PhpStorm.
 * User: aaflalo
 * Date: 29/11/16
 * Time: 1:30 PM
 */

namespace ProvulusSDK\Config\Version;


use MabeEnum\Enum;

/**
 * Class ProvulusApiVersion
 * @method static ProvulusApiVersion V1()
 *
 * @package ProvulusSDK\Config\Version
 */
class ProvulusApiVersion extends Enum
{
    const V1 = 'v1';
}