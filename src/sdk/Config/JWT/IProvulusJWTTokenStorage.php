<?php
/**
 * Created by PhpStorm.
 * User: aaflalo
 * Date: 29/11/16
 * Time: 2:22 PM
 */

namespace ProvulusSDK\Config\JWT;

/**
 * Interface IProvulusJWTTokenStorage
 *
 * Represent a storage used for the JWT Token
 *
 * @package ProvulusSDK\Config\JWT
 */
interface IProvulusJWTTokenStorage
{

    /**
     * Get the current token
     *
     * @return string
     */
    public function getToken();

    /**
     * Update the token
     *
     * @param string $token
     */
    public function updateToken($token);
}