<?php
/**
 * Created by PhpStorm.
 * User: ycoutu
 * Date: 21/07/17
 * Time: 9:09 AM
 */

namespace ProvulusSDK\Client\Response\Parameter\HomeMessage;

use Cake\Collection\Collection;
use ProvulusSDK\Client\Response\Collection\CollectionPaginatedResponse;

/**
 * Class HomeMessageCollectionResponse
 *
 * @method Collection|HomeMessageResponse[] data()
 *
 * @package ProvulusSDK\Client\Response\Parameter\HomeMessage
 */
class HomeMessageCollectionResponse extends CollectionPaginatedResponse
{

}
