<?php
/**
 * Created by PhpStorm.
 * User: ycoutu
 * Date: 20/07/17
 * Time: 4:53 PM
 */

namespace ProvulusSDK\Client\Response\Parameter\HomeMessage;

use Carbon\Carbon;
use ProvulusSDK\Client\Response\ProvulusBaseResponse;
use ProvulusSDK\Enum\Locale\LocaleEnum;
use ProvulusSDK\Utils\Str;

/**
 * Class HomeMessageResponse
 *
 * @property-read LocaleEnum  $language_code
 * @property-read string|null $message
 * @property-read string|null $image
 * @property-read Carbon      $created_at
 * @property-read Carbon      $updated_at
 *
 * @package ProvulusSDK\Client\Response\Parameter\HomeMessage
 */
class HomeMessageResponse extends ProvulusBaseResponse
{
    public  $dates = [
        'created_at',
        'updated_at'
    ];

    public function getLanguageCodeAttribute()
    {
        $languageCode = $this->data['attributes']['language_code'];

        return LocaleEnum::byName(Str::upper($languageCode));
    }
}
