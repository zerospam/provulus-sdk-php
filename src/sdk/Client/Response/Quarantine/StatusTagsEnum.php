<?php
/**
 * Created by PhpStorm.
 * User: pbb
 * Date: 14/03/17
 * Time: 10:42 AM
 */

namespace ProvulusSDK\Client\Response\Quarantine;


use MabeEnum\Enum;
use ProvulusSDK\Client\Request\Resource\PrimalValued;
use ProvulusSDK\Client\Request\Resource\PrimalValuedEnumTrait;

/**
 * Class StatusTagsEnum
 *
 * @method static StatusTagsEnum PHISHING()
 * @method static StatusTagsEnum BANNED()
 * @method static StatusTagsEnum SPAM()
 * @method static StatusTagsEnum GREYMAIL()
 * @method static StatusTagsEnum DMARC_FAIL()
 *
 * @package ProvulusSDK\Client\Response\Quarantine
 */
class StatusTagsEnum extends Enum implements PrimalValued
{

    use PrimalValuedEnumTrait;

    const PHISHING = 'phishing';
    const BANNED = 'banned';
    const SPAM = 'spam';
    const GREYMAIL = 'greymail';
    const DMARC_FAIL = 'dmarc_fail';

    /**
     * Representing letter of the tag
     *
     * @return string
     */
    public function representingLetter()
    {
        return substr($this->getName(), 0, 1);
    }
}