<?php
/**
 * Created by PhpStorm.
 * User: pbb
 * Date: 15/02/17
 * Time: 4:27 PM
 */

namespace ProvulusSDK\Client\Response\Quarantine;

use Cake\Collection\Collection;
use Carbon\Carbon;
use ProvulusSDK\Client\Response\ProvulusBaseResponse;
use ProvulusSDK\Utils\Quarantine\EmailAddress;
use ProvulusSDK\Utils\Str;

/**
 * Class AggregatedMessageResponse
 *
 * @property-read int                         $msg_id
 * @property-read int                         $msg_rcpt_id
 * @property-read int|null                    $usr_id
 * @property-read int                         $organization_id
 * @property-read string                      $rcpt_domain
 * @property-read string                      $rcpt_localpart
 * @property-read Carbon                      $received_at
 * @property-read string|null                 $subject
 * @property-read string|null                 $subject_normalized
 * @property-read string|null                 $content_preview
 * @property-read float|null                  $sa_score
 * @property-read bool                        $is_banned
 * @property-read bool                        $is_phish
 * @property-read bool                        $is_dmarc_fail
 * @property-read bool                        $is_released
 * @property-read int|null                    $released_by_usr_id
 * @property-read Carbon|null                 $released_at
 * @property-read bool                        $is_deleted
 * @property-read int|null                    $deleted_by_usr_id
 * @property-read Carbon|null                 $deleted_at
 * @property-read bool                        $is_declared_spam
 * @property-read int|null                    $declared_spam_by_usr_id
 * @property-read Carbon|null                 $declared_spam_at
 * @property-read bool                        $is_greymail
 * @property-read EmailAddress                $SMTPSender
 * @property-read Collection|EmailAddress[]   $ContentSenders
 * @property-read EmailAddress                $SMTPRecipient
 * @property-read Collection|EmailAddress[]   $ContentRecipients
 * @property-read Collection|StatusTagsEnum[] $Tags
 *
 *
 * @package ProvulusSDK\Client\Response\Quarantine
 */
class AggregatedMessageResponse extends ProvulusBaseResponse
{
    /**
     * @var string[]
     */
    public $dates
        = [
            'received_at',
            'released_at',
            'deleted_at',
            'declared_spam_at',
        ];


    /**
     * @return EmailAddress
     */
    public function getSMTPSenderAttribute()
    {
        return EmailAddress::fromArray($this->data['attributes']['SMTPSender']);
    }

    /**
     * @return EmailAddress
     */
    public function getSMTPRecipientAttribute()
    {
        return EmailAddress::fromArray($this->data['attributes']['SMTPRecipient']);
    }

    /**
     * @return Collection|EmailAddress[]
     */
    public function getContentSendersAttribute()
    {
        if (empty($this->data['attributes']['ContentSenders'])) {
            return new Collection([]);
        }

        $addressAndNames = array_map(function (array $address) {
            return EmailAddress::fromArray($address);
        }, $this->data['attributes']['ContentSenders']);

        return new Collection($addressAndNames);

    }

    /**
     * @return Collection|EmailAddress[]
     */
    public function getContentRecipientsAttribute()
    {
        if (empty($this->data['attributes']['ContentRecipients'])) {
            return new Collection([]);
        }

        $addressAndNames = array_map(function (array $address) {
            return EmailAddress::fromArray($address);
        }, $this->data['attributes']['ContentRecipients']);


        return new Collection($addressAndNames);
    }

    /**
     * @return Collection|StatusTagsEnum[]
     */
    public function getTagsAttribute()
    {
        if (empty($this->data['attributes']['Tags'])) {
            return new Collection([]);
        }

        $tags = array_map(function ($tag) {
            return StatusTagsEnum::byName(Str::upper($tag));
        }, $this->data['attributes']['Tags']);

        return new Collection($tags);
    }


}