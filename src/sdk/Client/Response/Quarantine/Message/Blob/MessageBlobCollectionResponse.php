<?php
/**
 * Created by PhpStorm.
 * User: ycoutu
 * Date: 17/11/17
 * Time: 10:11 AM
 */

namespace ProvulusSDK\Client\Response\Quarantine\Message\Blob;

use Cake\Collection\Collection;
use ProvulusSDK\Client\Response\Collection\CollectionResponse;

/**
 * Class MessageBlobCollectionResponse
 *
 * @method Collection|MessageBlobResponse[] data()
 *
 * @package ProvulusSDK\Client\Response\Quarantine\Message\Blob
 */
class MessageBlobCollectionResponse extends CollectionResponse
{

}
