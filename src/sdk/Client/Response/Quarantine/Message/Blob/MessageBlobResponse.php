<?php
/**
 * Created by PhpStorm.
 * User: pbb
 * Date: 10/05/17
 * Time: 1:59 PM
 */

namespace ProvulusSDK\Client\Response\Quarantine\Message\Blob;

use ProvulusSDK\Client\Response\ProvulusBaseResponse;

/**
 * Class MessageBlobResponse
 *
 * @property-read int    $msg_id
 * @property-read string $msg_data
 * @property-read string $data_type
 *
 * @package ProvulusSDK\Client\Response\Quarantine\Message\Blob
 */
class MessageBlobResponse extends ProvulusBaseResponse
{

    public function getMsgDataAttribute()
    {
        if (is_null($this->data['attributes']['msg_data'])) {
            return null;
        }

        return base64_decode($this->data['attributes']['msg_data']);
    }
}
