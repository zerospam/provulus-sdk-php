<?php
/**
 * Created by PhpStorm.
 * User: pbb
 * Date: 11/04/17
 * Time: 1:38 PM
 */

namespace ProvulusSDK\Client\Response\Quarantine\Message;

use Cake\Collection\Collection;
use Carbon\Carbon;
use ProvulusSDK\Client\Response\ProvulusBaseResponse;
use ProvulusSDK\Client\Response\Quarantine\StatusTagsEnum;
use ProvulusSDK\Utils\Quarantine\EmailActor;
use ProvulusSDK\Utils\Quarantine\EmailAttachment;
use ProvulusSDK\Utils\Quarantine\EmailBody;
use ProvulusSDK\Utils\Quarantine\QuarantineMessageStatus;
use ProvulusSDK\Utils\Quarantine\RelatedEmail;
use ProvulusSDK\Utils\Str;

/**
 * Class MessageResponse
 *
 * @property-read int                          $organization_id
 * @property-read Carbon                       $received_at
 * @property-read float                        $score
 * @property-read EmailActor                   $from
 * @property-read EmailActor                   $to
 * @property-read string|null                  $subject
 * @property-read EmailBody                    $body
 * @property-read string                       $headers
 * @property-read array|null                   $themis_metadata
 * @property-read Collection|EmailAttachment[] $attachments
 * @property-read QuarantineMessageStatus      $status
 * @property-read Collection|RelatedEmail[]    $related
 * @property-read Collection|StatusTagsEnum[]  $tags
 * @property-read string[]                     $content_categories
 *
 * @package ProvulusSDK\Client\Response\Quarantine\Message
 */
class QuarantineMessageResponse extends ProvulusBaseResponse
{

    public $dates
        = [
            'received_at',
        ];

    /**
     * @return EmailActor
     */
    public function getFromAttribute()
    {
        return EmailActor::fromArray($this->data['attributes']['from']);
    }

    /**
     * @return EmailActor
     */
    public function getToAttribute()
    {
        return EmailActor::fromArray($this->data['attributes']['to']);
    }

    /**
     * @return EmailBody
     */
    public function getBodyAttribute()
    {
        return EmailBody::fromArray($this->data['attributes']['body']);
    }

    /**
     * Decode 64
     *
     * @return bool|null|string
     */
    public function getHeadersAttribute()
    {
        if (is_null($this->data['attributes']['headers'])) {
            return null;
        }

        return mb_convert_encoding(base64_decode($this->data['attributes']['headers']), 'ASCII');
    }


    /**
     * Decode 64
     *
     * @return bool|null|string
     */
    public function getThemisMetadataAttribute()
    {
        if (is_null($this->data['attributes']['themis_metadata'])) {
            return null;
        }

        /** @noinspection PhpComposerExtensionStubsInspection */
        return json_decode(base64_decode($this->data['attributes']['themis_metadata']));
    }

    /**
     * @return QuarantineMessageStatus
     */
    public function getStatusAttribute()
    {
        return QuarantineMessageStatus::fromArray($this->data['attributes']['status']);
    }

    /**
     * @return Collection|EmailAttachment[]
     */
    public function getAttachmentsAttribute()
    {
        $array = array_map(function ($item) {
            return EmailAttachment::fromArray($item);
        }, $this->data['attributes']['attachments']);

        return new Collection($array);
    }

    /**
     * @return Collection|RelatedEmail[]
     */
    public function getRelatedAttribute()
    {
        $related = $this->data['attributes']['related'];

        $array = array_map(function ($item) {
            return RelatedEmail::fromArray($item);
        }, $related);

        return new Collection($array);
    }

    /**
     * @return Collection|StatusTagsEnum[]
     */
    public function getTagsAttribute()
    {
        if (empty($this->data['attributes']['tags'])) {
            return new Collection([]);
        }

        $tags = array_map(function ($tag) {
            return StatusTagsEnum::byName(Str::upper($tag));
        }, $this->data['attributes']['tags']);

        return new Collection($tags);
    }
}
