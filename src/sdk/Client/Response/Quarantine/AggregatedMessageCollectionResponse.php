<?php
/**
 * Created by PhpStorm.
 * User: pbb
 * Date: 16/02/17
 * Time: 8:24 AM
 */

namespace ProvulusSDK\Client\Response\Quarantine;

use Cake\Collection\Collection;
use ProvulusSDK\Client\Response\Collection\CollectionPaginatedResponse;

/**
 * Class QuarantineCollectionResponse
 *
 * @method Collection|AggregatedMessageResponse[] data()
 *
 * @package ProvulusSDK\Client\Response\Quarantine
 */
class AggregatedMessageCollectionResponse extends CollectionPaginatedResponse
{

}