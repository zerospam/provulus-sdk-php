<?php
/**
 * Created by PhpStorm.
 * User: aaflalo
 * Date: 06/12/16
 * Time: 11:44 AM
 */

namespace ProvulusSDK\Client\Response;


interface IProvulusObjectResponse extends IProvulusResponse
{

    /**
     * Id in the response
     *
     * @return int
     */
    public function id();


    /**
     * Get a specific field
     *
     * @param string $field
     *
     * @return mixed|null
     */
    public function get($field);


}