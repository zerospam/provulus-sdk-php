<?php
/**
 * Created by PhpStorm.
 * User: ycoutu
 * Date: 29/08/18
 * Time: 2:36 PM
 */

namespace ProvulusSDK\Client\Response\Ldap\Configuration\Connection;

use Cake\Collection\Collection;
use ProvulusSDK\Client\Response\Ldap\Synchronization\LdapSynchronizationResponse;
use ProvulusSDK\Client\Response\ProvulusBaseResponse;
use ProvulusSDK\Client\Response\Traits\IncludedResponseTrait;

/**
 * Class LdapConnectionResponse
 *
 * @property-read int                                      $ldap_config_id
 * @property-read string                                   $hostname
 * @property-read boolean                                  $is_ssl_enabled
 * @property-read int                                      $port
 * @property-read string                                   $user_dn
 * @property-read string                                   $search_dn
 * @property-read string                                   $email_binding
 * @property-read string|null                              $alias_binding
 * @property-read string|null                              $first_name_binding
 * @property-read string|null                              $last_name_binding
 * @property-read string|null                              $language_binding
 * @property-read string|null                              $sso_binding
 * @property-read int|null                                 $last_sync_id
 * @property-read int|null                                 $last_sync_changed_id
 * @property-read string|null                              $filter
 * @property-read int                                      $pagination
 * @property-read LdapSynchronizationResponse[]|Collection $ldap_syncs
 *
 * @package ProvulusSDK\Client\Response\Ldap\Configuration\Connection
 */
class LdapConnectionResponse extends ProvulusBaseResponse
{
    use IncludedResponseTrait;

    /**
     * Returns the ldap connections
     *
     * @return LdapSynchronizationResponse[]|Collection
     */
    public function getLdapSyncsAttribute()
    {
        $callback = function (array $data) {
            return new LdapSynchronizationResponse($data);
        };

        return $this->findRelatedItems('ldapSyncs', $callback);
    }
}
