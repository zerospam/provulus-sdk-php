<?php
/**
 * Created by PhpStorm.
 * User: ycoutu
 * Date: 29/08/18
 * Time: 2:48 PM
 */

namespace ProvulusSDK\Client\Response\Ldap\Configuration\Connection;

use Cake\Collection\Collection;
use ProvulusSDK\Client\Response\Collection\CollectionPaginatedResponse;

/**
 * Class LdapConnectionCollectionResponse
 *
 * @method Collection|LdapConnectionResponse[] data()
 * @package ProvulusSDK\Client\Response\Ldap\Configuration\Connection
 */
class LdapConnectionCollectionResponse extends CollectionPaginatedResponse
{

}
