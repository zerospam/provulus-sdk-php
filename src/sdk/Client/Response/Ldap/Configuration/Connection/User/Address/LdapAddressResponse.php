<?php
/**
 * Created by PhpStorm.
 * User: ycoutu
 * Date: 14/06/17
 * Time: 1:31 PM
 */

namespace ProvulusSDK\Client\Response\Ldap\Configuration\Connection\User\Address;

use ProvulusSDK\Client\Response\ProvulusBaseResponse;
use ProvulusSDK\Enum\Ldap\LdapAddressTypeEnum;
use ProvulusSDK\Utils\Str;

/**
 * Class LdapAddressResponse
 *
 * Response to a LdapAddress
 *
 * @property-read int                 $ldap_user_id
 * @property-read LdapAddressTypeEnum $address_type_code
 * @property-read string              $address
 * @package ProvulusSDK\Client\Response\LDAP
 */
class LdapAddressResponse extends ProvulusBaseResponse
{
    /**
     * AddressTypeCode to Enum
     *
     *
     * @return LdapAddressTypeEnum
     */
    public function getAddressTypeCodeAttribute()
    {
        return LdapAddressTypeEnum::byName(Str::upper($this->data['attributes']['address_type_code']));
    }
}
