<?php
/**
 * Created by PhpStorm.
 * User: ycoutu
 * Date: 14/06/17
 * Time: 11:29 AM
 */

namespace ProvulusSDK\Client\Response\Ldap\Configuration\Connection\User;

use Cake\Collection\Collection;
use ProvulusSDK\Client\Response\Ldap\Configuration\Connection\User\Address\LdapAddressResponse;
use ProvulusSDK\Client\Response\ProvulusBaseResponse;
use ProvulusSDK\Client\Response\Traits\IncludedResponseTrait;
use ProvulusSDK\Enum\Locale\LocaleEnum;
use ProvulusSDK\Utils\Str;

/**
 * Class LdapUserResponse
 *
 * Response to a ReadLdapUserRequest, or element of an LdapUserCollectionResponse
 *
 * @property-read int                              $ldap_connection_id
 * @property-read string                           $dn_path
 * @property-read string|null                      $first_name
 * @property-read string|null                      $last_name
 * @property-read string|null                      $sso_id
 * @property-read LocaleEnum|null                  $language
 * @property-read bool                             $is_declared_as_user
 * @property-read LdapAddressResponse[]|Collection $ldap_addresses
 *
 * @package ProvulusSDK\Client\Response\LDAP
 */
class LdapUserResponse extends ProvulusBaseResponse
{
    use IncludedResponseTrait;

    /**
     * Returns the LdapUser's addresses
     *
     * @return LdapAddressResponse[]|Collection
     */
    public function getLdapAddressesAttribute()
    {
        $callback = function (array $data) {
            return new LdapAddressResponse($data);
        };

        return $this->findRelatedItems('ldapAddresses', $callback);
    }


    /**
     * @return LocaleEnum|null
     */
    public function getLanguageAttribute()
    {
        $language = $this->getValue('language');
        if (is_null($language)) {
            return null;
        }
        return LocaleEnum::byName(Str::upper($language));
    }
}
