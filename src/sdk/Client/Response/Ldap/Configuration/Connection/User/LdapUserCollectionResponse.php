<?php
/**
 * Created by PhpStorm.
 * User: ycoutu
 * Date: 14/06/17
 * Time: 11:43 AM
 */

namespace ProvulusSDK\Client\Response\Ldap\Configuration\Connection\User;

use Cake\Collection\Collection;
use ProvulusSDK\Client\Response\Collection\CollectionPaginatedResponse;

/**
 * Class LdapUserCollectionResponse
 *
 * Response to a IndexLdapUserRequest
 *
 * @method Collection|LdapUserResponse[] data()
 * @package ProvulusSDK\Client\Response\LDAP
 */
class LdapUserCollectionResponse extends CollectionPaginatedResponse
{

}
