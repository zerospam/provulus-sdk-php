<?php
/**
 * Created by PhpStorm.
 * User: pbb
 * Date: 02/06/17
 * Time: 4:34 PM
 */

namespace ProvulusSDK\Client\Response\Ldap\Configuration;


use Cake\Collection\Collection;
use Carbon\Carbon;
use ProvulusSDK\Client\Response\Ldap\Configuration\Connection\LdapConnectionResponse;
use ProvulusSDK\Client\Response\ProvulusBaseResponse;
use ProvulusSDK\Client\Response\Traits\IncludedResponseTrait;
use ProvulusSDK\Enum\Ldap\LdapConfigurationStatusEnum;
use ProvulusSDK\Utils\Ldap\Synchronization;
use ProvulusSDK\Utils\Ldap\UserCreation;

/**
 * Class LdapResponse
 *
 * @property-read string|null                              $report_email
 * @property-read Carbon                                   $created_at
 * @property-read Carbon|null                              $updated_at
 * @property-read UserCreation                             $userCreation
 * @property-read Synchronization                          $synchronization
 * @property-read LdapConfigurationStatusEnum              $synchronization_status
 * @property-read Carbon|null                              $last_sync_at
 * @property-read LdapConnectionResponse[]|Collection      $ldap_connections
 *
 * @package ProvulusSDK\Client\Response\LDAP
 */
class LdapConfigurationResponse extends ProvulusBaseResponse
{
    use IncludedResponseTrait;

    public $dates
        = [
            'created_at',
            'updated_at',
            'created_users_at',
            'last_sync_at',
        ];

    /**
     * @return Synchronization
     */
    public function getSynchronizationAttribute()
    {
        $synchronization = $this->data['attributes']['synchronization'];

        return Synchronization::fromArray($synchronization);
    }

    /**
     * @return LdapConfigurationStatusEnum
     */
    public function getSynchronizationStatusAttribute()
    {
        return LdapConfigurationStatusEnum::byValue($this->data['attributes']['synchronization_status']);
    }

    /**
     * @return UserCreation
     */
    public function getUserCreationAttribute()
    {
        $userCreation = $this->data['attributes']['user_creation'];

        return UserCreation::fromArray($userCreation);
    }

    /**
     * Returns the ldap connections
     *
     * @return LdapConnectionResponse[]|Collection
     */
    public function getLdapConnectionsAttribute()
    {
        $callback = function (array $data) {
            return new LdapConnectionResponse($data);
        };

        return $this->findRelatedItems('ldapConnections', $callback);
    }
}