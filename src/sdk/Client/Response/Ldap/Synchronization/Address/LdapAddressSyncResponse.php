<?php
/**
 * Created by PhpStorm.
 * User: pbb
 * Date: 13/06/17
 * Time: 1:23 PM
 */

namespace ProvulusSDK\Client\Response\Ldap\Synchronization\Address;


use ProvulusSDK\Client\Response\ProvulusBaseResponse;
use ProvulusSDK\Enum\Ldap\LdapAddressTypeEnum;
use ProvulusSDK\Enum\Ldap\LdapDifferentialEnum;

/**
 * Class LdapAddressSyncResponse
 *
 * @property-read int                  $ldap_user_sync_id
 * @property-read LdapAddressTypeEnum  $address_type_code
 * @property-read string               $address
 * @property-read LdapDifferentialEnum $diff_code
 *
 * @package ProvulusSDK\Client\Response\LDAP
 */
class LdapAddressSyncResponse extends ProvulusBaseResponse
{
    /**
     * @return LdapDifferentialEnum
     */
    public function getDiffCodeAttribute()
    {
        return LdapDifferentialEnum::byValue($this->getValue('diff_code'));
    }

    /**
     * @return LdapAddressTypeEnum
     */
    public function getAddressTypeCodeAttribute()
    {
        return LdapAddressTypeEnum::byValue($this->getValue('address_type_code'));
    }
}