<?php
/**
 * Created by PhpStorm.
 * User: pbb
 * Date: 13/06/17
 * Time: 2:10 PM
 */

namespace ProvulusSDK\Client\Response\Ldap\Synchronization\Address;

use Cake\Collection\Collection;
use ProvulusSDK\Client\Response\Collection\CollectionPaginatedResponse;

/**
 * Class LdapAddressSyncCollectionResponse
 *
 * @method Collection|LdapAddressSyncResponse[] data()
 *
 * @package ProvulusSDK\Client\Response\LDAP\Synchronization\Address
 */
class LdapAddressSyncCollectionResponse extends CollectionPaginatedResponse
{

}