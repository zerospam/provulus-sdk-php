<?php
/**
 * Created by PhpStorm.
 * User: pbb
 * Date: 13/06/17
 * Time: 1:18 PM
 */

namespace ProvulusSDK\Client\Response\Ldap\Synchronization\User;

use Cake\Collection\Collection;
use ProvulusSDK\Client\Response\Ldap\Synchronization\Address\LdapAddressSyncResponse;
use ProvulusSDK\Client\Response\ProvulusBaseResponse;
use ProvulusSDK\Client\Response\Traits\IncludedResponseTrait;
use ProvulusSDK\Enum\Ldap\LdapDifferentialEnum;
use ProvulusSDK\Enum\Locale\LocaleEnum;
use ProvulusSDK\Utils\Str;

/**
 * Class LdapUserSyncResponse
 *
 * @property-read int                                  $ldap_connection_id
 * @property-read string                               $dn_path
 * @property-read LdapDifferentialEnum                 $diff_code
 * @property-read string                               $main_address
 * @property-read string|null                          $first_name
 * @property-read string|null                          $last_name
 * @property-read string|null                          $sso_id
 * @property-read LocaleEnum|null                      $language
 * @property-read bool                                 $is_declared_as_user
 * @property-read Collection|LdapAddressSyncResponse[] $ldap_addresses_sync
 *
 * @package ProvulusSDK\Client\Response\LDAP
 */
class LdapUserSyncResponse extends ProvulusBaseResponse
{
    use IncludedResponseTrait;

    /**
     * @return LdapDifferentialEnum
     */
    public function getDiffCodeAttribute()
    {
        return LdapDifferentialEnum::byValue($this->getValue('diff_code'));
    }

    /**
     * @return LocaleEnum|null
     */
    public function getLanguageAttribute()
    {
        $language = $this->getValue('language');
        if (is_null($language)) {
            return null;
        }
        return LocaleEnum::byName(Str::upper($language));
    }

    /**
     * Get Ldap Addresses syncs related to ldap user sync
     *
     * @return Collection|LdapAddressSyncResponse[]
     */
    public function getLdapAddressesSyncAttribute()
    {
        $callback = function (array $data) {
            return new LdapAddressSyncResponse($data);
        };

        $type = 'ldapAddressesSync';

        $result =  $this->findRelatedItems($type, $callback);

        if (is_null($result)) {
            return new Collection([]);
        }

        return $result;
    }

    public function getAddedAddressesAttribute()
    {

        return $this->ldap_addresses_sync->filter(
            function (LdapAddressSyncResponse $ldapAddressSync) {
                return $ldapAddressSync->diff_code->is(LdapDifferentialEnum::ADDED());
            }
        );
    }

    public function getDeletedAddressesAttribute()
    {
        return $this->ldap_addresses_sync->filter(
            function (LdapAddressSyncResponse $ldapAddressSync) {
                return $ldapAddressSync->diff_code->is(LdapDifferentialEnum::DELETED());
            }
        );
    }
}
