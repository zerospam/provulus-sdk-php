<?php
/**
 * Created by PhpStorm.
 * User: pbb
 * Date: 13/06/17
 * Time: 2:14 PM
 */

namespace ProvulusSDK\Client\Response\Ldap\Synchronization\User;

use Cake\Collection\Collection;
use ProvulusSDK\Client\Response\Collection\CollectionPaginatedResponse;

/**
 * Class LdapUserSyncCollectionResponse
 *
 * @method Collection|LdapUserSyncResponse[] data()
 *
 * @package ProvulusSDK\Client\Response\LDAP\Synchronization\User
 */
class LdapUserSyncCollectionResponse extends CollectionPaginatedResponse
{

}