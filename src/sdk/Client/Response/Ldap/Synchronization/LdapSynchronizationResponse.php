<?php
/**
 * Created by PhpStorm.
 * User: pbb
 * Date: 13/06/17
 * Time: 11:56 AM
 */

namespace ProvulusSDK\Client\Response\Ldap\Synchronization;


use Carbon\Carbon;
use ProvulusSDK\Client\Response\ProvulusBaseResponse;
use ProvulusSDK\Enum\Ldap\LdapSyncStatusEnum;

/**
 * Class LdapSynchronizationResponse
 *
 * @property-read int                $nb_new_entries
 * @property-read int                $nb_modified_entries
 * @property-read int                $nb_deleted_entries
 * @property-read int                $nb_entries_total
 * @property-read LdapSyncStatusEnum $sync_status
 * @property-read Carbon             $sync_at
 * @property-read int                $ldap_connection_id
 *
 * @package ProvulusSDK\Client\Response\LDAP
 */
class LdapSynchronizationResponse extends ProvulusBaseResponse
{

    protected $dates
        = [
            'sync_at',
        ];

    /**
     * @return LdapSyncStatusEnum
     */
    public function getSyncStatusAttribute()
    {
        $status = $this->data['attributes']['sync_status'];

        return LdapSyncStatusEnum::byValue($status);
    }
}