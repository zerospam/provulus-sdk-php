<?php
/**
 * Created by PhpStorm.
 * User: pbb
 * Date: 22/06/17
 * Time: 3:33 PM
 */

namespace ProvulusSDK\Client\Response\Ldap\Synchronization;


use Cake\Collection\Collection;
use ProvulusSDK\Client\Response\Collection\CollectionPaginatedResponse;

/**
 * Class LdapSynchronizationCollectionResponse
 *
 * @method Collection|LdapSynchronizationResponse[] data()
 * @package ProvulusSDK\Client\Response\LDAP\Synchronization
 */
class LdapSynchronizationCollectionResponse extends CollectionPaginatedResponse
{
}