<?php
/**
 * Created by PhpStorm.
 * User: aaflalo
 * Date: 06/12/16
 * Time: 11:46 AM
 */

namespace ProvulusSDK\Client\Response\Auth;


use ProvulusSDK\Client\Response\IProvulusResponse;
use ProvulusSDK\Client\Response\Traits\ResponseTrait;

class AuthResponse implements IProvulusResponse
{

    use ResponseTrait;

    /**
     * @var array
     */
    private $data;

    /**
     * AuthResponse constructor.
     *
     * @param array $data
     */
    public function __construct(array $data) { $this->data = $data; }


    /**
     * JWT Token
     *
     * @return string
     */
    public function token()
    {
        return $this->data['token'];
    }


    /**
     * Data contained in the response
     *
     * @return array
     */
    public function data()
    {
        return $this->data;
    }

    /**
     * Type of the response
     *
     * @return string
     */
    public function type()
    {
        return $this->data['type'];
    }
}