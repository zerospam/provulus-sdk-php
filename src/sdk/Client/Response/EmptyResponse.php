<?php
/**
 * Created by PhpStorm.
 * User: pbb
 * Date: 16/12/16
 * Time: 2:57 PM
 */

namespace ProvulusSDK\Client\Response;


final class EmptyResponse extends ProvulusBaseResponse
{

    /**
     * EmptyResponse constructor.
     */
    public function __construct()
    {
        parent::__construct(array());
    }
}