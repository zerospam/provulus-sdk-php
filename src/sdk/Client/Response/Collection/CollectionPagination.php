<?php
/**
 * Created by PhpStorm.
 * User: aaflalo
 * Date: 16/12/16
 * Time: 3:30 PM
 */

namespace ProvulusSDK\Client\Response\Collection;

use ProvulusSDK\Utils\Str;


/**
 * Class CollectionPagination
 *
 * @property-read int $total
 * @property-read int $count
 * @property-read int $perPage
 * @property-read int $currentPage
 * @property-read int $totalPages
 * @package ProvulusSDK\Client\Response\Collection
 */
class CollectionPagination
{

    private $pagination = array();

    /**
     * CollectionPagination constructor.
     *
     * @param array $pagination
     */
    public function __construct(array $pagination)
    {
        $this->pagination = $pagination;
    }


    function __isset($name)
    {
        $name = Str::snake($name);

        return isset($this->pagination[$name]);
    }


    function __get($name)
    {
        if (isset($this->{$name})) {
            $name = Str::snake($name);

            return $this->pagination[$name];
        }

        return null;
    }

    public static function noPagination()
    {
        return new CollectionPagination(
            [
                'total' => 0,
                'count' => 0,
                'per_page' => 0,
                'current_page' => 1,
                'total_pages' => 1,
            ]
        );
    }
}