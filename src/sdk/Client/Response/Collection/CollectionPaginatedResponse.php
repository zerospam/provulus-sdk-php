<?php
/**
 * Created by PhpStorm.
 * User: aaflalo
 * Date: 23/01/17
 * Time: 2:43 PM
 */

namespace ProvulusSDK\Client\Response\Collection;


use Cake\Collection\Collection;

abstract class CollectionPaginatedResponse extends CollectionResponse
{
    /** @var CollectionPagination */
    private $metaData;

    /**
     * CollectionResponse constructor.
     *
     * @param CollectionPagination $metaData
     * @param Collection           $collection
     */
    public function __construct(CollectionPagination $metaData, Collection $collection)
    {
        $this->metaData = $metaData;
        parent::__construct($collection);
    }


    /**
     * Getter for metaData
     *
     * @return CollectionPagination
     */
    public function getMetaData()
    {
        return $this->metaData;
    }


}