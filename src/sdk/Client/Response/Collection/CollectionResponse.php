<?php
/**
 * Created by PhpStorm.
 * User: aaflalo
 * Date: 17-08-16
 * Time: 13:48
 */

namespace ProvulusSDK\Client\Response\Collection;


use Cake\Collection\Collection;
use ProvulusSDK\Client\Response\IProvulusObjectResponse;
use ProvulusSDK\Client\Response\IProvulusResponse;
use ProvulusSDK\Client\Response\Traits\ResponseTrait;
use ProvulusSDK\Utils\Str;

abstract class CollectionResponse implements IProvulusResponse
{
    use ResponseTrait;
    /**
     * @var Collection
     */
    private $collection;

    /**
     * @var object[]
     */
    private $objReplacementCache = [];

    /**
     * CollectionResponse constructor.
     *
     * @param Collection           $collection
     */
    public function __construct(Collection $collection)
    {
        $this->collection = $collection;
        $this->setRequestResponse();
    }


    /**
     * Data contained in the response
     *
     * @return Collection|IProvulusObjectResponse[]
     */
    public function data()
    {
        return $this->collection;
    }


    /**
     * Type of the response
     *
     * @return string
     */
    public function type()
    {
        $first = $this->data()->first();
        if (!$first) {
            return '';
        }

        return $first->type();
    }

    /**
     * Check for computed field in the response
     *
     * @param mixed $name
     *
     * @return bool
     */
    function __isset($name)
    {
        $key = 'get' . Str::studly($name) . 'Attribute';

        return isset($this->data['attributes'][$name])
               || isset($this->data[$name])
               || method_exists($this, $key);
    }

    /**
     * Getter for computed field in the response
     *
     * @param string $name
     *
     * @return mixed
     */
    function __get($name)
    {
        $key = 'get' . Str::studly($name) . 'Attribute';

        //check if attribute transformer exists
        //Run it if exists and cache the result
        if (method_exists($this, $key)) {
            if (isset($this->objReplacementCache[$name])) {
                return $this->objReplacementCache[$name];
            }

            return $this->objReplacementCache[$name] = call_user_func([
                $this,
                $key,
            ]);
        }

        return null;
    }
}