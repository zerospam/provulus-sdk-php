<?php
/**
 * Created by PhpStorm.
 * User: fdenomme
 * Date: 14/04/20
 * Time: 15:40 AM
 */

namespace ProvulusSDK\Client\Response\Logs;

use ProvulusSDK\Client\Response\IProvulusResponse;
use ProvulusSDK\Client\Response\Traits\ResponseTrait;

class LogsResponse implements IProvulusResponse
{
    use ResponseTrait;

    /**
     * @var array
     */
    private $data;

    /**
     * LogsResponse constructor.
     *
     * @param array $data
     */
    public function __construct(array $data)
    {
        $this->data = $data;
    }

    /**
     * Data contained in the response
     *
     * @return array
     */
    public function data()
    {
        return $this->data;
    }

    /**
     * Type of the response
     *
     * @return string
     */
    public function type()
    {
        return $this->data['type'];
    }
}
