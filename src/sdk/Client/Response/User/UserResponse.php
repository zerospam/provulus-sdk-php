<?php
/**
 * Created by PhpStorm.
 * User: aaflalo
 * Date: 06/12/16
 * Time: 11:50 AM
 */

namespace ProvulusSDK\Client\Response\User;


use Cake\Collection\Collection;
use Carbon\Carbon;
use ProvulusSDK\Client\Response\ProvulusBaseResponse;
use ProvulusSDK\Client\Response\Traits\IncludedResponseTrait;
use ProvulusSDK\Client\Response\User\Session\UserSessionResponse;
use ProvulusSDK\Client\Response\User\UserEmailAddress\UserEmailAddressResponse;
use ProvulusSDK\Enum\Locale\LocaleEnum;
use ProvulusSDK\Enum\User\UserTypeEnum;
use ProvulusSDK\Utils\Str;

/**
 * Class UserResponse
 *
 * Response to UserRequests
 *
 *
 * @property-read int                      $organization_id
 * @property-read bool                     $is_access_allowed
 * @property-read UserTypeEnum             $usr_type_code
 * @property-read string                   $email_address
 * @property-read bool                     $is_admin_stats_push
 * @property-read bool                     $is_adminquar_only
 * @property-read int                      $login_failures
 * @property-read string|null              $display_name
 * @property-read string|null              $display_name_normalized
 * @property-read LocaleEnum               $language_code
 * @property-read \DateTimeZone            $timezone_code
 * @property-read int                      $quar_search_page_size
 * @property-read Carbon|null              $last_daily_digest_date
 * @property-read bool                     $is_active
 * @property-read int                      $version
 * @property-read bool|null                $is_active_backup
 * @property-read string|null              $name
 * @property-read string|null              $username
 * @property-read string|null              $surname
 * @property-read Carbon|null              $password_changed_at
 * @property-read string|null              $sso_id
 * @property-read bool                     $is_ldap
 * @property-read string|null              $phone_number
 * @property-read string|null              $daily_digest_email
 * @property-read bool                     $is_locked_down
 * @property-read UserSessionResponse|null $last_session
 * @property-read bool                     $should_configure_two_factor_auth
 * @property-read bool                     $is_two_factor_auth_enabled
 * @property-read Carbon|null              $last_login_date
 *
 * @property-read UserEmailAddressResponse[]|Collection $addresses
 *
 * @package ProvulusSDK\Client\Response\User
 */
class UserResponse extends ProvulusBaseResponse
{
    use IncludedResponseTrait;

    /**
     * @var string[]
     */
    public $dates
        = [
            'last_daily_digest_date',
            'password_changed_at',
            'last_login_date',
        ];

    /**
     * @return \DateTimeZone
     */
    public function getTimezoneCodeAttribute()
    {
        $timezone = $this->data['attributes']['timezone_code']['timezone'];

        return new \DateTimeZone($timezone);
    }

    /**
     * @return LocaleEnum
     */
    public function getLanguageCodeAttribute()
    {
        $language = $this->data['attributes']['language_code'];

        return LocaleEnum::byName(Str::upper($language));
    }

    /**
     * @return UserTypeEnum
     */
    public function getUsrTypeCodeAttribute()
    {
        $typeCode = $this->data['attributes']['usr_type_code'];

        return UserTypeEnum::get($typeCode);
    }

    /**
     * get the support Organization in the response
     *
     * @return UserEmailAddressResponse[]|Collection
     */
    public function getAddressesAttribute()
    {
        $callback = function (array $data) {
            return new UserEmailAddressResponse($data);
        };

        $type = 'addresses';

        return $this->findRelatedItems($type, $callback);
    }


    /**
     * get the support Organization in the response
     *
     * @return null|UserSessionResponse
     */
    public function getLastSessionAttribute()
    {
        $callback = function (array $data) {
            return new UserSessionResponse($data);
        };

        $type = 'lastSession';

        return $this->findRelatedItem($type, $callback);
    }
}