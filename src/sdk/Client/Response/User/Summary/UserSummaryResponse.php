<?php
/**
 * Created by PhpStorm.
 * User: ycoutu
 * Date: 26/10/18
 * Time: 2:59 PM
 */

namespace ProvulusSDK\Client\Response\User\Summary;

use ProvulusSDK\Client\Response\Organization\Summary\OrganizationSummaryResponse;
use ProvulusSDK\Client\Response\ProvulusBaseResponse;
use ProvulusSDK\Client\Response\Traits\IncludedResponseTrait;
use ProvulusSDK\Enum\Locale\LocaleEnum;
use ProvulusSDK\Enum\User\UserTypeEnum;
use ProvulusSDK\Utils\Str;

/**
 * Class UserConnectionInfoResponse
 *
 * @property-read string                      username
 * @property-read string                      display_name
 * @property-read string                      email_address
 * @property-read int                         quar_search_page_size
 * @property-read bool                        is_two_factor_auth_enabled
 * @property-read bool                        should_configure_two_factor_auth
 * @property-read bool                        is_anonymous
 * @property-read \DateTimeZone               timezone
 * @property-read LocaleEnum                  language
 * @property-read UserTypeEnum                type
 * @property-read OrganizationSummaryResponse organization_summary
 * @package ProvulusSDK\Client\Response\User\Connection
 */
class UserSummaryResponse extends ProvulusBaseResponse
{
    use IncludedResponseTrait;

    /**
     * @return \DateTimeZone
     */
    public function getTimezoneAttribute()
    {
        $timezone = $this->data['attributes']['timezone']['timezone'];

        return new \DateTimeZone($timezone);
    }

    /**
     * @return LocaleEnum
     */
    public function getLanguageAttribute()
    {
        $language = $this->data['attributes']['language'];

        return LocaleEnum::byName(Str::upper($language));
    }

    /**
     * @return UserTypeEnum
     */
    public function getTypeAttribute()
    {
        $type = $this->data['attributes']['type'];

        return UserTypeEnum::get($type);
    }

    /**
     * @return OrganizationSummaryResponse
     */
    public function getOrganizationSummaryAttribute()
    {
        $callback = function (array $data) {
            return new OrganizationSummaryResponse($data);
        };

        return $this->findRelatedItem('organizationSummary', $callback);
    }
}
