<?php
/**
 * Created by PhpStorm.
 * User: pbb
 * Date: 26/01/17
 * Time: 2:37 PM
 */

namespace ProvulusSDK\Client\Response\User;

use Cake\Collection\Collection;
use ProvulusSDK\Client\Response\Collection\CollectionPaginatedResponse;

/**
 * Class UserCollectionResponse
 *
 * A collection of UserResponse
 *
 * @method Collection|UserResponse[] data()
 * @package ProvulusSDK\Client\Response\User
 */
class UserCollectionResponse extends CollectionPaginatedResponse
{

}