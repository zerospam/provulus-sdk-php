<?php
/**
 * Created by PhpStorm.
 * User: pbb
 * Date: 21/08/17
 * Time: 2:02 PM
 */

namespace ProvulusSDK\Client\Response\User\Session;


use Carbon\Carbon;
use ProvulusSDK\Client\Response\ProvulusBaseResponse;
use ProvulusSDK\Client\Response\Traits\IncludedResponseTrait;

/**
 * Class UserSessionResponse
 *
 * @property-read int         $usr_id
 * @property-read bool        $is_expired
 * @property-read string|null $php_web_session_id
 * @property-read Carbon      $started_at
 * @property-read Carbon      $last_accessed_at
 * @property-read int         $mode_code
 * @property-read string|null $ip_address
 * @property-read string|null $browser
 * @property-read Carbon      $created_at
 * @property-read Carbon|null $updated_at
 * @property-read string|null $portal_prefix
 *
 * @package ProvulusSDK\Client\Response\User\Session
 */
class UserSessionResponse extends ProvulusBaseResponse
{
    use IncludedResponseTrait;

    public $dates
        = [
            'started_at',
            'last_accessed_at',
            'created_at',
            'updated_at',
        ];

}