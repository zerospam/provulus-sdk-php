<?php
/**
 * Created by PhpStorm.
 * User: pbb
 * Date: 28/02/17
 * Time: 3:42 PM
 */

namespace ProvulusSDK\Client\Response\User\UserEmailAddress;

use Carbon\Carbon;
use ProvulusSDK\Client\Response\ProvulusBaseResponse;

/**
 * Class UserEmailAddressResponse
 *
 * @property-read int         usr_id
 * @property-read int         domain_id
 * @property-read string      local_part
 * @property-read Carbon      created_at
 * @property-read Carbon|null updated_at
 * @property-read string      domain_name
 * @property-read string      email_address
 *
 * @package ProvulusSDK\Client\Response\User\UserEmailAddress
 */
class UserEmailAddressResponse extends ProvulusBaseResponse
{

    /**
     * @var string[]
     */
    public $dates
        = [
            'created_at',
            'updated_at',
        ];
}