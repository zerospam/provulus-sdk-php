<?php
/**
 * Created by PhpStorm.
 * User: pbb
 * Date: 28/02/17
 * Time: 3:48 PM
 */

namespace ProvulusSDK\Client\Response\User\UserEmailAddress;

use Cake\Collection\Collection;
use ProvulusSDK\Client\Response\Collection\CollectionPaginatedResponse;

/**
 * Class UserEmailAddressCollectionResponse
 *
 * @method Collection|UserEmailAddressResponse[] data()
 *
 * @package ProvulusSDK\Client\Response\User\UserEmailAddress
 */
class UserEmailAddressCollectionResponse extends CollectionPaginatedResponse
{

}