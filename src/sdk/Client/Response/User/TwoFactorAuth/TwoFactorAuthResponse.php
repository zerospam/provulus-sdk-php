<?php
/**
 * Created by PhpStorm.
 * User: ycoutu
 * Date: 28/12/17
 * Time: 11:28 AM
 */

namespace ProvulusSDK\Client\Response\User\TwoFactorAuth;

use Carbon\Carbon;
use ProvulusSDK\Client\Response\ProvulusBaseResponse;

/**
 * Class TwoFactorAuthResponse
 *
 * @property-read bool   $is_confirmed
 * @property-read int    $usr_id
 * @property-read string $key
 * @property-read string $qr
 * @property-read Carbon $created_at
 * @property-read Carbon $updated_at
 *
 * @package ProvulusSDK\Client\Response\User\TwoFactorAuth
 */
class TwoFactorAuthResponse extends ProvulusBaseResponse
{
    /** @var string[] */
    public $dates
        = [
            'updated_at',
            'created_at',
        ];
}
