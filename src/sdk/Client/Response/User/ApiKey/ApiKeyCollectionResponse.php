<?php
/**
 * Created by PhpStorm.
 * User: pbb
 * Date: 30/01/17
 * Time: 2:52 PM
 */

namespace ProvulusSDK\Client\Response\User\ApiKey;

use Cake\Collection\Collection;
use ProvulusSDK\Client\Response\Collection\CollectionPaginatedResponse;

/**
 * Class ApiKeyCollectionResponse
 *
 * Response for collection of ApiKeys
 * @method Collection|ApiKeyResponse[] data()
 *
 * @package ProvulusSDK\Client\Response\User\ApiKey
 */
class ApiKeyCollectionResponse extends CollectionPaginatedResponse
{

}