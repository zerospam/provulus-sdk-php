<?php
/**
 * Created by PhpStorm.
 * User: pbb
 * Date: 30/01/17
 * Time: 2:30 PM
 */

namespace ProvulusSDK\Client\Response\User\ApiKey;

use ProvulusSDK\Client\Response\ProvulusBaseResponse;

/**
 * Class ApiKeyResponse
 *
 * Response for ApiKey Requests
 *
 * @property-read string      api_key
 * @property-read string      comment
 * @property-read string      created_at
 * @property-read string|null updated_at
 * @property-read string[]    permissions
 * @property-read string      ip_access
 *
 * @package ProvulusSDK\Client\Response\User\ApiKey
 */
class ApiKeyResponse extends ProvulusBaseResponse
{

}