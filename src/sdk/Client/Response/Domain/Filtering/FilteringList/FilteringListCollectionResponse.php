<?php
/**
 * Created by PhpStorm.
 * User: ycoutu
 * Date: 29/08/17
 * Time: 5:00 PM
 */

namespace ProvulusSDK\Client\Response\Domain\Filtering\FilteringList;

use Cake\Collection\Collection;
use ProvulusSDK\Client\Response\Collection\CollectionResponse;

/**
 * Class FilteringListCollectionResponse
 *
 * @method Collection|FilteringListResponse[] data()
 *
 * @package ProvulusSDK\Client\Response\Domain\Filtering\FilteringList
 */
class FilteringListCollectionResponse extends CollectionResponse
{

}
