<?php
/**
 * Created by PhpStorm.
 * User: ycoutu
 * Date: 16/10/17
 * Time: 10:18 AM
 */

namespace ProvulusSDK\Client\Response\Domain\Filtering\FilteringList;

use function array_map;
use Cake\Collection\Collection;
use ProvulusSDK\Client\Request\Resource\Domain\Filtering\FilteringList\FilteringListRequestTypeEnum;
use ProvulusSDK\Client\Response\ProvulusBaseResponse;
use ProvulusSDK\Enum\Filtering\FilteringListActionEnum;

/**
 * Class AllFilteringListsResponse
 *
 * @package ProvulusSDK\Client\Response\Domain\Filtering\FilteringList
 */
class AllFilteringListsResponse extends ProvulusBaseResponse
{
    /**
     * Get a collection of filtering list items according to the filtering list request type and action
     *
     * @param FilteringListRequestTypeEnum $requestTypeEnum
     * @param FilteringListActionEnum      $listTypeEnum
     *
     * @return Collection
     */
    public function getFilteringList(FilteringListRequestTypeEnum $requestTypeEnum, FilteringListActionEnum $listTypeEnum)
    {
        $jsonData = $this->data['data']['attributes'][$requestTypeEnum->getValue()][$listTypeEnum->getValue()];

        return new Collection(
            array_map(
                function ($json) {
                    return new FilteringListResponse($json);
                },
                $jsonData['data']
            )
        );
    }
}
