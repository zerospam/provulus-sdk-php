<?php
/**
 * Created by PhpStorm.
 * User: ycoutu
 * Date: 29/08/17
 * Time: 4:51 PM
 */

namespace ProvulusSDK\Client\Response\Domain\Filtering\FilteringList;

use ProvulusSDK\Client\Response\ProvulusBaseResponse;
use ProvulusSDK\Enum\Filtering\FilteringListActionEnum;
use ProvulusSDK\Utils\Str;

/**
 * Class FilteringListResponse
 *
 * @property-read int                     domain_group_id
 * @property-read int                     organization_id
 * @property-read bool                    is_enabled
 * @property-read string                  value
 * @property-read string|null             description
 * @property-read string                  created_at
 * @property-read string                  updated_at
 * @property-read FilteringListActionEnum action
 *
 * @package ProvulusSDK\Client\Response\Domain\Filtering
 */
class FilteringListResponse extends ProvulusBaseResponse
{
    public $dates = [
        'created_at',
        'updated_at',
    ];

    /**
     * @return FilteringListActionEnum
     */
    public function getActionAttribute()
    {
        return FilteringListActionEnum::byName(Str::upper($this->data['attributes']['action']));
    }
}
