<?php
/**
 * Created by PhpStorm.
 * User: ycoutu
 * Date: 27/07/17
 * Time: 4:27 PM
 */

namespace ProvulusSDK\Client\Response\Domain\Filtering\Content;

use Carbon\Carbon;
use ProvulusSDK\Client\Response\ProvulusBaseResponse;

/**
 * Class ContentCategoryResponse
 *
 * @property-read string      backend_tag
 * @property-read int         sort_order
 * @property-read bool        enabled_by_default
 * @property-read Carbon      created_at
 * @property-read Carbon|null updated_at
 * @property-read array       description
 *
 * @package ProvulusSDK\Client\Response\Domain\Filtering\Content
 */
class ContentCategoryResponse extends ProvulusBaseResponse
{
    public $dates = [
        'created_at',
        'updated_at',
    ];
}
