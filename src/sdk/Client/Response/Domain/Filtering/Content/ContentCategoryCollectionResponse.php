<?php
/**
 * Created by PhpStorm.
 * User: ycoutu
 * Date: 28/07/17
 * Time: 11:17 AM
 */

namespace ProvulusSDK\Client\Response\Domain\Filtering\Content;

use Cake\Collection\Collection;
use ProvulusSDK\Client\Response\Collection\CollectionPaginatedResponse;

/**
 * Class ContentCategoryCollectionResponse
 *
 * @method Collection|ContentCategoryResponse[] data()
 * @package ProvulusSDK\Client\Response\Domain\Filtering\Content
 */
class ContentCategoryCollectionResponse extends CollectionPaginatedResponse
{

}
