<?php
/**
 * Created by PhpStorm.
 * User: pbb
 * Date: 21/12/16
 * Time: 2:45 PM
 */

namespace ProvulusSDK\Client\Response\Domain\Filtering;

use Cake\Collection\Collection;
use ProvulusSDK\Client\Response\Domain\Filtering\Content\ContentCategoryResponse;
use ProvulusSDK\Client\Response\Domain\Filtering\Item\FilteringPolicyItemResponse;
use ProvulusSDK\Client\Response\ProvulusBaseResponse;
use ProvulusSDK\Client\Response\Traits\IncludedResponseTrait;
use ProvulusSDK\Enum\Filtering\FilteringPolicyEnum;
use ProvulusSDK\Enum\Filtering\GreymailSensitivityEnum;

/**
 * Class FilteringPolicyResponse
 *
 * Single Filtering Policy Resource Response for Filtering Policy Requests
 *
 * @property-read int                                      organization_id
 * @property-read string                                   name
 * @property-read string                                   description
 * @property-read bool                                     is_using_greylist
 * @property-read bool                                     is_content_filtering_enabled
 * @property-read string                                   created_at
 * @property-read string                                   updated_at
 * @property-read bool                                     is_active
 * @property-read bool                                     is_active_backup
 * @property-read int                                      max_message_size_in_mb
 * @property-read bool                                     is_ldap_filtering_enabled
 * @property-read FilteringPolicyEnum                      filter_type_code
 * @property-read bool                                     is_using_authorized_addresses
 * @property-read array[]                                  items
 * @property-read bool                                     is_default
 * @property-read GreymailSensitivityEnum                  greymail_sensitivity
 * @property-read ContentCategoryResponse[]|Collection     $enabled_categories
 * @property-read FilteringPolicyItemResponse[]|Collection $filtering_policy_items
 *
 * @package ProvulusSDK\Client\Response\Domain\Filtering
 */
class FilteringPolicyResponse extends ProvulusBaseResponse
{
    use IncludedResponseTrait;

    public $dates = [
        'created_at',
        'updated_at',
    ];

    /**
     * Check whether the filtering policy has domains or not
     *
     * @return bool
     */
    public function getHasDomain()
    {
        return is_null($this->items) ? false : in_array(
                    true,
                    array_column(
                        $this->items,
                        'isDomain')
                );
    }

    /**
     * Check whether the filtering policy has items
     *
     * @return bool
     */
    public function getHasFilteringPolicyItem()
    {
        return is_null($this->items) ? false : in_array(
            false,
            array_column(
                $this->items,
                'isDomain')
        );
    }

    /**
     * Returns the filtering policy's items
     *
     * @return FilteringPolicyItemResponse[]|Collection
     */
    public function getFilteringPolicyItemsAttribute()
    {
        $callback = function (array $data) {
            return new FilteringPolicyItemResponse($data);
        };

        return $this->findRelatedItems('filteringPolicyItems', $callback);
    }

    /**
     * Returns the filtering policy's enabled content categories
     *
     * @return ContentCategoryResponse[]|Collection
     */
    public function getEnabledCategoriesAttribute()
    {
        $callback = function (array $data) {
            return new ContentCategoryResponse($data);
        };

        return $this->findRelatedItems('enabledCategories', $callback);
    }

    /**
     * Returns the filtering policy greymail sensitivity
     *
     * @return GreymailSensitivityEnum
     */
    public function getGreymailSensitivityAttribute()
    {
        $greymailSensitivity = $this->data['attributes']['greymail_sensitivity'];

        return GreymailSensitivityEnum::get($greymailSensitivity);
    }

    /**
     * Returns the filtering policy type code
     *
     * @return FilteringPolicyEnum
     */
    public function getFilterTypeCodeAttribute()
    {
        $filterTypeCode = $this->data['attributes']['filter_type_code'];

        return FilteringPolicyEnum::get($filterTypeCode);
    }
}