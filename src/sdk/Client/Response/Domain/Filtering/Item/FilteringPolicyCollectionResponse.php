<?php
/**
 * Created by PhpStorm.
 * User: pbb
 * Date: 26/01/17
 * Time: 9:28 AM
 */

namespace ProvulusSDK\Client\Response\Domain\Filtering\Item;

use Cake\Collection\Collection;
use ProvulusSDK\Client\Response\Collection\CollectionPaginatedResponse;
use ProvulusSDK\Client\Response\Domain\Filtering\FilteringPolicyResponse;

/**
 * Class FilteringPolicyCollectionResponse
 *
 * @method Collection|FilteringPolicyResponse[] data()
 * @package ProvulusSDK\Client\Response\Domain\Filtering\Item
 */
class FilteringPolicyCollectionResponse extends CollectionPaginatedResponse
{

}