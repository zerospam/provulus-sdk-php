<?php
/**
 * Created by PhpStorm.
 * User: pbb
 * Date: 26/01/17
 * Time: 10:27 AM
 */

namespace ProvulusSDK\Client\Response\Domain\Filtering\Item;


use Cake\Collection\Collection;
use ProvulusSDK\Client\Response\Collection\CollectionPaginatedResponse;

/**
 * Class FilteringPolicyItemCollectionResponse
 * @method Collection|FilteringPolicyItemResponse[] data()
 *
 * @package ProvulusSDK\Client\Response\Domain\Filtering\Item
 */
class FilteringPolicyItemCollectionResponse extends CollectionPaginatedResponse
{

}