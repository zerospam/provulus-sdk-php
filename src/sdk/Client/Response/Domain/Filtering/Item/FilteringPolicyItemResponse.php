<?php
/**
 * Created by PhpStorm.
 * User: pbb
 * Date: 22/12/16
 * Time: 1:56 PM
 */

namespace ProvulusSDK\Client\Response\Domain\Filtering\Item;

use ProvulusSDK\Client\Response\ProvulusBaseResponse;
use ProvulusSDK\Enum\Filtering\FilteringPolicyItemEnum;
use ProvulusSDK\Utils\Str;

/**
 * Class FilteringPolicyItemResponse
 *
 * Response for FilteringPolicyRequests generating a json body
 *
 *
 * @property-read int                                      domain_group_id
 * @property-read FilteringPolicyItemEnum                  value_type_code
 * @property-read string                                   value
 * @property-read bool                                     is_active
 * @property-read bool|null                                is_active_backup
 * @property-read string                                   created_at
 * @property-read string                                   updated_at
 * @property-read int|null                                 domain_id
 *
 * @package ProvulusSDK\Client\Response\Domain\Filtering\Item
 */
class FilteringPolicyItemResponse extends ProvulusBaseResponse
{
    public $dates = [
        'created_at',
        'updated_at',
    ];

    public function getValueTypeCodeAttribute()
    {
        return FilteringPolicyItemEnum::byName(Str::upper($this->data['attributes']['value_type_code']));
    }
}