<?php
/**
 * Created by PhpStorm.
 * User: aaflalo
 * Date: 23/01/17
 * Time: 2:54 PM
 */

namespace ProvulusSDK\Client\Response\Domain;


use Cake\Collection\Collection;
use ProvulusSDK\Client\Response\Collection\CollectionPaginatedResponse;

/**
 * Class DomainCollectionResponse
 *
 * @method Collection|DomainResponse[] data()
 * @package ProvulusSDK\Client\Response\Domain
 */
class DomainCollectionResponse extends CollectionPaginatedResponse
{

}