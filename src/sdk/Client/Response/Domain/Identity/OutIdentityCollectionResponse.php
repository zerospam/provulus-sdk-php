<?php
/**
 * Created by PhpStorm.
 * User: ycoutu
 * Date: 11/10/17
 * Time: 9:52 AM
 */

namespace ProvulusSDK\Client\Response\Domain\Identity;

use Cake\Collection\Collection;
use ProvulusSDK\Client\Response\Collection\CollectionPaginatedResponse;

/**
 * Class OutIdentityCollectionResponse
 *
 * @method Collection|OutIdentityResponse[] data()
 * @package ProvulusSDK\Client\Response\Domain\Identity
 */
class OutIdentityCollectionResponse extends CollectionPaginatedResponse
{

}
