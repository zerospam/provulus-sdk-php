<?php
/**
 * Created by PhpStorm.
 * User: ycoutu
 * Date: 11/10/17
 * Time: 9:56 AM
 */

namespace ProvulusSDK\Client\Response\Domain\Identity\Type;

use ProvulusSDK\Client\Response\ProvulusBaseResponse;

/**
 * Class OutIdentityIpResponse
 *
 *
 * @package ProvulusSDK\Client\Response\Domain\Identity\Type
 */
class OutIdentityOffice365Response extends ProvulusBaseResponse
{

}
