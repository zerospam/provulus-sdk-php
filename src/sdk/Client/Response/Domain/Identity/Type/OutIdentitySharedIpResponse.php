<?php
/**
 * Created by PhpStorm.
 * User: ycoutu
 * Date: 23/10/17
 * Time: 2:01 PM
 */

namespace ProvulusSDK\Client\Response\Domain\Identity\Type;

use Carbon\Carbon;
use ProvulusSDK\Client\Response\ProvulusBaseResponse;

/**
 * Class OutIdentityLoginResponse
 *
 * @property-read string[] ips
 * @property-read int      out_identity_id
 * @property-read int      reseller_id
 * @property-read string   reseller_name
 * @property-read Carbon   created_at
 * @property-read Carbon   updated_at
 *
 * @package ProvulusSDK\Client\Response\Domain\Identity\Type
 */
class OutIdentitySharedIpResponse extends ProvulusBaseResponse
{
    /** @var string[] */
    public $dates
        = [
            'created_at',
            'updated_at',
        ];
}
