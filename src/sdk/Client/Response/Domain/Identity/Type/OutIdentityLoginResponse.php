<?php
/**
 * Created by PhpStorm.
 * User: ycoutu
 * Date: 17/10/17
 * Time: 2:23 PM
 */

namespace ProvulusSDK\Client\Response\Domain\Identity\Type;

use ProvulusSDK\Client\Response\ProvulusBaseResponse;

/**
 * Class OutIdentityLoginResponse
 *
 * @property-read string      username
 * @property-read int         out_identity_id
 * @property-read string|null new_password
 *
 * @package ProvulusSDK\Client\Response\Domain\Identity\Type
 */
class OutIdentityLoginResponse extends ProvulusBaseResponse
{

}
