<?php
/**
 * Created by PhpStorm.
 * User: ycoutu
 * Date: 11/10/17
 * Time: 10:16 AM
 */

namespace ProvulusSDK\Client\Response\Domain\Identity\Ip;

use Carbon\Carbon;
use ProvulusSDK\Client\Response\ProvulusBaseResponse;

/**
 * Class OutClientsIpResponse
 *
 * @property-read int    reseller_id
 * @property-read string value
 * @property-read Carbon created_at
 * @property-read Carbon updated_at
 *
 * @package ProvulusSDK\Client\Response\Domain\Identity\Ip
 */
class OutClientsIpResponse extends ProvulusBaseResponse
{
    /** @var string[] */
    public $dates
        = [
            'created_at',
            'updated_at',
        ];
}
