<?php
/**
 * Created by PhpStorm.
 * User: ycoutu
 * Date: 11/10/17
 * Time: 10:20 AM
 */

namespace ProvulusSDK\Client\Response\Domain\Identity\Ip;

use Cake\Collection\Collection;
use ProvulusSDK\Client\Response\Collection\CollectionResponse;

/**
 * Class OutClientsIpCollectionResponse
 *
 * @method Collection|OutClientsIpResponse[] data()
 * @property-read string[] $ips
 *
 * @package ProvulusSDK\Client\Response\Domain\Identity\Ip
 */
class OutClientsIpCollectionResponse extends CollectionResponse
{
    /**
     * Getter for ips
     *
     * @return array
     */
    public function getIpsAttribute()
    {
        return $this->data()->map(
            function (OutClientsIpResponse $response) {
                return $response->value;
            }
        )->toArray();
    }
}
