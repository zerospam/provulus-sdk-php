<?php
/**
 * Created by PhpStorm.
 * User: ycoutu
 * Date: 11/10/17
 * Time: 9:38 AM
 */

namespace ProvulusSDK\Client\Response\Domain\Identity;

use ProvulusSDK\Client\Response\ProvulusBaseResponse;
use ProvulusSDK\Client\Response\Traits\IncludedResponseTrait;
use ProvulusSDK\Enum\Domain\Identity\IdentityAuthenticationTypeEnum;
use ProvulusSDK\Utils\Str;

/**
 * Class OutIdentityResponse
 *
 * @property-read int                            organization_id
 * @property-read string                         name
 * @property-read int|null                       forced_dg_id
 * @property-read bool                           allow_extras
 * @property-read IdentityAuthenticationTypeEnum authentication_type_code
 * @property-read mixed                          details
 *
 * @package ProvulusSDK\Client\Response\Domain\Identity
 */
class OutIdentityResponse extends ProvulusBaseResponse
{
    use IncludedResponseTrait;
    /**
     * @return IdentityAuthenticationTypeEnum
     */
    public function getAuthenticationTypeCodeAttribute()
    {
        return IdentityAuthenticationTypeEnum::byName(Str::upper($this->data['attributes']['authentication_type_code']));
    }

    /**
     * Getter for details
     *
     * @return \Cake\Collection\Collection|mixed|null|\ProvulusSDK\Client\Response\IProvulusResponse
     */
    public function getDetailsAttribute()
    {
        $identityType = $this->authentication_type_code->buildType();
        $callback = $identityType->getResponseCallback();

        if ($identityType->isResponseCollection()) {
            return $this->findRelatedItems('details', $callback);
        } else {
            return $this->findRelatedItem('details', $callback);
        }
    }
}
