<?php
/**
 * Created by PhpStorm.
 * User: ycoutu
 * Date: 07/03/19
 * Time: 10:11 AM
 */

namespace ProvulusSDK\Client\Response\Domain\Update;

use Carbon\Carbon;
use ProvulusSDK\Client\Response\ProvulusBaseResponse;
use ProvulusSDK\Enum\Domain\DomainTypeEnum;
use ProvulusSDK\Enum\Renewal\PaymentRenewalStateEnum;
use ProvulusSDK\Utils\Str;

/**
 * Class DomainMailboxResponse
 *
 * @property-read int                     $organization_id
 * @property-read int|null                $support_organization_id
 * @property-read int|null                $billing_organization_id
 * @property-read string                  $corporate_name_normalized
 * @property-read string                  $corporate_name
 * @property-read Carbon|null             $yearly_renewal_date
 * @property-read PaymentRenewalStateEnum $renewal_state
 * @property-read int                     $domain_id
 * @property-read string                  $domain_name
 * @property-read DomainTypeEnum          $domain_type
 * @property-read Carbon|null             $dmbcount_updated_at
 * @property-read int                     $mailbox_count
 * @property-read int|null                $last_nb_declared_mailboxes
 * @property-read int|null                $nb_declared_mailboxes
 *
 * @package ProvulusSDK\Client\Response\Domain\Update
 */
class DomainMailboxResponse extends ProvulusBaseResponse
{
    public $dates = [
        'yearly_renewal_date',
        'dmbcount_updated_at',
    ];

    /**
     * @return DomainTypeEnum
     */
    public function getDomainTypeAttribute()
    {
        return DomainTypeEnum::byName(Str::upper($this->data['attributes']['domain_type']));
    }

    /**
     * @return PaymentRenewalStateEnum
     */
    public function getRenewalStateAttribute()
    {
        return PaymentRenewalStateEnum::get($this->data['attributes']['renewal_state']);
    }
}
