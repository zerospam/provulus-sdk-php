<?php
/**
 * Created by PhpStorm.
 * User: ycoutu
 * Date: 07/03/19
 * Time: 10:26 AM
 */

namespace ProvulusSDK\Client\Response\Domain\Update;

use Cake\Collection\Collection;
use ProvulusSDK\Client\Response\Collection\CollectionPaginatedResponse;

/**
 * Class DomainMailboxCollectionResponse
 *
 * @method Collection|DomainMailboxResponse[] data()
 *
 * @package ProvulusSDK\Client\Response\Domain\Update
 */
class DomainMailboxCollectionResponse extends CollectionPaginatedResponse
{

}
