<?php
/**
 * Created by PhpStorm.
 * User: aaflalo
 * Date: 06/12/16
 * Time: 11:50 AM
 */

namespace ProvulusSDK\Client\Response\Domain;


use Carbon\Carbon;
use ProvulusSDK\Client\Response\ProvulusBaseResponse;
use ProvulusSDK\Enum\Domain\DomainTransportProtocolEnum;
use ProvulusSDK\Enum\Domain\DomainTypeEnum;
use ProvulusSDK\Utils\Str;

/**
 * Class DomainResponse
 *
 * @property-read int                         $domain_group_id
 * @property-read int                         $organization_id
 * @property-read string                      $name
 * @property-read string                      $description
 * @property-read string                      $transport
 * @property-read DomainTransportProtocolEnum $transport_protocol
 * @property-read int                         $transport_port
 * @property-read bool                        $transport_use_mx
 * @property-read bool                        $transport_hold
 * @property-read bool                        $is_active
 * @property-read Carbon                      $created_at
 * @property-read Carbon                      $updated_at
 * @property-read bool                        $is_active_backup
 * @property-read bool                        $is_unconditional_delete
 * @property-read Carbon                      $count_start_date
 * @property-read int                         $mailbox_count
 * @property-read int                         $rejected_mailbox_count
 * @property-read DomainTypeEnum              $domain_type
 * @property-read Carbon                      $dmbcount_updated_at
 * @property-read bool                        $is_dmbcount_declared
 * @property-read string                      $email
 * @property-read bool                        $is_check_transport_success
 * @property-read bool                        $is_check_mx_success
 * @property-read string                      $transport_server_type
 * @property-read Carbon                      $activation_date
 * @property-read float                       $annual_price
 * @property-read string                      $organization_name
 * @property-read string                      $transport_with_port
 * @property-read int                         $nb_mailboxes
 * @property-read bool|null                   $is_deletable
 *
 * @package ProvulusSDK\Client\Response\Domain
 */
class DomainResponse extends ProvulusBaseResponse
{
    /**
     * @var string[]
     */
    public $dates
        = [
            'created_at',
            'updated_at',
            'count_start_date',
            'activation_date',
            'dmbcount_updated_at',
        ];

    /**
     * @return DomainTransportProtocolEnum
     */
    public function getTransportProtocolAttribute()
    {
        return DomainTransportProtocolEnum::byName(Str::upper($this->data['attributes']['transport_protocol']));
    }

    /**
     * @return DomainTypeEnum
     */
    public function getDomainTypeAttribute()
    {
        return DomainTypeEnum::byName(Str::upper($this->data['attributes']['domain_type']));
    }

    /**
     * @return string
     */
    public function getTransportWithPortAttribute()
    {
        return $this->data['attributes']['transport'] . ':' . $this->data['attributes']['transport_port'];
    }
}