<?php
/**
 * Created by PhpStorm.
 * User: pbb
 * Date: 10/10/18
 * Time: 3:57 PM
 */

namespace ProvulusSDK\Client\Response\Domain;

use Cake\Collection\Collection;
use ProvulusSDK\Client\Response\Collection\CollectionResponse;

/**
 * Class UnpaginatedDomainCollectionResponse
 * @method Collection|DomainResponse[] data()
 *
 * @package ProvulusSDK\Client\Response\Domain
 */
class UnpaginatedDomainCollectionResponse extends CollectionResponse
{

}