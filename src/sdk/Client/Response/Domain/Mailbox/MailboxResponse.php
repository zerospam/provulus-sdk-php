<?php
/**
 * Created by PhpStorm.
 * User: ycoutu
 * Date: 28/02/19
 * Time: 1:56 PM
 */

namespace ProvulusSDK\Client\Response\Domain\Mailbox;

use Carbon\Carbon;
use ProvulusSDK\Client\Response\ProvulusBaseResponse;

/**
 * Class MailboxResponse
 *
 * @property-read int         $domain_id
 * @property-read string      $email_address
 * @property-read int         $message_count
 * @property-read Carbon      $last_message_date
 * @property-read string      $last_queue_tag
 * @property-read string      $last_log_entry
 * @property-read int         $version
 * @property-read Carbon      $created_at
 * @property-read Carbon|null $updated_at
 *
 * @package ProvulusSDK\Client\Response\Domain\Mailbox
 */
class MailboxResponse extends ProvulusBaseResponse
{
    /** @var string[] */
    public $dates = [
        'created_at',
        'updated_at',
        'last_message_date',
    ];
}
