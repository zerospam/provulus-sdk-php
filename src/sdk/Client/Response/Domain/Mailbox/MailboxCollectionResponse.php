<?php
/**
 * Created by PhpStorm.
 * User: ycoutu
 * Date: 28/02/19
 * Time: 2:00 PM
 */

namespace ProvulusSDK\Client\Response\Domain\Mailbox;

use Cake\Collection\Collection;
use ProvulusSDK\Client\Response\Collection\CollectionPaginatedResponse;

/**
 * Class MailboxCollectionResponse
 *
 * @method Collection|MailboxResponse[] data()
 *
 * @package ProvulusSDK\Client\Response\Domain\Mailbox
 */
class MailboxCollectionResponse extends CollectionPaginatedResponse
{

}
