<?php
/**
 * Created by PhpStorm.
 * User: aaflalo
 * Date: 23/01/17
 * Time: 1:32 PM
 */

namespace ProvulusSDK\Client\Response\Traits;

use Cake\Collection\Collection;
use ProvulusSDK\Client\Response\IProvulusResponse;
use ProvulusSDK\Utils\Relation\RelationContainer;

trait IncludedResponseTrait
{

    /**
     * Find the relation item in the relationships
     *
     * @param  string  $type
     * @param \Closure $callback
     *
     * @return null|mixed|IProvulusResponse (result of the Closure) Take the array of data as argument
     */
    private function findRelatedItem($type, \Closure $callback)
    {
        if (!isset($this->data['relationships'])) {
            return null;
        }

        if (!isset($this->data['relationships'][$type])) {
            return null;
        }

        $id       = $this->data['relationships'][$type]['data']['id'];
        $itemType = $this->data['relationships'][$type]['data']['type'];

        if ($id == $this->id() && $itemType == $this->type()) {
            return $this;
        }


        foreach ($this->included as $item) {
            if ($item['type'] == $itemType && $item['id'] == $id) {
                return $callback($item);
            }
        }

        return null;
    }

    /**
     * Used when the relation is an array of items
     *
     * @param  string  $type
     * @param \Closure $callback
     *
     * @return Collection
     */
    private function findRelatedItems($type, \Closure $callback)
    {
        if (!isset($this->data['relationships'])) {
            return new Collection([]);
        }

        if (!isset($this->data['relationships'][$type])) {
            return new Collection([]);
        }

        if (!is_array($this->data['relationships'][$type]['data'])) {
            throw new \InvalidArgumentException('Use only with array');
        }

        if (empty($this->data['relationships'][$type]['data'])) {
            return new Collection([]);
        }

        $currentItem = new RelationContainer($this->type(), $this->id());

        $relationType = null;

        //Parse the relationship of the current item
        $itemsRelations = [];
        foreach ($this->data['relationships'][$type]['data'] as $relation) {
            $container                         = new RelationContainer($relation['type'], $relation['id']);
            $itemsRelations[$container->key()] = $container;
            $relationType                      = $relation['type'];
        }


        $included = array_filter($this->included, function (array $item) use ($relationType) {
            return $item['type'] == $relationType;
        });

        //Find the full data of the relationship item
        $items = [];
        foreach ($included as $item) {
            $itemRelation = new RelationContainer($item['type'], $item['id']);
            if ($itemRelation->key() == $currentItem->key()) {
                $items[] = $this;
                continue;
            }
            /**
             * @var $relation RelationContainer
             */
            foreach ($itemsRelations as $key => $relation) {
                if ($key == $itemRelation->key()) {
                    $items[] = $callback($item);
                }
            }
        }

        return new Collection($items);
    }
}
