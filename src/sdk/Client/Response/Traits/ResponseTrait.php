<?php
/**
 * Created by PhpStorm.
 * User: aaflalo
 * Date: 23/01/17
 * Time: 1:27 PM
 */

namespace ProvulusSDK\Client\Response\Traits;


use ProvulusSDK\Client\RateLimit\IRateLimitData;
use ProvulusSDK\Client\RateLimit\RateLimitData;
use ProvulusSDK\Client\Request\IProvulusRequest;
use ProvulusSDK\Client\Response\IProvulusObjectResponse;
use ProvulusSDK\Client\Response\IProvulusResponse;

trait ResponseTrait
{

    /**
     * @var RateLimitData
     */
    private $rateLimit;

    /**
     * @var IProvulusRequest|null
     */
    private $parentRequest;

    /**
     * @var IProvulusObjectResponse|null
     */
    private $parentResponse;

    /**
     * Request that was done to generate this response
     *
     * @return IProvulusRequest|null
     */
    public function getParentRequest()
    {
        return $this->parentRequest;
    }

    /**
     * If this response has been created by another response, you'll find it here
     *
     * @return null|IProvulusObjectResponse
     */
    public function getParentResponse()
    {
        return $this->parentResponse;
    }

    /**
     * set the Request and response object
     */
    private function setRequestResponse()
    {
        foreach (debug_backtrace() as $debug) {
            if (!isset($debug['object'])) {
                throw new \InvalidArgumentException('Can\'t find related object');
            }
            $object = $debug['object'];
            if ($object instanceof IProvulusRequest) {
                $this->parentRequest = $object;
                break;
            }
            if ($object instanceof IProvulusResponse && $object !== $this) {
                $this->parentResponse = $object;
                break;
            }
        }
        if (!$this->parentRequest && !$this->parentResponse) {
            throw new \InvalidArgumentException('Can only be created by a request or a response');
        }
    }


    /**
     * Getter for rateLimit
     *
     * @return IRateLimitData
     */
    public function getRateLimit()
    {
        return $this->rateLimit;
    }

    /**
     * @param IRateLimitData $rateLimit
     */
    public function setRateLimit(IRateLimitData $rateLimit)
    {
        if (!is_null($this->rateLimit)) {
            throw new \InvalidArgumentException('Can only be set once');
        }
        $this->rateLimit = $rateLimit;
    }


}