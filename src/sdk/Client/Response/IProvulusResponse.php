<?php
/**
 * Created by PhpStorm.
 * User: aaflalo
 * Date: 23/01/17
 * Time: 1:23 PM
 */
namespace ProvulusSDK\Client\Response;

use ProvulusSDK\Client\RateLimit\IRateLimitData;
use ProvulusSDK\Client\Request\IProvulusRequest;

interface IProvulusResponse
{
    /**
     * Data contained in the response
     *
     * @return array
     */
    public function data();

    /**
     * Type of the response
     *
     * @return string
     */
    public function type();

    /**
     * Get the current rate limit
     *
     * @return IRateLimitData
     */
    public function getRateLimit();

    /**
     * Request that was done to generate this response
     *
     * @return IProvulusRequest|null
     */
    public function getParentRequest();

    /**
     * If this response has been created by another response, you'll find it here
     *
     * @return null|IProvulusResponse
     */
    public function getParentResponse();

    /**
     * @param IRateLimitData $rateLimit
     */
    public function setRateLimit(IRateLimitData $rateLimit);
}