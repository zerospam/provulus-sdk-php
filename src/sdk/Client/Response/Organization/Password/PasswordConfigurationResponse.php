<?php
/**
 * Created by PhpStorm.
 * User: ycoutu
 * Date: 18/04/18
 * Time: 9:37 AM
 */

namespace ProvulusSDK\Client\Response\Organization\Password;

use ProvulusSDK\Client\Response\ProvulusBaseResponse;

/**
 * Class PasswordConfigurationResponse
 *
 * @package ProvulusSDK\Client\Response\Organization\Password
 *
 * @property-read int                 $min_length
 * @property-read int                 $min_lower
 * @property-read int                 $min_upper
 * @property-read int                 $min_digits
 * @property-read int                 $min_specials
 * @property-read int|null            $life_in_days
 * @property-read int|null            $history_count
 * @property-read \Carbon\Carbon|null $created_at
 * @property-read \Carbon\Carbon|null $updated_at
 * @property-read bool                $is_history_enabled
 */
class PasswordConfigurationResponse extends ProvulusBaseResponse
{
    public $dates = [
        'created_at',
        'updated_at',
    ];

    public function getIsHistoryEnabledAttribute()
    {
        return !is_null($this->history_count);
    }
}
