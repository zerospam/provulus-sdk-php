<?php
/**
 * Created by PhpStorm.
 * User: ycoutu
 * Date: 11/12/18
 * Time: 4:37 PM
 */

namespace ProvulusSDK\Client\Response\Organization\Billing\Invoice;

use Carbon\Carbon;
use ProvulusSDK\Client\Response\ProvulusBaseResponse;
use ProvulusSDK\Enum\PaymentCurrency\PaymentCurrencyEnum;
use ProvulusSDK\Utils\Str;

/**
 * Class InvoiceResponse
 *
 * @property-read int                 $billing_id
 * @property-read string              $customer_external_id
 * @property-read string              $external_id
 * @property-read string              $note
 * @property-read string              $terms
 * @property-read PaymentCurrencyEnum $currency
 * @property-read Carbon              $created_at
 * @property-read Carbon              $updated_at
 * @property-read string              $invoice_number
 * @property-read float               $total_in_cad
 * @property-read Carbon              $billing_start
 * @property-read Carbon              $billing_end
 * @property-read Carbon|null         $sent_at
 * @property-read boolean             $is_sent
 * @property-read string|null         $external_url
 * @property-read string|null         $shared_link
 * @property-read float               $total
 *
 * @package ProvulusSDK\Client\Response\Organization\Billing\Invoice
 */
class InvoiceResponse extends ProvulusBaseResponse
{
    public $dates = [
        'created_at',
        'updated_at',
        'billing_start',
        'billing_end',
        'sent_at',
    ];

    /**
     * @return PaymentCurrencyEnum
     */
    public function getCurrencyAttribute()
    {
        $currency = $this->data['attributes']['currency'];

        return PaymentCurrencyEnum::byName(Str::upper($currency));
    }
}
