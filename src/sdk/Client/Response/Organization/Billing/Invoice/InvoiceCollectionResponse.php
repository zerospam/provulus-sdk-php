<?php
/**
 * Created by PhpStorm.
 * User: ycoutu
 * Date: 11/12/18
 * Time: 4:41 PM
 */

namespace ProvulusSDK\Client\Response\Organization\Billing\Invoice;

use Cake\Collection\Collection;
use ProvulusSDK\Client\Response\Collection\CollectionPaginatedResponse;

/**
 * Class InvoiceCollectionResponse
 *
 * @method Collection|InvoiceResponse[] data()
 *
 * @package ProvulusSDK\Client\Response\Organization\Billing\Invoice
 */
class InvoiceCollectionResponse extends CollectionPaginatedResponse
{

}
