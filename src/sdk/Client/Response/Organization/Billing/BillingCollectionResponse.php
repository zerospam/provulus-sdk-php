<?php
/**
 * Created by PhpStorm.
 * User: ycoutu
 * Date: 19/09/18
 * Time: 10:51 AM
 */

namespace ProvulusSDK\Client\Response\Organization\Billing;

use Cake\Collection\Collection;
use ProvulusSDK\Client\Response\Collection\CollectionPaginatedResponse;

/**
 * Class BillingCollectionResponse
 *
 * @method Collection|BillingResponse[] data()
 *
 * @package ProvulusSDK\Client\Response\Organization\Billing
 */
class BillingCollectionResponse extends CollectionPaginatedResponse
{

}
