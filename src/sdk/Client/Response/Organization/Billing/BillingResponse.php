<?php
/**
 * Created by PhpStorm.
 * User: ycoutu
 * Date: 19/09/18
 * Time: 10:51 AM
 */

namespace ProvulusSDK\Client\Response\Organization\Billing;

use Carbon\Carbon;
use ProvulusSDK\Client\Response\ProvulusBaseResponse;
use ProvulusSDK\Enum\PaymentCurrency\PaymentCurrencyEnum;
use ProvulusSDK\Utils\Str;

/**
 * Class BillingResponse
 *
 * @property-read string              $external_id
 * @property-read PaymentCurrencyEnum $currency
 * @property-read string              $city
 * @property-read string              $country
 * @property-read string              $postal_code
 * @property-read string              $state
 * @property-read string              $street1
 * @property-read string              $street2
 * @property-read Carbon|null         $created_at
 * @property-read Carbon|null         $updated_at
 * @property-read integer             $price_list_id
 * @property-read string              $email
 * @property-read string              $last_name
 * @property-read string              $first_name
 * @property-read string              $external_url
 *
 * @package ProvulusSDK\Client\Response\Organization\Billing
 */
class BillingResponse extends ProvulusBaseResponse
{
    public $dates = [
        'created_at',
        'updated_at'
    ];

    /**
     * @return PaymentCurrencyEnum
     */
    public function getCurrencyAttribute()
    {
        $currency = $this->data['attributes']['currency'];

        return PaymentCurrencyEnum::byName(Str::upper($currency));
    }
}
