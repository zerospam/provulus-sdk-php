<?php
/**
 * Created by PhpStorm.
 * User: ycoutu
 * Date: 26/09/18
 * Time: 10:32 AM
 */

namespace ProvulusSDK\Client\Response\Organization\Trial;

use Carbon\Carbon;
use ProvulusSDK\Client\Response\ProvulusBaseResponse;
use ProvulusSDK\Enum\Trial\PostTrialStateEnum;
use ProvulusSDK\Utils\Str;

/**
 * Class PostTrialActionResponse
 *
 * @property-read int                $organization_id
 * @property-read string|null        $comment
 * @property-read PostTrialStateEnum $state
 * @property-read Carbon|null        $created_at
 * @property-read Carbon|null        $updated_at
 * @property-read Carbon|null        $notification_sent_at
 *
 * @package ProvulusSDK\Client\Response\Organization\Trial
 */
class PostTrialActionResponse extends ProvulusBaseResponse
{
    public $dates = [
        'created_at',
        'updated_at',
        'notification_sent_at',
    ];

    /**
     * @return PostTrialStateEnum
     */
    public function getStateAttribute()
    {
        $rule = $this->data['attributes']['state'];

        return PostTrialStateEnum::byName(Str::upper($rule));
    }
}
