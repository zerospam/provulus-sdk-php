<?php
/**
 * Created by PhpStorm.
 * User: aaflalo
 * Date: 06/12/16
 * Time: 12:00 PM
 */

namespace ProvulusSDK\Client\Response\Organization;

use Cake\Collection\Collection;
use Carbon\Carbon;
use DateTimeZone;
use ProvulusSDK\Client\Response\Domain\DomainResponse;
use ProvulusSDK\Client\Response\Organization\Billing\BillingResponse;
use ProvulusSDK\Client\Response\Organization\Cancellation\CancellationInformationResponse;
use ProvulusSDK\Client\Response\Organization\Cidr\LoginCidrResponse;
use ProvulusSDK\Client\Response\Organization\Outbound\OutboundResponse;
use ProvulusSDK\Client\Response\Organization\Password\PasswordConfigurationResponse;
use ProvulusSDK\Client\Response\Organization\Trial\PostTrialActionResponse;
use ProvulusSDK\Client\Response\ProvulusBaseResponse;
use ProvulusSDK\Client\Response\Traits\IncludedResponseTrait;
use ProvulusSDK\Enum\Billing\BillingPeriodEnum;
use ProvulusSDK\Enum\Locale\LocaleEnum;
use ProvulusSDK\Enum\Login\TwoFactorAuthUsersEnum;
use ProvulusSDK\Enum\Organization\PaymentState\PaymentStateEnum;
use ProvulusSDK\Enum\Organization\Type\OrganizationTypeEnum;
use ProvulusSDK\Enum\PaymentCurrency\PaymentCurrencyEnum;
use ProvulusSDK\Enum\PersonOfInterest\PersonOfInterestMatchRuleEnum;
use ProvulusSDK\Enum\Renewal\PaymentRenewalStateEnum;
use ProvulusSDK\Utils\Str;

/**
 * Class OrganizationCreateResponse
 *
 * @property-read int|null                             $support_organization_id
 * @property-read int|null                             $billing_organization_id
 * @property-read bool                                 $is_access_allowed
 * @property-read bool                                 $is_billing_organization
 * @property-read bool                                 $is_support_organization
 * @property-read string                               $corporate_name
 * @property-read string                               $corporate_name_normalized
 * @property-read string|null                          $portal_prefix
 * @property-read LocaleEnum                           $default_language_code
 * @property-read DateTimeZone                         $timezone_code
 * @property-read string|null                          $note_shared
 * @property-read int                                  $quar_expiry_in_days
 * @property-read string|null                          $note_for_zs_only
 * @property-read string|null                          $logo_reference
 * @property-read bool                                 $is_active
 * @property-read int                                  $version
 * @property-read Carbon                               $created_at
 * @property-read Carbon|null                          $updated_at
 * @property-read bool                                 $dsbl_portal_selfprov
 * @property-read bool                                 $dsbl_org_selfprov
 * @property-read PaymentCurrencyEnum                  $currency
 * @property-read bool                                 $renewal_messages_allowed
 * @property-read string|null                          $renewal_note
 * @property-read Carbon|null                          $yearly_renewal_date
 * @property-read PaymentRenewalStateEnum              $renewal_state
 * @property-read Carbon|null                          $renewal_msg_sent_at
 * @property-read Carbon|null                          $next_renewal_msg
 * @property-read BillingPeriodEnum|null               $billing_period
 * @property-read Carbon|null                          $billing_base_date
 * @property-read Carbon|null                          $next_billing_date
 * @property-read string|null                          $salesforce_id
 * @property-read bool                                 $shows_powered_by_logo
 * @property-read bool                                 $is_distributor_organization
 * @property-read int|null                             $distributor_id
 * @property-read int|null                             $external_distributor_id
 * @property-read Carbon|null                          $activation_date
 * @property-read int|null                             $support_org
 * @property-read int|null                             $billing_org
 * @property-read int|null                             $distributor_org
 * @property-read Carbon                               $trial_end_date
 * @property-read Carbon|null                          $trial_notification_sent_at
 * @property-read bool                                 $has_trial
 * @property-read bool                                 $is_notified_end_trial
 * @property-read bool                                 $can_grant_trial
 * @property-read bool                                 $is_clients_notified_end_trial
 * @property-read PersonOfInterestMatchRuleEnum        $person_of_interest_match_rule
 * @property-read OrganizationTypeEnum                 $organization_type_enum
 * @property-read OrganizationResponse|null            $support_organization
 * @property-read OrganizationResponse|null            $distributor_organization
 * @property-read PostTrialActionResponse|null         $post_trial_action
 * @property-read CancellationInformationResponse|null $cancellation_information
 * @property-read BillingResponse|null                 $billing
 * @property-read PasswordConfigurationResponse        $password_configuration
 * @property-read LoginCidrResponse[]|Collection       $login_cidrs
 * @property-read DomainResponse[]|Collection          $domains
 * @property-read OutboundResponse                     $outbound
 * @property-read bool                                 $is_two_factor_auth_mandatory
 * @property-read TwoFactorAuthUsersEnum               $two_factor_auth_users
 * @property-read Carbon                               $end_billing_period
 * @property-read PaymentStateEnum                     $payment_state
 * @property-read bool                                 $is_school_organization
 * @property-read bool                                 $needs_to_fill_billing
 * @property-read bool                                 $is_billable
 * @package ProvulusSDK\Client\Response\Organization
 */
class OrganizationResponse extends ProvulusBaseResponse
{
    use IncludedResponseTrait;

    /**
     * @var string[]
     */
    public $dates
        = [
            'created_at',
            'updated_at',
            'activation_date',
            'next_renewal_msg',
            'renewal_msg_sent_at',
            'next_renewal_msg',
            'yearly_renewal_date',
            'billing_base_date',
            'trial_end_date',
            'trial_notification_sent_at',
            'next_billing_date',
            'end_billing_period'
        ];

    /**
     * @return PersonOfInterestMatchRuleEnum
     */
    public function getPersonOfInterestMatchRuleAttribute()
    {
        $rule = $this->data['attributes']['person_of_interest_match_rule'];

        return PersonOfInterestMatchRuleEnum::byName(Str::upper($rule));
    }

    /**
     * @return TwoFactorAuthUsersEnum
     */
    public function getTwoFactorAuthUsersAttribute()
    {
        $rule = $this->data['attributes']['two_factor_auth_users'];

        return TwoFactorAuthUsersEnum::byName(Str::upper($rule));
    }

    /**
     * @return DateTimeZone
     */
    public function getTimezoneCodeAttribute()
    {
        $timezone = $this->data['attributes']['timezone_code']['timezone'];

        return new DateTimeZone($timezone);
    }

    /**
     * @return LocaleEnum
     */
    public function getDefaultLanguageCodeAttribute()
    {
        $languageCode = $this->data['attributes']['default_language_code'];

        return LocaleEnum::byName(Str::upper($languageCode));
    }

    /**
     * @return PaymentRenewalStateEnum
     */
    public function getRenewalStateAttribute()
    {
        return PaymentRenewalStateEnum::get($this->data['attributes']['renewal_state']);
    }

    /**
     * @return PaymentCurrencyEnum
     */
    public function getCurrencyAttribute()
    {
        $currency = $this->data['attributes']['currency'];

        return PaymentCurrencyEnum::byName(Str::upper($currency));
    }

    /**
     * @return BillingPeriodEnum|null
     */
    public function getBillingPeriodAttribute()
    {
        $period = $this->data['attributes']['billing_period'];
        if ($period) {
            return BillingPeriodEnum::byName(Str::upper($period));
        }

        return null;
    }

    /**
     * get the support Organization in the response
     *
     * @return null|OrganizationResponse
     */
    public function getSupportOrganizationAttribute()
    {
        $callback = function (array $data) {
            return new OrganizationResponse($data);
        };

        $type = 'supportOrganization';

        return $this->findRelatedItem($type, $callback);
    }

    /**
     * get the support Organization in the response
     *
     * @return null|OrganizationResponse
     */
    public function getDistributorOrganizationAttribute()
    {
        $callback = function (array $data) {
            return new OrganizationResponse($data);
        };

        $type = 'distributorOrganization';

        return $this->findRelatedItem($type, $callback);
    }

    /**
     * get the cancellation information in the response
     *
     * @return null|CancellationInformationResponse
     */
    public function getCancellationInformationAttribute()
    {
        $callback = function (array $data) {
            return new CancellationInformationResponse($data);
        };

        $type = 'cancellationInformation';

        return $this->findRelatedItem($type, $callback);
    }

    /**
     * get the billing information in the response
     *
     * @return null|CancellationInformationResponse
     */
    public function getBillingAttribute()
    {
        $callback = function (array $data) {
            return new BillingResponse($data);
        };

        $type = 'billing';

        return $this->findRelatedItem($type, $callback);
    }

    /**
     * get the post trial action in the response
     *
     * @return null|PostTrialActionResponse
     */
    public function getPostTrialActionAttribute()
    {
        $callback = function (array $data) {
            return new PostTrialActionResponse($data);
        };

        $type = 'postTrialAction';

        return $this->findRelatedItem($type, $callback);
    }

    /**
     * get the password configuration in the response
     *
     * @return null|CancellationInformationResponse
     */
    public function getPasswordConfigurationAttribute()
    {
        $callback = function (array $data) {
            return new PasswordConfigurationResponse($data);
        };

        $type = 'passwordConfiguration';

        return $this->findRelatedItem($type, $callback);
    }

    /**
     * @return LoginCidrResponse[]|Collection
     */
    public function getLoginCidrsAttribute()
    {
        $callback = function (array $data) {
            return new LoginCidrResponse($data);
        };

        return $this->findRelatedItems('loginCidrs', $callback);
    }

    /**
     * @return LoginCidrResponse[]|Collection
     */
    public function getDomainsAttribute()
    {
        $callback = function (array $data) {
            return new DomainResponse($data);
        };

        return $this->findRelatedItems('domains', $callback);
    }

    /**
     * @return OutboundResponse
     */
    public function getOutboundAttribute()
    {
        $callback = function (array $data) {
            return new OutboundResponse($data);
        };

        return $this->findRelatedItem('outbound', $callback);
    }

    /**
     * Get the support organization id
     *
     * @return int|null
     */
    public function getSupportOrgAttribute()
    {
        return $this->support_organization_id;
    }

    /**
     * Get the billing organization id
     *
     * @return int|null
     */
    public function getBillingOrgAttribute()
    {
        return $this->billing_organization_id;
    }

    /**
     * Get the Distributor organization id
     *
     * @return int|null
     */
    public function getDistributorOrgAttribute()
    {
        return $this->distributor_id;
    }

    /**
     * @return PaymentStateEnum
     */
    public function getPaymentStateAttribute()
    {
        $paymentState = $this->data['attributes']['payment_state'];

        return PaymentStateEnum::byName(Str::upper($paymentState));
    }

    /**
     * @return OrganizationTypeEnum
     */
    public function getOrganizationTypeEnumAttribute()
    {
        $orgType = $this->data['attributes']['organization_type_enum'];

        return OrganizationTypeEnum::byName(Str::upper($orgType));
    }
}
