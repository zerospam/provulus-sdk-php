<?php


namespace ProvulusSDK\Client\Response\Organization\Outbound;

use ProvulusSDK\Client\Response\ProvulusBaseResponse;

/**
 * Class OutboundResponse
 *
 * @property-read string $relay
 * @property-read bool   $is_filter_inbound_domains
 * @property-read int    $default_filtering_policy_id
 * @property-read int    $bounce_filtering_policy_id
 *
 * @package ProvulusSDK\Client\Response\Organization\Outbound
 */
class OutboundResponse extends ProvulusBaseResponse
{

}
