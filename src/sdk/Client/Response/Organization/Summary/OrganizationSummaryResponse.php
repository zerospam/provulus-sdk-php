<?php
/**
 * Created by PhpStorm.
 * User: ycoutu
 * Date: 26/10/18
 * Time: 3:04 PM
 */

namespace ProvulusSDK\Client\Response\Organization\Summary;

use ProvulusSDK\Client\Response\ProvulusBaseResponse;
use ProvulusSDK\Enum\Organization\Type\OrganizationTypeEnum;
use ProvulusSDK\Utils\Str;

/**
 * Class OrganizationConnectionInfoResponse
 *
 * @property-read bool                    is_billing_organization
 * @property-read bool                    is_support_organization
 * @property-read string                  corporate_name
 * @property-read OrganizationTypeEnum    organization_type_enum
 * @property-read bool                    needs_to_fill_billing
 * @property-read bool                    is_owner
 * @property-read bool                    is_direct
 * @property-read bool                    is_distribution
 * @property-read bool                    can_update_dmb_count
 * @property-read bool                    has_ldap_configuration
 * @property-read bool                    has_cancellation_request
 * @property-read bool                    has_outbound_filtering
 * @property-read bool                    has_billing
 * @package ProvulusSDK\Client\Response\Organization\Connection
 */
class OrganizationSummaryResponse extends ProvulusBaseResponse
{
    /**
     * @return OrganizationTypeEnum
     */
    public function getOrganizationTypeEnumAttribute()
    {
        $orgType = $this->data['attributes']['organization_type_enum'];

        return OrganizationTypeEnum::byName(Str::upper($orgType));
    }
}
