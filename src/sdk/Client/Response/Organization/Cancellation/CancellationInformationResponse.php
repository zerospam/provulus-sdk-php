<?php

/**
 * Created by PhpStorm.
 * User: ycoutu
 * Date: 06/02/18
 * Time: 11:23 AM
 */

namespace ProvulusSDK\Client\Response\Organization\Cancellation;

use Carbon\Carbon;
use ProvulusSDK\Client\Response\ProvulusBaseResponse;

/**
 *
 * @property-read int         $organization_id
 * @property-read string      $reason
 * @property-read string      $from_display_name
 * @property-read string      $from_corporate_name
 * @property-read Carbon      $target_date
 * @property-read Carbon|null $executed_at
 * @property-read Carbon      $created_at
 * @property-read Carbon      $updated_at
 *
 */
class CancellationInformationResponse extends ProvulusBaseResponse
{
    public $dates = [
        'executed_at',
        'created_at',
        'updated_at',
        'target_date',
    ];
}
