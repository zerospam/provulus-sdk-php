<?php
/**
 * Created by PhpStorm.
 * User: pbb
 * Date: 14/12/16
 * Time: 3:59 PM
 */

namespace ProvulusSDK\Client\Response\Organization\Contact;

use Carbon\Carbon;
use ProvulusSDK\Client\Response\ProvulusBaseResponse;
use ProvulusSDK\Client\Response\Traits\IncludedResponseTrait;
use ProvulusSDK\Enum\Locale\LocaleEnum;
use ProvulusSDK\Utils\Str;

/**
 * Class ContactPersonCreationResponse
 *
 * Response to the Contact Person Creation Request
 *
 * @property-read string      $title
 * @property-read string      $email
 * @property-read int         $organization_id
 * @property-read string|null $phone
 * @property-read Carbon      $updated_at
 * @property-read Carbon      $created_at
 * @property-read LocaleEnum  $language
 * @property-read string[]    $mailgroups
 * @property-read string[]    $to_receive
 * @property-read bool        $is_default
 *
 * @package ProvulusSDK\Client\Response\Organization\Contact\Person
 */
class ContactPersonResponse extends ProvulusBaseResponse
{
    use IncludedResponseTrait;

    protected $dates = [
        'updated_at',
        'created_at'
    ];

    /**
     * Language mutator
     *
     * @return LocaleEnum|null
     */
    public function getLanguageAttribute()
    {
        $language = $this->data['attributes']['language'];

        return LocaleEnum::byName(Str::upper($language));
    }
}
