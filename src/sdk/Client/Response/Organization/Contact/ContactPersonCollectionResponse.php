<?php
/**
 * Created by PhpStorm.
 * User: aaflalo
 * Date: 23/01/17
 * Time: 3:32 PM
 */

namespace ProvulusSDK\Client\Response\Organization\Contact;

use Cake\Collection\Collection;
use ProvulusSDK\Client\Response\Collection\CollectionPaginatedResponse;

/**
 * Class ContactPersonCollection
 *
 * @method Collection|ContactPersonResponse[] data()
 * @package ProvulusSDK\Client\Response\Organization\Contact
 */
class ContactPersonCollectionResponse extends CollectionPaginatedResponse
{

}