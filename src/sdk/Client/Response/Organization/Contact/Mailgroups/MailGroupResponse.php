<?php
/**
 * Created by PhpStorm.
 * User: pbb
 * Date: 12/09/18
 * Time: 9:31 AM
 */

namespace ProvulusSDK\Client\Response\Organization\Contact\Mailgroups;

use ProvulusSDK\Client\Response\ProvulusBaseResponse;
use ProvulusSDK\Client\Response\Traits\IncludedResponseTrait;
use ProvulusSDK\Enum\Contact\Mailgroups\MailGroupEnum;

/**
 * Class MailgroupsResponse
 *
 * @property-read MailGroupEnum             $value
 * @property-read string                    $label
 * @property-read string                    $description
 * @property-read MailGroupCategoryResponse $category
 *
 * @package ProvulusSDK\Client\Response\Organization\Contact
 */
class MailGroupResponse extends ProvulusBaseResponse
{
    use IncludedResponseTrait;

    /**
     * get the cancellation information in the response
     *
     * @return MailGroupCategoryResponse
     */
    public function getCategoryAttribute()
    {
        $callback = function (array $data) {
            return new MailGroupCategoryResponse($data);
        };

        return $this->findRelatedItem('category', $callback);
    }

    /**
     * Value mutator
     *
     * @return MailGroupEnum
     */
    public function getValueAttribute()
    {
        return MailGroupEnum::get($this->data()['value']);
    }
}
