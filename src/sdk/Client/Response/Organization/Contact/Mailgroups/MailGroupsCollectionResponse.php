<?php
/**
 * Created by PhpStorm.
 * User: pbb
 * Date: 14/09/18
 * Time: 2:47 PM
 */

namespace ProvulusSDK\Client\Response\Organization\Contact\Mailgroups;


use Cake\Collection\Collection;
use ProvulusSDK\Client\Response\Collection\CollectionResponse;

/**
 * Class MailGroupsCollectionResponse
 *
 * @method Collection|MailGroupResponse[] data()
 *
 * @package ProvulusSDK\Client\Response\Organization\Contact\Mailgroups
 */
class MailGroupsCollectionResponse extends CollectionResponse
{
}