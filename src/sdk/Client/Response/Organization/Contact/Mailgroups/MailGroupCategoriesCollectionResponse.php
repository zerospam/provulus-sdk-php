<?php
/**
 * Created by PhpStorm.
 * User: pbb
 * Date: 13/09/18
 * Time: 10:32 AM
 */

namespace ProvulusSDK\Client\Response\Organization\Contact\Mailgroups;


use Cake\Collection\Collection;
use ProvulusSDK\Client\Response\Collection\CollectionResponse;

/**
 * Class MailgroupsCategoriesIndexRequest
 *
 * @method Collection|MailGroupCategoryResponse[] data()
 *
 * @package ProvulusSDK\Client\Request\Organization\Contact\Mailgroups
 */
class MailGroupCategoriesCollectionResponse extends CollectionResponse
{
}