<?php
/**
 * Created by PhpStorm.
 * User: pbb
 * Date: 13/09/18
 * Time: 10:25 AM
 */

namespace ProvulusSDK\Client\Response\Organization\Contact\Mailgroups;


use ProvulusSDK\Client\Response\ProvulusBaseResponse;
use ProvulusSDK\Enum\Contact\Mailgroups\MailGroupCategoriesEnum;

/**
 * Class MailGroupCategoryResponse
 *
 * @property-read MailGroupCategoriesEnum $category
 * @property-read string                  $label
 *
 * @package ProvulusSDK\Client\Response\Organization\Contact\Mailgroups
 */
class MailGroupCategoryResponse extends ProvulusBaseResponse
{
    /**
     * Category mutator
     *
     * @return MailGroupCategoriesEnum
     */
    public function getCategoryAttribute()
    {
        return MailGroupCategoriesEnum::get($this->data()['category']);
    }
}