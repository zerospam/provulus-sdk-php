<?php
/**
 * Created by PhpStorm.
 * User: aaflalo
 * Date: 17-09-13
 * Time: 15:05
 */

namespace ProvulusSDK\Client\Response\Organization\Logo;

use Cake\Collection\Collection;
use ProvulusSDK\Client\Response\ProvulusBaseResponse;
use ProvulusSDK\Utils\Maintenance\MaintenanceInformation;

/**
 * Class LogoResponse
 *
 * @property-read string                              $logo
 * @property-read string                              $name
 * @property-read bool                                $shows_powered_by_logo
 * @property-read bool                                $is_portal_selfprov_enabled
 * @property-read MaintenanceInformation|null         $maintenance
 * @property-read Collection|MaintenanceInformation[] $incidents
 * @package ProvulusSDK\Client\Response\Organization\Logo
 */
class PortalResponse extends ProvulusBaseResponse
{

    public function getMaintenanceAttribute()
    {
        if (!$this->data['maintenance']) {
            return null;
        }

        return MaintenanceInformation::fromArray($this->data['maintenance']);
    }

    public function getIncidentsAttribute()
    {
        return (new Collection($this->data['incidents']))
            ->map(
                function (array $item) {
                    return MaintenanceInformation::fromArray($item);
                }
            );
    }
}
