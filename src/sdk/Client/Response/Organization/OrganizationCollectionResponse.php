<?php
/**
 * Created by PhpStorm.
 * User: pbb
 * Date: 10/02/17
 * Time: 8:58 AM
 */

namespace ProvulusSDK\Client\Response\Organization;

use Cake\Collection\Collection;
use ProvulusSDK\Client\Response\Collection\CollectionPaginatedResponse;

/**
 * Class OrganizationCollectionResponse
 *
 * Represents a collection of organizations
 *
 * @method Collection|OrganizationResponse[] data()
 *
 * @package ProvulusSDK\Client\Response\Organization
 */
class OrganizationCollectionResponse extends CollectionPaginatedResponse
{

}