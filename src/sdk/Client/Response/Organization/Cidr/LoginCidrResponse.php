<?php
/**
 * Created by PhpStorm.
 * User: ycoutu
 * Date: 18/04/18
 * Time: 10:35 AM
 */

namespace ProvulusSDK\Client\Response\Organization\Cidr;

use ProvulusSDK\Client\Response\ProvulusBaseResponse;

/**
 * Class LoginCidrResponse
 *
 * @package ProvulusSDK\Client\Response\Organization\Cidr
 *
 * @property-read int                 $organization_id
 * @property-read string              $cidr
 * @property-read \Carbon\Carbon|null $created_at
 * @property-read \Carbon\Carbon|null $updated_at
 */
class LoginCidrResponse extends ProvulusBaseResponse
{
    public $dates = [
        'created_at',
        'updated_at',
    ];
}
