<?php
/**
 * Created by PhpStorm.
 * User: ycoutu
 * Date: 18/04/18
 * Time: 10:38 AM
 */

namespace ProvulusSDK\Client\Response\Organization\Cidr;

use Cake\Collection\Collection;
use ProvulusSDK\Client\Response\Collection\CollectionResponse;

/**
 * Class LoginCidrCollectionResponse
 *
 *
 * @method Collection|LoginCidrResponse[] data()
 * @property-read string[] $cidrs
 *
 * @package ProvulusSDK\Client\Response\Organization\Cidr
 */
class LoginCidrCollectionResponse extends CollectionResponse
{
    /**
     * Getter for ips
     *
     * @return array
     */
    public function getCidrsAttribute()
    {
        return $this->data()->map(
            function (LoginCidrResponse $response) {
                return $response->cidr;
            }
        )->toArray();
    }
}
