<?php
/**
 * Created by PhpStorm.
 * User: pbb
 * Date: 23/12/16
 * Time: 11:38 AM
 */

namespace ProvulusSDK\Client\Response\Organization\MX;

use ProvulusSDK\Client\Response\ProvulusBaseResponse;

/**
 * Class OrganizationMXResponse
 *
 * Response for MX requests
 *
 * @property-read int         organization_id
 * @property-read int         priority
 * @property-read string      name
 * @property-read string      created_at
 * @property-read string|null updated_at
 *
 * @package ProvulusSDK\Client\Response\Organization\MX
 */
class OrganizationMXResponse extends ProvulusBaseResponse
{

}