<?php
/**
 * Created by PhpStorm.
 * User: pbb
 * Date: 27/01/17
 * Time: 8:46 AM
 */

namespace ProvulusSDK\Client\Response\Organization\MX;

use Cake\Collection\Collection;
use ProvulusSDK\Client\Response\Collection\CollectionPaginatedResponse;

/**
 * Class OrganizationMXCollectionResponse
 *
 * Collection of MXResponse
 * @method Collection|OrganizationMXResponse[] data()
 *
 * @package ProvulusSDK\Client\Response\Organization\MX
 */
class OrganizationMXCollectionResponse extends CollectionPaginatedResponse
{

}