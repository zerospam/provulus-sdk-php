<?php
/**
 * Created by PhpStorm.
 * User: aaflalo
 * Date: 06/12/16
 * Time: 11:51 AM
 */

namespace ProvulusSDK\Client\Response;


use Carbon\Carbon;
use DateTime;
use ProvulusSDK\Client\Response\Traits\ResponseTrait;
use ProvulusSDK\Utils\Date\DateUtil;
use ProvulusSDK\Utils\Str;

abstract class ProvulusBaseResponse implements IProvulusObjectResponse
{

    use ResponseTrait;

    /**
     * @var object[]
     */
    private $objReplacementCache = [];
    /**
     * @var array
     */
    protected $data;

    /**
     * @var array
     */
    protected $included;

    /**
     * Dates to be transTyped from string to Carbon
     *
     * @var string[]
     */
    protected $dates = [];


    /**
     * UserCreationResponse constructor.
     *
     * @param array $data
     * @param array $included
     */
    public function __construct(array $data, array $included = [])
    {
        $this->data     = $data;
        $this->included = $included;

        $this->setRequestResponse();
    }

    /**
     * Id in the response
     *
     * @return int
     */
    public function id()
    {
        return $this->data['id'];
    }

    /**
     * Data contained in the response
     *
     * @return array
     */
    public function data()
    {
        return $this->data['attributes'];
    }

    public function type()
    {
        return $this->data['type'];
    }

    /**
     * Return value in attributes array of the response
     *
     * @param $key
     *
     * @return mixed
     */
    protected function getValue($key)
    {
        if (!array_key_exists($key, $this->data())) {
            throw new \InvalidArgumentException(sprintf('key [%s] is not present in attributes array.', $key));
        }

        return $this->data()[$key];
    }

    /**
     * Get a specific field
     *
     * @param string $field
     *
     * @return mixed|null
     */
    public function get($field)
    {

        $key = 'get' . Str::studly($field) . 'Attribute';

        //check if attribute transformer exists
        //Run it if exists and cache the result
        if (method_exists($this, $key)) {
            if (isset($this->objReplacementCache[$field])) {
                return $this->objReplacementCache[$field];
            }

            return $this->objReplacementCache[$field] = call_user_func([
                $this,
                $key,
            ]);
        }

        //Same as before but specific for dates
        if (isset($this->dates) && in_array($field, $this->dates)) {

            if (isset($this->objReplacementCache[$field])) {
                return $this->objReplacementCache[$field];
            }

            if (!isset($this->data['attributes'])) {
                return null;
            }

            if (is_null($this->data['attributes'][$field])) {
                return null;
            }

            if (!$dateTime
                = DateTime::createFromFormat(DateUtil::getZerospamDateFormat(), $this->data['attributes'][$field])
            ) {
                throw new \InvalidArgumentException('Date cannot be parsed');
            }

            return $this->objReplacementCache[$field] = Carbon::instance($dateTime);
        }

        if (isset($this->data['attributes'][$field])) {
            return $this->data['attributes'][$field];
        }

        if (isset($this->data[$field])) {
            return $this->data[$field];
        }

        return null;
    }


    function __isset($name)
    {
        $key = 'get' . Str::studly($name) . 'Attribute';

        return isset($this->data['attributes'][$name])
               || isset($this->data[$name])
               || method_exists($this, $key)
               || (isset($this->dates) && in_array($name, $this->dates));
    }


    function __get($name)
    {
        return $this->get($name);
    }
}