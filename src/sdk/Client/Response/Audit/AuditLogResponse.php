<?php
/**
 * Created by PhpStorm.
 * User: pbb
 * Date: 05/07/17
 * Time: 10:50 AM
 */

namespace ProvulusSDK\Client\Response\Audit;

use ProvulusSDK\Client\Response\ProvulusBaseResponse;
use ProvulusSDK\Enum\Audit\AuditedModelsEnum;
use ProvulusSDK\Enum\Audit\AuditLogTypeEnum;
use ProvulusSDK\Utils\Str;

/**
 * Class AuditLogResponse
 *
 * Data container class wrapping Audit logs JSON data received from API
 *
 *
 * @property-read $user_id         int|null
 * @property-read $real_user_id    int|null
 * @property-read $event           AuditLogTypeEnum
 * @property-read $auditable_id    int
 * @property-read $auditable_type  AuditedModelsEnum
 * @property-read $url             string|null
 * @property-read $ip_address      string
 * @property-read $organization_id int
 * @property-read $created_at      Carbon
 * @property-read $user_agent      string|null
 * @property-read $custom_message  array|string[]
 * @property-read $custom_fields   array|string[]
 * @property-read $elapsed_time    string|null
 * @property-read $label           string
 * @property-read $values          array
 *
 * @package ProvulusSDK\Client\Response\Audit
 */
class AuditLogResponse extends ProvulusBaseResponse
{
    protected $dates = ['created_at'];

    /**
     * @return AuditLogTypeEnum
     */
    public function getEventAttribute()
    {
        return AuditLogTypeEnum::byName(Str::upper($this->getValue('event')));
    }

    /**
     * @return AuditedModelsEnum
     */
    public function getAuditableTypeAttribute()
    {
        return AuditedModelsEnum::byName(Str::upper($this->getValue('auditable_type')));
    }
}