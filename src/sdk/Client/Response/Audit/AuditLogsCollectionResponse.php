<?php
/**
 * Created by PhpStorm.
 * User: pbb
 * Date: 05/07/17
 * Time: 10:50 AM
 */

namespace ProvulusSDK\Client\Response\Audit;

use Cake\Collection\Collection;
use ProvulusSDK\Client\Response\Collection\CollectionPaginatedResponse;

/**
 * Class AuditLogsCollectionResponse
 *
 * @method Collection|AuditLogResponse[] data()
 *
 * @package ProvulusSDK\Client\Response\Audit
 */
class AuditLogsCollectionResponse extends CollectionPaginatedResponse
{
}