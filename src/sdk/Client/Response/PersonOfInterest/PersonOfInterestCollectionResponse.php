<?php
/**
 * Created by PhpStorm.
 * User: pbb
 * Date: 17/10/17
 * Time: 3:22 PM
 */

namespace ProvulusSDK\Client\Response\PersonOfInterest;


use Cake\Collection\Collection;
use ProvulusSDK\Client\Response\Collection\CollectionPaginatedResponse;

/**
 * Class PersonOfInterestCollectionResponse
 *
 * Represents a collection of person of interest
 *
 * @method Collection|PersonOfInterestResponse[] data()
 *
 * @package ProvulusSDK\Client\Response\PersonOfInterest
 */
class PersonOfInterestCollectionResponse extends CollectionPaginatedResponse
{
}