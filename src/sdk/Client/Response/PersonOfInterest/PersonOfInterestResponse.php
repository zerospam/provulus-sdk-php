<?php
/**
 * Created by PhpStorm.
 * User: pbb
 * Date: 17/10/17
 * Time: 3:22 PM
 */

namespace ProvulusSDK\Client\Response\PersonOfInterest;

use Carbon\Carbon;
use ProvulusSDK\Client\Response\ProvulusBaseResponse;
use ProvulusSDK\Enum\PersonOfInterest\ActionUponMatchEnum;

/**
 * Class PersonOfInterestResponse
 *
 * @property-read string              $full_name
 * @property-read string              $full_name_normalized
 * @property-read int                 $organization_id
 * @property-read ActionUponMatchEnum $action_upon_match
 * @property-read Carbon              $updated_at
 * @property-read Carbon              $created_at
 *
 * @package ProvulusSDK\Client\Response\PersonOfInterest
 */
class PersonOfInterestResponse extends ProvulusBaseResponse
{
    protected $dates
        = [
            'created_at',
            'updated_at',
        ];

    /**
     * @return ActionUponMatchEnum
     */
    public function getActionUponMatchAttribute()
    {
        return ActionUponMatchEnum::byValue($this->data['attributes']['action_upon_match']);
    }
}