<?php
/**
 * Created by PhpStorm.
 * User: aaflalo
 * Date: 06/12/16
 * Time: 11:49 AM
 */

namespace ProvulusSDK\Client\Response\Client;


use ProvulusSDK\Client\Response\Domain\DomainResponse;
use ProvulusSDK\Client\Response\Organization\OrganizationResponse;
use ProvulusSDK\Client\Response\ProvulusBaseResponse;
use ProvulusSDK\Client\Response\Traits\IncludedResponseTrait;
use ProvulusSDK\Client\Response\User\UserResponse;

/**
 * Class ClientCreationResponse
 * @package ProvulusSDK\Client\Response\Client
 */
class ClientCreationResponse extends ProvulusBaseResponse
{

    use IncludedResponseTrait;

    /**
     * Domain created if presents
     *
     * @return null|DomainResponse
     */
    public function domain()
    {
        $callback = function (array $data) {
            return new DomainResponse($data);
        };

        $type = 'domain';

        return $this->findRelatedItem($type, $callback);

    }

    /**
     * User created if presents
     *
     * @return null|UserResponse
     */
    public function user()
    {
        $callback = function (array $data) {
            return new UserResponse($data);
        };

        $type = 'user';

        return $this->findRelatedItem($type, $callback);
    }

    /**
     * Organization created
     *
     * @return OrganizationResponse
     */
    public function organization()
    {
        $callback = function (array $data) {
            return new OrganizationResponse($data);
        };

        $type = 'organization';

        return $this->findRelatedItem($type, $callback);
    }
}