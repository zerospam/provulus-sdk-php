<?php
/**
 * Created by PhpStorm.
 * User: aaflalo
 * Date: 19/01/17
 * Time: 10:50 AM
 */

namespace ProvulusSDK\Client\Errors\Validation;


class ValidationError
{

    /**
     * @var string
     */
    private $message, $code, $value, $field;

    /**
     * @var string[]
     */
    private $enum;

    /**
     * ValidationError constructor.
     *
     * @param           $field
     * @param string    $message
     * @param string    $code
     * @param string    $value
     * @param \string[] $enum
     */
    private function __construct($field, $message, $code, $value, array $enum = [])
    {
        $this->message = $message;
        $this->code    = $code;
        $this->value   = $value;
        $this->enum    = $enum;
        $this->field   = $field;
    }

    /**
     * Array of string containing the error
     *
     * @param  string  $field
     * @param string[] $array
     *
     * @return ValidationError
     */
    public static function fromArray($field, array $array)
    {
        $enum = [];
        if (isset($array['enum'])) {
            $enum = $array['enum'];
        }

        return new self($field, $array['message'], $array['code'], $array['value'], $enum);
    }

    /**
     * Error message
     *
     * @return string
     */
    public function getMessage()
    {
        return $this->message;
    }

    /**
     * Code of the validator that triggered the validation error
     *
     * @return string
     */
    public function getCode()
    {
        return $this->code;
    }

    /**
     * Value that was given to the validator
     *
     * @return string
     */
    public function getValue()
    {
        return $this->value;
    }

    /**
     * In case it was an enumeration validator, contains the acceptable values.
     *
     * @return \string[]
     */
    public function getEnum()
    {
        return $this->enum;
    }

    /**
     * Field that triggered the validation error
     *
     * @return string
     */
    public function getField()
    {
        return $this->field;
    }




}