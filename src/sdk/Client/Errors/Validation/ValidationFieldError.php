<?php
/**
 * Created by PhpStorm.
 * User: aaflalo
 * Date: 19/01/17
 * Time: 11:08 AM
 */

namespace ProvulusSDK\Client\Errors\Validation;


use Cake\Collection\Collection;

class ValidationFieldError
{

    /**
     * @var Collection|ValidationError[]
     */
    private $validationErrorCollection;
    /**
     * @var string
     */
    private $field;

    /**
     * ValidationFieldError constructor.
     *
     * @param  string    $field
     * @param Collection $validationErrorCollection
     */
    public function __construct($field, Collection $validationErrorCollection)
    {
        $this->field                     = $field;
        $this->validationErrorCollection = $validationErrorCollection;
    }

    /**
     * Create the field from array
     *
     * @param  string $field
     * @param array   $errors
     *
     * @return ValidationFieldError
     */
    public static function fromArray($field, array $errors)
    {
        $arrayErrors = [];
        foreach ($errors as $error) {
            $arrayErrors[] = (ValidationError::fromArray($field, $error));
        }

        return new self($field, new Collection($arrayErrors));
    }

    /**
     * Getter for validationErrorCollection
     *
     * @return Collection|ValidationError[]
     */
    public function getValidationErrorCollection()
    {
        return $this->validationErrorCollection;
    }


    /**
     * Getter for field
     *
     * @return string
     */
    public function getField()
    {
        return $this->field;
    }


}