<?php
/**
 * Created by PhpStorm.
 * User: aaflalo
 * Date: 20/01/17
 * Time: 4:07 PM
 */

namespace ProvulusSDK\Client\Errors\Precondition;


class PreconditionError
{

    /**
     * @var string
     */
    private $code, $message;

    /**
     * @var int
     */
    private $objectId;

    /**
     * PreconditionError constructor.
     *
     * @param string $code
     * @param string $message
     * @param int    $objectId
     */
    public function __construct($code, $message, $objectId)
    {
        $this->code     = $code;
        $this->message  = $message;
        $this->objectId = $objectId;
    }

    /**
     * Build object from array
     *
     * @param string[] $array
     *
     * @return self
     */
    public static function fromArray(array $array) {
        return new self($array['code'], $array['message'], $array['obj_id']);
    }

    /**
     * Getter for code
     *
     * @return string
     */
    public function getCode()
    {
        return $this->code;
    }

    /**
     * Getter for message
     *
     * @return string
     */
    public function getMessage()
    {
        return $this->message;
    }

    /**
     * Getter for objectId
     *
     * @return int
     */
    public function getObjectId()
    {
        return $this->objectId;
    }




}