<?php
/**
 * Created by PhpStorm.
 * User: aaflalo
 * Date: 15/02/17
 * Time: 1:37 PM
 */

namespace ProvulusSDK\Client\Args\Relationship;


use ProvulusSDK\Client\Args\Merge\IMergeableArgument;
use ProvulusSDK\Client\Args\RequestArg;

/**
 * Class IncludeArg
 *
 * To include a relation into the results
 *
 * @package ProvulusSDK\Client\Args\Relationship
 */
class IncludeRelationArg extends RequestArg implements IMergeableArgument
{
    public function __construct($value)
    {
        if (!is_string($value)) {
            throw new \InvalidArgumentException('Only accepts string as arg');
        }
        parent::__construct('include', $value);
    }


    /**
     * Character used to glue the same args together
     *
     * @return string
     */
    public function glue()
    {
       return ';';
    }
}