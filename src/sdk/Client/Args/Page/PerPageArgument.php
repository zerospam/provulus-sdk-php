<?php
/**
 * Created by PhpStorm.
 * User: aaflalo
 * Date: 23/01/17
 * Time: 11:34 AM
 */

namespace ProvulusSDK\Client\Args\Page;


use ProvulusSDK\Client\Args\RequestArg;

class PerPageArgument extends RequestArg
{
    public function __construct($value)
    {
        if (!is_int($value)) {
            throw new \InvalidArgumentException('Per Page number need to be an int');
        }

        parent::__construct('perPage', $value);
    }

}