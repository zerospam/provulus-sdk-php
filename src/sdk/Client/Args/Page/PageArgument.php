<?php
/**
 * Created by PhpStorm.
 * User: aaflalo
 * Date: 09/01/17
 * Time: 1:18 PM
 */

namespace ProvulusSDK\Client\Args\Page;


use ProvulusSDK\Client\Args\RequestArg;

class PageArgument extends RequestArg
{
    public function __construct($value)
    {
        if (!is_int($value)) {
            throw new \InvalidArgumentException('Page number need to be an int');
        }

        parent::__construct('page', $value);
    }

}