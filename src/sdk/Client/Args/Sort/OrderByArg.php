<?php
/**
 * Created by PhpStorm.
 * User: aaflalo
 * Date: 16/02/17
 * Time: 10:44 AM
 */

namespace ProvulusSDK\Client\Args\Sort;


use ProvulusSDK\Client\Args\RequestArg;
use ProvulusSDK\Utils\Str;

class OrderByArg extends RequestArg
{
    public function __construct($value)
    {
        if (Str::contains($value, '|')) {
            $exploded    = explode('|', $value);
            $exploded[0] = Str::camel($exploded[0]);
            $value       = implode('|', $exploded);
        }
        parent::__construct('orderBy', $value);
    }

}