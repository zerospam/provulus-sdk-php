<?php
/**
 * Created by PhpStorm.
 * User: aaflalo
 * Date: 16/02/17
 * Time: 10:45 AM
 */

namespace ProvulusSDK\Client\Args\Sort;


use MabeEnum\Enum;
use ProvulusSDK\Client\Request\Resource\PrimalValued;
use ProvulusSDK\Client\Request\Resource\PrimalValuedEnumTrait;

/**
 * Class SortEnum
 *
 * @method static SortEnum ASCENDING()
 * @method static SortEnum DESCENDING()
 *
 * @package ProvulusSDK\Client\Args\Sort
 */
class SortEnum extends Enum implements PrimalValued
{
    use PrimalValuedEnumTrait;

    const ASCENDING  = 'ASC';
    const DESCENDING = 'DESC';
}