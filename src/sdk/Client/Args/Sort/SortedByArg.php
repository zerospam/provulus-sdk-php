<?php
/**
 * Created by PhpStorm.
 * User: aaflalo
 * Date: 16/02/17
 * Time: 10:45 AM
 */

namespace ProvulusSDK\Client\Args\Sort;


use ProvulusSDK\Client\Args\RequestArg;

class SortedByArg extends RequestArg
{
    public function __construct(SortEnum $value) { parent::__construct('sortedBy', $value); }

}