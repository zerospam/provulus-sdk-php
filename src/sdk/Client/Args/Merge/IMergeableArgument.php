<?php
/**
 * Created by PhpStorm.
 * User: aaflalo
 * Date: 15/02/17
 * Time: 1:47 PM
 */

namespace ProvulusSDK\Client\Args\Merge;


use ProvulusSDK\Client\Args\IRequestArg;

interface IMergeableArgument extends IRequestArg
{

    /**
     * Character used to glue the same args together
     *
     * @return string
     */
    public function glue();
}