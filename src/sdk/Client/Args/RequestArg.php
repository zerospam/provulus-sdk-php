<?php
/**
 * Created by PhpStorm.
 * User: aaflalo
 * Date: 09/01/17
 * Time: 11:42 AM
 */

namespace ProvulusSDK\Client\Args;


use ProvulusSDK\Client\Request\Resource\PrimalValued;

class RequestArg implements IRequestArg
{

    /**
     * @var string
     */
    private $key, $value;

    /**
     * RequestArg constructor.
     *
     * @param string $key
     * @param string $value
     */
    public function __construct($key, $value)
    {
        if (is_null($key)) {
            throw new \InvalidArgumentException("Key can't be null");
        }
        if (!is_string($key)) {
            throw new \InvalidArgumentException('The key need to be a string');
        }
        $this->key   = $key;
        $this->value = $value;
    }

    /**
     * Key for the argument
     *
     * @return string
     */
    public function getKey()
    {
        return $this->key;
    }


    function toPrimitive()
    {
        $value = $this->value;
        if ($value instanceof PrimalValued) {
            $value = $value->toPrimitive();
        }

        if (is_bool($value)) {
            return (int)$value;
        }

        if(is_float($value)) {
            return str_replace(',', '.', $value);
        }

        return $value;
    }
}