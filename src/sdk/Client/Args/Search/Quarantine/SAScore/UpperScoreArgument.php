<?php
/**
 * Created by PhpStorm.
 * User: pbb
 * Date: 15/02/17
 * Time: 9:48 AM
 */

namespace ProvulusSDK\Client\Args\Search\Quarantine\SAScore;

use ProvulusSDK\Client\Args\RequestArg;

/**
 * Class UpperScoreArgument
 *
 * Argument to filter quarantine mails with SA score <= specified criteria
 *
 * @package ProvulusSDK\Client\Args\Search\Quarantine\Score
 */
class UpperScoreArgument extends RequestArg
{

    /**
     * UpperScoreArgument constructor.
     *
     * @param float $upperScore
     */
    public function __construct($upperScore)
    {
        if (!is_float($upperScore)) {
            throw new \InvalidArgumentException('Lower Score argument needs to be of type float');
        }

        parent::__construct('upper_score', $upperScore);
    }
}