<?php
/**
 * Created by PhpStorm.
 * User: pbb
 * Date: 15/02/17
 * Time: 9:43 AM
 */

namespace ProvulusSDK\Client\Args\Search\Quarantine\SAScore;

use ProvulusSDK\Client\Args\RequestArg;

/**
 * Class LowerScoreArgument
 *
 * Argument to filter quarantine mails that have a SA score equal or larger than specified
 *
 * @package ProvulusSDK\Client\Args\Search\Quarantine\Score
 */
class LowerScoreArgument extends RequestArg
{
    /**
     * LowerScoreArgument constructor.
     *
     * @param float $lowerScore
     */
    public function __construct($lowerScore)
    {
        if (!is_float($lowerScore)) {
            throw new \InvalidArgumentException('Lower Score argument needs to be of type float');
        }

        parent::__construct('lower_score', $lowerScore);
    }
}