<?php
/**
 * Created by PhpStorm.
 * User: pbb
 * Date: 15/02/17
 * Time: 9:34 AM
 */

namespace ProvulusSDK\Client\Args\Search\Quarantine;

use ProvulusSDK\Client\Args\Merge\IMergeableArgument;
use ProvulusSDK\Client\Args\RequestArg;

/**
 * Class FromArgument
 *
 * Argument to filter quarantine mails based upon sender characteristics
 *
 * @package ProvulusSDK\Client\Args\Search\Quarantine
 */
class FromArgument extends RequestArg implements IMergeableArgument
{
    /**
     * FromArgument constructor.
     *
     * @param string $from
     */
    public function __construct($from)
    {
        if (!is_string($from)) {
            throw new \InvalidArgumentException('From argument needs to be of type string.');
        }

        parent::__construct('from', $from);
    }

    /**
     * Character used to glue the same args together
     *
     * @return string
     */
    public function glue()
    {
        return ';';
    }
}