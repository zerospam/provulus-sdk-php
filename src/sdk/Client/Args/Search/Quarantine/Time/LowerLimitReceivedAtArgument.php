<?php
/**
 * Created by PhpStorm.
 * User: pbb
 * Date: 15/02/17
 * Time: 9:39 AM
 */

namespace ProvulusSDK\Client\Args\Search\Quarantine\Time;

use ProvulusSDK\Client\Args\RequestArg;

/**
 * Class LowerLimitReceivedAtArgument
 *
 * Argument to filter quarantine mails that are more recent than specified criteria
 *
 * @package ProvulusSDK\Client\Args\Search\Quarantine\Time
 */
class LowerLimitReceivedAtArgument extends RequestArg
{

    /**
     * LowerLimitReceivedAtArgument constructor.
     *
     * @param string $lowerTime
     */
    public function __construct($lowerTime)
    {
        if (!strtotime($lowerTime)) {
            throw new \InvalidArgumentException('start time must be a string parseable by strtotime().');
        }

        parent::__construct('start', $lowerTime);
    }
}