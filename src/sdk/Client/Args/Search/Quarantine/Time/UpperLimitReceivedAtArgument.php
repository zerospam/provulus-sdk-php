<?php
/**
 * Created by PhpStorm.
 * User: pbb
 * Date: 15/02/17
 * Time: 9:41 AM
 */

namespace ProvulusSDK\Client\Args\Search\Quarantine\Time;

use ProvulusSDK\Client\Args\RequestArg;

/**
 * Class UpperLimitReceivedAtArgument
 *
 * Argument to filter quarantine mails that are more recent than specified criteria
 *
 * @package ProvulusSDK\Client\Args\Search\Quarantine\Time
 */
class UpperLimitReceivedAtArgument extends RequestArg
{
    /**
     * UpperLimitReceivedAtArgument constructor.
     *
     * @param string $upperTime
     */
    public function __construct($upperTime)
    {
        if (!strtotime($upperTime)) {
            throw new \InvalidArgumentException('stop time must be a string parseable by strtotime().');
        }

        parent::__construct('stop', $upperTime);
    }
}