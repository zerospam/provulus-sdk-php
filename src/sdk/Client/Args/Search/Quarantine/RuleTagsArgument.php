<?php
/**
 * Created by PhpStorm.
 * User: ycoutu
 * Date: 18/09/18
 * Time: 3:59 PM
 */

namespace ProvulusSDK\Client\Args\Search\Quarantine;

use ProvulusSDK\Client\Args\RequestArg;

/**
 * Class RuleTagsArgument
 *
 * Argument to filter the quarantine mails that hit the given SA rules
 *
 * @package ProvulusSDK\Client\Args\Search\Quarantine
 */
class RuleTagsArgument extends RequestArg
{
    /**
     * LowerLimitReceivedAtArgument constructor.
     *
     * @param string $tags
     */
    public function __construct($tags)
    {
        if (!is_string($tags)) {
            throw new \InvalidArgumentException('Rule tags argument needs to be of type string.');
        }
        parent::__construct('rule_tags', $tags);
    }
}
