<?php
/**
 * Created by PhpStorm.
 * User: pbb
 * Date: 15/02/17
 * Time: 9:57 AM
 */

namespace ProvulusSDK\Client\Args\Search\Quarantine\Flags\Status;

use ProvulusSDK\Client\Args\RequestArg;

/**
 * Class IncludeReleasedArgument
 *
 * Argument to filter quarantine mails based upon is_released value from database
 *
 * @package ProvulusSDK\Client\Args\Search\Quarantine\Flags
 */
class IsStatusReleasedArgument extends RequestArg
{

    /**
     * IncludeReleasedArgument constructor.
     *
     * @param bool $isStatusReleased
     */
    public function __construct($isStatusReleased)
    {
        if (!is_bool($isStatusReleased)) {
            throw new \InvalidArgumentException('Value for status released must be of type boolean');
        }

        parent::__construct('status_released', $isStatusReleased);
    }
}