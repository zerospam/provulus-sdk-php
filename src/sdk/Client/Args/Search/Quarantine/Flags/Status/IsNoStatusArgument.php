<?php
/**
 * Created by PhpStorm.
 * User: pbb
 * Date: 20/02/17
 * Time: 3:43 PM
 */

namespace ProvulusSDK\Client\Args\Search\Quarantine\Flags\Status;

use ProvulusSDK\Client\Args\RequestArg;

/**
 * Class NormalQuarantineArgument
 *
 * Argument to filter quarantine mails with default flags
 * i.e exclude deleted, released, declared as spam mails
 *
 * @package ProvulusSDK\Client\Args\Search\Quarantine\Flags
 */
class IsNoStatusArgument extends RequestArg
{

    /**
     * NormalQuarantineArgument constructor.
     *
     * @param bool $isNoStatus
     */
    public function __construct($isNoStatus)
    {
        if (!is_bool($isNoStatus)) {
            throw new \InvalidArgumentException('Value for no_status must be of type boolean');
        }

        parent::__construct('no_status', $isNoStatus);
    }
}