<?php
/**
 * Created by PhpStorm.
 * User: pbb
 * Date: 15/02/17
 * Time: 10:14 AM
 */

namespace ProvulusSDK\Client\Args\Search\Quarantine\Flags\Status;

use ProvulusSDK\Client\Args\RequestArg;

/**
 * Class IncludeDeclaredAsSpamArgument
 *
 * Argument to filter quarantine mails based upon is_declared_spam value from database
 *
 * @package ProvulusSDK\Client\Args\Search\Quarantine\Flags
 */
class IsStatusDeclaredAsSpamArgument extends RequestArg
{

    /**
     * IncludeDeclaredAsSpamArgument constructor.
     *
     * @param bool $isStatusDeclaredSpam
     */
    public function __construct($isStatusDeclaredSpam)
    {
        if (!is_bool($isStatusDeclaredSpam)) {
            throw new \InvalidArgumentException('Value for status declared as spam must be of type boolean');
        }

        parent::__construct('status_declared_spam', $isStatusDeclaredSpam);
    }
}