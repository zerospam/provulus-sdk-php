<?php
/**
 * Created by PhpStorm.
 * User: pbb
 * Date: 15/02/17
 * Time: 9:53 AM
 */

namespace ProvulusSDK\Client\Args\Search\Quarantine\Flags\Status;

use ProvulusSDK\Client\Args\RequestArg;

/**
 * Class IncludeDeletedArgument
 *
 * Argument to filter quarantine mails based upon is_deleted value from database
 *
 * @package ProvulusSDK\Client\Args\Search\Quarantine\Flags
 */
class IsStatusDeletedArgument extends RequestArg
{

    /**
     * IncludeDeletedArgument constructor.
     *
     * @param bool $isStatusDeleted
     */
    public function __construct($isStatusDeleted)
    {
        if (!is_bool($isStatusDeleted)) {
            throw new \InvalidArgumentException('Value for status deleted must be of type boolean');
        }

        parent::__construct('status_deleted', $isStatusDeleted);
    }
}