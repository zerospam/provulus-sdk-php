<?php
/**
 * Created by PhpStorm.
 * User: ycoutu
 * Date: 29/01/18
 * Time: 11:19 AM
 */

namespace ProvulusSDK\Client\Args\Search\Quarantine\Flags\Tags;

use ProvulusSDK\Client\Args\RequestArg;

class IsNoTagArgument extends RequestArg
{
    /**
     * IsNoTagArgument constructor.
     *
     * @param bool $isNoTag
     */
    public function __construct($isNoTag)
    {
        if (!is_bool($isNoTag)) {
            throw new \InvalidArgumentException('Value for no tag must be of type boolean');
        }

        parent::__construct('no_tag', $isNoTag);
    }
}
