<?php
/**
 * Created by PhpStorm.
 * User: pbb
 * Date: 22/01/18
 * Time: 10:09 AM
 */

namespace ProvulusSDK\Client\Args\Search\Quarantine\Flags\Tags;


use ProvulusSDK\Client\Args\RequestArg;

/**
 * Class IncludeGreymail
 *
 * Request Argument indicating if greymail tagged emails should be included in the response
 *
 * @package ProvulusSDK\Client\Args\Search\Quarantine\Flags
 */
class IsTagGreymail extends RequestArg
{

    /**
     * IncludeGreymail constructor.
     *
     * @param string $isTagGreymail
     */
    public function __construct($isTagGreymail)
    {
        if (!is_bool($isTagGreymail)) {
            throw new \InvalidArgumentException('Value for tag greymail must be of type boolean');
        }

        parent::__construct('tag_greymail', $isTagGreymail);
    }

}