<?php
/**
 * Created by PhpStorm.
 * User: ycoutu
 * Date: 07/12/17
 * Time: 11:29 AM
 */

namespace ProvulusSDK\Client\Args\Search\Quarantine\Flags\Tags;

use ProvulusSDK\Client\Args\RequestArg;

class IsTagBannedFile extends RequestArg
{
    /**
     * IsBannedArgument constructor.
     *
     * @param bool $isTagBannedFile
     */
    public function __construct($isTagBannedFile)
    {
        if (!is_bool($isTagBannedFile)) {
            throw new \InvalidArgumentException('Value for tag banned file must be of type boolean');
        }

        parent::__construct('tag_banned_file', $isTagBannedFile);
    }
}
