<?php
/**
 * Created by PhpStorm.
 * User: ycoutu
 * Date: 07/12/17
 * Time: 11:29 AM
 */

namespace ProvulusSDK\Client\Args\Search\Quarantine\Flags\Tags;

use ProvulusSDK\Client\Args\RequestArg;

class IsTagPhishArgument extends RequestArg
{
    /**
     * IsPhishArgument constructor.
     *
     * @param bool $isTagPhish
     */
    public function __construct($isTagPhish)
    {
        if (!is_bool($isTagPhish)) {
            throw new \InvalidArgumentException('Value for tag phish must be of type boolean');
        }

        parent::__construct('tag_phish', $isTagPhish);
    }
}
