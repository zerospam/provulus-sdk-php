<?php
/**
 * Created by PhpStorm.
 * User: pbb
 * Date: 15/02/17
 * Time: 9:35 AM
 */

namespace ProvulusSDK\Client\Args\Search\Quarantine;

use ProvulusSDK\Client\Args\RequestArg;

/**
 * Class SubjectArgument
 *
 * Argument to filter quarantine mails based upon subject characteristics
 *
 * @package ProvulusSDK\Client\Args\Search\Quarantine
 */
class SubjectArgument extends RequestArg
{

    /**
     * SubjectArgument constructor.
     *
     * @param string $subject
     */
    public function __construct($subject)
    {
        if (!is_string($subject)) {
            throw new \InvalidArgumentException('Subject argument needs to be of type string.');
        }

        parent::__construct('subject', $subject);
    }
}