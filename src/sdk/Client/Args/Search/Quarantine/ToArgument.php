<?php
/**
 * Created by PhpStorm.
 * User: pbb
 * Date: 15/02/17
 * Time: 9:27 AM
 */

namespace ProvulusSDK\Client\Args\Search\Quarantine;


use ProvulusSDK\Client\Args\Merge\IMergeableArgument;
use ProvulusSDK\Client\Args\RequestArg;

/**
 * Class ToArgument
 *
 * Argument to filter quarantine mails based upon recipient characteristics
 *
 * @package ProvulusSDK\Client\Args\Search\Quarantine
 */
class ToArgument extends RequestArg implements IMergeableArgument
{
    /**
     * ToArgument constructor.
     *
     * @param string $to
     */
    public function __construct($to)
    {
        if (!is_string($to)) {
            throw new \InvalidArgumentException('To argument needs to be of type string.');
        }

        parent::__construct('to', $to);
    }

    /**
     * Character used to glue the same args together
     *
     * @return string
     */
    public function glue()
    {
        return ';';
    }
}