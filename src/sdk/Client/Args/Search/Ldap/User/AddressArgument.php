<?php
/**
 * Created by PhpStorm.
 * User: ycoutu
 * Date: 14/06/17
 * Time: 3:05 PM
 */

namespace ProvulusSDK\Client\Args\Search\Ldap\User;

use ProvulusSDK\Client\Args\RequestArg;

/**
 * Address for the LdapUser search
 *
 * Class AddressArgument
 *
 * @package ProvulusSDK\Client\Args\Search\Ldap\User
 */
class AddressArgument extends RequestArg
{
    /**
     * AddressArgument constructor.
     *
     * @param string $value
     */
    public function __construct($value)
    {
        if (!is_string($value)) {
            throw new \InvalidArgumentException(sprintf('Address needs to be of type string.'));
        }

        parent::__construct('address', $value);
    }
}
