<?php
/**
 * Created by PhpStorm.
 * User: ycoutu
 * Date: 14/06/17
 * Time: 3:04 PM
 */

namespace ProvulusSDK\Client\Args\Search\Ldap\User;

use ProvulusSDK\Client\Args\RequestArg;

/**
 * Surname for the LdapUser search
 *
 * Class SurnameArgument
 *
 * @package ProvulusSDK\Client\Args\Search\Ldap\User
 */
class LastNameArgument extends RequestArg
{
    /**
     * SurnameArgument constructor.
     *
     * @param string $value
     */
    public function __construct($value)
    {
        if (!is_string($value)) {
            throw new \InvalidArgumentException(sprintf('Surname needs to be of type string.'));
        }

        parent::__construct('last_name', $value);
    }
}
