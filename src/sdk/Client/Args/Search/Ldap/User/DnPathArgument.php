<?php
/**
 * Created by PhpStorm.
 * User: ycoutu
 * Date: 14/06/17
 * Time: 3:04 PM
 */

namespace ProvulusSDK\Client\Args\Search\Ldap\User;

use ProvulusSDK\Client\Args\RequestArg;

/**
 * Dn path for the LdapUser search
 *
 * Class DnPathArgument
 *
 * @package ProvulusSDK\Client\Args\Search\Ldap\User
 */
class DnPathArgument extends RequestArg
{
    /**
     * DnPathArgument constructor.
     *
     * @param string $value
     */
    public function __construct($value)
    {
        if (!is_string($value)) {
            throw new \InvalidArgumentException(sprintf('Dn path needs to be of type string.'));
        }

        parent::__construct('dn_path', $value);
    }
}
