<?php
/**
 * Created by PhpStorm.
 * User: ycoutu
 * Date: 14/06/17
 * Time: 3:04 PM
 */

namespace ProvulusSDK\Client\Args\Search\Ldap\User;

use ProvulusSDK\Client\Args\RequestArg;

/**
 * Name for the LdapUser search
 *
 * Class NameArgument
 *
 * @package ProvulusSDK\Client\Args\Search\Ldap\User
 */
class FirstNameArgument extends RequestArg
{
    /**
     * NameArgument constructor.
     *
     * @param string $value
     */
    public function __construct($value)
    {
        if (!is_string($value)) {
            throw new \InvalidArgumentException(sprintf('Name needs to be of type string.'));
        }

        parent::__construct('first_name', $value);
    }
}
