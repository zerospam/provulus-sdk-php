<?php
/**
 * Created by PhpStorm.
 * User: pbb
 * Date: 26/06/17
 * Time: 3:52 PM
 */

namespace ProvulusSDK\Client\Args\Search\Ldap\Synchronizations;

use ProvulusSDK\Client\Args\Merge\IMergeableArgument;
use ProvulusSDK\Client\Args\RequestArg;
use ProvulusSDK\Enum\Ldap\LdapSyncStatusEnum;

/**
 * Class SyncStatusArgument
 *
 * Query string argument for filtering ldap syncs by ldap sync status
 *
 * @package ProvulusSDK\Client\Args\Search\Ldap\Synchronizations
 */
class SyncStatusArgument extends RequestArg implements IMergeableArgument
{

    /**
     * AddressArgument constructor.
     *
     * @param LdapSyncStatusEnum $value
     */
    public function __construct(LdapSyncStatusEnum $value)
    {
        parent::__construct('status', $value->toPrimitive());
    }

    /**
     * Character used to glue the same args together
     *
     * @return string
     */
    public function glue()
    {
        return ';';
    }
}