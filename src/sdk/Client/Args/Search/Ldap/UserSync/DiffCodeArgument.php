<?php
/**
 * Created by PhpStorm.
 * User: ycoutu
 * Date: 28/06/17
 * Time: 11:55 AM
 */

namespace ProvulusSDK\Client\Args\Search\Ldap\UserSync;

use ProvulusSDK\Client\Args\RequestArg;
use ProvulusSDK\Enum\Ldap\LdapDifferentialEnum;

class DiffCodeArgument extends RequestArg
{
    /**
     * DiffCodeArgument constructor.
     *
     * @param LdapDifferentialEnum $value
     */
    public function __construct(LdapDifferentialEnum $value)
    {
        parent::__construct('diff_code', $value->getValue());
    }
}
