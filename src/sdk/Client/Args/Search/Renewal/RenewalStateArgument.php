<?php
/**
 * Created by PhpStorm.
 * User: pbb
 * Date: 21/02/17
 * Time: 3:07 PM
 */

namespace ProvulusSDK\Client\Args\Search\Renewal;

use ProvulusSDK\Client\Args\RequestArg;
use ProvulusSDK\Enum\Renewal\PaymentRenewalStateEnum;

/**
 * Class RenewalStateArgument
 *
 * Request Argument specifying organization Renewal state
 *
 * @package ProvulusSDK\Client\Args\Search\Renewal
 */
class RenewalStateArgument extends RequestArg
{

    /**
     * RenewalStateArgument constructor.
     *
     * @param PaymentRenewalStateEnum $enum
     *
     */
    public function __construct(PaymentRenewalStateEnum $enum)
    {
        parent::__construct('renewal_state', $enum->getValue());
    }

}