<?php
/**
 * Created by PhpStorm.
 * User: ycoutu
 * Date: 04/03/19
 * Time: 3:49 PM
 */

namespace ProvulusSDK\Client\Args\Search\Domain\Mailbox;

use function is_integer;
use ProvulusSDK\Client\Args\RequestArg;

class MinimumMessageCountArgument extends RequestArg
{
    public function __construct($value)
    {
        if (!is_integer($value)) {
            throw new \InvalidArgumentException(sprintf('Minimum message count needs to be of type integer.'));
        }

        parent::__construct('min_msg_count', $value);
    }
}
