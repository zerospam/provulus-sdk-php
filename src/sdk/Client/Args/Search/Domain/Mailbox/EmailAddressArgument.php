<?php
/**
 * Created by PhpStorm.
 * User: ycoutu
 * Date: 04/03/19
 * Time: 3:48 PM
 */

namespace ProvulusSDK\Client\Args\Search\Domain\Mailbox;

use ProvulusSDK\Client\Args\RequestArg;

class EmailAddressArgument extends RequestArg
{
    /**
     * EmailAddressArgument constructor.
     * @param string $value
     */
    public function __construct($value)
    {
        if (!is_string($value)) {
            throw new \InvalidArgumentException(sprintf('Email Address needs to be of type string.'));
        }

        parent::__construct('email_address', $value);
    }
}
