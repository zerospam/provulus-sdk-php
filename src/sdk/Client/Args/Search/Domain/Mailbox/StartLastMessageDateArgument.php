<?php
/**
 * Created by PhpStorm.
 * User: ycoutu
 * Date: 04/03/19
 * Time: 3:51 PM
 */

namespace ProvulusSDK\Client\Args\Search\Domain\Mailbox;

use ProvulusSDK\Client\Args\RequestArg;

class StartLastMessageDateArgument extends RequestArg
{
    public function __construct($value)
    {
        if (!strtotime($value)) {
            throw new \InvalidArgumentException('Start Last Message Date must be a string parseable by strtotime().');
        }

        parent::__construct('start_last_message', $value);
    }
}
