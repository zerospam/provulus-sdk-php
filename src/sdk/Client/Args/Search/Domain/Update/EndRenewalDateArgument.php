<?php
/**
 * Created by PhpStorm.
 * User: ycoutu
 * Date: 08/03/19
 * Time: 2:43 PM
 */

namespace ProvulusSDK\Client\Args\Search\Domain\Update;

use ProvulusSDK\Client\Args\RequestArg;
use ProvulusSDK\Client\Args\Search\Common\CheckIfDateOrThrowTrait;

class EndRenewalDateArgument extends RequestArg
{
    use CheckIfDateOrThrowTrait;

    public function __construct($value)
    {
        $this->checkIfDateOrThrow($value);
        parent::__construct('end_renewal_date', $value);
    }
}
