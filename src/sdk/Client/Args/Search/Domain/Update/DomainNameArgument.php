<?php
/**
 * Created by PhpStorm.
 * User: ycoutu
 * Date: 08/03/19
 * Time: 2:34 PM
 */

namespace ProvulusSDK\Client\Args\Search\Domain\Update;

use ProvulusSDK\Client\Args\RequestArg;
use ProvulusSDK\Client\Args\Search\Common\CheckIfStringOrThrowTrait;

class DomainNameArgument extends RequestArg
{
    use CheckIfStringOrThrowTrait;

    /**
     * @param string $value
     */
    public function __construct($value)
    {
        $this->checkIfStringOrThrow($value);
        parent::__construct('domain_name', $value);
    }
}
