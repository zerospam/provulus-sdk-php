<?php
/**
 * Created by PhpStorm.
 * User: aaflalo
 * Date: 17-11-03
 * Time: 15:14
 */

namespace ProvulusSDK\Client\Args\Search\Domain;

use ProvulusSDK\Client\Args\RequestArg;

class DeletableArgument extends RequestArg
{
    public function __construct($value)
    {
        if (!is_bool($value)) {
            throw new \InvalidArgumentException('Value need to be a boolean');
        }
        parent::__construct('with_deletable', $value);
    }
}
