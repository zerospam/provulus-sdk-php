<?php
/**
 * Created by PhpStorm.
 * User: ycoutu
 * Date: 30/08/17
 * Time: 10:18 AM
 */

namespace ProvulusSDK\Client\Args\Search\Domain\Filtering\FilteringList;

use ProvulusSDK\Client\Args\RequestArg;
use ProvulusSDK\Enum\Filtering\FilteringListActionEnum;

class FilteringListActionArgument extends RequestArg
{
    /**
     * FilteringListActionArgument constructor.
     *
     * @param FilteringListActionEnum $value
     */
    public function __construct(FilteringListActionEnum $value)
    {
        parent::__construct('filtering_list_action', $value->getValue());
    }
}
