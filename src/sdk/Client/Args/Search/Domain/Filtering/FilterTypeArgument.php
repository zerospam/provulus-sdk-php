<?php
/**
 * Created by PhpStorm.
 * User: ycoutu
 * Date: 06/07/17
 * Time: 9:27 AM
 */

namespace ProvulusSDK\Client\Args\Search\Domain\Filtering;

use ProvulusSDK\Client\Args\RequestArg;
use ProvulusSDK\Enum\Filtering\FilteringPolicyEnum;

class FilterTypeArgument extends RequestArg
{
    /**
     * FilterTypeArgument constructor.
     *
     * @param FilteringPolicyEnum $value
     */
    public function __construct(FilteringPolicyEnum $value)
    {
        parent::__construct('filter_type', $value->getValue());
    }
}
