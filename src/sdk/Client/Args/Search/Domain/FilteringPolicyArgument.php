<?php
/**
 * Created by PhpStorm.
 * User: ycoutu
 * Date: 12/07/17
 * Time: 10:30 AM
 */

namespace ProvulusSDK\Client\Args\Search\Domain;

use ProvulusSDK\Client\Args\RequestArg;

class FilteringPolicyArgument extends RequestArg
{
    /**
     * FilteringPolicyArgument constructor.
     *
     * @param int $value
     */
    public function __construct($value)
    {
        parent::__construct('filtering_policy', $value);
    }
}
