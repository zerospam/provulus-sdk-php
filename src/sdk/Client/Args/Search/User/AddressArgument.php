<?php
/**
 * Created by PhpStorm.
 * User: pbb
 * Date: 17/08/17
 * Time: 1:15 PM
 */

namespace ProvulusSDK\Client\Args\Search\User;

use ProvulusSDK\Client\Args\RequestArg;
use ProvulusSDK\Client\Args\Search\Common\CheckIfStringOrThrowTrait;

/**
 * Class AddressArgument
 *
 * @package ProvulusSDK\Client\Args\Search\User
 */
class AddressArgument extends RequestArg
{

    use CheckIfStringOrThrowTrait;

    /**
     * AddressArgument constructor.
     *
     * @param string $value
     */
    public function __construct($value)
    {
        $this->checkIfStringOrThrow($value);

        parent::__construct('address', $value);
    }
}