<?php
/**
 * Created by PhpStorm.
 * User: pbb
 * Date: 05/09/17
 * Time: 9:27 AM
 */

namespace ProvulusSDK\Client\Args\Search\User;

use ProvulusSDK\Client\Args\RequestArg;
use ProvulusSDK\Client\Args\Search\Common\CheckIfBooleanOrThrowTrait;

/**
 * Class IncludeLdapArgument
 *
 * Argument added to the request to assess if we want to include ldap users or not
 *
 * @package ProvulusSDK\Client\Args\Search\User
 */
class IncludeLdapArgument extends RequestArg
{
    use CheckIfBooleanOrThrowTrait;

    /**
     * IncludeLdapArgument constructor.
     *
     * @param boolean $value
     */
    public function __construct($value)
    {
        $this->checkIfBooleanOrThrow($value);

        parent::__construct('include_ldap', $value);
    }
}