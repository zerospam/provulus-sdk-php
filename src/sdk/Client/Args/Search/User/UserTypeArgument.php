<?php
/**
 * Created by PhpStorm.
 * User: pbb
 * Date: 17/08/17
 * Time: 1:21 PM
 */

namespace ProvulusSDK\Client\Args\Search\User;

use ProvulusSDK\Client\Args\RequestArg;
use ProvulusSDK\Enum\User\UserTypeEnum;

/**
 * Class UserTypeArgument
 *
 * @package ProvulusSDK\Client\Args\Search\User
 */
class UserTypeArgument extends RequestArg
{
    /**
     * UserTypeArgument constructor.
     *
     * @param UserTypeEnum $enum
     */
    public function __construct(UserTypeEnum $enum)
    {
        parent::__construct('user_type', $enum);
    }
}