<?php
/**
 * Created by PhpStorm.
 * User: pbb
 * Date: 17/08/17
 * Time: 1:10 PM
 */

namespace ProvulusSDK\Client\Args\Search\User;

use ProvulusSDK\Client\Args\RequestArg;
use ProvulusSDK\Client\Args\Search\Common\CheckIfStringOrThrowTrait;

/**
 * Class DisplayNameArgument
 *
 * @package ProvulusSDK\Client\Args\Search\User
 */
class DisplayNameArgument extends RequestArg
{
    use CheckIfStringOrThrowTrait;

    /**
     * DisplayNameArgument constructor.
     *
     * @param string $value
     */
    public function __construct($value)
    {
        $this->checkIfStringOrThrow($value);

        parent::__construct('display_name', $value);
    }
}