<?php
/**
 * Created by PhpStorm.
 * User: pbb
 * Date: 17/08/17
 * Time: 1:32 PM
 */

namespace ProvulusSDK\Client\Args\Search\User;

use ProvulusSDK\Enum\User\UserTypeEnum;

/**
 * Trait UserSearchTrait
 *
 * Methods used for user filtering
 *
 * @package ProvulusSDK\Client\Args\Search\User
 */
trait UserSearchTrait
{
    /**
     * @param string|null $username
     *
     * @return $this
     */
    public function setUsernameArgument($username = null)
    {
        if (is_null($username)) {
            return $this->removeArgument(new UsernameArgument('null username'));
        }

        return $this->addArgument(new UsernameArgument($username));
    }

    /**
     * @param string|null $name
     *
     * @return $this
     */
    public function setDisplayNameArgument($name = null)
    {
        if (is_null($name)) {
            return $this->removeArgument(new DisplayNameArgument('null display name'));
        }

        return $this->addArgument(new DisplayNameArgument($name));
    }

    /**
     * @param string|null $address
     *
     * @return $this
     */
    public function setAddressArgument($address = null)
    {
        if (is_null($address)) {
            return $this->removeArgument(new AddressArgument('null address'));
        }

        return $this->addArgument(new AddressArgument($address));
    }

    /**
     * @param UserTypeEnum|null $userType
     *
     * @return $this
     */
    public function setUserTypeArgument($userType = null)
    {
        if (is_null($userType)) {
            return $this->removeArgument(new UserTypeArgument(UserTypeEnum::REGULAR()));
        }

        return $this->addArgument(new UserTypeArgument($userType));
    }

    /**
     * @param null|bool $include
     *
     * @return $this
     */
    public function setIncludeLdapArgument($include = null)
    {
        if (is_null(($include))) {
            return $this->removeArgument(new IncludeLdapArgument(false));
        }

        return $this->addArgument(new IncludeLdapArgument($include));
    }
}