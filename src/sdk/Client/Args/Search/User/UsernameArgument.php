<?php
/**
 * Created by PhpStorm.
 * User: pbb
 * Date: 17/08/17
 * Time: 1:06 PM
 */

namespace ProvulusSDK\Client\Args\Search\User;


use ProvulusSDK\Client\Args\RequestArg;
use ProvulusSDK\Client\Args\Search\Common\CheckIfStringOrThrowTrait;

/**
 * Class UsernameArgument
 *
 * Filters users by username
 *
 * @package ProvulusSDK\Client\Args\Search\User
 */
class UsernameArgument extends RequestArg
{
    use CheckIfStringOrThrowTrait;

    /**
     * UsernameArgument constructor.
     *
     * @param string $value
     */
    public function __construct($value)
    {
        $this->checkIfStringOrThrow($value);

        parent::__construct('username', $value);
    }
}