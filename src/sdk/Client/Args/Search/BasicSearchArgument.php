<?php
/**
 * Created by PhpStorm.
 * User: aaflalo
 * Date: 09/01/17
 * Time: 11:45 AM
 */

namespace ProvulusSDK\Client\Args\Search;


use ProvulusSDK\Client\Args\RequestArg;

class BasicSearchArgument extends RequestArg
{
    public function __construct($value)
    {
        parent::__construct('search', $value);
    }


}