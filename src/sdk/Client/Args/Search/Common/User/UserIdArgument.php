<?php
/**
 * Created by PhpStorm.
 * User: pbb
 * Date: 05/07/17
 * Time: 2:15 PM
 */

namespace ProvulusSDK\Client\Args\Search\Common\User;


use ProvulusSDK\Client\Args\RequestArg;

/**
 * Class UserIdArgument
 *
 * Request Argument for filtering data by user Id
 *
 * @package ProvulusSDK\Client\Args\Search\Common
 */
class UserIdArgument extends RequestArg
{
    /**
     * UserIdArgument constructor.
     *
     * @param mixed $id
     */
    function __construct($id)
    {
        if (!is_numeric($id)) {
            throw new \InvalidArgumentException(sprintf('%s id parameter must be numeric.'));
        }

        parent::__construct('user_id', (int)$id);
    }
}