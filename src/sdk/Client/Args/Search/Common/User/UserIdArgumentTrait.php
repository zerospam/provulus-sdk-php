<?php
/**
 * Created by PhpStorm.
 * User: pbb
 * Date: 05/07/17
 * Time: 2:25 PM
 */

namespace ProvulusSDK\Client\Args\Search\Common\User;

/**
 * Trait UserIdArgumentTrait
 *
 * Trait for setting user id request parameter
 *
 * @package ProvulusSDK\Client\Args\Search\Common\User
 */
trait UserIdArgumentTrait
{
    /**
     * Sets user id request argument
     * If $is = null (default), removes the argument
     *
     * @param null|mixed $id
     *
     * @return $this
     */
    public function setUserIdArgument($id = null)
    {
        if (is_null($id)) {
            return $this->removeArgument(new UserIdArgument(1));
        }

        return $this->addArgument(new UserIdArgument($id));
    }
}