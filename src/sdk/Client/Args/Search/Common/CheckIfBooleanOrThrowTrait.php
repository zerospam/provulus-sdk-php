<?php
/**
 * Created by PhpStorm.
 * User: pbb
 * Date: 05/09/17
 * Time: 9:29 AM
 */

namespace ProvulusSDK\Client\Args\Search\Common;

/**
 * Trait CheckIfBooleanOrThrowTrait
 *
 * Handles boolean type validation
 *
 * @package ProvulusSDK\Client\Args\Search\Common
 */
trait CheckIfBooleanOrThrowTrait
{
    /**
     * @param string $value
     */
    private function checkIfBooleanOrThrow($value)
    {
        if (!is_bool($value)) {
            throw new \InvalidArgumentException(sprintf('%s must receive a boolean value.', get_class($this)));
        }
    }
}