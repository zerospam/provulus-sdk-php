<?php
/**
 * Created by PhpStorm.
 * User: pbb
 * Date: 17/08/17
 * Time: 1:16 PM
 */

namespace ProvulusSDK\Client\Args\Search\Common;

/**
 * Class CheckIfStringOrThrowTrait
 *
 * Handles string validation
 *
 * @throws \InvalidArgumentException
 *
 * @package ProvulusSDK\Client\Args\Search\User
 */
trait CheckIfStringOrThrowTrait
{
    /**
     * @param string $value
     */
    private function checkIfStringOrThrow($value)
    {
        if (!is_string($value)) {
            throw new \InvalidArgumentException(sprintf('%s must receive a string value.', get_class($this)));
        }
    }
}