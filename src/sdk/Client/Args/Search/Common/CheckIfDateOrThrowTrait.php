<?php
/**
 * Created by PhpStorm.
 * User: ycoutu
 * Date: 08/03/19
 * Time: 2:44 PM
 */

namespace ProvulusSDK\Client\Args\Search\Common;

trait CheckIfDateOrThrowTrait
{
    /**
     * @param string $value
     */
    private function checkIfDateOrThrow($value)
    {
        if (!strtotime($value)) {
            throw new \InvalidArgumentException(
                sprintf(
                    '%s date must be parseable by strtotime().',
                    get_called_class()
                )
            );
        }
    }
}
