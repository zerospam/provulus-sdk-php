<?php
/**
 * Created by PhpStorm.
 * User: pbb
 * Date: 05/07/17
 * Time: 11:51 AM
 */

namespace ProvulusSDK\Client\Args\Search\Common\Date;


use ProvulusSDK\Client\Args\RequestArg;

/**
 * Class StartDateArgument
 *
 * Argument for start date criteria filtering
 *
 * @package ProvulusSDK\Client\Args\Search\Common
 */
class StartDateArgument extends RequestArg
{
    /**
     * StartDateArgument constructor.
     *
     * @param string $date
     */
    function __construct($date)
    {
        if (!strtotime($date)) {
            throw new \InvalidArgumentException(
                sprintf(
                    '%s date must be a string parseable by strtotime().',
                    get_called_class()
                )
            );
        }

        parent::__construct('start_date', $date);
    }
}
