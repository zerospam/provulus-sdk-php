<?php
/**
 * Created by PhpStorm.
 * User: pbb
 * Date: 05/07/17
 * Time: 1:10 PM
 */

namespace ProvulusSDK\Client\Args\Search\Common\Date;

use Carbon\Carbon;


/**
 * Trait AuditLogsArgumentsTrait
 *
 * Trait used to set request arguments (parameters) for AuditLogs
 *
 * @package ProvulusSDK\Client\Request\Audit
 */
trait DateRangeArgumentsTrait
{

    /**
     * Set StartDate Argument
     * If null (default), will remove argument
     *
     * @param Carbon|null $startDate
     *
     * @return $this
     */
    public function setStartDateArgument(Carbon $startDate = null)
    {
        if (is_null($startDate)) {
            return $this->removeArgument(new StartDateArgument(Carbon::now()->toDateTimeString()));
        }

        return $this->addArgument(new StartDateArgument($startDate->toDateTimeString()));
    }

    /**
     * Sets EndDate Argument
     * If null (default), will remove argument
     *
     * @param Carbon|null $endDate
     *
     * @return $this
     */
    public function setEndDateArgument(Carbon $endDate = null)
    {
        if (is_null($endDate)) {
            return $this->removeArgument(new EndDateArgument(Carbon::now()->toDateTimeString()));
        }

        return $this->addArgument(new EndDateArgument($endDate->toDateTimeString()));
    }
}