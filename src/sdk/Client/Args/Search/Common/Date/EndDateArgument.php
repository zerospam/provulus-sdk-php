<?php
/**
 * Created by PhpStorm.
 * User: pbb
 * Date: 05/07/17
 * Time: 11:55 AM
 */

namespace ProvulusSDK\Client\Args\Search\Common\Date;

use ProvulusSDK\Client\Args\RequestArg;

/**
 * Class EndDateArgument
 *
 * Argument for EndDateArgument
 *
 * @package ProvulusSDK\Client\Args\Search\Common
 */
class EndDateArgument extends RequestArg
{
    /**
     * EndDateArgument constructor.
     *
     * @param string $date
     */
    function __construct($date)
    {
        if (!strtotime($date)) {
            throw new \InvalidArgumentException(
                sprintf(
                    '%s date must be parseable by strtotime().',
                    get_called_class()
                )
            );
        }

        parent::__construct('end_date', $date);
    }
}