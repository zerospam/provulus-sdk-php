<?php
/**
 * Created by PhpStorm.
 * User: pbb
 * Date: 05/07/17
 * Time: 2:47 PM
 */

namespace ProvulusSDK\Client\Args\Search\Audit;

use ProvulusSDK\Client\Args\Search\Common\Date\DateRangeArgumentsTrait;
use ProvulusSDK\Client\Args\Search\Common\User\UserIdArgument;
use ProvulusSDK\Client\Args\Search\Common\User\UserIdArgumentTrait;
use ProvulusSDK\Enum\Audit\AuditedModelsEnum;
use ProvulusSDK\Enum\Audit\AuditLogTypeEnum;

/**
 * Class AuditLogsArgumentsTrait
 *
 * Traits regrouping methods useful for setting arguments on AuditLogs request
 *
 * See also the following
 *
 * @see     DateRangeArgumentsTrait
 * @see     UserIdArgumentTrait
 *
 * @package ProvulusSDK\Client\Args\Search\Audit
 */
trait AuditLogsArgumentsTrait
{
    /**
     * Sets auditable id request argument
     * If $id = null (default),will remove argument
     *
     * @param mixed|null $id
     *
     * @return $this
     */
    public function setAuditableId($id = null)
    {
        if (is_null($id)) {
            return $this->removeArgument(new AuditableIdArgument(1));
        }

        return $this->addArgument(new AuditableIdArgument($id));
    }

    /**
     * Sets auditable type request argument
     * If $enum is null (default), will remove argument
     *
     * @param AuditedModelsEnum|null $enum
     *
     * @return $this
     */
    public function setModelType(AuditedModelsEnum $enum = null)
    {
        if (is_null($enum)) {
            return $this->removeArgument(new AuditableTypeArgument(AuditedModelsEnum::DOMAIN()));
        }

        return $this->addArgument(new AuditableTypeArgument($enum));
    }

    /**
     * Sets auditable type request argument
     * If $enum is null (default), will remove argument
     *
     * @param AuditLogTypeEnum|null $enum
     *
     * @return $this
     */
    public function setEventType(AuditLogTypeEnum $enum = null)
    {
        if (is_null($enum)) {
            return $this->removeArgument(new AuditEventTypeArgument(AuditLogTypeEnum::DELETED()));
        }

        return $this->addArgument(new AuditEventTypeArgument($enum));
    }

    /**
     * Sets user id argument
     *
     * Removes the argument if $id is null
     *
     * @param int|null $id
     *
     * @return $this
     */
    public function setUserId($id = null)
    {
        if (is_null($id)) {
            return $this->removeArgument(new UserIdArgument(1));
        }

        return $this->addArgument(new UserIdArgument($id));
    }

}