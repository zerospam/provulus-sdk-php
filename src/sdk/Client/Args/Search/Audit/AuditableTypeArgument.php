<?php
/**
 * Created by PhpStorm.
 * User: pbb
 * Date: 05/07/17
 * Time: 2:56 PM
 */

namespace ProvulusSDK\Client\Args\Search\Audit;

use ProvulusSDK\Client\Args\RequestArg;
use ProvulusSDK\Enum\Audit\AuditedModelsEnum;

/**
 * Class AuditableTypeArgument
 *
 * Auditable type argument for filtering audits by Auditable Type
 *
 * @package ProvulusSDK\Client\Args\Search\Audit
 */
class AuditableTypeArgument extends RequestArg
{
    /**
     * AuditableTypeArgument constructor.
     *
     * @param AuditedModelsEnum $enum
     */
    public function __construct(AuditedModelsEnum $enum)
    {
        parent::__construct('auditable_type', $enum->toPrimitive());
    }
}