<?php
/**
 * Created by PhpStorm.
 * User: pbb
 * Date: 05/07/17
 * Time: 2:41 PM
 */

namespace ProvulusSDK\Client\Args\Search\Audit;


use ProvulusSDK\Client\Args\RequestArg;

/**
 * Class AuditableIdArgument
 *
 * Request parameter for filtering audits by auditable ids
 *
 * @package ProvulusSDK\Client\Args\Search\Audit
 */
class AuditableIdArgument extends RequestArg
{
    /**
     * AuditableIdArgument constructor.
     *
     * @param mixed $id
     */
    public function __construct($id)
    {
        if (!(is_numeric($id))) {
            throw new InvalidArgumentException(sprintf('%s id parameter needs to be numeric'));
        }

        parent::__construct('auditable_id', (int)$id);
    }
}