<?php
/**
 * Created by PhpStorm.
 * User: pbb
 * Date: 05/07/17
 * Time: 3:45 PM
 */

namespace ProvulusSDK\Client\Args\Search\Audit;

use ProvulusSDK\Client\Args\RequestArg;
use ProvulusSDK\Enum\Audit\AuditLogTypeEnum;

/**
 * Class EventTypeArgument
 *
 * Request parameter for filtering audit logs by event type
 *
 * @package ProvulusSDK\Client\Args\Search\Audit
 */
class AuditEventTypeArgument extends RequestArg
{
    /**
     * EventTypeArgument constructor.
     *
     * @param AuditLogTypeEnum $enum
     */
    public function __construct(AuditLogTypeEnum $enum)
    {
        parent::__construct('event', $enum->toPrimitive());
    }

}