<?php
/**
 * Created by PhpStorm.
 * User: pbb
 * Date: 13/02/17
 * Time: 10:37 AM
 */

namespace ProvulusSDK\Client\Args\Search\Organization;


use ProvulusSDK\Client\Args\RequestArg;

/**
 * Class DomainNameArgument
 *
 * Argument to search organizations based on domain name
 *
 * @package ProvulusSDK\Client\Args\Search\Organization
 */
class DomainNameArgument extends RequestArg
{
    /**
     * DomainNameArgument constructor.
     *
     * @param string $value
     */
    public function __construct($value)
    {
        if (!is_string($value)) {
            throw new \InvalidArgumentException(sprintf('Domain name needs to be of type string.'));
        }

        parent::__construct('domain_name', $value);
    }
}