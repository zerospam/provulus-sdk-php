<?php
/**
 * Created by PhpStorm.
 * User: pbb
 * Date: 13/02/17
 * Time: 10:05 AM
 */

namespace ProvulusSDK\Client\Args\Search\Organization;

use ProvulusSDK\Client\Args\RequestArg;
use ProvulusSDK\Enum\SearchCriteria\ActiveCriteriaEnum;

/**
 * Class ActiveArgument
 *
 * Argument for filtering organizations based of their activity status
 *
 * @package ProvulusSDK\Client\Args\Search\Organization
 */
class ActiveArgument extends RequestArg
{

    /**
     * ActiveArgument constructor.
     *
     * @param ActiveCriteriaEnum $enum
     */
    public function __construct(ActiveCriteriaEnum $enum)
    {
        parent::__construct('active', $enum);
    }
}