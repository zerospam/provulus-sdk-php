<?php
/**
 * Created by PhpStorm.
 * User: pbb
 * Date: 10/02/17
 * Time: 8:31 AM
 */

namespace ProvulusSDK\Client\Args\Search\Organization;

use ProvulusSDK\Client\Args\RequestArg;

/**
 * Class CorporateNameSearchArgument
 *
 * Argument to search organizations based on the corporate_name
 *
 * @package ProvulusSDK\Client\Args\Search\Organization
 */
class CorporateNameArgument extends RequestArg
{
    /**
     * CorporateNameArgument constructor.
     *
     * @param string $value
     */
    public function __construct($value)
    {
        if (!is_string($value)) {
            throw new \InvalidArgumentException(sprintf('Corporate Name needs to be of type string.'));
        }

        parent::__construct('corporate_name', $value);
    }
}