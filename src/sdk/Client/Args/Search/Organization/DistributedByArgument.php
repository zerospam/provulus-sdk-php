<?php
/**
 * Created by PhpStorm.
 * User: pbb
 * Date: 10/02/17
 * Time: 4:25 PM
 */

namespace ProvulusSDK\Client\Args\Search\Organization;


use ProvulusSDK\Client\Args\RequestArg;

/**
 * Class SupportedByArgument
 *
 * Argument for filtering organizations by Support Organization ID
 *
 * @package ProvulusSDK\Client\Args\Search\Organization
 */
class DistributedByArgument extends RequestArg
{
    /**
     * SupportedByArgument constructor.
     *
     * @param int $value
     */
    public function __construct($value)
    {
        if (!is_int($value)) {
            throw new \InvalidArgumentException('Supported by organization argument must be an integer : id of support organization.');
        }

        parent::__construct('distributed_by', $value);
    }
}