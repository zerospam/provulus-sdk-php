<?php
/**
 * Created by PhpStorm.
 * User: pbb
 * Date: 13/02/17
 * Time: 9:46 AM
 */

namespace ProvulusSDK\Client\Args\Search\Organization;

use ProvulusSDK\Client\Args\RequestArg;

/**
 * Class BilledByArgument
 *
 * Argument for filtering organizations by Billing Organization ID
 *
 * @package ProvulusSDK\Client\Args\Search\Organization
 */
class BilledByArgument extends RequestArg
{

    /**
     * BilledByArgument constructor.
     *
     * @param int $value
     */
    public function __construct($value)
    {
        if (!is_int($value)) {
            throw new \InvalidArgumentException('Billed by organization argument must be an integer : id of billing organization.');
        }

        parent::__construct('billed_by', $value);
    }
}