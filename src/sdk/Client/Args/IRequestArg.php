<?php
/**
 * Created by PhpStorm.
 * User: aaflalo
 * Date: 09/01/17
 * Time: 11:45 AM
 */
namespace ProvulusSDK\Client\Args;

use ProvulusSDK\Client\Request\Resource\PrimalValued;

interface IRequestArg extends PrimalValued
{
    /**
     * Key for the argument
     *
     * @return string
     */
    public function getKey();
}