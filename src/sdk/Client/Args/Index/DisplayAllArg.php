<?php
/**
 * Created by PhpStorm.
 * User: ycoutu
 * Date: 12/03/19
 * Time: 4:59 PM
 */

namespace ProvulusSDK\Client\Args\Index;

use ProvulusSDK\Client\Args\RequestArg;

class DisplayAllArg extends RequestArg
{
    public function __construct($value)
    {
        if (!is_bool($value)) {
            throw new \InvalidArgumentException('Value need to be a boolean');
        }
        parent::__construct('all', $value);
    }
}
