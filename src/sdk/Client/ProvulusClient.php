<?php
/**
 * Created by PhpStorm.
 * User: aaflalo
 * Date: 29/11/16
 * Time: 1:26 PM
 */

namespace ProvulusSDK\Client;

use GuzzleHttp\ClientInterface;
use GuzzleHttp\Exception\BadResponseException;
use GuzzleHttp\Exception\RequestException;
use GuzzleHttp\Psr7\Uri;
use GuzzleHttp\Psr7\UriResolver;
use GuzzleHttp\RequestOptions;
use ProvulusSDK\Client\RateLimit\RateLimitData;
use ProvulusSDK\Client\Request\Auth\LoginRequest;
use ProvulusSDK\Client\Request\Auth\Token\UserCryptToken;
use ProvulusSDK\Client\Request\IProvulusRequest;
use ProvulusSDK\Client\Request\RequestType;
use ProvulusSDK\Client\Response\Auth\AuthResponse;
use ProvulusSDK\Client\Response\IProvulusResponse;
use ProvulusSDK\Client\Response\ProvulusBaseResponse;
use ProvulusSDK\Config\IProvulusConfiguration;
use ProvulusSDK\Exception\API\APICrashException;
use ProvulusSDK\Exception\API\ForbiddenException;
use ProvulusSDK\Exception\API\HitRateLimitException;
use ProvulusSDK\Exception\API\NotFoundException;
use ProvulusSDK\Exception\API\PreconditionException;
use ProvulusSDK\Exception\API\Success\PartialSuccessException;
use ProvulusSDK\Exception\API\UnauthorizedException;
use ProvulusSDK\Exception\API\ValidationException;
use ProvulusSDK\Exception\Http\GuzzleException;
use Psr\Http\Message\ResponseInterface;
use function GuzzleHttp\Psr7\uri_for;

class ProvulusClient
{

    /**
     * @var IProvulusConfiguration
     */
    private $configuration;

    /**
     * @var ClientInterface
     */
    private $guzzleClient;

    /**
     * @var RateLimitData
     */
    private $rateLimit;

    /**
     * ProvulusClient constructor.
     *
     * @param IProvulusConfiguration $configuration
     */
    public function __construct(IProvulusConfiguration $configuration)
    {
        $this->configuration = $configuration;
        if (!$this->configuration->isValid()) {
            throw new \InvalidArgumentException('You need either a JWT storage or an API Key set in the configuration');
        }

        $this->guzzleClient = $configuration->buildClient();
        $this->rateLimit    = new RateLimitData();
    }

    /**
     * Use a login route to authenticate with the API
     *
     * @param string      $username
     * @param string      $password
     * @param string|null $code
     *
     * @return AuthResponse|IProvulusResponse
     */
    public function login($username, $password, $code = null)
    {
        if (!$this->configuration->getJwtStorage()) {
            throw new \InvalidArgumentException('You need a JWT storage to use the login functionality');
        }


        $request = (new LoginRequest($username))->setPassword($password);
        if (!is_null($code)) {
            $request->setCode($code);
        }
        $this->processRequest($request);
        $this->updateJwtToken($request->getResponse()->token());

        return $request->getResponse();
    }

    /**
     * Log an user using its unique token
     *
     * @param string         $username
     * @param UserCryptToken $token
     * @param string|null    $code
     *
     * @return AuthResponse|IProvulusResponse
     */
    public function loginToken($username, UserCryptToken $token, $code = null)
    {
        if (!$this->configuration->getJwtStorage()) {
            throw new \InvalidArgumentException('You need a JWT storage to use the login functionality');
        }
        $token->setEncryptKey($this->configuration->getEncryptionKey());

        $request = (new LoginRequest($username))->setPasswordToken($token);
        if (!is_null($code)) {
            $request->setCode($code);
        }
        $this->processRequest($request);
        $this->updateJwtToken($request->getResponse()->token());


        return $request->getResponse();
    }

    /**
     * Process the given request and return an array containing the results
     *
     * @param IProvulusRequest $request
     *
     * @throws UnauthorizedException if you don't have access to the route
     * @throws ValidationException if there is a problem in the data you sent
     * @throws NotFoundException if the request's object couldn't be found
     * @return IProvulusResponse
     */
    public function processRequest(IProvulusRequest $request)
    {
        $request->incrementTries();
        $defaultOptions = $this->defaultOptions();

        $requestOptions = array_merge_recursive($defaultOptions, $request->requestOptions());


        try {
            $response = $this->guzzleClient->request(
                $request->requestType()->getValue(),
                $request->toUri(),
                $requestOptions
            );

            switch ($response->getStatusCode()) {
                case 206:
                    $errors = $this->responseToJson($response);
                    throw new PartialSuccessException($errors['errors']);
                    break;
                default:
                    break;
            }
        } catch (BadResponseException $e) {
            /**
             * Check status code
             */
            $response = $e->getResponse();
            $this->processJwtToken($response);
            $this->processThrottleData($response);
            switch ($response->getStatusCode()) {
                case 401:
                    throw new UnauthorizedException('Unauthorized or wrong username/password');
                case 403:
                    throw new ForbiddenException('Forbidden access');
                case 422:
                    $errors = $this->responseToJson($response);
                    throw new ValidationException($errors['errors']);
                case 404:
                    throw new NotFoundException($request, $this->configuration->baseUrl(), $e);
                case 412:
                    $errors = $this->responseToJson($response);
                    throw new PreconditionException($errors['errors']);
                case 429:
                    throw new HitRateLimitException($this->rateLimit);
                case 500:
                    $errors   = $this->responseToJson($response);
                    $sentryId = null;
                    if (isset($errors['id'])) {
                        $sentryId = $errors['id'];
                    }
                    throw new APICrashException($e->getMessage(), $e->getCode(), $e, $sentryId);

                default:
                    throw new GuzzleException($e->getMessage(), $e->getCode(), $e);
            }
        } catch (RequestException $e) {
            if ($request->requestType()->is(RequestType::HTTP_GET()) &&
                $request->tries() < $this->configuration->getRetries()) {
                return $this->processRequest($request);
            }
            throw new GuzzleException($e->getMessage(), $e->getCode(), $e);
        }

        /**
         * @var $data ProvulusBaseResponse
         */
        $data = $request->processResponse($this->responseToJson($response));
        $data->setRateLimit($this->rateLimit);

        $this->processJwtToken($response);
        $this->processThrottleData($response);

        $request->setResponse($data);

        return $data;
    }

    /**
     * Getter for configuration
     *
     * @return IProvulusConfiguration
     */
    public function getConfiguration()
    {
        return $this->configuration;
    }

    /**
     * Parse the content of a response
     *
     * @param ResponseInterface $response
     *
     * @return array
     */
    private function responseToJson(ResponseInterface $response)
    {
        $contents = $response->getBody()->getContents();
        if (empty($contents)) {
            return [];
        }

        return \GuzzleHttp\json_decode($contents, true);
    }


    /**
     * Set the right headers for a request
     *
     * @return array
     */
    private function setHeaders()
    {
        $defaultHeaders = [
            'Accept'          => 'application/json',
            'Accept-Encoding' => 'gzip',
            'X-Forwarded-For' => $_SERVER['REMOTE_ADDR']
        ];

        $headers = $defaultHeaders;
        if ($this->configuration->getJwtStorage() && ($token = $this->configuration->getJwtStorage()->getToken())) {
            $headers['Authorization'] = 'Bearer ' . $token;
        } elseif ($this->configuration->getApiKey()) {
            $headers['X-Cumulus-Api'] = $this->configuration->getApiKey();
        }

        if ($orgId = $this->configuration->getOrganizationId()) {
            $headers['X-Cumulus-Org'] = $orgId;
        }

        if ($this->configuration->getLocale()) {
            $headers['X-Cumulus-Locale'] = $this->configuration->getLocale()->getValue();
        }

        return $headers;
    }

    /**
     * Retrieve and process the JWT token
     *
     * @param ResponseInterface $response
     */
    private function processJwtToken(ResponseInterface $response)
    {
        if (!empty($jwtToken = $response->getHeader('Authorization'))) {
            $this->updateJwtToken(substr($jwtToken[0], 7));
        }
    }

    /**
     * Process the rate limit
     *
     * @param ResponseInterface $response
     */
    private function processThrottleData(ResponseInterface $response)
    {
        if (empty($rateLimit = $response->getHeader('X-RateLimit-Limit'))) {
            return;
        }

        if (empty($remaining = $response->getHeader('X-RateLimit-Remaining'))) {
            return;
        }

        if (!empty($reset = $response->getHeader('X-RateLimit-Reset'))) {
            $this->rateLimit->setEndOfThrottle(intval($reset[0]));
        } else {
            $this->rateLimit->setEndOfThrottle(null);
        }

        $this->rateLimit->setMaxPerMinute(intval($rateLimit[0]))->setRemaining(intval($remaining[0]));
    }

    /**
     * Update the JWT Token and check that the storage is set
     *
     * @param $jwtToken
     */
    private function updateJwtToken($jwtToken)
    {
        if (!$this->configuration->getJwtStorage()) {
            throw new \InvalidArgumentException('A JWT token was returned but no JWT Storage is set');
        }

        $this->configuration->getJwtStorage()->updateToken($jwtToken);
    }

    /**
     * @return array
     */
    private function defaultOptions()
    {
        $headers = $this->setHeaders();

        $defaultOptions                                  = [];
        $defaultOptions[RequestOptions::HEADERS]         = $headers;
        $defaultOptions[RequestOptions::CONNECT_TIMEOUT] = $this->configuration->getConnectionTimeout();
        $defaultOptions[RequestOptions::TIMEOUT]         = $this->configuration->getRequestTimeout();

        return $defaultOptions;
    }

    /**
     * Generate the full URI of the given request
     *
     * @param IProvulusRequest $request
     *
     * @return Uri
     */
    public function uriOf(IProvulusRequest $request)
    {
        return UriResolver::resolve($this->configuration->baseUrl(), uri_for($request->routeUrl()))->withQuery(http_build_query($request->requestOptions()[RequestOptions::QUERY]));
    }
}
