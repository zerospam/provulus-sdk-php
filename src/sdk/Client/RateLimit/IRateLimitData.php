<?php
/**
 * Created by PhpStorm.
 * User: aaflalo
 * Date: 15/12/16
 * Time: 11:49 AM
 */
namespace ProvulusSDK\Client\RateLimit;

use Carbon\Carbon;


/**
 * Class RateLimitData
 *
 * Represent the number of request allowed in a minute
 * How many are still alowed and in the case there is too many done,
 * when  can the next one be done
 *
 * @package ProvulusSDK\Client
 */
interface IRateLimitData
{
    /**
     * Getter for maxPerMinute
     *
     * @return int
     */
    public function getMaxPerMinute();

    /**
     * Getter for currentUsage
     *
     * @return int
     */
    public function getRemaining();

    /**
     * Getter for endOfThrottle
     *
     * @return Carbon
     */
    public function getEndOfThrottle();
}