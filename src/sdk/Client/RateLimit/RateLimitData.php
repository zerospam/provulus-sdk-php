<?php
/**
 * Created by PhpStorm.
 * User: aaflalo
 * Date: 15/12/16
 * Time: 11:23 AM
 */

namespace ProvulusSDK\Client\RateLimit;


use Carbon\Carbon;

/**
 * Class RateLimitData
 *
 * Represent the number of request allowed in a minute
 * How many are still alowed and in the case there is too many done,
 * when  can the next one be done
 *
 * @package ProvulusSDK\Client
 */
class RateLimitData implements IRateLimitData
{

    /**
     * @var int
     */
    private $maxPerMinute, $remaining;

    /**
     * @var Carbon
     */
    private $endOfThrottle;

    /**
     * Getter for maxPerMinute
     *
     * @return int
     */
    public function getMaxPerMinute()
    {
        return $this->maxPerMinute;
    }

    /**
     * @param int $maxPerMinute
     *
     * @return $this
     */
    public function setMaxPerMinute($maxPerMinute)
    {
        $this->maxPerMinute = $maxPerMinute;

        return $this;
    }

    /**
     * Getter for currentUsage
     *
     * @return int
     */
    public function getRemaining()
    {
        return $this->remaining;
    }

    /**
     * @param int $remaining
     *
     * @return $this
     */
    public function setRemaining($remaining)
    {
        $this->remaining = $remaining;

        return $this;
    }

    /**
     * Getter for endOfThrottle
     *
     * @return Carbon
     */
    public function getEndOfThrottle()
    {
        return $this->endOfThrottle;
    }

    /**
     * @param Carbon $endOfThrottle
     *
     * @return $this
     */
    public function setEndOfThrottle($endOfThrottle)
    {
        if(is_int($endOfThrottle)) {
            $endOfThrottle = Carbon::createFromTimestamp($endOfThrottle);
        }

        $this->endOfThrottle = $endOfThrottle;

        return $this;
    }


}