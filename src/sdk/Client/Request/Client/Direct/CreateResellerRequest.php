<?php
/**
 * Created by PhpStorm.
 * User: aaflalo
 * Date: 08/12/16
 * Time: 11:41 AM
 */

namespace ProvulusSDK\Client\Request\Client\Direct;

use ProvulusSDK\Client\Response\Client\ClientCreationResponse;


/**
 * Class CreateResellerRequest
 *
 * Used to create a new reseller
 * Only by ZEROSPAM
 * @method ClientCreationResponse getResponse()
 *
 * @package ProvulusSDK\Client\Request\Client\Reseller\Reseller
 */
class CreateResellerRequest extends CreateDirectClientRequest
{

    /**
     * @var string
     */
    protected $testEmailDomain;

    /**
     * @var bool
     */
    protected $adminStats;

    /**
     * @var bool
     */
    protected $isSupport, $isBilling;

    /** @var  bool */
    protected $canGrantTrial, $isClientsNotifiedEndTrial;

    /**
     * @param bool $isSupport
     *
     * @return $this
     */
    public function setIsSupport($isSupport)
    {
        $this->isSupport = $isSupport;

        return $this;
    }

    /**
     * @param bool $isBilling
     *
     * @return $this
     */
    public function setIsBilling($isBilling)
    {
        $this->isBilling = $isBilling;

        return $this;
    }

    /**
     * @param string $testEmailDomain
     *
     * @return $this
     */
    public function setTestEmailDomain($testEmailDomain)
    {
        $this->testEmailDomain = $testEmailDomain;

        return $this;
    }

    /**
     * @param bool $adminStats
     *
     * @return $this
     */
    public function setAdminStats($adminStats)
    {
        $this->adminStats = $adminStats;

        return $this;
    }

    /**
     * @param bool $canGrantTrial
     *
     * @return CreateResellerRequest
     */
    public function setCanGrantTrial($canGrantTrial)
    {
        $this->canGrantTrial = $canGrantTrial;

        return $this;
    }

    /**
     * @param bool $isClientsNotifiedEndTrial
     *
     * @return CreateResellerRequest
     */
    public function setIsClientsNotifiedEndTrial($isClientsNotifiedEndTrial)
    {
        $this->isClientsNotifiedEndTrial = $isClientsNotifiedEndTrial;

        return $this;
    }

    public function routeUrl()
    {
        return 'resellers';
    }


}