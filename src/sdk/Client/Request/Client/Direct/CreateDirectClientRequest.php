<?php
/**
 * Created by PhpStorm.
 * User: aaflalo
 * Date: 06/12/16
 * Time: 11:34 AM
 */

namespace ProvulusSDK\Client\Request\Client\Direct;


use ProvulusSDK\Client\Request\BaseProvulusRequest;
use ProvulusSDK\Client\Request\RequestType;
use ProvulusSDK\Client\Response\Client\ClientCreationResponse;
use ProvulusSDK\Enum\Locale\LocaleEnum;
use ProvulusSDK\Enum\Organization\CountryEnum;

/**
 * Class CreateDirectClientRequest
 *
 * Create a direct client.
 * Only used by ZEROSPAM
 * @method ClientCreationResponse getResponse()
 *
 * @package ProvulusSDK\Client\Request\Client\Direct
 */
class CreateDirectClientRequest extends BaseProvulusRequest
{

    /**
     * @var string
     */
    protected $firstName, $lastName, $domain, $phone, $email, $organizationName, $transport;

    /**
     * @var int
     */
    protected $transportPort, $mailboxes;

    /**
     * @var LocaleEnum
     */
    protected $language;

    /**
     * @var \DateTimeZone
     */
    protected $timezone;

    /** @var  bool */
    protected $hasTrial, $isNotifiedEndTrial;

    /** @var CountryEnum */
    protected $country;

    /**
     * @param string $firstName
     *
     * @return $this
     */
    public function setFirstName($firstName)
    {
        $this->firstName = $firstName;

        return $this;
    }

    /**
     * @param string $lastName
     *
     * @return $this
     */
    public function setLastName($lastName)
    {
        $this->lastName = $lastName;

        return $this;
    }

    /**
     * @param string $domain
     *
     * @return $this
     */
    public function setDomain($domain)
    {
        $this->domain = $domain;

        return $this;
    }

    /**
     * @param string $phone
     *
     * @return $this
     */
    public function setPhone($phone)
    {
        $this->phone = $phone;

        return $this;
    }

    /**
     * @param string $email
     *
     * @return $this
     */
    public function setEmail($email)
    {
        $this->email = $email;

        return $this;
    }

    /**
     * @param string $organizationName
     *
     * @return $this
     */
    public function setOrganizationName($organizationName)
    {
        $this->organizationName = $organizationName;

        return $this;
    }


    /**
     * @param string $transport
     *
     * @return $this
     */
    public function setTransport($transport)
    {
        $this->transport = $transport;

        return $this;
    }

    /**
     * @param int $transportPort
     *
     * @return $this
     */
    public function setTransportPort($transportPort)
    {
        $this->transportPort = $transportPort;

        return $this;
    }

    /**
     * @param LocaleEnum $language
     *
     * @return $this
     */
    public function setLanguage(LocaleEnum $language)
    {
        $this->language = $language;

        return $this;
    }

    /**
     * @param int $mailboxes
     *
     * @return $this
     */
    public function setMailboxes($mailboxes)
    {
        $this->mailboxes = $mailboxes;

        return $this;
    }

    /**
     * Set the timezone of the organization
     *
     * If not set, take the one of the parent org
     *
     * @param \DateTimeZone $timezone
     *
     * @return $this
     */
    public function setTimezone(\DateTimeZone $timezone)
    {
        $this->timezone = $timezone;

        return $this;
    }

    /**
     * @param bool $hasTrial
     *
     * @return CreateDirectClientRequest
     */
    public function setHasTrial($hasTrial)
    {
        $this->hasTrial = $hasTrial;

        return $this;
    }

    /**
     * @param bool $isNotifiedEndTrial
     *
     * @return CreateDirectClientRequest
     */
    public function setIsNotifiedEndTrial($isNotifiedEndTrial)
    {
        $this->isNotifiedEndTrial = $isNotifiedEndTrial;

        return $this;
    }

    /**
     * @param CountryEnum $country
     *
     * @return $this
     */
    public function setCountry(CountryEnum $country)
    {
        $this->country = $country;

        return $this;
    }

    /**
     * The url of the route
     *
     * @return string
     */
    public function routeUrl()
    {
        return 'clients';
    }

    /**
     * Type of request
     *
     * @return RequestType
     */
    public function httpType()
    {
        return RequestType::HTTP_POST();
    }

    /**
     * Process the data that is in the response
     *
     * @param array $jsonResponse
     *
     * @return ClientCreationResponse
     * @internal param Response $response
     */
    public function processResponse(array $jsonResponse)
    {
        return new ClientCreationResponse($jsonResponse['data'], $jsonResponse['included']);
    }
}