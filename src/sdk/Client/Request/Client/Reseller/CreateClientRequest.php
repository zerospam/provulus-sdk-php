<?php
/**
 * Created by PhpStorm.
 * User: aaflalo
 * Date: 08/12/16
 * Time: 11:36 AM
 */

namespace ProvulusSDK\Client\Request\Client\Reseller;

use ProvulusSDK\Client\Request\Client\Direct\CreateDirectClientRequest;

/**
 * Class CreateClientRequest
 *
 * To be used to create a client for a reseller
 *
 * @package ProvulusSDK\Client\Request\Client\Reseller\Client
 */
class CreateClientRequest extends CreateDirectClientRequest
{
    /**
     * @var bool
     */
    protected $withAdmin;

    /**
     * @var string
     */
    protected $testEmailDomain;

    /**
     * @var bool
     */
    protected $isBillable;

    /**
     * @var bool
     */
    protected $adminStats;

    /**
     * @var int
     */
    protected $supportOrg, $billingOrg;

    public function routeUrl()
    {
        return 'resellers/clients';
    }

    /**
     * @param bool $withAdmin
     *
     * @return $this
     */
    public function setWithAdmin($withAdmin)
    {
        $this->withAdmin = $withAdmin;

        return $this;
    }

    /**
     * @param string $testEmailDomain
     *
     * @return $this
     */
    public function setTestEmailDomain($testEmailDomain)
    {
        $this->testEmailDomain = $testEmailDomain;

        return $this;
    }

    /**
     * Set support organization
     *
     * <b>Only for ZEROSPAM usage</b>
     *
     * @param int $supportOrg
     *
     * @return $this
     */
    public function setSupportOrg($supportOrg)
    {
        $this->supportOrg = $supportOrg;

        return $this;
    }

    /**
     * Set billing organization
     *
     * <b>Only for ZEROSPAM usage</b>
     *
     * @param int $billingOrg
     *
     * @return $this
     */
    public function setBillingOrg($billingOrg)
    {
        $this->billingOrg = $billingOrg;

        return $this;
    }

    /**
     * Set is billable organization
     *
     * <b>Only for ZEROSPAM usage</b>
     *
     * @param bool $isBillable
     *
     * @return $this
     */
    public function setIsBillable($isBillable)
    {
        $this->isBillable = $isBillable;

        return $this;
    }

    /**
     * Admin Receive stats
     *
     * @param bool $adminStats
     *
     * @return $this
     */
    public function setAdminStats($adminStats)
    {
        $this->adminStats = $adminStats;

        return $this;
    }
}