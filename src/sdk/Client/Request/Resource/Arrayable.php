<?php
/**
 * Created by PhpStorm.
 * User: aaflalo
 * Date: 17-06-16
 * Time: 11:09
 */

namespace ProvulusSDK\Client\Request\Resource;

interface Arrayable
{

    /**
     * Transform the object into an array
     *
     * @return array
     */
    public function toArray();
}
