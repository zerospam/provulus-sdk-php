<?php
/**
 * Created by PhpStorm.
 * User: aaflalo
 * Date: 07/02/17
 * Time: 1:20 PM
 */

namespace ProvulusSDK\Client\Request\Resource;


interface PrimalValued
{

    /**
     * Return a primitive value for this object
     *
     * @return int|float|string|double
     */
    function toPrimitive();
}