<?php
/**
 * Created by PhpStorm.
 * User: ycoutu
 * Date: 19/09/18
 * Time: 10:23 AM
 */

namespace ProvulusSDK\Client\Request\Resource\Organization\Billing;

use ProvulusSDK\Enum\PaymentCurrency\PaymentCurrencyEnum;

trait BillingAttributesTrait
{
    /** @var PaymentCurrencyEnum */
    private $currency;

    /** @var string */
    private $country;

    /** @var string|null */
    private $state;

    /** @var string */
    private $city;

    /** @var string */
    private $postalCode;

    /** @var string */
    private $street1;

    /** @var string|null */
    private $street2;

    /** @var string */
    private $email;

    /** @var string */
    private $firstName;

    /** @var string */
    private $lastName;

    /**
     * @param PaymentCurrencyEnum $currency
     * @return $this
     */
    public function setCurrency($currency)
    {
        $this->currency = $currency;
        return $this;
    }

    /**
     * @param string $country
     * @return $this
     */
    public function setCountry($country)
    {
        $this->country = $country;
        return $this;
    }

    /**
     * @param null|string $state
     * @return $this
     */
    public function setState($state)
    {
        $this->nullableChanged();
        $this->state = $state;
        return $this;
    }

    /**
     * @param string $city
     * @return $this
     */
    public function setCity($city)
    {
        $this->city = $city;
        return $this;
    }

    /**
     * @param string $postalCode
     * @return $this
     */
    public function setPostalCode($postalCode)
    {
        $this->postalCode = $postalCode;
        return $this;
    }

    /**
     * @param string $street1
     * @return $this
     */
    public function setStreet1($street1)
    {
        $this->street1 = $street1;
        return $this;
    }

    /**
     * @param null|string $street2
     * @return $this
     */
    public function setStreet2($street2)
    {
        $this->nullableChanged();
        $this->street2 = $street2;
        return $this;
    }

    /**
     * @param string $email
     * @return $this
     */
    public function setEmail($email)
    {
        $this->email = $email;
        return $this;
    }

    /**
     * @param string $firstName
     * @return $this
     */
    public function setFirstName($firstName)
    {
        $this->firstName = $firstName;
        return $this;
    }

    /**
     * @param string $lastName
     * @return $this
     */
    public function setLastName($lastName)
    {
        $this->lastName = $lastName;
        return $this;
    }

}
