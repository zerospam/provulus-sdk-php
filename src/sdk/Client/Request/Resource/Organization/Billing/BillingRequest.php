<?php
/**
 * Created by PhpStorm.
 * User: ycoutu
 * Date: 19/09/18
 * Time: 10:31 AM
 */

namespace ProvulusSDK\Client\Request\Resource\Organization\Billing;

use ProvulusSDK\Client\Request\Resource\Organization\OrganizationRequest;

abstract class BillingRequest extends OrganizationRequest
{
    /**
     * Base route for orgs resource
     *
     * @return string
     * @throws \Exception
     */
    function baseRoute()
    {
        return parent::baseRoute() . '/billing';
    }
}
