<?php
/**
 * Created by PhpStorm.
 * User: ycoutu
 * Date: 19/09/18
 * Time: 10:39 AM
 */

namespace ProvulusSDK\Client\Request\Resource\Organization\Billing;

use ProvulusSDK\Client\Response\IProvulusResponse;
use ProvulusSDK\Client\Response\Organization\Billing\BillingResponse;

/**
 * Trait BillingProcessResponseTrait
 *
 * @method BillingResponse getResponse()
 *
 * @package ProvulusSDK\Client\Request\Resource\Organization\Billing
 */
trait BillingProcessResponseTrait
{
    /**
     * Process the data that is in the response
     *
     * @param array $jsonResponse
     *
     * @return IProvulusResponse
     */
    public function processResponse(array $jsonResponse)
    {
        return new BillingResponse($jsonResponse['data']);
    }
}
