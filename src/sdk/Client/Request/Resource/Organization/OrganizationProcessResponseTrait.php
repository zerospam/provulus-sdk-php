<?php
/**
 * Created by PhpStorm.
 * User: pbb
 * Date: 10/02/17
 * Time: 2:02 PM
 */

namespace ProvulusSDK\Client\Request\Resource\Organization;

use ProvulusSDK\Client\Response\Organization\OrganizationResponse;

/**
 * Class OrganizationProcessResponseTrait
 *
 * @package ProvulusSDK\Client\Request\Resource\Organization
 */
trait OrganizationProcessResponseTrait
{
    /**
     * Process the data that is in the response
     *
     * @param array $jsonResponse
     *
     * @return OrganizationResponse
     */
    public function processResponse(array $jsonResponse)
    {
        $included = isset($jsonResponse['included'])
            ? $jsonResponse['included']
            : [];
        return new OrganizationResponse($jsonResponse['data'], $included);
    }
}