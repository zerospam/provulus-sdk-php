<?php
/**
 * Created by PhpStorm.
 * User: pbb
 * Date: 30/01/17
 * Time: 10:00 AM
 */

namespace ProvulusSDK\Client\Request\Resource\Organization\MX;

use ProvulusSDK\Client\Response\Organization\MX\OrganizationMXResponse;

/**
 * Class OrganizationMXProcessResponseTrait
 *
 * @package ProvulusSDK\Client\Request\Resource\Organization\MX
 */
trait OrganizationMXProcessResponseTrait
{

    /**
     * Process the data that is in the response
     *
     * @param array $jsonResponse
     *
     * @return OrganizationMXResponse
     *
     */
    public function processResponse(array $jsonResponse)
    {
        return new OrganizationMXResponse($jsonResponse['data']);
    }
}