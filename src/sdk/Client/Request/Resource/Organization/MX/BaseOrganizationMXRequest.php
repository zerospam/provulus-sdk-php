<?php
/**
 * Created by PhpStorm.
 * User: pbb
 * Date: 27/01/17
 * Time: 8:42 AM
 */

namespace ProvulusSDK\Client\Request\Resource\Organization\MX;

use ProvulusSDK\Client\Request\Resource\Organization\OrganizationRequest;

/**
 * Class OrganizationCollectionMXRequest
 *
 * Base class for Create and Index MX Requests
 *
 * @package ProvulusSDK\Client\Request\Resource\Organization\MX
 */
abstract class BaseOrganizationMXRequest extends OrganizationRequest
{
    /**
     * Base route for MX Requests
     *
     * @return string
     */
    function baseRoute()
    {
        return parent::baseRoute() . '/mxs';
    }
}