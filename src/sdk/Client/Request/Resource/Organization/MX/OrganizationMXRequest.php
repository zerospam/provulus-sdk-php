<?php
/**
 * Created by PhpStorm.
 * User: pbb
 * Date: 23/12/16
 * Time: 2:08 PM
 */

namespace ProvulusSDK\Client\Request\Resource\Organization\MX;

/**
 * Class OrganizationMXRequest
 *
 * Base class for Organization MX requests
 *
 * @package ProvulusSDK\Client\Request\Resource\Organization\MX
 */
abstract class OrganizationMXRequest extends BaseOrganizationMXRequest
{
    /**
     * Base route for reading a mx record.
     *
     * @return string
     */
    function baseRoute()
    {
        return parent::baseRoute() . '/:mxid';
    }

    /**
     * Sets the mx id for route binding
     *
     * @param $id
     *
     * @return $this
     */
    public function setMXId($id)
    {
        $this->addBinding('mxid', $id);

        return $this;
    }
}