<?php
/**
 * Created by PhpStorm.
 * User: pbb
 * Date: 27/01/17
 * Time: 9:06 AM
 */

namespace ProvulusSDK\Client\Request\Resource\Organization\MX;

/**
 * Class OrganizationMXSetAttributesTrait
 *
 * Trait for setting attributes
 *
 * @package ProvulusSDK\Client\Request\Organization\MX
 */
trait OrganizationMXSetAttributesTrait
{
    /**
     * @var string
     */
    private $name;

    /**
     * @var int
     */
    private $priority;

    /**
     * @param string $name
     *
     * @return $this
     */
    public function setName($name)
    {
        $this->name = $name;

        return $this;
    }

    /**
     * @param int $priority
     *
     * @return $this
     */
    public function setPriority($priority)
    {
        $this->priority = $priority;

        return $this;
    }
}