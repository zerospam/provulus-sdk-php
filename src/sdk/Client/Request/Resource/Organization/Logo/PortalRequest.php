<?php
/**
 * Created by PhpStorm.
 * User: aaflalo
 * Date: 17-09-13
 * Time: 15:00
 */

namespace ProvulusSDK\Client\Request\Resource\Organization\Logo;

use ProvulusSDK\Client\Request\BindableRequest;
use ProvulusSDK\Client\Request\RequestType;
use ProvulusSDK\Client\Response\IProvulusResponse;
use ProvulusSDK\Client\Response\Organization\Logo\PortalResponse;

/**
 * Class LogoRequest
 *
 * @method PortalResponse getResponse()
 *
 * @package ProvulusSDK\Client\Request\Resource\Organization\Logo
 */
class PortalRequest extends BindableRequest
{

    /**
     * Base route without binding
     *
     * @return string
     */
    function baseRoute()
    {
        return 'portal/:portalName';
    }

    /**
     * Set for which portal
     *
     * @param string $portalName
     *
     * @return $this
     */
    public function setPortalName($portalName)
    {
        if (!is_string($portalName)) {
            throw new \InvalidArgumentException('Portal name needs to be a string');

        }
        $this->addBinding('portalName', $portalName);

        return $this;
    }

    /**
     * Type of request
     *
     * @return RequestType
     */
    public function httpType()
    {
        return RequestType::HTTP_GET();
    }

    /**
     * Process the data that is in the response
     *
     * @param array $jsonResponse
     *
     * @return IProvulusResponse
     */
    public function processResponse(array $jsonResponse)
    {
        return new PortalResponse($jsonResponse);
    }
}
