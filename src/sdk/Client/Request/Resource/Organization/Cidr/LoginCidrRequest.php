<?php
/**
 * Created by PhpStorm.
 * User: ycoutu
 * Date: 18/04/18
 * Time: 10:22 AM
 */

namespace ProvulusSDK\Client\Request\Resource\Organization\Cidr;

abstract class LoginCidrRequest extends BaseLoginCidrRequest
{

    /**
     * Set the organization id for this request
     *
     * @param $id
     *
     * @return $this
     */
    public function setLoginCidrId($id)
    {
        $this->addBinding('logincidrId', $id);

        return $this;
    }

    /**
     * Base route for orgs resource
     *
     * @return string
     * @throws \Exception
     */
    function baseRoute()
    {
        if (!$this->hasBinding('logincidrId')) {
            throw new \Exception(sprintf('Login CIDR ID needs to be set for %s', get_class($this)));
        }

        return parent::baseRoute() . '/:logincidrId';
    }
}
