<?php
/**
 * Created by PhpStorm.
 * User: ycoutu
 * Date: 18/04/18
 * Time: 10:21 AM
 */

namespace ProvulusSDK\Client\Request\Resource\Organization\Cidr;

use ProvulusSDK\Client\Request\Resource\Organization\OrganizationRequest;

abstract class BaseLoginCidrRequest extends OrganizationRequest
{
    /**
     * Base route for orgs resource
     *
     * @return string
     * @throws \Exception
     */
    function baseRoute()
    {
        return parent::baseRoute() . '/logincidrs';
    }
}
