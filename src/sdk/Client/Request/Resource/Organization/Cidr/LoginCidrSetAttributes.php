<?php
/**
 * Created by PhpStorm.
 * User: ycoutu
 * Date: 18/04/18
 * Time: 10:48 AM
 */

namespace ProvulusSDK\Client\Request\Resource\Organization\Cidr;

trait LoginCidrSetAttributes
{
    /** @var string */
    private $cidr;

    /**
     * @param string $cidr
     *
     * @return $this
     */
    public function setCidr($cidr)
    {
        $this->cidr = $cidr;

        return $this;
    }
}
