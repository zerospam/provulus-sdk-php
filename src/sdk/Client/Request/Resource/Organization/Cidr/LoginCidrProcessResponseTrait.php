<?php
/**
 * Created by PhpStorm.
 * User: ycoutu
 * Date: 18/04/18
 * Time: 10:41 AM
 */

namespace ProvulusSDK\Client\Request\Resource\Organization\Cidr;

use ProvulusSDK\Client\Response\Organization\Cidr\LoginCidrResponse;

/**
 * Trait LoginCidrProcessResponseTrait
 *
 * @package ProvulusSDK\Client\Request\Resource\Organization\Cidr
 *
 * @method LoginCidrResponse getResponse()
 */
trait LoginCidrProcessResponseTrait
{
    /**
     * Process the data that is in the response
     *
     * @param array $jsonResponse
     *
     * @return LoginCidrResponse
     */
    public function processResponse(array $jsonResponse)
    {
        return new LoginCidrResponse($jsonResponse['data']);
    }
}
