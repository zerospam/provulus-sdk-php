<?php

namespace ProvulusSDK\Client\Request\Resource\Organization\Outbound;

use ProvulusSDK\Client\Response\EmptyResponse;
use ProvulusSDK\Client\Response\IProvulusResponse;
use ProvulusSDK\Client\Response\Organization\Outbound\OutboundResponse;

/**
 * Trait OutboundProcessResponseTrait
 *
 * @method OutboundResponse getResponse()
 *
 * @package ProvulusSDK\Client\Request\Resource\Organization\Outbound
 */
trait OutboundProcessResponseTrait
{
    /**
     * Process the data that is in the response
     *
     * @param array $jsonResponse
     *
     * @return IProvulusResponse
     */
    public function processResponse(array $jsonResponse)
    {
        if (empty($jsonResponse)) {
            return new EmptyResponse();
        }
        $included = isset($jsonResponse['included'])
            ? $jsonResponse['included']
            : [];
        return new OutboundResponse($jsonResponse['data'], $included);
    }
}
