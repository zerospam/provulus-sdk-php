<?php


namespace ProvulusSDK\Client\Request\Resource\Organization\Outbound;

use ProvulusSDK\Client\Request\Resource\Organization\OrganizationRequest;

abstract class OutboundRequest extends OrganizationRequest
{
    /**
     * Base route for orgs resource
     *
     * @return string
     * @throws \Exception
     */
    function baseRoute()
    {
        return parent::baseRoute() . '/outbound';
    }
}
