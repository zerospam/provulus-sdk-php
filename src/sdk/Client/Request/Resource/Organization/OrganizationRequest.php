<?php
/**
 * Created by PhpStorm.
 * User: aaflalo
 * Date: 16/12/16
 * Time: 1:59 PM
 */

namespace ProvulusSDK\Client\Request\Resource\Organization;


use ProvulusSDK\Client\Request\BindableRequest;

/**
 * Class OrganizationRequest
 *
 * Request having an organization as parent or is an organization request
 *
 * @package ProvulusSDK\Client\Request\Resource\Organization
 */
abstract class OrganizationRequest extends BindableRequest
{

    /**
     * Set the organization id for this request
     *
     * @param $id
     *
     * @return $this
     */
    public function setOrganizationId($id)
    {
        $this->addBinding('orgId', $id);

        return $this;
    }

    /**
     * Base route for orgs resource
     *
     * @return string
     * @throws \Exception
     */
    function baseRoute()
    {
        if (!$this->hasBinding('orgId')) {
            throw new \Exception(sprintf('Organization ID needs to be set for %s', get_class($this)));
        }

        return 'orgs/:orgId';
    }
}