<?php
/**
 * Created by PhpStorm.
 * User: ycoutu
 * Date: 21/06/17
 * Time: 9:47 AM
 */

namespace ProvulusSDK\Client\Request\Resource\Organization\Contact;

use ProvulusSDK\Client\Request\EmptyResponseTrait;
use ProvulusSDK\Client\Request\RequestType;
use ProvulusSDK\Client\Request\Resource\Organization\OrganizationRequest;
use ProvulusSDK\Enum\Contact\Mailgroups\MailGroupCategoriesEnum;

abstract class BaseContactUsRequest extends OrganizationRequest
{
    use EmptyResponseTrait;

    /** @var string */
    private $email;

    /** @var string */
    private $phone;

    /** @var MailGroupCategoriesEnum */
    private $helpTopic;

    /** @var string */
    private $subject;

    /** @var string */
    private $body;

    /** @var string */
    private $language;

    /**
     * Type of request
     *
     * @return RequestType
     */
    public function httpType()
    {
        return RequestType::HTTP_POST();
    }

    function baseRoute()
    {
        return parent::baseRoute() . '/contact';
    }

    /**
     * @param string $email
     *
     * @return BaseContactUsRequest
     */
    public function setEmail($email)
    {
        $this->email = $email;

        return $this;
    }

    /**
     * @param string $phone
     *
     * @return BaseContactUsRequest
     */
    public function setPhone($phone)
    {
        $this->phone = $phone;

        return $this;
    }

    /**
     * @param MailGroupCategoriesEnum $helpTopic
     *
     * @return BaseContactUsRequest
     */
    public function setHelpTopic(MailGroupCategoriesEnum $helpTopic)
    {
        $this->helpTopic = $helpTopic;

        return $this;
    }

    /**
     * @param string $subject
     *
     * @return BaseContactUsRequest
     */
    public function setSubject($subject)
    {
        $this->subject = $subject;

        return $this;
    }

    /**
     * @param string $body
     *
     * @return BaseContactUsRequest
     */
    public function setBody($body)
    {
        $this->body = $body;

        return $this;
    }

    /**
     * @param string $body
     *
     * @return BaseContactUsRequest
     */
    public function setLanguage($language)
    {
        $this->language = $language;

        return $this;
    }

    /**
     * @param $attachment
     *
     * @return $this
     */
    public function setAttachment($attachment)
    {
        $this->addFile($attachment);

        return $this;
    }
}
