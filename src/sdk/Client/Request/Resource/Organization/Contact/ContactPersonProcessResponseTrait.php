<?php
/**
 * Created by PhpStorm.
 * User: pbb
 * Date: 30/01/17
 * Time: 9:25 AM
 */

namespace ProvulusSDK\Client\Request\Resource\Organization\Contact;

use ProvulusSDK\Client\Response\Organization\Contact\ContactPersonResponse;

/**
 * Class ContactPersonProcessResponseTrait
 *
 * @method ContactPersonResponse getResponse()
 *
 * @package ProvulusSDK\Client\Request\Resource\Organization\Contact
 */
trait ContactPersonProcessResponseTrait
{

    /**
     * Process the data that is in the response
     *
     * @param array $jsonResponse
     *
     * @return ContactPersonResponse
     *
     */
    public function processResponse(array $jsonResponse)
    {
        return new ContactPersonResponse($jsonResponse['data']);
    }
}