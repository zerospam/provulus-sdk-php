<?php
/**
 * Created by PhpStorm.
 * User: aaflalo
 * Date: 16/12/16
 * Time: 2:13 PM
 */

namespace ProvulusSDK\Client\Request\Resource\Organization\Contact;

abstract class ContactPersonRequest extends BaseContactPersonRequest
{

    /**
     * Set the contact person of the request
     *
     * @param $id
     *
     * @return $this
     */
    public function setContactPersonId($id)
    {
        $this->addBinding('contactPersonId', $id);

        return $this;
    }

    /**
     * Return base route for ContactPerson requests
     *
     * @return string
     * @throws \Exception
     */
    function baseRoute()
    {
        return parent::baseRoute() . '/:contactPersonId';
    }
}
