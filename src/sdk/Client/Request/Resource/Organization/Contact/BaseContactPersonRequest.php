<?php
/**
 * Created by PhpStorm.
 * User: pbb
 * Date: 27/01/17
 * Time: 2:04 PM
 */

namespace ProvulusSDK\Client\Request\Resource\Organization\Contact;

use ProvulusSDK\Client\Request\Resource\Organization\OrganizationRequest;

/**
 * Class ContactPersonCollectionRequest
 *
 * Base class for ContactPersonRequest
 *
 * @package ProvulusSDK\Client\Request\Resource\Organization\Contact
 */
abstract class BaseContactPersonRequest extends OrganizationRequest
{
    /**
     * Return base route for ContactPerson requests
     *
     * @return string
     * @throws \Exception
     */
    function baseRoute()
    {
        return parent::baseRoute() . '/contacts';
    }
}
