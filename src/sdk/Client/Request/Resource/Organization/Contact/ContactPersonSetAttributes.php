<?php
/**
 * Created by PhpStorm.
 * User: pbb
 * Date: 27/01/17
 * Time: 2:15 PM
 */

namespace ProvulusSDK\Client\Request\Resource\Organization\Contact;

use ProvulusSDK\Enum\Contact\Mailgroups\MailGroupEnum;
use ProvulusSDK\Enum\Locale\LocaleEnum;

/**
 * Class ContactPersonSetAttributes
 *
 * Used when setting attributes is required (Update / Create Request)
 *
 * @package ProvulusSDK\Client\Request\Resource\Organization\Contact
 */
trait ContactPersonSetAttributes
{

    /**
     * @var string
     */
    private $title, $email, $phone;

    /** @var MailGroupEnum[] */
    private $mailgroups = [];

    /**
     * @var LocaleEnum
     */
    private $language;

    /**
     * @param string $title
     *
     * @return $this
     */
    public function setTitle($title)
    {
        $this->title = $title;

        return $this;
    }

    /**
     * @param string $email
     *
     * @return $this
     */
    public function setEmail($email)
    {
        $this->email = $email;

        return $this;
    }

    /**
     * @param string $phone
     *
     * @return $this
     */
    public function setPhone($phone)
    {
        $this->phone = $phone;

        $this->nullableChanged();

        return $this;
    }

    /**
     * @param LocaleEnum $language
     *
     * @return $this
     */
    public function setLanguage($language)
    {
        $this->language = $language;

        return $this;
    }

    /**
     * Set mailgroups
     *
     * @param array $mailgroups
     *
     * @return $this
     */
    public function setMailgroups(array $mailgroups)
    {
        $this->mailgroups = $this->checkMailgroups($mailgroups);

        return $this;
    }

    /**
     * Check mailgroups
     *
     * @param MailGroupEnum[] $mailgroups
     *
     * @return array|MailGroupEnum[]
     */
    private function checkMailgroups(array $mailgroups)
    {
        $enumValues = array_count_values(
            array_map(
                function (MailGroupEnum $enum) {
                    return $enum->getValue();
                }, $mailgroups)
        );

        foreach ($enumValues as $enum => $numberOfOccurrences) {
            if ($numberOfOccurrences > 1) {
                throw new \InvalidArgumentException('Cannot set mailgroup more than once.');
            }
        }

        return $mailgroups;
    }
}
