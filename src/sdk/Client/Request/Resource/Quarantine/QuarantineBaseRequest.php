<?php
/**
 * Created by PhpStorm.
 * User: ycoutu
 * Date: 09/11/17
 * Time: 4:39 PM
 */

namespace ProvulusSDK\Client\Request\Resource\Quarantine;

use ProvulusSDK\Client\Request\BindableRequest;
use ProvulusSDK\Enum\Quarantine\Type\QuarantineTypeEnum;

abstract class QuarantineBaseRequest extends BindableRequest
{
    /** @var QuarantineTypeEnum */
    private $quarantineType;

    /**
     * Set the organization id for this request
     *
     * @param $id
     *
     * @return $this
     */
    public function setOrganizationId($id)
    {
        $this->addBinding('orgId', $id);

        return $this;
    }

    /**
     * Set the user ID for this request
     *
     * @param $id
     *
     * @return $this
     */
    public function setUserId($id)
    {
        $this->addBinding('userId', $id);

        return $this;
    }

    /**
     * Set the quarantine type to use
     *
     * @param QuarantineTypeEnum $quarantineTypeEnum
     *
     * @return $this
     */
    public function setQuarantineType(QuarantineTypeEnum $quarantineTypeEnum)
    {
        $this->quarantineType = $quarantineTypeEnum;

        return $this;
    }

    /**
     * Base route for orgs resource
     *
     * @return string
     * @throws \Exception
     */
    function baseRoute()
    {
        $quarType = $this->quarantineType->buildType();
        foreach ($quarType->getRequiredBindings() as $binding) {
            if (!$this->hasBinding($binding)) {
                throw new \Exception(sprintf('%s needs to be set for %s', $binding, get_class($this)));
            }
        }

        return $quarType->getRoute() . '/quar';
    }
}
