<?php
/**
 * Created by PhpStorm.
 * User: aaflalo
 * Date: 27/03/17
 * Time: 10:09 AM
 */

namespace ProvulusSDK\Client\Request\Resource\Quarantine;


use Carbon\Carbon;
use ProvulusSDK\Client\Args\Search\Quarantine\Flags\Tags\IsNoTagArgument;
use ProvulusSDK\Client\Args\Search\Quarantine\Flags\Tags\IsTagBannedFile;
use ProvulusSDK\Client\Args\Search\Quarantine\Flags\Status\IsStatusDeclaredAsSpamArgument;
use ProvulusSDK\Client\Args\Search\Quarantine\Flags\Status\IsStatusDeletedArgument;
use ProvulusSDK\Client\Args\Search\Quarantine\Flags\Tags\IsTagGreymail;
use ProvulusSDK\Client\Args\Search\Quarantine\Flags\Tags\IsTagPhishArgument;
use ProvulusSDK\Client\Args\Search\Quarantine\Flags\Status\IsStatusReleasedArgument;
use ProvulusSDK\Client\Args\Search\Quarantine\Flags\Status\IsNoStatusArgument;
use ProvulusSDK\Client\Args\Search\Quarantine\FromArgument;
use ProvulusSDK\Client\Args\Search\Quarantine\NotRuleTagsArgument;
use ProvulusSDK\Client\Args\Search\Quarantine\RuleTagsArgument;
use ProvulusSDK\Client\Args\Search\Quarantine\SAScore\LowerScoreArgument;
use ProvulusSDK\Client\Args\Search\Quarantine\SAScore\UpperScoreArgument;
use ProvulusSDK\Client\Args\Search\Quarantine\SubjectArgument;
use ProvulusSDK\Client\Args\Search\Quarantine\Time\LowerLimitReceivedAtArgument;
use ProvulusSDK\Client\Args\Search\Quarantine\Time\UpperLimitReceivedAtArgument;
use ProvulusSDK\Client\Args\Search\Quarantine\ToArgument;

/**
 * Class QuarantineIndexTrait
 *
 * @package ProvulusSDK\Client\Request\Quarantine
 */
trait QuarantineIndexSearchTrait
{
    /**
     * Show the declared as spam
     *
     * @param bool $value
     *
     * @return $this
     */
    public function isStatusDeclaredAsSpam($value = true)
    {
        return $this->addArgument(new IsStatusDeclaredAsSpamArgument($value));
    }

    /**
     * Show the deleted
     *
     * @param bool $value
     *
     * @return $this
     */
    public function isStatusDeleted($value = true)
    {
        return $this->addArgument(new IsStatusDeletedArgument($value));
    }

    /**
     * Show the Released
     *
     * @param bool $value
     *
     * @return $this
     */
    public function isStatusReleased($value = true)
    {
        return $this->addArgument(new IsStatusReleasedArgument($value));
    }

    /**
     * Show the Normal quarantine
     *
     * @param bool $value
     *
     * @return $this
     */
    public function isNoStatus($value = true)
    {
        return $this->addArgument(new IsNoStatusArgument($value));
    }

    /**
     * Is the mail a phish
     *
     * @param bool $value
     *
     * @return $this
     */
    public function isTagPhish($value = true)
    {
        return $this->addArgument(new IsTagPhishArgument($value));
    }

    /**
     * Is the mail banned
     *
     * @param bool $value
     *
     * @return $this
     */
    public function isTagBannedFile($value = true)
    {
        return $this->addArgument(new IsTagBannedFile($value));
    }

    /**
     * Should greymail tagged email be included
     *
     * @param bool $value
     *
     * @return $this
     */
    public function isTagGreymail($value = true)
    {
        return $this->addArgument(new IsTagGreymail($value));
    }

    /**
     * Should spam without tags be included
     *
     * @param bool $value
     *
     * @return $this
     */
    public function isNoTag($value = true)
    {
        return $this->addArgument(new IsNoTagArgument($value));
    }

    /**
     * Score
     *
     * @param int|null $from one of the two parameter need to be set
     * @param int|null $to   one of the two parameter need to be set
     *
     * @return $this
     * @internal param int $value
     *
     */
    public function setScore($from = null, $to = null)
    {
        if (is_null($from) && is_null($to)) {
            throw new \InvalidArgumentException('One of the two parameter need to be set');
        }

        if (!is_null($from)) {
            $this->addArgument(new LowerScoreArgument($from));
        }

        if (!is_null($to)) {
            $this->addArgument(new UpperScoreArgument($to));
        }

        return $this;
    }


    /**
     * Who send the the message
     *
     * @param string $value
     *
     * @return $this
     */
    public function addFrom($value)
    {
        return $this->addArgument(new FromArgument($value));
    }

    /**
     * Who received the the message
     *
     * @param string $value
     *
     * @return $this
     */
    public function addTo($value)
    {
        return $this->addArgument(new ToArgument($value));
    }

    /**
     * Subject of the message
     *
     * @param string $value
     *
     * @return $this
     */
    public function setSubject($value)
    {
        return $this->addArgument(new SubjectArgument($value));
    }

    /**
     * Who send the the message
     *
     * @param Carbon|null $from one of the two parameter need to be set
     * @param Carbon|null $to   one of the two parameter need to be set
     *
     * @return $this
     * @internal param string $value
     *
     */
    public function setReceivedAt(Carbon $from = null, Carbon $to = null)
    {
        if (is_null($from) && is_null($to)) {
            throw new \InvalidArgumentException('One of the two parameter need to be set');
        }

        if ($from) {
            $this->addArgument(new LowerLimitReceivedAtArgument($from->toDateTimeString()));
        }

        if ($to) {
            $this->addArgument(new UpperLimitReceivedAtArgument($to->toDateTimeString()));
        }

        return $this;
    }

    /**
     * Rule tags of the message
     *
     * @param string $value
     *
     * @return $this
     */
    public function setRuleTags($value)
    {
        return $this->addArgument(new RuleTagsArgument($value));
    }

    /**
     * Rule tags absents from the message
     *
     * @param string $value
     *
     * @return $this
     */
    public function setNotRuleTags($value)
    {
        return $this->addArgument(new NotRuleTagsArgument($value));
    }
}