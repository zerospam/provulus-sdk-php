<?php
/**
 * Created by PhpStorm.
 * User: pbb
 * Date: 21/02/17
 * Time: 3:00 PM
 */

namespace ProvulusSDK\Client\Request\Resource;

use ProvulusSDK\Client\Args\Search\Renewal\RenewalStateArgument;
use ProvulusSDK\Client\Request\BaseProvulusRequest;
use ProvulusSDK\Client\Request\Collection\CollectionPaginatedTrait;
use ProvulusSDK\Client\Request\Collection\IProvulusCollectionPaginatedRequest;
use ProvulusSDK\Client\Request\Collection\SearchableTrait;
use ProvulusSDK\Client\Request\Organization\Traits\WithDomains;
use ProvulusSDK\Client\Request\RequestType;
use ProvulusSDK\Client\Response\IProvulusObjectResponse;
use ProvulusSDK\Client\Response\Organization\OrganizationCollectionResponse;
use ProvulusSDK\Client\Response\Organization\OrganizationResponse;
use ProvulusSDK\Enum\Renewal\PaymentRenewalStateEnum;

/**
 * Class RenewalOrganizationsRequest
 *
 * Request for fetching organizations depending on their renewal state
 *
 * @method OrganizationCollectionResponse getResponse()
 *
 * @package ProvulusSDK\Client\Request\Resource
 */
class RenewalOrganizationsIndexRequest extends BaseProvulusRequest implements IProvulusCollectionPaginatedRequest
{

    use CollectionPaginatedTrait, SearchableTrait, WithDomains;

    /**
     * The url of the route
     *
     * @return string
     */
    public function routeUrl()
    {
        return 'renewalOrgs';
    }

    /**
     * Type of request
     *
     * @return RequestType
     */
    public function httpType()
    {
        return RequestType::HTTP_GET();
    }

    /**
     * Used to parse each result of the collection
     *
     * Need to be overridden by the class
     *
     * @param array $objectData
     *
     * @param array $includedData
     *
     * @return IProvulusObjectResponse
     */
    function toResponse(array $objectData, array $includedData = [])
    {
        return new OrganizationResponse($objectData, $includedData);
    }

    /**
     * Class to be used to build the collection
     *
     * @return string
     */
    function collectionClass()
    {
        return OrganizationCollectionResponse::class;
    }

    /**
     * Set the renewal state parameter
     *
     * @param $enum PaymentRenewalStateEnum to set or null if you want to unset
     *
     * @return $this
     */
    public function setRenewalStateParameter($enum)
    {
        if (is_null($enum)) {
            return $this->removeArgument(new RenewalStateArgument(PaymentRenewalStateEnum::NO_DOMAIN()));
        }

        return $this->addArgument(new RenewalStateArgument($enum));
    }
}