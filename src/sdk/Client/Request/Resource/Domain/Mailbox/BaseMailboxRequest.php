<?php
/**
 * Created by PhpStorm.
 * User: ycoutu
 * Date: 28/02/19
 * Time: 1:53 PM
 */

namespace ProvulusSDK\Client\Request\Resource\Domain\Mailbox;

use ProvulusSDK\Client\Request\Resource\Domain\DomainRequest;

abstract class BaseMailboxRequest extends DomainRequest
{
    /**
     * Route for creating a domain
     *
     * @return string
     * @throws \Exception
     */
    function baseRoute()
    {
        return parent::baseRoute() . '/mailboxes';
    }
}
