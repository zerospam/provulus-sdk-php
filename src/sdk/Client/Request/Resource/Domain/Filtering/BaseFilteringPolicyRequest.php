<?php
/**
 * Created by PhpStorm.
 * User: pbb
 * Date: 26/01/17
 * Time: 9:19 AM
 */

namespace ProvulusSDK\Client\Request\Resource\Domain\Filtering;

use ProvulusSDK\Client\Request\Resource\Organization\OrganizationRequest;

/**
 * Class FilteringPolicyCollectionRequest
 *
 * Base class for create and Index Requests
 *
 * @package ProvulusSDK\Client\Request\Resource\Domain\Filtering
 */
abstract class BaseFilteringPolicyRequest extends OrganizationRequest
{

    /**
     * Base route for filtering policy requests
     *
     * @return string
     */
    function baseRoute()
    {
        return parent::baseRoute() . '/policies';
    }
}