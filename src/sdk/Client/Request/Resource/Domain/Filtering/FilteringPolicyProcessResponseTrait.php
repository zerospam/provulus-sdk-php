<?php
/**
 * Created by PhpStorm.
 * User: pbb
 * Date: 30/01/17
 * Time: 8:52 AM
 */

namespace ProvulusSDK\Client\Request\Resource\Domain\Filtering;

use ProvulusSDK\Client\Response\Domain\Filtering\FilteringPolicyResponse;

/**
 * Class FilteringPolicyProcessResponseTrait
 *
 * Trait for filtering policy process response
 *
 * @package ProvulusSDK\Client\Request\Resource\Domain\Filtering
 */
trait FilteringPolicyProcessResponseTrait
{

    /**
     * Process the data that is in the response
     *
     * @param array $jsonResponse
     *
     * @return FilteringPolicyResponse
     *
     */
    public function processResponse(array $jsonResponse)
    {
        $included = isset($jsonResponse['included'])
            ? $jsonResponse['included']
            : [];

        return new FilteringPolicyResponse($jsonResponse['data'], $included);
    }
}