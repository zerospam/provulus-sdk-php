<?php
/**
 * Created by PhpStorm.
 * User: pbb
 * Date: 26/01/17
 * Time: 9:16 AM
 */

namespace ProvulusSDK\Client\Request\Resource\Domain\Filtering;

use ProvulusSDK\Enum\Filtering\FilteringPolicyEnum;
use ProvulusSDK\Enum\Filtering\GreymailSensitivityEnum;

/**
 * Class FilteringPolicySetTrait
 *
 * Trait including needed attributes and setters for update and create requests
 *
 * @package ProvulusSDK\Client\Request\Domain\Filtering
 */
trait FilteringPolicySetTrait
{
    /**
     * @var string
     */
    private $name, $description;

    /**
     * @var FilteringPolicyEnum
     */
    private $filterTypeCode;

    /**
     * @var bool
     */
    private $isUsingGreylist, $isLdapFilteringEnabled, $isContentFilteringEnabled;

    /**
     * @var int
     */
    private $maxMessageSizeInMb;

    /** @var int[] */
    private $enabledCategories;

    /** @var GreymailSensitivityEnum */
    private $greymailSensitivity;

    /**
     * @param string $name
     *
     * @return $this
     */
    public function setName($name)
    {
        $this->name = $name;

        return $this;
    }

    /**
     * @param string $description
     *
     * @return $this
     */
    public function setDescription($description)
    {
        $this->description = $description;

        return $this;
    }

    /**
     * @param FilteringPolicyEnum $filterTypeCode
     *
     * @return $this
     */
    public function setFilterTypeCode($filterTypeCode)
    {
        $this->filterTypeCode = $filterTypeCode;

        return $this;
    }

    /**
     * @param bool $isUsingGreylist
     *
     * @return $this
     */
    public function setIsUsingGreylist($isUsingGreylist)
    {
        $this->isUsingGreylist = $isUsingGreylist;

        return $this;
    }

    /**
     * @param bool $isLdapFilteringEnabled
     *
     * @return $this
     */
    public function setIsLdapFilteringEnabled($isLdapFilteringEnabled)
    {
        $this->isLdapFilteringEnabled = $isLdapFilteringEnabled;

        return $this;
    }

    /**
     * @param bool $isContentFilteringEnabled
     *
     * @return $this
     */
    public function setIsContentFilteringEnabled($isContentFilteringEnabled)
    {
        $this->isContentFilteringEnabled = $isContentFilteringEnabled;

        return $this;
    }

    /**
     * @param int $maxMessageSizeInMb
     *
     * @return $this
     */
    public function setMaxMessageSizeInMb($maxMessageSizeInMb)
    {
        $this->maxMessageSizeInMb = $maxMessageSizeInMb;

        return $this;
    }

    public function setEnabledCategories(array $enabledCategories)
    {
        $this->enabledCategories = $enabledCategories;

        return $this;
    }

    /**
     * @param GreymailSensitivityEnum $greymailSensitivity
     *
     * @return $this
     */
    public function setGreymailSensitivity(GreymailSensitivityEnum $greymailSensitivity)
    {
        $this->greymailSensitivity = $greymailSensitivity;

        return $this;
    }

}