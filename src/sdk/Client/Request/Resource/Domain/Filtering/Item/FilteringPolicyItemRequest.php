<?php
/**
 * Created by PhpStorm.
 * User: pbb
 * Date: 22/12/16
 * Time: 1:43 PM
 */

namespace ProvulusSDK\Client\Request\Resource\Domain\Filtering\Item;


/**
 * Class FilteringPolicyItemRequest
 *
 * @package ProvulusSDK\Client\Request\Resource\Domain\Filtering\Item
 */
abstract class FilteringPolicyItemRequest extends BaseFilteringPolicyItemRequest
{

    /**
     * Base route for FilteringPolicyItemRequest
     *
     * @return string
     * @throws \Exception
     */
    function baseRoute()
    {
        return parent::baseRoute() . '/:policyitem';
    }

    /**
     * Sets the filtering policy item id in the request route.
     *
     * @param $id
     *
     * @return $this
     */
    public function setFilterItemId($id)
    {
        $this->addBinding('policyitem', $id);

        return $this;
    }
}