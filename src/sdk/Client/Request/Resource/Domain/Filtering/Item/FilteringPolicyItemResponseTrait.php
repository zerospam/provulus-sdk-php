<?php
/**
 * Created by PhpStorm.
 * User: pbb
 * Date: 27/01/17
 * Time: 4:20 PM
 */

namespace ProvulusSDK\Client\Request\Resource\Domain\Filtering\Item;

use ProvulusSDK\Client\Response\Domain\Filtering\Item\FilteringPolicyItemResponse;

/**
 * Class FilteringPolicyItemResponseTrait
 *
 * Trait for managing FilteringPolicyItemResponse
 *
 * @package ProvulusSDK\Client\Request\Resource\Domain\Filtering\Item
 */
trait FilteringPolicyItemResponseTrait
{
    /**
     * @param array $jsonResponse
     *
     * @return FilteringPolicyItemResponse
     *
     */
    public function processResponse(array $jsonResponse)
    {
        return new FilteringPolicyItemResponse($jsonResponse['data']);
    }
}