<?php
/**
 * Created by PhpStorm.
 * User: pbb
 * Date: 06/01/17
 * Time: 10:12 AM
 */

namespace ProvulusSDK\Client\Request\Resource\Domain\Filtering\Item;

use MabeEnum\Enum;
use ProvulusSDK\Client\Request\Resource\PrimalValued;
use ProvulusSDK\Client\Request\Resource\PrimalValuedEnumTrait;

/**
 * Class FilteringPolicyItemRequestType
 *
 * Type of requests corresponding to types
 *
 * @method static FilteringPolicyItemRequestTypeEnum  DOMAIN()
 * @method static FilteringPolicyItemRequestTypeEnum  ADDRESS()
 * @method static FilteringPolicyItemRequestTypeEnum  ALL()
 *
 * @package ProvulusSDK\Client\Request\Resource\Domain\Filtering\Item
 */
class FilteringPolicyItemRequestTypeEnum extends Enum implements PrimalValued
{
    use PrimalValuedEnumTrait;

    const DOMAIN  = 'policyitemsdomain';
    const ADDRESS = 'policyitemsaddress';
    const ALL     = 'policyitems';
}