<?php
/**
 * Created by PhpStorm.
 * User: pbb
 * Date: 26/01/17
 * Time: 10:11 AM
 */

namespace ProvulusSDK\Client\Request\Resource\Domain\Filtering\Item;

use ProvulusSDK\Client\Request\Resource\Domain\Filtering\FilteringPolicyRequest;

/**
 * Class FilteringPolicyItemCollectionRequest
 *
 * Base class for root of FilteringPolicyItem Route
 * i.e. org/1/filters/1/items
 *
 * @package ProvulusSDK\Client\Request\Resource\Domain\Filtering\Item
 */
abstract class BaseFilteringPolicyItemRequest extends FilteringPolicyRequest
{
    /**
     * Base route for FilteringPolicyItemRequest
     *
     * @return string
     * @throws \Exception
     */
    function baseRoute()
    {
        if (!$this->hasBinding('requestType')) {
            throw new \Exception('request Type needs to be set for filtering policy item request.');
        }

        return parent::baseRoute() . '/:requestType';
    }

    /**
     * Sets the request type ; will be used for resource binding in the route
     *
     * @param FilteringPolicyItemRequestTypeEnum $requestType
     *
     * @return $this
     */
    public function setRequestType(FilteringPolicyItemRequestTypeEnum $requestType)
    {
        $this->addBinding('requestType', $requestType->getValue());

        return $this;
    }
}