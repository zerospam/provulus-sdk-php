<?php
/**
 * Created by PhpStorm.
 * User: ycoutu
 * Date: 30/08/17
 * Time: 9:26 AM
 */

namespace ProvulusSDK\Client\Request\Resource\Domain\Filtering\FilteringList;

use ProvulusSDK\Client\Response\Domain\Filtering\FilteringList\FilteringListResponse;

trait FilteringListProcessResponseTrait
{
    /**
     * Process the data that is in the response
     *
     * @param array $jsonResponse
     *
     * @return FilteringListResponse
     *
     */
    public function processResponse(array $jsonResponse)
    {
        return new FilteringListResponse($jsonResponse['data']);
    }
}
