<?php
/**
 * Created by PhpStorm.
 * User: ycoutu
 * Date: 30/08/17
 * Time: 9:20 AM
 */

namespace ProvulusSDK\Client\Request\Resource\Domain\Filtering\FilteringList;

use Carbon\Carbon;
use ProvulusSDK\Enum\Filtering\FilteringListActionEnum;

trait FilteringListSetTrait
{
    /** @var bool */
    private $isEnabled;

    /** @var string */
    private $value;

    /** @var string|null */
    private $expiresAt;

    /** @var string|null */
    private $description;

    /** @var FilteringListActionEnum */
    private $action;

    /**
     * @param bool $isEnabled
     *
     * @return FilteringListSetTrait
     */
    public function setIsEnabled($isEnabled)
    {
        $this->isEnabled = $isEnabled;

        return $this;
    }

    /**
     * @param string $value
     *
     * @return FilteringListSetTrait
     */
    public function setValue($value)
    {
        $this->value = $value;

        return $this;
    }

    /**
     * @param null|Carbon $expiresAt
     *
     * @return WhitelistSetTrait
     */
    public function setExpiresAt($expiresAt)
    {
        $this->expiresAt = $expiresAt;
        $this->nullableChanged();

        return $this;
    }

    /**
     * @param null|string $description
     *
     * @return FilteringListSetTrait
     */
    public function setDescription($description)
    {
        $this->description = $description;
        $this->nullableChanged();

        return $this;
    }

    /**
     * @param FilteringListActionEnum $action
     *
     * @return FilteringListSetTrait
     */
    public function setAction(FilteringListActionEnum $action)
    {
        $this->action = $action;

        return $this;
    }
}
