<?php
/**
 * Created by PhpStorm.
 * User: ycoutu
 * Date: 29/08/17
 * Time: 4:39 PM
 */

namespace ProvulusSDK\Client\Request\Resource\Domain\Filtering\FilteringList;

use ProvulusSDK\Client\Request\Resource\Domain\Filtering\FilteringPolicyRequest;

abstract class BaseFilteringListRequest extends FilteringPolicyRequest
{
    /**
     * Base route for FilteringListRequest
     *
     * @return string
     * @throws \Exception
     */
    function baseRoute()
    {
        if (!$this->hasBinding('filteringListType')) {
            throw new \Exception('filtering list Type needs to be set for filtering list request.');
        }
        return parent::baseRoute() . '/:filteringListType';
    }

    /**
     * Sets the filtering list type ; will be used for resource binding in the route
     *
     * @param FilteringListRequestTypeEnum $filteringListRequestTypeEnum
     *
     * @return $this
     */
    public function setFilteringListType(FilteringListRequestTypeEnum $filteringListRequestTypeEnum)
    {
        $this->addBinding('filteringListType', $filteringListRequestTypeEnum->getValue());

        return $this;
    }
}
