<?php
/**
 * Created by PhpStorm.
 * User: ycoutu
 * Date: 29/08/17
 * Time: 4:28 PM
 */

namespace ProvulusSDK\Client\Request\Resource\Domain\Filtering\FilteringList;

use MabeEnum\Enum;
use ProvulusSDK\Client\Request\Resource\PrimalValued;
use ProvulusSDK\Client\Request\Resource\PrimalValuedEnumTrait;

/**
 * Class FilteringListRequestTypeEnum
 *
 * @method static FilteringListRequestTypeEnum AUTHORIZED_ADDRESS()
 * @method static FilteringListRequestTypeEnum FILE_EXTENSION()
 * @method static FilteringListRequestTypeEnum INBOUND_RECIPIENT()
 * @method static FilteringListRequestTypeEnum INBOUND_SENDER()
 * @method static FilteringListRequestTypeEnum IP()
 * @method static FilteringListRequestTypeEnum MIME()
 * @method static FilteringListRequestTypeEnum OUTBOUND_RECIPIENT()
 * @method static FilteringListRequestTypeEnum OUTBOUND_SENDER()
 * @method static FilteringListRequestTypeEnum SPF()
 * @method static FilteringListRequestTypeEnum BANNED_FILE_SENDER()
 *
 * @package ProvulusSDK\Client\Request\Resource\Domain\Filtering\FilteringList
 */
class FilteringListRequestTypeEnum extends Enum implements PrimalValued
{
    use PrimalValuedEnumTrait;

    const AUTHORIZED_ADDRESS = 'auth-addrs';
    const FILE_EXTENSION     = 'file-exts';
    const INBOUND_RECIPIENT  = 'in-rcpts';
    const INBOUND_SENDER     = 'in-snds';
    const IP                 = 'ips';
    const MIME               = 'mimes';
    const OUTBOUND_RECIPIENT = 'out-rcpts';
    const OUTBOUND_SENDER    = 'out-snds';
    const SPF                = 'spfs';
    const BANNED_FILE_SENDER = 'banned-file-snds';
}
