<?php /** @noinspection ALL */

/**
 * Created by PhpStorm.
 * User: ycoutu
 * Date: 30/08/17
 * Time: 9:36 AM
 */

namespace ProvulusSDK\Client\Request\Resource\Domain\Filtering\FilteringList;

abstract class FilteringListRequest extends BaseFilteringListRequest
{
    /**
     * Sets the filtering list id
     *
     * @param $id
     *
     * @return $this
     */
    public function setFilteringListId($id)
    {
        $this->addBinding('filteringListId', $id);

        return $this;
    }

    /**
     * Base route for filtering list requests
     *
     * @return string
     */
    function baseRoute()
    {
        return parent::baseRoute() . '/:filteringListId';
    }
}
