<?php
/**
 * Created by PhpStorm.
 * User: pbb
 * Date: 21/12/16
 * Time: 2:23 PM
 */

namespace ProvulusSDK\Client\Request\Resource\Domain\Filtering;

/**
 * Class FilteringPolicyRequest
 *
 * Base class for Filtering Policy Requests with ID resource
 *
 * @package ProvulusSDK\Client\Request\Resource\Domain\Filtering
 */
abstract class FilteringPolicyRequest extends BaseFilteringPolicyRequest
{
    /**
     * Sets the filtering id
     *
     * @param $id
     *
     * @return $this
     */
    public function setFilteringId($id)
    {
        $this->addBinding('policyId', $id);

        return $this;
    }

    /**
     * Base route for filtering policy requests
     *
     * @return string
     */
    function baseRoute()
    {
        return parent::baseRoute() . '/:policyId';
    }
}