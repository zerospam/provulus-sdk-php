<?php
/**
 * Created by PhpStorm.
 * User: pbb
 * Date: 20/12/16
 * Time: 2:13 PM
 */

namespace ProvulusSDK\Client\Request\Resource\Domain;


use ProvulusSDK\Client\Request\Resource\Organization\OrganizationRequest;

/**
 * Base class for Domain Request
 *
 * @package ProvulusSDK\Client\Request\Domain\Base
 */
abstract class BaseDomainRequest extends OrganizationRequest
{

    /**
     * Route for creating a domain
     *
     * @return string
     * @throws \Exception
     */
    function baseRoute()
    {
        return parent::baseRoute() . '/domains';
    }
}