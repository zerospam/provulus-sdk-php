<?php
/**
 * Created by PhpStorm.
 * User: aaflalo
 * Date: 16/12/16
 * Time: 2:05 PM
 */

namespace ProvulusSDK\Client\Request\Resource\Domain;


/**
 * Class DomainRequest
 *
 * Request having a domain as parent or is a domain request
 *
 * @package ProvulusSDK\Client\Request\Resource\Domain
 */
abstract class DomainRequest extends BaseDomainRequest
{
    /**
     * Route for the request
     *
     * @return string
     */
    public function baseRoute()
    {
        return parent::baseRoute() . '/:domainId';
    }

    /**
     * Set the domain for the request
     *
     * @param $id
     *
     * @return $this
     */
    public function setDomainId($id)
    {
        $this->addBinding('domainId', $id);

        return $this;
    }
}