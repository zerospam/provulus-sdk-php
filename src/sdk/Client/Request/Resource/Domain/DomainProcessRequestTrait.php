<?php
/**
 * Created by PhpStorm.
 * User: pbb
 * Date: 30/01/17
 * Time: 9:04 AM
 */

namespace ProvulusSDK\Client\Request\Resource\Domain;

use ProvulusSDK\Client\Response\Domain\DomainResponse;

/**
 * Class DomainProcessRequestTrait
 *
 * @method DomainResponse getResponse()
 *
 * @package ProvulusSDK\Client\Request\Resource\Domain
 */
trait DomainProcessRequestTrait
{

    /**
     * Process the data that is in the response
     *
     * @param array $jsonResponse
     *
     * @return DomainResponse
     *
     */
    public function processResponse(array $jsonResponse)
    {
        return new DomainResponse($jsonResponse['data']);
    }
}