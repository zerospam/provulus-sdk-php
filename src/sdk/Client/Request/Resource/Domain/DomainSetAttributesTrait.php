<?php
/**
 * Created by PhpStorm.
 * User: pbb
 * Date: 25/01/17
 * Time: 5:33 PM
 */

namespace ProvulusSDK\Client\Request\Resource\Domain;

use ProvulusSDK\Enum\Domain\DomainTransportProtocolEnum;
use ProvulusSDK\Enum\Domain\DomainTypeEnum;

/**
 * Class DomainAttributesTrait
 *
 * Trait for Create and Update Domain
 *
 * @package ProvulusSDK\Client\Request\Domain
 */
trait DomainSetAttributesTrait
{
    /**
     * @var string
     */
    private $transport, $email, $description;

    /**
     * @var DomainTypeEnum
     */
    private $domainType;

    /**
     * @var int
     */
    private $transportPort, $domainGroupId;

    /**
     * @var bool
     */
    private $transportHold, $isUnconditionalDelete, $transportUseMx;

    /**
     * @var DomainTransportProtocolEnum
     */
    private $transportProtocol;

    /**
     * @param string $transport
     *
     * @return $this
     */
    public function setTransport($transport)
    {
        $this->transport = $transport;

        return $this;
    }

    /**
     * @param string $email
     *
     * @return $this
     */
    public function setEmail($email)
    {
        $this->email = $email;

        return $this;
    }

    /**
     * @param string $description
     *
     * @return $this
     */
    public function setDescription($description)
    {
        $this->description = $description;

        return $this;
    }

    /**
     * @param DomainTypeEnum $domainType
     *
     * @return $this
     */
    public function setDomainType($domainType)
    {
        $this->domainType = $domainType;

        return $this;
    }

    /**
     * @param int $transportPort
     *
     * @return $this
     */
    public function setTransportPort($transportPort)
    {
        $this->transportPort = $transportPort;

        return $this;
    }

    /**
     * @param bool $transportUseMx
     *
     * @return DomainSetAttributesTrait
     */
    public function setTransportUseMx($transportUseMx)
    {
        $this->transportUseMx = $transportUseMx;

        return $this;
    }

    /**
     * @param int $domainGroupId
     *
     * @return $this
     */
    public function setDomainGroupId($domainGroupId)
    {
        $this->domainGroupId = $domainGroupId;

        return $this;
    }

    /**
     * @param bool $transportHold
     *
     * @return $this
     */
    public function setTransportHold($transportHold)
    {
        $this->transportHold = $transportHold;

        return $this;
    }


    /**
     * @param bool $isUnconditionalDelete
     *
     * @return DomainSetAttributesTrait
     */
    public function setIsUnconditionalDelete($isUnconditionalDelete)
    {
        $this->isUnconditionalDelete = $isUnconditionalDelete;

        return $this;
    }

    /**
     * @param DomainTransportProtocolEnum $transportProtocol
     *
     * @return DomainSetAttributesTrait
     */
    public function setTransportProtocol($transportProtocol)
    {
        $this->transportProtocol = $transportProtocol;

        return $this;
    }
}