<?php
/**
 * Created by PhpStorm.
 * User: ycoutu
 * Date: 10/10/17
 * Time: 3:56 PM
 */

namespace ProvulusSDK\Client\Request\Resource\Domain\Identity;

trait OutIdentitySetAttributesTrait
{
    /** @var string */
    private $name;

    /** @var int */
    private $forcedDgId;

    /** @var bool */
    private $allowExtras;

    /**
     * @param string $name
     *
     * @return $this
     */
    public function setName($name)
    {
        $this->name = $name;

        return $this;
    }

    /**
     * @param int $forcedDgId
     *
     * @return $this
     */
    public function setForcedDgId($forcedDgId)
    {
        $this->forcedDgId = $forcedDgId;
        $this->nullableChanged();

        return $this;
    }

    /**
     * @param bool $allowExtras
     *
     * @return OutIdentitySetAttributesTrait
     */
    public function setAllowExtras($allowExtras)
    {
        $this->allowExtras = $allowExtras;

        return $this;
    }

    
}
