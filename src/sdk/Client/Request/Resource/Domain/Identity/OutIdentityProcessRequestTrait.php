<?php
/**
 * Created by PhpStorm.
 * User: ycoutu
 * Date: 10/10/17
 * Time: 3:59 PM
 */

namespace ProvulusSDK\Client\Request\Resource\Domain\Identity;

use ProvulusSDK\Client\Response\Domain\Identity\OutIdentityResponse;
use ProvulusSDK\Client\Response\IProvulusResponse;

/**
 * Trait OutIdentityProcessRequestTrait
 *
 * @method OutIdentityResponse getResponse()
 *
 * @package ProvulusSDK\Client\Request\Resource\Domain\Identity
 */
trait OutIdentityProcessRequestTrait
{
    /**
     * Process the data that is in the response
     *
     * @param array $jsonResponse
     *
     * @return IProvulusResponse
     */
    public function processResponse(array $jsonResponse)
    {
        return new OutIdentityResponse(
            $jsonResponse['data'],
            array_key_exists('included', $jsonResponse) ? $jsonResponse['included'] : []
        );
    }
}
