<?php
/**
 * Created by PhpStorm.
 * User: ycoutu
 * Date: 10/10/17
 * Time: 3:45 PM
 */

namespace ProvulusSDK\Client\Request\Resource\Domain\Identity;

use ProvulusSDK\Client\Request\Resource\Organization\OrganizationRequest;

abstract class BaseOutIdentityRequest extends OrganizationRequest
{
    public function baseRoute()
    {
        return parent::baseRoute() . '/identities';
    }
}
