<?php
/**
 * Created by PhpStorm.
 * User: ycoutu
 * Date: 10/10/17
 * Time: 3:46 PM
 */

namespace ProvulusSDK\Client\Request\Resource\Domain\Identity;

abstract class OutIdentityRequest extends BaseOutIdentityRequest
{
    /**
     * Set the identity id for this request
     *
     * @param $id
     *
     * @return $this
     */
    public function setIdentityId($id)
    {
        $this->addBinding('identityId', $id);

        return $this;
    }

    public function baseRoute()
    {
        if (!$this->hasBinding('identityId')) {
            throw new \Exception(sprintf('Identity Id needs to be set for %s', get_class($this)));
        }

        return parent::baseRoute() . '/:identityId';
    }
}
