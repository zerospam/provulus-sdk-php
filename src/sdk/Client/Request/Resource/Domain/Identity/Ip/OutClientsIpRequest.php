<?php
/**
 * Created by PhpStorm.
 * User: ycoutu
 * Date: 10/10/17
 * Time: 4:18 PM
 */

namespace ProvulusSDK\Client\Request\Resource\Domain\Identity\Ip;

abstract class OutClientsIpRequest extends BaseOutClientsIpRequest
{
    /**
     * Set the out client ip id for this request
     *
     * @param $id
     *
     * @return $this
     */
    public function setClientsIpId($id)
    {
        $this->addBinding('clientsIpId', $id);

        return $this;
    }

    public function baseRoute()
    {
        if (!$this->hasBinding('clientsIpId')) {
            throw new \Exception(sprintf('Clients Ip Id needs to be set for %s', get_class($this)));
        }

        return parent::baseRoute() . '/:clientsIpId';
    }
}
