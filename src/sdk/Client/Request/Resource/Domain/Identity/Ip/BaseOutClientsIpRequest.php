<?php
/**
 * Created by PhpStorm.
 * User: ycoutu
 * Date: 10/10/17
 * Time: 4:16 PM
 */

namespace ProvulusSDK\Client\Request\Resource\Domain\Identity\Ip;

use ProvulusSDK\Client\Request\Resource\Organization\OrganizationRequest;

abstract class BaseOutClientsIpRequest extends OrganizationRequest
{
    public function baseRoute()
    {
        return parent::baseRoute() . '/clients-ips';
    }
}
