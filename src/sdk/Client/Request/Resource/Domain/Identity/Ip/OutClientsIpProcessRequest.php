<?php
/**
 * Created by PhpStorm.
 * User: ycoutu
 * Date: 10/10/17
 * Time: 4:20 PM
 */

namespace ProvulusSDK\Client\Request\Resource\Domain\Identity\Ip;

use ProvulusSDK\Client\Response\Domain\Identity\Ip\OutClientsIpResponse;
use ProvulusSDK\Client\Response\IProvulusResponse;

trait OutClientsIpProcessRequest
{
    /**
     * Process the data that is in the response
     *
     * @param array $jsonResponse
     *
     * @return IProvulusResponse
     */
    public function processResponse(array $jsonResponse)
    {
        return new OutClientsIpResponse($jsonResponse['data']);
    }
}
