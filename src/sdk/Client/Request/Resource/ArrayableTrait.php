<?php
/**
 * Created by PhpStorm.
 * User: aaflalo
 * Date: 17-06-16
 * Time: 11:24
 */

namespace ProvulusSDK\Client\Request\Resource;

use ProvulusSDK\Utils\ReflectionUtil;

trait ArrayableTrait
{

    /**
     * @return array
     */
    public function toArray()
    {
        return ReflectionUtil::objToSnakeArray($this, ['nullableChanged', 'reflectionProperties']);
    }
}
