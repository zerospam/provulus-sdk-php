<?php
/**
 * Created by PhpStorm.
 * User: pbb
 * Date: 16/05/17
 * Time: 3:57 PM
 */

namespace ProvulusSDK\Client\Request\Resource\User;

use ProvulusSDK\Enum\Locale\LocaleEnum;

/**
 * Trait UserSetBasicAttributesTrait
 *
 * Basic Attributes needed to create a User
 *
 * @package ProvulusSDK\Client\Request\Resource\User
 */
trait UserSetBasicAttributesTrait
{

    /**
     * @var string
     */
    private $name, $surname, $emailAddress, $phoneNumber;

    /**
     * @var \DateTimeZone
     */
    private $timezoneCode;

    /**
     * @var LocaleEnum
     */
    private $languageCode;

    /**
     * @param string $name
     *
     * @return $this
     */
    public function setName($name)
    {
        $this->name = $name;

        return $this;
    }

    /**
     * @param string $surname
     *
     * @return $this
     */
    public function setSurname($surname)
    {
        $this->surname = $surname;

        return $this;
    }

    /**
     * @param string $emailAddress
     *
     * @return $this
     */
    public function setEmailAddress($emailAddress)
    {
        $this->emailAddress = $emailAddress;

        return $this;
    }

    /**
     * @param string $phoneNumber
     *
     * @return $this
     */
    public function setPhoneNumber($phoneNumber)
    {
        $this->phoneNumber = $phoneNumber;
        $this->nullableChanged();

        return $this;
    }

    /**
     * @param LocaleEnum $languageCode
     *
     * @return $this
     */
    public function setLanguageCode(LocaleEnum $languageCode)
    {
        $this->languageCode = $languageCode;

        return $this;
    }

    /**
     * @param $timezoneCode \DateTimeZone
     *
     * @return $this
     */
    public function setTimezoneCode(\DateTimeZone $timezoneCode)
    {
        $this->timezoneCode = $timezoneCode;

        return $this;
    }
}