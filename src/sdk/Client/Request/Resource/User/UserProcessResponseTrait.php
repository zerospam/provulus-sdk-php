<?php
/**
 * Created by PhpStorm.
 * User: pbb
 * Date: 30/01/17
 * Time: 9:43 AM
 */

namespace ProvulusSDK\Client\Request\Resource\User;

use ProvulusSDK\Client\Response\User\UserResponse;

/**
 * Class UserProcessResponseTrait
 *
 * @package ProvulusSDK\Client\Request\Resource\User
 */
trait UserProcessResponseTrait
{

    /**
     * Process the data that is in the response
     * With included relationship objects if needed
     *
     * @param array $jsonResponse
     *
     * @return UserResponse
     *
     */
    public function processResponse(array $jsonResponse)
    {
        $included = isset($jsonResponse['included'])
            ? $jsonResponse['included']
            : [];

        return new UserResponse($jsonResponse['data'], $included);
    }
}