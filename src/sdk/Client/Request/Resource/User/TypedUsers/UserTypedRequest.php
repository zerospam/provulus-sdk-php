<?php
/**
 * Created by PhpStorm.
 * User: aaflalo
 * Date: 16/12/16
 * Time: 2:03 PM
 */

namespace ProvulusSDK\Client\Request\Resource\User\TypedUsers;

/**
 * Class UserRequest
 *
 * Organization having an User has parent or request
 *
 * @package ProvulusSDK\Client\Request\Resource\User
 */
abstract class UserTypedRequest extends BaseUserTypedRequest
{

    /**
     * Set the user ID for this request
     *
     * @param $id
     *
     * @return $this
     */
    public function setUserId($id)
    {
        $this->addBinding('userId', $id);

        return $this;
    }

    /**
     * @return string
     */
    function baseRoute()
    {
        if (!$this->hasBinding('userId')) {
            throw new \InvalidArgumentException('Need to have user id set.');
        }

        return parent::baseRoute() . '/:userId';
    }
}