<?php
/**
 * Created by PhpStorm.
 * User: pbb
 * Date: 05/01/17
 * Time: 8:55 AM
 */

namespace ProvulusSDK\Client\Request\Resource\User\TypedUsers;

use ProvulusSDK\Client\Request\Resource\Organization\OrganizationRequest;

/**
 * Class UserBaseRequest
 *
 * Base class for create and update user request
 *
 * @package ProvulusSDK\Client\Request\User
 */
abstract class BaseUserTypedRequest extends OrganizationRequest
{

    /**
     * What type of user is queried
     *
     * @param UserRequestType $userRequestType
     *
     * @return $this
     */
    public function setUserRequestType(UserRequestType $userRequestType)
    {
        $this->addBinding('userType', $userRequestType->getValue());

        return $this;
    }

    /**
     *
     * @return string
     */
    function baseRoute()
    {
        if (!$this->hasBinding('userType')) {
            throw new \InvalidArgumentException('Need to have the type of request set');
        }

        return parent::baseRoute() . '/:userType';
    }
}