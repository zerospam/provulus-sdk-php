<?php
/**
 * Created by PhpStorm.
 * User: aaflalo
 * Date: 16/12/16
 * Time: 2:08 PM
 */

namespace ProvulusSDK\Client\Request\Resource\User\TypedUsers;


use MabeEnum\Enum;
use ProvulusSDK\Client\Request\Resource\PrimalValued;
use ProvulusSDK\Client\Request\Resource\PrimalValuedEnumTrait;

/**
 * Class UserType
 *
 * @method static UserRequestType REGULAR()
 * @method static UserRequestType ADMIN()
 * @method static UserRequestType ALL()
 * @package ProvulusSDK\Client\Request\Resource\User
 */
class UserRequestType extends Enum implements PrimalValued
{
    use PrimalValuedEnumTrait;

    const REGULAR = 'regulars';
    const ADMIN   = 'admins';
    const ALL     = 'users';
}