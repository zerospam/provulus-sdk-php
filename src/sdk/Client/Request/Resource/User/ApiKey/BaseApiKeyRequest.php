<?php
/**
 * Created by PhpStorm.
 * User: pbb
 * Date: 23/12/16
 * Time: 4:15 PM
 */

namespace ProvulusSDK\Client\Request\Resource\User\ApiKey;

use ProvulusSDK\Client\Request\Resource\Organization\OrganizationRequest;

/**
 * Class ApiKeyRequest
 *
 * Base request class for building ApiKey related requests
 *
 * @package ProvulusSDK\Client\Request\Resource\User\ApiKey
 */
abstract class BaseApiKeyRequest extends OrganizationRequest
{

    /**
     * Route for apikeys
     *
     * @return string
     */
    function baseRoute()
    {
        return parent::baseRoute() . '/users/:userId/apikeys';
    }

    /**
     * Sets apikeys' owner id
     *
     * @param $id
     *
     * @return $this
     */
    function setUserId($id)
    {
        $this->addBinding('userId', $id);

        return $this;
    }
}