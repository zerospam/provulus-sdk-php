<?php
/**
 * Created by PhpStorm.
 * User: pbb
 * Date: 30/01/17
 * Time: 4:13 PM
 */

namespace ProvulusSDK\Client\Request\Resource\User\ApiKey;

/**
 * Class ApiKeySetAttributesTrait
 *
 * Trait useful for request that need to set attributes
 *
 * @package ProvulusSDK\Client\Request\Resource\User\ApiKey
 */
trait ApiKeySetAttributesTrait
{

    /**
     * @var array
     */
    private $permissions;

    /**
     * @var string
     */
    private $comment, $cidr;

    /**
     * @param array|string[] $permissions
     *
     * @return $this
     */
    public function setPermissions($permissions)
    {
        $this->permissions = $permissions;

        return $this;
    }

    /**
     * @param string $comment
     *
     * @return $this
     */
    public function setComment($comment)
    {
        $this->comment = $comment;

        return $this;
    }

    /**
     * @param string $cidr
     *
     * @return $this
     */
    public function setCidr($cidr)
    {
        $this->cidr = $cidr;

        return $this;
    }
}