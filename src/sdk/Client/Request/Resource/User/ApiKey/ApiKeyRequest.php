<?php
/**
 * Created by PhpStorm.
 * User: pbb
 * Date: 30/01/17
 * Time: 2:39 PM
 */

namespace ProvulusSDK\Client\Request\Resource\User\ApiKey;


/**
 * Class ApiKeyRequest
 *
 * @package ProvulusSDK\Client\Request\Resource\User\ApiKey
 */
abstract class ApiKeyRequest extends BaseApiKeyRequest
{

    /**
     * Base route for api key
     *
     * @return string
     * @throws \Exception
     */
    function baseRoute()
    {
        if (!$this->hasBinding('apikeyId')) {
            throw new \Exception('ApiKeyId needs to be set.');
        }

        return parent::baseRoute() . '/:apikeyId';
    }

    /**
     * Sets ApiKeyId
     *
     * @param int $id
     *
     * @return $this
     */
    public function setApiKeyId($id)
    {
        $this->addBinding('apikeyId', $id);

        return $this;
    }
}