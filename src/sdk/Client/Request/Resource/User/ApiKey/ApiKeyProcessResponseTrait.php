<?php
/**
 * Created by PhpStorm.
 * User: pbb
 * Date: 30/01/17
 * Time: 2:28 PM
 */

namespace ProvulusSDK\Client\Request\Resource\User\ApiKey;

use ProvulusSDK\Client\Response\IProvulusObjectResponse;
use ProvulusSDK\Client\Response\User\ApiKey\ApiKeyResponse;

/**
 * Class ApiKeyProcessResponseTrait
 *
 * @package ProvulusSDK\Client\Request\Resource\User\ApiKey
 */
trait ApiKeyProcessResponseTrait
{

    /**
     * Process the data that is in the response
     *
     * @param array $jsonResponse
     *
     * @return IProvulusObjectResponse
     *
     */
    public function processResponse(array $jsonResponse)
    {
        return new ApiKeyResponse($jsonResponse['data']);
    }
}