<?php
/**
 * Created by PhpStorm.
 * User: pbb
 * Date: 26/01/17
 * Time: 2:12 PM
 */

namespace ProvulusSDK\Client\Request\Resource\User;

/**
 * Class UserSetAttributesTrait
 *
 * Trait useful for create and update request
 *
 * @package ProvulusSDK\Client\Request\User
 */
trait UserSetAllAttributesTrait
{
    use UserSetBasicAttributesTrait;

    /**
     * @var string
     */
    private $ssoId;

    /**
     * @var bool
     */
    private $isAccessAllowed, $isAdminStatsPush, $isAdminquarOnly;

    /**
     * @var int
     */
    private $quarSearchPageSize;

    /**
     * @param string $ssoId
     *
     * @return $this
     */
    public function setSsoId($ssoId)
    {
        $this->ssoId = $ssoId;

        return $this;
    }

    /**
     * @param bool $isAccessAllowed
     *
     * @return $this
     */
    public function setIsAccessAllowed($isAccessAllowed)
    {
        $this->isAccessAllowed = $isAccessAllowed;

        return $this;
    }

    /**
     * @param bool $isAdminStatsPush
     *
     * @return $this
     */
    public function setIsAdminStatsPush($isAdminStatsPush)
    {
        $this->isAdminStatsPush = $isAdminStatsPush;

        return $this;
    }

    /**
     * @param bool $isAdminquarOnly
     *
     * @return $this
     */
    public function setIsAdminquarOnly($isAdminquarOnly)
    {
        $this->isAdminquarOnly = $isAdminquarOnly;

        return $this;
    }

    /**
     * @param int $quarSearchPageSize
     *
     * @return $this
     */
    public function setQuarSearchPageSize($quarSearchPageSize)
    {
        $this->quarSearchPageSize = $quarSearchPageSize;

        return $this;
    }
}