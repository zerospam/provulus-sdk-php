<?php
/**
 * Created by PhpStorm.
 * User: ycoutu
 * Date: 28/12/17
 * Time: 11:19 AM
 */

namespace ProvulusSDK\Client\Request\Resource\User\TwoFactorAuth;

use ProvulusSDK\Client\Request\Resource\User\AllUsers\UserAllRequest;

abstract class TwoFactorAuthBaseRequest extends UserAllRequest
{
    /**
     * @return string
     */
    function baseRoute()
    {
        return parent::baseRoute() . '/tfa';
    }
}
