<?php
/**
 * Created by PhpStorm.
 * User: ycoutu
 * Date: 28/12/17
 * Time: 11:21 AM
 */

namespace ProvulusSDK\Client\Request\Resource\User\TwoFactorAuth;

use ProvulusSDK\Client\Response\User\TwoFactorAuth\TwoFactorAuthResponse;

/**
 * Trait TwoFactorAuthResponseTrait
 *
 * @method TwoFactorAuthResponse getResponse()
 *
 * @package ProvulusSDK\Client\Request\Resource\User\TwoFactorAuth
 */
trait TwoFactorAuthResponseTrait
{
    /**
     * @param array $jsonResponse
     *
     * @return TwoFactorAuthResponse
     */
    public function processResponse(array $jsonResponse)
    {
        return new TwoFactorAuthResponse($jsonResponse['data']);
    }
}
