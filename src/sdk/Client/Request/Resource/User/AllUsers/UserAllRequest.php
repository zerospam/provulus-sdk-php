<?php
/**
 * Created by PhpStorm.
 * User: pbb
 * Date: 19/04/17
 * Time: 2:59 PM
 */

namespace ProvulusSDK\Client\Request\Resource\User\AllUsers;

/**
 * Class BaseUsersRequest
 *
 * Base class for users requests when no user typing is needed
 *
 * @package ProvulusSDK\Client\Request\Resource\User
 */
abstract class UserAllRequest extends UsersAllBaseRequest
{

    /**
     * Set the user ID for this request
     *
     * @param $id
     *
     * @return $this
     */
    public function setUserId($id)
    {
        $this->addBinding('userId', $id);

        return $this;
    }

    /**
     *
     * @return string
     */
    function baseRoute()
    {
        if (!$this->hasBinding('userId')) {
            throw new \InvalidArgumentException('Need to have user id set.');
        }

        return parent::baseRoute() . '/:userId';
    }


}