<?php
/**
 * Created by PhpStorm.
 * User: pbb
 * Date: 30/08/17
 * Time: 4:55 PM
 */

namespace ProvulusSDK\Client\Request\Resource\User\AllUsers;

use ProvulusSDK\Client\Request\Resource\Organization\OrganizationRequest;

/**
 * Class UsersAllBaseRequest
 *
 * Base class for request against collection of all users
 *
 * @package ProvulusSDK\Client\Request\Resource\User\AllUsers
 */
abstract class UsersAllBaseRequest extends OrganizationRequest
{
    /**
     *
     * @return string
     */
    function baseRoute()
    {
        return parent::baseRoute() . '/users';
    }
}