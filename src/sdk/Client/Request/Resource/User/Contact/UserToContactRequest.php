<?php
/**
 * Created by PhpStorm.
 * User: ycoutu
 * Date: 08/06/17
 * Time: 9:05 AM
 */

namespace ProvulusSDK\Client\Request\Resource\User\Contact;


use ProvulusSDK\Client\Request\RequestType;
use ProvulusSDK\Client\Request\Resource\Organization\Contact\ContactPersonProcessResponseTrait;
use ProvulusSDK\Client\Request\Resource\Organization\Contact\ContactPersonSetAttributes;
use ProvulusSDK\Client\Request\Resource\User\AllUsers\UserAllRequest;
use ProvulusSDK\Client\Response\Organization\Contact\ContactPersonResponse;

/**
 * Class UserToContactRequest
 *
 * @method static ContactPersonResponse getResponse()
 *
 * @package ProvulusSDK\Client\Request\Resource\User\Contact
 */
class UserToContactRequest extends UserAllRequest
{
    use ContactPersonProcessResponseTrait, ContactPersonSetAttributes;

    /**
     * Type of request
     *
     * @return RequestType
     */
    public function httpType()
    {
        return RequestType::HTTP_POST();
    }

    function baseRoute()
    {
        return parent::baseRoute() . '/toContact';
    }
}
