<?php
/**
 * Created by PhpStorm.
 * User: pbb
 * Date: 01/03/17
 * Time: 8:42 AM
 */

namespace ProvulusSDK\Client\Request\Resource\User\UserEmailAddress;

/**
 * Class UserEmailAddressRequest
 *
 * @package ProvulusSDK\Client\Request\Resource\User\UserEmailAddress
 */
abstract class UserEmailAddressRequest extends BaseUserEmailAddressRequest
{
    /**
     * @return string
     */
    function baseRoute()
    {
        return parent::baseRoute() . '/:emailId';
    }

    /**
     * Set the user email address id for this request
     *
     * @param $id
     *
     * @return $this
     */
    public function setEmailId($id)
    {
        $this->addBinding('emailId', $id);

        return $this;
    }
}