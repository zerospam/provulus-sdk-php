<?php
/**
 * Created by PhpStorm.
 * User: pbb
 * Date: 28/02/17
 * Time: 4:16 PM
 */

namespace ProvulusSDK\Client\Request\Resource\User\UserEmailAddress;


use ProvulusSDK\Client\Request\Resource\User\AllUsers\UserAllRequest;

/**
 * Class BaseUserEmailAddressRequest
 *
 * Base class for requesting UserEmailAddress resources
 *
 * @package ProvulusSDK\Client\Request\Resource\User\UserEmailAddress
 */
abstract class BaseUserEmailAddressRequest extends UserAllRequest
{

    /**
     * Base route for request
     *
     * @return string
     */
    function baseRoute()
    {
        return parent::baseRoute() . '/emails';
    }
}