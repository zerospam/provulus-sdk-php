<?php
/**
 * Created by PhpStorm.
 * User: pbb
 * Date: 28/02/17
 * Time: 4:20 PM
 */

namespace ProvulusSDK\Client\Request\Resource\User\UserEmailAddress;


use ProvulusSDK\Client\Response\User\UserEmailAddress\UserEmailAddressResponse;

trait UserEmailAddressProcessResponseTrait
{

    /**
     * Process the data that is in the response
     *
     * @param array $jsonResponse
     *
     * @return UserEmailAddressResponse
     *
     */
    public function processResponse(array $jsonResponse)
    {
        return new UserEmailAddressResponse($jsonResponse['data']);
    }
}