<?php
/**
 * Created by PhpStorm.
 * User: aaflalo
 * Date: 17-06-19
 * Time: 10:36
 */

namespace ProvulusSDK\Client\Request\Resource\Value;

interface HasValue
{
    /**
     * Get value
     *
     * <b>Internal use</b>
     *
     * @param $name
     *
     * @return null|mixed
     */
    public function getValue($name);
}
