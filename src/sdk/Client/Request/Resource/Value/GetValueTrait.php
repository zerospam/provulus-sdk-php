<?php
/**
 * Created by PhpStorm.
 * User: aaflalo
 * Date: 17-06-19
 * Time: 10:34
 */

namespace ProvulusSDK\Client\Request\Resource\Value;

use ProvulusSDK\Utils\ReflectionUtil;
use ProvulusSDK\Utils\Str;

trait GetValueTrait
{

    /**
     * @var \ReflectionProperty[]
     */
    private $reflectionProperties;

    /**
     * Get the reflection object for the method getValue
     *
     * @return \ReflectionProperty[]
     */
    private function reflectionProperties()
    {
        if ($this->reflectionProperties) {
            return $this->reflectionProperties;
        }

        return $this->reflectionProperties = array_reduce
        (
            ReflectionUtil::getAllProperties($this),
            function (array $result, \ReflectionProperty $property) {
                $result[$property->name] = $property;
                return $result;
            },
            []
        );
    }

    /**
     * Get value
     *
     * <b>Internal use</b>
     *
     * @param $name
     *
     * @return null|mixed
     */
    public function getValue($name)
    {
        $name            = Str::camel($name);
        $reflectionProps = $this->reflectionProperties();

        if (!isset($reflectionProps[$name])) {
            return null;
        }

        $prop = $reflectionProps[$name];

        $prop->setAccessible(true);
        $val = $prop->getValue($this);
        $prop->setAccessible(false);

        return $val;
    }
}
