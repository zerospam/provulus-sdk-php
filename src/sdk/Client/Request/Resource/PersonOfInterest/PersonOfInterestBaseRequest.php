<?php
/**
 * Created by PhpStorm.
 * User: pbb
 * Date: 17/10/17
 * Time: 3:51 PM
 */

namespace ProvulusSDK\Client\Request\Resource\PersonOfInterest;

use ProvulusSDK\Client\Request\Resource\Organization\OrganizationRequest;

/**
 * Class PersonOfInterestBaseRequest
 *
 * Base request for person of interest pointing at resource collection
 *
 * @package ProvulusSDK\Client\Request\Resource\PersonOfInterest
 */
abstract class PersonOfInterestBaseRequest extends OrganizationRequest
{
    const RESOURCE_NAME = 'personinterests';

    /**
     * @return string
     */
    function baseRoute()
    {
        return parent::baseRoute() . '/' . self::RESOURCE_NAME;
    }

}