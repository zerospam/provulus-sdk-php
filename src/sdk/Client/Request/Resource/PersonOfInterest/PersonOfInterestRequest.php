<?php
/**
 * Created by PhpStorm.
 * User: pbb
 * Date: 17/10/17
 * Time: 3:58 PM
 */

namespace ProvulusSDK\Client\Request\Resource\PersonOfInterest;

/**
 * Class PersonOfInterestRequest
 *
 * Base class for requests pointing to a specific person of interest
 *
 * @package ProvulusSDK\Client\Request\Resource\PersonOfInterest
 */
abstract class PersonOfInterestRequest extends PersonOfInterestBaseRequest
{
    const BINDING = 'personOfInterestId';

    /**
     * Set person of interest Id
     *
     * @param $id
     *
     * @return $this
     */
    public function setPersonOfInterestId($id)
    {
        $this->addBinding(self::BINDING, $id);

        return $this;
    }

    /**
     * @return string
     * @throws \Exception
     */
    function baseRoute()
    {
        if (!$this->hasBinding(self::BINDING)) {
            throw new \Exception(sprintf('Binding [%s] needs to be set for [%s].', self::BINDING, get_called_class()));
        }

        return parent::baseRoute() . '/:' . self::BINDING;
    }
}