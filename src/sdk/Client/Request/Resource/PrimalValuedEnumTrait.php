<?php
/**
 * Created by PhpStorm.
 * User: aaflalo
 * Date: 07/02/17
 * Time: 1:24 PM
 */

namespace ProvulusSDK\Client\Request\Resource;


trait PrimalValuedEnumTrait
{

    function toPrimitive()
    {
       return $this->getValue();
    }
}