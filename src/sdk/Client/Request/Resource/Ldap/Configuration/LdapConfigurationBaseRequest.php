<?php
/**
 * Created by PhpStorm.
 * User: pbb
 * Date: 14/06/17
 * Time: 1:03 PM
 */

namespace ProvulusSDK\Client\Request\Resource\Ldap\Configuration;

use ProvulusSDK\Client\Request\Resource\Organization\OrganizationRequest;

/**
 * Class LdapConfigBaseRequest
 *
 * Base request for ldap config request
 *
 * @package ProvulusSDK\Client\Request\Resource\LDAP
 */
abstract class LdapConfigurationBaseRequest extends OrganizationRequest
{
    /**
     * @return string
     * @throws \Exception
     */
    function baseRoute()
    {
        return parent::baseRoute() . '/ldap';
    }
}