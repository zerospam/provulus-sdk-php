<?php
/**
 * Created by PhpStorm.
 * User: pbb
 * Date: 08/06/17
 * Time: 4:25 PM
 */

namespace ProvulusSDK\Client\Request\Resource\Ldap\Configuration;

use ProvulusSDK\Client\Response\Ldap\Configuration\LdapConfigurationResponse;

/**
 * Trait LdapConfigResponseTrait
 *
 * Trait used when responding a LdapConfigResponding is needed
 *
 * @method LdapConfigurationResponse getResponse()
 *
 * @package ProvulusSDK\Client\Request\Resource\LDAP
 */
trait LdapConfigurationResponseTrait
{

    /**
     * Process the data that is in the response
     *
     * @param array $jsonResponse
     *
     * @return LdapConfigurationResponse
     */
    public function processResponse(array $jsonResponse)
    {
        $included = isset($jsonResponse['included'])
            ? $jsonResponse['included']
            : [];

        return new LdapConfigurationResponse($jsonResponse['data'], $included);
    }
}