<?php
/**
 * Created by PhpStorm.
 * User: pbb
 * Date: 09/06/17
 * Time: 9:01 AM
 */

namespace ProvulusSDK\Client\Request\Resource\Ldap\Configuration;

use ProvulusSDK\Utils\Ldap\Synchronization;
use ProvulusSDK\Utils\Ldap\UserCreation;

/**
 * Class LdapConfigurationRequestOptionalAttributesTrait
 *
 *  Attributes and setters for optional ldap config values
 *
 * @package ProvulusSDK\Client\Request\Resource\LDAP
 */
trait LdapConfigurationRequestOptionalAttributesTrait
{
    /** @var string|null */
    private $reportEmail;

    /**
     * @param UserCreation $creation
     *
     * @return $this
     */
    public function setUserCreation(UserCreation $creation)
    {
        $this->userCreation = $creation;

        return $this;
    }

    /**
     * @param Synchronization $synchronization
     *
     * @return $this
     */
    public function setSynchronization(Synchronization $synchronization)
    {
        $this->synchronization = $synchronization;

        return $this;
    }

    /**
     * @param null|string $reportEmail
     *
     * @return $this
     */
    public function setReportEmail($reportEmail)
    {
        $this->nullableChanged();
        $this->reportEmail = $reportEmail;

        return $this;
    }
}