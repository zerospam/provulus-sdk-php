<?php
/**
 * Created by PhpStorm.
 * User: ycoutu
 * Date: 29/08/18
 * Time: 2:31 PM
 */

namespace ProvulusSDK\Client\Request\Resource\Ldap\Configuration\Connection;

abstract class LdapConnectionRequest extends LdapConnectionBaseRequest
{
    /**
     * Set the connection id for this request
     *
     * @param $id
     *
     * @return $this
     */
    public function setLdapConnectionId($id)
    {
        $this->addBinding('ldapConnectionId', $id);

        return $this;
    }

    /**
     * Base route for orgs resource
     *
     * @return string
     * @throws \Exception
     */
    function baseRoute()
    {
        if (!$this->hasBinding('ldapConnectionId')) {
            throw new \Exception(sprintf('Ldap connection ID needs to be set for %s', get_class($this)));
        }

        return parent::baseRoute() . '/:ldapConnectionId';
    }
}
