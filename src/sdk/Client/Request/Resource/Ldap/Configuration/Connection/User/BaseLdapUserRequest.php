<?php
/**
 * Created by PhpStorm.
 * User: ycoutu
 * Date: 14/06/17
 * Time: 11:18 AM
 */

namespace ProvulusSDK\Client\Request\Resource\Ldap\Configuration\Connection\User;

use ProvulusSDK\Client\Request\Resource\Ldap\Configuration\Connection\LdapConnectionRequest;

/**
 * Base class for LdapUser requests
 *
 * Class BaseLdapUserRequest
 *
 * @package ProvulusSDK\Client\Request\LDAP
 */
abstract class BaseLdapUserRequest extends LdapConnectionRequest
{
    /**
     * Base route for LdapUser
     *
     * @return string
     * @throws \Exception
     */
    function baseRoute()
    {
        return parent::baseRoute() . '/users';
    }
}
