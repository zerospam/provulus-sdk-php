<?php
/**
 * Created by PhpStorm.
 * User: ycoutu
 * Date: 29/08/18
 * Time: 2:30 PM
 */

namespace ProvulusSDK\Client\Request\Resource\Ldap\Configuration\Connection;

use ProvulusSDK\Client\Request\Resource\Ldap\Configuration\LdapConfigurationBaseRequest;

abstract class LdapConnectionBaseRequest extends LdapConfigurationBaseRequest
{
    /**
     * @return string
     * @throws \Exception
     */
    function baseRoute()
    {
        return parent::baseRoute() . '/connections';
    }
}
