<?php
/**
 * Created by PhpStorm.
 * User: ycoutu
 * Date: 29/08/18
 * Time: 2:56 PM
 */

namespace ProvulusSDK\Client\Request\Resource\Ldap\Configuration\Connection;

trait LdapConnectionAttributesTrait
{
    /** @var string */
    private $hostname;

    /** @var int */
    private $port;

    /** @var bool */
    private $isSslEnabled;

    /** @var string */
    private $userDn;

    /** @var string */
    private $password;

    /** @var string */
    private $searchDn;

    /** @var string */
    private $emailBinding;

    /** @var string|null */
    private $aliasBinding;

    /** @var string|null */
    private $firstNameBinding;

    /** @var string|null */
    private $lastNameBinding;

    /** @var string|null */
    private $languageBinding;

    /** @var string|null */
    private $ssoBinding;

    /** @var string|null */
    private $filter;

    /** @var int */
    private $pagination;

    /**
     * @param string $hostname
     * @return LdapConnectionAttributesTrait
     */
    public function setHostname($hostname)
    {
        $this->hostname = $hostname;
        return $this;
    }

    /**
     * @param int $port
     * @return LdapConnectionAttributesTrait
     */
    public function setPort($port)
    {
        $this->port = $port;
        return $this;
    }

    /**
     * @param bool $isSslEnabled
     * @return LdapConnectionAttributesTrait
     */
    public function setIsSslEnabled($isSslEnabled)
    {
        $this->isSslEnabled = $isSslEnabled;
        return $this;
    }

    /**
     * @param string $userDn
     * @return LdapConnectionAttributesTrait
     */
    public function setUserDn($userDn)
    {
        $this->userDn = $userDn;
        return $this;
    }

    /**
     * @param string $password
     * @return LdapConnectionAttributesTrait
     */
    public function setPassword($password)
    {
        $this->password = $password;
        return $this;
    }

    /**
     * @param string $searchDn
     * @return LdapConnectionAttributesTrait
     */
    public function setSearchDn($searchDn)
    {
        $this->searchDn = $searchDn;
        return $this;
    }

    /**
     * @param string $emailBinding
     * @return LdapConnectionAttributesTrait
     */
    public function setEmailBinding($emailBinding)
    {
        $this->emailBinding = $emailBinding;
        return $this;
    }

    /**
     * @param null|string $aliasBinding
     * @return LdapConnectionAttributesTrait
     */
    public function setAliasBinding($aliasBinding)
    {
        $this->aliasBinding = $aliasBinding;
        $this->nullableChanged();

        return $this;
    }

    /**
     * @param null|string $firstNameBinding
     * @return LdapConnectionAttributesTrait
     */
    public function setFirstNameBinding($firstNameBinding)
    {
        $this->firstNameBinding = $firstNameBinding;
        $this->nullableChanged();

        return $this;
    }

    /**
     * @param null|string $lastNameBinding
     * @return LdapConnectionAttributesTrait
     */
    public function setLastNameBinding($lastNameBinding)
    {
        $this->lastNameBinding = $lastNameBinding;
        $this->nullableChanged();

        return $this;
    }

    /**
     * @param null|string $languageBinding
     * @return LdapConnectionAttributesTrait
     */
    public function setLanguageBinding($languageBinding)
    {
        $this->languageBinding = $languageBinding;
        $this->nullableChanged();

        return $this;
    }

    /**
     * @param null|string $ssoBinding
     * @return LdapConnectionAttributesTrait
     */
    public function setSsoBinding($ssoBinding)
    {
        $this->ssoBinding = $ssoBinding;
        $this->nullableChanged();

        return $this;
    }

    /**
     * @param null|string $filter
     * @return LdapConnectionAttributesTrait
     */
    public function setFilter($filter)
    {
        $this->filter = $filter;
        $this->nullableChanged();

        return $this;
    }

    /**
     * @param int $pagination
     * @return LdapConnectionAttributesTrait
     */
    public function setPagination($pagination)
    {
        $this->pagination = $pagination;
        return $this;
    }
}
