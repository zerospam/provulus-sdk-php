<?php
/**
 * Created by PhpStorm.
 * User: ycoutu
 * Date: 29/08/18
 * Time: 2:34 PM
 */

namespace ProvulusSDK\Client\Request\Resource\Ldap\Configuration\Connection;

use ProvulusSDK\Client\Response\Ldap\Configuration\Connection\LdapConnectionResponse;

/**
 * Trait LdapConnectionResponseTrait
 *
 * @method LdapConnectionResponse getResponse()
 *
 * @package ProvulusSDK\Client\Request\Resource\Ldap\Configuration\Connection
 */
trait LdapConnectionResponseTrait
{
    /**
     * Process the data that is in the response
     *
     * @param array $jsonResponse
     *
     * @return
     */
    public function processResponse(array $jsonResponse)
    {
        $included = isset($jsonResponse['included'])
            ? $jsonResponse['included']
            : [];

        return new LdapConnectionResponse($jsonResponse['data'], $included);
    }
}
