<?php
/**
 * Created by PhpStorm.
 * User: pbb
 * Date: 22/06/17
 * Time: 2:44 PM
 */

namespace ProvulusSDK\Client\Request\Resource\Ldap\Synchronization;


use ProvulusSDK\Client\Request\Resource\Ldap\Configuration\LdapConfigurationBaseRequest;

/**
 * Class LdapSynchronizationBaseRequest
 *
 * Base request for ldap synchronizations requests
 *
 * @package ProvulusSDK\Client\Request\Resource\LDAP
 */
abstract class LdapSynchronizationBaseRequest extends LdapConfigurationBaseRequest
{
    /**
     * Base route for ldap synchronizations requests
     *
     * @return string
     * @throws \Exception
     */
    function baseRoute()
    {
        return parent::baseRoute() . '/synchronization';
    }
}
