<?php
/**
 * Created by PhpStorm.
 * User: ycoutu
 * Date: 10/11/17
 * Time: 10:40 AM
 */

namespace ProvulusSDK\Client\Request\Quarantine;

use ProvulusSDK\Client\Request\RequestType;
use ProvulusSDK\Client\Request\Resource\Quarantine\QuarantineBaseRequest;
use ProvulusSDK\Client\Response\IProvulusResponse;
use ProvulusSDK\Client\Response\Quarantine\Message\QuarantineMessageResponse;

/**
 * Class ReadQuarantineRequest
 *
 * @method QuarantineMessageResponse getResponse()
 *
 * @package ProvulusSDK\Client\Request\Quarantine
 */
class ReadQuarantineRequest extends QuarantineBaseRequest
{

    /**
     * Set the organization id for this request
     *
     * @param $id
     *
     * @return $this
     */
    public function setMessageRecipientId($id)
    {
        $this->addBinding('msgId', $id);

        return $this;
    }

    /**
     * Type of request
     *
     * @return RequestType
     */
    public function httpType()
    {
        return RequestType::HTTP_GET();
    }

    function baseRoute()
    {
        if (!$this->hasBinding('msgId')) {
            throw new \Exception(sprintf('Message Id needs to be set for %s', get_class($this)));
        }

        return parent::baseRoute() . '/:msgId';
    }

    /**
     * Process the data that is in the response
     *
     * @param array $jsonResponse
     *
     * @return IProvulusResponse
     */
    public function processResponse(array $jsonResponse)
    {
        return new QuarantineMessageResponse($jsonResponse['data']);
    }
}
