<?php
/**
 * Created by PhpStorm.
 * User: ycoutu
 * Date: 17/11/17
 * Time: 10:06 AM
 */

namespace ProvulusSDK\Client\Request\Quarantine;

use ProvulusSDK\Client\Request\BulkRequestTrait;
use ProvulusSDK\Client\Request\Collection\CollectionOrderTrait;
use ProvulusSDK\Client\Request\Collection\IProvulusCollectionRequest;
use ProvulusSDK\Client\Request\RequestType;
use ProvulusSDK\Client\Request\Resource\Quarantine\QuarantineBaseRequest;
use ProvulusSDK\Client\Response\IProvulusObjectResponse;
use ProvulusSDK\Client\Response\Quarantine\Message\Blob\MessageBlobCollectionResponse;
use ProvulusSDK\Client\Response\Quarantine\Message\Blob\MessageBlobResponse;

class BulkFullMessageQuarantineRequest extends QuarantineBaseRequest implements IProvulusCollectionRequest
{
    use BulkRequestTrait, CollectionOrderTrait;

    /**
     * Type of request
     *
     * @return RequestType
     */
    public function httpType()
    {
        return RequestType::HTTP_GET();
    }

    function baseRoute()
    {
        return parent::baseRoute() . '/content';
    }

    /**
     * Used to parse each result of the collection
     *
     * Need to be overridden by the class
     *
     * @param array $objectData
     *
     * @return IProvulusObjectResponse
     */
    function toResponse(array $objectData)
    {
        return new MessageBlobResponse($objectData);
    }

    /**
     * Class to be used to build the collection
     *
     * @return string
     */
    function collectionClass()
    {
        return MessageBlobCollectionResponse::class;
    }
}
