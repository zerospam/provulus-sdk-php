<?php
/**
 * Created by PhpStorm.
 * User: ycoutu
 * Date: 10/11/17
 * Time: 11:19 AM
 */

namespace ProvulusSDK\Client\Request\Quarantine;

use ProvulusSDK\Client\Request\BulkRequestTrait;
use ProvulusSDK\Client\Request\EmptyResponseTrait;
use ProvulusSDK\Client\Request\RequestType;
use ProvulusSDK\Client\Request\Resource\Quarantine\QuarantineBaseRequest;
use ProvulusSDK\Enum\Quarantine\Action\QuarantineActionEnum;

class QuarantineBulkActionRequest extends QuarantineBaseRequest
{
    use EmptyResponseTrait, BulkRequestTrait;

    /** @var QuarantineActionEnum */
    private $quarantineAction;

    /**
     * Type of request
     *
     * @return RequestType
     */
    public function httpType()
    {
        return RequestType::HTTP_POST();
    }

    public function setQuarantineAction(QuarantineActionEnum $actionEnum)
    {
        $this->quarantineAction = $actionEnum;

        return $this;
    }

    function baseRoute()
    {
        return parent::baseRoute() . '/' . $this->quarantineAction->buildType()->getRoute();
    }
}
