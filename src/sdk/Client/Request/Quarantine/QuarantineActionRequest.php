<?php
/**
 * Created by PhpStorm.
 * User: ycoutu
 * Date: 10/11/17
 * Time: 11:06 AM
 */

namespace ProvulusSDK\Client\Request\Quarantine;

use ProvulusSDK\Client\Request\EmptyResponseTrait;
use ProvulusSDK\Client\Request\RequestType;
use ProvulusSDK\Client\Request\Resource\Quarantine\QuarantineBaseRequest;
use ProvulusSDK\Enum\Quarantine\Action\QuarantineActionEnum;

class QuarantineActionRequest extends QuarantineBaseRequest
{
    use EmptyResponseTrait;

    /** @var QuarantineActionEnum */
    private $quarantineAction;

    public function setQuarantineAction(QuarantineActionEnum $actionEnum)
    {
        $this->quarantineAction = $actionEnum;

        return $this;
    }

    /**
     * Set the organization id for this request
     *
     * @param $id
     *
     * @return $this
     */
    public function setMessageRecipientId($id)
    {
        $this->addBinding('msgId', $id);

        return $this;
    }

    /**
     * Type of request
     *
     * @return RequestType
     */
    public function httpType()
    {
        return RequestType::HTTP_POST();
    }

    function baseRoute()
    {
        if (!$this->hasBinding('msgId')) {
            throw new \Exception(sprintf('Message Id needs to be set for %s', get_class($this)));
        }
        return parent::baseRoute() . '/:msgId/' . $this->quarantineAction->buildType()->getRoute();
    }
}
