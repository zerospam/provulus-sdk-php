<?php
/**
 * Created by PhpStorm.
 * User: ycoutu
 * Date: 17/11/17
 * Time: 9:21 AM
 */

namespace ProvulusSDK\Client\Request\Quarantine;

use ProvulusSDK\Client\Request\RequestType;
use ProvulusSDK\Client\Request\Resource\Quarantine\QuarantineBaseRequest;
use ProvulusSDK\Client\Response\IProvulusResponse;
use ProvulusSDK\Client\Response\Quarantine\Message\Blob\MessageBlobResponse;

class FullMessageQuarantineRequest extends QuarantineBaseRequest
{

    /**
     * Type of request
     *
     * @return RequestType
     */
    public function httpType()
    {
        return RequestType::HTTP_GET();
    }

    /**
     * Process the data that is in the response
     *
     * @param array $jsonResponse
     *
     * @return IProvulusResponse
     */
    public function processResponse(array $jsonResponse)
    {
        return new MessageBlobResponse($jsonResponse['data']);
    }

    /**
     * Set the organization id for this request
     *
     * @param $id
     *
     * @return $this
     */
    public function setMessageRecipientId($id)
    {
        $this->addBinding('msgId', $id);

        return $this;
    }

    /**
     * Base route for orgs resource
     *
     * @return string
     * @throws \Exception
     */
    function baseRoute()
    {
        if (!$this->hasBinding('msgId')) {
            throw new \Exception(sprintf('Message Id needs to be set for %s', get_class($this)));
        }
        return parent::baseRoute() . '/:msgId/content';
    }
}
