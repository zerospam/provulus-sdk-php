<?php
/**
 * Created by PhpStorm.
 * User: aaflalo
 * Date: 29/11/16
 * Time: 3:07 PM
 */

namespace ProvulusSDK\Client\Request;

use ProvulusSDK\Client\Args\IRequestArg;
use ProvulusSDK\Client\Args\Merge\ArgMerger;
use ProvulusSDK\Client\Response\IProvulusResponse;
use Psr\Http\Message\UriInterface;

/**
 * Interface IProvulusRoute
 *
 * Route of the API
 *
 * @package ProvulusSDK\Client\Routes
 */
interface IProvulusRequest
{

    /**
     * Get the data used for the route
     *
     * @return array
     */
    public function toArray();

    /**
     * The url of the route
     *
     * @return string
     */
    public function routeUrl();


    /**
     * Type of request
     *
     * @return RequestType
     */
    public function httpType();

    /**
     * Process the data that is in the response
     *
     * @param array $jsonResponse
     *
     * @return IProvulusResponse
     */
    public function processResponse(array $jsonResponse);

    /**
     * Add a request argument
     *
     * @param IRequestArg $arg
     *
     * @return $this
     */
    public function addArgument(IRequestArg $arg);

    /**
     * Remove a request argument
     *
     * @param IRequestArg $arg
     *
     * @return $this
     */
    public function removeArgument(IRequestArg $arg);

    /**
     * Get all the request arguments
     *
     * @return IRequestArg[]
     */
    public function getArguments();

    /**
     * Return the URI of the request
     *
     * @return UriInterface
     */
    public function toUri();

    /**
     * Getter for response
     *
     * @return IProvulusResponse
     */
    public function getResponse();

    /**
     * @param IProvulusResponse $response
     */
    public function setResponse($response);

    /**
     * Options for this request to be used by the client
     *
     * @return array
     */
    public function requestOptions();

    /**
     * Get the argument that can be merged with each other
     *
     * @return ArgMerger[]
     */
    public function getMergeableArguments();

    /**
     * Number of tries of the request
     *
     * @return int
     */
    public function tries();

    /**
     * Increment the number of tries and returns it
     *
     * @return int
     */
    public function incrementTries();

    /**
     * Request type to use for the request
     *
     * @return RequestType
     */
    public function requestType();

    /**
     * Add a file to the request
     *
     * @param  string|resource $filePath  full path to the file or a resource (@see fopen)
     * @param  string          $fieldName $fieldName name of the field that file should be send with. If null, will
     *                                    take it from the name of the method
     * @param string           $filename  name of the file with extension. Optional.
     */
    public function addFile($filePath, $fieldName = null, $filename = null);
}
