<?php
/**
 * Created by PhpStorm.
 * User: ycoutu
 * Date: 12/07/17
 * Time: 10:29 AM
 */

namespace ProvulusSDK\Client\Request\Domain;

use ProvulusSDK\Client\Args\Search\Domain\FilteringPolicyArgument;

trait DomainSearchArguments
{
    /**
     * Setter for the filtering policy argument
     *
     * @param int $filteringPolicyId
     *
     * @return $this
     */
    public function setFilteringId($filteringPolicyId)
    {
        if (is_null($filteringPolicyId)) {
            return $this->removeArgument(new FilteringPolicyArgument(0));
        }

        return $this->addArgument(new FilteringPolicyArgument($filteringPolicyId));
    }
}
