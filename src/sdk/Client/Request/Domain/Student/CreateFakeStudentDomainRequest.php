<?php
/**
 * Created by PhpStorm.
 * User: pbb
 * Date: 04/10/18
 * Time: 3:48 PM
 */

namespace ProvulusSDK\Client\Request\Domain\Student;


use ProvulusSDK\Client\Request\RequestType;
use ProvulusSDK\Client\Request\Resource\Domain\BaseDomainRequest;
use ProvulusSDK\Client\Request\Resource\Domain\DomainProcessRequestTrait;

/**
 * Class DomainCreateFakeStudentRequest
 *
 * @package ProvulusSDK\Client\Request\Domain
 */
class CreateFakeStudentDomainRequest extends BaseDomainRequest
{
    use DomainProcessRequestTrait;

    /** @var  int */
    private $nbMailboxes;

    /** @var int */
    private $domainId;

    /**
     * Type of request
     *
     * @return RequestType
     */
    public function httpType()
    {
        return RequestType::HTTP_POST();
    }

    /**
     * Route for creating a domain
     *
     * @return string
     */
    function baseRoute()
    {
        return parent::baseRoute() . '/fake';
    }

    /**
     * @param int $nbMailboxes
     *
     * @return $this
     */
    public function setNbMailboxes($nbMailboxes)
    {
        $this->nbMailboxes = $nbMailboxes;

        return $this;
    }

    /**
     * @param int $domainId
     *
     * @return $this
     */
    public function setDomainId($domainId)
    {
        $this->domainId = $domainId;

        return $this;
    }
}