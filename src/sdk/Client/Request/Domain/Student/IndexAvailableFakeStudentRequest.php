<?php
/**
 * Created by PhpStorm.
 * User: pbb
 * Date: 10/10/18
 * Time: 2:34 PM
 */

namespace ProvulusSDK\Client\Request\Domain\Student;

use ProvulusSDK\Client\Request\Collection\CollectionOrderTrait;
use ProvulusSDK\Client\Request\Collection\IProvulusCollectionRequest;
use ProvulusSDK\Client\Request\RequestType;
use ProvulusSDK\Client\Request\Resource\Domain\Filtering\FilteringPolicyRequest;
use ProvulusSDK\Client\Response\Domain\DomainResponse;
use ProvulusSDK\Client\Response\Domain\UnpaginatedDomainCollectionResponse;
use ProvulusSDK\Client\Response\IProvulusObjectResponse;

/**
 * Class IndexAvailableFakeStudentRequest
 *
 * @method UnpaginatedDomainCollectionResponse getResponse()
 *
 * @package ProvulusSDK\Client\Request\Domain\Student
 */
class IndexAvailableFakeStudentRequest extends FilteringPolicyRequest implements IProvulusCollectionRequest
{

    use CollectionOrderTrait;

    /**
     * Type of request
     *
     * @return RequestType
     */
    public function httpType()
    {
        return RequestType::HTTP_GET();
    }

    /**
     * Route for creating a domain
     *
     * @return string
     * @throws \Exception
     */
    function baseRoute()
    {
        return parent::baseRoute() . '/standards_for_fake';
    }

    /**
     * Used to parse each result of the collection
     *
     * Need to be overridden by the class
     *
     * @param array $objectData
     *
     * @param array $includedData
     *
     * @return IProvulusObjectResponse
     */
    function toResponse(array $objectData, array $includedData = [])
    {
        return new DomainResponse($objectData);
    }

    /**
     * Class to be used to build the collection
     *
     * @return string
     */
    function collectionClass()
    {
        return UnpaginatedDomainCollectionResponse::class;
    }
}