<?php
/**
 * Created by PhpStorm.
 * User: pbb
 * Date: 19/12/16
 * Time: 10:24 AM
 */

namespace ProvulusSDK\Client\Request\Domain;


use ProvulusSDK\Client\Request\RequestType;
use ProvulusSDK\Client\Request\Resource\Domain\BaseDomainRequest;
use ProvulusSDK\Client\Request\Resource\Domain\DomainProcessRequestTrait;
use ProvulusSDK\Client\Request\Resource\Domain\DomainSetAttributesTrait;

/**
 * Class CreateDomainRequest
 *
 * Request for creating a domain for an organization.
 *
 * @package ProvulusSDK\Client\Request\Domain
 */
class CreateDomainRequest extends BaseDomainRequest
{
    use DomainSetAttributesTrait, DomainProcessRequestTrait;

    /**
     * @var boolean
     */
    private $acceptCost;

    /** @var  string */
    private $name;

    /** @var  int */
    private $nbMailboxes;

    /**
     * @param bool $acceptCost
     *
     * @return $this
     */
    public function setAcceptCost($acceptCost)
    {
        $this->acceptCost = $acceptCost;

        return $this;
    }

    /**
     * @param string $name
     *
     * @return CreateDomainRequest
     */
    public function setName($name)
    {
        $this->name = $name;

        return $this;
    }

    /**
     * @param int $nbMailboxes
     *
     * @return CreateDomainRequest
     */
    public function setNbMailboxes($nbMailboxes)
    {
        $this->nbMailboxes = $nbMailboxes;

        return $this;
    }

    /**
     * Type of request
     *
     * @return RequestType
     */
    public function httpType()
    {
        return RequestType::HTTP_POST();
    }
}