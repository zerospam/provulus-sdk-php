<?php
/**
 * Created by PhpStorm.
 * User: pbb
 * Date: 20/12/16
 * Time: 3:05 PM
 */

namespace ProvulusSDK\Client\Request\Domain;


use ProvulusSDK\Client\Request\RequestType;
use ProvulusSDK\Client\Request\Resource\Domain\DomainProcessRequestTrait;
use ProvulusSDK\Client\Request\Resource\Domain\DomainRequest;

/**
 * Class ReadDomainRequest
 *
 * Request for reading Domain
 *
 * @package ProvulusSDK\Client\Request\Domain
 */
class ReadDomainRequest extends DomainRequest
{
    use DomainProcessRequestTrait;

    /**
     * Type of request
     *
     * @return RequestType
     */
    public function httpType()
    {
        return RequestType::HTTP_GET();
    }
}