<?php
/**
 * Created by PhpStorm.
 * User: ycoutu
 * Date: 16/10/17
 * Time: 10:14 AM
 */

namespace ProvulusSDK\Client\Request\Domain\Filtering;

use ProvulusSDK\Client\Request\RequestType;
use ProvulusSDK\Client\Request\Resource\Domain\Filtering\FilteringPolicyRequest;
use ProvulusSDK\Client\Response\Domain\Filtering\FilteringList\AllFilteringListsResponse;
use ProvulusSDK\Client\Response\IProvulusResponse;

class AllFilteringListsRequest extends FilteringPolicyRequest
{

    /**
     * Type of request
     *
     * @return RequestType
     */
    public function httpType()
    {
        return RequestType::HTTP_GET();
    }

    /**
     * Process the data that is in the response
     *
     * @param array $jsonResponse
     *
     * @return IProvulusResponse
     */
    public function processResponse(array $jsonResponse)
    {
        return new AllFilteringListsResponse($jsonResponse);
    }

    public function baseRoute()
    {
        return parent::baseRoute() . '/all-items';
    }
}
