<?php
/**
 * Created by PhpStorm.
 * User: ycoutu
 * Date: 29/12/17
 * Time: 2:02 PM
 */

namespace ProvulusSDK\Client\Request\Domain\Filtering\Item;

use ProvulusSDK\Client\Request\BulkRequestTrait;
use ProvulusSDK\Client\Request\EmptyResponseTrait;
use ProvulusSDK\Client\Request\RequestType;
use ProvulusSDK\Client\Request\Resource\Organization\OrganizationRequest;
use ProvulusSDK\Enum\Filtering\FilteringPolicyEnum;

class MoveToOrganizationFilteringPolicyItemRequest extends OrganizationRequest
{
    use EmptyResponseTrait, BulkRequestTrait;

    /** @var int */
    private $organizationId;

    /** @var string */
    private $filteringPolicyName;

    /** @var FilteringPolicyEnum */
    private $filterTypeCode;

    /**
     * Type of request
     *
     * @return RequestType
     */
    public function httpType()
    {
        return RequestType::HTTP_POST();
    }

    /**
     * @return string
     * @throws \Exception
     */
    function baseRoute()
    {
        return parent::baseRoute() . '/policyitems/movetoorg';
    }


    /**
     * Setter for organization id
     *
     * @param $organizationId
     *
     * @return $this
     */
    public function setTargetOrganizationId($organizationId)
    {
        $this->organizationId = $organizationId;

        return $this;
    }

    /**
     * Setter for filtering policy name
     *
     * @param $filteringPolicyName
     *
     * @return $this
     */
    public function setFilteringPolicyName($filteringPolicyName)
    {
        $this->filteringPolicyName = $filteringPolicyName;

        return $this;
    }

    /**
     * @param FilteringPolicyEnum $filterTypeCode
     *
     * @return MoveToOrganizationFilteringPolicyItemRequest
     */
    public function setFilterTypeCode($filterTypeCode)
    {
        $this->filterTypeCode = $filterTypeCode;

        return $this;
    }
}
