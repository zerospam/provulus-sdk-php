<?php
/**
 * Created by PhpStorm.
 * User: ycoutu
 * Date: 14/07/17
 * Time: 11:47 AM
 */

namespace ProvulusSDK\Client\Request\Domain\Filtering\Item\Bulk;

use ProvulusSDK\Client\Request\BulkRequestTrait;
use ProvulusSDK\Client\Request\EmptyResponseTrait;
use ProvulusSDK\Client\Request\RequestType;
use ProvulusSDK\Client\Request\Resource\Domain\Filtering\Item\BaseFilteringPolicyItemRequest;

class BulkDeleteFilteringPolicyItemRequest extends BaseFilteringPolicyItemRequest
{
    use BulkRequestTrait, EmptyResponseTrait;
    /**
     * Type of request
     *
     * @return RequestType
     */
    public function httpType()
    {
        return RequestType::HTTP_DELETE();
    }
}
