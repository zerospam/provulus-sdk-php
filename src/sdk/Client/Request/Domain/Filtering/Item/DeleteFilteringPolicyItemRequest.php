<?php
/**
 * Created by PhpStorm.
 * User: pbb
 * Date: 22/12/16
 * Time: 3:03 PM
 */

namespace ProvulusSDK\Client\Request\Domain\Filtering\Item;

use ProvulusSDK\Client\Request\EmptyResponseTrait;
use ProvulusSDK\Client\Request\RequestType;
use ProvulusSDK\Client\Request\Resource\Domain\Filtering\Item\FilteringPolicyItemRequest;

/**
 * Class DeleteFilteringPolicyItemRequest
 *
 * Request for deleting a Filtering Policy item
 *
 * @package ProvulusSDK\Client\Request\Domain\Filtering\Item
 */
class DeleteFilteringPolicyItemRequest extends FilteringPolicyItemRequest
{
    use EmptyResponseTrait;

    /**
     * Type of request
     *
     * @return RequestType
     */
    public function httpType()
    {
        return RequestType::HTTP_DELETE();
    }
}