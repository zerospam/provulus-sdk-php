<?php
/**
 * Created by PhpStorm.
 * User: ycoutu
 * Date: 02/01/18
 * Time: 11:54 AM
 */

namespace ProvulusSDK\Client\Request\Domain\Filtering\Item;

use ProvulusSDK\Client\Args\Search\Domain\Filtering\FilterTypeArgument;
use ProvulusSDK\Client\Request\Collection\CollectionPaginatedTrait;
use ProvulusSDK\Client\Request\Collection\IProvulusCollectionPaginatedRequest;
use ProvulusSDK\Client\Request\Collection\SearchableTrait;
use ProvulusSDK\Client\Request\RequestType;
use ProvulusSDK\Client\Request\Resource\Domain\Filtering\Item\FilteringPolicyItemRequestTypeEnum;
use ProvulusSDK\Client\Request\Resource\Organization\OrganizationRequest;
use ProvulusSDK\Client\Response\Domain\Filtering\Item\FilteringPolicyItemCollectionResponse;
use ProvulusSDK\Client\Response\Domain\Filtering\Item\FilteringPolicyItemResponse;
use ProvulusSDK\Enum\Filtering\FilteringPolicyEnum;

/**
 * Class IndexFilteringPolicyItemByOrganizationRequest
 *
 * @method FilteringPolicyItemCollectionResponse getResponse()
 *
 * @package ProvulusSDK\Client\Request\Domain\Filtering\Item
 */
class AllFilteringPolicyItemRequest extends OrganizationRequest implements IProvulusCollectionPaginatedRequest
{
    use CollectionPaginatedTrait, SearchableTrait;

    /**
     * Type of request
     *
     * @return RequestType
     */
    public function httpType()
    {
        return RequestType::HTTP_GET();
    }

    /**
     * Used to parse each result of the collection
     *
     * Need to be overridden by the class
     *
     * @param array $array
     *
     * @return FilteringPolicyItemResponse
     */
    function toResponse(array $array)
    {
        return new FilteringPolicyItemResponse($array);
    }

    /**
     * Class to be used to build the collection
     *
     * @return string
     */
    function collectionClass()
    {
        return FilteringPolicyItemCollectionResponse::class;
    }

    /**
     * Sets the request type ; will be used for resource binding in the route
     *
     * @param FilteringPolicyItemRequestTypeEnum $requestType
     *
     * @return $this
     */
    public function setRequestType(FilteringPolicyItemRequestTypeEnum $requestType)
    {
        $this->addBinding('requestType', $requestType->getValue());

        return $this;
    }

    /**
     * Get only the filtering policy items with the given FilteringPolicyEnum
     *
     * @param FilteringPolicyEnum $type
     *
     * @return $this
     */
    public function setFilterType($type)
    {
        if (is_null($type)) {
            return $this->removeArgument(new FilterTypeArgument(FilteringPolicyEnum::INBOUND()));
        }

        return $this->addArgument(new FilterTypeArgument($type));
    }

    /**
     * @return string
     * @throws \Exception
     */
    function baseRoute()
    {
        if (!$this->hasBinding('requestType')) {
            throw new \Exception('request Type needs to be set for filtering policy item request.');
        }

        return parent::baseRoute() . '/:requestType';
    }
}
