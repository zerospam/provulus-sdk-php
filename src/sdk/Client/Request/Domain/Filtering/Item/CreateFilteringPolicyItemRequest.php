<?php
/**
 * Created by PhpStorm.
 * User: pbb
 * Date: 23/12/16
 * Time: 8:51 AM
 */

namespace ProvulusSDK\Client\Request\Domain\Filtering\Item;

use ProvulusSDK\Client\Request\RequestType;
use ProvulusSDK\Client\Request\Resource\Domain\Filtering\Item\BaseFilteringPolicyItemRequest;
use ProvulusSDK\Client\Request\Resource\Domain\Filtering\Item\FilteringPolicyItemRequestTypeEnum;
use ProvulusSDK\Client\Request\Resource\Domain\Filtering\Item\FilteringPolicyItemResponseTrait;
use ProvulusSDK\Client\Request\Resource\Domain\Filtering\Item\FilteringPolicyItemSetAttributesTrait;
use ProvulusSDK\Utils\Str;

/**
 * Class CreateFilteringPolicyItemDomainRequest
 *
 * Creates a Filtering Policy Item of type domain
 *
 * @package ProvulusSDK\Client\Request\Domain\Filtering\Item\Domain
 */
class CreateFilteringPolicyItemRequest extends BaseFilteringPolicyItemRequest
{
    use FilteringPolicyItemResponseTrait;

    /**
     * @var string
     */
    private $value;

    /**
     * Type of request
     *
     * @return RequestType
     */
    public function httpType()
    {
        return RequestType::HTTP_POST();
    }

    /**
     * Override the setValue function to allow it to set the request type according to the value
     *
     * @param $value
     *
     * @return $this
     */
    public function setValue($value)
    {
        $this->value = $value;
        $this->setRequestType(

            Str::contains($value, '@')
            ? FilteringPolicyItemRequestTypeEnum::ADDRESS()
            : FilteringPolicyItemRequestTypeEnum::DOMAIN()
        );

        return $this;
    }
}