<?php
/**
 * Created by PhpStorm.
 * User: pbb
 * Date: 22/12/16
 * Time: 2:24 PM
 */

namespace ProvulusSDK\Client\Request\Domain\Filtering\Item;


use ProvulusSDK\Client\Request\Collection\CollectionPaginatedTrait;
use ProvulusSDK\Client\Request\Collection\IProvulusCollectionPaginatedRequest;
use ProvulusSDK\Client\Request\Collection\SearchableTrait;
use ProvulusSDK\Client\Request\RequestType;
use ProvulusSDK\Client\Request\Resource\Domain\Filtering\Item\BaseFilteringPolicyItemRequest;
use ProvulusSDK\Client\Response\Domain\Filtering\Item\FilteringPolicyItemCollectionResponse;
use ProvulusSDK\Client\Response\Domain\Filtering\Item\FilteringPolicyItemResponse;

/**
 * Class IndexFilteringPolicyItemRequest
 *
 * Request for retrieving Filtering Policy Item Collection in the form
 * of a json body response
 *
 * @method FilteringPolicyItemCollectionResponse getResponse()
 *
 * @package ProvulusSDK\Client\Request\Domain\Filtering\Item
 */
class IndexFilteringPolicyItemRequest extends BaseFilteringPolicyItemRequest implements IProvulusCollectionPaginatedRequest
{

    use CollectionPaginatedTrait, SearchableTrait;

    /**
     * Type of request
     *
     * @return RequestType
     */
    public function httpType()
    {
        return RequestType::HTTP_GET();
    }

    /**
     * Used to parse each result of the collection
     *
     * Need to be overridden by the class
     *
     * @param array $array
     *
     * @return FilteringPolicyItemResponse
     */
    function toResponse(array $array)
    {
        return new FilteringPolicyItemResponse($array);
    }

    /**
     * Class to be used to build the collection
     *
     * @return string
     */
    function collectionClass()
    {
        return FilteringPolicyItemCollectionResponse::class;
    }
}