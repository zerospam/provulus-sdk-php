<?php
/**
 * Created by PhpStorm.
 * User: ycoutu
 * Date: 29/12/17
 * Time: 2:02 PM
 */

namespace ProvulusSDK\Client\Request\Domain\Filtering\Item;

use ProvulusSDK\Client\Request\BulkRequestTrait;
use ProvulusSDK\Client\Request\EmptyResponseTrait;
use ProvulusSDK\Client\Request\RequestType;
use ProvulusSDK\Client\Request\Resource\Organization\OrganizationRequest;

class MoveToPolicyFilteringPolicyItemRequest extends OrganizationRequest
{
    use EmptyResponseTrait, BulkRequestTrait;

    /** @var int */
    private $filteringPolicyId;

    /**
     * Type of request
     *
     * @return RequestType
     */
    public function httpType()
    {
        return RequestType::HTTP_POST();
    }

    /**
     * @return string
     * @throws \Exception
     */
    function baseRoute()
    {
        return parent::baseRoute() . '/policyitems/movetopolicy';
    }

    /**
     * Setter for filtering policy id
     *
     * @param int $filteringPolicyId
     *
     * @return $this
     */
    public function setTargetFilteringPolicyId($filteringPolicyId) {
        $this->filteringPolicyId = $filteringPolicyId;

        return $this;
    }
}
