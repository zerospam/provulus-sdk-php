<?php
/**
 * Created by PhpStorm.
 * User: pbb
 * Date: 22/12/16
 * Time: 2:06 PM
 */

namespace ProvulusSDK\Client\Request\Domain\Filtering\Item;

use ProvulusSDK\Client\Request\RequestType;
use ProvulusSDK\Client\Request\Resource\Domain\Filtering\Item\FilteringPolicyItemRequest;
use ProvulusSDK\Client\Request\Resource\Domain\Filtering\Item\FilteringPolicyItemResponseTrait;

/**
 * Class ReadFilteringPolicyItemRequest
 *
 * Request for retrieving a Filtering Policy Item Response
 *
 * @package ProvulusSDK\Client\Request\Domain\Filtering\Item
 */
class ReadFilteringPolicyItemRequest extends FilteringPolicyItemRequest
{
    use FilteringPolicyItemResponseTrait;

    /**
     * Type of request
     *
     * @return RequestType
     */
    public function httpType()
    {
        return RequestType::HTTP_GET();
    }
}