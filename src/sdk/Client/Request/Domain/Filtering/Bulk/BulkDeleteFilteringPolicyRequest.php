<?php
/**
 * Created by PhpStorm.
 * User: ycoutu
 * Date: 17/07/17
 * Time: 11:24 AM
 */

namespace ProvulusSDK\Client\Request\Domain\Filtering\Bulk;

use ProvulusSDK\Client\Request\BulkRequestTrait;
use ProvulusSDK\Client\Request\EmptyResponseTrait;
use ProvulusSDK\Client\Request\RequestType;
use ProvulusSDK\Client\Request\Resource\Domain\Filtering\BaseFilteringPolicyRequest;

class BulkDeleteFilteringPolicyRequest extends BaseFilteringPolicyRequest
{
    use BulkRequestTrait, EmptyResponseTrait;

    /**
     * Type of request
     *
     * @return RequestType
     */
    public function httpType()
    {
        return RequestType::HTTP_DELETE();
    }
}
