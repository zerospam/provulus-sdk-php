<?php
/**
 * Created by PhpStorm.
 * User: ycoutu
 * Date: 28/07/17
 * Time: 11:12 AM
 */

namespace ProvulusSDK\Client\Request\Domain\Filtering\Content;

use ProvulusSDK\Client\Request\Collection\CollectionPaginatedTrait;
use ProvulusSDK\Client\Request\Collection\IProvulusCollectionPaginatedRequest;
use ProvulusSDK\Client\Request\RequestType;
use ProvulusSDK\Client\Request\Resource\Domain\Filtering\FilteringPolicyRequest;
use ProvulusSDK\Client\Response\Domain\Filtering\Content\ContentCategoryCollectionResponse;
use ProvulusSDK\Client\Response\Domain\Filtering\Content\ContentCategoryResponse;
use ProvulusSDK\Client\Response\IProvulusObjectResponse;

class IndexContentCategoryRequest extends FilteringPolicyRequest implements IProvulusCollectionPaginatedRequest
{
    use CollectionPaginatedTrait;

    /**
     * Type of request
     *
     * @return RequestType
     */
    public function httpType()
    {
        return RequestType::HTTP_GET();
    }

    /**
     * Used to parse each result of the collection
     *
     * Need to be overridden by the class
     *
     * @param array $objectData
     *
     * @return IProvulusObjectResponse
     */
    function toResponse(array $objectData)
    {
        return new ContentCategoryResponse($objectData);
    }

    /**
     * Class to be used to build the collection
     *
     * @return string
     */
    function collectionClass()
    {
        return ContentCategoryCollectionResponse::class;
    }

    function baseRoute()
    {
        return parent::baseRoute() . '/contentcategories';
    }
}
