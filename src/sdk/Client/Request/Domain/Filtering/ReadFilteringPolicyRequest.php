<?php
/**
 * Created by PhpStorm.
 * User: pbb
 * Date: 21/12/16
 * Time: 4:11 PM
 */

namespace ProvulusSDK\Client\Request\Domain\Filtering;

use ProvulusSDK\Client\Request\RequestType;
use ProvulusSDK\Client\Request\Resource\Domain\Filtering\FilteringPolicyProcessResponseTrait;
use ProvulusSDK\Client\Request\Resource\Domain\Filtering\FilteringPolicyRequest;

/**
 * Class ReadFilteringPolicyRequest
 *
 * Request for reading a single filtering policy
 *
 * @package ProvulusSDK\Client\Request\Domain\Filtering\Base
 */
class ReadFilteringPolicyRequest extends FilteringPolicyRequest
{

    use FilteringPolicyProcessResponseTrait, WithFilteringPolicyItems, WithContentCategories;

    /**
     * Type of request
     *
     * @return RequestType
     */
    public function httpType()
    {
        return RequestType::HTTP_GET();
    }
}