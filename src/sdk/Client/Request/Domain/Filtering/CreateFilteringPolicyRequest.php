<?php
/**
 * Created by PhpStorm.
 * User: pbb
 * Date: 21/12/16
 * Time: 2:42 PM
 */

namespace ProvulusSDK\Client\Request\Domain\Filtering;

use ProvulusSDK\Client\Request\RequestType;
use ProvulusSDK\Client\Request\Resource\Domain\Filtering\BaseFilteringPolicyRequest;
use ProvulusSDK\Client\Request\Resource\Domain\Filtering\FilteringPolicyProcessResponseTrait;
use ProvulusSDK\Client\Request\Resource\Domain\Filtering\FilteringPolicySetTrait;

/**
 * Class CreateFilteringPolicyRequest
 *
 * Request for creating a filtering policy
 *
 * @package ProvulusSDK\Client\Request\Domain\Filtering
 */
class CreateFilteringPolicyRequest extends BaseFilteringPolicyRequest
{
    use FilteringPolicySetTrait, FilteringPolicyProcessResponseTrait;

    /** @var integer */
    private $baseFilteringPolicy;

    /**
     * @param int $baseFilteringPolicy
     * @return $this
     */
    public function setBaseFilteringPolicy($baseFilteringPolicy)
    {
        $this->baseFilteringPolicy = $baseFilteringPolicy;
        return $this;
    }

    /**
     * Type of request
     *
     * @return RequestType
     */
    public function httpType()
    {
        return RequestType::HTTP_POST();
    }
}