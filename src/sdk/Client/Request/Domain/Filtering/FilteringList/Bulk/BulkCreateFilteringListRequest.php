<?php
/**
 * Created by PhpStorm.
 * User: ycoutu
 * Date: 30/08/17
 * Time: 9:48 AM
 */

namespace ProvulusSDK\Client\Request\Domain\Filtering\FilteringList\Bulk;

use ProvulusSDK\Client\Request\Collection\CollectionOrderTrait;
use ProvulusSDK\Client\Request\Collection\IProvulusCollectionRequest;
use ProvulusSDK\Client\Request\RequestType;
use ProvulusSDK\Client\Request\Resource\Domain\Filtering\FilteringList\BaseFilteringListRequest;
use ProvulusSDK\Client\Response\Domain\Filtering\FilteringList\FilteringListCollectionResponse;
use ProvulusSDK\Client\Response\Domain\Filtering\FilteringList\FilteringListResponse;
use ProvulusSDK\Client\Response\IProvulusObjectResponse;
use ProvulusSDK\Utils\FilteringList\FilteringListObject;

class BulkCreateFilteringListRequest extends BaseFilteringListRequest implements IProvulusCollectionRequest
{
    use CollectionOrderTrait;

    /** @var FilteringListObject[] */
    private $filteringList;

    public function __construct()
    {
        $this->filteringList = [];
    }

    /**
     * Add a filtering list to create
     *
     * @param FilteringListObject $filteringList
     *
     * @return $this
     */
    public function addFilteringList(FilteringListObject $filteringList)
    {
        $this->filteringList[] = $filteringList->toArray();

        return $this;
    }

    /**
     * Type of request
     *
     * @return RequestType
     */
    public function httpType()
    {
        return RequestType::HTTP_POST();
    }

    /**
     * Used to parse each result of the collection
     *
     * Need to be overridden by the class
     *
     * @param array $objectData
     *
     * @return IProvulusObjectResponse
     */
    function toResponse(array $objectData)
    {
        return new FilteringListResponse($objectData);
    }

    /**
     * Class to be used to build the collection
     *
     * @return string
     */
    function collectionClass()
    {
        return FilteringListCollectionResponse::class;
    }

    function baseRoute()
    {
        return parent::baseRoute() . '/bulkcreate';
    }
}
