<?php
/**
 * Created by PhpStorm.
 * User: ycoutu
 * Date: 30/08/17
 * Time: 9:48 AM
 */

namespace ProvulusSDK\Client\Request\Domain\Filtering\FilteringList\Bulk;

use ProvulusSDK\Client\Request\BulkRequestTrait;
use ProvulusSDK\Client\Request\EmptyResponseTrait;
use ProvulusSDK\Client\Request\RequestType;
use ProvulusSDK\Client\Request\Resource\Domain\Filtering\FilteringList\BaseFilteringListRequest;

class BulkDeleteFilteringListRequest extends BaseFilteringListRequest
{
    use BulkRequestTrait, EmptyResponseTrait;

    /**
     * Type of request
     *
     * @return RequestType
     */
    public function httpType()
    {
        return RequestType::HTTP_DELETE();
    }
}
