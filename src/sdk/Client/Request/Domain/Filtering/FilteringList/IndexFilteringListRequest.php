<?php
/**
 * Created by PhpStorm.
 * User: ycoutu
 * Date: 30/08/17
 * Time: 9:38 AM
 */

namespace ProvulusSDK\Client\Request\Domain\Filtering\FilteringList;

use ProvulusSDK\Client\Request\Collection\CollectionPaginatedTrait;
use ProvulusSDK\Client\Request\Collection\IProvulusCollectionRequest;
use ProvulusSDK\Client\Request\Collection\SearchableTrait;
use ProvulusSDK\Client\Request\Domain\Filtering\FilteringList\Traits\FilteringListSearchArguments;
use ProvulusSDK\Client\Request\RequestType;
use ProvulusSDK\Client\Request\Resource\Domain\Filtering\FilteringList\BaseFilteringListRequest;
use ProvulusSDK\Client\Response\Domain\Filtering\FilteringList\FilteringListCollectionResponse;
use ProvulusSDK\Client\Response\Domain\Filtering\FilteringList\FilteringListResponse;
use ProvulusSDK\Client\Response\IProvulusObjectResponse;

class IndexFilteringListRequest extends BaseFilteringListRequest implements IProvulusCollectionRequest
{
    use CollectionPaginatedTrait, SearchableTrait, FilteringListSearchArguments;

    /**
     * Type of request
     *
     * @return RequestType
     */
    public function httpType()
    {
        return RequestType::HTTP_GET();
    }

    /**
     * Used to parse each result of the collection
     *
     * Need to be overridden by the class
     *
     * @param array $objectData
     *
     * @return IProvulusObjectResponse
     */
    function toResponse(array $objectData)
    {
        return new FilteringListResponse($objectData);
    }

    /**
     * Class to be used to build the collection
     *
     * @return string
     */
    function collectionClass()
    {
        return FilteringListCollectionResponse::class;
    }
}
