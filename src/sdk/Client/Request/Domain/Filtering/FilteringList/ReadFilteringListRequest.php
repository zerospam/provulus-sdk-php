<?php
/**
 * Created by PhpStorm.
 * User: ycoutu
 * Date: 30/08/17
 * Time: 9:40 AM
 */

namespace ProvulusSDK\Client\Request\Domain\Filtering\FilteringList;

use ProvulusSDK\Client\Request\RequestType;
use ProvulusSDK\Client\Request\Resource\Domain\Filtering\FilteringList\FilteringListProcessResponseTrait;
use ProvulusSDK\Client\Request\Resource\Domain\Filtering\FilteringList\FilteringListRequest;

class ReadFilteringListRequest extends FilteringListRequest
{
    use FilteringListProcessResponseTrait;

    /**
     * Type of request
     *
     * @return RequestType
     */
    public function httpType()
    {
        return RequestType::HTTP_GET();
    }
}
