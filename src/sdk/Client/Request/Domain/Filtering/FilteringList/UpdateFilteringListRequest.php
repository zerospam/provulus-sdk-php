<?php
/**
 * Created by PhpStorm.
 * User: ycoutu
 * Date: 30/08/17
 * Time: 9:41 AM
 */

namespace ProvulusSDK\Client\Request\Domain\Filtering\FilteringList;

use ProvulusSDK\Client\Request\RequestType;
use ProvulusSDK\Client\Request\Resource\Domain\Filtering\FilteringList\FilteringListProcessResponseTrait;
use ProvulusSDK\Client\Request\Resource\Domain\Filtering\FilteringList\FilteringListRequest;
use ProvulusSDK\Client\Request\Resource\Domain\Filtering\FilteringList\FilteringListSetTrait;

class UpdateFilteringListRequest extends FilteringListRequest
{
    use FilteringListSetTrait, FilteringListProcessResponseTrait;

    /**
     * Type of request
     *
     * @return RequestType
     */
    public function httpType()
    {
        return RequestType::HTTP_PUT();
    }
}
