<?php
/**
 * Created by PhpStorm.
 * User: ycoutu
 * Date: 30/08/17
 * Time: 10:22 AM
 */

namespace ProvulusSDK\Client\Request\Domain\Filtering\FilteringList\Traits;

use ProvulusSDK\Client\Args\Search\Domain\Filtering\FilteringList\FilteringListActionArgument;
use ProvulusSDK\Enum\Filtering\FilteringListActionEnum;

trait FilteringListSearchArguments
{
    /**
     * Set the filtering list action that we want to see
     *
     * @param FilteringListActionEnum|null $enum
     *
     * @return $this
     */
    public function setFilteringListAction($enum)
    {
        if (is_null($enum)) {
            return $this->removeArgument(new FilteringListActionArgument(FilteringListActionEnum::ALLOW()));
        }

        return $this->addArgument(new FilteringListActionArgument($enum));
    }
}
