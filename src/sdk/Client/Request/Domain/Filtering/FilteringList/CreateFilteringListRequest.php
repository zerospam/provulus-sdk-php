<?php
/**
 * Created by PhpStorm.
 * User: ycoutu
 * Date: 30/08/17
 * Time: 9:28 AM
 */

namespace ProvulusSDK\Client\Request\Domain\Filtering\FilteringList;

use ProvulusSDK\Client\Request\RequestType;
use ProvulusSDK\Client\Request\Resource\Domain\Filtering\FilteringList\BaseFilteringListRequest;
use ProvulusSDK\Client\Request\Resource\Domain\Filtering\FilteringList\FilteringListProcessResponseTrait;
use ProvulusSDK\Client\Request\Resource\Domain\Filtering\FilteringList\FilteringListSetTrait;

class CreateFilteringListRequest extends BaseFilteringListRequest
{
    use FilteringListSetTrait, FilteringListProcessResponseTrait;
    /**
     * Type of request
     *
     * @return RequestType
     */
    public function httpType()
    {
        return RequestType::HTTP_POST();
    }
}
