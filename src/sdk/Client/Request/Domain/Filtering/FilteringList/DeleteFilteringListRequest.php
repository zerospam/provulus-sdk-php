<?php
/**
 * Created by PhpStorm.
 * User: ycoutu
 * Date: 30/08/17
 * Time: 9:37 AM
 */

namespace ProvulusSDK\Client\Request\Domain\Filtering\FilteringList;

use ProvulusSDK\Client\Request\EmptyResponseTrait;
use ProvulusSDK\Client\Request\RequestType;
use ProvulusSDK\Client\Request\Resource\Domain\Filtering\FilteringList\FilteringListRequest;

class DeleteFilteringListRequest extends FilteringListRequest
{
    use EmptyResponseTrait;

    /**
     * Type of request
     *
     * @return RequestType
     */
    public function httpType()
    {
        return RequestType::HTTP_DELETE();
    }
}
