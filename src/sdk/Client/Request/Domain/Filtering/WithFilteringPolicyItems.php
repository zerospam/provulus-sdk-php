<?php
/**
 * Created by PhpStorm.
 * User: ycoutu
 * Date: 12/07/17
 * Time: 11:00 AM
 */

namespace ProvulusSDK\Client\Request\Domain\Filtering;

use ProvulusSDK\Client\Args\Relationship\IncludeRelationArg;

/**
 * Trait WithFilteringPolicyItems
 *
 * @package ProvulusSDK\Client\Request\Domain\Filtering
 */
trait WithFilteringPolicyItems
{
    /**
     * Include related filteringPolicyItems
     *
     * @return $this
     */
    public function withFilteringPolicyItems()
    {
        return $this->addArgument(new IncludeRelationArg('filteringPolicyItems'));
    }

    /**
     * Exclude related filteringPolicyItems
     *
     * @return $this
     */
    public function withoutFilteringPolicyItems()
    {
        return $this->removeArgument(new IncludeRelationArg('filteringPolicyItems'));
    }
}
