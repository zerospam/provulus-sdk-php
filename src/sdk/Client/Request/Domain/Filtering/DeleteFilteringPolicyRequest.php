<?php
/**
 * Created by PhpStorm.
 * User: pbb
 * Date: 22/12/16
 * Time: 11:13 AM
 */

namespace ProvulusSDK\Client\Request\Domain\Filtering;


use ProvulusSDK\Client\Request\EmptyResponseTrait;
use ProvulusSDK\Client\Request\RequestType;
use ProvulusSDK\Client\Request\Resource\Domain\Filtering\FilteringPolicyRequest;

/**
 * Class DeleteFilteringPolicyRequest
 *
 * Delete Filtering Policy Request
 *
 * @package ProvulusSDK\Client\Request\Domain\Filtering
 */
class DeleteFilteringPolicyRequest extends FilteringPolicyRequest
{
    use EmptyResponseTrait;

    /**
     * Type of request
     *
     * @return RequestType
     */
    public function httpType()
    {
        return RequestType::HTTP_DELETE();
    }
}