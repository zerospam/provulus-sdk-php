<?php
/**
 * Created by PhpStorm.
 * User: pbb
 * Date: 22/12/16
 * Time: 11:34 AM
 */

namespace ProvulusSDK\Client\Request\Domain\Filtering;

use ProvulusSDK\Client\Request\Collection\CollectionPaginatedTrait;
use ProvulusSDK\Client\Request\Collection\IProvulusCollectionPaginatedRequest;
use ProvulusSDK\Client\Request\Collection\SearchableTrait;
use ProvulusSDK\Client\Request\Domain\Filtering\Traits\FilteringPolicySearchArgument;
use ProvulusSDK\Client\Request\RequestType;
use ProvulusSDK\Client\Request\Resource\Domain\Filtering\BaseFilteringPolicyRequest;
use ProvulusSDK\Client\Response\Domain\Filtering\FilteringPolicyResponse;
use ProvulusSDK\Client\Response\Domain\Filtering\Item\FilteringPolicyCollectionResponse;
use ProvulusSDK\Client\Response\IProvulusObjectResponse;

/**
 * Class IndexFilteringPolicyRequest
 *
 * Request for retrieving Filtering Policy Collection
 * @method FilteringPolicyCollectionResponse getResponse()
 *
 * @package ProvulusSDK\Client\Request\Domain\Filtering
 */
class IndexFilteringPolicyRequest extends BaseFilteringPolicyRequest implements IProvulusCollectionPaginatedRequest
{

    use CollectionPaginatedTrait, SearchableTrait, FilteringPolicySearchArgument;
    /**
     * Type of request
     *
     * @return RequestType
     */
    public function httpType()
    {
        return RequestType::HTTP_GET();
    }

    /**
     * Used to parse each result of the collection
     *
     * Need to be overridden by the class
     *
     * @param array $array
     *
     * @return IProvulusObjectResponse
     */
    function toResponse(array $array)
    {
        return new FilteringPolicyResponse($array);
    }

    /**
     * Class to be used to build the collection
     *
     * @return string
     */
    function collectionClass()
    {
        return FilteringPolicyCollectionResponse::class;
    }
}