<?php
/**
 * Created by PhpStorm.
 * User: ycoutu
 * Date: 06/07/17
 * Time: 9:25 AM
 */

namespace ProvulusSDK\Client\Request\Domain\Filtering\Traits;

use ProvulusSDK\Client\Args\Search\Domain\Filtering\FilterTypeArgument;
use ProvulusSDK\Enum\Filtering\FilteringPolicyEnum;

trait FilteringPolicySearchArgument
{
    /**
     * Get only the filtering policy with the given FilteringPolicyEnum
     *
     * @param $type
     *
     * @return $this
     */
    public function setFilterType($type)
    {
        if (is_null($type)) {
            return $this->removeArgument(new FilterTypeArgument(FilteringPolicyEnum::INBOUND()));
        }

        return $this->addArgument(new FilterTypeArgument($type));
    }
}
