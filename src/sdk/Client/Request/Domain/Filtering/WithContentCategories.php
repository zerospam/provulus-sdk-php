<?php
/**
 * Created by PhpStorm.
 * User: ycoutu
 * Date: 27/07/17
 * Time: 3:25 PM
 */

namespace ProvulusSDK\Client\Request\Domain\Filtering;

use ProvulusSDK\Client\Args\Relationship\IncludeRelationArg;

trait WithContentCategories
{
    /**
     * Include related filteringPolicyItems
     *
     * @return $this
     */
    public function withContentCategories()
    {
        return $this->addArgument(new IncludeRelationArg('enabledCategories'));
    }

    /**
     * Exclude related filteringPolicyItems
     *
     * @return $this
     */
    public function withoutContentCategories()
    {
        return $this->removeArgument(new IncludeRelationArg('enabledCategories'));
    }
}
