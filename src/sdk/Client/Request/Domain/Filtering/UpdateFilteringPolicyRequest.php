<?php
/**
 * Created by PhpStorm.
 * User: pbb
 * Date: 21/12/16
 * Time: 3:37 PM
 */

namespace ProvulusSDK\Client\Request\Domain\Filtering;


use ProvulusSDK\Client\Request\RequestType;
use ProvulusSDK\Client\Request\Resource\Domain\Filtering\FilteringPolicyProcessResponseTrait;
use ProvulusSDK\Client\Request\Resource\Domain\Filtering\FilteringPolicyRequest;
use ProvulusSDK\Client\Request\Resource\Domain\Filtering\FilteringPolicySetTrait;

/**
 * Class UpdateFilteringPolicyRequest
 *
 * Request for updating a filtering policy
 *
 * @package ProvulusSDK\Client\Request\Domain\Filtering
 */
class UpdateFilteringPolicyRequest extends FilteringPolicyRequest
{

    use FilteringPolicySetTrait, FilteringPolicyProcessResponseTrait;

    /**
     * Type of request
     *
     * @return RequestType
     */
    public function httpType()
    {
        return RequestType::HTTP_PUT();
    }
}