<?php
/**
 * Created by PhpStorm.
 * User: pbb
 * Date: 20/12/16
 * Time: 2:11 PM
 */

namespace ProvulusSDK\Client\Request\Domain;


use ProvulusSDK\Client\Request\RequestType;
use ProvulusSDK\Client\Request\Resource\Domain\DomainProcessRequestTrait;
use ProvulusSDK\Client\Request\Resource\Domain\DomainRequest;
use ProvulusSDK\Client\Request\Resource\Domain\DomainSetAttributesTrait;

/**
 * Class UpdateDomainRequest
 *
 * Class for updating a domain
 *
 * @package ProvulusSDK\Client\Request\Domain
 */
class UpdateDomainRequest extends DomainRequest
{
    use DomainSetAttributesTrait, DomainProcessRequestTrait;

    /**
     * @param $description
     *
     * @return $this
     */
    public function setDescription($description)
    {
        $this->description = $description;
        $this->nullableChanged();

        return $this;
    }

    /**
     * Type of request
     *
     * @return RequestType
     */
    public function httpType()
    {
        return RequestType::HTTP_PUT();
    }
}