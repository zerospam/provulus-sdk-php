<?php
/**
 * Created by PhpStorm.
 * User: ycoutu
 * Date: 05/09/17
 * Time: 10:27 AM
 */

namespace ProvulusSDK\Client\Request\Domain;

use ProvulusSDK\Client\Request\BulkRequestTrait;
use ProvulusSDK\Client\Request\EmptyResponseTrait;
use ProvulusSDK\Client\Request\RequestType;
use ProvulusSDK\Client\Request\Resource\Domain\BaseDomainRequest;

class MoveToFilteringPolicyDomainRequest extends BaseDomainRequest
{
    use BulkRequestTrait, EmptyResponseTrait;

    /** @var int */
    private $filteringPolicyId;

    /**
     * Type of request
     *
     * @return RequestType
     */
    public function httpType()
    {
        return RequestType::HTTP_POST();
    }

    public function baseRoute()
    {
        return parent::baseRoute() . '/movetopolicy';
    }

    /**
     * Setter for filtering policy id
     *
     * @param int $filteringPolicyId
     *
     * @return $this
     */
    public function setTargetFilteringPolicyId($filteringPolicyId) {
        $this->filteringPolicyId = $filteringPolicyId;

        return $this;
    }
}
