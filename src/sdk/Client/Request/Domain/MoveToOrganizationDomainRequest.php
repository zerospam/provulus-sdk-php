<?php
/**
 * Created by PhpStorm.
 * User: ycoutu
 * Date: 05/09/17
 * Time: 10:27 AM
 */

namespace ProvulusSDK\Client\Request\Domain;

use ProvulusSDK\Client\Request\BulkRequestTrait;
use ProvulusSDK\Client\Request\EmptyResponseTrait;
use ProvulusSDK\Client\Request\RequestType;
use ProvulusSDK\Client\Request\Resource\Domain\BaseDomainRequest;

class MoveToOrganizationDomainRequest extends BaseDomainRequest
{
    use BulkRequestTrait, EmptyResponseTrait;

    /** @var int */
    private $organizationId;

    /** @var string */
    private $filteringPolicyName;

    /**
     * Type of request
     *
     * @return RequestType
     */
    public function httpType()
    {
        return RequestType::HTTP_POST();
    }

    public function baseRoute()
    {
        return parent::baseRoute() . '/movetoorg';
    }

    /**
     * Setter for organization id
     *
     * @param $organizationId
     *
     * @return $this
     */
    public function setTargetOrganizationId($organizationId)
    {
        $this->organizationId = $organizationId;

        return $this;
    }

    /**
     * Setter for filtering policy name
     *
     * @param $filteringPolicyName
     *
     * @return $this
     */
    public function setFilteringPolicyName($filteringPolicyName)
    {
        $this->filteringPolicyName = $filteringPolicyName;

        return $this;
    }
}
