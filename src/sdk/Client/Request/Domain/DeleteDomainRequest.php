<?php
/**
 * Created by PhpStorm.
 * User: pbb
 * Date: 20/12/16
 * Time: 4:42 PM
 */

namespace ProvulusSDK\Client\Request\Domain;


use ProvulusSDK\Client\Request\EmptyResponseTrait;
use ProvulusSDK\Client\Request\RequestType;
use ProvulusSDK\Client\Request\Resource\Domain\DomainRequest;

/**
 * Class DeleteDomainRequest
 *
 * Request for deleting a domain.
 *
 * @package ProvulusSDK\Client\Request\Domain
 */
class DeleteDomainRequest extends DomainRequest
{

    use EmptyResponseTrait;

    /**
     * Type of request
     *
     * @return RequestType
     */
    public function httpType()
    {
        return RequestType::HTTP_DELETE();
    }
}