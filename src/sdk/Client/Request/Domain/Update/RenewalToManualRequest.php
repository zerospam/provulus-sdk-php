<?php
/**
 * Created by PhpStorm.
 * User: ycoutu
 * Date: 08/03/19
 * Time: 11:21 AM
 */

namespace ProvulusSDK\Client\Request\Domain\Update;

use ProvulusSDK\Client\Request\RequestType;
use ProvulusSDK\Client\Request\Resource\Organization\OrganizationProcessResponseTrait;
use ProvulusSDK\Client\Request\Resource\Organization\OrganizationRequest;

class RenewalToManualRequest extends OrganizationRequest
{
    use OrganizationProcessResponseTrait;

    /**
     * Type of request
     *
     * @return RequestType
     */
    public function httpType()
    {
        return RequestType::HTTP_POST();
    }

    /**
     * Base route for orgs resource
     *
     * @return string
     * @throws \Exception
     */
    function baseRoute()
    {
        return parent::baseRoute() . '/mailbox/to-manual';
    }
}
