<?php
/**
 * Created by PhpStorm.
 * User: ycoutu
 * Date: 07/03/19
 * Time: 10:08 AM
 */

namespace ProvulusSDK\Client\Request\Domain\Update;

use Carbon\Carbon;
use ProvulusSDK\Client\Args\Search\Domain\Update\DomainNameArgument;
use ProvulusSDK\Client\Args\Search\Domain\Update\EndRenewalDateArgument;
use ProvulusSDK\Client\Args\Search\Domain\Update\RenewalStatesArgument;
use ProvulusSDK\Client\Args\Search\Domain\Update\StartRenewalDateArgument;
use ProvulusSDK\Client\Args\Search\Organization\BilledByArgument;
use ProvulusSDK\Client\Args\Search\Organization\CorporateNameArgument;
use ProvulusSDK\Client\Args\Search\Organization\SupportedByArgument;
use ProvulusSDK\Client\Request\BaseProvulusRequest;
use ProvulusSDK\Client\Request\Collection\CollectionPaginatedTrait;
use ProvulusSDK\Client\Request\Collection\IProvulusCollectionPaginatedRequest;
use ProvulusSDK\Client\Request\IndexCanDisplayAllTrait;
use ProvulusSDK\Client\Request\RequestType;
use ProvulusSDK\Client\Response\Domain\Update\DomainMailboxCollectionResponse;
use ProvulusSDK\Client\Response\Domain\Update\DomainMailboxResponse;
use ProvulusSDK\Client\Response\IProvulusObjectResponse;

/**
 * Class IndexDomainMailboxRequest
 *
 * @method DomainMailboxCollectionResponse getResponse()
 *
 * @package ProvulusSDK\Client\Request\Domain\Update
 */
class IndexDomainMailboxRequest extends BaseProvulusRequest implements IProvulusCollectionPaginatedRequest
{
    use CollectionPaginatedTrait, IndexCanDisplayAllTrait;

    /**
     * Set the corporate name to search for
     *
     * @param string|null $corporateName
     *
     * @return $this
     */
    public function setCorporateName($corporateName)
    {
        if (is_null($corporateName)) {
            $this->removeArgument(new CorporateNameArgument(''));
        }

        $this->addArgument(new CorporateNameArgument($corporateName));

        return $this;
    }

    /**
     * Set the domain name to search for
     *
     * @param string|null $domainName
     *
     * @return $this
     */
    public function setDomainName($domainName)
    {
        if (is_null($domainName)) {
            $this->removeArgument(new DomainNameArgument(''));
        }

        $this->addArgument(new DomainNameArgument($domainName));

        return $this;
    }

    /**
     * Set the start renewal date to search for
     *
     * @param string|Carbon|null $renewalDate
     *
     * @return $this
     */
    public function setStartRenewalDate($renewalDate)
    {
        if (is_null($renewalDate)) {
            $this->removeArgument(new StartRenewalDateArgument(Carbon::today()->toDateTimeString()));
        }

        $this->addArgument(new StartRenewalDateArgument($renewalDate));

        return $this;
    }

    /**
     * Set the end renewal date to search for
     *
     * @param string|Carbon|null $renewalDate
     *
     * @return $this
     */
    public function setEndRenewalDate($renewalDate)
    {
        if (is_null($renewalDate)) {
            $this->removeArgument(new EndRenewalDateArgument(Carbon::today()->toDateTimeString()));
        }

        $this->addArgument(new EndRenewalDateArgument($renewalDate));

        return $this;
    }

    /**
     * Set the renewal states to search for
     *
     * @param string|null $renewalStates
     *
     * @return $this
     */
    public function setRenewalStates($renewalStates)
    {
        if (is_null($renewalStates)) {
            $this->removeArgument(new RenewalStatesArgument(''));
        }

        $this->addArgument(new RenewalStatesArgument($renewalStates));

        return $this;
    }

    /**
     * Set the support organization id to search for
     *
     * @param int|null $supportedBy
     *
     * @return $this
     */
    public function setSupportedBy($supportedBy)
    {
        if (is_null($supportedBy)) {
            $this->removeArgument(new SupportedByArgument(''));
        }

        $this->addArgument(new SupportedByArgument($supportedBy));

        return $this;
    }

    /**
     * Set the billing organization id to search for
     *
     * @param int|null $billedBy
     *
     * @return $this
     */
    public function setBilledBy($billedBy)
    {
        if (is_null($billedBy)) {
            $this->removeArgument(new BilledByArgument(''));
        }

        $this->addArgument(new BilledByArgument($billedBy));

        return $this;
    }

    /**
     * The url of the route
     *
     * @return string
     */
    public function routeUrl()
    {
        return 'domain-mailboxes';
    }

    /**
     * Type of request
     *
     * @return RequestType
     */
    public function httpType()
    {
        return RequestType::HTTP_GET();
    }

    /**
     * Used to parse each result of the collection
     *
     * Need to be overridden by the class
     *
     * @param array $objectData
     *
     * @param array $includedData
     *
     * @return IProvulusObjectResponse
     */
    function toResponse(array $objectData, array $includedData = [])
    {
        return new DomainMailboxResponse($objectData, $includedData);
    }

    /**
     * Class to be used to build the collection
     *
     * @return string
     */
    function collectionClass()
    {
        return DomainMailboxCollectionResponse::class;
    }
}
