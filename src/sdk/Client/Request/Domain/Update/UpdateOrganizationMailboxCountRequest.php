<?php
/**
 * Created by PhpStorm.
 * User: ycoutu
 * Date: 11/03/19
 * Time: 2:18 PM
 */

namespace ProvulusSDK\Client\Request\Domain\Update;

use ProvulusSDK\Client\Request\EmptyResponseTrait;
use ProvulusSDK\Client\Request\RequestType;
use ProvulusSDK\Client\Request\Resource\Organization\OrganizationRequest;

class UpdateOrganizationMailboxCountRequest extends OrganizationRequest
{
    use EmptyResponseTrait;

    private $domains = [];

    /**
     * Type of request
     *
     * @return RequestType
     */
    public function httpType()
    {
        return RequestType::HTTP_POST();
    }

    /**
     * Base route for orgs resource
     *
     * @return string
     * @throws \Exception
     */
    function baseRoute()
    {
        return parent::baseRoute() . '/mailbox/submit';
    }

    /**
     * Set the domain count for a specific id
     *
     * @param int $id
     * @param int $count
     * @return $this
     */
    public function addDomainCount($id, $count)
    {
        $this->domains[$id] = $count;

        return $this;
    }

    /**
     * Set all the domains count
     *
     * @param array $domains
     * @return $this
     */
    public function setDomainsCount($domains)
    {
        $this->domains = $domains;

        return $this;
    }
}
