<?php
/**
 * Created by PhpStorm.
 * User: ycoutu
 * Date: 08/03/19
 * Time: 1:15 PM
 */

namespace ProvulusSDK\Client\Request\Domain\Update;

use ProvulusSDK\Client\Request\RequestType;
use ProvulusSDK\Client\Request\Resource\Organization\OrganizationProcessResponseTrait;
use ProvulusSDK\Client\Request\Resource\Organization\OrganizationRequest;

class ConfirmMailboxUpdateRequest extends OrganizationRequest
{
    use OrganizationProcessResponseTrait;

    /**
     * Type of request
     *
     * @return RequestType
     */
    public function httpType()
    {
        return RequestType::HTTP_POST();
    }

    /**
     * Base route for orgs resource
     *
     * @return string
     * @throws \Exception
     */
    function baseRoute()
    {
        return parent::baseRoute() . '/mailbox/confirm';
    }
}
