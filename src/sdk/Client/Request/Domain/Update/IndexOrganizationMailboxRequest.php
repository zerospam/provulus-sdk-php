<?php
/**
 * Created by PhpStorm.
 * User: ycoutu
 * Date: 08/03/19
 * Time: 3:49 PM
 */

namespace ProvulusSDK\Client\Request\Domain\Update;

use ProvulusSDK\Client\Request\Collection\CollectionPaginatedTrait;
use ProvulusSDK\Client\Request\IndexCanDisplayAllTrait;
use ProvulusSDK\Client\Request\RequestType;
use ProvulusSDK\Client\Request\Resource\Organization\OrganizationRequest;
use ProvulusSDK\Client\Response\Domain\Update\DomainMailboxCollectionResponse;
use ProvulusSDK\Client\Response\Domain\Update\DomainMailboxResponse;
use ProvulusSDK\Client\Response\IProvulusObjectResponse;

/**
 * Class IndexOrganizationMailboxRequest
 *
 * @method DomainMailboxCollectionResponse getResponse()
 *
 * @package ProvulusSDK\Client\Request\Domain\Update
 */
class IndexOrganizationMailboxRequest extends OrganizationRequest
{
    use CollectionPaginatedTrait, IndexCanDisplayAllTrait;
    /**
     * Type of request
     *
     * @return RequestType
     */
    public function httpType()
    {
        return RequestType::HTTP_GET();
    }

    /**
     * Used to parse each result of the collection
     *
     * Need to be overridden by the class
     *
     * @param array $objectData
     *
     * @param array $includedData
     *
     * @return IProvulusObjectResponse
     */
    function toResponse(array $objectData, array $includedData = [])
    {
        return new DomainMailboxResponse($objectData, $includedData);
    }

    /**
     * Class to be used to build the collection
     *
     * @return string
     */
    function collectionClass()
    {
        return DomainMailboxCollectionResponse::class;
    }

    function baseRoute()
    {
        return parent::baseRoute() . '/mailbox';
    }
}
