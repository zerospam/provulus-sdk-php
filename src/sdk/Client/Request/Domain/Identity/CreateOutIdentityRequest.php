<?php
/**
 * Created by PhpStorm.
 * User: ycoutu
 * Date: 10/10/17
 * Time: 3:59 PM
 */

namespace ProvulusSDK\Client\Request\Domain\Identity;

use ProvulusSDK\Client\Request\RequestType;
use ProvulusSDK\Client\Request\Resource\Domain\Identity\BaseOutIdentityRequest;
use ProvulusSDK\Client\Request\Resource\Domain\Identity\OutIdentityProcessRequestTrait;
use ProvulusSDK\Client\Request\Resource\Domain\Identity\OutIdentitySetAttributesTrait;
use ProvulusSDK\Enum\Domain\Identity\IdentityAuthenticationTypeEnum;

class CreateOutIdentityRequest extends BaseOutIdentityRequest
{
    use OutIdentitySetAttributesTrait, OutIdentityProcessRequestTrait;

    /** @var IdentityAuthenticationTypeEnum */
    private $authenticationTypeCode;

    /**
     * @param IdentityAuthenticationTypeEnum $authenticationType
     *
     * @return $this
     */
    public function setAuthenticationTypeCode($authenticationType)
    {
        $this->authenticationTypeCode = $authenticationType;

        return $this;
    }

    /**
     * Type of request
     *
     * @return RequestType
     */
    public function httpType()
    {
        return RequestType::HTTP_POST();
    }
}
