<?php
/**
 * Created by PhpStorm.
 * User: ycoutu
 * Date: 18/10/17
 * Time: 11:11 AM
 */

namespace ProvulusSDK\Client\Request\Domain\Identity\Type;

use ProvulusSDK\Client\Request\RequestType;
use ProvulusSDK\Client\Request\Resource\Domain\Identity\OutIdentityRequest;
use ProvulusSDK\Client\Response\Domain\Identity\Type\OutIdentityLoginResponse;
use ProvulusSDK\Client\Response\IProvulusResponse;

/**
 * Class ResetPasswordOutIdentityRequest
 *
 * @method OutIdentityLoginResponse getResponse()
 *
 * @package ProvulusSDK\Client\Request\Domain\Identity\Type
 */
class ResetPasswordOutIdentityRequest extends OutIdentityRequest
{

    /**
     * Type of request
     *
     * @return RequestType
     */
    public function httpType()
    {
        return RequestType::HTTP_POST();
    }

    /**
     * Process the data that is in the response
     *
     * @param array $jsonResponse
     *
     * @return IProvulusResponse
     */
    public function processResponse(array $jsonResponse)
    {
        return new OutIdentityLoginResponse($jsonResponse['data']);
    }

    public function baseRoute()
    {
        return parent::baseRoute() . '/identity-logins/reset-password';
    }
}
