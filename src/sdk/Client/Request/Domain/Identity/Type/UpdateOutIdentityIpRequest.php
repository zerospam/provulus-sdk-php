<?php
/**
 * Created by PhpStorm.
 * User: ycoutu
 * Date: 18/10/17
 * Time: 2:48 PM
 */

namespace ProvulusSDK\Client\Request\Domain\Identity\Type;

use ProvulusSDK\Client\Request\RequestType;
use ProvulusSDK\Client\Request\Resource\Domain\Identity\OutIdentityProcessRequestTrait;
use ProvulusSDK\Client\Request\Resource\Domain\Identity\OutIdentityRequest;

class UpdateOutIdentityIpRequest extends OutIdentityRequest
{
    use OutIdentityProcessRequestTrait;
    /** @var string[] */
    private $ips;

    /**
     * Setter for ips
     *
     * @param string[] $ips
     *
     * @return $this
     */
    public function setIps($ips)
    {
        $this->ips = $ips;

        return $this;
    }

    /**
     * Type of request
     *
     * @return RequestType
     */
    public function httpType()
    {
        return RequestType::HTTP_POST();
    }

    public function baseRoute()
    {
        return parent::baseRoute() . '/identity-ips';
    }
}
