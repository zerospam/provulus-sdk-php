<?php
/**
 * Created by PhpStorm.
 * User: ycoutu
 * Date: 10/10/17
 * Time: 4:05 PM
 */

namespace ProvulusSDK\Client\Request\Domain\Identity;

use ProvulusSDK\Client\Request\RequestType;
use ProvulusSDK\Client\Request\Resource\Domain\Identity\OutIdentityProcessRequestTrait;
use ProvulusSDK\Client\Request\Resource\Domain\Identity\OutIdentityRequest;

class ReadOutIdentityRequest extends OutIdentityRequest
{
    use OutIdentityProcessRequestTrait;
    /**
     * Type of request
     *
     * @return RequestType
     */
    public function httpType()
    {
        return RequestType::HTTP_GET();
    }
}
