<?php
/**
 * Created by PhpStorm.
 * User: ycoutu
 * Date: 10/10/17
 * Time: 4:06 PM
 */

namespace ProvulusSDK\Client\Request\Domain\Identity;

use ProvulusSDK\Client\Request\RequestType;
use ProvulusSDK\Client\Request\Resource\Domain\Identity\OutIdentityProcessRequestTrait;
use ProvulusSDK\Client\Request\Resource\Domain\Identity\OutIdentityRequest;
use ProvulusSDK\Client\Request\Resource\Domain\Identity\OutIdentitySetAttributesTrait;
use ProvulusSDK\Client\Response\Domain\Identity\OutIdentityResponse;

/**
 * Class UpdateOutIdentityRequest
 *
 * @method OutIdentityResponse getResponse()
 *
 * @package ProvulusSDK\Client\Request\Domain\Identity
 */
class UpdateOutIdentityRequest extends OutIdentityRequest
{
    use OutIdentityProcessRequestTrait, OutIdentitySetAttributesTrait;
    /**
     * Type of request
     *
     * @return RequestType
     */
    public function httpType()
    {
        return RequestType::HTTP_PUT();
    }
}
