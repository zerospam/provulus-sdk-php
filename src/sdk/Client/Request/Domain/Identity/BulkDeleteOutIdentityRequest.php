<?php
/**
 * Created by PhpStorm.
 * User: ycoutu
 * Date: 12/10/17
 * Time: 2:01 PM
 */

namespace ProvulusSDK\Client\Request\Domain\Identity;

use ProvulusSDK\Client\Request\BulkRequestTrait;
use ProvulusSDK\Client\Request\EmptyResponseTrait;
use ProvulusSDK\Client\Request\RequestType;
use ProvulusSDK\Client\Request\Resource\Domain\Identity\BaseOutIdentityRequest;

class BulkDeleteOutIdentityRequest extends BaseOutIdentityRequest
{
    use BulkRequestTrait, EmptyResponseTrait;

    /**
     * Type of request
     *
     * @return RequestType
     */
    public function httpType()
    {
        return RequestType::HTTP_DELETE();
    }
}
