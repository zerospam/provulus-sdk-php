<?php
/**
 * Created by PhpStorm.
 * User: ycoutu
 * Date: 10/10/17
 * Time: 4:08 PM
 */

namespace ProvulusSDK\Client\Request\Domain\Identity;

use ProvulusSDK\Client\Request\EmptyResponseTrait;
use ProvulusSDK\Client\Request\RequestType;
use ProvulusSDK\Client\Request\Resource\Domain\Identity\OutIdentityRequest;

class DeleteOutIdentityRequest extends OutIdentityRequest
{
    use EmptyResponseTrait;

    /**
     * Type of request
     *
     * @return RequestType
     */
    public function httpType()
    {
        return RequestType::HTTP_DELETE();
    }
}
