<?php
/**
 * Created by PhpStorm.
 * User: ycoutu
 * Date: 10/10/17
 * Time: 4:02 PM
 */

namespace ProvulusSDK\Client\Request\Domain\Identity;

use ProvulusSDK\Client\Request\Collection\CollectionPaginatedTrait;
use ProvulusSDK\Client\Request\Collection\IProvulusCollectionPaginatedRequest;
use ProvulusSDK\Client\Request\RequestType;
use ProvulusSDK\Client\Request\Resource\Domain\Identity\BaseOutIdentityRequest;
use ProvulusSDK\Client\Response\Domain\Identity\OutIdentityCollectionResponse;
use ProvulusSDK\Client\Response\Domain\Identity\OutIdentityResponse;
use ProvulusSDK\Client\Response\IProvulusObjectResponse;

/**
 * Class IndexOutIdentityRequest
 *
 * @method OutIdentityCollectionResponse getResponse()
 *
 * @package ProvulusSDK\Client\Request\Domain\Identity
 */
class IndexOutIdentityRequest extends BaseOutIdentityRequest implements IProvulusCollectionPaginatedRequest
{
    use CollectionPaginatedTrait;

    /**
     * Type of request
     *
     * @return RequestType
     */
    public function httpType()
    {
        return RequestType::HTTP_GET();
    }

    /**
     * Used to parse each result of the collection
     *
     * Need to be overridden by the class
     *
     * @param array $objectData
     * @param array $includedData
     *
     * @return IProvulusObjectResponse
     */
    function toResponse(array $objectData, array $includedData = [])
    {
        return new OutIdentityResponse($objectData, $includedData);
    }

    /**
     * Class to be used to build the collection
     *
     * @return string
     */
    function collectionClass()
    {
        return OutIdentityCollectionResponse::class;
    }
}
