<?php
/**
 * Created by PhpStorm.
 * User: ycoutu
 * Date: 10/10/17
 * Time: 4:22 PM
 */

namespace ProvulusSDK\Client\Request\Domain\Identity\Ip;

use ProvulusSDK\Client\Request\Collection\CollectionOrderTrait;
use ProvulusSDK\Client\Request\Collection\IProvulusCollectionRequest;
use ProvulusSDK\Client\Request\RequestType;
use ProvulusSDK\Client\Request\Resource\Domain\Identity\Ip\BaseOutClientsIpRequest;
use ProvulusSDK\Client\Response\Domain\Identity\Ip\OutClientsIpCollectionResponse;
use ProvulusSDK\Client\Response\Domain\Identity\Ip\OutClientsIpResponse;
use ProvulusSDK\Client\Response\IProvulusObjectResponse;

/**
 * Class UpdateOutClientsIpRequest
 *
 * @method OutClientsIpCollectionResponse getResponse()
 *
 * @package ProvulusSDK\Client\Request\Domain\Identity\Ip
 */
class IndexOutClientsIpRequest extends BaseOutClientsIpRequest implements IProvulusCollectionRequest
{
    use CollectionOrderTrait;

    /**
     * Type of request
     *
     * @return RequestType
     */
    public function httpType()
    {
        return RequestType::HTTP_GET();
    }

    /**
     * Used to parse each result of the collection
     *
     * Need to be overridden by the class
     *
     * @param array $objectData
     *
     * @return IProvulusObjectResponse
     */
    function toResponse(array $objectData)
    {
        return new OutClientsIpResponse($objectData);
    }

    /**
     * Class to be used to build the collection
     *
     * @return string
     */
    function collectionClass()
    {
        return OutClientsIpCollectionResponse::class;
    }
}
