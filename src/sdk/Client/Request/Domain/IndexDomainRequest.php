<?php
/**
 * Created by PhpStorm.
 * User: pbb
 * Date: 21/12/16
 * Time: 1:03 PM
 */

namespace ProvulusSDK\Client\Request\Domain;

use ProvulusSDK\Client\Args\Search\Domain\DeletableArgument;
use ProvulusSDK\Client\Request\Collection\CollectionPaginatedTrait;
use ProvulusSDK\Client\Request\Collection\IProvulusCollectionPaginatedRequest;
use ProvulusSDK\Client\Request\Collection\SearchableTrait;
use ProvulusSDK\Client\Request\IndexCanDisplayAllTrait;
use ProvulusSDK\Client\Request\RequestType;
use ProvulusSDK\Client\Request\Resource\Domain\BaseDomainRequest;
use ProvulusSDK\Client\Response\Domain\DomainCollectionResponse;
use ProvulusSDK\Client\Response\Domain\DomainResponse;
use ProvulusSDK\Client\Response\IProvulusObjectResponse;

/**
 * Class IndexDomainRequest
 *
 * Request for retrieving Domain Collection
 *
 * @method DomainCollectionResponse getResponse()
 *
 * @package ProvulusSDK\Client\Request\Domain
 */
class IndexDomainRequest extends BaseDomainRequest implements IProvulusCollectionPaginatedRequest
{

    use CollectionPaginatedTrait, SearchableTrait, DomainSearchArguments;

    /**
     * Type of request
     *
     * @return RequestType
     */
    public function httpType()
    {
        return RequestType::HTTP_GET();
    }

    /**
     * Used to parse each result of the collection
     *
     * Need to be overridden by the class
     *
     * @param array $array
     *
     * @return IProvulusObjectResponse
     */
    function toResponse(array $array)
    {
        return new DomainResponse($array);
    }

    /**
     * Class to be used to build the collection
     *
     * @return string
     */
    function collectionClass()
    {
        return DomainCollectionResponse::class;
    }

    /**
     * Ask for the deletable information
     *
     * @return $this
     */
    function withDeletable()
    {
        $this->addArgument(new DeletableArgument(true));

        return $this;
    }
}
