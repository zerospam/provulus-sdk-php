<?php
/**
 * Created by PhpStorm.
 * User: ycoutu
 * Date: 28/02/19
 * Time: 1:52 PM
 */

namespace ProvulusSDK\Client\Request\Domain\Mailbox;

use ProvulusSDK\Client\Request\RequestType;
use ProvulusSDK\Client\Request\Resource\Domain\Mailbox\BaseMailboxRequest;
use ProvulusSDK\Client\Response\Domain\Mailbox\MailboxResponse;
use ProvulusSDK\Client\Response\IProvulusResponse;

/**
 * Class ReadMailboxRequest
 *
 * @method MailboxResponse getResponse()
 *
 * @package ProvulusSDK\Client\Request\Domain\Mailbox
 */
class ReadMailboxRequest extends BaseMailboxRequest
{

    /**
     * Type of request
     *
     * @return RequestType
     */
    public function httpType()
    {
        return RequestType::HTTP_GET();
    }

    /**
     * Process the data that is in the response
     *
     * @param array $jsonResponse
     *
     * @return IProvulusResponse
     */
    public function processResponse(array $jsonResponse)
    {
        return new MailboxResponse($jsonResponse['data']);
    }

    /**
     * Route for the request
     *
     * @return string
     * @throws \Exception
     */
    public function baseRoute()
    {
        return parent::baseRoute() . '/:mailboxId';
    }

    /**
     * Set the domain for the request
     *
     * @param $id
     *
     * @return $this
     */
    public function setMailboxId($id)
    {
        $this->addBinding('mailboxId', $id);

        return $this;
    }
}
