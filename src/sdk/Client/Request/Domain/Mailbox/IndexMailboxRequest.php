<?php
/**
 * Created by PhpStorm.
 * User: ycoutu
 * Date: 28/02/19
 * Time: 1:55 PM
 */

namespace ProvulusSDK\Client\Request\Domain\Mailbox;

use Carbon\Carbon;
use ProvulusSDK\Client\Args\Search\Domain\Mailbox\EmailAddressArgument;
use ProvulusSDK\Client\Args\Search\Domain\Mailbox\MaximumMessageCountArgument;
use ProvulusSDK\Client\Args\Search\Domain\Mailbox\MinimumMessageCountArgument;
use ProvulusSDK\Client\Args\Search\Domain\Mailbox\StartLastMessageDateArgument;
use ProvulusSDK\Client\Request\Collection\CollectionPaginatedTrait;
use ProvulusSDK\Client\Request\Collection\IProvulusCollectionPaginatedRequest;
use ProvulusSDK\Client\Request\RequestType;
use ProvulusSDK\Client\Request\Resource\Domain\Mailbox\BaseMailboxRequest;
use ProvulusSDK\Client\Response\Domain\Mailbox\MailboxCollectionResponse;
use ProvulusSDK\Client\Response\Domain\Mailbox\MailboxResponse;
use ProvulusSDK\Client\Response\IProvulusObjectResponse;

/**
 * Class IndexMailboxRequest
 *
 * @method MailboxCollectionResponse getResponse()
 *
 * @package ProvulusSDK\Client\Request\Domain\Mailbox
 */
class IndexMailboxRequest extends BaseMailboxRequest implements IProvulusCollectionPaginatedRequest
{
    use CollectionPaginatedTrait;
    /**
     * Type of request
     *
     * @return RequestType
     */
    public function httpType()
    {
        return RequestType::HTTP_GET();
    }

    /**
     * Used to parse each result of the collection
     *
     * Need to be overridden by the class
     *
     * @param array $objectData
     *
     * @return IProvulusObjectResponse
     */
    function toResponse(array $objectData)
    {
        return new MailboxResponse($objectData);
    }

    /**
     * Class to be used to build the collection
     *
     * @return string
     */
    function collectionClass()
    {
        return MailboxCollectionResponse::class;
    }

    /**
     * Set the local part of the email address to search with
     *
     * @param string|null $value
     * @return $this
     */
    public function setEmailAddress($value)
    {
        if (is_null($value)) {
            return $this->removeArgument(new EmailAddressArgument('mock'));
        }

        return $this->addArgument(new EmailAddressArgument($value));
    }

    /**
     * Set the minimum of messages the recipient has received
     *
     * @param int|null $value
     * @return $this
     */
    public function setMinimumMessageCount($value)
    {
        if (is_null($value)) {
            return $this->removeArgument(new MinimumMessageCountArgument(0));
        }

        return $this->addArgument(new MinimumMessageCountArgument($value));
    }

    /**
     * Set the maximum of messages the recipient has received
     *
     * @param int|null $value
     * @return $this
     */
    public function setMaximumMessageCount($value)
    {
        if (is_null($value)) {
            return $this->removeArgument(new MaximumMessageCountArgument(0));
        }

        return $this->addArgument(new MaximumMessageCountArgument($value));
    }

    /**
     * Set the start last message date to search for
     *
     * @param Carbon|null $lastMessageDate
     * @return IndexMailboxRequest
     */
    public function setStartLastMessageDate(Carbon $lastMessageDate)
    {
        if (is_null($lastMessageDate)) {
            return $this->removeArgument(new StartLastMessageDateArgument(Carbon::now()->toDateTimeString()));
        }

        return $this->addArgument(new StartLastMessageDateArgument($lastMessageDate->toDateTimeString()));
    }
}
