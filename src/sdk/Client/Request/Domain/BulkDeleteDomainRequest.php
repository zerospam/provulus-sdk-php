<?php
/**
 * Created by PhpStorm.
 * User: pbb
 * Date: 23/08/17
 * Time: 11:36 AM
 */

namespace ProvulusSDK\Client\Request\Domain;


use ProvulusSDK\Client\Request\BulkRequestTrait;
use ProvulusSDK\Client\Request\EmptyResponseTrait;
use ProvulusSDK\Client\Request\RequestType;
use ProvulusSDK\Client\Request\Resource\Domain\BaseDomainRequest;

/**
 * Class BulkDeleteDomainRequest
 *
 * Request for deleting multiple domains at once
 *
 * @package ProvulusSDK\Client\Request\Domain
 */
class BulkDeleteDomainRequest extends BaseDomainRequest
{
    use EmptyResponseTrait,
        BulkRequestTrait;

    /**
     * Type of request
     *
     * @return RequestType
     */
    public function httpType()
    {
        return RequestType::HTTP_DELETE();
    }
}