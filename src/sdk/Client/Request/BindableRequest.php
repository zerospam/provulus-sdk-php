<?php
/**
 * Created by PhpStorm.
 * User: aaflalo
 * Date: 14/12/16
 * Time: 4:47 PM
 */

namespace ProvulusSDK\Client\Request;


use ProvulusSDK\Utils\Str;

abstract class BindableRequest extends BaseProvulusRequest
{

    /**
     * @var array
     */
    private $routeBindings = [];

    protected function ignoredProperties()
    {
        $ignored   = parent::ignoredProperties();
        $ignored[] = 'routeBindings';

        return $ignored;
    }


    /**
     * Set a new binding
     *
     * @param      $key
     * @param      $value
     * @param bool $override
     */
    protected function addBinding($key, $value, $override = true)
    {
        //addBinding(orgId, 5)
        if (isset($this->routeBindings[$key]) && !$override) {
            throw new \InvalidArgumentException("You can't override the key: $key");
        }

        if (is_object($value) || is_array($value)) {
            throw new \InvalidArgumentException("You can't use an object or an array as binding");
        }

        $this->routeBindings[$key] = $value;
    }

    /**
     * Is the binding set
     *
     * @param $key
     *
     * @return bool
     */
    protected function hasBinding($key)
    {
        return isset($this->routeBindings[$key]);
    }

    public function routeUrl()
    {
        $bindingsPatterns = array_map(function ($item) {
            return ':' . $item;
        }, array_keys($this->routeBindings));

        $url = str_replace($bindingsPatterns, array_values($this->routeBindings), $this->baseRoute());
        if (Str::contains($url, ':')) {
            throw new \InvalidArgumentException('One or more bindings haven\'t been set.');
        }

        return $url;
    }


    /**
     * Base route without binding
     *
     * @return string
     */
    abstract function baseRoute();
}