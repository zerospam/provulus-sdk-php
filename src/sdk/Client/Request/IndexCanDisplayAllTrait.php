<?php
/**
 * Created by PhpStorm.
 * User: ycoutu
 * Date: 12/03/19
 * Time: 4:57 PM
 */

namespace ProvulusSDK\Client\Request;

use ProvulusSDK\Client\Args\Index\DisplayAllArg;

trait IndexCanDisplayAllTrait
{
    /**
     * @param $displayAll
     *
     * @return $this
     */
    public function setDisplayAll($displayAll)
    {
        return $this->addArgument(new DisplayAllArg($displayAll));
    }

    /**
     * @return $this
     */
    public function paginateResults()
    {
        return $this->setDisplayAll(false);
    }

    /**
     * @return $this
     */
    public function getAllResults()
    {
        return $this->setDisplayAll(true);
    }
}
