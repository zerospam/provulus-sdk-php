<?php
/**
 * Created by PhpStorm.
 * User: aaflalo
 * Date: 05/05/17
 * Time: 10:31 AM
 */

namespace ProvulusSDK\Client\Request;


interface HasNullableFields
{
    /**
     * Is the given field nullable
     *
     * @param $field
     *
     * @return bool
     */
    public function isNullable($field);

    /**
     * Check if the given field is nullable and if it should be included in the request
     *
     * @param $field
     *
     * @return bool
     *
     */
    public function isValueChanged($field);

}