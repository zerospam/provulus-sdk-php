<?php
/**
 * Created by PhpStorm.
 * User: aaflalo
 * Date: 15/02/17
 * Time: 1:40 PM
 */

namespace ProvulusSDK\Client\Request\Organization\Traits;

use ProvulusSDK\Client\Args\Relationship\IncludeRelationArg;

trait WithSupportOrganization
{

    /**
     * Include the support organization in the result
     *
     * @return $this
     */
    public function withSupportOrganization()
    {
        return $this->addArgument(new IncludeRelationArg('supportOrganization'));
    }

    /**
     * Remove the support organization from the results
     *
     * @return $this
     */
    public function withoutSupportOrganization()
    {
        return $this->removeArgument(new IncludeRelationArg('supportOrganization'));
    }
}