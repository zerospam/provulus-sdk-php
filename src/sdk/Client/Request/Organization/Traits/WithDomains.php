<?php
/**
 * Created by PhpStorm.
 * User: pbb
 * Date: 22/02/17
 * Time: 10:10 AM
 */

namespace ProvulusSDK\Client\Request\Organization\Traits;

use ProvulusSDK\Client\Args\Relationship\IncludeRelationArg;

/**
 * Class WithDomains
 *
 * Trait used to include related domains when fetching organization
 *
 * @package ProvulusSDK\Client\Request\Organization\Traits
 */
trait WithDomains
{

    /**
     * Include related domains
     *
     * @return $this
     */
    public function withDomains()
    {
        return $this->addArgument(new IncludeRelationArg('domains'));

    }

    /**
     * Remove the related domains
     *
     * @return $this
     */
    public function withoutDomains()
    {
        return $this->removeArgument(new IncludeRelationArg('domains'));
    }
}