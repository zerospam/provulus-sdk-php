<?php
/**
 * Created by PhpStorm.
 * User: ycoutu
 * Date: 06/02/18
 * Time: 11:21 AM
 */

namespace ProvulusSDK\Client\Request\Organization\Traits;

use ProvulusSDK\Client\Args\Relationship\IncludeRelationArg;

trait WithCancellationInformation
{
    /**
     * Include the cancellation information in the result
     *
     * @return $this
     */
    public function withCancellationInformation()
    {
        return $this->addArgument(new IncludeRelationArg('cancellationInformation'));
    }

    /**
     * Remove the support organization from the results
     *
     * @return $this
     */
    public function withoutCancellationInformation()
    {
        return $this->removeArgument(new IncludeRelationArg('cancellationInformation'));
    }
}
