<?php
/**
 * Created by PhpStorm.
 * User: ycoutu
 * Date: 26/09/18
 * Time: 10:38 AM
 */

namespace ProvulusSDK\Client\Request\Organization\Traits;

use ProvulusSDK\Client\Args\Relationship\IncludeRelationArg;

trait WithPostTrialAction
{
    /**
     * Include the support organization in the result
     *
     * @return $this
     */
    public function withPostTrialAction()
    {
        return $this->addArgument(new IncludeRelationArg('postTrialAction'));
    }

    /**
     * Remove the support organization from the results
     *
     * @return $this
     */
    public function withoutPostTrialAction()
    {
        return $this->removeArgument(new IncludeRelationArg('postTrialAction'));
    }
}
