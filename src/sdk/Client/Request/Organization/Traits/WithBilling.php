<?php
/**
 * Created by PhpStorm.
 * User: ycoutu
 * Date: 26/09/18
 * Time: 9:13 AM
 */

namespace ProvulusSDK\Client\Request\Organization\Traits;

use ProvulusSDK\Client\Args\Relationship\IncludeRelationArg;

trait WithBilling
{
    /**
     * Include the support organization in the result
     *
     * @return $this
     */
    public function withBilling()
    {
        return $this->addArgument(new IncludeRelationArg('billing'));
    }

    /**
     * Remove the support organization from the results
     *
     * @return $this
     */
    public function withoutBilling()
    {
        return $this->removeArgument(new IncludeRelationArg('billing'));
    }
}
