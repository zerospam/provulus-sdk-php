<?php
/**
 * Created by PhpStorm.
 * User: ycoutu
 * Date: 18/04/18
 * Time: 3:39 PM
 */

namespace ProvulusSDK\Client\Request\Organization\Traits;

use ProvulusSDK\Client\Args\Relationship\IncludeRelationArg;

trait WithPasswordConfiguration
{
    /**
     * Include related domains
     *
     * @return $this
     */
    public function withPasswordConfiguration()
    {
        return $this->addArgument(new IncludeRelationArg('passwordConfiguration'));

    }

    /**
     * Remove the related domains
     *
     * @return $this
     */
    public function withoutPasswordConfiguration()
    {
        return $this->removeArgument(new IncludeRelationArg('passwordConfiguration'));
    }
}
