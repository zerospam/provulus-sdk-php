<?php


namespace ProvulusSDK\Client\Request\Organization\Traits;

use ProvulusSDK\Client\Args\Relationship\IncludeRelationArg;

trait WithDistributorOrganization
{
    /**
     * Include the support organization in the result
     *
     * @return $this
     */
    public function withDistributorOrganization()
    {
        return $this->addArgument(new IncludeRelationArg('distributorOrganization'));
    }

    /**
     * Remove the support organization from the results
     *
     * @return $this
     */
    public function withoutDistributorOrganization()
    {
        return $this->removeArgument(new IncludeRelationArg('distributorOrganization'));
    }
}
