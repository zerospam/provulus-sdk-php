<?php
/**
 * Created by PhpStorm.
 * User: pbb
 * Date: 21/02/17
 * Time: 2:53 PM
 */

namespace ProvulusSDK\Client\Request\Organization\Traits;

use ProvulusSDK\Client\Args\Search\Organization\ActiveArgument;
use ProvulusSDK\Client\Args\Search\Organization\BilledByArgument;
use ProvulusSDK\Client\Args\Search\Organization\CorporateNameArgument;
use ProvulusSDK\Client\Args\Search\Organization\DistributedByArgument;
use ProvulusSDK\Client\Args\Search\Organization\DomainNameArgument;
use ProvulusSDK\Client\Args\Search\Organization\SupportedByArgument;
use ProvulusSDK\Enum\SearchCriteria\ActiveCriteriaEnum;

/**
 * Class SearchArguments
 *
 * Trait for adding parameters to Organization Request
 *
 * @package ProvulusSDK\Client\Request\Organization\Traits
 */
trait OrganizationSearchArguments
{
    /**
     * Request will filter organizations with specified partial corporate name
     * Pass null parameter to remove argument
     * i.e zero will find Zerospam
     *
     * Name will be checked against normalized corporate name
     * i.e. special chars and case won't be taken into account
     *
     * @param string|null $corporateName
     *
     * @return $this
     */
    public function setCorporateName($corporateName)
    {
        if (is_null($corporateName)) {
            return $this->removeArgument(new CorporateNameArgument('mocked organization name'));
        }

        return $this->addArgument(new CorporateNameArgument($corporateName));
    }

    /**
     * Request will filter organizations owning specified partial domain name
     * Pass null parameter to remove argument
     * i.e. zozo will find zozo.com
     *
     * Name will be checked against normalized domain name
     * i.e. special chars and case won't be taken into account
     *
     * @param string|null $domainName
     *
     * @return $this
     */
    public function setDomainName($domainName)
    {
        if (is_null($domainName)) {
            return $this->removeArgument(new DomainNameArgument('mocked domain name'));
        }

        return $this->addArgument(new DomainNameArgument($domainName));
    }

    /**
     * Request will filter down results based on if organizations are active or not, or both
     * Pass null parameter to remove argument
     *
     * @param ActiveCriteriaEnum|null $enum
     *
     * @return $this
     */
    public function setActive($enum)
    {
        if (is_null($enum)) {
            return $this->removeArgument(new ActiveArgument(ActiveCriteriaEnum::ALL()));
        }

        return $this->addArgument(new ActiveArgument($enum));
    }


    /**
     * Request will filter down results based on support organization
     * Pass null parameter to remove argument
     *
     * @param int|null $supportOrganizationId
     *
     * @return $this
     */
    public function setSupportedBy($supportOrganizationId)
    {
        if (is_null($supportOrganizationId)) {
            return $this->removeArgument(new SupportedByArgument(1));
        }

        return $this->addArgument(new SupportedByArgument($supportOrganizationId));
    }

    /**
     * Request will filter down results based on distributor organization
     * Pass null parameter to remove argument
     *
     * @param int|null $distributorId
     *
     * @return $this
     */
    public function setDistributedBy($distributorId)
    {
        if (is_null($distributorId)) {
            return $this->removeArgument(new DistributedByArgument(1));
        }

        return $this->addArgument(new DistributedByArgument($distributorId));
    }

    /**
     * Request will filter down results based on billing organization
     * Pass null parameter to remove argument
     *
     * @param int|null $billingOrganizationId
     *
     * @return $this
     */
    public function setBilledBy($billingOrganizationId)
    {
        if (is_null($billingOrganizationId)) {
            return $this->removeArgument(new BilledByArgument(1));
        }

        return $this->addArgument(new BilledByArgument($billingOrganizationId));
    }
}