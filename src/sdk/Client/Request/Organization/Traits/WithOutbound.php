<?php
/**
 * Created by PhpStorm.
 * User: pbb
 * Date: 22/02/17
 * Time: 10:10 AM
 */

namespace ProvulusSDK\Client\Request\Organization\Traits;

use ProvulusSDK\Client\Args\Relationship\IncludeRelationArg;

/**
 * Class WithDomains
 *
 * Trait used to include related domains when fetching organization
 *
 * @package ProvulusSDK\Client\Request\Organization\Traits
 */
trait WithOutbound
{

    /**
     * Include related domains
     *
     * @return $this
     */
    public function withOutbound()
    {
        return $this->addArgument(new IncludeRelationArg('outbound'));

    }

    /**
     * Remove the related domains
     *
     * @return $this
     */
    public function withoutOutbound()
    {
        return $this->removeArgument(new IncludeRelationArg('outbound'));
    }
}