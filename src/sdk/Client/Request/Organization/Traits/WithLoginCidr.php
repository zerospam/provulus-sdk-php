<?php
/**
 * Created by PhpStorm.
 * User: ycoutu
 * Date: 18/04/18
 * Time: 3:39 PM
 */

namespace ProvulusSDK\Client\Request\Organization\Traits;

use ProvulusSDK\Client\Args\Relationship\IncludeRelationArg;

trait WithLoginCidr
{
    /**
     * Include related domains
     *
     * @return $this
     */
    public function withLoginCidr()
    {
        return $this->addArgument(new IncludeRelationArg('loginCidrs'));

    }

    /**
     * Remove the related domains
     *
     * @return $this
     */
    public function withoutLoginCidr()
    {
        return $this->removeArgument(new IncludeRelationArg('loginCidrs'));
    }
}
