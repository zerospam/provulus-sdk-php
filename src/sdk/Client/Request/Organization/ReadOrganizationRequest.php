<?php
/**
 * Created by PhpStorm.
 * User: pbb
 * Date: 10/02/17
 * Time: 2:00 PM
 */

namespace ProvulusSDK\Client\Request\Organization;

use ProvulusSDK\Client\Request\Organization\Traits\WithBilling;
use ProvulusSDK\Client\Request\Organization\Traits\WithCancellationInformation;
use ProvulusSDK\Client\Request\Organization\Traits\WithDistributorOrganization;
use ProvulusSDK\Client\Request\Organization\Traits\WithDomains;
use ProvulusSDK\Client\Request\Organization\Traits\WithLoginCidr;
use ProvulusSDK\Client\Request\Organization\Traits\WithOutbound;
use ProvulusSDK\Client\Request\Organization\Traits\WithPasswordConfiguration;
use ProvulusSDK\Client\Request\Organization\Traits\WithPostTrialAction;
use ProvulusSDK\Client\Request\Organization\Traits\WithSupportOrganization;
use ProvulusSDK\Client\Request\RequestType;
use ProvulusSDK\Client\Request\Resource\Organization\OrganizationProcessResponseTrait;
use ProvulusSDK\Client\Request\Resource\Organization\OrganizationRequest;
use ProvulusSDK\Client\Response\Organization\OrganizationResponse;

/**
 * Class ReadOrganizationRequest
 *
 * Gets an organization
 * @method OrganizationResponse getResponse()
 *
 * @package ProvulusSDK\Client\Request\Organization
 */
class ReadOrganizationRequest extends OrganizationRequest
{
    use OrganizationProcessResponseTrait,
        WithSupportOrganization,
        WithCancellationInformation,
        WithLoginCidr,
        WithPasswordConfiguration,
        WithBilling,
        WithPostTrialAction,
        WithDomains,
        WithOutbound,
        WithDistributorOrganization;

    /**
     * Type of request
     *
     * @return RequestType
     */
    public function httpType()
    {
        return RequestType::HTTP_GET();
    }
}
