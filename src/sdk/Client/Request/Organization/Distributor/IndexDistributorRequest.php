<?php
/**
 * Created by PhpStorm.
 * User: aaflalo
 * Date: 17-08-09
 * Time: 16:03
 */

namespace ProvulusSDK\Client\Request\Organization\Distributor;


use ProvulusSDK\Client\Request\Organization\IndexOrganizationRequest;

class IndexDistributorRequest extends IndexOrganizationRequest
{
    /**
     * The url of the route
     *
     * @return string
     */
    public function routeUrl()
    {
        return 'distributors';
    }
}