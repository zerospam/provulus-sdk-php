<?php
/**
 * Created by PhpStorm.
 * User: ycoutu
 * Date: 18/04/18
 * Time: 10:06 AM
 */

namespace ProvulusSDK\Client\Request\Organization\Cidr;

use ProvulusSDK\Client\Request\RequestType;
use ProvulusSDK\Client\Request\Resource\Organization\Cidr\LoginCidrProcessResponseTrait;
use ProvulusSDK\Client\Request\Resource\Organization\Cidr\LoginCidrRequest;

class ReadLoginCidrRequest extends LoginCidrRequest
{
    use LoginCidrProcessResponseTrait;

    /**
     * Type of request
     *
     * @return RequestType
     */
    public function httpType()
    {
        return RequestType::HTTP_GET();
    }
}
