<?php
/**
 * Created by PhpStorm.
 * User: ycoutu
 * Date: 18/04/18
 * Time: 10:05 AM
 */

namespace ProvulusSDK\Client\Request\Organization\Cidr;

use ProvulusSDK\Client\Request\RequestType;
use ProvulusSDK\Client\Request\Resource\Organization\Cidr\LoginCidrProcessResponseTrait;
use ProvulusSDK\Client\Request\Resource\Organization\Cidr\LoginCidrRequest;
use ProvulusSDK\Client\Request\Resource\Organization\Cidr\LoginCidrSetAttributes;

class UpdateLoginCidrRequest extends LoginCidrRequest
{
    use LoginCidrProcessResponseTrait, LoginCidrSetAttributes;

    /**
     * Type of request
     *
     * @return RequestType
     */
    public function httpType()
    {
        return RequestType::HTTP_PUT();
    }
}
