<?php
/**
 * Created by PhpStorm.
 * User: ycoutu
 * Date: 18/04/18
 * Time: 10:08 AM
 */

namespace ProvulusSDK\Client\Request\Organization\Cidr;

use ProvulusSDK\Client\Request\Collection\CollectionPaginatedTrait;
use ProvulusSDK\Client\Request\Collection\IProvulusCollectionRequest;
use ProvulusSDK\Client\Request\RequestType;
use ProvulusSDK\Client\Request\Resource\Organization\Cidr\BaseLoginCidrRequest;
use ProvulusSDK\Client\Response\IProvulusObjectResponse;
use ProvulusSDK\Client\Response\Organization\Cidr\LoginCidrCollectionResponse;
use ProvulusSDK\Client\Response\Organization\Cidr\LoginCidrResponse;

/**
 * Class IndexLoginCidrRequest
 *
 * @package ProvulusSDK\Client\Request\Organization\Cidr
 *
 * @method LoginCidrCollectionResponse getResponse()
 */
class IndexLoginCidrRequest extends BaseLoginCidrRequest implements IProvulusCollectionRequest
{
    use CollectionPaginatedTrait;
    /**
     * Type of request
     *
     * @return RequestType
     */
    public function httpType()
    {
        return RequestType::HTTP_GET();
    }

    /**
     * Used to parse each result of the collection
     *
     * Need to be overridden by the class
     *
     * @param array $objectData
     *
     * @param array $includedData
     *
     * @return IProvulusObjectResponse
     */
    function toResponse(array $objectData)
    {
        return new LoginCidrResponse($objectData);
    }

    /**
     * Class to be used to build the collection
     *
     * @return string
     */
    function collectionClass()
    {
        return LoginCidrCollectionResponse::class;
    }
}
