<?php
/**
 * Created by PhpStorm.
 * User: ycoutu
 * Date: 18/04/18
 * Time: 10:07 AM
 */

namespace ProvulusSDK\Client\Request\Organization\Cidr\Bulk;

use ProvulusSDK\Client\Request\BulkRequestTrait;
use ProvulusSDK\Client\Request\EmptyResponseTrait;
use ProvulusSDK\Client\Request\RequestType;
use ProvulusSDK\Client\Request\Resource\Organization\Cidr\BaseLoginCidrRequest;

class BulkDeleteLoginCidrRequest extends BaseLoginCidrRequest
{
    use EmptyResponseTrait,
        BulkRequestTrait;

    /**
     * Type of request
     *
     * @return RequestType
     */
    public function httpType()
    {
        return RequestType::HTTP_DELETE();
    }
}
