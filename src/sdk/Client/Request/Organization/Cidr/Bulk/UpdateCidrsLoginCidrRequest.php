<?php
/**
 * Created by PhpStorm.
 * User: ycoutu
 * Date: 18/04/18
 * Time: 10:08 AM
 */

namespace ProvulusSDK\Client\Request\Organization\Cidr\Bulk;

use ProvulusSDK\Client\Request\Collection\CollectionOrderTrait;
use ProvulusSDK\Client\Request\Collection\IProvulusCollectionRequest;
use ProvulusSDK\Client\Request\RequestType;
use ProvulusSDK\Client\Request\Resource\Organization\Cidr\BaseLoginCidrRequest;
use ProvulusSDK\Client\Response\Organization\Cidr\LoginCidrCollectionResponse;
use ProvulusSDK\Client\Response\Organization\Cidr\LoginCidrResponse;

/**
 * Class UpdateCidrsLoginCidrRequest
 *
 * @package ProvulusSDK\Client\Request\Organization\Cidr\Bulk
 *
 * @method LoginCidrCollectionResponse getResponse()
 */
class UpdateCidrsLoginCidrRequest extends BaseLoginCidrRequest implements IProvulusCollectionRequest
{
    use CollectionOrderTrait;

    /** @var string[] */
    private $cidrs;

    /**
     * @param string[] $cidrs
     *
     * @return $this
     */
    public function setCidrs($cidrs)
    {
        $this->cidrs = $cidrs;

        return $this;
    }

    /**
     * Type of request
     *
     * @return RequestType
     */
    public function httpType()
    {
        return RequestType::HTTP_POST();
    }


    /**
     * Used to parse each result of the collection
     *
     * Need to be overridden by the class
     *
     * @param array $objectData
     *
     * @return LoginCidrResponse
     */
    function toResponse(array $objectData)
    {
        return new LoginCidrResponse($objectData);
    }

    /**
     * Class to be used to build the collection
     *
     * @return string
     */
    function collectionClass()
    {
        return LoginCidrCollectionResponse::class;
    }

    /**
     * Base route for orgs resource
     *
     * @return string
     * @throws \Exception
     */
    function baseRoute()
    {
        return parent::baseRoute() . '/update-cidrs';
    }
}
