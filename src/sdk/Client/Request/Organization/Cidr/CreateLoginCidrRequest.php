<?php
/**
 * Created by PhpStorm.
 * User: ycoutu
 * Date: 18/04/18
 * Time: 10:05 AM
 */

namespace ProvulusSDK\Client\Request\Organization\Cidr;

use ProvulusSDK\Client\Request\RequestType;
use ProvulusSDK\Client\Request\Resource\Organization\Cidr\BaseLoginCidrRequest;
use ProvulusSDK\Client\Request\Resource\Organization\Cidr\LoginCidrProcessResponseTrait;
use ProvulusSDK\Client\Request\Resource\Organization\Cidr\LoginCidrSetAttributes;

class CreateLoginCidrRequest extends BaseLoginCidrRequest
{
    use LoginCidrProcessResponseTrait, LoginCidrSetAttributes;

    /**
     * Type of request
     *
     * @return RequestType
     */
    public function httpType()
    {
        return RequestType::HTTP_POST();
    }
}
