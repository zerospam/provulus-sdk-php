<?php
/**
 * Created by PhpStorm.
 * User: pbb
 * Date: 18/09/18
 * Time: 11:36 AM
 */

namespace ProvulusSDK\Client\Request\Organization\Contact;


use ProvulusSDK\Client\Request\RequestType;
use ProvulusSDK\Client\Request\Resource\Organization\Contact\ContactPersonProcessResponseTrait;
use ProvulusSDK\Client\Request\Resource\Organization\Contact\ContactPersonRequest;

/**
 * Class SetDefaultContactPersonRequest
 *
 * @package ProvulusSDK\Client\Request\Organization\Contact
 */
class SetDefaultContactPersonRequest extends ContactPersonRequest
{
    use ContactPersonProcessResponseTrait;

    /**
     * Type of request
     *
     * @return RequestType
     */
    public function httpType()
    {
        return RequestType::HTTP_POST();
    }

    /**
     * Base route for set default request
     *
     * @return string
     * @throws \Exception
     */
    function baseRoute()
    {
        return parent::baseRoute() . '/default';
    }
}