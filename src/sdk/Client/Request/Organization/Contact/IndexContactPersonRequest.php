<?php
/**
 * Created by PhpStorm.
 * User: pbb
 * Date: 16/12/16
 * Time: 11:15 AM
 */

namespace ProvulusSDK\Client\Request\Organization\Contact;


use ProvulusSDK\Client\Request\Collection\CollectionPaginatedTrait;
use ProvulusSDK\Client\Request\Collection\IProvulusCollectionPaginatedRequest;
use ProvulusSDK\Client\Request\RequestType;
use ProvulusSDK\Client\Request\Resource\Organization\Contact\BaseContactPersonRequest;
use ProvulusSDK\Client\Response\IProvulusObjectResponse;
use ProvulusSDK\Client\Response\Organization\Contact\ContactPersonCollectionResponse;
use ProvulusSDK\Client\Response\Organization\Contact\ContactPersonResponse;


/**
 * Class IndexContactPersonRequest
 *
 *
 * Request for reading all Contact Persons of the Organization
 *
 * @method ContactPersonCollectionResponse getResponse()
 *
 * @package ProvulusSDK\Client\Request\Organization\Contact\Person\Base
 */
class IndexContactPersonRequest extends BaseContactPersonRequest implements IProvulusCollectionPaginatedRequest
{
    use CollectionPaginatedTrait;

    /**
     * Type of request
     *
     * @return RequestType
     */
    public function httpType()
    {
        return RequestType::HTTP_GET();
    }

    /**
     * Used to parse each result of the collection
     *
     * Need to be overridden by the class
     *
     * @param array $array
     *
     * @return IProvulusObjectResponse
     */
    function toResponse(array $array)
    {
        return new ContactPersonResponse($array);
    }

    /**
     * Class to be used to build the collection
     *
     * @return string
     */
    function collectionClass()
    {
        return ContactPersonCollectionResponse::class;
    }
}