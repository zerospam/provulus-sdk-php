<?php
/**
 * Created by PhpStorm.
 * User: ycoutu
 * Date: 13/06/17
 * Time: 9:01 AM
 */

namespace ProvulusSDK\Client\Request\Organization\Contact\Bulk;

use ProvulusSDK\Client\Request\BulkRequestTrait;
use ProvulusSDK\Client\Request\EmptyResponseTrait;
use ProvulusSDK\Client\Request\RequestType;
use ProvulusSDK\Client\Request\Resource\Organization\Contact\BaseContactPersonRequest;

class BulkDeleteContactPersonRequest extends BaseContactPersonRequest
{
    use BulkRequestTrait, EmptyResponseTrait;
    /**
     * Type of request
     *
     * @return RequestType
     */
    public function httpType()
    {
        return RequestType::HTTP_DELETE();
    }
}
