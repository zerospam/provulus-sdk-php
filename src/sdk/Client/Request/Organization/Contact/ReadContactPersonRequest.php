<?php
/**
 * Created by PhpStorm.
 * User: pbb
 * Date: 16/12/16
 * Time: 11:13 AM
 */

namespace ProvulusSDK\Client\Request\Organization\Contact;


use ProvulusSDK\Client\Request\RequestType;
use ProvulusSDK\Client\Request\Resource\Organization\Contact\ContactPersonProcessResponseTrait;
use ProvulusSDK\Client\Request\Resource\Organization\Contact\ContactPersonRequest;

/**
 * Class ReadContactPersonRequest
 *
 * Read Request for a single Instance of Contact Person
 *
 * @package ProvulusSDK\Client\Request\Organization\Contact\Person
 */
class ReadContactPersonRequest extends ContactPersonRequest
{
    use ContactPersonProcessResponseTrait;

    /**
     * Type of request
     *
     * @return RequestType
     */
    public function httpType()
    {
        return RequestType::HTTP_GET();
    }
}