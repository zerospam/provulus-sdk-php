<?php
/**
 * Created by PhpStorm.
 * User: pbb
 * Date: 14/12/16
 * Time: 3:43 PM
 */

namespace ProvulusSDK\Client\Request\Organization\Contact;

use ProvulusSDK\Client\Request\RequestType;
use ProvulusSDK\Client\Request\Resource\Organization\Contact\BaseContactPersonRequest;
use ProvulusSDK\Client\Request\Resource\Organization\Contact\ContactPersonProcessResponseTrait;
use ProvulusSDK\Client\Request\Resource\Organization\Contact\ContactPersonSetAttributes;

/**
 * Class CreateContactPersonRequest
 *
 * This request is used to create a new Contact Person for an Organization
 *
 * @package ProvulusSDK\Client\Request\Organization\Contact\Person
 */
class CreateContactPersonRequest extends BaseContactPersonRequest
{
    use ContactPersonSetAttributes,
        ContactPersonProcessResponseTrait;

    /**
     * Type of request
     *
     * @return RequestType
     */
    public function httpType()
    {
        return RequestType::HTTP_POST();
    }
}