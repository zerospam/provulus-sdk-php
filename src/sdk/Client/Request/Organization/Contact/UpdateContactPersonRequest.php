<?php
/**
 * Created by PhpStorm.
 * User: pbb
 * Date: 15/12/16
 * Time: 3:54 PM
 */

namespace ProvulusSDK\Client\Request\Organization\Contact;

use ProvulusSDK\Client\Request\RequestType;
use ProvulusSDK\Client\Request\Resource\Organization\Contact\ContactPersonProcessResponseTrait;
use ProvulusSDK\Client\Request\Resource\Organization\Contact\ContactPersonRequest;
use ProvulusSDK\Client\Request\Resource\Organization\Contact\ContactPersonSetAttributes;

/**
 * Class UpdateContactPersonRequest
 * Request for updating the Contact Person
 *
 * @package ProvulusSDK\Client\Request\Organization\Contact\Person
 */
class UpdateContactPersonRequest extends ContactPersonRequest
{
    use ContactPersonSetAttributes,
        ContactPersonProcessResponseTrait;

    /**
     * Method for request
     *
     * @return RequestType
     */
    public function httpType()
    {
        return RequestType::HTTP_PUT();
    }
}