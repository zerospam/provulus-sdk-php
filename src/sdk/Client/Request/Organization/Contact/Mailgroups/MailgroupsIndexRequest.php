<?php
/**
 * Created by PhpStorm.
 * User: pbb
 * Date: 12/09/18
 * Time: 9:27 AM
 */

namespace ProvulusSDK\Client\Request\Organization\Contact\Mailgroups;

use ProvulusSDK\Client\Request\Collection\CollectionOrderTrait;
use ProvulusSDK\Client\Request\Collection\IProvulusCollectionRequest;
use ProvulusSDK\Client\Request\RequestType;
use ProvulusSDK\Client\Request\Resource\Organization\OrganizationRequest;
use ProvulusSDK\Client\Response\IProvulusObjectResponse;
use ProvulusSDK\Client\Response\Organization\Contact\Mailgroups\MailGroupResponse;
use ProvulusSDK\Client\Response\Organization\Contact\Mailgroups\MailGroupsCollectionResponse;

/**
 * Class MailgroupsReadRequest
 *
 * @method MailGroupsCollectionResponse getResponse()
 *
 * @package ProvulusSDK\Client\Request\Organization\Contact
 */
class MailgroupsIndexRequest extends OrganizationRequest implements IProvulusCollectionRequest
{
    use CollectionOrderTrait;

    /**
     * The url of the route
     *
     * @return string
     * @throws \Exception
     */
    public function baseRoute()
    {
        return parent::baseRoute() . '/mailgroups';
    }

    /**
     * Type of request
     *
     * @return RequestType
     */
    public function httpType()
    {
        return RequestType::HTTP_GET();
    }

    /**
     * Used to parse each result of the collection
     *
     * Need to be overridden by the class
     *
     * @param array $objectData
     *
     * @param array $includedData
     *
     * @return IProvulusObjectResponse
     */
    function toResponse(array $objectData, array $includedData = [])
    {
        return new MailGroupResponse($objectData, $includedData);
    }

    /**
     * Class to be used to build the collection
     *
     * @return string
     */
    function collectionClass()
    {
        return MailGroupsCollectionResponse::class;
    }
}
