<?php
/**
 * Created by PhpStorm.
 * User: pbb
 * Date: 13/09/18
 * Time: 10:23 AM
 */

namespace ProvulusSDK\Client\Request\Organization\Contact\Mailgroups;

use ProvulusSDK\Client\Request\BaseProvulusRequest;
use ProvulusSDK\Client\Request\Collection\CollectionOrderTrait;
use ProvulusSDK\Client\Request\Collection\IProvulusCollectionRequest;
use ProvulusSDK\Client\Request\RequestType;
use ProvulusSDK\Client\Response\IProvulusObjectResponse;
use ProvulusSDK\Client\Response\Organization\Contact\Mailgroups\MailGroupCategoriesCollectionResponse;
use ProvulusSDK\Client\Response\Organization\Contact\Mailgroups\MailGroupCategoryResponse;

/**
 * Class MailgroupsCategoriesIndexRequest
 *
 * @method MailGroupCategoriesCollectionResponse getResponse()
 *
 * @package ProvulusSDK\Client\Request\Organization\Contact\Mailgroups
 */
class MailgroupsCategoriesIndexRequest extends BaseProvulusRequest implements IProvulusCollectionRequest
{
    use CollectionOrderTrait;

    /**
     * The url of the route
     *
     * @return string
     */
    public function routeUrl()
    {
        return 'mailgroupcategories';
    }

    /**
     * Type of request
     *
     * @return RequestType
     */
    public function httpType()
    {
        return RequestType::HTTP_GET();
    }

    /**
     * Used to parse each result of the collection
     *
     * Need to be overridden by the class
     *
     * @param array $objectData
     *
     * @param array $includedData
     *
     * @return IProvulusObjectResponse
     */
    function toResponse(array $objectData, array $includedData = [])
    {
        return new MailGroupCategoryResponse($objectData);
    }

    /**
     * Class to be used to build the collection
     *
     * @return string
     */
    function collectionClass()
    {
        return MailGroupCategoriesCollectionResponse::class;
    }
}
