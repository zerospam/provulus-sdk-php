<?php
/**
 * Created by PhpStorm.
 * User: pbb
 * Date: 15/12/16
 * Time: 4:51 PM
 */

namespace ProvulusSDK\Client\Request\Organization\Contact;


use ProvulusSDK\Client\Request\EmptyResponseTrait;
use ProvulusSDK\Client\Request\RequestType;
use ProvulusSDK\Client\Request\Resource\Organization\Contact\ContactPersonRequest;

/**
 * Class DeleteContactPersonRequest
 *
 * Request for deleting a contact person from an organization
 *
 * @package ProvulusSDK\Client\Request\Organization\Contact\Person
 */
class DeleteContactPersonRequest extends ContactPersonRequest
{
    use EmptyResponseTrait;

    /**
     * Type of request
     *
     * @return RequestType
     */
    public function httpType()
    {
        return RequestType::HTTP_DELETE();
    }
}