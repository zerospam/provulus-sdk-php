<?php
/**
 * Created by PhpStorm.
 * User: ycoutu
 * Date: 21/06/17
 * Time: 9:50 AM
 */

namespace ProvulusSDK\Client\Request\Organization\Contact\ContactUs;

use ProvulusSDK\Client\Request\Resource\Organization\Contact\BaseContactUsRequest;

class CreateContactUsSupportRequest extends BaseContactUsRequest
{
    function baseRoute()
    {
        return parent::baseRoute() . '/support';
    }
}
