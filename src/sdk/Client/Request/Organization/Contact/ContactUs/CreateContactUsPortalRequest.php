<?php
/**
 * Created by PhpStorm.
 * User: ycoutu
 * Date: 21/06/17
 * Time: 10:27 AM
 */

namespace ProvulusSDK\Client\Request\Organization\Contact\ContactUs;

use ProvulusSDK\Client\Request\Resource\Organization\Contact\BaseContactUsRequest;

class CreateContactUsPortalRequest extends BaseContactUsRequest
{
    /** @var string */
    private $firstName;

    /** @var string */
    private $lastName;

    /** @var string */
    private $companyName;

    function baseRoute()
    {
        return parent::baseRoute() . '/portal';
    }

    /**
     * @param string $firstName
     *
     * @return CreateContactUsPortalRequest
     */
    public function setFirstName($firstName)
    {
        $this->firstName = $firstName;

        return $this;
    }

    /**
     * @param string $lastName
     *
     * @return CreateContactUsPortalRequest
     */
    public function setLastName($lastName)
    {
        $this->lastName = $lastName;

        return $this;
    }

    /**
     * @param string $companyName
     *
     * @return CreateContactUsPortalRequest
     */
    public function setCompanyName($companyName)
    {
        $this->companyName = $companyName;

        return $this;
    }
}
