<?php
/**
 * Created by PhpStorm.
 * User: ycoutu
 * Date: 03/05/17
 * Time: 11:59 AM
 */

namespace ProvulusSDK\Client\Request\Organization;

use Carbon\Carbon;
use ProvulusSDK\Client\Request\RequestType;
use ProvulusSDK\Client\Request\Resource\Organization\OrganizationProcessResponseTrait;
use ProvulusSDK\Client\Request\Resource\Organization\OrganizationRequest;
use ProvulusSDK\Enum\Billing\BillingPeriodEnum;
use ProvulusSDK\Enum\Locale\LocaleEnum;
use ProvulusSDK\Enum\Login\TwoFactorAuthUsersEnum;
use ProvulusSDK\Enum\PaymentCurrency\PaymentCurrencyEnum;
use ProvulusSDK\Enum\PersonOfInterest\PersonOfInterestMatchRuleEnum;

class UpdateOrganizationRequest extends OrganizationRequest
{
    use OrganizationProcessResponseTrait;

    /**
     * @var string
     */
    private $corporateName,
        $salesforceId,
        $noteShared,
        $portalPrefix,
        $noteForZsOnly,
        $renewalNote;

    /**
     * @var bool
     */
    private $dsblOrgSelfprov, $dsblPortalSelfprov, $isAccessAllowed, $isBillingOrganization, $isSupportOrganization,
        $isDistributorOrganization, $showsPoweredByLogo, $isBillable, $hasTrial, $isNotifiedEndTrial, $canGrantTrial,
        $isClientsNotifiedEndTrial, $isTwoFactorAuthMandatory, $renewalMessagesAllowed, $isSchoolOrganization;

    /**
     * @var \DateTimeZone;
     */
    private $timezoneCode;

    /**
     * @var Carbon
     */
    private $billingBaseDate, $yearlyRenewalDate, $trialEndDate;

    /**
     * @var int
     */
    private $supportOrg, $distributorOrg, $quarExpiryInDays, $lockdownValue;

    /**
     * @var PaymentCurrencyEnum
     */
    private $currency;

    /**
     * @var BillingPeriodEnum
     */
    private $billingPeriod;

    /**
     * @var LocaleEnum
     */
    private $defaultLanguageCode;

    /** @var TwoFactorAuthUsersEnum */
    private $twoFactorAuthUsers;

    /** @var  PersonOfInterestMatchRuleEnum */
    private $personOfInterestMatchRule;

    /**
     * @param string $corporateName
     *
     * @return $this
     */
    public function setCorporateName($corporateName)
    {
        $this->corporateName = $corporateName;

        return $this;
    }

    /**
     * @param PersonOfInterestMatchRuleEnum $personOfInterestMatchRule
     *
     * @return UpdateOrganizationRequest
     */
    public function setPersonOfInterestMatchRule($personOfInterestMatchRule)
    {
        $this->personOfInterestMatchRule = $personOfInterestMatchRule;

        return $this;
    }

    /**
     * @param string $noteShared
     *
     * @return $this
     */
    public function setNoteShared($noteShared)
    {
        $this->noteShared = $noteShared;
        $this->nullableChanged();

        return $this;
    }

    /**
     * @param string $portalPrefix
     *
     * @return $this
     */
    public function setPortalPrefix($portalPrefix)
    {
        $this->portalPrefix = $portalPrefix;
        $this->nullableChanged();

        return $this;
    }

    /**
     * @param string $salesforceId
     *
     * @return $this
     */
    public function setSalesforceId($salesforceId)
    {
        $this->salesforceId = $salesforceId;
        $this->nullableChanged();

        return $this;
    }

    /**
     * @param bool $dsblOrgSelfprov
     *
     * @return $this
     */
    public function setDsblOrgSelfprov($dsblOrgSelfprov)
    {
        $this->dsblOrgSelfprov = $dsblOrgSelfprov;

        return $this;
    }

    /**
     * @param bool $dsblPortalSelfprov
     *
     * @return $this
     */
    public function setDsblPortalSelfprov($dsblPortalSelfprov)
    {
        $this->dsblPortalSelfprov = $dsblPortalSelfprov;

        return $this;
    }

    /**
     * @param bool $isAccessAllowed
     *
     * @return $this
     */
    public function setIsAccessAllowed($isAccessAllowed)
    {
        $this->isAccessAllowed = $isAccessAllowed;

        return $this;
    }

    /**
     * @param bool $isBillingOrganization
     *
     * @return $this
     */
    public function setIsBillingOrganization($isBillingOrganization)
    {
        $this->isBillingOrganization = $isBillingOrganization;

        return $this;
    }

    /**
     * @param bool $isSupportOrganization
     *
     * @return $this
     */
    public function setIsSupportOrganization($isSupportOrganization)
    {
        $this->isSupportOrganization = $isSupportOrganization;

        return $this;
    }

    /**
     * @param bool $isDistributorOrganization
     *
     * @return $this
     */
    public function setIsDistributorOrganization($isDistributorOrganization)
    {
        $this->isDistributorOrganization = $isDistributorOrganization;

        return $this;
    }

    /**
     * @param bool $showsPoweredByLogo
     *
     * @return $this
     */
    public function setShowsPoweredByLogo($showsPoweredByLogo)
    {
        $this->showsPoweredByLogo = $showsPoweredByLogo;

        return $this;
    }

    /**
     * @param \DateTimeZone $timezoneCode
     *
     * @return $this
     */
    public function setTimezoneCode($timezoneCode)
    {
        $this->timezoneCode = $timezoneCode;

        return $this;
    }

    /**
     * @param Carbon $billingBaseDate
     *
     * @return $this
     */
    public function setBillingBaseDate($billingBaseDate)
    {
        $this->billingBaseDate = $billingBaseDate;
        $this->nullableChanged();

        return $this;
    }

    /**
     * @param int $supportOrg
     *
     * @return $this
     */
    public function setSupportOrg($supportOrg)
    {
        $this->supportOrg = $supportOrg;

        return $this;
    }

    /**
     * @param int $distributorOrg
     *
     * @return $this
     */
    public function setDistributorOrg($distributorOrg)
    {
        $this->distributorOrg = $distributorOrg;

        return $this;
    }

    /**
     * @param int $quarExpiryInDays
     *
     * @return $this
     */
    public function setQuarExpiryInDays($quarExpiryInDays)
    {
        $this->quarExpiryInDays = $quarExpiryInDays;

        return $this;
    }

    /**
     * @param int $lockdownValue
     *
     * @return $this
     */
    public function setLockdownValue($lockdownValue)
    {
        $this->lockdownValue = $lockdownValue;

        return $this;
    }

    /**
     * @param PaymentCurrencyEnum $currency
     *
     * @return $this
     */
    public function setCurrency($currency)
    {
        $this->currency = $currency;

        return $this;
    }

    /**
     * @param BillingPeriodEnum $billingPeriod
     *
     * @return $this
     */
    public function setBillingPeriod($billingPeriod)
    {
        $this->billingPeriod = $billingPeriod;
        $this->nullableChanged();

        return $this;
    }

    /**
     * @param LocaleEnum $defaultLanguageCode
     *
     * @return $this
     */
    public function setDefaultLanguageCode($defaultLanguageCode)
    {
        $this->defaultLanguageCode = $defaultLanguageCode;

        return $this;
    }

    /**
     * @param string $noteForZsOnly
     *
     * @return $this
     */
    public function setNoteForZsOnly($noteForZsOnly)
    {
        $this->noteForZsOnly = $noteForZsOnly;
        $this->nullableChanged();

        return $this;
    }

    /**
     * @param $yearlyRenewalDate
     *
     * @return $this
     */
    public function setYearlyRenewalDate($yearlyRenewalDate)
    {
        $this->yearlyRenewalDate = $yearlyRenewalDate;
        $this->nullableChanged();

        return $this;
    }

    /**
     * @param Carbon $trialEndDate
     *
     * @return UpdateOrganizationRequest
     */
    public function setTrialEndDate($trialEndDate)
    {
        $this->trialEndDate = $trialEndDate;

        return $this;
    }

    /**
     * @param bool $isBillable
     *
     * @return UpdateOrganizationRequest
     */
    public function setIsBillable($isBillable)
    {
        $this->isBillable = $isBillable;

        return $this;
    }

    /**
     * @param bool $hasTrial
     *
     * @return UpdateOrganizationRequest
     */
    public function setHasTrial($hasTrial)
    {
        $this->hasTrial = $hasTrial;

        return $this;
    }

    /**
     * @param bool $isNotifiedEndTrial
     *
     * @return UpdateOrganizationRequest
     */
    public function setIsNotifiedEndTrial($isNotifiedEndTrial)
    {
        $this->isNotifiedEndTrial = $isNotifiedEndTrial;

        return $this;
    }

    /**
     * @param bool $canGrantTrial
     *
     * @return UpdateOrganizationRequest
     */
    public function setCanGrantTrial($canGrantTrial)
    {
        $this->canGrantTrial = $canGrantTrial;

        return $this;
    }

    /**
     * @param bool $isClientsNotifiedEndTrial
     *
     * @return UpdateOrganizationRequest
     */
    public function setIsClientsNotifiedEndTrial($isClientsNotifiedEndTrial)
    {
        $this->isClientsNotifiedEndTrial = $isClientsNotifiedEndTrial;

        return $this;
    }

    /**
     * @param bool $isTwoFactorAuthMandatory
     *
     * @return UpdateOrganizationRequest
     */
    public function setIsTwoFactorAuthMandatory($isTwoFactorAuthMandatory)
    {
        $this->isTwoFactorAuthMandatory = $isTwoFactorAuthMandatory;

        return $this;
    }

    /**
     * @param TwoFactorAuthUsersEnum $twoFactorAuthUsers
     *
     * @return UpdateOrganizationRequest
     */
    public function setTwoFactorAuthUsers(TwoFactorAuthUsersEnum $twoFactorAuthUsers)
    {
        $this->twoFactorAuthUsers = $twoFactorAuthUsers;

        return $this;
    }

    /**
     * @param bool $renewalMessagesAllowed
     *
     * @return UpdateOrganizationRequest
     */
    public function setRenewalMessagesAllowed($renewalMessagesAllowed)
    {
        $this->renewalMessagesAllowed = $renewalMessagesAllowed;

        return $this;
    }

    /**
     * @param string $renewalNote
     *
     * @return UpdateOrganizationRequest
     */
    public function setRenewalNote($renewalNote)
    {
        $this->renewalNote = $renewalNote;
        $this->nullableChanged();

        return $this;
    }

    /**
     * @param bool $isSchoolOrganization
     *
     * @return UpdateOrganizationRequest
     */
    public function setIsSchoolOrganization($isSchoolOrganization)
    {
        $this->isSchoolOrganization = $isSchoolOrganization;
        return $this;
    }

    /**
     * Type of request
     *
     * @return RequestType
     */
    public function httpType()
    {
        return RequestType::HTTP_PUT();
    }
}