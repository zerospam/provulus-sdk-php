<?php
/**
 * Created by PhpStorm.
 * User: ycoutu
 * Date: 19/09/18
 * Time: 10:34 AM
 */

namespace ProvulusSDK\Client\Request\Organization\Billing;

use ProvulusSDK\Client\Request\RequestType;
use ProvulusSDK\Client\Request\Resource\Organization\Billing\BillingAttributesTrait;
use ProvulusSDK\Client\Request\Resource\Organization\Billing\BillingProcessResponseTrait;
use ProvulusSDK\Client\Request\Resource\Organization\Billing\BillingRequest;
use ProvulusSDK\Utils\Organization\Billing\Rebate;

class UpdateBillingRequest extends BillingRequest
{
    use BillingAttributesTrait, BillingProcessResponseTrait;

    /** @var string */
    private $externalId;

    /** @var Rebate[] */
    private $rebates;

    /** @var int */
    private $pricelist;

    /**
     * @param string $externalId
     * @return $this
     */
    public function setExternalId($externalId)
    {
        $this->externalId = $externalId;
        return $this;
    }

    /**
     * @param Rebate[] $rebates
     * @return $this
     */
    public function setRebates($rebates)
    {
        $this->rebates = $rebates;
        return $this;
    }

    /**
     * @param int $pricelist
     * @return $this
     */
    public function setPricelist($pricelist)
    {
        $this->pricelist = $pricelist;
        return $this;
    }

    /**
     * Type of request
     *
     * @return RequestType
     */
    public function httpType()
    {
        return RequestType::HTTP_PUT();
    }
}
