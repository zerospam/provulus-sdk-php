<?php
/**
 * Created by PhpStorm.
 * User: ycoutu
 * Date: 19/09/18
 * Time: 10:36 AM
 */

namespace ProvulusSDK\Client\Request\Organization\Billing;

use ProvulusSDK\Client\Request\RequestType;
use ProvulusSDK\Client\Request\Resource\Organization\Billing\BillingProcessResponseTrait;
use ProvulusSDK\Client\Request\Resource\Organization\Billing\BillingRequest;

class ReadBillingRequest extends BillingRequest
{
    use BillingProcessResponseTrait;

    /**
     * Type of request
     *
     * @return RequestType
     */
    public function httpType()
    {
        return RequestType::HTTP_GET();
    }
}
