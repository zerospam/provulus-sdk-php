<?php
/**
 * Created by PhpStorm.
 * User: ycoutu
 * Date: 11/12/18
 * Time: 4:34 PM
 */

namespace ProvulusSDK\Client\Request\Organization\Billing\Invoice;

use ProvulusSDK\Client\Request\Collection\CollectionPaginatedTrait;
use ProvulusSDK\Client\Request\Collection\IProvulusCollectionPaginatedRequest;
use ProvulusSDK\Client\Request\RequestType;
use ProvulusSDK\Client\Request\Resource\Organization\Billing\BillingRequest;
use ProvulusSDK\Client\Response\IProvulusObjectResponse;
use ProvulusSDK\Client\Response\Organization\Billing\Invoice\InvoiceCollectionResponse;
use ProvulusSDK\Client\Response\Organization\Billing\Invoice\InvoiceResponse;

/**
 * Class IndexInvoiceRequest
 *
 * @method InvoiceCollectionResponse getResponse()
 *
 * @package ProvulusSDK\Client\Request\Organization\Billing\Invoice
 */
class IndexInvoiceRequest extends BillingRequest implements IProvulusCollectionPaginatedRequest
{
    use CollectionPaginatedTrait;

    /**
     * Base route for orgs resource
     *
     * @return string
     * @throws \Exception
     */
    function baseRoute()
    {
        return parent::baseRoute() . '/invoices';
    }

    /**
     * Type of request
     *
     * @return RequestType
     */
    public function httpType()
    {
        return RequestType::HTTP_GET();
    }

    /**
     * Used to parse each result of the collection
     *
     * Need to be overridden by the class
     *
     * @param array $objectData
     *
     * @return IProvulusObjectResponse
     */
    function toResponse(array $objectData)
    {
        return new InvoiceResponse($objectData);
    }

    /**
     * Class to be used to build the collection
     *
     * @return string
     */
    function collectionClass()
    {
        return InvoiceCollectionResponse::class;
    }
}
