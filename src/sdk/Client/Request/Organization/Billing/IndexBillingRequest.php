<?php
/**
 * Created by PhpStorm.
 * User: ycoutu
 * Date: 19/09/18
 * Time: 10:36 AM
 */

namespace ProvulusSDK\Client\Request\Organization\Billing;

use ProvulusSDK\Client\Request\BaseProvulusRequest;
use ProvulusSDK\Client\Request\Collection\CollectionPaginatedTrait;
use ProvulusSDK\Client\Request\Collection\IProvulusCollectionPaginatedRequest;
use ProvulusSDK\Client\Request\RequestType;
use ProvulusSDK\Client\Response\IProvulusObjectResponse;
use ProvulusSDK\Client\Response\Organization\Billing\BillingCollectionResponse;
use ProvulusSDK\Client\Response\Organization\Billing\BillingResponse;

/**
 * Class IndexBillingRequest
 *
 * @method BillingCollectionResponse getResponse()
 *
 * @package ProvulusSDK\Client\Request\Organization\Billing
 */
class IndexBillingRequest extends BaseProvulusRequest implements IProvulusCollectionPaginatedRequest
{
    use CollectionPaginatedTrait;

    /**
     * Type of request
     *
     * @return RequestType
     */
    public function httpType()
    {
        return RequestType::HTTP_GET();
    }

    /**
     * Used to parse each result of the collection
     *
     * Need to be overridden by the class
     *
     * @param array $objectData
     *
     * @return IProvulusObjectResponse
     */
    function toResponse(array $objectData)
    {
        return new BillingResponse($objectData);
    }

    /**
     * Class to be used to build the collection
     *
     * @return string
     */
    function collectionClass()
    {
        return BillingCollectionResponse::class;
    }

    /**
     * Base route without binding
     *
     * @return string
     */
    function routeUrl()
    {
        return 'orgs/billings';
    }
}
