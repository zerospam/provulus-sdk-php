<?php
/**
 * Created by PhpStorm.
 * User: ycoutu
 * Date: 29/08/17
 * Time: 11:57 AM
 */

namespace ProvulusSDK\Client\Request\Organization\Logo;

use ProvulusSDK\Client\Request\RequestType;
use ProvulusSDK\Client\Request\Resource\Organization\OrganizationProcessResponseTrait;
use ProvulusSDK\Client\Request\Resource\Organization\OrganizationRequest;

class UpdateOrganizationLogoRequest extends OrganizationRequest
{
    use OrganizationProcessResponseTrait;

    /** @var null */
    private $logoReference;

    public function setLogoReference($logo)
    {
        if (is_null($logo)) {
            $this->logoReference = null;
            $this->nullableChanged();
        } else {
            $this->addFile($logo);
        }

        return $this;
    }

    /**
     * Type of request
     *
     * @return RequestType
     */
    public function httpType()
    {
        return RequestType::HTTP_POST();
    }

    /**
     * @return string
     */
    function baseRoute()
    {
        return parent::baseRoute().'/logo';
    }
}
