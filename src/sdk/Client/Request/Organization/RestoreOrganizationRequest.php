<?php
/**
 * Created by PhpStorm.
 * User: ycoutu
 * Date: 05/02/18
 * Time: 4:46 PM
 */

namespace ProvulusSDK\Client\Request\Organization;

use ProvulusSDK\Client\Request\EmptyResponseTrait;
use ProvulusSDK\Client\Request\RequestType;
use ProvulusSDK\Client\Request\Resource\Organization\OrganizationRequest;

class RestoreOrganizationRequest extends OrganizationRequest
{
    use EmptyResponseTrait;

    /**
     * Type of request
     *
     * @return RequestType
     */
    public function httpType()
    {
        return RequestType::HTTP_POST();
    }

    function baseRoute()
    {
        return parent::baseRoute() . '/restore';
    }
}
