<?php
/**
 * Created by PhpStorm.
 * User: ycoutu
 * Date: 05/02/18
 * Time: 4:44 PM
 */

namespace ProvulusSDK\Client\Request\Organization\Cancellation;

use ProvulusSDK\Client\Request\EmptyResponseTrait;
use ProvulusSDK\Client\Request\RequestType;
use ProvulusSDK\Client\Request\Resource\Organization\OrganizationRequest;

class DeleteCancellationRequest extends OrganizationRequest
{
    use EmptyResponseTrait;

    /**
     * Type of request
     *
     * @return RequestType
     */
    public function httpType()
    {
        return RequestType::HTTP_DELETE();
    }

    function baseRoute()
    {
        return parent::baseRoute() . '/cancellation';
    }
}
