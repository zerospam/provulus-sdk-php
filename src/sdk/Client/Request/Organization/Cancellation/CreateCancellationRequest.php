<?php
/**
 * Created by PhpStorm.
 * User: ycoutu
 * Date: 05/02/18
 * Time: 4:41 PM
 */

namespace ProvulusSDK\Client\Request\Organization\Cancellation;

use Carbon\Carbon;
use ProvulusSDK\Client\Request\RequestType;
use ProvulusSDK\Client\Request\Resource\Organization\OrganizationRequest;
use ProvulusSDK\Client\Response\IProvulusResponse;
use ProvulusSDK\Client\Response\Organization\Cancellation\CancellationInformationResponse;

/**
 * Class CreateCancellationRequest
 *
 * @method CancellationInformationResponse getResponse()
 *
 * @package ProvulusSDK\Client\Request\Organization\Cancellation
 */
class CreateCancellationRequest extends OrganizationRequest
{
    /** @var string */
    private $reason;

    /** @var Carbon */
    private $requestedDate;

    /**
     * @param string $reason
     *
     * @return CreateCancellationRequest
     */
    public function setReason($reason)
    {
        $this->reason = $reason;

        return $this;
    }

    /**
     * @param Carbon $requestedDate
     *
     * @return CreateCancellationRequest
     */
    public function setRequestedDate($requestedDate)
    {
        $this->requestedDate = $requestedDate;

        return $this;
    }

    /**
     * Type of request
     *
     * @return RequestType
     */
    public function httpType()
    {
        return RequestType::HTTP_POST();
    }

    function baseRoute()
    {
        return parent::baseRoute() . '/cancellation';
    }

    /**
     * Process the data that is in the response
     *
     * @param array $jsonResponse
     *
     * @return IProvulusResponse
     */
    public function processResponse(array $jsonResponse)
    {
        return new CancellationInformationResponse($jsonResponse['data']);
    }
}
