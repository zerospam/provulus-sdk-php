<?php
/**
 * Created by PhpStorm.
 * User: ycoutu
 * Date: 18/04/18
 * Time: 9:49 AM
 */

namespace ProvulusSDK\Client\Request\Organization\Password;

use ProvulusSDK\Client\Request\RequestType;
use ProvulusSDK\Client\Request\Resource\Organization\OrganizationRequest;
use ProvulusSDK\Client\Response\Organization\Password\PasswordConfigurationResponse;

/**
 * Class ReadPasswordConfigurationRequest
 *
 * @package ProvulusSDK\Client\Request\Organization\Password
 *
 * @method PasswordConfigurationResponse getResponse()
 */
class ReadPasswordConfigurationRequest extends OrganizationRequest
{

    /**
     * Type of request
     *
     * @return RequestType
     */
    public function httpType()
    {
        return RequestType::HTTP_GET();
    }

    /**
     * Process the data that is in the response
     *
     * @param array $jsonResponse
     *
     * @return PasswordConfigurationResponse
     */
    public function processResponse(array $jsonResponse)
    {
        return new PasswordConfigurationResponse($jsonResponse['data']);
    }

    /**
     * Base route for orgs resource
     *
     * @return string
     * @throws \Exception
     */
    function baseRoute()
    {
        return parent::baseRoute() . '/password-configuration';
    }
}
