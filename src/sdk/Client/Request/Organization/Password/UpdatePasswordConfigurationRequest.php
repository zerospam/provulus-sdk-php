<?php
/**
 * Created by PhpStorm.
 * User: ycoutu
 * Date: 18/04/18
 * Time: 9:27 AM
 */

namespace ProvulusSDK\Client\Request\Organization\Password;

use ProvulusSDK\Client\Request\RequestType;
use ProvulusSDK\Client\Request\Resource\Organization\OrganizationRequest;
use ProvulusSDK\Client\Response\IProvulusResponse;
use ProvulusSDK\Client\Response\Organization\Password\PasswordConfigurationResponse;

/**
 * Class UpdatePasswordConfigurationRequest
 *
 * @package ProvulusSDK\Client\Request\Organization\Password
 *
 * @method PasswordConfigurationResponse getResponse()
 */
class UpdatePasswordConfigurationRequest extends OrganizationRequest
{
    /** @var int */
    private $minLength;

    /** @var int */
    private $minLower;

    /** @var int */
    private $minUpper;

    /** @var int */
    private $minDigits;

    /** @var int */
    private $minSpecials;

    /** @var int|null */
    private $lifeInDays;

    /** @var int|null */
    private $historyCount;

    /**
     * @param int $minLength
     *
     * @return $this
     */
    public function setMinLength($minLength)
    {
        $this->minLength = $minLength;

        return $this;
    }

    /**
     * @param int $minLower
     *
     * @return $this
     */
    public function setMinLower($minLower)
    {
        $this->minLower = $minLower;

        return $this;
    }

    /**
     * @param int $minUpper
     *
     * @return $this
     */
    public function setMinUpper($minUpper)
    {
        $this->minUpper = $minUpper;

        return $this;
    }

    /**
     * @param int $minDigits
     *
     * @return $this
     */
    public function setMinDigits($minDigits)
    {
        $this->minDigits = $minDigits;

        return $this;
    }

    /**
     * @param int $minSpecials
     *
     * @return $this
     */
    public function setMinSpecials($minSpecials)
    {
        $this->minSpecials = $minSpecials;

        return $this;
    }

    /**
     * @param int|null $lifeInDays
     *
     * @return $this
     */
    public function setLifeInDays($lifeInDays)
    {
        $this->nullableChanged();
        $this->lifeInDays = $lifeInDays;

        return $this;
    }

    /**
     * @param int|null $historyCount
     *
     * @return $this
     */
    public function setHistoryCount($historyCount)
    {
        $this->nullableChanged();
        $this->historyCount = $historyCount;

        return $this;
    }

    /**
     * Type of request
     *
     * @return RequestType
     */
    public function httpType()
    {
        return RequestType::HTTP_POST();
    }

    /**
     * Process the data that is in the response
     *
     * @param array $jsonResponse
     *
     * @return IProvulusResponse
     */
    public function processResponse(array $jsonResponse)
    {
        return new PasswordConfigurationResponse($jsonResponse['data']);
    }


    /**
     * Base route for orgs resource
     *
     * @return string
     * @throws \Exception
     */
    function baseRoute()
    {
        return parent::baseRoute() . '/password-configuration';
    }
}
