<?php
/**
 * Created by PhpStorm.
 * User: pbb
 * Date: 23/12/16
 * Time: 2:27 PM
 */

namespace ProvulusSDK\Client\Request\Organization\MX;

use ProvulusSDK\Client\Request\EmptyResponseTrait;
use ProvulusSDK\Client\Request\RequestType;
use ProvulusSDK\Client\Request\Resource\Organization\MX\OrganizationMXRequest;

/**
 * Class DeleteMXRequest
 *
 * Request to delete a MX record for an organization
 *
 * @package ProvulusSDK\Client\Request\Organization\MX
 */
class DeleteMXRequest extends OrganizationMXRequest
{
    use EmptyResponseTrait;

    /**
     * Type of request
     *
     * @return RequestType
     */
    public function httpType()
    {
        return RequestType::HTTP_DELETE();
    }
}