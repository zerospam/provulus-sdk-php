<?php
/**
 * Created by PhpStorm.
 * User: pbb
 * Date: 23/12/16
 * Time: 2:13 PM
 */

namespace ProvulusSDK\Client\Request\Organization\MX;

use ProvulusSDK\Client\Request\Collection\CollectionPaginatedTrait;
use ProvulusSDK\Client\Request\Collection\IProvulusCollectionPaginatedRequest;
use ProvulusSDK\Client\Request\Collection\SearchableTrait;
use ProvulusSDK\Client\Request\RequestType;
use ProvulusSDK\Client\Request\Resource\Organization\MX\BaseOrganizationMXRequest;
use ProvulusSDK\Client\Response\IProvulusObjectResponse;
use ProvulusSDK\Client\Response\Organization\MX\OrganizationMXCollectionResponse;
use ProvulusSDK\Client\Response\Organization\MX\OrganizationMXResponse;

/**
 * Class IndexMXRequest
 *
 * Request to retrieve MX Collection for Organization
 *
 * @method OrganizationMXCollectionResponse getResponse()
 *
 * @package ProvulusSDK\Client\Request\Organization\MX
 */
class IndexMXRequest extends BaseOrganizationMXRequest implements IProvulusCollectionPaginatedRequest
{
    use CollectionPaginatedTrait, SearchableTrait;

    /**
     * Type of request
     *
     * @return RequestType
     */
    public function httpType()
    {
        return RequestType::HTTP_GET();
    }

    /**
     * Used to parse each result of the collection
     *
     * Need to be overridden by the class
     *
     * @param array $array
     *
     * @return IProvulusObjectResponse
     */
    function toResponse(array $array)
    {
        return new OrganizationMXResponse($array);
    }

    /**
     * Class to be used to build the collection
     *
     * @return string
     */
    function collectionClass()
    {
        return OrganizationMXCollectionResponse::class;
    }
}