<?php
/**
 * Created by PhpStorm.
 * User: pbb
 * Date: 23/12/16
 * Time: 2:33 PM
 */

namespace ProvulusSDK\Client\Request\Organization\MX;

use ProvulusSDK\Client\Request\RequestType;
use ProvulusSDK\Client\Request\Resource\Organization\MX\OrganizationMXProcessResponseTrait;
use ProvulusSDK\Client\Request\Resource\Organization\MX\OrganizationMXRequest;
use ProvulusSDK\Client\Request\Resource\Organization\MX\OrganizationMXSetAttributesTrait;

/**
 * Class UpdateMXRequest
 *
 * Update a MX record
 *
 * @package ProvulusSDK\Client\Request\Organization\MX
 */
class UpdateMXRequest extends OrganizationMXRequest
{
    use OrganizationMXSetAttributesTrait, OrganizationMXProcessResponseTrait;

    /**
     * Type of request
     *
     * @return RequestType
     */
    public function httpType()
    {
        return RequestType::HTTP_PUT();
    }
}