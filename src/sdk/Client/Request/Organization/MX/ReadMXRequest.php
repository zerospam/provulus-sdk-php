<?php
/**
 * Created by PhpStorm.
 * User: pbb
 * Date: 23/12/16
 * Time: 11:32 AM
 */

namespace ProvulusSDK\Client\Request\Organization\MX;

use ProvulusSDK\Client\Request\RequestType;
use ProvulusSDK\Client\Request\Resource\Organization\MX\OrganizationMXProcessResponseTrait;
use ProvulusSDK\Client\Request\Resource\Organization\MX\OrganizationMXRequest;

/**
 * Class ReadMXRequest
 *
 * Read MX record of
 *
 * @package ProvulusSDK\Client\Request\Organization\MX
 */
class ReadMXRequest extends OrganizationMXRequest
{
    use OrganizationMXProcessResponseTrait;

    /**
     * Type of request
     *
     * @return RequestType
     */
    public function httpType()
    {
        return RequestType::HTTP_GET();
    }
}