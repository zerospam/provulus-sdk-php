<?php
/**
 * Created by PhpStorm.
 * User: pbb
 * Date: 23/12/16
 * Time: 11:41 AM
 */

namespace ProvulusSDK\Client\Request\Organization\MX;

use ProvulusSDK\Client\Request\Collection\ResponseCollectionTrait;
use ProvulusSDK\Client\Request\RequestType;
use ProvulusSDK\Client\Request\Resource\Organization\MX\BaseOrganizationMXRequest;
use ProvulusSDK\Client\Response\Organization\MX\OrganizationMXCollectionResponse;
use ProvulusSDK\Client\Response\Organization\MX\OrganizationMXResponse;

/**
 * Class CreateMXRequest
 *
 * Create MX record for an organization
 *
 * @method OrganizationMXCollectionResponse getResponse()
 * @package ProvulusSDK\Client\Request\Organization\MX
 */
class CreateMXRequest extends BaseOrganizationMXRequest
{
    use ResponseCollectionTrait;

    /**
     * @var array
     */
    private $mxs;

    /**
     * @param string $name
     * @param int    $priority
     *
     * @return CreateMXRequest
     */
    public function addMXRecord($name, $priority)
    {
        $this->mxs [] = array(
            'name'     => $name,
            'priority' => $priority,
        );

        return $this;
    }

    /**
     * Type of request
     *
     * @return RequestType
     */
    public function httpType()
    {
        return RequestType::HTTP_POST();
    }

    /**
     * Used to parse each result of the collection
     *
     * Need to be overridden by the class
     *
     * @param array $objectData
     *
     * @param array $includedData
     *
     * @return OrganizationMXResponse
     */
    function toResponse(array $objectData, array $includedData = [])
    {
        return new OrganizationMXResponse($objectData, $includedData);
    }

    /**
     * Class to be used to build the collection
     *
     * @return string
     */
    function collectionClass()
    {
        return OrganizationMXCollectionResponse::class;
    }
}