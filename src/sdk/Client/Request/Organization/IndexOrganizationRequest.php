<?php
/**
 * Created by PhpStorm.
 * User: pbb
 * Date: 10/02/17
 * Time: 8:53 AM
 */

namespace ProvulusSDK\Client\Request\Organization;

use ProvulusSDK\Client\Request\BaseProvulusRequest;
use ProvulusSDK\Client\Request\Collection\CollectionPaginatedTrait;
use ProvulusSDK\Client\Request\Collection\IProvulusCollectionPaginatedRequest;
use ProvulusSDK\Client\Request\Collection\SearchableTrait;
use ProvulusSDK\Client\Request\Organization\Traits\OrganizationSearchArguments;
use ProvulusSDK\Client\Request\Organization\Traits\WithBilling;
use ProvulusSDK\Client\Request\Organization\Traits\WithCancellationInformation;
use ProvulusSDK\Client\Request\Organization\Traits\WithDistributorOrganization;
use ProvulusSDK\Client\Request\Organization\Traits\WithDomains;
use ProvulusSDK\Client\Request\Organization\Traits\WithOutbound;
use ProvulusSDK\Client\Request\Organization\Traits\WithPostTrialAction;
use ProvulusSDK\Client\Request\Organization\Traits\WithSupportOrganization;
use ProvulusSDK\Client\Request\RequestType;
use ProvulusSDK\Client\Response\IProvulusObjectResponse;
use ProvulusSDK\Client\Response\Organization\OrganizationCollectionResponse;
use ProvulusSDK\Client\Response\Organization\OrganizationResponse;

/**
 * Class IndexOrganizationRequest
 *
 * Request for retrieving collection of organizations
 * @method OrganizationCollectionResponse getResponse()
 *
 * @package ProvulusSDK\Client\Request\Organization
 */
class IndexOrganizationRequest extends BaseProvulusRequest implements IProvulusCollectionPaginatedRequest
{
    use CollectionPaginatedTrait,
        SearchableTrait,
        WithSupportOrganization,
        OrganizationSearchArguments,
        WithDomains,
        WithCancellationInformation,
        WithBilling,
        WithPostTrialAction,
        WithOutbound,
        WithDistributorOrganization;

    /**
     * Type of request
     *
     * @return RequestType
     */
    public function httpType()
    {
        return RequestType::HTTP_GET();
    }

    /**
     * Used to parse each result of the collection
     *
     * Need to be overridden by the class
     *
     * @param array $array
     *
     * @param array $includedData
     *
     * @return IProvulusObjectResponse
     */
    function toResponse(array $array, array $includedData = [])
    {
        return new OrganizationResponse($array, $includedData);
    }

    /**
     * Class to be used to build the collection
     *
     * @return string
     */
    function collectionClass()
    {
        return OrganizationCollectionResponse::class;
    }

    /**
     * The url of the route
     *
     * @return string
     */
    public function routeUrl()
    {
        return 'orgs';
    }
}
