<?php


namespace ProvulusSDK\Client\Request\Organization\Outbound;

use ProvulusSDK\Client\Request\RequestType;
use ProvulusSDK\Client\Request\Resource\Organization\Outbound\OutboundProcessResponseTrait;
use ProvulusSDK\Client\Request\Resource\Organization\Outbound\OutboundRequest;

class UpdateOutboundRequest extends OutboundRequest
{
    use OutboundProcessResponseTrait;

    /** @var bool */
    private $isFilterInboundDomains;

    /** @var int */
    private $defaultFilteringPolicyId;

    /** @var int */
    private $bounceFilteringPolicyId;

    /**
     * Type of request
     *
     * @return RequestType
     */
    public function httpType()
    {
        return RequestType::HTTP_PUT();
    }

    /**
     * @param bool $isFilterInboundDomains
     *
     * @return $this
     */
    public function setIsFilterInboundDomains($isFilterInboundDomains)
    {
        $this->isFilterInboundDomains = $isFilterInboundDomains;

        return $this;
    }

    /**
     * @param int $defaultFilteringPolicyId
     *
     * @return $this
     */
    public function setDefaultFilteringPolicyId($defaultFilteringPolicyId)
    {
        $this->defaultFilteringPolicyId = $defaultFilteringPolicyId;

        return $this;
    }

    /**
     * @param int $bounceFilteringPolicyId
     *
     * @return $this
     */
    public function setBounceFilteringPolicyId($bounceFilteringPolicyId)
    {
        $this->bounceFilteringPolicyId = $bounceFilteringPolicyId;

        return $this;
    }


}
