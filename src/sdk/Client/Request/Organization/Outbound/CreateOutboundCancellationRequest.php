<?php

namespace ProvulusSDK\Client\Request\Organization\Outbound;

use ProvulusSDK\Client\Request\EmptyResponseTrait;
use ProvulusSDK\Client\Request\RequestType;
use ProvulusSDK\Client\Request\Resource\Organization\Outbound\OutboundRequest;

class CreateOutboundCancellationRequest extends OutboundRequest
{
    use EmptyResponseTrait;

    /**
     * Type of request
     *
     * @return RequestType
     */
    public function httpType()
    {
        return RequestType::HTTP_POST();
    }

    /**
     * The url of the route
     *
     * @return string
     */
    public function baseRoute()
    {
        return parent::baseRoute() . '/create-cancellation';
    }
}
