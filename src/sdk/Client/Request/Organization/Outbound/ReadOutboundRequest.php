<?php


namespace ProvulusSDK\Client\Request\Organization\Outbound;

use ProvulusSDK\Client\Request\RequestType;
use ProvulusSDK\Client\Request\Resource\Organization\Outbound\OutboundProcessResponseTrait;
use ProvulusSDK\Client\Request\Resource\Organization\Outbound\OutboundRequest;

class ReadOutboundRequest extends OutboundRequest
{
    use OutboundProcessResponseTrait;

    /**
     * Type of request
     *
     * @return RequestType
     */
    public function httpType()
    {
        return RequestType::HTTP_GET();
    }
}
