<?php
/**
 * Created by PhpStorm.
 * User: pbb
 * Date: 26/01/18
 * Time: 10:47 AM
 */

namespace ProvulusSDK\Client\Request\Organization\Trial;


use ProvulusSDK\Client\Request\BaseProvulusRequest;
use ProvulusSDK\Client\Request\EmptyResponseTrait;
use ProvulusSDK\Client\Request\RequestType;
use ProvulusSDK\Enum\Trial\PostTrialStateEnum;

/**
 * Class PostTrialClientConfirmRequest
 *
 * @package ProvulusSDK\Client\Request\Organization\Trial
 */
class PostTrialConfirmRequest extends BaseProvulusRequest
{
    use EmptyResponseTrait;

    /** @var  string */
    private $token;

    /** @var  PostTrialStateEnum */
    private $state;

    /**
     * The url of the route
     *
     * @return string
     */
    public function routeUrl()
    {
        return 'posttrial/confirm';
    }

    /**
     * Type of request
     *
     * @return RequestType
     */
    public function httpType()
    {
        return RequestType::HTTP_POST();
    }

    /**
     * @param string $token
     *
     * @return PostTrialConfirmRequest
     */
    public function setToken($token)
    {
        $this->token = $token;

        return $this;
    }

    /**
     * @param PostTrialStateEnum $state
     *
     * @return PostTrialConfirmRequest
     */
    public function setState(PostTrialStateEnum $state)
    {
        $this->state = $state;

        return $this;
    }
}