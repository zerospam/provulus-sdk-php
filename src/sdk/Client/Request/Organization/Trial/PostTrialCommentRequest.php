<?php
/**
 * Created by PhpStorm.
 * User: pbb
 * Date: 30/01/18
 * Time: 11:29 AM
 */

namespace ProvulusSDK\Client\Request\Organization\Trial;

/**
 * Class PostTrialCommentRequest
 *
 * @package ProvulusSDK\Client\Request\Organization\Trial
 */
class PostTrialCommentRequest extends PostTrialConfirmRequest
{

    /** @var  string */
    private $comment;

    /**
     * The url of the route
     *
     * @return string
     */
    public function routeUrl()
    {
        return 'posttrial/comment';
    }

    /**
     * @param string $comment
     *
     * @return PostTrialCommentRequest
     */
    public function setComment($comment)
    {
        $this->comment = $comment;

        return $this;
    }
}