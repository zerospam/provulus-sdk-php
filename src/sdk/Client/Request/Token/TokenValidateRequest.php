<?php
/**
 * Created by PhpStorm.
 * User: pbb
 * Date: 15/05/17
 * Time: 3:38 PM
 */

namespace ProvulusSDK\Client\Request\Token;

use ProvulusSDK\Client\Request\BaseProvulusRequest;
use ProvulusSDK\Client\Request\EmptyResponseTrait;
use ProvulusSDK\Client\Request\RequestType;

/**
 * Class TokenValidateRequest
 *
 * Request for token validation
 *
 * @package ProvulusSDK\Client\Request\Token
 */
class TokenValidateRequest extends BaseProvulusRequest
{

    use EmptyResponseTrait;

    /** @var string $token */
    private $token;

    /**
     * The url of the route
     *
     * @return string
     */
    public function routeUrl()
    {
        return 'validate/token';
    }

    /**
     * Type of request
     *
     * @return RequestType
     */
    public function httpType()
    {
        return RequestType::HTTP_POST();
    }

    /**
     * @param string $token
     *
     * @return TokenValidateRequest
     */
    public function setToken($token)
    {
        $this->token = $token;

        return $this;
    }
}