<?php
/**
 * Created by PhpStorm.
 * User: pbb
 * Date: 20/10/17
 * Time: 11:22 AM
 */

namespace ProvulusSDK\Client\Request\PersonOfInterest;

use ProvulusSDK\Client\Request\BulkRequestTrait;
use ProvulusSDK\Client\Request\EmptyResponseTrait;
use ProvulusSDK\Client\Request\RequestType;
use ProvulusSDK\Client\Request\Resource\PersonOfInterest\PersonOfInterestBaseRequest;

/**
 * Class BulkDeletePersonOfInterestRequest
 *
 * @package ProvulusSDK\Client\Request\PersonOfInterest
 */
class BulkDeletePersonOfInterestRequest extends PersonOfInterestBaseRequest
{

    use EmptyResponseTrait,
        BulkRequestTrait;

    /**
     * Type of request
     *
     * @return RequestType
     */
    public function httpType()
    {
        return RequestType::HTTP_DELETE();
    }
}