<?php
/**
 * Created by PhpStorm.
 * User: pbb
 * Date: 18/10/17
 * Time: 9:13 AM
 */

namespace ProvulusSDK\Client\Request\PersonOfInterest\Traits;

use ProvulusSDK\Client\Response\PersonOfInterest\PersonOfInterestResponse;

/**
 * Trait PersonOfInterestProcessResponse
 *
 * Trait used for requests processing a single Person of interest resource
 *
 * @package ProvulusSDK\Client\Request\PersonOfInterest
 */
trait ProcessResponsePersonOfInterest
{
    /**
     * Process the data that is in the response
     *
     * @param array $jsonResponse
     *
     * @return PersonOfInterestResponse
     */
    public function processResponse(array $jsonResponse)
    {
        return new PersonOfInterestResponse($jsonResponse['data']);
    }
}