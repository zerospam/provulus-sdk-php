<?php
/**
 * Created by PhpStorm.
 * User: pbb
 * Date: 18/10/17
 * Time: 9:29 AM
 */

namespace ProvulusSDK\Client\Request\PersonOfInterest\Traits;

use ProvulusSDK\Enum\PersonOfInterest\ActionUponMatchEnum;

/**
 * Trait SetAttributesPersonOfInterestRequest
 *
 * Common code for Creation and Update of Person of Interest
 *
 * @package ProvulusSDK\Client\Request\PersonOfInterest
 */
trait SetRequestAttributesPersonOfInterest
{
    /** @var string */
    private $fullName;

    /** @var  ActionUponMatchEnum */
    private $actionUponMatch;

    /**
     * @param string $fullName
     *
     * @return $this
     */
    public function setFullName($fullName)
    {
        $this->fullName = $fullName;

        return $this;
    }

    /**
     * @param ActionUponMatchEnum $actionUponMatch
     *
     * @return $this
     */
    public function setActionUponMatch($actionUponMatch)
    {
        $this->actionUponMatch = $actionUponMatch;

        return $this;
    }
}