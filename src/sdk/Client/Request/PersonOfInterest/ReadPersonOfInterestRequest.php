<?php
/**
 * Created by PhpStorm.
 * User: pbb
 * Date: 17/10/17
 * Time: 3:50 PM
 */

namespace ProvulusSDK\Client\Request\PersonOfInterest;


use ProvulusSDK\Client\Request\PersonOfInterest\Traits\ProcessResponsePersonOfInterest;
use ProvulusSDK\Client\Request\RequestType;
use ProvulusSDK\Client\Request\Resource\PersonOfInterest\PersonOfInterestRequest;
use ProvulusSDK\Client\Response\PersonOfInterest\PersonOfInterestResponse;

/**
 * Class ReadPersonOfInterestRequest
 *
 * @method PersonOfInterestResponse getResponse()
 *
 * @package ProvulusSDK\Client\Request\PersonOfInterest
 */
class ReadPersonOfInterestRequest extends PersonOfInterestRequest
{

    use ProcessResponsePersonOfInterest;

    /**
     * Type of request
     *
     * @return RequestType
     */
    public function httpType()
    {
        return RequestType::HTTP_GET();
    }
}