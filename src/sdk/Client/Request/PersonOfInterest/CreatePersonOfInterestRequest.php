<?php
/**
 * Created by PhpStorm.
 * User: pbb
 * Date: 18/10/17
 * Time: 9:12 AM
 */

namespace ProvulusSDK\Client\Request\PersonOfInterest;

use ProvulusSDK\Client\Request\PersonOfInterest\Traits\ProcessResponsePersonOfInterest;
use ProvulusSDK\Client\Request\PersonOfInterest\Traits\SetRequestAttributesPersonOfInterest;
use ProvulusSDK\Client\Request\RequestType;
use ProvulusSDK\Client\Request\Resource\PersonOfInterest\PersonOfInterestBaseRequest;
use ProvulusSDK\Client\Response\PersonOfInterest\PersonOfInterestResponse;

/**
 * Class CreatePersonOfInterestRequest
 *
 * @method PersonOfInterestResponse getResponse()
 *
 * @package ProvulusSDK\Client\Request\PersonOfInterest
 */
class CreatePersonOfInterestRequest extends PersonOfInterestBaseRequest
{

    use ProcessResponsePersonOfInterest,
        SetRequestAttributesPersonOfInterest;

    /**
     * Type of request
     *
     * @return RequestType
     */
    public function httpType()
    {
        return RequestType::HTTP_POST();
    }
}