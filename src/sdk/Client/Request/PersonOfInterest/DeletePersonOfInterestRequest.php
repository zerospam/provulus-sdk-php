<?php
/**
 * Created by PhpStorm.
 * User: pbb
 * Date: 20/10/17
 * Time: 11:22 AM
 */

namespace ProvulusSDK\Client\Request\PersonOfInterest;

use ProvulusSDK\Client\Request\EmptyResponseTrait;
use ProvulusSDK\Client\Request\RequestType;
use ProvulusSDK\Client\Request\Resource\PersonOfInterest\PersonOfInterestRequest;

/**
 * Class DeletePersonOfInterestRequest
 *
 * @package ProvulusSDK\Client\Request\PersonOfInterest
 */
class DeletePersonOfInterestRequest extends PersonOfInterestRequest
{

    use EmptyResponseTrait;

    /**
     * Type of request
     *
     * @return RequestType
     */
    public function httpType()
    {
        return RequestType::HTTP_DELETE();
    }
}