<?php
/**
 * Created by PhpStorm.
 * User: pbb
 * Date: 17/10/17
 * Time: 3:50 PM
 */

namespace ProvulusSDK\Client\Request\PersonOfInterest;


use ProvulusSDK\Client\Request\Collection\CollectionPaginatedTrait;
use ProvulusSDK\Client\Request\Collection\IProvulusCollectionPaginatedRequest;
use ProvulusSDK\Client\Request\RequestType;
use ProvulusSDK\Client\Request\Resource\PersonOfInterest\PersonOfInterestBaseRequest;
use ProvulusSDK\Client\Response\IProvulusObjectResponse;
use ProvulusSDK\Client\Response\PersonOfInterest\PersonOfInterestCollectionResponse;
use ProvulusSDK\Client\Response\PersonOfInterest\PersonOfInterestResponse;

/**
 * Class IndexPersonOfInterestRequest
 *
 * @method PersonOfInterestCollectionResponse getResponse()
 *
 * @package ProvulusSDK\Client\Request\PersonOfInterest
 */
class IndexPersonOfInterestRequest extends PersonOfInterestBaseRequest implements IProvulusCollectionPaginatedRequest
{
    use CollectionPaginatedTrait;

    /**
     * Type of request
     *
     * @return RequestType
     */
    public function httpType()
    {
        return RequestType::HTTP_GET();
    }

    /**
     * Used to parse each result of the collection
     *
     * Need to be overridden by the class
     *
     * @param array $objectData
     *
     * @param array $includedData
     *
     * @return IProvulusObjectResponse
     */
    function toResponse(array $objectData, array $includedData = [])
    {
        return new PersonOfInterestResponse($objectData, $includedData);
    }

    /**
     * Class to be used to build the collection
     *
     * @return string
     */
    function collectionClass()
    {
        return PersonOfInterestCollectionResponse::class;
    }
}