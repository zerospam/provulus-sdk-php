<?php
/**
 * Created by PhpStorm.
 * User: ycoutu
 * Date: 21/07/17
 * Time: 9:15 AM
 */

namespace ProvulusSDK\Client\Request\Parameter\HomeMessage;

use ProvulusSDK\Client\Request\BindableRequest;
use ProvulusSDK\Client\Request\RequestType;
use ProvulusSDK\Client\Response\IProvulusResponse;
use ProvulusSDK\Client\Response\Parameter\HomeMessage\HomeMessageResponse;
use ProvulusSDK\Enum\Locale\LocaleEnum;

class ReadHomeMessageRequest extends BindableRequest
{
    /**
     * Base route without binding
     *
     * @return string
     */
    function baseRoute()
    {
        return 'homemessages/:language';
    }

    /**
     * Type of request
     *
     * @return RequestType
     */
    public function httpType()
    {
        return RequestType::HTTP_GET();
    }

    /**
     * Process the data that is in the response
     *
     * @param array $jsonResponse
     *
     * @return IProvulusResponse
     */
    public function processResponse(array $jsonResponse)
    {
        return new HomeMessageResponse($jsonResponse['data']);
    }

    /**
     * @param LocaleEnum $language
     *
     * @return $this
     */
    public function setLanguage(LocaleEnum $language)
    {
        $this->addBinding('language', $language->getValue());

        return $this;
    }
}
