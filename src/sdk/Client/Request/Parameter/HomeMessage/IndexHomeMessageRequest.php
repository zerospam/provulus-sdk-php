<?php
/**
 * Created by PhpStorm.
 * User: ycoutu
 * Date: 20/07/17
 * Time: 4:50 PM
 */

namespace ProvulusSDK\Client\Request\Parameter\HomeMessage;

use ProvulusSDK\Client\Request\BaseProvulusRequest;
use ProvulusSDK\Client\Request\Collection\CollectionPaginatedTrait;
use ProvulusSDK\Client\Request\Collection\IProvulusCollectionPaginatedRequest;
use ProvulusSDK\Client\Request\RequestType;
use ProvulusSDK\Client\Response\IProvulusObjectResponse;
use ProvulusSDK\Client\Response\Parameter\HomeMessage\HomeMessageCollectionResponse;
use ProvulusSDK\Client\Response\Parameter\HomeMessage\HomeMessageResponse;

class IndexHomeMessageRequest extends BaseProvulusRequest implements IProvulusCollectionPaginatedRequest
{
    use CollectionPaginatedTrait;

    /**
     * Type of request
     *
     * @return RequestType
     */
    public function httpType()
    {
        return RequestType::HTTP_GET();
    }

    /**
     * Used to parse each result of the collection
     *
     * Need to be overridden by the class
     *
     * @param array $objectData
     *
     * @return IProvulusObjectResponse
     */
    function toResponse(array $objectData)
    {
        return new HomeMessageResponse($objectData);
    }

    /**
     * Class to be used to build the collection
     *
     * @return string
     */
    function collectionClass()
    {
        return HomeMessageCollectionResponse::class;
    }

    /**
     * The url of the route
     *
     * @return string
     */
    public function routeUrl()
    {
        return 'homemessages';
    }
}
