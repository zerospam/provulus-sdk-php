<?php
/**
 * Created by PhpStorm.
 * User: ycoutu
 * Date: 24/07/17
 * Time: 10:59 AM
 */

namespace ProvulusSDK\Client\Request\Parameter\HomeMessage;

use ProvulusSDK\Client\Request\BaseProvulusRequest;
use ProvulusSDK\Client\Request\RequestType;
use ProvulusSDK\Client\Response\IProvulusResponse;
use ProvulusSDK\Client\Response\Parameter\HomeMessage\HomeMessageResponse;
use ProvulusSDK\Enum\Locale\LocaleEnum;

class CreateHomeMessageRequest extends BaseProvulusRequest
{
    private $languageCode;
    private $message;

    /**
     * The url of the route
     *
     * @return string
     */
    public function routeUrl()
    {
        return 'homemessages';
    }

    /**
     * Type of request
     *
     * @return RequestType
     */
    public function httpType()
    {
        return RequestType::HTTP_POST();
    }

    /**
     * Process the data that is in the response
     *
     * @param array $jsonResponse
     *
     * @return IProvulusResponse
     */
    public function processResponse(array $jsonResponse)
    {
        return new HomeMessageResponse($jsonResponse);
    }

    /**
     * Setter for language_code
     *
     * @param LocaleEnum $languageCode
     *
     * @return $this
     */
    public function setLanguageCode(LocaleEnum $languageCode)
    {
        $this->languageCode = $languageCode->getValue();

        return $this;
    }

    /**
     * Setter for message
     *
     * @param $message
     *
     * @return $this
     */
    public function setMessage($message)
    {
        $this->message = $message;

        return $this;
    }

    /**
     * Setter for the image
     *
     * @param $image
     *
     * @return $this
     */
    public function setImage($image)
    {
        $this->addFile($image);

        return $this;
    }
}
