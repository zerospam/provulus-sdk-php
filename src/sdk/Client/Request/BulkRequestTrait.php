<?php
/**
 * Created by PhpStorm.
 * User: ycoutu
 * Date: 13/06/17
 * Time: 8:58 AM
 */

namespace ProvulusSDK\Client\Request;

trait BulkRequestTrait
{
    protected $ids;

    /**
     * Sets the ids that will be acted on
     *
     * @param int[] $ids
     *
     * @return $this
     */
    public function setIds($ids)
    {
        $this->ids = $ids;

        array_walk($ids, function ($id) {
            if (!is_int($id)) {
                throw new \InvalidArgumentException(sprintf('Id [%s] needs to be an integer.', $id));
            }
        });

        return $this;
    }
}
