<?php
/**
 * Created by PhpStorm.
 * User: pbb
 * Date: 27/01/17
 * Time: 4:23 PM
 */

namespace ProvulusSDK\Client\Request;

use ProvulusSDK\Client\Response\EmptyResponse;

/**
 * Class DeleteResponseTrait
 *
 * Standard Empty Response for Delete Requests
 *
 * @method EmptyResponse getResponse()
 *
 * @package ProvulusSDK\Client\Request\Resource
 */
trait EmptyResponseTrait
{
    /**
     * @param array $jsonData
     *
     * @return EmptyResponse
     */
    public function processResponse(array $jsonData)
    {
        return new EmptyResponse();
    }
}