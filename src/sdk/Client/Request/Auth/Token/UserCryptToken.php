<?php
/**
 * Created by PhpStorm.
 * User: aaflalo
 * Date: 07/02/17
 * Time: 1:37 PM
 */

namespace ProvulusSDK\Client\Request\Auth\Token;


use ProvulusSDK\Client\Request\Resource\PrimalValued;

class UserCryptToken implements PrimalValued
{
    /**
     * @var array
     */
    private $payload;

    /**
     * @var string
     */
    private $encryptKey;


    /**
     * UserCryptoToken constructor.
     *
     * @param $userID
     * @param $userEmail
     * @param $userFullName
     * @param $userType
     */
    public function __construct($userID, $userEmail, $userFullName, $userType)
    {
        $this->payload = [
            $userID,
            $userEmail,
            $userFullName,
            $userType,
        ];
    }

    /**
     * @param string $encryptKey
     *
     * @return $this
     */
    public function setEncryptKey($encryptKey)
    {
        $this->encryptKey = $encryptKey;

        return $this;
    }

    /**
     * Build string payload
     *
     * @return string
     */
    private function buildPayload()
    {
        return implode('/*/', $this->payload);
    }


    /**
     * Return a primitive value for this object
     *
     * @return int|float|string|double
     */
    function toPrimitive()
    {
        return hash_hmac('sha512', $this->buildPayload(), $this->encryptKey);
    }
}