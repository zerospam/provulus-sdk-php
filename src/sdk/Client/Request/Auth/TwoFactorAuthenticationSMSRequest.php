<?php
/**
 * Created by PhpStorm.
 * User: ycoutu
 * Date: 15/02/18
 * Time: 11:00 AM
 */

namespace ProvulusSDK\Client\Request\Auth;

use ProvulusSDK\Client\Request\BaseProvulusRequest;
use ProvulusSDK\Client\Request\EmptyResponseTrait;
use ProvulusSDK\Client\Request\RequestType;

class TwoFactorAuthenticationSMSRequest extends BaseProvulusRequest
{
    /** @var string */
    private $username, $password;

    /** @var UserCryptToken */
    private $passwordToken;

    use EmptyResponseTrait;

    /**
     * TwoFactorAuthenticationSMSRequest constructor.
     *
     * @param string $username
     */
    public function __construct($username)
    {
        $this->username = $username;
    }

    /**
     * Setter for password
     *
     * @param string $password
     *
     * @return $this
     */
    public function setPassword($password)
    {
        $this->password = $password;

        return $this;
    }

    /**
     * @param UserCryptToken $passwordToken
     *
     * @return TwoFactorAuthenticationSMSRequest
     */
    public function setPasswordToken($passwordToken)
    {
        $this->passwordToken = $passwordToken;

        return $this;
    }

    /**
     * The url of the route
     *
     * @return string
     */
    public function routeUrl()
    {
        return 'tfaSms';
    }

    /**
     * Type of request
     *
     * @return RequestType
     */
    public function httpType()
    {
        return RequestType::HTTP_POST();
    }
}
