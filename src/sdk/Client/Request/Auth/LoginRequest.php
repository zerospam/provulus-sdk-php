<?php
/**
 * Created by PhpStorm.
 * User: aaflalo
 * Date: 29/11/16
 * Time: 3:14 PM
 */

namespace ProvulusSDK\Client\Request\Auth;


use ProvulusSDK\Client\Request\Auth\Token\UserCryptToken;
use ProvulusSDK\Client\Request\BaseProvulusRequest;
use ProvulusSDK\Client\Request\RequestType;
use ProvulusSDK\Client\Response\Auth\AuthResponse;

/**
 * Class LoginRequest
 *
 * @method AuthResponse getResponse()
 * @package ProvulusSDK\Client\Request\Auth
 */
class LoginRequest extends BaseProvulusRequest
{


    /**
     * @var string
     */
    private $username, $password, $code;

    /**
     * @var UserCryptToken
     */
    private $passwordToken;

    /**
     * LoginRoute constructor.
     *
     * @param string $username
     *
     * @internal param string $password
     */
    public function __construct($username)
    {
        $this->username = $username;
    }

    /**
     * Set the User password to login
     *
     * @param string $password
     *
     * @return $this
     */
    public function setPassword($password)
    {
        $this->password = $password;

        return $this;
    }

    /**
     * @param UserCryptToken $passwordToken
     *
     * @return $this
     */
    public function setPasswordToken($passwordToken)
    {
        $this->passwordToken = $passwordToken;

        return $this;
    }

    /**
     * Setter for the two factor auth code
     *
     * @param $code
     *
     * @return $this
     */
    public function setCode($code)
    {
        $this->code = $code;

        return $this;
    }


    /**
     * The url of the route
     *
     * @return string
     */
    public function routeUrl()
    {
        return 'authenticate';
    }

    /**
     * Type of request
     *
     * @return RequestType
     */
    public function httpType()
    {
        return RequestType::HTTP_POST();
    }

    /**
     * Process the data that is in the response
     *
     * @param array $jsonResponse
     *
     * @return AuthResponse
     * @internal param Response $response
     */
    public function processResponse(array $jsonResponse)
    {
        return new AuthResponse($jsonResponse);
    }
}