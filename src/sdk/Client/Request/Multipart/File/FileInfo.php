<?php
/**
 * Created by PhpStorm.
 * User: aaflalo
 * Date: 17-07-25
 * Time: 11:57
 */

namespace ProvulusSDK\Client\Request\Multipart\File;

use ProvulusSDK\Client\Request\Resource\Arrayable;

class FileInfo implements Arrayable
{

    /**
     * @var string
     */
    private $name;

    /**
     * @var string
     */
    private $fileHandle;

    /**
     * @var string
     */
    private $filename;

    /**
     * FileInfo constructor.
     *
     * @param string           $fieldName
     * @param  string|resource $file
     * @param string|null      $filename
     */
    public function __construct($fieldName, $file, $filename = null)
    {
        $this->name = $fieldName;

        if (is_resource($file)) {
            $this->fileHandle = $file;
            $path             = stream_get_meta_data($file)['uri'];
        } else {
            if (!file_exists($file)) {
                throw new \InvalidArgumentException('The file does\'t exists: ' . $file);
            }
            $this->fileHandle = fopen($file, 'r');
            $path             = $file;
        }

        $this->filename = $filename ?: basename($path);
    }

    /**
     * Transform the object into an array
     *
     * @return array
     */
    public function toArray()
    {
        return [
            'contents' => $this->fileHandle,
            'name'     => $this->name,
            'filename' => $this->filename,
        ];
    }
}
