<?php
/**
 * Created by PhpStorm.
 * User: aaflalo
 * Date: 14/12/16
 * Time: 4:45 PM
 */

namespace ProvulusSDK\Client\Request;

use GuzzleHttp\RequestOptions;
use ProvulusSDK\Client\Args\IRequestArg;
use ProvulusSDK\Client\Args\Merge\ArgMerger;
use ProvulusSDK\Client\Args\Merge\IMergeableArgument;
use ProvulusSDK\Client\Request\Multipart\File\FileInfo;
use ProvulusSDK\Client\Request\Resource\NullableFieldsTrait;
use ProvulusSDK\Client\Request\Resource\Value\GetValueTrait;
use ProvulusSDK\Client\Request\Resource\Value\HasValue;
use ProvulusSDK\Client\Response\IProvulusResponse;
use ProvulusSDK\Utils\ReflectionUtil;
use ProvulusSDK\Utils\Str;
use Psr\Http\Message\UriInterface;

abstract class BaseProvulusRequest implements IProvulusRequest, HasNullableFields, HasValue
{
    use NullableFieldsTrait, GetValueTrait;


    /**
     * @var IRequestArg[]
     */
    private $arguments = [];

    /**
     * @var ArgMerger[]
     */
    private $mergeableArguments = [];

    /**
     * @var FileInfo[]
     */
    private $requestFiles = [];


    /**
     * @var IProvulusResponse
     */
    private $response;

    /** @var int Number of tries of this request */
    private $tries = 0;

    /**
     * @var RequestType
     */
    private $requestTypeOverride;

    /**
     * Add a request argument
     *
     * @param IRequestArg $arg
     *
     * @return $this
     */
    public function addArgument(IRequestArg $arg)
    {
        if ($arg instanceof IMergeableArgument) {
            if (!isset($this->mergeableArguments[$arg->getKey()])) {
                $this->mergeableArguments[$arg->getKey()] = new ArgMerger();
            }

            $this->mergeableArguments[$arg->getKey()]->addArgument($arg);

            return $this;
        }

        $this->arguments[$arg->getKey()] = $arg;

        return $this;
    }

    /**
     * Remove a request argument
     *
     * @param IRequestArg $arg
     *
     * @return $this
     */
    public function removeArgument(IRequestArg $arg)
    {
        if ($arg instanceof IMergeableArgument) {
            if (!isset($this->mergeableArguments[$arg->getKey()])) {
                throw new \InvalidArgumentException("This argument doesn't exists");
            }
            $this->mergeableArguments[$arg->getKey()]->removeArgument($arg);

            if ($this->mergeableArguments[$arg->getKey()]->isEmpty()) {
                unset($this->mergeableArguments[$arg->getKey()]);
            }
        } else {
            unset($this->arguments[$arg->getKey()]);
        }

        return $this;
    }

    /**
     * Get all the request arguments
     *
     * @return IRequestArg[]
     */
    public function getArguments()
    {
        return $this->arguments;
    }

    /**
     * Get the argument that can be merged with each other
     *
     * @return ArgMerger[]
     */
    public function getMergeableArguments()
    {
        return $this->mergeableArguments;
    }


    /**
     * Get the data used for the route
     *
     * @return array
     */
    public function toArray()
    {
        return ReflectionUtil::objToSnakeArray($this, $this->ignoredProperties());
    }

    /**
     * List of properties to not serialize
     *
     * @return array
     */
    protected function ignoredProperties()
    {
        static $ignored = [
            'arguments',
            'response',
            'mergeableArguments',
            'reflectionProperties',
            'nullableChanged',
            'tries',
            'requestFiles',
            'requestTypeOverride',
        ];

        return $ignored;
    }

    /**
     * Return the URI of the request
     *
     * @return UriInterface
     */
    public function toUri()
    {
        return \GuzzleHttp\Psr7\uri_for($this->routeUrl());
    }

    /**
     * Request type to use for the request
     *
     * @return RequestType
     */
    final public function requestType()
    {
        return $this->requestTypeOverride ?: $this->httpType();
    }


    /**
     * Options for this request to be used by the client
     *
     * @return array
     */
    public function requestOptions()
    {
        $query = [];
        /**
         * @var $value IRequestArg
         */
        foreach ($this->arguments as $key => $value) {
            $query[$key] = $value->toPrimitive();
        }

        foreach ($this->mergeableArguments as $key => $value) {
            $query[$key] = $value->toPrimitive();
        }


        $options        = [
            RequestOptions::QUERY => $query,
        ];
        $requestContent = $this->toArray();

        if (!empty($this->requestFiles)) {
            $options[RequestOptions::MULTIPART] = [];

            // Forms don't support other methods than GET and POST we need to use the _method to emulate a PATH or PUT
            if ($this->httpType()->is(RequestType::HTTP_PUT) || $this->httpType()->is(RequestType::HTTP_PATCH)) {
                $options[RequestOptions::MULTIPART][] = [
                    'name'     => '_method',
                    'contents' => Str::lower($this->httpType()->getValue()),
                ];
                $this->requestTypeOverride            = RequestType::HTTP_POST();
            }

            foreach ($requestContent as $field => $value) {
                $options[RequestOptions::MULTIPART][] = [
                    'name'     => $field,
                    'contents' => $value,
                ];
            }

            $options[RequestOptions::MULTIPART] = array_merge(
                $options[RequestOptions::MULTIPART],
                array_map(
                    function (FileInfo $file) {
                        return $file->toArray();
                    },
                    $this->requestFiles
                )
            );
        } else {
            $options[RequestOptions::JSON] = $requestContent;
        }

        return $options;
    }

    /**
     * Add a file to the request
     *
     * @param  string|resource $filePath  full path to the file or a resource (@see fopen)
     * @param  string          $fieldName $fieldName name of the field that file should be send with. If null, will
     *                                    take it from the name of the method
     * @param string           $filename  name of the file with extension. Optional.
     */
    public function addFile($filePath, $fieldName = null, $filename = null)
    {
        if (!$fieldName) {
            $function  = debug_backtrace()[1]['function'];
            $fieldName = Str::snake(lcfirst(substr($function, 3)));
        }
        $this->requestFiles[] = new FileInfo($fieldName, $filePath, $filename);
    }

    /**
     * Getter for response
     *
     * @return IProvulusResponse
     */
    public function getResponse()
    {
        return $this->response;
    }

    /**
     * @param IProvulusResponse $response
     */
    public function setResponse($response)
    {
        $this->response = $response;
    }

    /**
     * Number of tries of the request
     *
     * @return int
     */
    public function tries()
    {
        return $this->tries;
    }

    /**
     * Increment the number of tries and returns it
     *
     * @return int
     */
    public function incrementTries()
    {
        return ++$this->tries;
    }
}
