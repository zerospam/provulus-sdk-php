<?php
/**
 * Created by PhpStorm.
 * User: ycoutu
 * Date: 26/10/18
 * Time: 2:56 PM
 */

namespace ProvulusSDK\Client\Request\User\Summary;

use ProvulusSDK\Client\Request\BindableRequest;
use ProvulusSDK\Client\Request\RequestType;
use ProvulusSDK\Client\Response\IProvulusResponse;
use ProvulusSDK\Client\Response\User\Summary\UserSummaryResponse;

/**
 * Class ReadUserConnectionInfoRequest
 *
 * @method UserSummaryResponse getResponse()
 *
 * @package ProvulusSDK\Client\Request\User\Connection
 */
class ReadUserSummaryRequest extends BindableRequest
{

    /**
     * Base route without binding
     *
     * @return string
     * @throws \Exception
     */
    function baseRoute()
    {
        if (!$this->hasBinding('userId')) {
            throw new \Exception(sprintf('User ID needs to be set for %s', get_class($this)));
        }
        return 'users/:userId/summary';
    }

    /**
     * Process the data that is in the response
     *
     * @param array $jsonResponse
     *
     * @return IProvulusResponse
     */
    public function processResponse(array $jsonResponse)
    {
        $included = isset($jsonResponse['included'])
            ? $jsonResponse['included']
            : [];

        return new UserSummaryResponse($jsonResponse['data'], $included);
    }

    public function setUserId($id)
    {
        $this->addBinding('userId', $id);

        return $this;
    }

    /**
     * Type of request
     *
     * @return RequestType
     */
    public function httpType()
    {
        return RequestType::HTTP_GET();
    }
}
