<?php
/**
 * Created by PhpStorm.
 * User: ycoutu
 * Date: 29/10/18
 * Time: 9:36 AM
 */

namespace ProvulusSDK\Client\Request\User\Summary;

use ProvulusSDK\Client\Request\BaseProvulusRequest;
use ProvulusSDK\Client\Request\RequestType;
use ProvulusSDK\Client\Response\IProvulusResponse;
use ProvulusSDK\Client\Response\User\Summary\UserSummaryResponse;

/**
 * Class ReadAnonymousConnectionInfoRequest
 *
 * @method UserSummaryResponse getResponse()
 *
 * @package ProvulusSDK\Client\Request\User\Connection
 */
class ReadAnonymousSummaryRequest extends BaseProvulusRequest
{

    /**
     * The url of the route
     *
     * @return string
     */
    public function routeUrl()
    {
        return 'anonymous';
    }

    /**
     * Type of request
     *
     * @return RequestType
     */
    public function httpType()
    {
        return RequestType::HTTP_GET();
    }

    /**
     * Process the data that is in the response
     *
     * @param array $jsonResponse
     *
     * @return IProvulusResponse
     */
    public function processResponse(array $jsonResponse)
    {
        $included = isset($jsonResponse['included'])
            ? $jsonResponse['included']
            : [];

        return new UserSummaryResponse($jsonResponse['data'], $included);
    }
}
