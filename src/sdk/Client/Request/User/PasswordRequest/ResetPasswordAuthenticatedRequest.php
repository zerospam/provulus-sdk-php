<?php
/**
 * Created by PhpStorm.
 * User: aaflalo
 * Date: 07/06/17
 * Time: 9:55 AM
 */

namespace ProvulusSDK\Client\Request\User\PasswordRequest;

use ProvulusSDK\Client\Request\EmptyResponseTrait;
use ProvulusSDK\Client\Request\RequestType;
use ProvulusSDK\Client\Request\Resource\User\AllUsers\UserAllRequest;

/**
 * Class ResetPasswordAuthenticatedRequest
 *
 * Request to reset the password of the given user
 *
 * @package ProvulusSDK\Client\Request\User\PasswordRequest
 */
class ResetPasswordAuthenticatedRequest extends UserAllRequest
{
    use EmptyResponseTrait;

    /**
     * Type of request
     *
     * @return RequestType
     */
    public function httpType()
    {
        return RequestType::HTTP_POST();
    }

    function baseRoute()
    {
        return parent::baseRoute() . '/reset';
    }
}
