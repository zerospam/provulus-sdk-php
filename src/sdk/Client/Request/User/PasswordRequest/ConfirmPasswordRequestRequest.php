<?php
/**
 * Created by PhpStorm.
 * User: pbb
 * Date: 26/04/17
 * Time: 2:46 PM
 */

namespace ProvulusSDK\Client\Request\User\PasswordRequest;

use ProvulusSDK\Client\Request\BaseProvulusRequest;
use ProvulusSDK\Client\Request\RequestType;
use ProvulusSDK\Client\Response\IProvulusResponse;
use ProvulusSDK\Client\Response\User\Summary\UserSummaryResponse;

/**
 * Class ConfirmPasswordRequestRequest
 *
 * Request to confirm a password request token
 * @method UserSummaryResponse getResponse()
 *
 * @package ProvulusSDK\Client\Request\User\PasswordRequest
 */
class ConfirmPasswordRequestRequest extends BaseProvulusRequest
{
    /**
     * @var string
     */
    private $token;

    /** @var string */
    private $password;

    /**
     * The url of the route
     *
     * @return string
     */
    public function routeUrl()
    {
        return 'confirm/password';
    }

    /**
     * Type of request
     *
     * @return RequestType
     */
    public function httpType()
    {
        return RequestType::HTTP_POST();
    }

    /**
     * Set needed token for the UserEmailAddress confirmation
     *
     * @param string $token
     *
     * @return $this
     */
    public function setToken($token)
    {
        $this->token = $token;

        return $this;
    }

    /**
     * @param string $password
     *
     * @return ConfirmPasswordRequestRequest
     */
    public function setPassword($password)
    {
        $this->password = $password;

        return $this;
    }

    /**
     * Process the data that is in the response
     * With included relationship objects if needed
     *
     * @param array $jsonResponse
     *
     * @return IProvulusResponse
     */
    public function processResponse(array $jsonResponse)
    {
        $included = isset($jsonResponse['included'])
            ? $jsonResponse['included']
            : [];

        return new UserSummaryResponse($jsonResponse['data'], $included);
    }
}