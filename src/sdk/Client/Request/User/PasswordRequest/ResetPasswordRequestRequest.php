<?php
/**
 * Created by PhpStorm.
 * User: pbb
 * Date: 19/04/17
 * Time: 2:41 PM
 */

namespace ProvulusSDK\Client\Request\User\PasswordRequest;


use ProvulusSDK\Client\Request\BaseProvulusRequest;
use ProvulusSDK\Client\Request\EmptyResponseTrait;
use ProvulusSDK\Client\Request\RequestType;

/**
 * Class ResetPasswordRequestRequest
 *
 * @package ProvulusSDK\Client\Request\User\PasswordRequest
 */
class ResetPasswordRequestRequest extends BaseProvulusRequest
{
    use EmptyResponseTrait;

    /** @var string */
    private $username;

    /**
     * Type of request
     *
     * @return RequestType
     */
    public function httpType()
    {
        return RequestType::HTTP_POST();
    }

    /**
     * The url of the route
     *
     * @return string
     */
    public function routeUrl()
    {
        return 'reset/password';
    }

    /**
     * @param mixed $username
     *
     * @return ResetPasswordRequestRequest
     */
    public function setUsername($username)
    {
        $this->username = $username;

        return $this;
    }
}