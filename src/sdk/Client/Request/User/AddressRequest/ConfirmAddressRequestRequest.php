<?php
/**
 * Created by PhpStorm.
 * User: pbb
 * Date: 16/03/17
 * Time: 11:08 AM
 */

namespace ProvulusSDK\Client\Request\User\AddressRequest;


use ProvulusSDK\Client\Request\BaseProvulusRequest;
use ProvulusSDK\Client\Request\EmptyResponseTrait;
use ProvulusSDK\Client\Request\RequestType;

/**
 * Class ConfirmAddressRequestRequest
 *
 * Request to confirm a requested UserEmailAddress
 *
 * @package ProvulusSDK\Client\Request\User\AddressRequest
 */
class ConfirmAddressRequestRequest extends BaseProvulusRequest
{
    use EmptyResponseTrait;

    /**
     * @var string
     */
    private $token;

    /**
     * The url of the route
     *
     * @return string
     */
    public function routeUrl()
    {
        return 'confirm/email';
    }

    /**
     * Type of request
     *
     * @return RequestType
     */
    public function httpType()
    {
        return RequestType::HTTP_POST();
    }

    /**
     * Set needed token for the UserEmailAddress confirmation
     *
     * @param string $token
     *
     * @return $this
     */
    public function setToken($token)
    {
        $this->token = $token;

        return $this;
    }
}