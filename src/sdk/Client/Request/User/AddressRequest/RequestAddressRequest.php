<?php
/**
 * Created by PhpStorm.
 * User: pbb
 * Date: 13/03/17
 * Time: 10:07 AM
 */

namespace ProvulusSDK\Client\Request\User\AddressRequest;


use ProvulusSDK\Client\Request\EmptyResponseTrait;
use ProvulusSDK\Client\Request\RequestType;
use ProvulusSDK\Client\Request\Resource\User\AllUsers\UserAllRequest;

/**
 * Class CreateAddressRequest
 *
 * @package ProvulusSDK\Client\Request\User\AddressRequest
 */
class RequestAddressRequest extends UserAllRequest
{

    use EmptyResponseTrait;

    /**
     * @var string
     */
    private $emailAddress;

    /**
     * Type of request
     *
     * @return RequestType
     */
    public function httpType()
    {
        return RequestType::HTTP_POST();
    }

    /**
     * @param mixed $emailAddress
     *
     * @return RequestAddressRequest
     */
    public function setEmailAddress($emailAddress)
    {
        $this->emailAddress = $emailAddress;

        return $this;
    }

    /**
     * @return string
     */
    function baseRoute()
    {
        return parent::baseRoute() . '/requestAddress';
    }


}