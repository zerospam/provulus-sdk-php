<?php
/**
 * Created by PhpStorm.
 * User: pbb
 * Date: 28/02/17
 * Time: 3:28 PM
 */

namespace ProvulusSDK\Client\Request\User\Traits;

use ProvulusSDK\Client\Args\Relationship\IncludeRelationArg;

/**
 * Class withEmailAddresses
 *
 * Add request parameter to include addresses relationship
 *
 * @package ProvulusSDK\Client\Request\User\Traits
 */
trait WithEmailAddresses
{
    /**
     * Include the related userEmailAddresses in the result
     *
     * @return $this
     */
    public function withEmailAddresses()
    {
        return $this->addArgument(new IncludeRelationArg('addresses'));

    }

    /**
     * Remove related userEmailAddresses from the results
     *
     * @return $this
     */
    public function withoutEmailAddresses()
    {
        return $this->removeArgument(new IncludeRelationArg('addresses'));
    }

}