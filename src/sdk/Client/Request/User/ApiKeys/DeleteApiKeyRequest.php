<?php
/**
 * Created by PhpStorm.
 * User: pbb
 * Date: 30/01/17
 * Time: 2:57 PM
 */

namespace ProvulusSDK\Client\Request\User\ApiKeys;

use ProvulusSDK\Client\Request\EmptyResponseTrait;
use ProvulusSDK\Client\Request\RequestType;
use ProvulusSDK\Client\Request\Resource\User\ApiKey\ApiKeyRequest;

/**
 * Class DeleteApiKeyRequest
 *
 * Delete request for api key
 *
 * @package ProvulusSDK\Client\Request\User\ApiKeys
 */
class DeleteApiKeyRequest extends ApiKeyRequest
{

    use EmptyResponseTrait;

    /**
     * Type of request
     *
     * @return RequestType
     */
    public function httpType()
    {
        return RequestType::HTTP_DELETE();
    }
}