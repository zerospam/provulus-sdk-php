<?php
/**
 * Created by PhpStorm.
 * User: pbb
 * Date: 30/01/17
 * Time: 2:49 PM
 */

namespace ProvulusSDK\Client\Request\User\ApiKeys;

use ProvulusSDK\Client\Request\Collection\CollectionPaginatedTrait;
use ProvulusSDK\Client\Request\Collection\IProvulusCollectionPaginatedRequest;
use ProvulusSDK\Client\Request\RequestType;
use ProvulusSDK\Client\Request\Resource\User\ApiKey\BaseApiKeyRequest;
use ProvulusSDK\Client\Response\IProvulusObjectResponse;
use ProvulusSDK\Client\Response\User\ApiKey\ApiKeyCollectionResponse;
use ProvulusSDK\Client\Response\User\ApiKey\ApiKeyResponse;

/**
 * Class IndexApiKeyRequest
 *
 * Gets all api keys for user
 * @method ApiKeyCollectionResponse getResponse()
 *
 * @package ProvulusSDK\Client\Request\User\ApiKeys
 */
class IndexApiKeyRequest extends BaseApiKeyRequest implements IProvulusCollectionPaginatedRequest
{
    use CollectionPaginatedTrait;


    /**
     * Used to parse each result of the collection
     *
     * Need to be overridden by the class
     *
     * @param array $array
     *
     * @return IProvulusObjectResponse
     */
    function toResponse(array $array)
    {
        return new ApiKeyResponse($array);
    }

    /**
     * Class to be used to build the collection
     *
     * @return string
     */
    function collectionClass()
    {
        return ApiKeyCollectionResponse::class;
    }

    /**
     * Type of request
     *
     * @return RequestType
     */
    public function httpType()
    {
        return RequestType::HTTP_GET();
    }
}