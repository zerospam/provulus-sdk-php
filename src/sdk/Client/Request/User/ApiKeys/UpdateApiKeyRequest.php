<?php
/**
 * Created by PhpStorm.
 * User: pbb
 * Date: 30/01/17
 * Time: 2:56 PM
 */

namespace ProvulusSDK\Client\Request\User\ApiKeys;

use ProvulusSDK\Client\Request\RequestType;
use ProvulusSDK\Client\Request\Resource\User\ApiKey\ApiKeyProcessResponseTrait;
use ProvulusSDK\Client\Request\Resource\User\ApiKey\ApiKeyRequest;
use ProvulusSDK\Client\Request\Resource\User\ApiKey\ApiKeySetAttributesTrait;

/**
 * Class UpdateApiKeyRequest
 *
 * Request for api key update
 *
 * @package ProvulusSDK\Client\Request\User\ApiKeys
 */
class UpdateApiKeyRequest extends ApiKeyRequest
{
    use ApiKeyProcessResponseTrait, ApiKeySetAttributesTrait;

    /**
     * Type of request
     *
     * @return RequestType
     */
    public function httpType()
    {
        return RequestType::HTTP_PUT();
    }
}