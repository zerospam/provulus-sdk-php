<?php
/**
 * Created by PhpStorm.
 * User: pbb
 * Date: 30/01/17
 * Time: 2:48 PM
 */

namespace ProvulusSDK\Client\Request\User\ApiKeys;

use ProvulusSDK\Client\Request\RequestType;
use ProvulusSDK\Client\Request\Resource\User\ApiKey\ApiKeyProcessResponseTrait;
use ProvulusSDK\Client\Request\Resource\User\ApiKey\ApiKeySetAttributesTrait;
use ProvulusSDK\Client\Request\Resource\User\ApiKey\BaseApiKeyRequest;

/**
 * Class CreateApiKeyRequest
 *
 * Request to create api key for user
 *
 * @package ProvulusSDK\Client\Request\User\ApiKeys
 */
class CreateApiKeyRequest extends BaseApiKeyRequest
{
    use ApiKeyProcessResponseTrait, ApiKeySetAttributesTrait;


    /**
     * Type of request
     *
     * @return RequestType
     */
    public function httpType()
    {
        return RequestType::HTTP_POST();
    }
}