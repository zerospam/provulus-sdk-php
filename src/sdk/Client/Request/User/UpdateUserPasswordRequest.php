<?php
/**
 * Created by PhpStorm.
 * User: pbb
 * Date: 24/05/17
 * Time: 3:49 PM
 */

namespace ProvulusSDK\Client\Request\User;


use ProvulusSDK\Client\Request\EmptyResponseTrait;
use ProvulusSDK\Client\Request\RequestType;
use ProvulusSDK\Client\Request\Resource\User\AllUsers\UserAllRequest;

/**
 * Class UpdateUserPasswordRequest
 *
 * @package ProvulusSDK\Client\Request\User
 */
class UpdateUserPasswordRequest extends UserAllRequest
{
    use EmptyResponseTrait;

    /** @var string */
    private $currentPassword;

    /** @var string */
    private $newPassword;

    /** @var string */
    private $newPasswordConfirm;

    /** @var string */
    private $tfaCode;

    /**
     * Type of request
     *
     * @return RequestType
     */
    public function httpType()
    {
        return RequestType::HTTP_PATCH();
    }

    /**
     * @param string $currentPassword
     *
     * @return UpdateUserPasswordRequest
     */
    public function setCurrentPassword($currentPassword)
    {
        $this->currentPassword = $currentPassword;

        return $this;
    }

    /**
     * @param string $newPassword
     *
     * @return UpdateUserPasswordRequest
     */
    public function setNewPassword($newPassword)
    {
        $this->newPassword = $newPassword;

        return $this;
    }

    /**
     * @param string $newPasswordConfirm
     *
     * @return UpdateUserPasswordRequest
     */
    public function setNewPasswordConfirm($newPasswordConfirm)
    {
        $this->newPasswordConfirm = $newPasswordConfirm;

        return $this;
    }

    /**
     * @param string $tfaCode
     *
     * @return UpdateUserPasswordRequest
     */
    public function setTfaCode($tfaCode)
    {
        $this->tfaCode = $tfaCode;

        return $this;
    }

    /**
     * @return string
     */
    function baseRoute()
    {
        return parent::baseRoute() . '/updatePassword';
    }


}