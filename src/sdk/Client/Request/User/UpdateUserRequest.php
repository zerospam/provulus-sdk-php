<?php
/**
 * Created by PhpStorm.
 * User: pbb
 * Date: 05/01/17
 * Time: 9:04 AM
 */

namespace ProvulusSDK\Client\Request\User;


use ProvulusSDK\Client\Request\RequestType;
use ProvulusSDK\Client\Request\Resource\User\TypedUsers\UserRequestType;
use ProvulusSDK\Client\Request\Resource\User\TypedUsers\UserTypedRequest;
use ProvulusSDK\Client\Request\Resource\User\UserProcessResponseTrait;
use ProvulusSDK\Client\Request\Resource\User\UserSetAllAttributesTrait;
use ProvulusSDK\Client\Response\User\UserResponse;

/**
 * Class UpdateUserRequest
 *
 * Update user's information
 *
 * @package ProvulusSDK\Client\Request\User
 *
 * @method  UserResponse getResponse()
 */
class UpdateUserRequest extends UserTypedRequest
{

    use UserSetAllAttributesTrait, UserProcessResponseTrait;

    /**
     * @var string
     */
    private $dailyDigestEmail;

    /**
     * Type of request
     *
     * @return RequestType
     */
    public function httpType()
    {
        return RequestType::HTTP_PUT();
    }

    /**
     * Sets user request Type
     *
     * @param UserRequestType $userRequestType
     *
     * @return $this
     */
    public function setUserRequestType(UserRequestType $userRequestType)
    {
        if ($userRequestType->is(UserRequestType::ALL())) {
            throw new \InvalidArgumentException(sprintf('Cannot update a user on users path; use %s or %s',
                UserRequestType::REGULAR()->getValue(), UserRequestType::ADMIN()->getValue()));
        }

        $this->addBinding('userType', $userRequestType->getValue());

        return $this;
    }

    /**
     * @param string $dailyDigestEmail
     *
     * @return UpdateUserRequest
     */
    public function setDailyDigestEmail($dailyDigestEmail)
    {
        $this->dailyDigestEmail = $dailyDigestEmail;
        $this->nullableChanged();

        return $this;
    }

}