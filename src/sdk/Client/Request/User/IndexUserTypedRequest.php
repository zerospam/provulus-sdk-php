<?php
/**
 * Created by PhpStorm.
 * User: pbb
 * Date: 03/01/17
 * Time: 3:51 PM
 */

namespace ProvulusSDK\Client\Request\User;


use ProvulusSDK\Client\Args\Search\User\UserSearchTrait;
use ProvulusSDK\Client\Request\Collection\CollectionPaginatedTrait;
use ProvulusSDK\Client\Request\Collection\IProvulusCollectionPaginatedRequest;
use ProvulusSDK\Client\Request\Collection\SearchableTrait;
use ProvulusSDK\Client\Request\RequestType;
use ProvulusSDK\Client\Request\Resource\User\TypedUsers\BaseUserTypedRequest;
use ProvulusSDK\Client\Request\User\Session\WithLastSessionTrait;
use ProvulusSDK\Client\Request\User\Traits\WithEmailAddresses;
use ProvulusSDK\Client\Response\IProvulusObjectResponse;
use ProvulusSDK\Client\Response\User\UserCollectionResponse;
use ProvulusSDK\Client\Response\User\UserResponse;

/**
 * Class IndexUserRequest
 *
 * Request for retrieving all users of an organization
 *
 * @method UserCollectionResponse getResponse()
 * @package ProvulusSDK\Client\Request\User
 */
class IndexUserTypedRequest extends BaseUserTypedRequest implements IProvulusCollectionPaginatedRequest
{
    use CollectionPaginatedTrait,
        SearchableTrait,
        WithEmailAddresses,
        WithLastSessionTrait,
        UserSearchTrait;

    /**
     * Type of request
     *
     * @return RequestType
     */
    public function httpType()
    {
        return RequestType::HTTP_GET();
    }

    /**
     * Used to parse each result of the collection
     *
     * Need to be overridden by the class
     *
     * @param array $array
     * @param array $includedData
     *
     * @return IProvulusObjectResponse
     */
    function toResponse(array $array, array $includedData = [])
    {
        return new UserResponse($array, $includedData);
    }

    /**
     * Class to be used to build the collection
     *
     * @return string
     */
    function collectionClass()
    {
        return UserCollectionResponse::class;
    }
}