<?php
/**
 * Created by PhpStorm.
 * User: pbb
 * Date: 04/01/17
 * Time: 11:09 AM
 */

namespace ProvulusSDK\Client\Request\User;

use ProvulusSDK\Client\Request\RequestType;
use ProvulusSDK\Client\Request\Resource\User\TypedUsers\BaseUserTypedRequest;
use ProvulusSDK\Client\Request\Resource\User\TypedUsers\UserRequestType;
use ProvulusSDK\Client\Request\Resource\User\UserProcessResponseTrait;
use ProvulusSDK\Client\Request\Resource\User\UserSetAllAttributesTrait;
use ProvulusSDK\Client\Response\User\UserResponse;

/**
 * Class CreateUserRequest
 *
 * Creates a user for an organization.
 *
 * @package ProvulusSDK\Client\Request\User
 *
 * @method  UserResponse getResponse()
 */
class CreateUserTypedRequest extends BaseUserTypedRequest
{
    use UserSetAllAttributesTrait, UserProcessResponseTrait;

    /**
     * @var bool
     */
    private $isDailyDigest;

    /**
     * Type of request
     *
     * @return RequestType
     */
    public function httpType()
    {
        return RequestType::HTTP_POST();
    }

    /**
     * When creating a user, need to use the admin or regular route
     *
     * @param UserRequestType $userRequestType
     *
     * @throws \InvalidArgumentException
     * @return CreateUserTypedRequest|BaseUserTypedRequest
     */
    public function setUserRequestType(UserRequestType $userRequestType)
    {
        if ($userRequestType->is(UserRequestType::ALL())) {
            throw new \InvalidArgumentException('User type cannot be all for this type of request');
        }

        return parent::setUserRequestType($userRequestType);
    }

    /**
     * @param bool $isDailyDigest
     *
     * @return $this
     */
    public function setIsDailyDigest($isDailyDigest)
    {
        $this->isDailyDigest = $isDailyDigest;

        return $this;
    }
}