<?php
/**
 * Created by PhpStorm.
 * User: pbb
 * Date: 04/01/17
 * Time: 11:01 AM
 */

namespace ProvulusSDK\Client\Request\User;


use ProvulusSDK\Client\Request\EmptyResponseTrait;
use ProvulusSDK\Client\Request\RequestType;
use ProvulusSDK\Client\Request\Resource\User\TypedUsers\UserTypedRequest;

/**
 * Class DeleteUserRequest
 *
 * Request to delete user from organization
 *
 * @package ProvulusSDK\Client\Request\User
 */
class DeleteUserRequest extends UserTypedRequest
{

    use EmptyResponseTrait;

    /**
     * Type of request
     *
     * @return RequestType
     */
    public function httpType()
    {
        return RequestType::HTTP_DELETE();
    }
}