<?php
/**
 * Created by PhpStorm.
 * User: ycoutu
 * Date: 21/09/17
 * Time: 10:39 AM
 */

namespace ProvulusSDK\Client\Request\User;

use ProvulusSDK\Client\Request\RequestType;
use ProvulusSDK\Client\Request\Resource\User\TypedUsers\UserTypedRequest;
use ProvulusSDK\Client\Request\Resource\User\UserProcessResponseTrait;

class DemoteUserRequest extends UserTypedRequest
{
    use UserProcessResponseTrait;

    /**
     * Type of request
     *
     * @return RequestType
     */
    public function httpType()
    {
        return RequestType::HTTP_POST();
    }

    public function baseRoute()
    {
        return parent::baseRoute() . '/demote';
    }
}
