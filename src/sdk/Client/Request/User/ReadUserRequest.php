<?php
/**
 * Created by PhpStorm.
 * User: pbb
 * Date: 04/01/17
 * Time: 10:19 AM
 */

namespace ProvulusSDK\Client\Request\User;


use ProvulusSDK\Client\Request\RequestType;
use ProvulusSDK\Client\Request\Resource\User\TypedUsers\UserTypedRequest;
use ProvulusSDK\Client\Request\Resource\User\UserProcessResponseTrait;
use ProvulusSDK\Client\Request\User\Traits\WithEmailAddresses;
use ProvulusSDK\Client\Response\User\UserResponse;

/**
 * Class ReadUserRequest
 *
 * Request to read user data from organization
 *
 * @package ProvulusSDK\Client\Request\User
 *
 * @method  UserResponse getResponse()
 */
class ReadUserRequest extends UserTypedRequest
{

    use UserProcessResponseTrait, WithEmailAddresses;

    /**
     * Type of request
     *
     * @return RequestType
     */
    public function httpType()
    {
        return RequestType::HTTP_GET();
    }
}