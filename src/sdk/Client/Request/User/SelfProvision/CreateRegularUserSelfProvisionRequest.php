<?php
/**
 * Created by PhpStorm.
 * User: pbb
 * Date: 16/05/17
 * Time: 3:44 PM
 */

namespace ProvulusSDK\Client\Request\User\SelfProvision;

use ProvulusSDK\Client\Request\EmptyResponseTrait;
use ProvulusSDK\Client\Request\RequestType;
use ProvulusSDK\Client\Request\Resource\Organization\OrganizationRequest;
use ProvulusSDK\Client\Request\Resource\User\UserSetBasicAttributesTrait;

/**
 * Class CreateRegularUserSelfProvisionRequest
 *
 * Request a new regular user account by self-provision
 *
 * @package ProvulusSDK\Client\Request\User\SelfProvision
 */
class CreateRegularUserSelfProvisionRequest extends OrganizationRequest
{

    use EmptyResponseTrait, UserSetBasicAttributesTrait;

    /**
     * @var bool
     */
    private $isDailyDigest;

    /** @var string */
    private $password;

    /**
     * The url of the route
     *
     * @return string
     */
    public function baseRoute()
    {
        return parent::baseRoute() . '/users/selfprov/regular';
    }

    /**
     * Type of request
     *
     * @return RequestType
     */
    public function httpType()
    {
        return RequestType::HTTP_POST();
    }

    /**
     * @param bool $isDailyDigest
     *
     * @return CreateRegularUserSelfProvisionRequest
     */
    public function setIsDailyDigest($isDailyDigest)
    {
        $this->isDailyDigest = $isDailyDigest;

        return $this;
    }

    /**
     * @param string $password
     *
     * @return CreateRegularUserSelfProvisionRequest
     */
    public function setPassword($password)
    {
        $this->password = $password;

        return $this;
    }

}