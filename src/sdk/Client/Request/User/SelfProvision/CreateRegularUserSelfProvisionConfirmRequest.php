<?php
/**
 * Created by PhpStorm.
 * User: pbb
 * Date: 16/05/17
 * Time: 3:47 PM
 */

namespace ProvulusSDK\Client\Request\User\SelfProvision;


use ProvulusSDK\Client\Request\BaseProvulusRequest;
use ProvulusSDK\Client\Request\RequestType;
use ProvulusSDK\Client\Request\Resource\User\UserProcessResponseTrait;

/**
 * Class CreateRegularUserSelfProvisionConfirmRequest
 *
 * Confirmation request for regular user self provisioning
 *
 * @method UserResponse getResponse()
 *
 * @package ProvulusSDK\Client\Request\User\SelfProvision
 */
class CreateRegularUserSelfProvisionConfirmRequest extends BaseProvulusRequest
{

    use UserProcessResponseTrait;

    /** @var string $token */
    private $token;

    /**
     * The url of the route
     *
     * @return string
     */
    public function routeUrl()
    {
        return 'confirm/selfprov/regular';
    }

    /**
     * Type of request
     *
     * @return RequestType
     */
    public function httpType()
    {
        return RequestType::HTTP_POST();
    }

    /**
     * @param string $token
     *
     * @return CreateRegularUserSelfProvisionConfirmRequest
     */
    public function setToken($token)
    {
        $this->token = $token;

        return $this;
    }
}