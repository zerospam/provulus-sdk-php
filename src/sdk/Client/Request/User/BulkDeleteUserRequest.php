<?php
/**
 * Created by PhpStorm.
 * User: pbb
 * Date: 30/08/17
 * Time: 4:47 PM
 */

namespace ProvulusSDK\Client\Request\User;

use ProvulusSDK\Client\Request\BulkRequestTrait;
use ProvulusSDK\Client\Request\EmptyResponseTrait;
use ProvulusSDK\Client\Request\RequestType;
use ProvulusSDK\Client\Request\Resource\User\AllUsers\UsersAllBaseRequest;

/**
 * Class BulkDeleteUserRequest
 *
 * @package ProvulusSDK\Client\Request\User
 */
class BulkDeleteUserRequest extends UsersAllBaseRequest
{
    use EmptyResponseTrait,
        BulkRequestTrait;

    /**
     * Type of request
     *
     * @return RequestType
     */
    public function httpType()
    {
        return RequestType::HTTP_DELETE();
    }

}