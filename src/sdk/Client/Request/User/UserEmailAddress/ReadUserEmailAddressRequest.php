<?php
/**
 * Created by PhpStorm.
 * User: pbb
 * Date: 28/02/17
 * Time: 4:14 PM
 */

namespace ProvulusSDK\Client\Request\User\UserEmailAddress;


use ProvulusSDK\Client\Request\RequestType;
use ProvulusSDK\Client\Request\Resource\User\UserEmailAddress\UserEmailAddressRequest;
use ProvulusSDK\Client\Request\Resource\User\UserProcessResponseTrait;


/**
 * Class ReadEmailUserAddressRequest
 *
 * @package ProvulusSDK\Client\Request\User\UserEmailAddress
 */
class ReadUserEmailAddressRequest extends UserEmailAddressRequest
{

    use UserProcessResponseTrait;

    /**
     * Type of request
     *
     * @return RequestType
     */
    public function httpType()
    {
        return RequestType::HTTP_GET();
    }
}