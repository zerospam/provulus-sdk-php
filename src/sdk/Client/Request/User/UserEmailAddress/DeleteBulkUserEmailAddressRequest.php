<?php
/**
 * Created by PhpStorm.
 * User: pbb
 * Date: 27/04/17
 * Time: 2:08 PM
 */

namespace ProvulusSDK\Client\Request\User\UserEmailAddress;

use ProvulusSDK\Client\Request\BulkRequestTrait;
use ProvulusSDK\Client\Request\EmptyResponseTrait;
use ProvulusSDK\Client\Request\RequestType;
use ProvulusSDK\Client\Request\Resource\User\UserEmailAddress\BaseUserEmailAddressRequest;

/**
 * Class DeleteBulkUserEmailAddressRequest
 *
 * @package ProvulusSDK\Client\Request\User\UserEmailAddress
 */
class DeleteBulkUserEmailAddressRequest extends BaseUserEmailAddressRequest
{

    use EmptyResponseTrait,
        BulkRequestTrait;

    /**
     * Type of request
     *
     * @return RequestType
     */
    public function httpType()
    {
        return RequestType::HTTP_DELETE();
    }
}