<?php
/**
 * Created by PhpStorm.
 * User: pbb
 * Date: 28/02/17
 * Time: 4:32 PM
 */

namespace ProvulusSDK\Client\Request\User\UserEmailAddress;


use ProvulusSDK\Client\Request\Collection\CollectionPaginatedTrait;
use ProvulusSDK\Client\Request\Collection\IProvulusCollectionPaginatedRequest;
use ProvulusSDK\Client\Request\RequestType;
use ProvulusSDK\Client\Request\Resource\User\UserEmailAddress\BaseUserEmailAddressRequest;
use ProvulusSDK\Client\Response\IProvulusObjectResponse;
use ProvulusSDK\Client\Response\User\UserEmailAddress\UserEmailAddressCollectionResponse;
use ProvulusSDK\Client\Response\User\UserEmailAddress\UserEmailAddressResponse;

/**
 * Class IndexUserEmailAddressRequest
 *
 * @method UserEmailAddressCollectionResponse getResponse()
 *
 * @package ProvulusSDK\Client\Request\User\UserEmailAddress
 */
class IndexUserEmailAddressRequest extends BaseUserEmailAddressRequest implements IProvulusCollectionPaginatedRequest
{
    use CollectionPaginatedTrait;

    /**
     * Type of request
     *
     * @return RequestType
     */
    public function httpType()
    {
        return RequestType::HTTP_GET();
    }

    /**
     * Used to parse each result of the collection
     *
     * Need to be overridden by the class
     *
     * @param array $objectData
     *
     * @param array $includedData
     *
     * @return IProvulusObjectResponse
     */
    function toResponse(array $objectData, array $includedData = [])
    {
        return new UserEmailAddressResponse($objectData, $includedData);
    }

    /**
     * Class to be used to build the collection
     *
     * @return string
     */
    function collectionClass()
    {
        return UserEmailAddressCollectionResponse::class;
    }
}