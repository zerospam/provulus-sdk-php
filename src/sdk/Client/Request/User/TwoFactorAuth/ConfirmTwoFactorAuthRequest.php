<?php
/**
 * Created by PhpStorm.
 * User: ycoutu
 * Date: 28/12/17
 * Time: 11:18 AM
 */

namespace ProvulusSDK\Client\Request\User\TwoFactorAuth;

use ProvulusSDK\Client\Request\RequestType;
use ProvulusSDK\Client\Request\Resource\User\TwoFactorAuth\TwoFactorAuthBaseRequest;
use ProvulusSDK\Client\Request\Resource\User\TwoFactorAuth\TwoFactorAuthResponseTrait;

class ConfirmTwoFactorAuthRequest extends TwoFactorAuthBaseRequest
{
    use TwoFactorAuthResponseTrait;

    /** @var string */
    private $code;

    /**
     * Type of request
     *
     * @return RequestType
     */
    public function httpType()
    {
        return RequestType::HTTP_POST();
    }

    /**
     * @param string $code
     *
     * @return $this
     */
    public function setCode($code)
    {
        $this->code = $code;

        return $this;
    }

    /**
     * @return string
     */
    function baseRoute()
    {
        return parent::baseRoute() . '/confirm';
    }
}
