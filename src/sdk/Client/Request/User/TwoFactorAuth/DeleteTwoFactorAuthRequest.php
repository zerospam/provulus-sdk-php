<?php
/**
 * Created by PhpStorm.
 * User: ycoutu
 * Date: 28/12/17
 * Time: 11:17 AM
 */

namespace ProvulusSDK\Client\Request\User\TwoFactorAuth;

use ProvulusSDK\Client\Request\EmptyResponseTrait;
use ProvulusSDK\Client\Request\RequestType;
use ProvulusSDK\Client\Request\Resource\User\TwoFactorAuth\TwoFactorAuthBaseRequest;

class DeleteTwoFactorAuthRequest extends TwoFactorAuthBaseRequest
{
    use EmptyResponseTrait;

    /**
     * Type of request
     *
     * @return RequestType
     */
    public function httpType()
    {
        return RequestType::HTTP_DELETE();
    }
}
