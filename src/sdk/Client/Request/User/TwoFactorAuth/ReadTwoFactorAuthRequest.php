<?php
/**
 * Created by PhpStorm.
 * User: ycoutu
 * Date: 28/12/17
 * Time: 11:18 AM
 */

namespace ProvulusSDK\Client\Request\User\TwoFactorAuth;

use ProvulusSDK\Client\Request\RequestType;
use ProvulusSDK\Client\Request\Resource\User\TwoFactorAuth\TwoFactorAuthBaseRequest;
use ProvulusSDK\Client\Request\Resource\User\TwoFactorAuth\TwoFactorAuthResponseTrait;

class ReadTwoFactorAuthRequest extends TwoFactorAuthBaseRequest
{
    use TwoFactorAuthResponseTrait;

    /**
     * Type of request
     *
     * @return RequestType
     */
    public function httpType()
    {
       return RequestType::HTTP_GET();
    }
}
