<?php
/**
 * Created by PhpStorm.
 * User: ycoutu
 * Date: 28/12/17
 * Time: 11:17 AM
 */

namespace ProvulusSDK\Client\Request\User\TwoFactorAuth;

use ProvulusSDK\Client\Request\RequestType;
use ProvulusSDK\Client\Request\Resource\User\TwoFactorAuth\TwoFactorAuthBaseRequest;
use ProvulusSDK\Client\Request\Resource\User\TwoFactorAuth\TwoFactorAuthResponseTrait;

class CreateTwoFactorAuthRequest extends TwoFactorAuthBaseRequest
{
    use TwoFactorAuthResponseTrait;

    /** @var bool */
    private $regenerateKey;

    /**
     * @param bool $regenerateKey
     *
     * @return CreateTwoFactorAuthRequest
     */
    public function setRegenerateKey($regenerateKey)
    {
        $this->regenerateKey = $regenerateKey;

        return $this;
    }

    /**
     * Type of request
     *
     * @return RequestType
     */
    public function httpType()
    {
        return RequestType::HTTP_POST();
    }
}
