<?php
/**
 * Created by PhpStorm.
 * User: pbb
 * Date: 21/08/17
 * Time: 2:35 PM
 */

namespace ProvulusSDK\Client\Request\User\Session;

use ProvulusSDK\Client\Args\Relationship\IncludeRelationArg;

/**
 * Trait WithLastSessionTrait
 *
 * @package ProvulusSDK\Client\Request\User\Session
 */
trait WithLastSessionTrait
{
    /**
     * Include the support organization in the result
     *
     * @return $this
     */
    public function withLastSession()
    {
        return $this->addArgument(new IncludeRelationArg('lastSession'));
    }

    /**
     * Remove the support organization from the results
     *
     * @return $this
     */
    public function withoutSupportOrganization()
    {
        return $this->removeArgument(new IncludeRelationArg('lastSession'));
    }
}