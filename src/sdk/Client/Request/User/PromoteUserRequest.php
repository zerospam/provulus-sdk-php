<?php
/**
 * Created by PhpStorm.
 * User: ycoutu
 * Date: 21/09/17
 * Time: 10:42 AM
 */

namespace ProvulusSDK\Client\Request\User;

use ProvulusSDK\Client\Request\RequestType;
use ProvulusSDK\Client\Request\Resource\User\TypedUsers\UserTypedRequest;
use ProvulusSDK\Client\Request\Resource\User\UserProcessResponseTrait;

class PromoteUserRequest extends UserTypedRequest
{
    use UserProcessResponseTrait;

    /** @var bool */
    private $isQuarantine;

    /**
     * Fluent setter for is quarantine
     *
     * @param bool $isQuarantine
     *
     * @return $this
     */
    public function setIsQuarantine($isQuarantine)
    {
        $this->isQuarantine = $isQuarantine;

        return $this;
    }

    /**
     * Type of request
     *
     * @return RequestType
     */
    public function httpType()
    {
        return RequestType::HTTP_POST();
    }

    public function baseRoute()
    {
        return parent::baseRoute() . '/promote';
    }
}