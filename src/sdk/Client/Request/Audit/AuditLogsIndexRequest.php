<?php
/**
 * Created by PhpStorm.
 * User: pbb
 * Date: 05/07/17
 * Time: 11:33 AM
 */

namespace ProvulusSDK\Client\Request\Audit;

use ProvulusSDK\Client\Args\Search\Audit\AuditLogsArgumentsTrait;
use ProvulusSDK\Client\Args\Search\Common\Date\DateRangeArgumentsTrait;
use ProvulusSDK\Client\Request\Collection\CollectionPaginatedTrait;
use ProvulusSDK\Client\Request\Collection\IProvulusCollectionPaginatedRequest;
use ProvulusSDK\Client\Request\RequestType;
use ProvulusSDK\Client\Request\Resource\Organization\OrganizationRequest;
use ProvulusSDK\Client\Response\Audit\AuditLogResponse;
use ProvulusSDK\Client\Response\Audit\AuditLogsCollectionResponse;
use ProvulusSDK\Client\Response\IProvulusObjectResponse;

/**
 * Class AuditLogsIndexRequest
 *
 * @method AuditLogsCollectionResponse getResponse()
 *
 * @package ProvulusSDK\Client\Request\Audit
 */
class AuditLogsIndexRequest extends OrganizationRequest implements IProvulusCollectionPaginatedRequest
{
    use CollectionPaginatedTrait, DateRangeArgumentsTrait, AuditLogsArgumentsTrait;

    /**
     * Base route for the audits index route
     *
     * @return string
     */
    function baseRoute()
    {
        return parent::baseRoute() . '/audits';
    }

    /**
     * Type of request
     *
     * @return RequestType
     */
    public function httpType()
    {
        return RequestType::HTTP_GET();
    }

    /**
     * Used to parse each result of the collection
     *
     * Need to be overridden by the class
     *
     * @param array $objectData
     *
     * @param array $includedData
     *
     * @return IProvulusObjectResponse
     */
    function toResponse(array $objectData, array $includedData = [])
    {
        return new AuditLogResponse($objectData, $includedData);
    }

    /**
     * Class to be used to build the collection
     *
     * @return string
     */
    function collectionClass()
    {
        return AuditLogsCollectionResponse::class;
    }
}