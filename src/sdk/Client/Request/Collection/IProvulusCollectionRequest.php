<?php
/**
 * Created by PhpStorm.
 * User: aaflalo
 * Date: 17-08-16
 * Time: 14:00
 */

namespace ProvulusSDK\Client\Request\Collection;

use ProvulusSDK\Client\Args\Sort\SortEnum;
use ProvulusSDK\Client\Response\Collection\CollectionPaginatedResponse;


/**
 * Interface IProvulusCollectionRequest
 *
 * Represent a request that interact with a Collection
 *
 * @method CollectionPaginatedResponse getResponse()
 *
 * @package ProvulusSDK\Client\Request\Collection
 */
interface IProvulusCollectionRequest
{
    /**
     * Set the order for the results
     *
     * @param  string|null $field null to remove the ordering argument
     * @param SortEnum     $enum
     *
     * @return $this
     */
    public function setOrder($field, SortEnum $enum);
}