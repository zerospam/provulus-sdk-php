<?php
/**
 * Created by PhpStorm.
 * User: aaflalo
 * Date: 23/01/17
 * Time: 11:29 AM
 */

namespace ProvulusSDK\Client\Request\Collection;


use ProvulusSDK\Client\Args\Page\PageArgument;
use ProvulusSDK\Client\Args\Page\PerPageArgument;

trait CollectionPaginatedTrait
{
    use CollectionOrderTrait;

    /**
     * Set the starting page
     *
     * @param $page int|null null to remove the page argument
     *
     * @return $this
     */
    public function setPage($page)
    {
        if (is_null($page)) {
            return $this->removeArgument(new PageArgument(-1));
        }

        return $this->addArgument(new PageArgument($page));
    }

    /**
     * Number of result per page
     *
     * @param $perPage int|null null to remove the perPage argument
     *
     * @return $this
     */
    public function setPerPage($perPage)
    {
        if (is_null($perPage)) {
            return $this->removeArgument(new PerPageArgument(-1));
        }

        return $this->addArgument(new PerPageArgument($perPage));

    }
}