<?php
/**
 * Created by PhpStorm.
 * User: aaflalo
 * Date: 17-08-16
 * Time: 14:01
 */

namespace ProvulusSDK\Client\Request\Collection;


use ProvulusSDK\Client\Args\Sort\OrderByArg;
use ProvulusSDK\Client\Args\Sort\SortedByArg;
use ProvulusSDK\Client\Args\Sort\SortEnum;

trait CollectionOrderTrait
{
    use ResponseCollectionTrait;

    /**
     * Set the order for the results
     *
     * @param  string|null $field null to remove the ordering argument
     * @param SortEnum     $enum
     *
     * @return $this
     */
    public function setOrder($field, SortEnum $enum)
    {
        if (is_null($field)) {
            return $this->removeArgument(new OrderByArg($field))->removeArgument(new SortedByArg($enum));
        }

        return $this->addArgument(new OrderByArg($field))->addArgument(new SortedByArg($enum));
    }
}