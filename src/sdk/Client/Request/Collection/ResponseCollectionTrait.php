<?php
/**
 * Created by PhpStorm.
 * User: pbb
 * Date: 27/01/17
 * Time: 11:25 AM
 */

namespace ProvulusSDK\Client\Request\Collection;

use Cake\Collection\Collection;
use ProvulusSDK\Client\Response\Collection\CollectionPaginatedResponse;
use ProvulusSDK\Client\Response\Collection\CollectionPagination;
use ProvulusSDK\Client\Response\Collection\CollectionResponse;
use ProvulusSDK\Client\Response\IProvulusObjectResponse;

/**
 * Class ResponseCollectionTrait
 *
 * @package ProvulusSDK\Client\Request\Collection
 */
trait ResponseCollectionTrait
{

    /**
     * Used to parse each result of the collection
     *
     * Need to be overridden by the class
     *
     * @param array $objectData
     *
     * @param array $includedData
     *
     * @return IProvulusObjectResponse
     */
    abstract function toResponse(array $objectData, array $includedData = []);

    /**
     * Class to be used to build the collection
     *
     * @return string
     */
    abstract function collectionClass();

    /**
     *
     * Array to collection of response
     *
     * @param array $collectionData
     *
     * @param array $includedData
     *
     * @return Collection|IProvulusObjectResponse[]
     */
    private function toCollection(array $collectionData, array $includedData = [])
    {
        $items = array_map(function (array $item) use ($includedData) {
            return $this->toResponse($item, $includedData);
        }, $collectionData);

        return new Collection($items);
    }

    /**
     * Process the data that is in the response
     *
     * @param array $jsonData
     *
     * @return CollectionResponse
     *
     * @throws \ReflectionException
     */
    public function processResponse(array $jsonData)
    {
        $class        = new \ReflectionClass($this->collectionClass());
        $includedData = [];
        if (isset($jsonData['included'])) {
            $includedData = $jsonData['included'];
        }

        $collection = $this->toCollection($jsonData['data'], array_merge($jsonData['data'], $includedData));
        /**
         * @var CollectionResponse $collectionResponse
         */
        if (!isset($jsonData['meta']) || !isset($jsonData['meta']['pagination'])) {
            if ($class->isSubclassOf(CollectionPaginatedResponse::class)) {
                $collectionResponse = $class->newInstanceArgs([CollectionPagination::noPagination(), $collection]);
                return $collectionResponse;
            }

            $collectionResponse = $class->newInstanceArgs([$collection]);
        } else {
            $collectionResponse = $class->newInstanceArgs([
                new CollectionPagination($jsonData['meta']['pagination']),
                $collection,
            ]);
        }

        return $collectionResponse;
    }
}
