<?php
/**
 * Created by PhpStorm.
 * User: aaflalo
 * Date: 23/01/17
 * Time: 3:27 PM
 */

namespace ProvulusSDK\Client\Request\Collection;


use ProvulusSDK\Client\Args\Search\BasicSearchArgument;

trait SearchableTrait
{


    /**
     * Set the search if needed
     *
     * @param $search string|null null to remove the search argument
     *
     * @return $this
     */
    public function setSearch($search)
    {
        if (is_null($search)) {
            return $this->removeArgument(new BasicSearchArgument(null));
        }

        return $this->addArgument(new BasicSearchArgument($search));
    }
}