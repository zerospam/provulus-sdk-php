<?php
/**
 * Created by PhpStorm.
 * User: aaflalo
 * Date: 14/02/17
 * Time: 3:48 PM
 */

namespace ProvulusSDK\Client\Request\Collection;

use ProvulusSDK\Client\Request\IProvulusRequest;


interface IProvulusCollectionPaginatedRequest extends IProvulusRequest, IProvulusCollectionRequest
{
    /**
     * Set the starting page
     *
     * @param $page int|null null to remove the page argument
     *
     * @return $this
     */
    public function setPage($page);

    /**
     * Number of result per page
     *
     * @param $perPage int|null null to remove the perPage argument
     *
     * @return $this
     */
    public function setPerPage($perPage);


}