<?php
/**
 * Created by PhpStorm.
 * User: fdenomme
 * Date: 02/03/20
 * Time: 10:00 AM
 */

namespace ProvulusSDK\Client\Request\Logs;

use ProvulusSDK\Client\Request\BaseProvulusRequest;
use ProvulusSDK\Client\Request\RequestType;
use ProvulusSDK\Client\Response\Logs\LogsResponse;

/**
 * LogsMessagesInRequest
 *
 * @package ProvulusSDK\Client\Request\Logs
 */
class LogsMessagesInRequest extends BaseProvulusRequest
{
    /* @var string */
    private $to_domain, $to_local_part, $from_domain,
        $limit, $sort, $begin_dt, $end_dt, $before, $after;

    /**
     * LogsMessagesInRequest
     *
     * @param array $parameters
     */
    public function __construct(array $parameters)
    {
        $this->to_domain        = $parameters['toDomain'];
        $this->to_local_part    = $parameters['toLocalPart'];
        $this->from_domain      = $parameters['fromDomain'];
        $this->limit            = $parameters['maxEntries'];
        $this->sort             = $parameters['sort'];
        $this->begin_dt         = $parameters['minDate'];
        $this->end_dt           = $parameters['maxDate'];
        $this->before           = $parameters['before'];
        $this->after            = $parameters['after'];
    }

    /**
     * The url of the route
     *
     * @return string
     */
    public function routeUrl()
    {
        return 'logs/messages/in';
    }

    /**
     * Type of request
     *
     * @return RequestType
     */
    public function httpType()
    {
        return RequestType::HTTP_GET();
    }

    /**
     * Process the data that is in the response
     *
     * @param array $jsonResponse
     *
     * @return LogsResponse
     * @internal param Response $response
     */
    public function processResponse(array $jsonResponse)
    {
        return new LogsResponse($jsonResponse);
    }
}
