<?php
/**
 * Created by PhpStorm.
 * User: fdenomme
 * Date: 02/03/20
 * Time: 10:00 AM
 */

namespace ProvulusSDK\Client\Request\Logs;

use ProvulusSDK\Client\Request\BaseProvulusRequest;
use ProvulusSDK\Client\Request\RequestType;
use ProvulusSDK\Client\Response\Logs\LogsResponse;

/**
 * LogsMessagesOutRequest
 *
 * @package ProvulusSDK\Client\Request\Logs
 */
class LogsMessagesOutRequest extends BaseProvulusRequest
{
    /* @var string */
    private $organization_id, $from_local_part, $to_domain,
        $limit, $sort, $begin_dt, $end_dt, $before, $after;

    /**
     * LogsMessagesOutRequest
     *
     * @param array $parameters
     */
    public function __construct(array $parameters)
    {
        $this->organization_id  = $parameters['orgId'];
        $this->from_local_part  = $parameters['fromLocalPart'];
        $this->to_domain        = $parameters['toDomain'];
        $this->limit            = $parameters['maxEntries'];
        $this->sort             = $parameters['sort'];
        $this->begin_dt         = $parameters['minDate'];
        $this->end_dt           = $parameters['maxDate'];
        $this->before           = $parameters['before'];
        $this->after            = $parameters['after'];
    }

    /**
     * The url of the route
     *
     * @return string
     */
    public function routeUrl()
    {
        return 'logs/messages/out';
    }

    /**
     * Type of request
     *
     * @return RequestType
     */
    public function httpType()
    {
        return RequestType::HTTP_GET();
    }

    /**
     * Process the data that is in the response
     *
     * @param array $jsonResponse
     *
     * @return LogsResponse
     * @internal param Response $response
     */
    public function processResponse(array $jsonResponse)
    {
        return new LogsResponse($jsonResponse);
    }
}
