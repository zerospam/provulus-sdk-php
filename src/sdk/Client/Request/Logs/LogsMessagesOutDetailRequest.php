<?php
/**
 * Created by PhpStorm.
 * User: fdenomme
 * Date: 02/03/20
 * Time: 10:00 AM
 */

namespace ProvulusSDK\Client\Request\Logs;

use ProvulusSDK\Client\Request\BaseProvulusRequest;
use ProvulusSDK\Client\Request\RequestType;
use ProvulusSDK\Client\Response\Logs\LogsResponse;

/**
 * LogsMessagesOutDetailRequest
 *
 * @package ProvulusSDK\Client\Request\Logs
 */
class LogsMessagesOutDetailRequest extends BaseProvulusRequest
{
    /* @var string */
    private $organization_id, $uuid;

    /**
     * LogsMessagesOutDetailRequest
     *
     * @param string $organization_id
     * @param string $uuid
     */
    public function __construct($organization_id, $uuid)
    {
        $this->organization_id = $organization_id;
        $this->uuid            = $uuid;
    }

    /**
     * The url of the route
     *
     * @return string
     */
    public function routeUrl()
    {
        return 'logs/messages/out/detail';
    }

    /**
     * Type of request
     *
     * @return RequestType
     */
    public function httpType()
    {
        return RequestType::HTTP_GET();
    }

    /**
     * Process the data that is in the response
     *
     * @param array $jsonResponse
     *
     * @return LogsResponse
     * @internal param Response $response
     */
    public function processResponse(array $jsonResponse)
    {
        return new LogsResponse($jsonResponse);
    }
}
