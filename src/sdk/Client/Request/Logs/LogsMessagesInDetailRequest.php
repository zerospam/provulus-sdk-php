<?php
/**
 * Created by PhpStorm.
 * User: fdenomme
 * Date: 02/03/20
 * Time: 10:00 AM
 */

namespace ProvulusSDK\Client\Request\Logs;

use ProvulusSDK\Client\Request\BaseProvulusRequest;
use ProvulusSDK\Client\Request\RequestType;
use ProvulusSDK\Client\Response\Logs\LogsResponse;

/**
 * LogsMessagesInDetailRequest
 *
 * @package ProvulusSDK\Client\Request\Logs
 */
class LogsMessagesInDetailRequest extends BaseProvulusRequest
{
    /* @var string */
    private $to_domain, $uuid;

    /**
     * LogsMessagesInDetailRequest
     *
     * @param string $to_domain
     * @param string $uuid
     */
    public function __construct($to_domain, $uuid)
    {
        $this->to_domain = $to_domain;
        $this->uuid      = $uuid;
    }

    /**
     * The url of the route
     *
     * @return string
     */
    public function routeUrl()
    {
        return 'logs/messages/in/detail';
    }

    /**
     * Type of request
     *
     * @return RequestType
     */
    public function httpType()
    {
        return RequestType::HTTP_GET();
    }

    /**
     * Process the data that is in the response
     *
     * @param array $jsonResponse
     *
     * @return LogsResponse
     * @internal param Response $response
     */
    public function processResponse(array $jsonResponse)
    {
        return new LogsResponse($jsonResponse);
    }
}
