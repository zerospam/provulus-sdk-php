<?php
/**
 * Created by PhpStorm.
 * User: pbb
 * Date: 02/06/17
 * Time: 4:33 PM
 */

namespace ProvulusSDK\Client\Request\Ldap\Configuration;

use ProvulusSDK\Client\Request\RequestType;
use ProvulusSDK\Client\Request\Resource\Ldap\Configuration\LdapConfigurationBaseRequest;
use ProvulusSDK\Client\Request\Resource\Ldap\Configuration\LdapConfigurationRequestOptionalAttributesTrait;
use ProvulusSDK\Client\Request\Resource\Ldap\Configuration\LdapConfigurationResponseTrait;
use ProvulusSDK\Utils\Ldap\Synchronization;
use ProvulusSDK\Utils\Ldap\UserCreation;

/**
 * Class LdapConfigurationCreateRequest
 *
 * @package ProvulusSDK\Client\Request\LDAP
 */
class LdapConfigurationCreateRequest extends LdapConfigurationBaseRequest
{
    use LdapConfigurationRequestOptionalAttributesTrait, LdapConfigurationResponseTrait;

    /**
     * @var UserCreation
     */
    private $userCreation;
    /**
     * @var Synchronization
     */
    private $synchronization;

    /**
     * LdapConfigurationCreateRequest constructor.
     */
    function __construct()
    {
        $this->userCreation    = UserCreation::defaultInstance();
        $this->synchronization = Synchronization::defaultInstance();
    }

    /**
     * Type of request
     *
     * @return RequestType
     */
    public function httpType()
    {
        return RequestType::HTTP_POST();
    }
}