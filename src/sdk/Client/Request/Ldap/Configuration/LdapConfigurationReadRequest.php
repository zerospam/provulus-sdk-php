<?php
/**
 * Created by PhpStorm.
 * User: pbb
 * Date: 08/06/17
 * Time: 4:23 PM
 */

namespace ProvulusSDK\Client\Request\Ldap\Configuration;


use ProvulusSDK\Client\Request\RequestType;
use ProvulusSDK\Client\Request\Resource\Ldap\Configuration\LdapConfigurationBaseRequest;
use ProvulusSDK\Client\Request\Resource\Ldap\Configuration\LdapConfigurationResponseTrait;

/**
 * Class LdapConfigurationReadRequest
 *
 * @package ProvulusSDK\Client\Request\LDAP
 */
class LdapConfigurationReadRequest extends LdapConfigurationBaseRequest
{
    use LdapConfigurationResponseTrait, WithLdapConnections;

    /**
     * Type of request
     *
     * @return RequestType
     */
    public function httpType()
    {
        return RequestType::HTTP_GET();
    }
}