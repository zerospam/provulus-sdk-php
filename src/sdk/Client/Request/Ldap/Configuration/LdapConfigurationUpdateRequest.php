<?php
/**
 * Created by PhpStorm.
 * User: pbb
 * Date: 09/06/17
 * Time: 8:58 AM
 */

namespace ProvulusSDK\Client\Request\Ldap\Configuration;

use ProvulusSDK\Client\Request\RequestType;
use ProvulusSDK\Client\Request\Resource\Ldap\Configuration\LdapConfigurationBaseRequest;
use ProvulusSDK\Client\Request\Resource\Ldap\Configuration\LdapConfigurationRequestOptionalAttributesTrait;
use ProvulusSDK\Client\Request\Resource\Ldap\Configuration\LdapConfigurationResponseTrait;
use ProvulusSDK\Utils\Ldap\Synchronization;
use ProvulusSDK\Utils\Ldap\UserCreation;

/**
 * Class LdapConfigUpdateRequest
 *
 * Used for updating ldap configuration values
 *
 * @package ProvulusSDK\Client\Request\LDAP
 */
class LdapConfigurationUpdateRequest extends LdapConfigurationBaseRequest
{
    use LdapConfigurationResponseTrait, LdapConfigurationRequestOptionalAttributesTrait;

    /** @var UserCreation */
    private $userCreation;

    /** @var Synchronization */
    private $synchronization;

    function __construct()
    {
        $this->userCreation    = UserCreation::defaultInstance();
        $this->synchronization = Synchronization::defaultInstance();
    }

    /**
     * Type of request
     *
     * @return RequestType
     */
    public function httpType()
    {
        return RequestType::HTTP_PUT();
    }
}