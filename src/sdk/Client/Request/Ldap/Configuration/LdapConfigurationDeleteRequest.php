<?php
/**
 * Created by PhpStorm.
 * User: pbb
 * Date: 08/06/17
 * Time: 11:06 AM
 */

namespace ProvulusSDK\Client\Request\Ldap\Configuration;

use ProvulusSDK\Client\Request\EmptyResponseTrait;
use ProvulusSDK\Client\Request\RequestType;
use ProvulusSDK\Client\Request\Resource\Ldap\Configuration\LdapConfigurationBaseRequest;

/**
 * Class LdapConfigDeleteRequest
 *
 * @package ProvulusSDK\Client\Request\LDAP
 */
class LdapConfigurationDeleteRequest extends LdapConfigurationBaseRequest
{

    use EmptyResponseTrait;

    /**
     * Type of request
     *
     * @return RequestType
     */
    public function httpType()
    {
        return RequestType::HTTP_DELETE();
    }
}