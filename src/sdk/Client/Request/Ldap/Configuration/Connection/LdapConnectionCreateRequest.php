<?php
/**
 * Created by PhpStorm.
 * User: ycoutu
 * Date: 29/08/18
 * Time: 2:55 PM
 */

namespace ProvulusSDK\Client\Request\Ldap\Configuration\Connection;

use ProvulusSDK\Client\Request\RequestType;
use ProvulusSDK\Client\Request\Resource\Ldap\Configuration\Connection\LdapConnectionAttributesTrait;
use ProvulusSDK\Client\Request\Resource\Ldap\Configuration\Connection\LdapConnectionBaseRequest;
use ProvulusSDK\Client\Request\Resource\Ldap\Configuration\Connection\LdapConnectionResponseTrait;

class LdapConnectionCreateRequest extends LdapConnectionBaseRequest
{
    use LdapConnectionResponseTrait, LdapConnectionAttributesTrait;

    /**
     * Type of request
     *
     * @return RequestType
     */
    public function httpType()
    {
        return RequestType::HTTP_POST();
    }
}
