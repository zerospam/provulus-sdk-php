<?php
/**
 * Created by PhpStorm.
 * User: ycoutu
 * Date: 29/08/18
 * Time: 3:05 PM
 */

namespace ProvulusSDK\Client\Request\Ldap\Configuration\Connection;

use ProvulusSDK\Client\Request\EmptyResponseTrait;
use ProvulusSDK\Client\Request\RequestType;
use ProvulusSDK\Client\Request\Resource\Ldap\Configuration\LdapConfigurationBaseRequest;

class LdapConnectionTestRequest extends LdapConfigurationBaseRequest
{
    use EmptyResponseTrait;

    /** @var string */
    private $hostname;

    /** @var int */
    private $port;

    /** @var boolean */
    private $isSslEnabled;

    /** @var string */
    private $userDn;

    /** @var string */
    private $password;

    /** @var string */
    private $searchDn;

    /** @var string */
    private $emailBinding;

    /**
     * @param string $hostname
     * @return LdapConnectionTestRequest
     */
    public function setHostname($hostname)
    {
        $this->hostname = $hostname;
        return $this;
    }

    /**
     * @param int $port
     * @return LdapConnectionTestRequest
     */
    public function setPort($port)
    {
        $this->port = $port;
        return $this;
    }

    /**
     * @param bool $isSslEnabled
     * @return LdapConnectionTestRequest
     */
    public function setIsSslEnabled($isSslEnabled)
    {
        $this->isSslEnabled = $isSslEnabled;
        return $this;
    }

    /**
     * @param string $userDn
     * @return LdapConnectionTestRequest
     */
    public function setUserDn($userDn)
    {
        $this->userDn = $userDn;
        return $this;
    }

    /**
     * @param string $password
     * @return LdapConnectionTestRequest
     */
    public function setPassword($password)
    {
        $this->password = $password;
        return $this;
    }

    /**
     * @param string $searchDn
     * @return LdapConnectionTestRequest
     */
    public function setSearchDn($searchDn)
    {
        $this->searchDn = $searchDn;
        return $this;
    }

    /**
     * @param string $emailBinding
     * @return LdapConnectionTestRequest
     */
    public function setEmailBinding($emailBinding)
    {
        $this->emailBinding = $emailBinding;
        return $this;
    }

    /**
     * Type of request
     *
     * @return RequestType
     */
    public function httpType()
    {
        return RequestType::HTTP_POST();
    }

    /**
     * @return string
     * @throws \Exception
     */
    function baseRoute()
    {
        return parent::baseRoute() . '/test';
    }
}
