<?php
/**
 * Created by PhpStorm.
 * User: ycoutu
 * Date: 29/08/18
 * Time: 3:03 PM
 */

namespace ProvulusSDK\Client\Request\Ldap\Configuration\Connection;

use ProvulusSDK\Client\Request\RequestType;
use ProvulusSDK\Client\Request\Resource\Ldap\Configuration\Connection\LdapConnectionRequest;
use ProvulusSDK\Client\Request\Resource\Ldap\Configuration\Connection\LdapConnectionResponseTrait;

class LdapConnectionReadRequest extends LdapConnectionRequest
{
    use LdapConnectionResponseTrait, WithLdapSyncs;

    /**
     * Type of request
     *
     * @return RequestType
     */
    public function httpType()
    {
        return RequestType::HTTP_GET();
    }
}
