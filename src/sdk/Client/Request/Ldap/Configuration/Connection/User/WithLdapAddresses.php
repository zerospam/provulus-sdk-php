<?php
/**
 * Created by PhpStorm.
 * User: ycoutu
 * Date: 14/06/17
 * Time: 11:33 AM
 */

namespace ProvulusSDK\Client\Request\Ldap\Configuration\Connection\User;

use ProvulusSDK\Client\Args\Relationship\IncludeRelationArg;

/**
 * Trait WithLdapAddresses
 *
 * Trait used to include LdapAddresses when fetching LdapUser
 *
 * @package ProvulusSDK\Client\Request\LDAP
 */
trait WithLdapAddresses
{
    /**
     * Include related LdapAddresses
     *
     * @return $this
     */
    public function withLdapAddresses()
    {
        return $this->addArgument(new IncludeRelationArg('ldapAddresses'));
    }

    /**
     * Exclude related LdapAddresses
     *
     * @return $this
     */
    public function withoutLdapAddresses()
    {
        return $this->removeArgument(new IncludeRelationArg('ldapAddresses'));
    }
}
