<?php
/**
 * Created by PhpStorm.
 * User: ycoutu
 * Date: 14/06/17
 * Time: 3:01 PM
 */

namespace ProvulusSDK\Client\Request\Ldap\Configuration\Connection\User;

use ProvulusSDK\Client\Args\Search\Ldap\User\AddressArgument;
use ProvulusSDK\Client\Args\Search\Ldap\User\DnPathArgument;
use ProvulusSDK\Client\Args\Search\Ldap\User\FirstNameArgument;
use ProvulusSDK\Client\Args\Search\Ldap\User\LastNameArgument;

/**
 * Functions for the LdapUser search
 *
 * Trait LdapUserSearchArgument
 *
 * @package ProvulusSDK\Client\Request\LDAP
 */
trait LdapUserSearchArgument
{

    /**
     * Setter for the dn path argument
     *
     * @param $dnPath
     *
     * @return $this
     */
    public function setDnPath($dnPath)
    {
        if (is_null($dnPath)) {
            return $this->removeArgument(new DnPathArgument('mocked path name'));
        }

        return $this->addArgument(new DnPathArgument($dnPath));
    }

    /**
     * Setter for the address argument
     *
     * @param $address
     *
     * @return $this
     */
    public function setAddress($address)
    {
        if (is_null($address)) {
            return $this->removeArgument(new AddressArgument('mocked address name'));
        }

        return $this->addArgument(new AddressArgument($address));
    }

    /**
     * Setter for the name argument
     *
     * @param $name
     *
     * @return $this
     */
    public function setFirstName($name)
    {
        if (is_null($name)) {
            return $this->removeArgument(new FirstNameArgument('mocked name name'));
        }

        return $this->addArgument(new FirstNameArgument($name));
    }

    /**
     * Setter for the surname argument
     *
     * @param $surname
     *
     * @return $this
     */
    public function setLastName($surname)
    {
        if (is_null($surname)) {
            return $this->removeArgument(new LastNameArgument('mocked surname name'));
        }

        return $this->addArgument(new LastNameArgument($surname));
    }
}
