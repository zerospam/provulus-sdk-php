<?php
/**
 * Created by PhpStorm.
 * User: ycoutu
 * Date: 14/06/17
 * Time: 11:36 AM
 */

namespace ProvulusSDK\Client\Request\Ldap\Configuration\Connection\User;

use ProvulusSDK\Client\Request\Collection\CollectionPaginatedTrait;
use ProvulusSDK\Client\Request\Collection\IProvulusCollectionPaginatedRequest;
use ProvulusSDK\Client\Request\Collection\SearchableTrait;
use ProvulusSDK\Client\Request\RequestType;
use ProvulusSDK\Client\Request\Resource\Ldap\Configuration\Connection\User\BaseLdapUserRequest;
use ProvulusSDK\Client\Response\IProvulusObjectResponse;
use ProvulusSDK\Client\Response\Ldap\Configuration\Connection\User\LdapUserCollectionResponse;
use ProvulusSDK\Client\Response\Ldap\Configuration\Connection\User\LdapUserResponse;

/**
 * Request for list of LdapUser for an organization
 *
 * Class IndexLdapUserRequest
 *
 * @method LdapUserCollectionResponse getResponse()
 *
 * @package ProvulusSDK\Client\Request\LDAP
 */
class LdapUserIndexRequest extends BaseLdapUserRequest implements IProvulusCollectionPaginatedRequest
{
    use CollectionPaginatedTrait, SearchableTrait, LdapUserSearchArgument;

    /**
     * Type of request
     *
     * @return RequestType
     */
    public function httpType()
    {
        return RequestType::HTTP_GET();
    }

    /**
     * Used to parse each result of the collection
     *
     * Need to be overridden by the class
     *
     * @param array $objectData
     * @return IProvulusObjectResponse
     */
    function toResponse(array $objectData)
    {
        return new LdapUserResponse($objectData);
    }

    /**
     * Class to be used to build the collection
     *
     * @return string
     */
    function collectionClass()
    {
        return LdapUserCollectionResponse::class;
    }
}
