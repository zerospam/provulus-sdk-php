<?php
/**
 * Created by PhpStorm.
 * User: ycoutu
 * Date: 14/06/17
 * Time: 11:19 AM
 */

namespace ProvulusSDK\Client\Request\Ldap\Configuration\Connection\User;

use ProvulusSDK\Client\Request\RequestType;
use ProvulusSDK\Client\Request\Resource\Ldap\Configuration\Connection\User\BaseLdapUserRequest;
use ProvulusSDK\Client\Response\IProvulusResponse;
use ProvulusSDK\Client\Response\Ldap\Configuration\Connection\User\LdapUserResponse;

/**
 * Class ReadLdapUserRequest
 *
 * Read request for LdapUser
 *
 * @method LdapUserResponse getResponse()
 *
 * @package ProvulusSDK\Client\Request\LDAP
 */
class LdapUserReadRequest extends BaseLdapUserRequest
{
    use WithLdapAddresses;
    /**
     * Type of request
     *
     * @return RequestType
     */
    public function httpType()
    {
        return RequestType::HTTP_GET();
    }

    /**
     * Process the data that is in the response
     *
     * @param array $jsonResponse
     *
     * @return IProvulusResponse
     */
    public function processResponse(array $jsonResponse)
    {
        $included = isset($jsonResponse['included'])
            ? $jsonResponse['included']
            : [];

        return new LdapUserResponse($jsonResponse['data'], $included);
    }

    /**
     * Base route to read a specific LdapUser
     *
     * @return string
     * @throws \Exception
     */
    function baseRoute()
    {
        return parent::baseRoute() . '/:ldapUserId';
    }

    /**
     * Binding for LdapUser
     *
     * @param $id
     *
     * @return $this
     */
    public function setLdapUserId($id)
    {
        $this->addBinding('ldapUserId', $id);

        return $this;
    }
}
