<?php
/**
 * Created by PhpStorm.
 * User: ycoutu
 * Date: 29/08/18
 * Time: 3:04 PM
 */

namespace ProvulusSDK\Client\Request\Ldap\Configuration\Connection;

use ProvulusSDK\Client\Request\RequestType;
use ProvulusSDK\Client\Request\Resource\Ldap\Configuration\Connection\LdapConnectionAttributesTrait;
use ProvulusSDK\Client\Request\Resource\Ldap\Configuration\Connection\LdapConnectionRequest;
use ProvulusSDK\Client\Request\Resource\Ldap\Configuration\Connection\LdapConnectionResponseTrait;

class LdapConnectionUpdateRequest extends LdapConnectionRequest
{
    use LdapConnectionResponseTrait, LdapConnectionAttributesTrait;

    /**
     * Type of request
     *
     * @return RequestType
     */
    public function httpType()
    {
        return RequestType::HTTP_PUT();
    }
}
