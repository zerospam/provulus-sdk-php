<?php
/**
 * Created by PhpStorm.
 * User: ycoutu
 * Date: 04/09/18
 * Time: 2:57 PM
 */

namespace ProvulusSDK\Client\Request\Ldap\Configuration\Connection;

use ProvulusSDK\Client\Request\BulkRequestTrait;
use ProvulusSDK\Client\Request\EmptyResponseTrait;
use ProvulusSDK\Client\Request\RequestType;
use ProvulusSDK\Client\Request\Resource\Ldap\Configuration\Connection\LdapConnectionBaseRequest;

class LdapConnectionBulkDeleteRequest extends LdapConnectionBaseRequest
{
    use EmptyResponseTrait,
        BulkRequestTrait;

    /**
     * Type of request
     *
     * @return RequestType
     */
    public function httpType()
    {
        return RequestType::HTTP_DELETE();
    }
}
