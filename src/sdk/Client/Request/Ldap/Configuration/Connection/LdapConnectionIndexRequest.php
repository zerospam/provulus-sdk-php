<?php
/**
 * Created by PhpStorm.
 * User: ycoutu
 * Date: 29/08/18
 * Time: 2:33 PM
 */

namespace ProvulusSDK\Client\Request\Ldap\Configuration\Connection;

use ProvulusSDK\Client\Request\Collection\CollectionPaginatedTrait;
use ProvulusSDK\Client\Request\Collection\IProvulusCollectionPaginatedRequest;
use ProvulusSDK\Client\Request\RequestType;
use ProvulusSDK\Client\Request\Resource\Ldap\Configuration\Connection\LdapConnectionBaseRequest;
use ProvulusSDK\Client\Response\IProvulusObjectResponse;
use ProvulusSDK\Client\Response\Ldap\Configuration\Connection\LdapConnectionCollectionResponse;
use ProvulusSDK\Client\Response\Ldap\Configuration\Connection\LdapConnectionResponse;

/**
 * Class LdapConnectionIndexRequest
 *
 * @method LdapConnectionCollectionResponse getResponse()
 *
 * @package ProvulusSDK\Client\Request\Ldap\Configuration\Connection
 */
class LdapConnectionIndexRequest extends LdapConnectionBaseRequest implements IProvulusCollectionPaginatedRequest
{
    use CollectionPaginatedTrait, WithLdapSyncs;

    /**
     * Type of request
     *
     * @return RequestType
     */
    public function httpType()
    {
        return RequestType::HTTP_GET();
    }

    /**
     * Used to parse each result of the collection
     *
     * Need to be overridden by the class
     *
     * @param array $objectData
     *
     * @param array $includedData
     *
     * @return IProvulusObjectResponse
     */
    function toResponse(array $objectData, array $includedData = [])
    {
        return new LdapConnectionResponse($objectData, $includedData);
    }

    /**
     * Class to be used to build the collection
     *
     * @return string
     */
    function collectionClass()
    {
        return LdapConnectionCollectionResponse::class;
    }
}
