<?php
/**
 * Created by PhpStorm.
 * User: ycoutu
 * Date: 30/08/18
 * Time: 1:52 PM
 */

namespace ProvulusSDK\Client\Request\Ldap\Configuration\Connection;

use ProvulusSDK\Client\Args\Relationship\IncludeRelationArg;

trait WithLdapSyncs
{
    /**
     * Include related ldap syncs
     *
     * @return $this
     */
    public function withLdapSyncs()
    {
        return $this->addArgument(new IncludeRelationArg('ldapSyncs'));
    }

    /**
     * Exclude related ldap syncs
     *
     * @return $this
     */
    public function withoutLdapSyncs()
    {
        return $this->removeArgument(new IncludeRelationArg('ldapSyncs'));
    }
}
