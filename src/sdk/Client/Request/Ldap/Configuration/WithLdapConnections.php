<?php
/**
 * Created by PhpStorm.
 * User: ycoutu
 * Date: 30/08/18
 * Time: 1:52 PM
 */

namespace ProvulusSDK\Client\Request\Ldap\Configuration;

use ProvulusSDK\Client\Args\Relationship\IncludeRelationArg;

trait WithLdapConnections
{
    /**
     * Include related filteringPolicyItems
     *
     * @return $this
     */
    public function withLdapConnections()
    {
        return $this->addArgument(new IncludeRelationArg('ldapConnections'));
    }

    /**
     * Exclude related filteringPolicyItems
     *
     * @return $this
     */
    public function withoutLdapConnections()
    {
        return $this->removeArgument(new IncludeRelationArg('ldapConnections'));
    }
}
