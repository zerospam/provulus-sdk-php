<?php
/**
 * Created by PhpStorm.
 * User: ycoutu
 * Date: 28/06/17
 * Time: 11:54 AM
 */

namespace ProvulusSDK\Client\Request\Ldap\Synchronization\User;

use ProvulusSDK\Client\Args\Search\Ldap\UserSync\DiffCodeArgument;
use ProvulusSDK\Enum\Ldap\LdapDifferentialEnum;

trait LdapUserSyncSearchArgument
{
    /**
     * Setter for the diff code argument
     *
     * @param LdapDifferentialEnum $diffCode
     *
     * @return $this
     */
    public function setDiffCode($diffCode)
    {
        if (is_null($diffCode)) {
            return $this->removeArgument(new DiffCodeArgument(LdapDifferentialEnum::ADDED()));
        }

        return $this->addArgument(new DiffCodeArgument($diffCode));
    }
}
