<?php
/**
 * Created by PhpStorm.
 * User: pbb
 * Date: 13/06/17
 * Time: 2:23 PM
 */

namespace ProvulusSDK\Client\Request\Ldap\Synchronization\User;

use ProvulusSDK\Client\Args\Relationship\IncludeRelationArg;

/**
 * Trait WithLdapAddressSync
 *
 * Includes LdapAddressSync relationship
 *
 * @package ProvulusSDK\Client\Request\LDAP\Synchronization
 */
trait WithLdapAddressSync
{

    /**
     * Return ldapAddressesSync as included data in the response
     *
     * @return $this
     */
    public function withLdapAddressesSync()
    {
        return $this->addArgument(new IncludeRelationArg('ldapAddressesSync'));
    }
}