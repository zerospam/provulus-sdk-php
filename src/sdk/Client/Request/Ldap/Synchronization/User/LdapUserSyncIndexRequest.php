<?php
/**
 * Created by PhpStorm.
 * User: pbb
 * Date: 13/06/17
 * Time: 2:00 PM
 */

namespace ProvulusSDK\Client\Request\Ldap\Synchronization\User;


use ProvulusSDK\Client\Request\Collection\CollectionPaginatedTrait;
use ProvulusSDK\Client\Request\Collection\IProvulusCollectionPaginatedRequest;
use ProvulusSDK\Client\Request\RequestType;
use ProvulusSDK\Client\Request\Resource\Ldap\Synchronization\LdapSynchronizationBaseRequest;
use ProvulusSDK\Client\Response\IProvulusObjectResponse;
use ProvulusSDK\Client\Response\Ldap\Synchronization\User\LdapUserSyncCollectionResponse;
use ProvulusSDK\Client\Response\Ldap\Synchronization\User\LdapUserSyncResponse;

/**
 * Class LdapSynchronisationUserSyncRequest
 *
 * @method LdapUserSyncCollectionResponse getResponse()
 *
 * @package ProvulusSDK\Client\Request\LDAP\Synchronization
 */
class LdapUserSyncIndexRequest extends LdapSynchronizationBaseRequest implements IProvulusCollectionPaginatedRequest
{
    use CollectionPaginatedTrait, WithLdapAddressSync, LdapUserSyncSearchArgument;

    /**
     * Type of request
     *
     * @return RequestType
     */
    public function httpType()
    {
        return RequestType::HTTP_GET();
    }

    /**
     * @return string
     * @throws \Exception
     */
    function baseRoute()
    {
        return parent::baseRoute() . '/details';
    }

    /**
     * Used to parse each result of the collection
     *
     * Need to be overridden by the class
     *
     * @param array $objectData
     *
     * @param array $includedData
     *
     * @return IProvulusObjectResponse
     */
    function toResponse(array $objectData, array $includedData = [])
    {
        return new LdapUserSyncResponse($objectData, $includedData);
    }

    /**
     * Class to be used to build the collection
     *
     * @return string
     */
    function collectionClass()
    {
        return LdapUserSyncCollectionResponse::class;
    }
}