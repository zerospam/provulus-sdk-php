<?php
/**
 * Created by PhpStorm.
 * User: pbb
 * Date: 22/06/17
 * Time: 3:29 PM
 */

namespace ProvulusSDK\Client\Request\Ldap\Synchronization;

use ProvulusSDK\Client\Args\Search\Ldap\Synchronizations\SyncStatusArgument;
use ProvulusSDK\Client\Request\Collection\CollectionPaginatedTrait;
use ProvulusSDK\Client\Request\Collection\IProvulusCollectionPaginatedRequest;
use ProvulusSDK\Client\Request\RequestType;
use ProvulusSDK\Client\Request\Resource\Ldap\Configuration\Connection\LdapConnectionRequest;
use ProvulusSDK\Client\Response\IProvulusObjectResponse;
use ProvulusSDK\Client\Response\Ldap\Synchronization\LdapSynchronizationCollectionResponse;
use ProvulusSDK\Client\Response\Ldap\Synchronization\LdapSynchronizationResponse;
use ProvulusSDK\Enum\Ldap\LdapSyncStatusEnum;

/**
 * Class LdapSynchronizationIndexRequest
 *
 * Index request for LdapSynchronization
 *
 * @method LdapSynchronizationCollectionResponse getResponse()
 *
 * @package ProvulusSDK\Client\Request\LDAP\Synchronization
 */
class LdapSynchronizationIndexRequest extends LdapConnectionRequest implements IProvulusCollectionPaginatedRequest
{
    use CollectionPaginatedTrait;

    /**
     * Type of request
     *
     * @return RequestType
     */
    public function httpType()
    {
        return RequestType::HTTP_GET();
    }

    /**
     * Used to parse each result of the collection
     *
     * Need to be overridden by the class
     *
     * @param array $objectData
     *
     * @param array $includedData
     *
     * @return IProvulusObjectResponse
     */
    function toResponse(array $objectData, array $includedData = [])
    {
        return new LdapSynchronizationResponse($objectData, $includedData);
    }

    /**
     * Class to be used to build the collection
     *
     * @return string
     */
    function collectionClass()
    {
        return LdapSynchronizationCollectionResponse::class;
    }

    /**
     * @param LdapSyncStatusEnum[] $statuses
     */
    public function filterByStatuses(array $statuses)
    {
        array_walk($statuses, function (LdapSyncStatusEnum $status) {
            $this->addArgument(new SyncStatusArgument($status));
        });
    }

    /**
     * Base route for orgs resource
     *
     * @return string
     * @throws \Exception
     */
    function baseRoute()
    {
        if (!$this->hasBinding('ldapConnectionId')) {
            throw new \Exception(sprintf('Ldap connection ID needs to be set for %s', get_class($this)));
        }

        return parent::baseRoute() . '/synchronizations';
    }
}