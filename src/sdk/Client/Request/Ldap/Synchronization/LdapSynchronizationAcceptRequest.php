<?php
/**
 * Created by PhpStorm.
 * User: pbb
 * Date: 15/06/17
 * Time: 9:19 AM
 */

namespace ProvulusSDK\Client\Request\Ldap\Synchronization;


use ProvulusSDK\Client\Request\EmptyResponseTrait;
use ProvulusSDK\Client\Request\RequestType;
use ProvulusSDK\Client\Request\Resource\Ldap\Synchronization\LdapSynchronizationBaseRequest;

/**
 * Class SynchronizationConfirmRequest
 *
 * Confirm the manual synchronization request
 *
 * @package ProvulusSDK\Client\Request\LDAP\Synchronization
 */
class LdapSynchronizationAcceptRequest extends LdapSynchronizationBaseRequest
{

    use EmptyResponseTrait;

    /**
     * Type of request
     *
     * @return RequestType
     */
    public function httpType()
    {
        return RequestType::HTTP_POST();
    }

    /**
     * @return string
     * @throws \Exception
     */
    function baseRoute()
    {
        return parent::baseRoute() . '/accept';
    }
}