<?php
/**
 * Created by PhpStorm.
 * User: aaflalo
 * Date: 30/11/16
 * Time: 1:46 PM
 */


use ProvulusSDK\Client\ProvulusClient;
use ProvulusSDK\Config\Endpoint\ProvulusEndpoint;
use ProvulusSDK\Config\ProvulusConfiguration;
use ProvulusSDK\Config\Version\ProvulusApiVersion;
use ProvulusTest\BasicJWTStorage;

class LoginTest extends PHPUnit_Framework_TestCase
{

    /**
     * @test
     */
    public function testLoginSuccess()
    {
        $conf = new ProvulusConfiguration(ProvulusEndpoint::STAGING(), ProvulusApiVersion::V1());
        $conf->setJwtStorage(new BasicJWTStorage());
        $client = new ProvulusClient($conf);
        $client->login('admintest@epic.ca', '_TestTest123');
        $this->assertNotNull($conf->getJwtStorage()->getToken(), "Coudn't get a JWT Token");
    }

    /**
     * @test
     * @expectedException  ProvulusSDK\Exception\API\UnauthorizedException
     */
    public function testLoginFail()
    {
        $conf = new ProvulusConfiguration(ProvulusEndpoint::STAGING(), ProvulusApiVersion::V1());
        $conf->setJwtStorage(new BasicJWTStorage());
        $client = new ProvulusClient($conf);

        $client->login('admintest@epic.ca', '');
    }
}
