<?php
/**
 * Created by PhpStorm.
 * User: aaflalo
 * Date: 06/12/16
 * Time: 3:20 PM
 */

use ProvulusSDK\Client\ProvulusClient;
use ProvulusSDK\Client\Request\Client\Direct\CreateDirectClientRequest;
use ProvulusSDK\Config\Endpoint\ProvulusEndpoint;
use ProvulusSDK\Config\ProvulusConfiguration;
use ProvulusSDK\Config\Version\ProvulusApiVersion;
use ProvulusSDK\Enum\Locale\LocaleEnum;
use ProvulusSDK\Exception\API\ValidationException;
use ProvulusTest\BasicJWTStorage;

class CreateTest extends PHPUnit_Framework_TestCase
{

    /**
     * @test
     */
    public function create()
    {
        $conf = new ProvulusConfiguration(ProvulusEndpoint::LOCAL(), ProvulusApiVersion::V1());
        $conf->setJwtStorage(new BasicJWTStorage());
        $client = new ProvulusClient($conf);
        $client->login('reseller@supe.res', '_TestTest123');

        $createRequest = new CreateDirectClientRequest();
        $createRequest->setFirstName('Test')
                      ->setLastName('Test')
                      ->setDomain('testaclient.com')
                      ->setOrganizationName('Test a client dot com')
                      ->setEmail('test@testaclient.com')
                      ->setTransport('mailcatcher')
                      ->setMailboxes('10')
                      ->setTransportPort(25)
                      ->setPhone('5146244520')
                      ->setLanguage(LocaleEnum::EN_US());
        try {
            $response = $client->processRequest($createRequest);
        } catch (ValidationException $e) {
            var_dump($e->getValidationData());
            throw $e;
        }


        var_dump($response);
    }
}