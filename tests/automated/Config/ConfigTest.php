<?php
/**
 * Created by PhpStorm.
 * User: aaflalo
 * Date: 30/11/16
 * Time: 3:45 PM
 */

namespace automated\Config;


use GuzzleHttp\Psr7\Uri;
use ProvulusSDK\Config\Endpoint\ProvulusEndpoint;
use ProvulusSDK\Config\ProvulusConfiguration;
use ProvulusSDK\Config\Version\ProvulusApiVersion;
use ProvulusTest\Request\TestRequest;
use ProvulusTest\TestCase;

class ConfigTest extends TestCase
{

    /** @test */
    public function client_config_can_be_instantiated()
    {
        $configuration = new ProvulusConfiguration(ProvulusEndpoint::LOCAL(), ProvulusApiVersion::V1());
        $this->assertInstanceOf('ProvulusSDK\Config\ProvulusConfiguration', $configuration);
    }

    /** @test */
    public function client_config_end_point_generated_correctly()
    {
        $configuration = new ProvulusConfiguration(ProvulusEndpoint::LOCAL(), ProvulusApiVersion::V1());
        $expected      = ProvulusEndpoint::LOCAL()->getValue() . '/api/' . ProvulusApiVersion::V1()->getValue() . '/';
        $this->assertEquals($expected, (string)$configuration->baseUrl());
    }

    /**
     * @test
     */
    public function client_config_test_combining_url()
    {
        $configuration = new ProvulusConfiguration(ProvulusEndpoint::LOCAL(), ProvulusApiVersion::V1());

        $testRequest = new TestRequest();
        $received    = (string)Uri::resolve($configuration->baseUrl(), $testRequest->toUri());
        $this->assertEquals('https://cumulus.local/api/v1/test/request', $received);
    }
}