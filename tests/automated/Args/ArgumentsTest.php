<?php
/**
 * Created by PhpStorm.
 * User: aaflalo
 * Date: 09/01/17
 * Time: 1:13 PM
 */

namespace automated\Args;


use ProvulusSDK\Client\Args\Page\PageArgument;
use ProvulusSDK\Client\Args\Page\PerPageArgument;
use ProvulusSDK\Client\Args\RequestArg;
use ProvulusSDK\Client\Args\Search\BasicSearchArgument;
use ProvulusSDK\Client\Args\Search\Organization\ActiveArgument;
use ProvulusSDK\Client\Args\Search\Organization\BilledByArgument;
use ProvulusSDK\Client\Args\Search\Organization\CorporateNameArgument;
use ProvulusSDK\Client\Args\Search\Organization\DomainNameArgument;
use ProvulusSDK\Client\Args\Search\Organization\SupportedByArgument;
use ProvulusSDK\Client\Args\Search\Renewal\RenewalStateArgument;
use ProvulusSDK\Client\Args\Sort\OrderByArg;
use ProvulusSDK\Client\Args\Sort\SortedByArg;
use ProvulusSDK\Client\Args\Sort\SortEnum;
use ProvulusSDK\Client\ProvulusClient;
use ProvulusSDK\Enum\Locale\LocaleEnum;
use ProvulusSDK\Enum\Renewal\PaymentRenewalStateEnum;
use ProvulusSDK\Enum\SearchCriteria\ActiveCriteriaEnum;
use ProvulusSDK\Utils\Str;
use ProvulusTest\Request\TestRequest;
use ProvulusTest\TestCaseApiKey;

class ArgumentsTest extends TestCaseApiKey
{

    /**
     * @test
     * @expectedException \InvalidArgumentException
     **/
    public function argument_failure_not_string()
    {
        new RequestArg(new \stdClass(), 'test');
    }

    /**
     * @test
     * @expectedException \InvalidArgumentException
     **/
    public function argument_failure_null()
    {
        new RequestArg(null, 'test');
    }

    /**
     * @test
     */
    public function argument_test_boolean_true()
    {
        $test = new RequestArg('test', true);

        $this->assertSame(1, $test->toPrimitive());
    }


    /**
     * @test
     */
    public function argument_test_boolean_false()
    {
        $test = new RequestArg('test', false);

        $this->assertSame(0, $test->toPrimitive());
    }

    /**
     * @test
     **/
    public function add_remove_args()
    {
        $request = new TestRequest();
        $arg     = new BasicSearchArgument('test');
        $request->addArgument($arg)
                ->removeArgument($arg);

        $this->assertArrayNotHasKey($arg->getKey(), $request->getArguments());
    }

    /**
     * @test
     **/
    public function add_remove_args_with_request()
    {

        $config = $this->preSuccess('{}');

        $request = new TestRequest();
        $request->addArgument(new BasicSearchArgument('test'))
                ->removeArgument(new BasicSearchArgument('test'));


        $client = new ProvulusClient($config);

        $client->processRequest($request);

        $trans = $this->lastTransaction($config);
        $this->assertNotContains('?search=test', (string)$trans->request()->getUri());
    }


    /**
     * @test
     **/
    public function search_arg()
    {
        $config = $this->preSuccess('{}');


        $request = new TestRequest();
        $request->addArgument(new BasicSearchArgument('test'));


        $client = new ProvulusClient($config);

        $client->processRequest($request);

        $trans = $this->lastTransaction($config);
        $this->assertContains('?search=test', (string)$trans->request()->getUri());
    }

    /**
     * @test
     **/
    public function search_arg_enum()
    {

        $config = $this->preSuccess('{}');


        $request = new TestRequest();
        $request->addArgument(new BasicSearchArgument(LocaleEnum::FR_CA()));


        $client = new ProvulusClient($config);

        $client->processRequest($request);

        $trans = $this->lastTransaction($config);


        $this->assertContains('?search=fr_CA', (string)$trans->request()->getUri());
    }

    /**
     * @test
     * @expectedException \InvalidArgumentException
     **/
    public function page_arg_failure()
    {
        new PageArgument('test');
    }

    /**
     * @test
     **/
    public function page_arg_success()
    {
        $config = $this->preSuccess('{}');

        $request = new TestRequest();
        $request->addArgument(new PageArgument(2));


        $client = new ProvulusClient($config);

        $client->processRequest($request);

        $trans = $this->lastTransaction($config);

        $this->assertContains('?page=2', (string)$trans->request()->getUri());
    }

    /**
     * @test
     * @expectedException \InvalidArgumentException
     **/
    public function per_page_arg_failure()
    {
        new PerPageArgument('test');
    }

    /**
     * @test
     **/
    public function per_page_arg_success()
    {

        $config = $this->preSuccess('{}');

        $request = new TestRequest();
        $request->addArgument(new PerPageArgument(25));


        $client = new ProvulusClient($config);

        $client->processRequest($request);

        $trans = $this->lastTransaction($config);

        $this->assertContains('?perPage=25', (string)$trans->request()->getUri());
    }


    /**
     * @test
     **/
    public function order_by_argument()
    {
        $config = $this->preSuccess([]);

        $request = new TestRequest();
        $request->addArgument(new OrderByArg('test'));

        $client = new ProvulusClient($config);
        $client->processRequest($request);

        $this->validateRequestUrl($config, ['orderBy=test']);
    }

    /**
     * @test
     **/
    public function sorted_by_argument()
    {
        $config = $this->preSuccess([]);

        $request = new TestRequest();
        $request->addArgument(new SortedByArg(SortEnum::ASCENDING()));

        $client = new ProvulusClient($config);
        $client->processRequest($request);

        $this->validateRequestUrl($config, ['sortedBy=ASC']);

    }

    /**
     * @test
     **/
    public function multi_args()
    {
        $config = $this->preSuccess('{}');


        $request = new TestRequest();
        $request->addArgument(new PerPageArgument(25))->addArgument(new PageArgument(2));


        $client = new ProvulusClient($config);

        $client->processRequest($request);

        $trans = $this->lastTransaction($config);
        $this->assertTrue(Str::containsAll((string)$trans->request()->getUri(), [
            'perPage=25',
            'page=2',
        ]));
    }

    /**
     * @test
     **/
    public function corporate_name_argument()
    {
        $config = $this->preSuccess('{}');

        $request = new TestRequest();
        $request->addArgument(new CorporateNameArgument('Zerospam'));

        $client = new ProvulusClient($config);
        $client->processRequest($request);

        $trans = $this->lastTransaction($config);
        $this->assertContains('corporate_name=Zerospam', (string)$trans->request()->getUri());
    }

    /**
     * @test
     **/
    public function add_remove_corporate_name_argument()
    {
        $config = $this->preSuccess('{}');

        $request = new TestRequest();
        $request->addArgument(new CorporateNameArgument('Zerospam'))
                ->removeArgument(new CorporateNameArgument('Zerospam'));

        $client = new ProvulusClient($config);
        $client->processRequest($request);

        $trans = $this->lastTransaction($config);
        $this->assertNotContains('corporate_name=Zerospam', (string)$trans->request()->getUri());
    }

    /**
     * @test
     **/
    public function domain_name_argument()
    {
        $config = $this->preSuccess('{}');

        $request = new TestRequest();
        $request->addArgument(new DomainNameArgument('zozo'));

        $client = new ProvulusClient($config);
        $client->processRequest($request);

        $trans = $this->lastTransaction($config);
        $this->assertContains('domain_name=zozo', (string)$trans->request()->getUri());
    }

    /**
     * @test
     **/
    public function add_remove_domain_name_argument()
    {
        $config = $this->preSuccess('{}');

        $request = new TestRequest();
        $request->addArgument(new DomainNameArgument('zozo'))
                ->removeArgument(new DomainNameArgument('zozo'));

        $client = new ProvulusClient($config);
        $client->processRequest($request);

        $trans = $this->lastTransaction($config);
        $this->assertNotContains('domain_name=zozo', (string)$trans->request()->getUri());
    }

    /**
     * @test
     **/
    public function supported_by_argument()
    {
        $config = $this->preSuccess('{}');

        $request = new TestRequest();
        $request->addArgument(new SupportedByArgument(1));

        $client = new ProvulusClient($config);
        $client->processRequest($request);

        $trans = $this->lastTransaction($config);
        $this->assertContains('supported_by=1', (string)$trans->request()->getUri());
    }

    /**
     * @test
     **/
    public function add_remove_supported_by_argument()
    {
        $config = $this->preSuccess('{}');

        $request = new TestRequest();
        $request->addArgument(new SupportedByArgument(1))
                ->removeArgument(new SupportedByArgument(1));

        $client = new ProvulusClient($config);
        $client->processRequest($request);

        $trans = $this->lastTransaction($config);
        $this->assertNotContains('supported_by=1', (string)$trans->request()->getUri());
    }

    /**
     * @test
     **/
    public function billed_by_argument()
    {
        $config = $this->preSuccess('{}');

        $request = new TestRequest();
        $request->addArgument(new BilledByArgument(1));

        $client = new ProvulusClient($config);
        $client->processRequest($request);

        $transaction = $this->lastTransaction($config);
        self::assertContains('billed_by=1', (string)$transaction->request()->getUri());
    }

    /**
     * @test
     **/
    public function add_remove_billed_by_argument()
    {
        $config = $this->preSuccess('{}');

        $request = new TestRequest();
        $request->addArgument(new BilledByArgument(1))
                ->removeArgument(new BilledByArgument(1));

        $client = new ProvulusClient($config);
        $client->processRequest($request);

        $transaction = $this->lastTransaction($config);
        self::assertNotContains('billed_by=1', (string)$transaction->request()->getUri());
    }

    /**
     * @test
     **/
    public function active_argument()
    {
        $config = $this->preSuccess('{}');

        $request = new TestRequest();
        $request->addArgument(new ActiveArgument(ActiveCriteriaEnum::ALL()));

        $client = new ProvulusClient($config);
        $client->processRequest($request);

        $transaction = $this->lastTransaction($config);
        self::assertContains('active=all', (string)$transaction->request()->getUri());
    }

    /**
     * @test
     **/
    public function add_remove_active_argument()
    {
        $config = $this->preSuccess('{}');

        $request = new TestRequest();
        $request->addArgument(new ActiveArgument(ActiveCriteriaEnum::ALL()))
                ->removeArgument(new ActiveArgument(ActiveCriteriaEnum::ALL()));

        $client = new ProvulusClient($config);
        $client->processRequest($request);

        $transaction = $this->lastTransaction($config);
        self::assertNotContains('active=all', (string)$transaction->request()->getUri());
    }

    /**
     * @test
     **/
    public function renewal_state_argument_Success()
    {
        $config = $this->preSuccess([]);

        $request = new TestRequest();
        $request->addArgument(new RenewalStateArgument(PaymentRenewalStateEnum::RENEWING()));

        $client = new ProvulusClient($config);
        $client->processRequest($request);

        $transaction = $this->lastTransaction($config);
        self::assertContains('renewal_state=renewing', (string)$transaction->request()->getUri());
    }

    /**
     * @test
     **/
    public function add_support_organization_argument()
    {
        $config = $this->preSuccess('{}');

        $request = new TestRequest();
        $request->withSupportOrganization();

        $client = new ProvulusClient($config);
        $client->processRequest($request);

        $transaction = $this->lastTransaction($config);
        self::assertContains('include=supportOrganization', (string)$transaction->request()->getUri());
    }

    /**
     * @test
     **/
    public function remove_support_organization_argument()
    {
        $config = $this->preSuccess([]);

        $request = new TestRequest();
        $request->withSupportOrganization()
                ->withoutSupportOrganization();

        $client = new ProvulusClient($config);
        $client->processRequest($request);

        $transaction = $this->lastTransaction($config);
        self::assertNotContains('include=supportOrganization', (string)$transaction->request()->getUri());
    }

    /**
     * @test
     **/
    public function include_domains_argument()
    {
        $config = $this->preSuccess([]);

        $request = new TestRequest();
        $request->withDomains();

        $client = new ProvulusClient($config);
        $client->processRequest($request);

        $transaction = $this->lastTransaction($config);
        self::assertContains('include=domains', (string)$transaction->request()->getUri());
    }

    /**
     * @test
     **/
    public function remove_domains_argument()
    {
        $config = $this->preSuccess([]);

        $request = new TestRequest();
        $request->withDomains();
        $request->withoutDomains();

        $client = new ProvulusClient($config);
        $client->processRequest($request);

        $transaction = $this->lastTransaction($config);
        self::assertNotContains('include=domains', (string)$transaction->request()->getUri());
    }

    /**
     * @test
     **/
    public function add_user_email_addresses_argument()
    {
        $config = $this->preSuccess([]);

        $request = new TestRequest();
        $request->withEmailAddresses();

        $client = new ProvulusClient($config);
        $client->processRequest($request);

        $transaction = $this->lastTransaction($config);
        self::assertContains('include=addresses', (string)$transaction->request()->getUri());
    }

    /**
     * @test
     **/
    public function remove_user_email_addresses_argument()
    {
        $config = $this->preSuccess([]);

        $request = new TestRequest();
        $request->withEmailAddresses()
                ->withoutEmailAddresses();

        $client = new ProvulusClient($config);
        $client->processRequest($request);

        $transaction = $this->lastTransaction($config);
        self::assertNotContains('include=addresses', (string)$transaction->request()->getUri());
    }


    /**
     * @test
     * @expectedException \InvalidArgumentException
     **/
    public function corporate_name_argument_invalid_value()
    {
        new CorporateNameArgument(5);
    }

    /**
     * @test
     * @expectedException \InvalidArgumentException
     **/
    public function domain_name_argument_invalid_value()
    {
        new DomainNameArgument(10);
    }

    /**
     * @test
     * @expectedException \InvalidArgumentException
     **/
    public function supported_by_argument_invalid_value()
    {
        new SupportedByArgument('organization');
    }

    /**
     * @test
     * @expectedException \InvalidArgumentException
     **/
    public function billed_by_argument_invalid_value()
    {
        new BilledByArgument('organization');
    }


}