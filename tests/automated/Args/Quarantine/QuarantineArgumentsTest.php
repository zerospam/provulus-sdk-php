<?php
/**
 * Created by PhpStorm.
 * User: pbb
 * Date: 15/02/17
 * Time: 3:27 PM
 */

namespace automated\Args\Quarantine;

use GuzzleHttp\Handler\MockHandler;
use ProvulusSDK\Client\Args\Search\Quarantine\Flags\Status\IsStatusDeclaredAsSpamArgument;
use ProvulusSDK\Client\Args\Search\Quarantine\Flags\Status\IsStatusDeletedArgument;
use ProvulusSDK\Client\Args\Search\Quarantine\Flags\Tags\IsTagGreymail;
use ProvulusSDK\Client\Args\Search\Quarantine\Flags\Status\IsStatusReleasedArgument;
use ProvulusSDK\Client\Args\Search\Quarantine\Flags\Status\IsNoStatusArgument;
use ProvulusSDK\Client\Args\Search\Quarantine\FromArgument;
use ProvulusSDK\Client\Args\Search\Quarantine\SAScore\LowerScoreArgument;
use ProvulusSDK\Client\Args\Search\Quarantine\SAScore\UpperScoreArgument;
use ProvulusSDK\Client\Args\Search\Quarantine\SubjectArgument;
use ProvulusSDK\Client\Args\Search\Quarantine\Time\LowerLimitReceivedAtArgument;
use ProvulusSDK\Client\Args\Search\Quarantine\Time\UpperLimitReceivedAtArgument;
use ProvulusSDK\Client\Args\Search\Quarantine\ToArgument;
use ProvulusSDK\Client\ProvulusClient;
use ProvulusTest\Request\TestRequest;
use ProvulusTest\TestCaseApiKey;

/**
 * Class QuarantineArgumentsTest
 *
 * Tests for quarantine arguments
 *
 * @package automated\Client\Args
 */
class QuarantineArgumentsTest extends TestCaseApiKey
{

    /**
     * @test
     */
    public function to_argument()
    {
        $config = $this->preSuccess('{}');

        $request = new TestRequest();
        $request->addArgument(new ToArgument('zerospam'));

        $client = new ProvulusClient($config);
        $client->processRequest($request);

        $transaction = $this->lastTransaction($config);
        self::assertContains('to=zerospam', (string)$transaction->request()->getUri());
    }

    /**
     * @test
     **/
    public function remove_to_argument()
    {
        $config = $this->preSuccess('{}');

        $request = new TestRequest();
        $request->addArgument(new ToArgument('zerospam'))
                ->removeArgument(new ToArgument('zerospam'));

        $client = new ProvulusClient($config);
        $client->processRequest($request);

        $transaction = $this->lastTransaction($config);
        self::assertNotContains('to=zerospam', (string)$transaction->request()->getUri());
    }

    /**
     * @test
     **/
    public function from_argument()
    {
        $config = $this->preSuccess('{}');

        $request = new TestRequest();
        $request->addArgument(new FromArgument('zerospam'))
                ->addArgument(new FromArgument('epic'));

        $client = new ProvulusClient($config);
        $client->processRequest($request);

        $transaction = $this->lastTransaction($config);
        self::assertContains('from=zerospam', (string)$transaction->request()->getUri());
    }

    /**
     * @test
     **/
    public function remove_from_argument()
    {
        $config = $this->preSuccess('{}');

        $request = new TestRequest();
        $request->addArgument(new FromArgument('zerospam'))
                ->removeArgument(new FromArgument('epic'));

        $client = new ProvulusClient($config);
        $client->processRequest($request);

        $transaction = $this->lastTransaction($config);
        self::assertNotContains('from=zerospam;epic', urldecode((string)$transaction->request()->getUri()));
    }

    /**
     * @test
     **/
    public function subject_argument()
    {
        $config = $this->preSuccess('{}');

        $request = new TestRequest();
        $request->addArgument(new SubjectArgument('zerospam'));

        $client = new ProvulusClient($config);
        $client->processRequest($request);

        $transaction = $this->lastTransaction($config);
        self::assertContains('subject=zerospam', (string)$transaction->request()->getUri());
    }

    /**
     * @test
     **/
    public function remove_subject_argument()
    {
        $config = $this->preSuccess('{}');

        $request = new TestRequest();
        $request->addArgument(new SubjectArgument('zerospam'))
                ->removeArgument(new SubjectArgument('zerospam'));

        $client = new ProvulusClient($config);
        $client->processRequest($request);

        $transaction = $this->lastTransaction($config);
        self::assertNotContains('subject=zerospam', (string)$transaction->request()->getUri());
    }

    /**
     * @test
     **/
    public function lower_limit_received_at_argument()
    {
        $config = $this->preSuccess('{}');

        $request = new TestRequest();
        $request->addArgument(new LowerLimitReceivedAtArgument('2017-01-19 00:00'));

        $client = new ProvulusClient($config);
        $client->processRequest($request);

        $transaction = $this->lastTransaction($config);
        self::assertContains('start=2017-01-19%2000%3A00', (string)$transaction->request()->getUri());
    }

    /**
     * @test
     **/
    public function remove_lower_limit_received_at_argument()
    {
        $config = $this->preSuccess('{}');

        $request = new TestRequest();
        $request->addArgument(new LowerLimitReceivedAtArgument('2017-01-19 00:00'))
                ->removeArgument(new LowerLimitReceivedAtArgument('2017-01-19 00:00'));

        $client = new ProvulusClient($config);
        $client->processRequest($request);

        $transaction = $this->lastTransaction($config);
        self::assertNotContains('start=2017-01-19%2000%3A00', (string)$transaction->request()->getUri());
    }

    /**
     * @test
     **/
    public function upper_limit_received_at_argument()
    {
        $config = $this->preSuccess('{}');

        $request = new TestRequest();
        $request->addArgument(new UpperLimitReceivedAtArgument('2017-01-19 00:00'));

        $client = new ProvulusClient($config);
        $client->processRequest($request);

        $transaction = $this->lastTransaction($config);
        self::assertContains('stop=2017-01-19%2000%3A00', (string)$transaction->request()->getUri());
    }


    /**
     * @test
     **/
    public function remove_upper_limit_received_at_argument()
    {
        $config = $this->preSuccess('{}');

        $request = new TestRequest();
        $request->addArgument(new UpperLimitReceivedAtArgument('2017-01-19 00:00'))
                ->removeArgument(new UpperLimitReceivedAtArgument('2017-01-19 00:00'));

        $client = new ProvulusClient($config);
        $client->processRequest($request);

        $transaction = $this->lastTransaction($config);
        self::assertNotContains('stop=2017-01-19%2000%3A00', (string)$transaction->request()->getUri());
    }

    /**
     * @test
     **/
    public function lower_limit_SAScore_at_argument()
    {
        $config = $this->preSuccess('{}');

        $request = new TestRequest();
        $request->addArgument(new LowerScoreArgument(5.01));

        $client = new ProvulusClient($config);
        $client->processRequest($request);

        $transaction = $this->lastTransaction($config);
        self::assertContains('lower_score=5.01', (string)$transaction->request()->getUri());
    }

    /**
     * @test
     **/
    public function remove_lower_limit_SAScore_at_argument()
    {
        $config = $this->preSuccess('{}');

        $request = new TestRequest();
        $request->addArgument(new LowerScoreArgument(5.01))
                ->removeArgument(new LowerScoreArgument(5.01));

        $client = new ProvulusClient($config);
        $client->processRequest($request);

        $transaction = $this->lastTransaction($config);
        self::assertNotContains('lower_score=5.01', (string)$transaction->request()->getUri());
    }

    /**
     * @test
     **/
    public function upper_limit_SAScore_at_argument()
    {
        $config = $this->preSuccess('{}');

        $request = new TestRequest();
        $request->addArgument(new UpperScoreArgument(5.01));

        $client = new ProvulusClient($config);
        $client->processRequest($request);

        $transaction = $this->lastTransaction($config);
        self::assertContains('upper_score=5.01', (string)$transaction->request()->getUri());
    }

    /**
     * @test
     **/
    public function remove_upper_limit_SAScore_at_argument()
    {
        $config = $this->preSuccess('{}');

        $request = new TestRequest();
        $request->addArgument(new UpperScoreArgument(5.01))
                ->removeArgument(new UpperScoreArgument(5.01));

        $client = new ProvulusClient($config);
        $client->processRequest($request);

        $transaction = $this->lastTransaction($config);
        self::assertNotContains('upperscore=5.01', (string)$transaction->request()->getUri());
    }


    /**
     * @test
     **/
    public function include_deleted_argument()
    {
        $config = $this->preSuccess('{}');

        $request = new TestRequest();
        $request->addArgument(new IsStatusDeletedArgument(true));

        $client = new ProvulusClient($config);
        $client->processRequest($request);

        $transaction = $this->lastTransaction($config);
        self::assertContains('status_deleted=1', (string)$transaction->request()->getUri());
    }

    /**
     * @test
     **/
    public function remove_include_deleted_argument()
    {
        $config = $this->preSuccess('{}');

        $request = new TestRequest();
        $request->addArgument(new IsStatusDeletedArgument(true))
                ->removeArgument(new IsStatusDeletedArgument(true));

        $client = new ProvulusClient($config);
        $client->processRequest($request);

        $transaction = $this->lastTransaction($config);
        self::assertNotContains('status_deleted=1', (string)$transaction->request()->getUri());
    }

    /**
     * @test
     **/
    public function include_released_argument()
    {
        $config = $this->preSuccess('{}');

        $request = new TestRequest();
        $request->addArgument(new IsStatusReleasedArgument(true));

        $client = new ProvulusClient($config);
        $client->processRequest($request);

        $transaction = $this->lastTransaction($config);
        self::assertContains('status_released=1', (string)$transaction->request()->getUri());
    }

    /**
     * @test
     **/
    public function remove_include_released_argument()
    {
        $config = $this->preSuccess('{}');

        $request = new TestRequest();
        $request->addArgument(new IsStatusReleasedArgument(true))
                ->removeArgument(new IsStatusReleasedArgument(true));

        $client = new ProvulusClient($config);
        $client->processRequest($request);

        $transaction = $this->lastTransaction($config);
        self::assertNotContains('status_released=1', (string)$transaction->request()->getUri());
    }

    /**
     * @test
     **/
    public function include_declared_spam_argument()
    {
        $config = $this->preSuccess('{}');

        $request = new TestRequest();
        $request->addArgument(new IsStatusDeclaredAsSpamArgument(true));

        $client = new ProvulusClient($config);
        $client->processRequest($request);

        $transaction = $this->lastTransaction($config);
        self::assertContains('status_declared_spam=1', (string)$transaction->request()->getUri());
    }

    /**
     * @test
     */
    public function include_greymail_argument()
    {
        $config = $this->preSuccess([]);

        $request = new TestRequest();
        $request->addArgument(new IsTagGreymail(true));

        $client = new ProvulusClient($config);
        $client->processRequest($request);

        $transaction = $this->lastTransaction($config);
        self::assertContains('tag_greymail=1', (string)$transaction->request()->getUri());
    }

    /**
     * @test
     **/
    public function remove_include_declared_spam_argument()
    {
        $config = $this->preSuccess('{}');

        $request = new TestRequest();
        $request->addArgument(new IsStatusDeclaredAsSpamArgument(true))
                ->removeArgument(new IsStatusDeclaredAsSpamArgument(true));

        $client = new ProvulusClient($config);
        $client->processRequest($request);

        $transaction = $this->lastTransaction($config);
        self::assertNotContains('status_declared_spam=1', (string)$transaction->request()->getUri());
    }

    /**
     * @test
     **/
    public function normal_quarantine_argument()
    {
        $config = $this->preSuccess([]);

        $request = new TestRequest();
        $request->addArgument(new IsNoStatusArgument(true));

        $client = new ProvulusClient($config);
        $client->processRequest($request);

        $transaction = $this->lastTransaction($config);
        self::assertContains('no_status=1', (string)$transaction->request()->getUri());
    }

    /**
     * @test
     * @expectedException  \InvalidArgumentException
     **/
    public function to_invalid_argument()
    {
        $request = new TestRequest();
        $request->addArgument(new ToArgument(1));
    }

    /**
     * @test
     * @expectedException  \InvalidArgumentException
     **/
    public function from_invalid_argument()
    {
        $request = new TestRequest();
        $request->addArgument(new FromArgument(1));
    }

    /**
     * @test
     * @expectedException  \InvalidArgumentException
     **/
    public function subject_invalid_argument()
    {
        $request = new TestRequest();
        $request->addArgument(new SubjectArgument(1));
    }

    /**
     * @test
     * @expectedException \InvalidArgumentException
     **/
    public function upper_limit_SAScore_at_invalid_argument()
    {
        $request = new TestRequest();
        $request->addArgument(new UpperScoreArgument('asdf'));
    }

    /**
     * @test
     * @expectedException \InvalidArgumentException
     **/
    public function lower_limit_SAScore_at_invalid_argument()
    {
        $request = new TestRequest();
        $request->addArgument(new LowerScoreArgument('asdf'));
    }

    /**
     * @test
     * @expectedException \InvalidArgumentException
     **/
    public function include_deleted_invalid_argument()
    {
        $request = new TestRequest();
        $request->addArgument(new IsStatusDeletedArgument(1));
    }

    /**
     * @test
     * @expectedException \InvalidArgumentException
     **/
    public function include_released_invalid_argument()
    {
        $request = new TestRequest();
        $request->addArgument(new IsStatusReleasedArgument(1));
    }

    /**
     * @test
     * @expectedException \InvalidArgumentException
     **/
    public function include_declared_as_spam_invalid_argument()
    {
        $request = new TestRequest();
        $request->addArgument(new IsStatusDeclaredAsSpamArgument(1));
    }

    /**
     * @test
     * @expectedException \InvalidArgumentException
     **/
    public function normal_quarantine_invalid_argument()
    {
        $request = new TestRequest();
        $request->addArgument(new IsNoStatusArgument('asd'));
    }

    /**
     * @test
     * @expectedException \InvalidArgumentException
     **/
    public function start_time_invalid_argument()
    {
        $request = new TestRequest();
        $request->addArgument(new LowerLimitReceivedAtArgument('A new day has come.'));
    }

    /**
     * @test
     * @expectedException \InvalidArgumentException
     **/
    public function stop_time_invalid_argument()
    {
        $request = new TestRequest();
        $request->addArgument(new UpperLimitReceivedAtArgument('asdf'));
    }


    protected function getConfig(MockHandler $handler)
    {
        return parent::getConfig($handler)->setApiKey('test');
    }

}