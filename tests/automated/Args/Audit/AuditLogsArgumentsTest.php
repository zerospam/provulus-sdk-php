<?php
/**
 * Created by PhpStorm.
 * User: pbb
 * Date: 05/07/17
 * Time: 3:03 PM
 */

namespace automated\Args\Audit;

use ProvulusSDK\Client\ProvulusClient;
use ProvulusSDK\Enum\Audit\AuditedModelsEnum;
use ProvulusSDK\Enum\Audit\AuditLogTypeEnum;
use ProvulusTest\Request\TestRequest;
use ProvulusTest\TestCaseApiKey;

/**
 * Class AuditLogsArgumentsTest
 *
 * @package automated\Args\Audit
 */
class AuditLogsArgumentsTest extends TestCaseApiKey
{

    /**
     * @test
     **/
    public function auditLogsArguments()
    {
        $config = $this->preSuccess([]);

        $request = new TestRequest();
        $request->setAuditableId()
                ->setAuditableId(1)
                ->setModelType()
                ->setModelType(AuditedModelsEnum::DOMAIN())
                ->setEventType()
                ->setEventType(AuditLogTypeEnum::RESTORED())
                ->setUserId()
                ->setUserId(111);

        $client = new ProvulusClient($config);
        $client->processRequest($request);

        $this->validateUrl($config, '?auditable_id=1&auditable_type=domain&event=restored&user_id=111');
    }
}