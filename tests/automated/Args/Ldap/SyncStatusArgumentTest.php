<?php
/**
 * Created by PhpStorm.
 * User: pbb
 * Date: 27/06/17
 * Time: 10:18 AM
 */

namespace automated\Args\Ldap;


use GuzzleHttp\RequestOptions;
use ProvulusSDK\Client\Args\Search\Ldap\Synchronizations\SyncStatusArgument;
use ProvulusSDK\Enum\Ldap\LdapSyncStatusEnum;
use ProvulusTest\Request\TestRequest;
use ProvulusTest\TestCaseApiKey;

class SyncStatusArgumentTest extends TestCaseApiKey
{
    /**
     * @test
     **/
    public function filterLdapSyncsOptionsValidation()
    {
        $request = new TestRequest();
        $request->addArgument(new SyncStatusArgument(LdapSyncStatusEnum::REQ()))
                ->addArgument(new SyncStatusArgument(LdapSyncStatusEnum::AWAITING_CONFIRMATION()));

        $options = $request->requestOptions();

        $this->assertEquals('req;awaiting_confirmation', $options[RequestOptions::QUERY]['status']);
    }
}