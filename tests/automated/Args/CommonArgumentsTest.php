<?php
/**
 * Created by PhpStorm.
 * User: pbb
 * Date: 05/07/17
 * Time: 1:03 PM
 */

namespace automated\Args;

use Carbon\Carbon;
use ProvulusSDK\Client\ProvulusClient;
use ProvulusTest\Request\TestRequest;
use ProvulusTest\TestCaseApiKey;

class CommonArgumentsTest extends TestCaseApiKey
{
    /**
     * @test
     */
    public function auditLogsArguments()
    {
        $config = $this->preSuccess('{}');

        $request = new TestRequest();
        $request->setStartDateArgument()
                ->setStartDateArgument(Carbon::createFromDate(2017, 06, 05))
                ->setEndDateArgument()
                ->setEndDateArgument(Carbon::createFromDate(2017, 06, 07))
                ->setUserIdArgument()
                ->setUserIdArgument(28);

        $client = new ProvulusClient($config);
        $client->processRequest($request);

        $this->validateRequestUrl($config, ['?start_date=2017-06-05', '&end_date=2017-06-07', '&user_id=28']);
    }
}