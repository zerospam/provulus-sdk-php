<?php
/**
 * Created by PhpStorm.
 * User: aaflalo
 * Date: 15/02/17
 * Time: 2:27 PM
 */

namespace automated\Args;


use GuzzleHttp\RequestOptions;
use PHPUnit\Framework\TestCase;
use ProvulusSDK\Client\Args\Relationship\IncludeRelationArg;
use ProvulusTest\Request\TestRequest;

class MergeableArgumentTest extends TestCase
{

    /**
     * @test
     */
    public function add_argument_merge()
    {
        $request = new TestRequest();
        $request->addArgument(new IncludeRelationArg('test'))->addArgument(new IncludeRelationArg('superTest'));

        $options = $request->requestOptions();

        $this->assertEquals('test;superTest', $options[RequestOptions::QUERY]['include']);
    }

    /**
     * @test
     */
    public function remove_argument_merge()
    {
        $request = new TestRequest();
        $request->addArgument(new IncludeRelationArg('test'))
                ->addArgument(new IncludeRelationArg('superTest'))
                ->addArgument(new IncludeRelationArg('foo'))
                ->removeArgument(new IncludeRelationArg('superTest'));

        $options = $request->requestOptions();

        $this->assertEquals('test;foo', $options[RequestOptions::QUERY]['include']);
    }

    /**
     * @test
     * @expectedException \InvalidArgumentException
     */
    public function remove_not_present_arg()
    {
        $request = new TestRequest();
        $request->removeArgument(new IncludeRelationArg('superTest'));

    }
}