<?php
/**
 * Created by PhpStorm.
 * User: pbb
 * Date: 17/08/17
 * Time: 1:26 PM
 */

namespace automated\Args\User;

use ProvulusSDK\Client\ProvulusClient;
use ProvulusSDK\Enum\User\UserTypeEnum;
use ProvulusTest\Request\TestRequest;
use ProvulusTest\TestCaseApiKey;

class UserArgumentsTest extends TestCaseApiKey
{
    /**
     * @test
     **/
    public function addUsernameArgument()
    {
        $config = $this->preSuccess([]);

        $request = new TestRequest();
        $request->setUsernameArgument('username');

        $client = new ProvulusClient($config);
        $client->processRequest($request);

        $trans = $this->lastTransaction($config);
        $this->assertContains('?username=username', (string)$trans->request()->getUri());
    }

    /**
     * @test
     **/
    public function removeUserNameArgument()
    {
        $config = $this->preSuccess([]);

        $request = new TestRequest();
        $request->setUsernameArgument('username')
                ->setUsernameArgument();

        $client = new ProvulusClient($config);
        $client->processRequest($request);

        $trans = $this->lastTransaction($config);
        $this->assertNotContains('username', (string)$trans->request()->getUri());
    }

    /**
     * @test
     **/
    public function addDisplayNameArgument()
    {
        $config = $this->preSuccess([]);

        $request = new TestRequest();
        $request->setDisplayNameArgument('display');

        $client = new ProvulusClient($config);
        $client->processRequest($request);

        $trans = $this->lastTransaction($config);
        $this->assertContains('?display_name=display', (string)$trans->request()->getUri());
    }

    /**
     * @test
     **/
    public function addAddressArgument()
    {
        $config = $this->preSuccess([]);

        $request = new TestRequest();
        $request->setAddressArgument('email');

        $client = new ProvulusClient($config);
        $client->processRequest($request);

        $trans = $this->lastTransaction($config);
        $this->assertContains('?address=email', (string)$trans->request()->getUri());
    }

    /**
     * @test
     **/
    public function addUserTypeArgument()
    {
        $config = $this->preSuccess([]);

        $request = new TestRequest();
        $request->setUserTypeArgument(UserTypeEnum::LDAP());

        $client = new ProvulusClient($config);
        $client->processRequest($request);

        $trans = $this->lastTransaction($config);
        $this->assertContains('?user_type=ldap', (string)$trans->request()->getUri());
    }

}