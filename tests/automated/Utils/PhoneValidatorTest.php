<?php
/**
 * Created by PhpStorm.
 * User: pbb
 * Date: 15/12/16
 * Time: 3:26 PM
 */

namespace automated\Utils;


use Doctrine\Instantiator\Exception\InvalidArgumentException;
use ProvulusTest\TestCase;

/**
 * Class PhoneValidatorTest
 *
 * Tests the phone Validator
 *
 * @package automated\Utils
 */
class PhoneValidatorTest extends TestCase
{

    /**
     * @test
     */
    public function phone_number_valid_success()
    {
        self::assertTrue(\ProvulusSDK\Utils\Validator\PhoneNumberValidator::isValidPhoneNumber('4186674206'));
    }

    /**
     * @test
     * @expectedException InvalidArgumentException
     */
    public function phone_number_invalid_argument()
    {
        \ProvulusSDK\Utils\Validator\PhoneNumberValidator::isValidPhoneNumber(array());
    }

    /**
     * @test
     * @expectedException \libphonenumber\NumberParseException
     */
    public function phone_number_number_parse_exception()
    {
        \ProvulusSDK\Utils\Validator\PhoneNumberValidator::isValidPhoneNumber('hello');
    }

}