<?php
/**
 * Created by PhpStorm.
 * User: aaflalo
 * Date: 15/12/16
 * Time: 1:13 PM
 */

namespace automated\Utils;


use ProvulusSDK\Utils\ReflectionUtil;
use ProvulusTest\Obj\BasicEnum;
use ProvulusTest\Obj\BasicObj;
use ProvulusTest\Obj\BasicObjArrayEnum;
use ProvulusTest\Obj\BasicObjEnum;
use ProvulusTest\TestCase;

class ReflectionTest extends TestCase
{

    /**
     * @test
     */
    public function reflection_class_to_array() {
        $test = new BasicObj('bar');
        $this->assertArraySubset(array('foo' => 'bar'), ReflectionUtil::objToSnakeArray($test));
    }

    /**
     * @test
     */
    public function reflection_class_to_array_with_enum() {
        $test = new BasicObjEnum(BasicEnum::TEST());
        $this->assertArraySubset(array('enum' => 'test'), ReflectionUtil::objToSnakeArray($test));
    }

    /**
     * @test
     */
    public function reflection_class_to_array_with_array_enum() {
        $test = new BasicObjArrayEnum(array(BasicEnum::TEST()));
        $array = ReflectionUtil::objToSnakeArray($test);
        $this->assertArraySubset(array('enum_array' => array('test')), $array);
    }
}