<?php
/**
 * Created by PhpStorm.
 * User: aaflalo
 * Date: 15/12/16
 * Time: 1:37 PM
 */

namespace automated\Utils;


use ProvulusSDK\Utils\Arr;
use ProvulusTest\Obj\BasicEnum;
use ProvulusTest\TestCase;

class ArrTest extends TestCase
{

    /**
     * @test
     */
    public function transform_array_nested_array()
    {
        $array = array(
            'test' => BasicEnum::TEST(),
            'foo'  => array(BasicEnum::TEST()),
        );

        $this->assertArraySubset(array(
            'test' => 'test',
            'foo'  => array('test'),
        ), Arr::transformArray($array));
    }
}