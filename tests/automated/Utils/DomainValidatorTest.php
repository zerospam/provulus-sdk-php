<?php
/**
 * Created by PhpStorm.
 * User: pbb
 * Date: 23/12/16
 * Time: 3:27 PM
 */

namespace automated\Utils;


use ProvulusSDK\Utils\Validator\DomainValidator;
use ProvulusTest\TestCase;

/**
 * Class DomainValidatorTest
 *
 * Tests DomainValidator
 *
 * @package automated\Utils
 */
class DomainValidatorTest extends TestCase
{
    /**
     * @test
     */
    public function email_val_success()
    {
        \ProvulusSDK\Utils\Validator\DomainValidator::checkValidDomainSyntax('zerospam.ca');
    }

    /**
     * @test
     * @expectedException \InvalidArgumentException
     */
    public function email_val_failure()
    {
        DomainValidator::checkValidDomainSyntax('123.1234');
    }
}