<?php
/**
 * Created by PhpStorm.
 * User: pbb
 * Date: 14/06/17
 * Time: 2:05 PM
 */

namespace automated\Utils\LDAP;


use ProvulusSDK\Utils\Ldap\UserCreation;
use ProvulusTest\TestCase;

class UserCreationTest extends TestCase
{

    /**
     * @test
     **/
    public function UserCreationTest()
    {
        $array = [
            "is_create_users"        => true,
            "has_digest"             => true,
            "send_email_on_creation" => true,
            "created_at"             => "2017-06-13T14:04:46+00:00",
        ];

        $userCreation = UserCreation::fromArray($array);

        $toArray = $userCreation->toArray();
        self::assertArraySubset($array, $toArray);
        self::assertTrue($userCreation->isCreateUsers());
        self::assertTrue($userCreation->hasDigest());
        self::assertTrue($userCreation->sendEmailOnCreation());
    }
}