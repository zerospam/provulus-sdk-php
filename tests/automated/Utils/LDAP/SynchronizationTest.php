<?php
/**
 * Created by PhpStorm.
 * User: pbb
 * Date: 14/06/17
 * Time: 2:15 PM
 */

namespace automated\Utils\LDAP;


use ProvulusSDK\Utils\Ldap\Synchronization;
use ProvulusTest\TestCase;

class SynchronizationTest extends TestCase
{

    /**
     * @test
     **/
    public function synchronizationCoverageTest()
    {

        $array = [
            "is_automatic" => true,
            "frequency"    => "every_day",
        ];

        $sync = Synchronization::fromArray($array);

        self::assertArraySubset($array, $sync->toArray());
        self::assertTrue($sync->isAutomatic());
        self::assertEquals("every_day", $sync->getFrequency()->getValue());
    }
}