<?php
/**
 * Created by PhpStorm.
 * User: pbb
 * Date: 20/02/17
 * Time: 4:18 PM
 */

namespace automated\Utils;


use ProvulusTest\TestCase;

/**
 * Class AddressAndNameTest
 *
 * Test for data container AddressAndName class
 *
 * @package automated\Utils
 */
class EmailAddressTest extends TestCase
{

    /**
     * @test
     **/
    public function array_To_AddressAndName()
    {
        $array = [
            'local_part'  => 'phiphi',
            'domain_part' => 'boubou.com',
            'name'        => 'phiphi_boubou',
        ];

        $object = \ProvulusSDK\Utils\Quarantine\EmailAddress::fromArray($array);

        self::assertObjectHasAttribute('domainPart', $object);
        self::assertEquals('boubou.com', $object->getDomainPart());
        self::assertEquals('phiphi', $object->getLocalPart());
        self::assertEquals('phiphi_boubou', $object->getName());
    }

      /**
     * @test
     **/
    public function AddressAndNameToArray()
    {
        $array = [
            'local_part'  => 'phiphi',
            'domain_part' => 'boubou.com',
            'name'        => 'phiphi_boubou',
        ];

        $object = \ProvulusSDK\Utils\Quarantine\EmailAddress::fromArray($array);

        $array = $object->toArray();

        self::assertTrue(is_array($array));
        self::assertContains('boubou.com', $array);
        self::assertArrayHasKey('domain_part', $array);
    }
}