<?php
/**
 * Created by PhpStorm.
 * User: aaflalo
 * Date: 16/12/16
 * Time: 2:38 PM
 */

namespace automated\Utils;


use ProvulusSDK\Utils\Validator\EmailValidator;
use ProvulusTest\TestCase;

class EmailValidatorTest extends TestCase
{

    /**
     * @test
     */
    public function email_val_success() {
        \ProvulusSDK\Utils\Validator\EmailValidator::checkValidEmailConstraint('test@test.com');
    }

    /**
     * @test
     * @expectedException \InvalidArgumentException
     */
    public function email_val_failure() {
        EmailValidator::checkValidEmailConstraint('test');
    }
}