<?php
/**
 * Created by PhpStorm.
 * User: pbb
 * Date: 15/05/17
 * Time: 3:46 PM
 */

namespace automated\Requests\Token;

use ProvulusSDK\Client\Errors\Validation\ValidationError;
use ProvulusSDK\Client\ProvulusClient;
use ProvulusSDK\Client\Request\Token\TokenValidateRequest;
use ProvulusSDK\Client\Response\EmptyResponse;
use ProvulusSDK\Exception\API\ValidationException;
use ProvulusTest\TestCaseApiKey;

/**
 * Class TokenValidateRequestTest
 *
 * @package automated\Requests\Token
 */
class TokenValidateRequestTest extends TestCaseApiKey
{

    /**
     * @test
     * @expectedException \ProvulusSDK\Exception\API\ValidationException
     **/
    public function validate_token_failure()
    {
        $jsonRequest
            = '{
  "token":"H4sIAAAAAAAAA7WRQU8DIRCF_4rhbBtYdoGlF7V68FRj6o3EzMJUibtLLbuJtel_l8Wa6NUolxmYL--9DAcS_VNPNGG8amiBQlIHXDZO1KBcXVlVb5StmkoJtBysUpRLtOikgkJuGkttUUCNNTknQ3jBSWqlhdSGLMdubMdozNUYfY8xdeuJMOYO9m0AZ8zN29bv0JhL53YZODX3-DpiHL44ork-RC2rpGpGms7faWe5MeLuNvksvK4LLihjTC2SYflfhtiBb5Nf1MXkAX0PrY84BwddvNjDcwhzG7pMSPqbFLlC0-JP3_w6-NBfw4BJfqUZn_4Kdk1IMqd6WvgUzX1yKahIt4IyOaPVjMkzVmomdCnnTNSMlpnJWoPv8D30-Djst5h3ytNIfZtkdkIf1svUH4_k-AF83JzDiAIAAA"
}';

        $jsonResult
            = '{  
   "errors":{  
      "token":[  
         {  
            "code":"payload_token_error",
            "message":"The token is either invalid or has expired.",
            "value":"H4sIAAAAAAAAA7WRQU8DIRCF_4rhbBtYdoGlF7V68FRj6o3EzMJUibtLLbuJtel_l8Wa6NUolxmYL--9DAcS_VNPNGG8amiBQlIHXDZO1KBcXVlVb5StmkoJtBysUpRLtOikgkJuGkttUUCNNTknQ3jBSWqlhdSGLMdubMdozNUYfY8xdeuJMOYO9m0AZ8zN29bv0JhL53YZODX3-DpiHL44ork-RC2rpGpGms7faWe5MeLuNvksvK4LLihjTC2SYflfhtiBb5Nf1MXkAX0PrY84BwddvNjDcwhzG7pMSPqbFLlC0-JP3_w6-NBfw4BJfqUZn_4Kdk1IMqd6WvgUzX1yKahIt4IyOaPVjMkzVmomdCnnTNSMlpnJWoPv8D30-Djst5h3ytNIfZtkdkIf1svUH4_k-AF83JzDiAIAAA"
         }
      ]
   }
}';

        $request = new TokenValidateRequest();
        $request->setToken('H4sIAAAAAAAAA7WRQU8DIRCF_4rhbBtYdoGlF7V68FRj6o3EzMJUibtLLbuJtel_l8Wa6NUolxmYL--9DAcS_VNPNGG8amiBQlIHXDZO1KBcXVlVb5StmkoJtBysUpRLtOikgkJuGkttUUCNNTknQ3jBSWqlhdSGLMdubMdozNUYfY8xdeuJMOYO9m0AZ8zN29bv0JhL53YZODX3-DpiHL44ork-RC2rpGpGms7faWe5MeLuNvksvK4LLihjTC2SYflfhtiBb5Nf1MXkAX0PrY84BwddvNjDcwhzG7pMSPqbFLlC0-JP3_w6-NBfw4BJfqUZn_4Kdk1IMqd6WvgUzX1yKahIt4IyOaPVjMkzVmomdCnnTNSMlpnJWoPv8D30-Djst5h3ytNIfZtkdkIf1svUH4_k-AF83JzDiAIAAA');

        $config = $this->preFailure($jsonResult);

        $client = new ProvulusClient($config);

        try {
            $client->processRequest($request);
        } catch (ValidationException $exception) {
            $this->validateRequest($config, $jsonRequest);

            $validationData = $exception->getValidationData();
            /** @var ValidationError $first */
            $first = $validationData['token']->getValidationErrorCollection()->first();
            $this->assertInstanceOf(ValidationError::class, $first);
            $this->assertEquals('payload_token_error', $first->getCode());

            throw $exception;
        }
    }

    /**
     * @test
     **/
    public function validate_token_success()
    {
        $jsonRequest
            = '{
  "token":"H4sIAAAAAAAAA7WRQU8DIRCF_4rhbBtYdoGlF7V68FRj6o3EzMJUibtLLbuJtel_l8Wa6NUolxmYL--9DAcS_VNPNGG8amiBQlIHXDZO1KBcXVlVb5StmkoJtBysUpRLtOikgkJuGkttUUCNNTknQ3jBSWqlhdSGLMdubMdozNUYfY8xdeuJMOYO9m0AZ8zN29bv0JhL53YZODX3-DpiHL44ork-RC2rpGpGms7faWe5MeLuNvksvK4LLihjTC2SYflfhtiBb5Nf1MXkAX0PrY84BwddvNjDcwhzG7pMSPqbFLlC0-JP3_w6-NBfw4BJfqUZn_4Kdk1IMqd6WvgUzX1yKahIt4IyOaPVjMkzVmomdCnnTNSMlpnJWoPv8D30-Djst5h3ytNIfZtkdkIf1svUH4_k-AF83JzDiAIAAA"
}';

        $jsonResult = [];

        $request = new TokenValidateRequest();
        $request->setToken('H4sIAAAAAAAAA7WRQU8DIRCF_4rhbBtYdoGlF7V68FRj6o3EzMJUibtLLbuJtel_l8Wa6NUolxmYL--9DAcS_VNPNGG8amiBQlIHXDZO1KBcXVlVb5StmkoJtBysUpRLtOikgkJuGkttUUCNNTknQ3jBSWqlhdSGLMdubMdozNUYfY8xdeuJMOYO9m0AZ8zN29bv0JhL53YZODX3-DpiHL44ork-RC2rpGpGms7faWe5MeLuNvksvK4LLihjTC2SYflfhtiBb5Nf1MXkAX0PrY84BwddvNjDcwhzG7pMSPqbFLlC0-JP3_w6-NBfw4BJfqUZn_4Kdk1IMqd6WvgUzX1yKahIt4IyOaPVjMkzVmomdCnnTNSMlpnJWoPv8D30-Djst5h3ytNIfZtkdkIf1svUH4_k-AF83JzDiAIAAA');

        $config = $this->preSuccess($jsonResult);

        $client = new ProvulusClient($config);
        $client->processRequest($request);

        $response = $request->getResponse();

        $this->validateRequest($config, $jsonRequest);
        $this->validateRequestUrl($config, ['validate/token']);
        $this->assertInstanceOf(EmptyResponse::class, $response);
    }

}