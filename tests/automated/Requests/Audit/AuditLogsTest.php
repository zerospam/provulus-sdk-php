<?php
/**
 * Created by PhpStorm.
 * User: pbb
 * Date: 05/07/17
 * Time: 11:44 AM
 */

namespace automated\Requests\Audit;


use Carbon\Carbon;
use ProvulusSDK\Client\ProvulusClient;
use ProvulusSDK\Client\Request\Audit\AuditLogsIndexRequest;
use ProvulusSDK\Enum\Audit\AuditedModelsEnum;
use ProvulusTest\TestCaseApiKey;

/**
 * Class AuditLogsTest
 *
 * @package automated\Requests\Audit
 */
class AuditLogsTest extends TestCaseApiKey
{

    /**
     * @test
     **/
    public function audits_index_success()
    {
        $responseBody
            = '{  
   "data":[  
      {  
         "type":"audits",
         "id":"5590",
         "attributes":{  
            "user_id":null,
            "real_user_id":null,
            "event":"updated",
            "auditable_id":144,
            "auditable_type":"LdapConfig",
            "url":"console",
            "ip_address":"127.0.0.1",
            "organization_id":567,
            "created_at":"2017-06-29 20:54:44.000000 +00:00",
            "user_agent":"Symfony\/3.X",
            "custom_message":"",
            "custom_fields":[  

            ],
            "elapsed_time":"il y a 6 jours",
            "label":"mis \u00e0 jour",
            "values":[  

            ]
         }
      },
      {  
         "type":"audits",
         "id":"5589",
         "attributes":{  
            "user_id":null,
            "real_user_id":null,
            "event":"updated",
            "auditable_id":144,
            "auditable_type":"LdapConfig",
            "url":"console",
            "ip_address":"127.0.0.1",
            "organization_id":567,
            "created_at":"2017-06-29 20:54:40.000000 +00:00",
            "user_agent":"Symfony\/3.X",
            "custom_message":"",
            "custom_fields":[  

            ],
            "elapsed_time":"il y a 6 jours",
            "label":"mis \u00e0 jour",
            "values":[  

            ]
         }
      },
      {  
         "type":"audits",
         "id":"5588",
         "attributes":{  
            "user_id":null,
            "real_user_id":null,
            "event":"updated",
            "auditable_id":144,
            "auditable_type":"LdapConfig",
            "url":"console",
            "ip_address":"127.0.0.1",
            "organization_id":567,
            "created_at":"2017-06-29 20:54:36.000000 +00:00",
            "user_agent":"Symfony\/3.X",
            "custom_message":"",
            "custom_fields":[  

            ],
            "elapsed_time":"il y a 6 jours",
            "label":"mis \u00e0 jour",
            "values":[  

            ]
         }
      },
      {  
         "type":"audits",
         "id":"5587",
         "attributes":{  
            "user_id":null,
            "real_user_id":null,
            "event":"updated",
            "auditable_id":144,
            "auditable_type":"LdapConfig",
            "url":"console",
            "ip_address":"127.0.0.1",
            "organization_id":567,
            "created_at":"2017-06-29 20:54:32.000000 +00:00",
            "user_agent":"Symfony\/3.X",
            "custom_message":"",
            "custom_fields":[  

            ],
            "elapsed_time":"il y a 6 jours",
            "label":"mis \u00e0 jour",
            "values":[  

            ]
         }
      },
      {  
         "type":"audits",
         "id":"5586",
         "attributes":{  
            "user_id":null,
            "real_user_id":null,
            "event":"updated",
            "auditable_id":144,
            "auditable_type":"LdapConfig",
            "url":"console",
            "ip_address":"127.0.0.1",
            "organization_id":567,
            "created_at":"2017-06-29 20:53:01.000000 +00:00",
            "user_agent":"Symfony\/3.X",
            "custom_message":"",
            "custom_fields":[  

            ],
            "elapsed_time":"il y a 6 jours",
            "label":"mis \u00e0 jour",
            "values":[  

            ]
         }
      }
   ],
   "meta":{  
      "pagination":{  
         "total":5,
         "count":5,
         "per_page":15,
         "current_page":1,
         "total_pages":1
      }
   },
   "links":{  
      "self":"http:\/\/cumulus.local\/api\/v1\/orgs\/567\/audits?page=1",
      "first":"http:\/\/cumulus.local\/api\/v1\/orgs\/567\/audits?page=1",
      "last":"http:\/\/cumulus.local\/api\/v1\/orgs\/567\/audits?page=1"
   }
}';

        $requestBody = [];


        $config  = $this->preSuccess($responseBody);
        $request = new AuditLogsIndexRequest();

        $timezone = new \DateTimeZone('America/Winnipeg');

        $request->setOrganizationId(567)
                ->setAuditableId(144)
                ->setStartDateArgument(Carbon::parse('2017-06-29 15:54:00', $timezone))
                ->setEndDateArgument(Carbon::parse('2017-06-29 16:55:00', $timezone))
                ->setModelType(AuditedModelsEnum::LDAP_CONFIG());

        $client = new ProvulusClient($config);

        $client->processRequest($request);

        $this->validateRequest($config, $requestBody);
        $this->validateUrl($config,
            'orgs/567/audits?auditable_id=144&start_date=2017-06-29 15:54:00&end_date=2017-06-29 16:55:00&auditable_type=ldap_config');
    }
}