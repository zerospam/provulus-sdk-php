<?php
/**
 * Created by PhpStorm.
 * User: aaflalo
 * Date: 30/11/16
 * Time: 3:43 PM
 */

namespace automated\Auth;

use GuzzleHttp\Exception\BadResponseException;
use GuzzleHttp\Handler\MockHandler;
use GuzzleHttp\Psr7\Request;
use GuzzleHttp\Psr7\Response;
use Mockery as m;
use ProvulusSDK\Client\ProvulusClient;
use ProvulusSDK\Client\Request\Auth\Token\UserCryptToken;
use ProvulusSDK\Client\Request\Auth\TwoFactorAuthenticationSMSRequest;
use ProvulusSDK\Exception\API\UnauthorizedException;
use ProvulusSDK\Exception\API\ValidationException;
use ProvulusTest\BasicJWTStorage;
use ProvulusTest\Obj\Conf\TestConf;
use ProvulusTest\Request\TestRequest;
use ProvulusTest\TestCase;

class LoginTest extends TestCase
{
    /** @test */
    public function client_will_login()
    {

        $mockHandler = new MockHandler([
            new Response(200, [], '{"token": "testToken"}'),
        ]);

        $config = $this->getConfig($mockHandler);


        $client   = new ProvulusClient($config);
        $response = $client->login('root', 'password');


        $this->assertEquals('testToken', $response->token());
        $this->assertEquals('testToken', $config->getJwtStorage()
                                                ->getToken(), 'The token given need to be the token set');

        $trans  = $this->lastTransaction($config);
        $decode = \GuzzleHttp\json_decode($trans->request()->getBody()->getContents(), true);

        $this->assertArraySubset([
            'password' => 'password',
            'username' => 'root',
        ], $decode);


    }

    /** @test */
    public function client_will_login_with_password_token()
    {
        $mockHandler = new MockHandler([
            new Response(200, [], '{"token": "testToken"}'),
        ]);

        $config = $this->getConfig($mockHandler);

        $client   = new ProvulusClient($config);
        $token    = new UserCryptToken('test', 'super@testing.com', 'Nice test', 'test');
        $response = $client->loginToken('root', $token);


        $this->assertEquals('testToken', $response->token());
        $this->assertEquals('testToken', $config->getJwtStorage()
                                                ->getToken(), 'The token given need to be the token set');

        $trans  = $this->lastTransaction($config);
        $decode = \GuzzleHttp\json_decode($trans->request()->getBody()->getContents(), true);

        $this->assertArraySubset([
            'password_token' => '4b54b81bc9cbfd5d42bcf28e98fd44dc2111108a11ec6e4dd67f5f1ed36ecc75c43baaf6141232c05030ab4f971dbd8e7f4c2e499b78cc0c13891ce41a1d00e6',
            'username'       => 'root',
        ], $decode);


    }

    /** @test */
    public function logged_client_send_jwt_token()
    {
        $mockHandler = new MockHandler([
            new Response(201),
        ]);

        $config = $this->getConfig($mockHandler);
        $config->getJwtStorage()->updateToken('oldToken');


        $client = new ProvulusClient($config);
        $client->processRequest(new TestRequest());

        $trans = $this->lastTransaction($config);

        $this->assertArraySubset(['Bearer oldToken'], $trans->request()->getHeader('Authorization'));


    }

    /** @test */
    public function logged_client_send_update_jwt_token()
    {
        $mockHandler = new MockHandler([
            new Response(201, [
                'Authorization' => 'Bearer newToken',
            ]),
        ]);

        $config = $this->getConfig($mockHandler);
        $config->getJwtStorage()->updateToken('oldToken');


        $client = new ProvulusClient($config);
        $client->processRequest(new TestRequest());

        $trans = $this->lastTransaction($config);

        $this->assertArraySubset(['Bearer oldToken'], $trans->request()->getHeader('Authorization'));

        $this->assertEquals('newToken', $config->getJwtStorage()->getToken(), "The token hasn't been updated");

    }

    /**
     * @test
     * @expectedException \ProvulusSDK\Exception\API\ValidationException
     */
    public function logged_client_send_update_jwt_token_even_on_request_error()
    {
        $mockHandler = new MockHandler([
            new BadResponseException('Bad', new Request('GET', 'test'), new Response(422, ['Authorization' => 'Bearer newToken'], '{"errors": []}')),
        ]);

        $config = $this->getConfig($mockHandler);
        $config->getJwtStorage()->updateToken('oldToken');


        $client = new ProvulusClient($config);
        try {
            $client->processRequest(new TestRequest());

        } catch (ValidationException $e) {

            $trans = $this->lastTransaction($config);

            $this->assertArraySubset(['Bearer oldToken'], $trans->request()->getHeader('Authorization'));

            $this->assertEquals('newToken', $config->getJwtStorage()->getToken(), "The token hasn't been updated");
            throw $e;
        }

    }

    /**
     * @test
     * @expectedException \ProvulusSDK\Exception\API\UnauthorizedException
     */
    public function client_wrong_username_password()
    {
        $mockHandler = new MockHandler([
            new BadResponseException('Bad', new Request('GET', 'test'), new Response(401)),
        ]);

        $config = $this->getConfig($mockHandler);


        $client = new ProvulusClient($config);
        try {
            $client->login('test', 'test');

        } catch (UnauthorizedException $e) {

            $trans = $this->lastTransaction($config);

            $this->assertArraySubset([
                'password' => 'test',
                'username' => 'test',
            ], \GuzzleHttp\json_decode($trans->request()->getBody()->getContents(), true));

            $this->assertNull($config->getJwtStorage()->getToken(), 'No token should have been set');
            throw $e;
        }


    }

    /**
     * @test
     * @expectedException \ProvulusSDK\Exception\API\UnauthorizedException
     */
    public function client_wrong_username_password_token()
    {
        $mockHandler = new MockHandler([
            new BadResponseException('Bad', new Request('GET', 'test'), new Response(401)),
        ]);

        $config = $this->getConfig($mockHandler);


        $client = new ProvulusClient($config);
        try {
            $token = new UserCryptToken('test', 'super@sdsdsd.com', 'sdsd test', 'test');
            $client->loginToken('test', $token);

        } catch (UnauthorizedException $e) {

            $trans = $this->lastTransaction($config);

            $decoded = \GuzzleHttp\json_decode($trans->request()->getBody()->getContents(), true);
            $this->assertArraySubset([
                'password_token' => '00276f6eb281ec8298aa67bb906c4c31b5d739ca90987aa921261bd70880c0342f9b8a0c1f3706d1648bbee1a777f75020230f5e0e13d5da9d57612896a03d25',
                'username'       => 'test',
            ], $decoded);

            $this->assertNull($config->getJwtStorage()->getToken(), 'No token should have been set');
            throw $e;
        }
    }

    /**
     * @test
     */
    public function tfa_sms_success()
    {
        $jsonResponse = '{}';
        $jsonRequest = '{
            "username": "user@email.com",
            "password": "Password1"
        }';
        $request = (new TwoFactorAuthenticationSMSRequest('user@email.com'))
            ->setPassword('Password1');


        $config = $this->preSuccess($jsonResponse);
        $client = new ProvulusClient($config);

        $client->processRequest($request);

        $this->validateRequest($config, $jsonRequest);
    }

    /**
     * @param MockHandler $handler
     *
     * @return TestConf
     */
    protected function getConfig(MockHandler $handler)
    {
        $config = parent::getConfig($handler);

        return $config
            ->setJwtStorage(new BasicJWTStorage())
            ->setEncryptionKey('base64:5hZn544RIGWi68/H6C7wKdI7CUv868mTDRVFSDyJEyGZliSM9iIdlrKB5ocJEKz+JduotMFXuL7E4bE7BcGuow==');
    }
}