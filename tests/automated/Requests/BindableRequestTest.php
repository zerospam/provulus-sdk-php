<?php
/**
 * Created by PhpStorm.
 * User: aaflalo
 * Date: 14/12/16
 * Time: 4:58 PM
 */

namespace automated\Requests;


use ProvulusTest\Request\BindableMultiTestRequest;
use ProvulusTest\Request\BindableTestRequest;
use ProvulusTest\TestCase;

class BindableRequestTest extends TestCase
{

    /**
     * @test
     */
    public function bindable_replace_bindings() {
        $bindableRequest = new BindableTestRequest();
        $bindableRequest->setNiceId(4)->setTestId(5);

        $this->assertEquals('test/5/nice/4',$bindableRequest->routeUrl());
    }

    /**
     * @test
     */
    public function bindable_replace_bindings_multiple() {
        $bindableRequest = new BindableMultiTestRequest();
        $bindableRequest->setNiceId(4)->setTestId(5);

        $this->assertEquals('test/5/nice/4/super/4',$bindableRequest->routeUrl());
    }

    /**
     * @test
     */
    public function bindable_test_to_array_without_bindings() {
        $bindableRequest = new BindableMultiTestRequest();
        $bindableRequest->setNiceId(4)->setTestId(5)->setTest('foo');

        $toArray = $bindableRequest->toArray();
        $this->assertArrayNotHasKey('bindings', $toArray);
        $this->assertArrayHasKey('test', $toArray);
        $this->assertEquals('foo', $toArray['test']);
    }

    /**
     * @test
     * @expectedException \InvalidArgumentException
     */
    public function bindable_failure_exception_object() {
        $bindableRequest = new BindableMultiTestRequest();
        $bindableRequest->setNiceId(new \stdClass());
    }

    /**
     * @test
     * @expectedException \InvalidArgumentException
     */
    public function bindable_failure_exception_array() {
        $bindableRequest = new BindableMultiTestRequest();
        $bindableRequest->setNiceId(array());
    }
}