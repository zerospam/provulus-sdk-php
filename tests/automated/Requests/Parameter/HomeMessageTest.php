<?php
/**
 * Created by PhpStorm.
 * User: ycoutu
 * Date: 21/07/17
 * Time: 11:09 AM
 */

namespace automated\Requests\Parameter;

use GuzzleHttp\Handler\MockHandler;
use ProvulusSDK\Client\ProvulusClient;
use ProvulusSDK\Client\Request\Parameter\HomeMessage\ReadHomeMessageRequest;
use ProvulusSDK\Client\Response\Parameter\HomeMessage\HomeMessageResponse;
use ProvulusSDK\Config\ProvulusConfiguration;
use ProvulusSDK\Enum\Locale\LocaleEnum;
use ProvulusTest\TestCase;

class HomeMessageTest extends TestCase
{
    /** @test */
    public function read_home_message_success()
    {
        $jsonResult =
            '{
                "data": {
                    "type": "home_message",
                    "id": "2",
                    "attributes": {
                        "language_code": "en_us",
                        "message": "This is an amazing message! Wow!",
                        "image": "331:ce0dbb29d15222e2317c5de47ef0586a",
                        "created_at": null,
                        "updated_at": null
                    }
                }
            }';

        $jsonRequest = '{}';

        $config = $this->preSuccess($jsonResult);

        $request = new ReadHomeMessageRequest();
        $request->setLanguage(LocaleEnum::EN_US());

        $client = new ProvulusClient($config);
        /** @var HomeMessageResponse $response */
        $response = $client->processRequest($request);

        $this->validateRequest($config, $jsonRequest);

        self::assertEquals("This is an amazing message! Wow!", $response->message);
    }
    /**
     * Gets config for tests
     *
     * @param MockHandler $handler
     *
     * @return ProvulusConfiguration
     */
    protected function getConfig(MockHandler $handler)
    {
        return parent::getConfig($handler)->setApiKey('test');
    }
}
