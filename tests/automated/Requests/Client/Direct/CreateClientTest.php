<?php
/**
 * Created by PhpStorm.
 * User: aaflalo
 * Date: 07/12/16
 * Time: 9:41 AM
 */

namespace automated\Client\Direct;


use DateTimeZone;
use GuzzleHttp\Handler\MockHandler;
use ProvulusSDK\Client\Errors\Validation\ValidationError;
use ProvulusSDK\Client\ProvulusClient;
use ProvulusSDK\Client\Request\Client\Direct\CreateDirectClientRequest;
use ProvulusSDK\Client\Request\Client\Direct\CreateResellerRequest;
use ProvulusSDK\Client\Response\Client\ClientCreationResponse;
use ProvulusSDK\Enum\Locale\LocaleEnum;
use ProvulusSDK\Enum\User\UserTypeEnum;
use ProvulusSDK\Exception\API\APICrashException;
use ProvulusSDK\Exception\API\ValidationException;
use ProvulusTest\Request\TestRequest;
use ProvulusTest\TestCase;

class CreateClientTest extends TestCase
{

    /**
     * @test
     */
    public function create_client_success()
    {
        $jsonResult
            = <<<JSON
{
    "data": {
        "type": "client",
        "id":"2788",
        "attributes":[],
        "relationships":{
            "organization":{
                "data":{
                    "type":"orgs",
                    "id":"2788"
                }
            },
            "domain":{
                "data":{
                    "type":"domains",
                    "id":"4461"
                }
            },
            "user":{
                "data":{
                    "type":"users",
                    "id":"9359"
                }
            }
        }
    },
    "included":[{
        "type":"orgs",
        "id":"2788",
        "attributes":{
            "support_organization_id":2773,
            "billing_organization_id":2773,
            "is_access_allowed":true,
            "is_billing_organization":false,
            "is_support_organization":true,
            "corporate_name":"new.cool Client",
            "corporate_name_normalized":"new.cool client",
            "portal_prefix":null,"default_language_code":
            "en_us",
            "timezone_code":{"timezone_type":3,"timezone":"America\/Montreal"},
            "note_shared":null,
            "quar_expiry_in_days":14,
            "logo_reference":null,
            "is_deact_req":false,
            "deact_req_reason":null,
            "deact_req_from_display_name":null,
            "deact_req_from_corporate_name":null,
            "deact_req_issued_at":null,
            "deact_req_target_date":null,
            "deact_req_executed_at":null,
            "is_active":true,
            "version":0,
            "created_at":"2016-12-07 18:21:14.000000 +00:00",
            "updated_at":"2016-12-07 18:21:14.000000 +00:00",
            "dsbl_portal_selfprov":false,
            "dsbl_org_selfprov":false,
            "currency":"cad",
            "renewal_messages_allowed":true,
            "renewal_note":null,
            "yearly_renewal_date":"2017-12-07 00:00:00.000000 +00:00",
            "renewal_state":"ok",
            "renewal_msg_sent_at":null,
            "next_renewal_msg":null,
            "billing_period":null,
            "billing_base_date":null,
            "shows_powered_by_logo":true,
            "outbound_dg_bounce":null,
            "outbound_dg_default":null,
            "outbound_global_domains":true,
            "outbound_filter_active":false,
            "outbound_relay":null,
            "is_distributor_organization":false,
            "distributor_id":null,
            "external_distributor_id":null,
            "activation_date":null,
            "support_organization_name":"group.hole Reseller",
            "billing_organization_name":"group.hole Reseller",
            "distributor_organization_name":null
        }
    },{
        "type":"domains",
        "id":"4461",
        "attributes":{
            "domain_group_id":2907,
            "organization_id":2788,
            "name":"new.cool",
            "description":null,
            "transport":"mailcatcher",
            "transport_protocol":"smtp",
            "transport_port":25,
            "transport_use_mx":false,
            "transport_hold":false,
            "is_active":true,
            "created_at":"2016-12-07 18:21:14.000000 +00:00",
            "updated_at":"2016-12-07 18:21:14.000000 +00:00",
            "is_active_backup":null,
            "is_unconditional_delete":false,
            "count_start_date":null,
            "mailbox_count":null,
            "rejected_mailbox_count":null,
            "nb_declared_mailboxes":10,
            "domain_type":"standard",
            "dmbcount_updated_at":null,
            "last_nb_declared_mailboxes":null,
            "is_dmbcount_declared":true,
            "email":"client@new.cool",
            "is_check_transport_success":false,
            "is_check_mx_success":false,
            "transport_server_type":"BASIC",
            "activation_date":null,
            "annual_price":600,
            "organization_name":"new.cool Client"
        }
    },{
        "type":"users",
        "id":"9359",
        "attributes":{
            "is_access_allowed":true,
            "usr_type_code":"admin",
            "email_address": "client@new.cool",
            "display_name":"Suis un acheteur, Je",
            "language_code":"en_us",
            "name":"Je",
            "surname":"Suis un acheteur"
        }
    }]
}
JSON;
        $jsonRequest
            = <<<JSON
{
    "first_name":"Je",
    "timezone":"America/Montreal",
    "last_name":"suis un acheteur",
    "domain":"new.cool",
    "phone":"5146244520",
    "email":"client@new.cool",
    "organization_name":"new.cool Client",
    "transport":"mailcatcher",
    "transport_port":25,
    "mailboxes":100,
    "language":"en_US"
}
JSON;
        $config = $this->preSuccess($jsonResult);


        $request = new CreateDirectClientRequest();
        $request->setEmail('client@new.cool')
                ->setPhone('5146244520')
                ->setDomain('new.cool')
                ->setLanguage(LocaleEnum::EN_US())
                ->setLastName('suis un acheteur')
                ->setFirstName('Je')
                ->setTransportPort(25)
                ->setTransport('mailcatcher')
                ->setOrganizationName('new.cool Client')
                ->setMailboxes(100)
                ->setTimezone(new DateTimeZone('America/Montreal'));

        $client = new ProvulusClient($config);
        /**
         * @var $response ClientCreationResponse
         */
        $response = $client->processRequest($request);

        $this->validateRequest($config, $jsonRequest);

        $this->assertEquals(2788, $response->organization()->id(), 'Organization ID incorrect');
        $this->assertEquals('new.cool Client', $response->organization()->corporate_name, 'Organization Name incorrect');
        $this->assertEquals(4461, $response->domain()->id(), 'Domain ID incorrect');
        $this->assertTrue(isset($response->domain()->name));
        $this->assertEquals('new.cool', $response->domain()->name, 'Domain Name incorrect');
        $this->assertEquals(9359, $response->user()->id(), 'User ID incorrect');
        $this->assertTrue(isset($response->user()->usr_type_code));
        $this->assertTrue($response->user()->usr_type_code->is(UserTypeEnum::ADMIN()), 'User ID isn\'t an admin');

        $this->assertEquals($response, $response->organization()->getParentResponse());
    }


    /**
     * @test
     * @expectedException \ProvulusSDK\Exception\API\ValidationException
     */
    public function create_client_failure()
    {

        $jsonResult
            = '{"errors":{"organization_name":[{"code":"unique_corporate_name_error","message":"There is already an organization with this name.","value":"new.cool Client"}]}}';
        $jsonRequest
            = '{"first_name":"Je","last_name":"suis un acheteur","domain":"new.cool","phone":"5146244520","email":"client@new.cool","organization_name":"new.cool Client","transport":"mailcatcher","transport_port":25,"mailboxes":100,"language":"en_US"}';

        $config = $this->preFailure($jsonResult);


        $request = new CreateDirectClientRequest();
        $request->setEmail('client@new.cool')
                ->setPhone('5146244520')
                ->setDomain('new.cool')
                ->setLanguage(LocaleEnum::EN_US())
                ->setLastName('suis un acheteur')
                ->setFirstName('Je')
                ->setTransportPort(25)
                ->setTransport('mailcatcher')
                ->setOrganizationName('new.cool Client')
                ->setMailboxes(100);

        $client = new ProvulusClient($config);
        /**
         * @var $response ClientCreationResponse
         */
        try {
            $client->processRequest($request);
        } catch (ValidationException $validationException) {

            $this->validateRequest($config, $jsonRequest);

            $validationData = $validationException->getValidationData();
            $this->assertArrayHasKey('organization_name', $validationData);
            $this->assertEquals('new.cool Client', $validationData['organization_name']->getValidationErrorCollection()
                                                                                       ->first()->getValue());
            throw $validationException;
        }
    }


    /**
     * @test
     */
    public function create_reseller_success()
    {

        $jsonResult = <<<JSON
{
    "data":{
        "type":"client",
        "id":"2788",
        "attributes":[
            
        ],
        "relationships":{
            "organization":{
                "data":{
                    "type":"orgs",
                    "id":"2788"
                }
            },
            "domain":{
                "data":{
                    "type":"domains",
                    "id":"4461"
                }
            },
            "user":{
                "data":{
                    "type":"users",
                    "id":"9359"
                }
            }
        }
    },
    "included":[
        {
            "type":"orgs",
            "id":"2788",
            "attributes":{
                "support_organization_id":2773,
                "billing_organization_id":2773,
                "is_access_allowed":true,
                "is_billing_organization":true,
                "is_support_organization":false,
                "corporate_name":"new.cool Client",
                "corporate_name_normalized":"new.cool client",
                "portal_prefix":null,
                "default_language_code":"en_us",
                "timezone_code":{
                    "timezone_type":3,
                    "timezone":"America\/Montreal"
                },
                "note_shared":null,
                "quar_expiry_in_days":14,
                "logo_reference":null,
                "is_deact_req":false,
                "deact_req_reason":null,
                "deact_req_from_display_name":null,
                "deact_req_from_corporate_name":null,
                "deact_req_issued_at":null,
                "deact_req_target_date":null,
                "deact_req_executed_at":null,
                "is_active":true,
                "version":0,
                "created_at":"2016-12-07 18:21:14.000000 +00:00",
                "updated_at":"2016-12-07 18:21:14.000000 +00:00",
                "dsbl_portal_selfprov":false,
                "dsbl_org_selfprov":false,
                "currency":"cad",
                "renewal_messages_allowed":true,
                "renewal_note":null,
                "yearly_renewal_date":"2017-12-07 00:00:00.000000 +00:00",
                "renewal_state":"ok",
                "renewal_msg_sent_at":null,
                "next_renewal_msg":null,
                "billing_period":null,
                "billing_base_date":null,
                "shows_powered_by_logo":true,
                "outbound_dg_bounce":null,
                "outbound_dg_default":null,
                "outbound_global_domains":true,
                "outbound_filter_active":false,
                "outbound_relay":null,
                "is_distributor_organization":false,
                "distributor_id":null,
                "external_distributor_id":null,
                "activation_date":null,
                "support_organization_name":"group.hole Reseller",
                "billing_organization_name":"group.hole Reseller",
                "distributor_organization_name":null
            }
        },
        {
            "type":"domains",
            "id":"4461",
            "attributes":{
                "domain_group_id":2907,
                "organization_id":2788,
                "name":"new.cool",
                "description":null,
                "transport":"mailcatcher",
                "transport_protocol":"smtp",
                "transport_port":25,
                "transport_use_mx":false,
                "transport_hold":false,
                "is_active":true,
                "created_at":"2016-12-07 18:21:14.000000 +00:00",
                "updated_at":"2016-12-07 18:21:14.000000 +00:00",
                "is_active_backup":null,
                "is_unconditional_delete":false,
                "count_start_date":null,
                "mailbox_count":null,
                "rejected_mailbox_count":null,
                "nb_declared_mailboxes":10,
                "domain_type":"standard",
                "dmbcount_updated_at":null,
                "last_nb_declared_mailboxes":null,
                "is_dmbcount_declared":true,
                "email":"client@new.cool",
                "is_check_transport_success":false,
                "is_check_mx_success":false,
                "transport_server_type":"BASIC",
                "activation_date":null,
                "annual_price":600,
                "organization_name":"new.cool Client"
            }
        },
        {
            "type":"users",
            "id":"9359",
            "attributes":{
                "is_access_allowed":true,
                "usr_type_code":"admin",
                "email_address":"client@new.cool",
                "display_name":"Suis un acheteur, Je",
                "language_code":"en_us",
                "name":"Je",
                "surname":"Suis un acheteur"
            }
        }
    ]
}
JSON;
        $jsonRequest = <<<JSON
{
    "first_name":"Je",
    "last_name":"suis un acheteur",
    "phone":"5146244520",
    "email":"client@new.cool",
    "organization_name":"new.cool Client",
    "transport":"mailcatcher",
    "transport_port":25,
    "mailboxes":100,
    "language":"en_US",
    "is_support":true,
    "is_billing":true
}
JSON;
        $config = $this->preSuccess($jsonResult);


        $request = new CreateResellerRequest();
        $request->setEmail('client@new.cool')
                ->setPhone('5146244520')
                ->setLanguage(LocaleEnum::EN_US())
                ->setLastName('suis un acheteur')
                ->setFirstName('Je')
                ->setTransportPort(25)
                ->setTransport('mailcatcher')
                ->setOrganizationName('new.cool Client')
                ->setMailboxes(100)
                ->setIsBilling(true)
                ->setIsSupport(true);

        $client = new ProvulusClient($config);
        /**
         * @var $response ClientCreationResponse
         */
        $response = $client->processRequest($request);

        $this->validateRequest($config, $jsonRequest);

        $this->assertEquals(2788, $response->organization()->id(), 'Organization ID incorrect');
        $this->assertEquals('new.cool Client', $response->organization()->corporate_name, 'Organization Name incorrect');
        $this->assertEquals(4461, $response->domain()->id(), 'Domain ID incorrect');
        $this->assertTrue(isset($response->domain()->name));
        $this->assertEquals('new.cool', $response->domain()->name, 'Domain Name incorrect');
        $this->assertEquals(9359, $response->user()->id(), 'User ID incorrect');
        $this->assertTrue(isset($response->user()->usr_type_code));
        $this->assertTrue($response->user()->usr_type_code->is(UserTypeEnum::ADMIN()), 'User ID isn\'t an admin');
    }


    /**
     * @test
     * @expectedException \ProvulusSDK\Exception\API\ValidationException
     */
    public function create_reseller_failure()
    {

        $jsonResult
            = '{"errors":{"organization_name":[{"code":"unique_corporate_name_error","message":"There is already an organization with this name.","value":"new.cool Client"}]}}';
        $jsonRequest
            = '{"first_name":"Je","last_name":"suis un acheteur","phone":"5146244520","email":"client@new.cool","organization_name":"new.cool Client","transport":"mailcatcher","transport_port":25,"mailboxes":100,"language":"en_US","is_support":true,"is_billing":true}';

        $config = $this->preFailure($jsonResult);


        $request = new CreateResellerRequest();
        $request->setEmail('client@new.cool')
                ->setPhone('5146244520')
                ->setLanguage(LocaleEnum::EN_US())
                ->setLastName('suis un acheteur')
                ->setFirstName('Je')
                ->setTransportPort(25)
                ->setTransport('mailcatcher')
                ->setOrganizationName('new.cool Client')
                ->setMailboxes(100)
                ->setIsBilling(true)
                ->setIsSupport(true);

        $client = new ProvulusClient($config);
        /**
         * @var $response ClientCreationResponse
         */
        try {
            $client->processRequest($request);
        } catch (ValidationException $validationException) {
            $this->validateRequest($config, $jsonRequest);
            $validationData = $validationException->getValidationData();
            $this->assertArrayHasKey('organization_name', $validationData);
            /**
             * @var $first ValidationError
             */
            $first = $validationData['organization_name']->getValidationErrorCollection()->first();
            $this->assertInstanceOf(ValidationError::class, $first);
            $this->assertEquals('new.cool Client', $first->getValue());
            $this->assertEquals('unique_corporate_name_error', $first->getCode());
            $this->assertEquals('There is already an organization with this name.', $first->getMessage());
            throw $validationException;
        }
    }


    /**
     * @test
     */
    public function create_client_success_response_set()
    {

        $jsonResult = <<<JSON
{
  "data": {
    "type": "client",
    "id": "2788",
    "attributes": [],
    "relationships": {
      "organization": {
        "data": {
          "type": "orgs",
          "id": "2788"
        }
      },
      "domain": {
        "data": {
          "type": "domains",
          "id": "4461"
        }
      },
      "user": {
        "data": {
          "type": "users",
          "id": "9359"
        }
      }
    }
  },
  "included": [
    {
      "type": "orgs",
      "id": "2788",
      "attributes": {
        "support_organization_id": 2773,
        "billing_organization_id": 2773,
        "is_access_allowed": true,
        "is_billing_organization": false,
        "is_support_organization": true,
        "corporate_name": "new.cool Client",
        "corporate_name_normalized": "new.cool client",
        "portal_prefix": null,
        "default_language_code": "en_us",
        "timezone_code": {
          "timezone_type": 3,
          "timezone": "America/Montreal"
        },
        "note_shared": null,
        "quar_expiry_in_days": 14,
        "logo_reference": null,
        "is_deact_req": false,
        "deact_req_reason": null,
        "deact_req_from_display_name": null,
        "deact_req_from_corporate_name": null,
        "deact_req_issued_at": null,
        "deact_req_target_date": null,
        "deact_req_executed_at": null,
        "is_active": true,
        "version": 0,
        "created_at": "2016-12-07 18:21:14.000000 +00:00",
        "updated_at": "2016-12-07 18:21:14.000000 +00:00",
        "dsbl_portal_selfprov": false,
        "dsbl_org_selfprov": false,
        "currency": "cad",
        "renewal_messages_allowed": true,
        "renewal_note": null,
        "yearly_renewal_date": "2017-12-07 00:00:00.000000 +00:00",
        "renewal_state": "ok",
        "renewal_msg_sent_at": null,
        "next_renewal_msg": null,
        "billing_period": null,
        "billing_base_date": null,
        "shows_powered_by_logo": true,
        "outbound_dg_bounce": null,
        "outbound_dg_default": null,
        "outbound_global_domains": true,
        "outbound_filter_active": false,
        "outbound_relay": null,
        "is_distributor_organization": false,
        "distributor_id": null,
        "external_distributor_id": null,
        "activation_date": null,
        "support_organization_name": "group.hole Reseller",
        "billing_organization_name": "group.hole Reseller",
        "distributor_organization_name": null
      }
    },
    {
      "type": "domains",
      "id": "4461",
      "attributes": {
        "domain_group_id": 2907,
        "organization_id": 2788,
        "name": "new.cool",
        "description": null,
        "transport": "mailcatcher",
        "transport_protocol": "smtp",
        "transport_port": 25,
        "transport_use_mx": false,
        "transport_hold": false,
        "is_active": true,
        "created_at": "2016-12-07 18:21:14.000000 +00:00",
        "updated_at": "2016-12-07 18:21:14.000000 +00:00",
        "is_active_backup": null,
        "is_unconditional_delete": false,
        "count_start_date": null,
        "mailbox_count": null,
        "rejected_mailbox_count": null,
        "nb_declared_mailboxes": 10,
        "domain_type": "standard",
        "dmbcount_updated_at": null,
        "last_nb_declared_mailboxes": null,
        "is_dmbcount_declared": true,
        "email": "client@new.cool",
        "is_check_transport_success": false,
        "is_check_mx_success": false,
        "transport_server_type": "BASIC",
        "activation_date": null,
        "annual_price": 600,
        "organization_name": "new.cool Client"
      }
    },
    {
      "type": "users",
      "id": "9359",
      "attributes": {
        "is_access_allowed": true,
        "usr_type_code": "admin",
        "email_address": "client@new.cool",
        "display_name": "Suis un acheteur, Je",
        "language_code": "en_us",
        "name": "Je",
        "surname": "Suis un acheteur"
      }
    }
  ]
}
JSON;
        $jsonRequest
                    = '{"first_name":"Je","last_name":"suis un acheteur","domain":"new.cool","phone":"5146244520","email":"client@new.cool","organization_name":"new.cool Client","transport":"mailcatcher","transport_port":25,"mailboxes":100,"language":"en_US"}';
        $config     = $this->preSuccess($jsonResult);


        $request = new CreateDirectClientRequest();
        $request->setEmail('client@new.cool')
                ->setPhone('5146244520')
                ->setDomain('new.cool')
                ->setLanguage(LocaleEnum::EN_US())
                ->setLastName('suis un acheteur')
                ->setFirstName('Je')
                ->setTransportPort(25)
                ->setTransport('mailcatcher')
                ->setOrganizationName('new.cool Client')
                ->setMailboxes(100);

        $client = new ProvulusClient($config);
        /**
         * @var $response ClientCreationResponse
         */
        $response = $client->processRequest($request);

        $this->validateRequest($config, $jsonRequest);

        $this->assertEquals($response, $response->organization()->getParentResponse());
        $this->assertEquals($response, $response->domain()->getParentResponse());
        $this->assertEquals($response, $response->user()->getParentResponse());

    }

    /**
     * @test
     * @expectedException \ProvulusSDK\Exception\API\APICrashException
     */
    public function test_api_crash_exception()
    {
        $jsonResult
                 = '{
  "statusCode": 500,
  "message": "Something went wrong!",
  "time": "2017-03-02 15:24:13",
  "id": "45s84dz8d4qs8dz82qsdfqs4"}';
        $request = new TestRequest();

        $config = $this->preFailure($jsonResult, 500);

        $client = new ProvulusClient($config);

        try {
            $client->processRequest($request);
        } catch (APICrashException $e) {
            $this->assertEquals('45s84dz8d4qs8dz82qsdfqs4', $e->getSentryId());
            throw $e;
        }

    }

    /**
     * @test
     * @expectedException \ProvulusSDK\Exception\API\APICrashException
     */
    public function test_api_crash_exception_no_sentry_id()
    {
        $jsonResult
                 = '{
  "statusCode": 500,
  "message": "Something went wrong!",
  "time": "2017-03-02 15:24:13"}';
        $request = new TestRequest();

        $config = $this->preFailure($jsonResult, 500);

        $client = new ProvulusClient($config);

        try {
            $client->processRequest($request);
        } catch (APICrashException $e) {
            $this->assertNull($e->getSentryId());
            throw $e;
        }

    }


    protected function getConfig(MockHandler $handler)
    {
        $config = parent::getConfig($handler);

        return $config->setApiKey('test');
    }
}
