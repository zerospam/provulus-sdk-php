<?php

use GuzzleHttp\Handler\MockHandler;
use GuzzleHttp\Psr7\Response;
use ProvulusSDK\Client\ProvulusClient;
use ProvulusTest\Request\File\FileRequest;
use ProvulusTest\Request\File\FileUpdateRequest;
use ProvulusTest\TestCase;

/**
 * Created by PhpStorm.
 * User: aaflalo
 * Date: 17-07-25
 * Time: 13:29
 */
class FileTestRequest extends TestCase
{
    /**
     * @var resource
     */
    private $testFile;

    /**
     * @var string
     */
    private $filename;


    protected function setUp()
    {
        $this->testFile = tmpfile();
        $meta_data      = stream_get_meta_data($this->testFile);
        $this->filename = $meta_data["uri"];
        file_put_contents($this->filename,
            base64_decode('iVBORw0KGgoAAAANSUhEUgAAAB4AAAAeCAYAAAFMN540AAAAGXRFWHRTb2Z0d2FyZQBBZG9iZSBJbWFnZVJlYWR5ccllPAAAAyJpVFh0WE1MOmNvbS5hZG9iZS54bXAAAAAAADw/eHBhY2tldCBiZWdpbj0i77u/IiBpZD0iVzVNME1wQ2VoaUh6cmVTek5UY3prYzlkIj8+IDx4OnhtcG1ldGEgeG1sbnM6eD0iYWRvYmU6bnM6bWV0YS8iIHg6eG1wdGs9IkFkb2JlIFhNUCBDb3JlIDUuMy1jMDExIDY2LjE0NTY2MSwgMjAxMi8wMi8wNi0xNDo1NjoyNyAgICAgICAgIj4gPHJkZjpSREYgeG1sbnM6cmRmPSJodHRwOi8vd3d3LnczLm9yZy8xOTk5LzAyLzIyLXJkZi1zeW50YXgtbnMjIj4gPHJkZjpEZXNjcmlwdGlvbiByZGY6YWJvdXQ9IiIgeG1sbnM6eG1wPSJodHRwOi8vbnMuYWRvYmUuY29tL3hhcC8xLjAvIiB4bWxuczp4bXBNTT0iaHR0cDovL25zLmFkb2JlLmNvbS94YXAvMS4wL21tLyIgeG1sbnM6c3RSZWY9Imh0dHA6Ly9ucy5hZG9iZS5jb20veGFwLzEuMC9zVHlwZS9SZXNvdXJjZVJlZiMiIHhtcDpDcmVhdG9yVG9vbD0iQWRvYmUgUGhvdG9zaG9wIENTNiAoV2luZG93cykiIHhtcE1NOkluc3RhbmNlSUQ9InhtcC5paWQ6Mzc0QTY0OEVCNTUyMTFFNjg5RkVFNzdBRDdFQjNDQUQiIHhtcE1NOkRvY3VtZW50SUQ9InhtcC5kaWQ6Mzc0QTY0OEZCNTUyMTFFNjg5RkVFNzdBRDdFQjNDQUQiPiA8eG1wTU06RGVyaXZlZEZyb20gc3RSZWY6aW5zdGFuY2VJRD0ieG1wLmlpZDozNzRBNjQ4Q0I1NTIxMUU2ODlGRUU3N0FEN0VCM0NBRCIgc3RSZWY6ZG9jdW1lbnRJRD0ieG1wLmRpZDozNzRBNjQ4REI1NTIxMUU2ODlGRUU3N0FEN0VCM0NBRCIvPiA8L3JkZjpEZXNjcmlwdGlvbj4gPC9yZGY6UkRGPiA8L3g6eG1wbWV0YT4gPD94cGFja2V0IGVuZD0iciI/PmokTB0AAAPDSURBVHjaYvz//z8DMmACESYmJlYwAUasKmAAqPI/QAAxgFQYGxu7gmiwaiBHDCr4H0SDtLyA6viAYihQvxmIAgggmBn/gZgHyv4IxKzIZirBODAMMx+EkY2EOfgZEH8D4t4zZ87MwPAIkh9MgVQGQAAhG+uIbhVM4geUZkO2mwlq50eQcUC7fgEpTrj5yCqRrDAH0SxA+XtojvkFNIENzIGqvAkNmP/IfsbqHZjfmbD5E2gsI4gGCCDkkGKEupoXTe0PkBhQwx8MU6BueA3Ev6Ds9+g+BPI/AfFPjEAECiqiewQbhnrYGlsM/MeWEpA0CmAzGN1kEFZFEpMF4j9ILktH1owrqEWBFDMwkF5A+RZA6jiSkky8yQ5HUgRlh5MwPkAAYWgGKhAHUhaQ6DzzFJ9hyPGcC6QmYVHTADSkEatuaEAsggZWDVoo50DF92CLBUZQLgaa8RmIrYEY5OR1QFwFtK0d6iI1IHUTiOWAYo+RLQZliU1A/BuILwHxG1i6RUrHt4AGvAMyj4IMQC+cHIF4GhD3AbEljrDpAGJZdEEWKM0GtCENT8CyYBME2bwdiNMJRHE1EF/BpjkARAP9FYojYdgCKW4gdsDQDC1x6oB4FVDhEjSNk4HUISCeC8Qc+BKJE5Dai8XyMCA+BsRPQMkCaBkTzuQJNUgBSD0EKvwP5YPKfH5YuoIZQHTGwGYAqbkK2YAakjRDDQD5XRqUEgEC1Fo+oRBFURgXW0VCSllIzUqNhbezUJRYSRaSMrFBWdpZaLKShdVQiB02dhZj8za2otnIRslORJIslN+n8+rO9Hpz32Refd0799935p5zvnOTykg7zQqYAZmYJQ/gVNFZGfM+XxkxZCoRR0YWfZLyC6UIeAGtIAsmQIez7hJMY8S7D3GjQ5qj+TbSZxBYcvaBWzCqGGNsB+RAp81nLHw0/8Y5eW9iFi/RHNrYlg6VpDCu4vRo60S2F1Px7oESfTVyPvu2q141+tVk+qU6HnLIMBsX6O+bEWspgkcZOWs/e5J8r3/S7zweQmvvwDxYTxkzodPvbagiaSXwaSIzDjaw9MpENu035vRLiT6G5MeRzYDrOq6BUNe8STMVySznvvpWxxGaosaAlHSOzWcehKo0546yLLLvIFUe20EqCnkz4E/jwLVBOapiOgAGK952BaDS3mW+VhouY0TBi9gxQP6ftChVoWlzpj9M/qVcJxz+xfpuh7Ds1WAG7HoR1+jnwNzVEvdscQ34V2JPAyRK2boQexgwVFdix4Bme4g8cdU3GvsF0ZiCw+a1lH4AAAAASUVORK5CYII='));
    }

    public function tearDown()
    {
        parent::tearDown();
        if (is_resource($this->testFile)) {
            fclose($this->testFile);
        }
    }

    /**
     * @test
     */
    public function add_file_to_request_with_handle()
    {
        $handler     = new MockHandler([new Response(201)]);
        $config      = $this->getConfig($handler)->setApiKey('test');
        $fileRequest = new FileRequest();
        $fileRequest->setImage($this->testFile);

        $client = new ProvulusClient($config);
        $client->processRequest($fileRequest);

        $trans = $this->lastTransaction($config);
        $request = $trans->request();
        $this->assertContains('PNG', $request->getBody()->getContents());
        $header = $request->getHeader('Content-Type');
        $this->assertContains('multipart/form-data', $header[0]);
    }

    /**
     * @test
     */
    public function add_file_to_request_with_file_path()
    {
        $handler     = new MockHandler([new Response(201)]);
        $config      = $this->getConfig($handler)->setApiKey('test');
        $fileRequest = new FileRequest();
        $fileRequest->setImage($this->filename);

        $client = new ProvulusClient($config);
        $client->processRequest($fileRequest);

        $trans = $this->lastTransaction($config);
        $this->assertContains('PNG', $trans->request()->getBody()->getContents());
    }

    /**
     * @test
     */
    public function add_file_to_request_with_handle_and_field()
    {
        $handler     = new MockHandler([new Response(201)]);
        $config      = $this->getConfig($handler)->setApiKey('test');
        $fileRequest = new FileRequest();
        $fileRequest->setImage($this->testFile)->setTestInfo('new value that should be here');

        $client = new ProvulusClient($config);
        $client->processRequest($fileRequest);

        $trans   = $this->lastTransaction($config);
        $content = $trans->request()->getBody()->getContents();
        $this->assertContains('PNG', $content);
        $this->assertContains('new value that should be here', $content);
    }

    /**
     * @test
     */
    public function add_file_to_update_request_make_it_post()
    {
        $handler     = new MockHandler([new Response(201)]);
        $config      = $this->getConfig($handler)->setApiKey('test');
        $fileRequest = new FileUpdateRequest();
        $fileRequest->setImage($this->testFile);

        $client = new ProvulusClient($config);
        $client->processRequest($fileRequest);

        $trans = $this->lastTransaction($config);
        $request = $trans->request();

        $contents = $request->getBody()->getContents();
        $this->assertContains('PNG', $contents);
        $this->assertContains('name="_method"', $contents);
        $this->assertContains('put', $contents);
        $header = $request->getHeader('Content-Type');
        $this->assertContains('multipart/form-data', $header[0]);
        $this->assertEquals('POST', $request->getMethod());
    }

}
