<?php
/**
 * Created by PhpStorm.
 * User: pbb
 * Date: 16/02/17
 * Time: 10:00 AM
 */

namespace automated\Requests\Quarantine;

use Carbon\Carbon;
use GuzzleHttp\Handler\MockHandler;
use ProvulusSDK\Client\ProvulusClient;
use ProvulusSDK\Client\Request\Quarantine\IndexQuarantineRequest;
use ProvulusSDK\Client\Response\Quarantine\AggregatedMessageResponse;
use ProvulusSDK\Config\ProvulusConfiguration;
use ProvulusSDK\Enum\Quarantine\Type\QuarantineTypeEnum;
use ProvulusTest\TestCaseApiKey;

/**
 * Class SystemQuarantineTest
 *
 * @package automated\Requests\Quarantine
 */
class QuarantineIndexTest extends TestCaseApiKey
{

    /**
     * @test
     **/
    public function index_system_request_success()
    {
        $jsonResult
            = '{  
                   "data":[  
                      {  
                         "type":"quarMessage",
                         "id":"456935",
                         "attributes":{  
                            "msg_id":49833551,
                            "msg_rcpt_id":51079142,
                            "usr_id":null,
                            "organization_id":111,
                            "rcpt_domain":"zozo.com",
                            "rcpt_localpart":"bobo",
                            "received_at":"2017-01-20 23:32:22.000000 +00:00",
                            "subject":"DDO EventViewer Log",
                            "subject_normalized":"ddo eventviewer log",
                            "content_preview":"Bonjour,\n\nEn pi\u00e8ce jointe vous trouverez les logs du poste BIB-PUBLIC10 en date du 2017-01-20_18-30-41.\n\nCordialement,\nSupport Techinque Bibliomondo",
                            "sa_score":"0.2608",
                            "is_banned":true,
                            "is_phish":false,
                            "is_dmarc_fail": false,
                            "is_released":false,
                            "released_by_usr_id":null,
                            "released_at":null,
                            "is_deleted":false,
                            "deleted_by_usr_id":null,
                            "deleted_at":null,
                            "is_declared_spam":false,
                            "declared_spam_by_usr_id":null,
                            "declared_spam_at":null,
                            "SMTPSender":{  
                               "local_part":"mondopc.bibliomondo",
                               "domain_part":"gmail.com",
                               "name":null
                            },
                            "ContentSenders":[  
                               {  
                                  "local_part":"it",
                                  "domain_part":"bibliomondo.com",
                                  "name":""
                               },
                               {  
                                  "local_part":"mondopc.bibliomondo",
                                  "domain_part":"gmail.com",
                                  "name":""
                               }
                            ],
                            "SMTPRecipient":{  
                               "local_part":"bobo",
                               "domain_part":"zozo.com",
                               "name":null
                            },
                            "ContentRecipients":[  
                               {  
                                  "local_part":"bobo",
                                  "domain_part":"zozo.com",
                                  "name":""
                               }
                            ]
                         }
                      },
                      {  
                         "type":"quarMessage",
                         "id":"336709",
                         "attributes":{  
                            "msg_id":49715072,
                            "msg_rcpt_id":50958916,
                            "usr_id":null,
                            "organization_id":111,
                            "rcpt_domain":"zozo.com",
                            "rcpt_localpart":"bobo",
                            "received_at":"2017-01-19 02:23:39.000000 +00:00",
                            "subject":"Hi",
                            "subject_normalized":"hi",
                            "content_preview":"Hi\nVjagra helps 8 out of 10 males keep erection strong enough for sex\n\nMake love better click here",
                            "sa_score":"12.633",
                            "is_banned":false,
                            "is_phish":false,
                            "is_dmarc_fail": true,
                            "is_released":false,
                            "released_by_usr_id":null,
                            "released_at":null,
                            "is_deleted":false,
                            "deleted_by_usr_id":null,
                            "deleted_at":null,
                            "is_declared_spam":false,
                            "declared_spam_by_usr_id":null,
                            "declared_spam_at":null,
                            "SMTPSender":{  
                               "local_part":"no-reply",
                               "domain_part":"blog.cassol.com.br",
                               "name":null
                            },
                            "ContentSenders":[  
                               {  
                                  "local_part":"no-reply",
                                  "domain_part":"blog.cassol.com.br",
                                  "name":"info25"
                               }
                            ],
                            "SMTPRecipient":{  
                               "local_part":"bobo",
                               "domain_part":"zozo.com",
                               "name":null
                            },
                            "ContentRecipients":[  
                               {  
                                  "local_part":"bobo",
                                  "domain_part":"zozo.com",
                                  "name":""
                               }
                            ]
                         }
                      }
                   ],
                   "meta":{  
                      "pagination":{  
                         "total":2,
                         "count":2,
                         "per_page":15,
                         "current_page":1,
                         "total_pages":1
                      }
                   }
                }';

        $jsonRequest = '{}';

        $config = $this->preSuccess($jsonResult);

        $request = (new IndexQuarantineRequest)
            ->setQuarantineType(QuarantineTypeEnum::SYSTEM());

        $client = new ProvulusClient($config);
        $client->processRequest($request);

        $collectionResponse = $request->getResponse();

        /**
         * @var AggregatedMessageResponse $quarantineResponse
         */
        $quarantineResponse = $collectionResponse->data()->first();

        $this->validateRequest($config, $jsonRequest);
        $this->validateRequestUrl($config, ['/system/quar']);

        $this->assertEquals('bobo', $quarantineResponse->ContentRecipients->first()->getLocalPart());
        $this->assertEquals('bibliomondo.com', $quarantineResponse->ContentSenders->first()->getDomainPart());
        $this->assertCount(2, $quarantineResponse->ContentSenders);
        $this->assertEquals('mondopc.bibliomondo', $quarantineResponse->SMTPSender->getLocalPart());
        $this->assertEquals('zozo.com', $quarantineResponse->SMTPRecipient->getDomainPart());
    }

    /**
     * @test
     **/
    public function index_aggregated_request_success()
    {
        $jsonResult
            = '{  
                   "data":[  
                      {  
                         "type":"quarMessage",
                         "id":"456935",
                         "attributes":{  
                            "msg_id":49833551,
                            "msg_rcpt_id":51079142,
                            "usr_id":null,
                            "organization_id":111,
                            "rcpt_domain":"zozo.com",
                            "rcpt_localpart":"bobo",
                            "received_at":"2017-01-20 23:32:22.000000 +00:00",
                            "subject":"DDO EventViewer Log",
                            "subject_normalized":"ddo eventviewer log",
                            "content_preview":"Bonjour,\n\nEn pi\u00e8ce jointe vous trouverez les logs du poste BIB-PUBLIC10 en date du 2017-01-20_18-30-41.\n\nCordialement,\nSupport Techinque Bibliomondo",
                            "sa_score":"0.2608",
                            "is_banned":true,
                            "is_phish":false,
                            "is_dmarc_fail": false,
                            "is_released":false,
                            "released_by_usr_id":null,
                            "released_at":null,
                            "is_deleted":false,
                            "deleted_by_usr_id":null,
                            "deleted_at":null,
                            "is_declared_spam":false,
                            "declared_spam_by_usr_id":null,
                            "declared_spam_at":null,
                            "SMTPSender":{  
                               "local_part":"mondopc.bibliomondo",
                               "domain_part":"gmail.com",
                               "name":null
                            },
                            "ContentSenders":[  
                               {  
                                  "local_part":"it",
                                  "domain_part":"bibliomondo.com",
                                  "name":""
                               },
                               {  
                                  "local_part":"mondopc.bibliomondo",
                                  "domain_part":"gmail.com",
                                  "name":""
                               }
                            ],
                            "SMTPRecipient":{  
                               "local_part":"bobo",
                               "domain_part":"zozo.com",
                               "name":null
                            },
                            "ContentRecipients":[  
                               {  
                                  "local_part":"bobo",
                                  "domain_part":"zozo.com",
                                  "name":""
                               }
                            ]
                         }
                      },
                      {  
                         "type":"quarMessage",
                         "id":"336709",
                         "attributes":{  
                            "msg_id":49715072,
                            "msg_rcpt_id":50958916,
                            "usr_id":null,
                            "organization_id":111,
                            "rcpt_domain":"zozo.com",
                            "rcpt_localpart":"bobo",
                            "received_at":"2017-01-19 02:23:39.000000 +00:00",
                            "subject":"Hi",
                            "subject_normalized":"hi",
                            "content_preview":"Hi\nVjagra helps 8 out of 10 males keep erection strong enough for sex\n\nMake love better click here",
                            "sa_score":"12.633",
                            "is_banned":false,
                            "is_phish":false,
                            "is_dmarc_fail": true,
                            "is_released":false,
                            "released_by_usr_id":null,
                            "released_at":null,
                            "is_deleted":false,
                            "deleted_by_usr_id":null,
                            "deleted_at":null,
                            "is_declared_spam":false,
                            "declared_spam_by_usr_id":null,
                            "declared_spam_at":null,
                            "SMTPSender":{  
                               "local_part":"no-reply",
                               "domain_part":"blog.cassol.com.br",
                               "name":null
                            },
                            "ContentSenders":[  
                               {  
                                  "local_part":"no-reply",
                                  "domain_part":"blog.cassol.com.br",
                                  "name":"info25"
                               }
                            ],
                            "SMTPRecipient":{  
                               "local_part":"bobo",
                               "domain_part":"zozo.com",
                               "name":null
                            },
                            "ContentRecipients":[  
                               {  
                                  "local_part":"bobo",
                                  "domain_part":"zozo.com",
                                  "name":""
                               }
                            ]
                         }
                      }
                   ],
                   "meta":{  
                      "pagination":{  
                         "total":2,
                         "count":2,
                         "per_page":15,
                         "current_page":1,
                         "total_pages":1
                      }
                   }
                }';

        $jsonRequest = '{}';

        $config = $this->preSuccess($jsonResult);

        $request = (new IndexQuarantineRequest)
            ->setQuarantineType(QuarantineTypeEnum::AGGREGATED());

        $client = new ProvulusClient($config);
        $client->processRequest($request);

        $collectionResponse = $request->getResponse();

        /**
         * @var AggregatedMessageResponse $quarantineResponse
         */
        $quarantineResponse = $collectionResponse->data()->first();

        $this->validateRequest($config, $jsonRequest);
        $this->validateRequestUrl($config, ['/aggregated/quar']);

        $this->assertEquals('bobo', $quarantineResponse->ContentRecipients->first()->getLocalPart());
        $this->assertEquals('bibliomondo.com', $quarantineResponse->ContentSenders->first()->getDomainPart());
        $this->assertCount(2, $quarantineResponse->ContentSenders);
        $this->assertEquals('mondopc.bibliomondo', $quarantineResponse->SMTPSender->getLocalPart());
        $this->assertEquals('zozo.com', $quarantineResponse->SMTPRecipient->getDomainPart());
    }

    /**
     * @test
     **/
    public function empty_content_collections_success()
    {
        $jsonResult
            = '{  
                   "data":[  
                      {  
                         "type":"quarMessage",
                         "id":"456935",
                         "attributes":{  
                            "msg_id":49833551,
                            "msg_rcpt_id":51079142,
                            "usr_id":null,
                            "organization_id":111,
                            "rcpt_domain":"zozo.com",
                            "rcpt_localpart":"bobo",
                            "received_at":"2017-01-20 23:32:22.000000 +00:00",
                            "subject":"DDO EventViewer Log",
                            "subject_normalized":"ddo eventviewer log",
                            "content_preview":"Bonjour,\n\nEn pi\u00e8ce jointe vous trouverez les logs du poste BIB-PUBLIC10 en date du 2017-01-20_18-30-41.\n\nCordialement,\nSupport Techinque Bibliomondo",
                            "sa_score":"0.2608",
                            "is_banned":true,
                            "is_phish":false,
                            "is_dmarc_fail": false,
                            "is_released":false,
                            "released_by_usr_id":null,
                            "released_at":null,
                            "is_deleted":false,
                            "deleted_by_usr_id":null,
                            "deleted_at":null,
                            "is_declared_spam":false,
                            "declared_spam_by_usr_id":null,
                            "declared_spam_at":null,
                            "SMTPSender":{  
                               "local_part":"mondopc.bibliomondo",
                               "domain_part":"gmail.com",
                               "name":null
                            },
                            "ContentSenders":[],
                            "SMTPRecipient":{  
                               "local_part":"bobo",
                               "domain_part":"zozo.com",
                               "name":null
                            },
                            "ContentRecipients":[]
                         }
                      }
                   ],
                   "meta":{  
                      "pagination":{  
                         "total":1,
                         "count":1,
                         "per_page":15,
                         "current_page":1,
                         "total_pages":1
                      }
                   }
                }';

        $jsonRequest = '{}';

        $config = $this->preSuccess($jsonResult);

        $request = (new IndexQuarantineRequest)
            ->setQuarantineType(QuarantineTypeEnum::SYSTEM());

        $client = new ProvulusClient($config);
        $client->processRequest($request);

        $collectionResponse = $request->getResponse();

        /**
         * @var AggregatedMessageResponse $quarantineResponse
         */
        $quarantineResponse = $collectionResponse->data()->first();

        $this->validateRequest($config, $jsonRequest);

        $this->assertCount(0, $quarantineResponse->ContentRecipients);
        $this->assertCount(0, $quarantineResponse->ContentSenders);
    }


    /**
     * @test
     **/
    public function index_organization_request_success()
    {
        $jsonResult
            = '{  
                   "data":[  
                      {  
                         "type":"quarMessage",
                         "id":"456935",
                         "attributes":{  
                            "msg_id":49833551,
                            "msg_rcpt_id":51079142,
                            "usr_id":null,
                            "organization_id":111,
                            "rcpt_domain":"zozo.com",
                            "rcpt_localpart":"bobo",
                            "received_at":"2017-01-20 23:32:22.000000 +00:00",
                            "subject":"DDO EventViewer Log",
                            "subject_normalized":"ddo eventviewer log",
                            "content_preview":"Bonjour,\n\nEn pi\u00e8ce jointe vous trouverez les logs du poste BIB-PUBLIC10 en date du 2017-01-20_18-30-41.\n\nCordialement,\nSupport Techinque Bibliomondo",
                            "sa_score":"0.2608",
                            "is_banned":true,
                            "is_phish":false,
                            "is_dmarc_fail": false,
                            "is_released":false,
                            "released_by_usr_id":null,
                            "released_at":null,
                            "is_deleted":false,
                            "deleted_by_usr_id":null,
                            "deleted_at":null,
                            "is_declared_spam":false,
                            "declared_spam_by_usr_id":null,
                            "declared_spam_at":null,
                            "SMTPSender":{  
                               "local_part":"mondopc.bibliomondo",
                               "domain_part":"gmail.com",
                               "name":null
                            },
                            "ContentSenders":[  
                               {  
                                  "local_part":"it",
                                  "domain_part":"bibliomondo.com",
                                  "name":""
                               },
                               {  
                                  "local_part":"mondopc.bibliomondo",
                                  "domain_part":"gmail.com",
                                  "name":""
                               }
                            ],
                            "SMTPRecipient":{  
                               "local_part":"bobo",
                               "domain_part":"zozo.com",
                               "name":null
                            },
                            "ContentRecipients":[  
                               {  
                                  "local_part":"bobo",
                                  "domain_part":"zozo.com",
                                  "name":""
                               }
                            ]
                         }
                      },
                      {  
                         "type":"quarMessage",
                         "id":"336709",
                         "attributes":{  
                            "msg_id":49715072,
                            "msg_rcpt_id":50958916,
                            "usr_id":null,
                            "organization_id":111,
                            "rcpt_domain":"zozo.com",
                            "rcpt_localpart":"bobo",
                            "received_at":"2017-01-19 02:23:39.000000 +00:00",
                            "subject":"Hi",
                            "subject_normalized":"hi",
                            "content_preview":"Hi\nVjagra helps 8 out of 10 males keep erection strong enough for sex\n\nMake love better click here",
                            "sa_score":"12.633",
                            "is_banned":false,
                            "is_phish":false,
                            "is_dmarc_fail": false,
                            "is_released":false,
                            "released_by_usr_id":null,
                            "released_at":null,
                            "is_deleted":false,
                            "deleted_by_usr_id":null,
                            "deleted_at":null,
                            "is_declared_spam":false,
                            "declared_spam_by_usr_id":null,
                            "declared_spam_at":null,
                            "SMTPSender":{  
                               "local_part":"no-reply",
                               "domain_part":"blog.cassol.com.br",
                               "name":null
                            },
                            "ContentSenders":[  
                               {  
                                  "local_part":"no-reply",
                                  "domain_part":"blog.cassol.com.br",
                                  "name":"info25"
                               }
                            ],
                            "SMTPRecipient":{  
                               "local_part":"bobo",
                               "domain_part":"zozo.com",
                               "name":null
                            },
                            "ContentRecipients":[  
                               {  
                                  "local_part":"bobo",
                                  "domain_part":"zozo.com",
                                  "name":""
                               }
                            ]
                         }
                      }
                   ],
                   "meta":{  
                      "pagination":{  
                         "total":2,
                         "count":2,
                         "per_page":15,
                         "current_page":1,
                         "total_pages":1
                      }
                   }
                }';

        $jsonRequest = '{}';

        $config = $this->preSuccess($jsonResult);

        $request = (new IndexQuarantineRequest)
            ->setQuarantineType(QuarantineTypeEnum::ORGANIZATION())
            ->setOrganizationId(50);

        $client = new ProvulusClient($config);
        $client->processRequest($request);

        $collectionResponse = $request->getResponse();
        $this->validateRequest($config, $jsonRequest);
        $this->validateRequestUrl($config, ['/orgs/50/quar']);

        $collectionResponse->data()->each(function (AggregatedMessageResponse $response) {
            self::assertEquals(111, $response->get('organization_id'));
        });
    }


    /**
     * @test
     **/
    public function index_user_request_success()
    {
        $jsonResult
            = '{  
                   "data":[  
                      {  
                         "type":"quarMessage",
                         "id":"456935",
                         "attributes":{  
                            "msg_id":49833551,
                            "msg_rcpt_id":51079142,
                            "usr_id":221,
                            "organization_id":111,
                            "rcpt_domain":"zozo.com",
                            "rcpt_localpart":"bobo",
                            "received_at":"2017-01-20 23:32:22.000000 +00:00",
                            "subject":"DDO EventViewer Log",
                            "subject_normalized":"ddo eventviewer log",
                            "content_preview":"Bonjour,\n\nEn pi\u00e8ce jointe vous trouverez les logs du poste BIB-PUBLIC10 en date du 2017-01-20_18-30-41.\n\nCordialement,\nSupport Techinque Bibliomondo",
                            "sa_score":"0.2608",
                            "is_banned":true,
                            "is_phish":false,
                            "is_dmarc_fail": false,
                            "is_released":false,
                            "released_by_usr_id":null,
                            "released_at":null,
                            "is_deleted":false,
                            "deleted_by_usr_id":null,
                            "deleted_at":null,
                            "is_declared_spam":false,
                            "declared_spam_by_usr_id":null,
                            "declared_spam_at":null,
                            "SMTPSender":{  
                               "local_part":"mondopc.bibliomondo",
                               "domain_part":"gmail.com",
                               "name":null
                            },
                            "ContentSenders":[  
                               {  
                                  "local_part":"it",
                                  "domain_part":"bibliomondo.com",
                                  "name":""
                               },
                               {  
                                  "local_part":"mondopc.bibliomondo",
                                  "domain_part":"gmail.com",
                                  "name":""
                               }
                            ],
                            "SMTPRecipient":{  
                               "local_part":"bobo",
                               "domain_part":"zozo.com",
                               "name":null
                            },
                            "ContentRecipients":[  
                               {  
                                  "local_part":"bobo",
                                  "domain_part":"zozo.com",
                                  "name":""
                               }
                            ]
                         }
                      },
                      {  
                         "type":"quarMessage",
                         "id":"336709",
                         "attributes":{  
                            "msg_id":49715072,
                            "msg_rcpt_id":50958916,
                            "usr_id":221,
                            "organization_id":111,
                            "rcpt_domain":"zozo.com",
                            "rcpt_localpart":"bobo",
                            "received_at":"2017-01-19 02:23:39.000000 +00:00",
                            "subject":"Hi",
                            "subject_normalized":"hi",
                            "content_preview":"Hi\nVjagra helps 8 out of 10 males keep erection strong enough for sex\n\nMake love better click here",
                            "sa_score":"12.633",
                            "is_banned":false,
                            "is_phish":false,
                            "is_dmarc_fail": false,
                            "is_released":false,
                            "released_by_usr_id":null,
                            "released_at":null,
                            "is_deleted":false,
                            "deleted_by_usr_id":null,
                            "deleted_at":null,
                            "is_declared_spam":false,
                            "declared_spam_by_usr_id":null,
                            "declared_spam_at":null,
                            "SMTPSender":{  
                               "local_part":"no-reply",
                               "domain_part":"blog.cassol.com.br",
                               "name":null
                            },
                            "ContentSenders":[  
                               {  
                                  "local_part":"no-reply",
                                  "domain_part":"blog.cassol.com.br",
                                  "name":"info25"
                               }
                            ],
                            "SMTPRecipient":{  
                               "local_part":"bobo",
                               "domain_part":"zozo.com",
                               "name":null
                            },
                            "ContentRecipients":[  
                               {  
                                  "local_part":"bobo",
                                  "domain_part":"zozo.com",
                                  "name":""
                               }
                            ]
                         }
                      }
                   ],
                   "meta":{  
                      "pagination":{  
                         "total":2,
                         "count":2,
                         "per_page":15,
                         "current_page":1,
                         "total_pages":1
                      }
                   }
                }';

        $jsonRequest = '{}';

        $config = $this->preSuccess($jsonResult);

        $request = (new IndexQuarantineRequest)
            ->setQuarantineType(QuarantineTypeEnum::USER())
            ->setOrganizationId(5)
            ->setUserId(4);

        $client = new ProvulusClient($config);
        $client->processRequest($request);

        $collectionResponse = $request->getResponse();
        $this->validateRequest($config, $jsonRequest);
        $this->validateRequestUrl($config, ['/orgs/5/users/4/quar']);

        $collectionResponse->data()->each(function (AggregatedMessageResponse $response) {
            self::assertEquals(221, $response->get('usr_id'));
        });
    }

    /**
     * @test
     **/
    public function index_system_with_arguments()
    {
        $jsonResult
                     = '{  
   "data":[  
      {  
         "type":"quarMessage",
         "id":"249726",
         "attributes":{  
            "msg_id":49628674,
            "msg_rcpt_id":50871933,
            "usr_id":null,
            "organization_id":1,
            "rcpt_domain":"zerospam.ca",
            "rcpt_localpart":"dp666",
            "received_at":"2017-01-17 19:46:19.000000 +00:00",
            "subject":"(repaymentproprietary4) CNN Report Trump Drops a BOMB",
            "subject_normalized":"(repaymentproprietary4) cnn report trump drops a bomb",
            "content_preview":"Trumps Plan To MAGA! Click HereDonald Trump: Issues Huge PlanDoubling Average Americans Income.The Republican Party and Even the Morning talk show circuit are all losing their minds about a new plan proposed by President Trump.Read the exclusive story on the Presidents\' New Plan to help you earn more. Even his party tried to stop him from releasing this.Stop these here or write to 1626 Timoney Rd; Draper, UT 8402013_1581_387_1882462_1\nThis email went to xxx@xxxxxxx.xxx. To skip additional messages, go here\nOr mail\n222 W lee St, Mineral Springs, AR 71851\n\nInertial electric, so bound, and so more heavier this, then because will, but plasma while, or bang required, and files fact, or form fusion, then this fraction, so early, but nuclear with, and bytes, and so overcome optimal, or shell rate nuclear, and module ions, but stars, so higher, then does, and so devices should called, then does produced both, so higher ribbon, and achieve, or nuklir, but enough energy, and so data, or inverse ",
            "sa_score":"9.762",
            "is_banned":false,
            "is_phish":false,
            "is_dmarc_fail": false,
            "is_released":false,
            "released_by_usr_id":null,
            "released_at":null,
            "is_deleted":false,
            "deleted_by_usr_id":null,
            "deleted_at":null,
            "is_declared_spam":false,
            "declared_spam_by_usr_id":null,
            "declared_spam_at":null,
            "SMTPSender":{  
               "local_part":"messages",
               "domain_part":"tructury.com",
               "name":null
            },
            "ContentSenders":[  
               {  
                  "local_part":"messages",
                  "domain_part":"tructury.com",
                  "name":""
               },
               {  
                  "local_part":"messages",
                  "domain_part":"tructury.com",
                  "name":"BREXIT News"
               }
            ],
            "SMTPRecipient":{  
               "local_part":"dp666",
               "domain_part":"zerospam.ca",
               "name":null
            },
            "ContentRecipients":[  

            ],
            "TagsLabel":"Consult Spam",
            "Tags":[  
               "spam"
            ],
            "Status":"quarantined"
         }
      }
   ],
   "meta":{  
      "pagination":{  
         "total":1,
         "count":1,
         "per_page":15,
         "current_page":1,
         "total_pages":1
      }
   },
   "links":{  
      "self":"http:\/\/cumulus.local\/api\/v1\/system\/quar?page=1",
      "first":"http:\/\/cumulus.local\/api\/v1\/system\/quar?page=1",
      "last":"http:\/\/cumulus.local\/api\/v1\/system\/quar?page=1"
   }
}';
        $jsonRequest = [];
        $config      = $this->preSuccess($jsonResult);

        $request = (new IndexQuarantineRequest)
            ->setQuarantineType(QuarantineTypeEnum::SYSTEM());
        $request->setSubject('Pay')
                ->setScore(1.0, 15.0)
                ->setReceivedAt(Carbon::parse('2017-01-17 00:00'), Carbon::parse('2017-01-17 23:59'))
                ->isStatusDeleted(false)
                ->isStatusDeclaredAsSpam(false)
                ->isNoStatus(true)
                ->isStatusReleased(false)
                ->addFrom('messages')
                ->addTo('zerospam.ca')
                ->isTagBannedFile(true)
                ->isTagPhish(false)
                ->setRuleTags('ZS_TEST');

        $client = new ProvulusClient($config);
        $client->processRequest($request);

        $this->validateRequest($config, $jsonRequest);
        $this->validateRequestUrl(
            $config,
            [
                'subject=Pay',
                'lower_score=1',
                'upper_score=15',
                'start=2017-01-17%2000%3A00%3A00',
                'stop=2017-01-17%2023%3A59%3A00',
                'status_deleted=0',
                'status_declared_spam=0',
                'no_status=1',
                'status_released=0',
                'tag_banned_file=1',
                'tag_phish=0',
                'from=messages',
                'to=zerospam.ca',
                'rule_tags=ZS_TEST'
            ]
        );
    }

    /**
     * @test
     */
    public function index_org_with_greymail()
    {
        $jsonResponse
            = '{
  "data": [
    {
      "type": "quarMessage",
      "id": "51473504",
      "attributes": {
        "msg_id": 50222381,
        "msg_rcpt_id": 51473504,
        "usr_id": null,
        "organization_id": 1234,
        "rcpt_domain": "zerospam.ca",
        "rcpt_localpart": "test",
        "received_at": "2017-08-17 21:23:12.000000 +00:00",
        "subject": "Good news - Your application has been accepted.",
        "subject_normalized": "good news - your application has been accepted.",
        "content_preview": "Hey,\nGood news! Your application has been accepted!\nPlease review the details of the position Here\nWhen I last checked there was only 2 positions available in your areaso I recommend you apply now before all the positions are gone.\nIt\'s all explained RIGHT HERE..\nCheers,\nMeghan\n-\n--\n\n--\n\n---\n5536 Carson St, Orlando, FL, United States, 97218\nIf you no longer want our emails, please Unsubscribe",
        "sa_score": "7.631",
        "is_banned": false,
        "is_phish": false,
        "is_dmarc_fail": false,
        "is_released": false,
        "released_by_usr_id": null,
        "released_at": null,
        "is_deleted": true,
        "deleted_by_usr_id": 9885,
        "deleted_at": "2017-11-22 21:06:27.830594 +00:00",
        "is_declared_spam": false,
        "declared_spam_by_usr_id": null,
        "declared_spam_at": null,
        "is_greymail": true,
        "SMTPSender": {
          "local_part": "bounce-3308-1631840-656-248",
          "domain_part": "fastfbcommissions.com",
          "name": null
        },
        "ContentSenders": [
          {
            "local_part": "meghan",
            "domain_part": "fastfbcommissions.com",
            "name": "Meghan"
          }
        ],
        "SMTPRecipient": {
          "local_part": "test",
          "domain_part": "zerospam.ca",
          "name": null
        },
        "ContentRecipients": [],
        "Tags": [
          "greymail"
        ],
        "Status": "deleted",
        "CurrentRecipient": {
          "local_part": "test",
          "domain_part": "zerospam.ca",
          "name": null
        }
      }
    }
  ],
  "meta": {
    "pagination": {
      "total": 1,
      "count": 1,
      "per_page": 15,
      "current_page": 1,
      "total_pages": 1
    }
  },
  "links": {
    "self": "http://cumulus.local/api/v1/orgs/1234/quar?page=1",
    "first": "http://cumulus.local/api/v1/orgs/1234/quar?page=1",
    "last": "http://cumulus.local/api/v1/orgs/1234/quar?page=1"
  }
}';

        $jsonRequest = [];
        $config      = $this->preSuccess($jsonResponse);

        $request = (new IndexQuarantineRequest)
            ->setQuarantineType(QuarantineTypeEnum::ORGANIZATION());

        $request->setOrganizationId(1234)
                ->setSubject('Good news - Your application has been accepted.')
                ->isTagGreymail(true)
                ->addTo('zerospam.ca');

        $client = new ProvulusClient($config);
        $client->processRequest($request);

        $this->validateRequest($config, $jsonRequest);
        $this->validateRequestUrl($config,
            ['/orgs/1234/quar?subject=Good%20news%20-%20Your%20application%20has%20been%20accepted.&tag_greymail=1&to=zerospam.ca']);
    }

    /**
     * @test
     * @expectedException \InvalidArgumentException
     **/
    public function index_argument_received_at_misuse()
    {
        $request = (new IndexQuarantineRequest)
            ->setQuarantineType(QuarantineTypeEnum::SYSTEM());
        $request->setScore();
    }

    /**
     * Gets config for tests
     *
     * @param MockHandler $handler
     *
     * @return ProvulusConfiguration
     */
    protected function getConfig(MockHandler $handler)
    {
        return parent::getConfig($handler)->setApiKey('test');
    }


}