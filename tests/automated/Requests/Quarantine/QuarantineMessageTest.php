<?php
/**
 * Created by PhpStorm.
 * User: pbb
 * Date: 11/04/17
 * Time: 3:15 PM
 */

namespace automated\Requests\Quarantine;


use ProvulusSDK\Client\ProvulusClient;
use ProvulusSDK\Client\Request\Quarantine\FullMessageQuarantineRequest;
use ProvulusSDK\Client\Request\Quarantine\QuarantineActionRequest;
use ProvulusSDK\Client\Request\Quarantine\ReadQuarantineRequest;
use ProvulusSDK\Client\Response\EmptyResponse;
use ProvulusSDK\Client\Response\Quarantine\StatusTagsEnum;
use ProvulusSDK\Enum\Quarantine\Action\QuarantineActionEnum;
use ProvulusSDK\Enum\Quarantine\Type\QuarantineTypeEnum;
use ProvulusSDK\Utils\Quarantine\EmailAttachment;
use ProvulusSDK\Utils\Quarantine\QuarantineMessageStatus;
use ProvulusSDK\Utils\Quarantine\RelatedEmail;
use ProvulusTest\TestCaseApiKey;

class QuarantineMessageTest extends TestCaseApiKey
{
    /**
     * @test
     **/
    public function global_quarantine_message_read()
    {
        $jsonResult
            = '{  
   "data":{  
      "type":"quarantineMessage",
      "id":"851213",
      "attributes":{  
         "organization_id":1,
         "received_at":"2017-01-30 07:18:47.000000 +00:00",
         "score":11.351
         ,
         "from":[  
            {  
               "local_part":"bounce-eleanorestirk=xxxxx.xxx",
               "domain_part":"mail37.dls130.vm1.icbounce25.com",
               "name":null
            },
            {  
               "local_part":"monica",
               "domain_part":"binarypaycheck.com",
               "name":"Monica"
            }
         ],
         "to":[  
            {  
               "local_part":"dp666",
               "domain_part":"zerospam.ca",
               "name":null
            }
         ],
         "subject":"You have (1) new YouTube message.",
         "attachments":[  

         ],
         "plainText":"{&quot;id&quot;:198793042,&quot;msg_id&quot;:50222297,&quot;msg_data&quot;:&quot;Hey,\\n\\nOne of your friends has sent you a video message.\\n[http:\\\/\\\/tracking.icbounce25.com\\\/tc\\\/x8rQV0RLad\\\/51f4efbfb3e18f4ea053c4d3d282c4e2\\\/fa09e3699873d1527aaf0facc335f535\\\/binaryfx\\n\\nThis video message can be viewed on your browser. Be sure\\nto turn your speakers on if you cannot hear it.\\n\\n [http:\\\/\\\/tracking.icbounce25.com\\\/tc\\\/x8rQV0RLad\\\/51f4efbfb3e18f4ea053c4d3d282c4e2\\\/91cb564f651c1effe578669509766f63\\\/binaryfx\\nhere to view your message [http:\\\/\\\/tracking.icbounce25.com\\\/tc\\\/x8rQV0RLad\\\/51f4efbfb3e18f4ea053c4d3d282c4e2\\\/fa09e3699873d1527aaf0facc335f535\\\/binaryfx\\n\\nTalk soon,\\n\\nMonica &amp; Dave\\n\\n\\n\\nThis email was sent to xxx@xxxxxxx.xxx by:\\nWAHP, 575 6th Ave, San Diego, CA, United States, 92101\\n\\nTo update or remove your contact information: \\nhttp:\\\/\\\/tracking.icbounce25.com\\\/uml\\\/x8rQV0RLad\\\/51f4efbfb3e18f4ea053c4d3d282c4e2\\\/7f3b61ca89cacc525e7e39450d65495c\\\/binaryfx\\n\\n\\n&quot;,&quot;data_type&quot;:{}}",
         "html":"<div class=\"repeatable ui-droppable\">     <!-- Start of Text -->       <table width=\"100%\" bgcolor=\"#ffffff\" cellpadding=\"0\" cellspacing=\"0\" border=\"0\" id=\"backgroundTable\" block=\"Text\"><tbody><tr><td>                     <table width=\"86.5%\" cellpadding=\"0\" cellspacing=\"0\" border=\"0\" align=\"center\" class=\"device-width full-width editingoutline\" modulebg=\"edit\" bgcolor=\"#ffffff\" style=\"border-top-color: rgb(255, 255, 255);\"><tbody class=\"\"><tr vblock=\"edit\"><td style=\"padding:10px;\"><\/td><\/tr><tr vblock=\"edit\"><td style=\"padding:10px;\">                                 <table width=\"100%\" cellpadding=\"0\" cellspacing=\"0\" border=\"0\" align=\"center\" class=\"device-width\" style=\"max-width:620px; margin:0 auto;\" contentmod=\"\"><tbody class=\"\"><tr><td  align=\"left\" valign=\"middle\" style=\"font-family: arial, sans-serif; line-height: 20px; color: rgb(51, 51, 51); text-align: justify; font-size: 15px;\" mgpara=\"preheadertext\" class=\"merge_tags\">Hey,<br><br>One of your friends has sent you a <u><a href=\"http:\/\/tracking.icbounce25.com\/tc\/x8rQV0RLad\/51f4efbfb3e18f4ea053c4d3d282c4e2\/fa09e3699873d1527aaf0facc335f535\/binaryfx\" data-tag=\"style=\'color:rgb(66,\">video message.<\/a><\/u><br><br>This video message can be viewed on your browser. Be sure<br>to turn your speakers on if you cannot hear it.<br><br><b><u><a href=\"http:\/\/tracking.icbounce25.com\/tc\/x8rQV0RLad\/51f4efbfb3e18f4ea053c4d3d282c4e2\/f2108f32896c23954a21539d386e2725\/binaryfx\"><\/a><a href=\"http:\/\/tracking.icbounce25.com\/tc\/x8rQV0RLad\/51f4efbfb3e18f4ea053c4d3d282c4e2\/fa09e3699873d1527aaf0facc335f535\/binaryfx\" style=\"color: rgb(66, 139, 202);\">Clicking here to view your message<\/a><\/u><\/b><br><br>Talk soon,<br><br>Monica &amp; Dave<br><br><br><br><br><br><br><br><br><\/td>                                       <\/tr><\/tbody><\/table><\/td>                           <\/tr><\/tbody><\/table><\/td>               <\/tr><\/tbody><\/table><!-- End of Text --><div style=\"display:none\" class=\"fn-hide ui-state-highlight ui-droppable\"><\/div><\/div><p style=\"margin:30px 0 20px 0; font-size:12px;\">To update or remove your contact information please <a href=\"http:\/\/tracking.icbounce25.com\/uml\/x8rQV0RLad\/51f4efbfb3e18f4ea053c4d3d282c4e2\/7f3b61ca89cacc525e7e39450d65495c\/binaryfx\" style=\"text-decoration:none; color:#08c;\">Manage Your Subscription<\/a><\/p><div style=\"color:#0b5875; background:#f3f5f5; padding:10px; font-size:12px; overflow:hidden;\"><p style=\"font-weight:bold; margin:0 0 9px 0\">This email was sent to xxx@xxxxxxx.xxx by:<\/p><span style=\"margin:0;\">WAHP<\/span>, <span style=\"margin:0;\">575 6th Ave<\/span>, <span style=\"margin:0;\">San Diego<\/span>, <span style=\"margin:0;\">CA<\/span>, <span style=\"margin:0;\">United States<\/span>, <span style=\"margin:0;\">92101<\/span><\/div><img src=\"http:\/\/tracking.icbounce25.com\/getimage\/51f4efbfb3e18f4ea053c4d3d282c4e2$$$607\/binaryfx\" width=\"0\" height=\"0\" border=\"0\" \/>\n",
         "headers":"X-Spam-Flag: YES\nX-Spam-Score: 11.351\nX-Spam-Status: Yes, score=11.351 required=4 tests=[IVM_BL=2, RDNS_NONE=2,\n\tZ_RDNS_BL=2, RCVD_IN_IVMSIP24=1, URIBL_IVMURI=1, Z_JANUS_S_SCORE=1,\n\tZ_RECIPS_READ_BROWSER=1, Z_GENERIC_HI_UNSUB=0.5, Z_IMPERSONAL_RECIPS=0.5, Z_RCPT_DOM_NOT_IN_HEADERS=0.5,\n\tZ_URI_MISMATCH=0.5, HTML_IMAGE_ONLY_32=0.3, HTML_IMG_ONLY=0.1, Z_DIFF_FROM_TO_DOMAIN=0.1,\n\tZ_GENERIC_HI=0.1, Z_RCPT_NOT_IN_HEADERS=0.1, T_Z_HAS_DKIM_HEAD=0.01, Z_DIFF_FROM_TRIM_DOMAIN=0.01,\n\tZ_MED_EML=0.01, Z_MED_SUBJ=0.01, Z_MULTI_RECIPS=0.01, Z_SIZE_5k_10k=0.01,\n\tZ_WHITE_TEXT=0.01, HTML_MESSAGE=0.001, Z_JANUS_S=0.001, Z_MULTIBL=0.001,\n\tZS_LIST_HEAD=-1, LIST_UNSUB=-0.1, SPF_PASS=-0.1, Z_LIST_BODY_UNSUB=-0.1,\n\tZ_NL_UNSUB=-0.1, HAS_SENDER=-0.01, Z_ANY_UNSUB=-0.01, ZS_LIST_UNSUB2X=-0.001,\n\tZ_NL_HEADER=-0.001] autolearn=disabled\nReceived: from 127.0.0.1 (127.0.0.1:10024)\n\t(original ip: 63.143.37.175)\n\tby kitty.zerospam.ca (Themis) with ESMTP id tyJPSJsuwaU7Bq6u4Xi;\n\tMon, 30 Jan 2017 02:18:47 -0500\nX-ZEROSPAM-Spammy-Score: 2.222\nX-ZS-Size: 6262\nReceived-SPF: Pass (dp666@zerospam.ca: domain of mail37.dls130.vm1.icbounce25.com designates 63.143.37.175 as permitted sender) client-ip=63.143.37.175; envelope-from=\"bounce-eleanorestirk=xxxxx.xxx@mail37.dls130.vm1.icbounce25.com\"; helo=legendery.net; receiver=dp666@zerospam.ca; mechanism=\"ip4:63.143.37.0\/24\"; identity=mailfrom\nReceived: from legendery.net (unknown [63.143.37.175])\n        by kitty.zerospam.ca (Postfix) with ESMTP id 3vBgjg0NfLz10\n        for <dp666@zerospam.ca>; Mon, 30 Jan 2017 02:18:46 -0500 (EST)\nX-Original-To: phpfeed@zstest.com\nDelivered-To: phpfeed@sonya.zerospam.ca\nDKIM-Signature: v=1; a=rsa-sha1; c=relaxed\/relaxed; s=dikkim; d=icbounce25.com;\n h=Sender:Message-ID:Date:Subject:From:To:MIME-Version:Content-Type:List-Unsubscribe;\n i=binaryfx@icbounce25.com; bh=OThAX50NI9S8+BRrmuqX4MqtAtE=;\n b=dgUJyh2iiaYQ5c32wIo99GxhKWXTGL4NTogh1Piw2f9nJUaFTjaMYSAHO\/7ldyVN7lzrPOApCYJp\n QKyJoybOPdcCzEgBgq4rLHdE3jTkspZhZNZ\/Wcy2MYgUPy5lgAlYJPHJyDlZS2YHm0GhHrtMtcH2\n BJ4VH8ZzZN0sA7lF\/OE=\nDomainKey-Signature: a=rsa-sha1; c=nofws; q=dns; s=dikkim; d=icbounce25.com;\n b=o3liwn4PMaKlyyaqHl4Vy8oEYnXtV6BKInYK3GtjVF2SyU7K5ArD2sv1MZiO8ZWS0BsQtyCXpK0q\n kd4TvM0+BYdR5ri6qYwhsveTbA0xSyosNGiQQrvuJmPOExNe\/9sXAljg343\/duOxfGXJ70wEaNuf\n 2aoNNhoPWl8EyixJz6c=;\nSender: binaryfx@icbounce25.com\nMessage-ID: <0000014g3ylatfbs-0dc4w821-472d-2d5f-c85b-lnrx0dq1pk2i-000000@binaryfx.icbounce25.com>\nDate: Mon, 30 Jan 2017 02:18:21 -0500\nSubject: You have (1) new YouTube message. \nFrom: Monica <monica@binarypaycheck.com>\nTo: \" \" <xxx@xxxxxxx.xxx>\nMIME-Version: 1.0\nContent-Type: multipart\/alternative;\n boundary=148576070196bbf94306499e2d8f8a0cdd620dfde0\nX-Complaints-To: abuse@icbounce25.com\nX-Subscription: Subscribed on 10\/19\/2016, via web form, from import\nList-Unsubscribe: <http:\/\/tracking.icbounce25.com\/uml\/x8rQV0RLad\/51f4efbfb3e18f4ea053c4d3d282c4e2\/7f3b61ca89cacc525e7e39450d65495c\/binaryfx>\nX-Mailer: Sendlane 1.0",
         "status":{  
            "type":"quarantined",
            "changed":null,
            "user":{  
               "name":null,
               "organization":null
            }
         }
      }
   }
}';

        $jsonRequest = [];

        $config = $this->preSuccess($jsonResult);

        $request = (new ReadQuarantineRequest)
            ->setQuarantineType(QuarantineTypeEnum::SYSTEM());
        $request->setMessageRecipientId(51473420);

        $client = new ProvulusClient($config);
        $client->processRequest($request);

        $response = $request->getResponse();
        $this->validateRequest($config, $jsonRequest);
        $this->validateRequestUrl($config, ['/system/quar/51473420']);

        $this->assertEquals('quarantined', $response->status->getType());
        $this->assertNull($response->status->getChanged());
        $this->assertNull($response->status->getOrganizationName());
        $this->assertNull($response->status->getUserName());
    }


    /**
     * @test
     **/
    public function organization_quarantine_message_read()
    {
        $jsonResult
            = '{  
   "data":{  
      "type":"quarantineMessage",
      "id":"51473024",
      "attributes":{  
         "organization_id":1,
         "received_at":"2017-01-30 06:48:23.000000 +00:00",
         "score":8.362,
         "from":{  
            "envelope":{  
               "local_part":"schooluniforms",
               "domain_part":"schooluniformsplus.co.uk",
               "name":null
            },
            "header":[{  
               "local_part":"jessie.may",
               "domain_part":"schooluniformsplus.co.uk",
               "name":"USPS International"
            }]
         },
         "to":{  
            "envelope":{  
               "local_part":"cumulus",
               "domain_part":"zerospam.ca",
               "name":null
            },
            "header":[{  
               "local_part":"cumulus",
               "domain_part":"zerospam.ca",
               "name":""
            }]
         },
         "subject":"Shipment delivery problem #005258796",
         "body":{  
            "plainText":"eyZxdW90O2lkJnF1b3Q7OjE5ODc5MTE5NCwmcXVvdDttc2dfaWQmcXVvdDs6NTAyMjE5MTEsJnF1b3Q7bXNnX2RhdGEmcXVvdDs6JnF1b3Q7RGVhciBDdXN0b21lcixXZSBjYW4gbm90IGRlbGl2ZXIgeW91ciBwYXJjZWwgYXJyaXZlZCBhdCBKYW51YXJ5IDI3LlBsZWFzZSByZXZpZXcgZGVsaXZlcnkgbGFiZWwgaW4gYXR0YWNobWVudCFUaGFuayB5b3UgZm9yIHlvdXIgYXNzaXN0YW5jZSBpbiB0aGlzIG1hdHRlcixKZXNzaWUgTWF5LFVTUFMgUGFyY2VscyBPcGVyYXRpb24gQWdlbnQuJnF1b3Q7LCZxdW90O2RhdGFfdHlwZSZxdW90Ozp7fX0=",
            "html":null
         },
         "headers":"X-Spam-Flag: YES\nX-Spam-Score: 8.362\nX-Spam-Status: Yes, score=8.362 required=4 tests=[Z_MISMATCH_USPS=5, Z_SHIPPING_MISMATCH=2,\n\tZ_SMALL_ARCHIVE=0.5, ZS_POLITE=0.3, ZS_OPENME=0.1, Z_GENERIC_HI=0.1,\n\tZ_HAS_ZIP=0.1, Z_SUSPICIOUS_DOC=0.1, Z_SUSP_DOC=0.1, T_Z_SPF_TEMPERROR=0.01,\n\tZ_ANY_MISMATCH=0.01, Z_HAS_ARCHIVE=0.01, Z_MED_SUBJ=0.01, Z_SHORT_EML=0.01,\n\tZ_SIZE_3k_5k=0.01, Z_ANY_ATTACH=0.001, Z_JANUS_DUNNO=0.001] autolearn=disabled\nReceived: from 127.0.0.1 (127.0.0.1:10024)\n\t(original ip: 149.255.63.116)\n\tby anna.zerospam.ca (Themis) with ESMTP id zPmRcaAkSshLcdVC1KF;\n\tMon, 30 Jan 2017 01:48:23 -0500\nX-ZEROSPAM-Spammy-Score: 0.163\nX-ZS-Size: 3848\nReceived-SPF: TempError (cumulus@zerospam.ca: temporary error in processing during lookup of schooluniformsplus.co.uk) client-ip=149.255.63.116; envelope-from=\"schooluniforms@schooluniformsplus.co.uk\"; helo=cloud.designaweb.co.uk; receiver=cumulus@zerospam.ca; identity=mailfrom\nX-Greylist: delayed 456 seconds by postgrey-1.34 at anna.zerospam.ca; Mon, 30 Jan 2017 01:48:23 EST\nReceived: from cloud.designaweb.co.uk (cloud.designaweb.co.uk [149.255.63.116])\n        (using TLSv1 with cipher ADH-AES256-SHA (256\/256 bits))\n        (No client certificate requested)\n        by anna.zerospam.ca (Postfix) with ESMTPS id 3vBg2b3lczzp\n        for <cumulus@zerospam.ca>; Mon, 30 Jan 2017 01:48:13 -0500 (EST)\nReceived: by cloud.designaweb.co.uk (Postfix, from userid 10132)\n        id D186D52B03; Mon, 30 Jan 2017 06:40:34 +0000 (GMT)\nTo: cumulus@zerospam.ca\nSubject: Shipment delivery problem #005258796\nDate: Mon, 30 Jan 2017 06:40:34 +0000\nMIME-Version: 1.0\nMessage-ID: <5c3b4b528e924c108575afdf683e7a23@schooluniformsplus.co.uk>\nReply-To: \"USPS International\" <jessie.may@schooluniformsplus.co.uk>\nFrom: \"USPS International\" <jessie.may@schooluniformsplus.co.uk>\nContent-Type: multipart\/mixed; boundary=\"b1_3e802f16b59d1bf705c88788efee78ca\"\nX-PPP-Message-ID: <20170130064034.14657.92418@cloud.designaweb.co.uk>\nX-PPP-Vhost: schooluniformsplus.co.uk\nContent-Transfer-Encoding: 7bit",
         "attachments":[  
            {  
               "id":4408030,
               "msg_id":50221911,
               "name":"Undelivered-Package-005258796.zip",
               "type":null,
               "size":1408,
               "is_banned":false
            }
         ],
         "status":{  
            "type":"quarantined",
            "changed":null,
            "user":{  
               "name":null,
               "organization":null
            }
         },
         "tags":["phishing"],
         "related":[  
            {  
               "msg_id":51348229,
               "recipient":{  
                  "local_part":"cumulus2",
                  "domain_part":"zerospam.ca",
                  "name":"Cumulus"
               },
               "status":{  
                  "type":"quarantined",
                  "changed":null,
                  "user":{  
                     "name":null,
                     "organization":null
                  }
               }
            },
            {  
               "msg_id":51348228,
               "recipient":{  
                  "local_part":"cumulus3",
                  "domain_part":"zerospam.ca",
                  "name":null
               },
               "status":{  
                  "type":"quarantined",
                  "changed":null,
                  "user":{  
                     "name":null,
                     "organization":null
                  }
               }
            }
         ]
      }
   }
}';

        $jsonRequest = [];

        $config = $this->preSuccess($jsonResult);

        $request = (new ReadQuarantineRequest)
            ->setQuarantineType(QuarantineTypeEnum::ORGANIZATION())
            ->setOrganizationId(1)
            ->setMessageRecipientId(51473024);

        $client = new ProvulusClient($config);
        $client->processRequest($request);

        $response = $request->getResponse();
        $this->validateRequest($config, $jsonRequest);
        $this->validateRequestUrl($config, ['/orgs/1/quar/51473024']);

        $this->assertEquals('Shipment delivery problem #005258796', $response->subject);
        $this->assertNull($response->body->getHtml());

        $this->assertEquals($response->from->getHeader()->first()->getName(), 'USPS International');
        $this->assertEquals('{&quot;id&quot;:198791194,&quot;msg_id&quot;:50221911,&quot;msg_data&quot;:&quot;Dear Customer,We can not deliver your parcel arrived at January 27.Please review delivery label in attachment!Thank you for your assistance in this matter,Jessie May,USPS Parcels Operation Agent.&quot;,&quot;data_type&quot;:{}}',
            $response->body->getText());
        $this->assertEquals('quarantined', $response->status->getType());
        $this->assertNull($response->status->getChanged());
        $this->assertNull($response->status->getOrganizationName());
        $this->assertNull($response->status->getUserName());

        $this->assertEquals('schooluniforms', $response->from->getEnvelope()->getLocalPart());
        $this->assertEquals(StatusTagsEnum::PHISHING(), $response->tags->first());

        /**
         * @var EmailAttachment $attachment
         */
        $attachment = $response->attachments->first();
        $this->assertEquals('Undelivered-Package-005258796.zip', $attachment->getName());
        $this->assertEquals(4408030, $attachment->getId());
        $this->assertEquals(50221911, $attachment->getMsgId());
        $this->assertEquals(1408, $attachment->getSize());
        $this->assertNull($attachment->getType());
        $this->assertFalse($attachment->isBanned());

        $this->assertCount(2, $response->related);

        /**
         * @var $related RelatedEmail
         */
        $related = $response->related->first();
        $this->assertEquals('cumulus2', $related->getRecipient()->getLocalPart());
        $this->assertEquals(51348229, $related->getMsgId());
        $this->assertInstanceOf(QuarantineMessageStatus::class, $related->getStatus());
        $this->assertEquals('cumulus2@zerospam.ca', $related->getRecipient()->getEmail());
    }


    /**
     * @test
     **/
    public function user_quarantine_message_read()
    {
        $jsonResult
            = '{  
   "data":{  
      "type":"quarantineMessage",
      "id":"851213",
      "attributes":{  
         "organization_id":1,
         "received_at":"2017-01-30 07:18:47.000000 +00:00",
         "score":11.351
         ,
         "from":[  
            {  
               "local_part":"bounce-eleanorestirk=xxxxx.xxx",
               "domain_part":"mail37.dls130.vm1.icbounce25.com",
               "name":null
            },
            {  
               "local_part":"monica",
               "domain_part":"binarypaycheck.com",
               "name":"Monica"
            }
         ],
         "to":[  
            {  
               "local_part":"dp666",
               "domain_part":"zerospam.ca",
               "name":null
            }
         ],
         "subject":"You have (1) new YouTube message.",
         "attachments":[  

         ],
         "plainText":"{&quot;id&quot;:198793042,&quot;msg_id&quot;:50222297,&quot;msg_data&quot;:&quot;Hey,\\n\\nOne of your friends has sent you a video message.\\n[http:\\\/\\\/tracking.icbounce25.com\\\/tc\\\/x8rQV0RLad\\\/51f4efbfb3e18f4ea053c4d3d282c4e2\\\/fa09e3699873d1527aaf0facc335f535\\\/binaryfx\\n\\nThis video message can be viewed on your browser. Be sure\\nto turn your speakers on if you cannot hear it.\\n\\n [http:\\\/\\\/tracking.icbounce25.com\\\/tc\\\/x8rQV0RLad\\\/51f4efbfb3e18f4ea053c4d3d282c4e2\\\/91cb564f651c1effe578669509766f63\\\/binaryfx\\nhere to view your message [http:\\\/\\\/tracking.icbounce25.com\\\/tc\\\/x8rQV0RLad\\\/51f4efbfb3e18f4ea053c4d3d282c4e2\\\/fa09e3699873d1527aaf0facc335f535\\\/binaryfx\\n\\nTalk soon,\\n\\nMonica &amp; Dave\\n\\n\\n\\nThis email was sent to xxx@xxxxxxx.xxx by:\\nWAHP, 575 6th Ave, San Diego, CA, United States, 92101\\n\\nTo update or remove your contact information: \\nhttp:\\\/\\\/tracking.icbounce25.com\\\/uml\\\/x8rQV0RLad\\\/51f4efbfb3e18f4ea053c4d3d282c4e2\\\/7f3b61ca89cacc525e7e39450d65495c\\\/binaryfx\\n\\n\\n&quot;,&quot;data_type&quot;:{}}",
         "html":"<div class=\"repeatable ui-droppable\">     <!-- Start of Text -->       <table width=\"100%\" bgcolor=\"#ffffff\" cellpadding=\"0\" cellspacing=\"0\" border=\"0\" id=\"backgroundTable\" block=\"Text\"><tbody><tr><td>                     <table width=\"86.5%\" cellpadding=\"0\" cellspacing=\"0\" border=\"0\" align=\"center\" class=\"device-width full-width editingoutline\" modulebg=\"edit\" bgcolor=\"#ffffff\" style=\"border-top-color: rgb(255, 255, 255);\"><tbody class=\"\"><tr vblock=\"edit\"><td style=\"padding:10px;\"><\/td><\/tr><tr vblock=\"edit\"><td style=\"padding:10px;\">                                 <table width=\"100%\" cellpadding=\"0\" cellspacing=\"0\" border=\"0\" align=\"center\" class=\"device-width\" style=\"max-width:620px; margin:0 auto;\" contentmod=\"\"><tbody class=\"\"><tr><td  align=\"left\" valign=\"middle\" style=\"font-family: arial, sans-serif; line-height: 20px; color: rgb(51, 51, 51); text-align: justify; font-size: 15px;\" mgpara=\"preheadertext\" class=\"merge_tags\">Hey,<br><br>One of your friends has sent you a <u><a href=\"http:\/\/tracking.icbounce25.com\/tc\/x8rQV0RLad\/51f4efbfb3e18f4ea053c4d3d282c4e2\/fa09e3699873d1527aaf0facc335f535\/binaryfx\" data-tag=\"style=\'color:rgb(66,\">video message.<\/a><\/u><br><br>This video message can be viewed on your browser. Be sure<br>to turn your speakers on if you cannot hear it.<br><br><b><u><a href=\"http:\/\/tracking.icbounce25.com\/tc\/x8rQV0RLad\/51f4efbfb3e18f4ea053c4d3d282c4e2\/f2108f32896c23954a21539d386e2725\/binaryfx\"><\/a><a href=\"http:\/\/tracking.icbounce25.com\/tc\/x8rQV0RLad\/51f4efbfb3e18f4ea053c4d3d282c4e2\/fa09e3699873d1527aaf0facc335f535\/binaryfx\" style=\"color: rgb(66, 139, 202);\">Clicking here to view your message<\/a><\/u><\/b><br><br>Talk soon,<br><br>Monica &amp; Dave<br><br><br><br><br><br><br><br><br><\/td>                                       <\/tr><\/tbody><\/table><\/td>                           <\/tr><\/tbody><\/table><\/td>               <\/tr><\/tbody><\/table><!-- End of Text --><div style=\"display:none\" class=\"fn-hide ui-state-highlight ui-droppable\"><\/div><\/div><p style=\"margin:30px 0 20px 0; font-size:12px;\">To update or remove your contact information please <a href=\"http:\/\/tracking.icbounce25.com\/uml\/x8rQV0RLad\/51f4efbfb3e18f4ea053c4d3d282c4e2\/7f3b61ca89cacc525e7e39450d65495c\/binaryfx\" style=\"text-decoration:none; color:#08c;\">Manage Your Subscription<\/a><\/p><div style=\"color:#0b5875; background:#f3f5f5; padding:10px; font-size:12px; overflow:hidden;\"><p style=\"font-weight:bold; margin:0 0 9px 0\">This email was sent to xxx@xxxxxxx.xxx by:<\/p><span style=\"margin:0;\">WAHP<\/span>, <span style=\"margin:0;\">575 6th Ave<\/span>, <span style=\"margin:0;\">San Diego<\/span>, <span style=\"margin:0;\">CA<\/span>, <span style=\"margin:0;\">United States<\/span>, <span style=\"margin:0;\">92101<\/span><\/div><img src=\"http:\/\/tracking.icbounce25.com\/getimage\/51f4efbfb3e18f4ea053c4d3d282c4e2$$$607\/binaryfx\" width=\"0\" height=\"0\" border=\"0\" \/>\n",
         "headers":"X-Spam-Flag: YES\nX-Spam-Score: 11.351\nX-Spam-Status: Yes, score=11.351 required=4 tests=[IVM_BL=2, RDNS_NONE=2,\n\tZ_RDNS_BL=2, RCVD_IN_IVMSIP24=1, URIBL_IVMURI=1, Z_JANUS_S_SCORE=1,\n\tZ_RECIPS_READ_BROWSER=1, Z_GENERIC_HI_UNSUB=0.5, Z_IMPERSONAL_RECIPS=0.5, Z_RCPT_DOM_NOT_IN_HEADERS=0.5,\n\tZ_URI_MISMATCH=0.5, HTML_IMAGE_ONLY_32=0.3, HTML_IMG_ONLY=0.1, Z_DIFF_FROM_TO_DOMAIN=0.1,\n\tZ_GENERIC_HI=0.1, Z_RCPT_NOT_IN_HEADERS=0.1, T_Z_HAS_DKIM_HEAD=0.01, Z_DIFF_FROM_TRIM_DOMAIN=0.01,\n\tZ_MED_EML=0.01, Z_MED_SUBJ=0.01, Z_MULTI_RECIPS=0.01, Z_SIZE_5k_10k=0.01,\n\tZ_WHITE_TEXT=0.01, HTML_MESSAGE=0.001, Z_JANUS_S=0.001, Z_MULTIBL=0.001,\n\tZS_LIST_HEAD=-1, LIST_UNSUB=-0.1, SPF_PASS=-0.1, Z_LIST_BODY_UNSUB=-0.1,\n\tZ_NL_UNSUB=-0.1, HAS_SENDER=-0.01, Z_ANY_UNSUB=-0.01, ZS_LIST_UNSUB2X=-0.001,\n\tZ_NL_HEADER=-0.001] autolearn=disabled\nReceived: from 127.0.0.1 (127.0.0.1:10024)\n\t(original ip: 63.143.37.175)\n\tby kitty.zerospam.ca (Themis) with ESMTP id tyJPSJsuwaU7Bq6u4Xi;\n\tMon, 30 Jan 2017 02:18:47 -0500\nX-ZEROSPAM-Spammy-Score: 2.222\nX-ZS-Size: 6262\nReceived-SPF: Pass (dp666@zerospam.ca: domain of mail37.dls130.vm1.icbounce25.com designates 63.143.37.175 as permitted sender) client-ip=63.143.37.175; envelope-from=\"bounce-eleanorestirk=xxxxx.xxx@mail37.dls130.vm1.icbounce25.com\"; helo=legendery.net; receiver=dp666@zerospam.ca; mechanism=\"ip4:63.143.37.0\/24\"; identity=mailfrom\nReceived: from legendery.net (unknown [63.143.37.175])\n        by kitty.zerospam.ca (Postfix) with ESMTP id 3vBgjg0NfLz10\n        for <dp666@zerospam.ca>; Mon, 30 Jan 2017 02:18:46 -0500 (EST)\nX-Original-To: phpfeed@zstest.com\nDelivered-To: phpfeed@sonya.zerospam.ca\nDKIM-Signature: v=1; a=rsa-sha1; c=relaxed\/relaxed; s=dikkim; d=icbounce25.com;\n h=Sender:Message-ID:Date:Subject:From:To:MIME-Version:Content-Type:List-Unsubscribe;\n i=binaryfx@icbounce25.com; bh=OThAX50NI9S8+BRrmuqX4MqtAtE=;\n b=dgUJyh2iiaYQ5c32wIo99GxhKWXTGL4NTogh1Piw2f9nJUaFTjaMYSAHO\/7ldyVN7lzrPOApCYJp\n QKyJoybOPdcCzEgBgq4rLHdE3jTkspZhZNZ\/Wcy2MYgUPy5lgAlYJPHJyDlZS2YHm0GhHrtMtcH2\n BJ4VH8ZzZN0sA7lF\/OE=\nDomainKey-Signature: a=rsa-sha1; c=nofws; q=dns; s=dikkim; d=icbounce25.com;\n b=o3liwn4PMaKlyyaqHl4Vy8oEYnXtV6BKInYK3GtjVF2SyU7K5ArD2sv1MZiO8ZWS0BsQtyCXpK0q\n kd4TvM0+BYdR5ri6qYwhsveTbA0xSyosNGiQQrvuJmPOExNe\/9sXAljg343\/duOxfGXJ70wEaNuf\n 2aoNNhoPWl8EyixJz6c=;\nSender: binaryfx@icbounce25.com\nMessage-ID: <0000014g3ylatfbs-0dc4w821-472d-2d5f-c85b-lnrx0dq1pk2i-000000@binaryfx.icbounce25.com>\nDate: Mon, 30 Jan 2017 02:18:21 -0500\nSubject: You have (1) new YouTube message. \nFrom: Monica <monica@binarypaycheck.com>\nTo: \" \" <xxx@xxxxxxx.xxx>\nMIME-Version: 1.0\nContent-Type: multipart\/alternative;\n boundary=148576070196bbf94306499e2d8f8a0cdd620dfde0\nX-Complaints-To: abuse@icbounce25.com\nX-Subscription: Subscribed on 10\/19\/2016, via web form, from import\nList-Unsubscribe: <http:\/\/tracking.icbounce25.com\/uml\/x8rQV0RLad\/51f4efbfb3e18f4ea053c4d3d282c4e2\/7f3b61ca89cacc525e7e39450d65495c\/binaryfx>\nX-Mailer: Sendlane 1.0",
         "status":{  
            "type":"quarantined",
            "changed":null,
            "user":{  
               "name":null,
               "organization":null
            }
         }
      }
   }
}';

        $jsonRequest = [];

        $config = $this->preSuccess($jsonResult);

        $request = (new ReadQuarantineRequest)
            ->setQuarantineType(QuarantineTypeEnum::USER())
            ->setOrganizationId(1)
            ->setMessageRecipientId(51473420)
            ->setUserId(9885);

        $client = new ProvulusClient($config);
        $client->processRequest($request);

        $response = $request->getResponse();
        $this->validateRequest($config, $jsonRequest);
        $this->validateRequestUrl($config, ['/orgs/1/users/9885/quar/51473420']);

        $this->assertEquals('quarantined', $response->status->getType());
        $this->assertNull($response->status->getChanged());
        $this->assertNull($response->status->getOrganizationName());
        $this->assertNull($response->status->getUserName());
    }


    /**
     * @test
     * @expectedException  \Exception
     **/
    public function global_quarantine_message_read_message_id_not_set()
    {
        $jsonResult
                = '{  
   "data":{  
      "type":"quarantineMessage",
      "id":"851213",
      "attributes":{  
         "organization_id":1,
         "received_at":"2017-01-30 07:18:47.000000 +00:00",
         "score":11.351
         ,
         "from":[  
            {  
               "local_part":"bounce-eleanorestirk=xxxxx.xxx",
               "domain_part":"mail37.dls130.vm1.icbounce25.com",
               "name":null
            },
            {  
               "local_part":"monica",
               "domain_part":"binarypaycheck.com",
               "name":"Monica"
            }
         ],
         "to":[  
            {  
               "local_part":"dp666",
               "domain_part":"zerospam.ca",
               "name":null
            }
         ],
         "subject":"You have (1) new YouTube message.",
         "attachments":[  

         ],
         "plainText":"{&quot;id&quot;:198793042,&quot;msg_id&quot;:50222297,&quot;msg_data&quot;:&quot;Hey,\\n\\nOne of your friends has sent you a video message.\\n[http:\\\/\\\/tracking.icbounce25.com\\\/tc\\\/x8rQV0RLad\\\/51f4efbfb3e18f4ea053c4d3d282c4e2\\\/fa09e3699873d1527aaf0facc335f535\\\/binaryfx\\n\\nThis video message can be viewed on your browser. Be sure\\nto turn your speakers on if you cannot hear it.\\n\\n [http:\\\/\\\/tracking.icbounce25.com\\\/tc\\\/x8rQV0RLad\\\/51f4efbfb3e18f4ea053c4d3d282c4e2\\\/91cb564f651c1effe578669509766f63\\\/binaryfx\\nhere to view your message [http:\\\/\\\/tracking.icbounce25.com\\\/tc\\\/x8rQV0RLad\\\/51f4efbfb3e18f4ea053c4d3d282c4e2\\\/fa09e3699873d1527aaf0facc335f535\\\/binaryfx\\n\\nTalk soon,\\n\\nMonica &amp; Dave\\n\\n\\n\\nThis email was sent to xxx@xxxxxxx.xxx by:\\nWAHP, 575 6th Ave, San Diego, CA, United States, 92101\\n\\nTo update or remove your contact information: \\nhttp:\\\/\\\/tracking.icbounce25.com\\\/uml\\\/x8rQV0RLad\\\/51f4efbfb3e18f4ea053c4d3d282c4e2\\\/7f3b61ca89cacc525e7e39450d65495c\\\/binaryfx\\n\\n\\n&quot;,&quot;data_type&quot;:{}}",
         "html":"<div class=\"repeatable ui-droppable\">     <!-- Start of Text -->       <table width=\"100%\" bgcolor=\"#ffffff\" cellpadding=\"0\" cellspacing=\"0\" border=\"0\" id=\"backgroundTable\" block=\"Text\"><tbody><tr><td>                     <table width=\"86.5%\" cellpadding=\"0\" cellspacing=\"0\" border=\"0\" align=\"center\" class=\"device-width full-width editingoutline\" modulebg=\"edit\" bgcolor=\"#ffffff\" style=\"border-top-color: rgb(255, 255, 255);\"><tbody class=\"\"><tr vblock=\"edit\"><td style=\"padding:10px;\"><\/td><\/tr><tr vblock=\"edit\"><td style=\"padding:10px;\">                                 <table width=\"100%\" cellpadding=\"0\" cellspacing=\"0\" border=\"0\" align=\"center\" class=\"device-width\" style=\"max-width:620px; margin:0 auto;\" contentmod=\"\"><tbody class=\"\"><tr><td  align=\"left\" valign=\"middle\" style=\"font-family: arial, sans-serif; line-height: 20px; color: rgb(51, 51, 51); text-align: justify; font-size: 15px;\" mgpara=\"preheadertext\" class=\"merge_tags\">Hey,<br><br>One of your friends has sent you a <u><a href=\"http:\/\/tracking.icbounce25.com\/tc\/x8rQV0RLad\/51f4efbfb3e18f4ea053c4d3d282c4e2\/fa09e3699873d1527aaf0facc335f535\/binaryfx\" data-tag=\"style=\'color:rgb(66,\">video message.<\/a><\/u><br><br>This video message can be viewed on your browser. Be sure<br>to turn your speakers on if you cannot hear it.<br><br><b><u><a href=\"http:\/\/tracking.icbounce25.com\/tc\/x8rQV0RLad\/51f4efbfb3e18f4ea053c4d3d282c4e2\/f2108f32896c23954a21539d386e2725\/binaryfx\"><\/a><a href=\"http:\/\/tracking.icbounce25.com\/tc\/x8rQV0RLad\/51f4efbfb3e18f4ea053c4d3d282c4e2\/fa09e3699873d1527aaf0facc335f535\/binaryfx\" style=\"color: rgb(66, 139, 202);\">Clicking here to view your message<\/a><\/u><\/b><br><br>Talk soon,<br><br>Monica &amp; Dave<br><br><br><br><br><br><br><br><br><\/td>                                       <\/tr><\/tbody><\/table><\/td>                           <\/tr><\/tbody><\/table><\/td>               <\/tr><\/tbody><\/table><!-- End of Text --><div style=\"display:none\" class=\"fn-hide ui-state-highlight ui-droppable\"><\/div><\/div><p style=\"margin:30px 0 20px 0; font-size:12px;\">To update or remove your contact information please <a href=\"http:\/\/tracking.icbounce25.com\/uml\/x8rQV0RLad\/51f4efbfb3e18f4ea053c4d3d282c4e2\/7f3b61ca89cacc525e7e39450d65495c\/binaryfx\" style=\"text-decoration:none; color:#08c;\">Manage Your Subscription<\/a><\/p><div style=\"color:#0b5875; background:#f3f5f5; padding:10px; font-size:12px; overflow:hidden;\"><p style=\"font-weight:bold; margin:0 0 9px 0\">This email was sent to xxx@xxxxxxx.xxx by:<\/p><span style=\"margin:0;\">WAHP<\/span>, <span style=\"margin:0;\">575 6th Ave<\/span>, <span style=\"margin:0;\">San Diego<\/span>, <span style=\"margin:0;\">CA<\/span>, <span style=\"margin:0;\">United States<\/span>, <span style=\"margin:0;\">92101<\/span><\/div><img src=\"http:\/\/tracking.icbounce25.com\/getimage\/51f4efbfb3e18f4ea053c4d3d282c4e2$$$607\/binaryfx\" width=\"0\" height=\"0\" border=\"0\" \/>\n",
         "headers":"X-Spam-Flag: YES\nX-Spam-Score: 11.351\nX-Spam-Status: Yes, score=11.351 required=4 tests=[IVM_BL=2, RDNS_NONE=2,\n\tZ_RDNS_BL=2, RCVD_IN_IVMSIP24=1, URIBL_IVMURI=1, Z_JANUS_S_SCORE=1,\n\tZ_RECIPS_READ_BROWSER=1, Z_GENERIC_HI_UNSUB=0.5, Z_IMPERSONAL_RECIPS=0.5, Z_RCPT_DOM_NOT_IN_HEADERS=0.5,\n\tZ_URI_MISMATCH=0.5, HTML_IMAGE_ONLY_32=0.3, HTML_IMG_ONLY=0.1, Z_DIFF_FROM_TO_DOMAIN=0.1,\n\tZ_GENERIC_HI=0.1, Z_RCPT_NOT_IN_HEADERS=0.1, T_Z_HAS_DKIM_HEAD=0.01, Z_DIFF_FROM_TRIM_DOMAIN=0.01,\n\tZ_MED_EML=0.01, Z_MED_SUBJ=0.01, Z_MULTI_RECIPS=0.01, Z_SIZE_5k_10k=0.01,\n\tZ_WHITE_TEXT=0.01, HTML_MESSAGE=0.001, Z_JANUS_S=0.001, Z_MULTIBL=0.001,\n\tZS_LIST_HEAD=-1, LIST_UNSUB=-0.1, SPF_PASS=-0.1, Z_LIST_BODY_UNSUB=-0.1,\n\tZ_NL_UNSUB=-0.1, HAS_SENDER=-0.01, Z_ANY_UNSUB=-0.01, ZS_LIST_UNSUB2X=-0.001,\n\tZ_NL_HEADER=-0.001] autolearn=disabled\nReceived: from 127.0.0.1 (127.0.0.1:10024)\n\t(original ip: 63.143.37.175)\n\tby kitty.zerospam.ca (Themis) with ESMTP id tyJPSJsuwaU7Bq6u4Xi;\n\tMon, 30 Jan 2017 02:18:47 -0500\nX-ZEROSPAM-Spammy-Score: 2.222\nX-ZS-Size: 6262\nReceived-SPF: Pass (dp666@zerospam.ca: domain of mail37.dls130.vm1.icbounce25.com designates 63.143.37.175 as permitted sender) client-ip=63.143.37.175; envelope-from=\"bounce-eleanorestirk=xxxxx.xxx@mail37.dls130.vm1.icbounce25.com\"; helo=legendery.net; receiver=dp666@zerospam.ca; mechanism=\"ip4:63.143.37.0\/24\"; identity=mailfrom\nReceived: from legendery.net (unknown [63.143.37.175])\n        by kitty.zerospam.ca (Postfix) with ESMTP id 3vBgjg0NfLz10\n        for <dp666@zerospam.ca>; Mon, 30 Jan 2017 02:18:46 -0500 (EST)\nX-Original-To: phpfeed@zstest.com\nDelivered-To: phpfeed@sonya.zerospam.ca\nDKIM-Signature: v=1; a=rsa-sha1; c=relaxed\/relaxed; s=dikkim; d=icbounce25.com;\n h=Sender:Message-ID:Date:Subject:From:To:MIME-Version:Content-Type:List-Unsubscribe;\n i=binaryfx@icbounce25.com; bh=OThAX50NI9S8+BRrmuqX4MqtAtE=;\n b=dgUJyh2iiaYQ5c32wIo99GxhKWXTGL4NTogh1Piw2f9nJUaFTjaMYSAHO\/7ldyVN7lzrPOApCYJp\n QKyJoybOPdcCzEgBgq4rLHdE3jTkspZhZNZ\/Wcy2MYgUPy5lgAlYJPHJyDlZS2YHm0GhHrtMtcH2\n BJ4VH8ZzZN0sA7lF\/OE=\nDomainKey-Signature: a=rsa-sha1; c=nofws; q=dns; s=dikkim; d=icbounce25.com;\n b=o3liwn4PMaKlyyaqHl4Vy8oEYnXtV6BKInYK3GtjVF2SyU7K5ArD2sv1MZiO8ZWS0BsQtyCXpK0q\n kd4TvM0+BYdR5ri6qYwhsveTbA0xSyosNGiQQrvuJmPOExNe\/9sXAljg343\/duOxfGXJ70wEaNuf\n 2aoNNhoPWl8EyixJz6c=;\nSender: binaryfx@icbounce25.com\nMessage-ID: <0000014g3ylatfbs-0dc4w821-472d-2d5f-c85b-lnrx0dq1pk2i-000000@binaryfx.icbounce25.com>\nDate: Mon, 30 Jan 2017 02:18:21 -0500\nSubject: You have (1) new YouTube message. \nFrom: Monica <monica@binarypaycheck.com>\nTo: \" \" <xxx@xxxxxxx.xxx>\nMIME-Version: 1.0\nContent-Type: multipart\/alternative;\n boundary=148576070196bbf94306499e2d8f8a0cdd620dfde0\nX-Complaints-To: abuse@icbounce25.com\nX-Subscription: Subscribed on 10\/19\/2016, via web form, from import\nList-Unsubscribe: <http:\/\/tracking.icbounce25.com\/uml\/x8rQV0RLad\/51f4efbfb3e18f4ea053c4d3d282c4e2\/7f3b61ca89cacc525e7e39450d65495c\/binaryfx>\nX-Mailer: Sendlane 1.0",
         "status":{  
            "type":"quarantined",
            "changed":null,
            "user":{  
               "name":null,
               "organization":null
            }
         }
      }
   }
}';
        $config = $this->preSuccess($jsonResult);

        $request = (new ReadQuarantineRequest)
            ->setQuarantineType(QuarantineTypeEnum::SYSTEM());

        $client = new ProvulusClient($config);
        $client->processRequest($request);
    }

    /**
     * @test
     * @expectedException \Exception
     **/
    public function user_quarantine_message_read_userId_not_set()
    {

        $jsonResult
            = '{  
   "data":{  
      "type":"quarantineMessage",
      "id":"851213",
      "attributes":{  
         "organization_id":1,
         "received_at":"2017-01-30 07:18:47.000000 +00:00",
         "score":11.351
         ,
         "from":[  
            {  
               "local_part":"bounce-eleanorestirk=xxxxx.xxx",
               "domain_part":"mail37.dls130.vm1.icbounce25.com",
               "name":null
            },
            {  
               "local_part":"monica",
               "domain_part":"binarypaycheck.com",
               "name":"Monica"
            }
         ],
         "to":[  
            {  
               "local_part":"dp666",
               "domain_part":"zerospam.ca",
               "name":null
            }
         ],
         "subject":"You have (1) new YouTube message.",
         "attachments":[  

         ],
         "plainText":"{&quot;id&quot;:198793042,&quot;msg_id&quot;:50222297,&quot;msg_data&quot;:&quot;Hey,\\n\\nOne of your friends has sent you a video message.\\n[http:\\\/\\\/tracking.icbounce25.com\\\/tc\\\/x8rQV0RLad\\\/51f4efbfb3e18f4ea053c4d3d282c4e2\\\/fa09e3699873d1527aaf0facc335f535\\\/binaryfx\\n\\nThis video message can be viewed on your browser. Be sure\\nto turn your speakers on if you cannot hear it.\\n\\n [http:\\\/\\\/tracking.icbounce25.com\\\/tc\\\/x8rQV0RLad\\\/51f4efbfb3e18f4ea053c4d3d282c4e2\\\/91cb564f651c1effe578669509766f63\\\/binaryfx\\nhere to view your message [http:\\\/\\\/tracking.icbounce25.com\\\/tc\\\/x8rQV0RLad\\\/51f4efbfb3e18f4ea053c4d3d282c4e2\\\/fa09e3699873d1527aaf0facc335f535\\\/binaryfx\\n\\nTalk soon,\\n\\nMonica &amp; Dave\\n\\n\\n\\nThis email was sent to xxx@xxxxxxx.xxx by:\\nWAHP, 575 6th Ave, San Diego, CA, United States, 92101\\n\\nTo update or remove your contact information: \\nhttp:\\\/\\\/tracking.icbounce25.com\\\/uml\\\/x8rQV0RLad\\\/51f4efbfb3e18f4ea053c4d3d282c4e2\\\/7f3b61ca89cacc525e7e39450d65495c\\\/binaryfx\\n\\n\\n&quot;,&quot;data_type&quot;:{}}",
         "html":"<div class=\"repeatable ui-droppable\">     <!-- Start of Text -->       <table width=\"100%\" bgcolor=\"#ffffff\" cellpadding=\"0\" cellspacing=\"0\" border=\"0\" id=\"backgroundTable\" block=\"Text\"><tbody><tr><td>                     <table width=\"86.5%\" cellpadding=\"0\" cellspacing=\"0\" border=\"0\" align=\"center\" class=\"device-width full-width editingoutline\" modulebg=\"edit\" bgcolor=\"#ffffff\" style=\"border-top-color: rgb(255, 255, 255);\"><tbody class=\"\"><tr vblock=\"edit\"><td style=\"padding:10px;\"><\/td><\/tr><tr vblock=\"edit\"><td style=\"padding:10px;\">                                 <table width=\"100%\" cellpadding=\"0\" cellspacing=\"0\" border=\"0\" align=\"center\" class=\"device-width\" style=\"max-width:620px; margin:0 auto;\" contentmod=\"\"><tbody class=\"\"><tr><td  align=\"left\" valign=\"middle\" style=\"font-family: arial, sans-serif; line-height: 20px; color: rgb(51, 51, 51); text-align: justify; font-size: 15px;\" mgpara=\"preheadertext\" class=\"merge_tags\">Hey,<br><br>One of your friends has sent you a <u><a href=\"http:\/\/tracking.icbounce25.com\/tc\/x8rQV0RLad\/51f4efbfb3e18f4ea053c4d3d282c4e2\/fa09e3699873d1527aaf0facc335f535\/binaryfx\" data-tag=\"style=\'color:rgb(66,\">video message.<\/a><\/u><br><br>This video message can be viewed on your browser. Be sure<br>to turn your speakers on if you cannot hear it.<br><br><b><u><a href=\"http:\/\/tracking.icbounce25.com\/tc\/x8rQV0RLad\/51f4efbfb3e18f4ea053c4d3d282c4e2\/f2108f32896c23954a21539d386e2725\/binaryfx\"><\/a><a href=\"http:\/\/tracking.icbounce25.com\/tc\/x8rQV0RLad\/51f4efbfb3e18f4ea053c4d3d282c4e2\/fa09e3699873d1527aaf0facc335f535\/binaryfx\" style=\"color: rgb(66, 139, 202);\">Clicking here to view your message<\/a><\/u><\/b><br><br>Talk soon,<br><br>Monica &amp; Dave<br><br><br><br><br><br><br><br><br><\/td>                                       <\/tr><\/tbody><\/table><\/td>                           <\/tr><\/tbody><\/table><\/td>               <\/tr><\/tbody><\/table><!-- End of Text --><div style=\"display:none\" class=\"fn-hide ui-state-highlight ui-droppable\"><\/div><\/div><p style=\"margin:30px 0 20px 0; font-size:12px;\">To update or remove your contact information please <a href=\"http:\/\/tracking.icbounce25.com\/uml\/x8rQV0RLad\/51f4efbfb3e18f4ea053c4d3d282c4e2\/7f3b61ca89cacc525e7e39450d65495c\/binaryfx\" style=\"text-decoration:none; color:#08c;\">Manage Your Subscription<\/a><\/p><div style=\"color:#0b5875; background:#f3f5f5; padding:10px; font-size:12px; overflow:hidden;\"><p style=\"font-weight:bold; margin:0 0 9px 0\">This email was sent to xxx@xxxxxxx.xxx by:<\/p><span style=\"margin:0;\">WAHP<\/span>, <span style=\"margin:0;\">575 6th Ave<\/span>, <span style=\"margin:0;\">San Diego<\/span>, <span style=\"margin:0;\">CA<\/span>, <span style=\"margin:0;\">United States<\/span>, <span style=\"margin:0;\">92101<\/span><\/div><img src=\"http:\/\/tracking.icbounce25.com\/getimage\/51f4efbfb3e18f4ea053c4d3d282c4e2$$$607\/binaryfx\" width=\"0\" height=\"0\" border=\"0\" \/>\n",
         "headers":"X-Spam-Flag: YES\nX-Spam-Score: 11.351\nX-Spam-Status: Yes, score=11.351 required=4 tests=[IVM_BL=2, RDNS_NONE=2,\n\tZ_RDNS_BL=2, RCVD_IN_IVMSIP24=1, URIBL_IVMURI=1, Z_JANUS_S_SCORE=1,\n\tZ_RECIPS_READ_BROWSER=1, Z_GENERIC_HI_UNSUB=0.5, Z_IMPERSONAL_RECIPS=0.5, Z_RCPT_DOM_NOT_IN_HEADERS=0.5,\n\tZ_URI_MISMATCH=0.5, HTML_IMAGE_ONLY_32=0.3, HTML_IMG_ONLY=0.1, Z_DIFF_FROM_TO_DOMAIN=0.1,\n\tZ_GENERIC_HI=0.1, Z_RCPT_NOT_IN_HEADERS=0.1, T_Z_HAS_DKIM_HEAD=0.01, Z_DIFF_FROM_TRIM_DOMAIN=0.01,\n\tZ_MED_EML=0.01, Z_MED_SUBJ=0.01, Z_MULTI_RECIPS=0.01, Z_SIZE_5k_10k=0.01,\n\tZ_WHITE_TEXT=0.01, HTML_MESSAGE=0.001, Z_JANUS_S=0.001, Z_MULTIBL=0.001,\n\tZS_LIST_HEAD=-1, LIST_UNSUB=-0.1, SPF_PASS=-0.1, Z_LIST_BODY_UNSUB=-0.1,\n\tZ_NL_UNSUB=-0.1, HAS_SENDER=-0.01, Z_ANY_UNSUB=-0.01, ZS_LIST_UNSUB2X=-0.001,\n\tZ_NL_HEADER=-0.001] autolearn=disabled\nReceived: from 127.0.0.1 (127.0.0.1:10024)\n\t(original ip: 63.143.37.175)\n\tby kitty.zerospam.ca (Themis) with ESMTP id tyJPSJsuwaU7Bq6u4Xi;\n\tMon, 30 Jan 2017 02:18:47 -0500\nX-ZEROSPAM-Spammy-Score: 2.222\nX-ZS-Size: 6262\nReceived-SPF: Pass (dp666@zerospam.ca: domain of mail37.dls130.vm1.icbounce25.com designates 63.143.37.175 as permitted sender) client-ip=63.143.37.175; envelope-from=\"bounce-eleanorestirk=xxxxx.xxx@mail37.dls130.vm1.icbounce25.com\"; helo=legendery.net; receiver=dp666@zerospam.ca; mechanism=\"ip4:63.143.37.0\/24\"; identity=mailfrom\nReceived: from legendery.net (unknown [63.143.37.175])\n        by kitty.zerospam.ca (Postfix) with ESMTP id 3vBgjg0NfLz10\n        for <dp666@zerospam.ca>; Mon, 30 Jan 2017 02:18:46 -0500 (EST)\nX-Original-To: phpfeed@zstest.com\nDelivered-To: phpfeed@sonya.zerospam.ca\nDKIM-Signature: v=1; a=rsa-sha1; c=relaxed\/relaxed; s=dikkim; d=icbounce25.com;\n h=Sender:Message-ID:Date:Subject:From:To:MIME-Version:Content-Type:List-Unsubscribe;\n i=binaryfx@icbounce25.com; bh=OThAX50NI9S8+BRrmuqX4MqtAtE=;\n b=dgUJyh2iiaYQ5c32wIo99GxhKWXTGL4NTogh1Piw2f9nJUaFTjaMYSAHO\/7ldyVN7lzrPOApCYJp\n QKyJoybOPdcCzEgBgq4rLHdE3jTkspZhZNZ\/Wcy2MYgUPy5lgAlYJPHJyDlZS2YHm0GhHrtMtcH2\n BJ4VH8ZzZN0sA7lF\/OE=\nDomainKey-Signature: a=rsa-sha1; c=nofws; q=dns; s=dikkim; d=icbounce25.com;\n b=o3liwn4PMaKlyyaqHl4Vy8oEYnXtV6BKInYK3GtjVF2SyU7K5ArD2sv1MZiO8ZWS0BsQtyCXpK0q\n kd4TvM0+BYdR5ri6qYwhsveTbA0xSyosNGiQQrvuJmPOExNe\/9sXAljg343\/duOxfGXJ70wEaNuf\n 2aoNNhoPWl8EyixJz6c=;\nSender: binaryfx@icbounce25.com\nMessage-ID: <0000014g3ylatfbs-0dc4w821-472d-2d5f-c85b-lnrx0dq1pk2i-000000@binaryfx.icbounce25.com>\nDate: Mon, 30 Jan 2017 02:18:21 -0500\nSubject: You have (1) new YouTube message. \nFrom: Monica <monica@binarypaycheck.com>\nTo: \" \" <xxx@xxxxxxx.xxx>\nMIME-Version: 1.0\nContent-Type: multipart\/alternative;\n boundary=148576070196bbf94306499e2d8f8a0cdd620dfde0\nX-Complaints-To: abuse@icbounce25.com\nX-Subscription: Subscribed on 10\/19\/2016, via web form, from import\nList-Unsubscribe: <http:\/\/tracking.icbounce25.com\/uml\/x8rQV0RLad\/51f4efbfb3e18f4ea053c4d3d282c4e2\/7f3b61ca89cacc525e7e39450d65495c\/binaryfx>\nX-Mailer: Sendlane 1.0",
         "status":{  
            "type":"quarantined",
            "changed":null,
            "user":{  
               "name":null,
               "organization":null
            }
         }
      }
   }
}';

        $config = $this->preSuccess($jsonResult);

        $request = (new ReadQuarantineRequest)
            ->setQuarantineType(QuarantineTypeEnum::USER())
            ->setOrganizationId(1)
            ->setMessageRecipientId(51473420);

        $client = new ProvulusClient($config);
        $client->processRequest($request);
    }

    /**
     * @test
     * @expectedException \Exception
     **/
    public function user_quarantine_message_read_messageId_not_set()
    {

        $jsonResult
            = '{  
   "data":{  
      "type":"quarantineMessage",
      "id":"851213",
      "attributes":{  
         "organization_id":1,
         "received_at":"2017-01-30 07:18:47.000000 +00:00",
         "score":11.351
         ,
         "from":[  
            {  
               "local_part":"bounce-eleanorestirk=xxxxx.xxx",
               "domain_part":"mail37.dls130.vm1.icbounce25.com",
               "name":null
            },
            {  
               "local_part":"monica",
               "domain_part":"binarypaycheck.com",
               "name":"Monica"
            }
         ],
         "to":[  
            {  
               "local_part":"dp666",
               "domain_part":"zerospam.ca",
               "name":null
            }
         ],
         "subject":"You have (1) new YouTube message.",
         "attachments":[  

         ],
         "plainText":"{&quot;id&quot;:198793042,&quot;msg_id&quot;:50222297,&quot;msg_data&quot;:&quot;Hey,\\n\\nOne of your friends has sent you a video message.\\n[http:\\\/\\\/tracking.icbounce25.com\\\/tc\\\/x8rQV0RLad\\\/51f4efbfb3e18f4ea053c4d3d282c4e2\\\/fa09e3699873d1527aaf0facc335f535\\\/binaryfx\\n\\nThis video message can be viewed on your browser. Be sure\\nto turn your speakers on if you cannot hear it.\\n\\n [http:\\\/\\\/tracking.icbounce25.com\\\/tc\\\/x8rQV0RLad\\\/51f4efbfb3e18f4ea053c4d3d282c4e2\\\/91cb564f651c1effe578669509766f63\\\/binaryfx\\nhere to view your message [http:\\\/\\\/tracking.icbounce25.com\\\/tc\\\/x8rQV0RLad\\\/51f4efbfb3e18f4ea053c4d3d282c4e2\\\/fa09e3699873d1527aaf0facc335f535\\\/binaryfx\\n\\nTalk soon,\\n\\nMonica &amp; Dave\\n\\n\\n\\nThis email was sent to xxx@xxxxxxx.xxx by:\\nWAHP, 575 6th Ave, San Diego, CA, United States, 92101\\n\\nTo update or remove your contact information: \\nhttp:\\\/\\\/tracking.icbounce25.com\\\/uml\\\/x8rQV0RLad\\\/51f4efbfb3e18f4ea053c4d3d282c4e2\\\/7f3b61ca89cacc525e7e39450d65495c\\\/binaryfx\\n\\n\\n&quot;,&quot;data_type&quot;:{}}",
         "html":"<div class=\"repeatable ui-droppable\">     <!-- Start of Text -->       <table width=\"100%\" bgcolor=\"#ffffff\" cellpadding=\"0\" cellspacing=\"0\" border=\"0\" id=\"backgroundTable\" block=\"Text\"><tbody><tr><td>                     <table width=\"86.5%\" cellpadding=\"0\" cellspacing=\"0\" border=\"0\" align=\"center\" class=\"device-width full-width editingoutline\" modulebg=\"edit\" bgcolor=\"#ffffff\" style=\"border-top-color: rgb(255, 255, 255);\"><tbody class=\"\"><tr vblock=\"edit\"><td style=\"padding:10px;\"><\/td><\/tr><tr vblock=\"edit\"><td style=\"padding:10px;\">                                 <table width=\"100%\" cellpadding=\"0\" cellspacing=\"0\" border=\"0\" align=\"center\" class=\"device-width\" style=\"max-width:620px; margin:0 auto;\" contentmod=\"\"><tbody class=\"\"><tr><td  align=\"left\" valign=\"middle\" style=\"font-family: arial, sans-serif; line-height: 20px; color: rgb(51, 51, 51); text-align: justify; font-size: 15px;\" mgpara=\"preheadertext\" class=\"merge_tags\">Hey,<br><br>One of your friends has sent you a <u><a href=\"http:\/\/tracking.icbounce25.com\/tc\/x8rQV0RLad\/51f4efbfb3e18f4ea053c4d3d282c4e2\/fa09e3699873d1527aaf0facc335f535\/binaryfx\" data-tag=\"style=\'color:rgb(66,\">video message.<\/a><\/u><br><br>This video message can be viewed on your browser. Be sure<br>to turn your speakers on if you cannot hear it.<br><br><b><u><a href=\"http:\/\/tracking.icbounce25.com\/tc\/x8rQV0RLad\/51f4efbfb3e18f4ea053c4d3d282c4e2\/f2108f32896c23954a21539d386e2725\/binaryfx\"><\/a><a href=\"http:\/\/tracking.icbounce25.com\/tc\/x8rQV0RLad\/51f4efbfb3e18f4ea053c4d3d282c4e2\/fa09e3699873d1527aaf0facc335f535\/binaryfx\" style=\"color: rgb(66, 139, 202);\">Clicking here to view your message<\/a><\/u><\/b><br><br>Talk soon,<br><br>Monica &amp; Dave<br><br><br><br><br><br><br><br><br><\/td>                                       <\/tr><\/tbody><\/table><\/td>                           <\/tr><\/tbody><\/table><\/td>               <\/tr><\/tbody><\/table><!-- End of Text --><div style=\"display:none\" class=\"fn-hide ui-state-highlight ui-droppable\"><\/div><\/div><p style=\"margin:30px 0 20px 0; font-size:12px;\">To update or remove your contact information please <a href=\"http:\/\/tracking.icbounce25.com\/uml\/x8rQV0RLad\/51f4efbfb3e18f4ea053c4d3d282c4e2\/7f3b61ca89cacc525e7e39450d65495c\/binaryfx\" style=\"text-decoration:none; color:#08c;\">Manage Your Subscription<\/a><\/p><div style=\"color:#0b5875; background:#f3f5f5; padding:10px; font-size:12px; overflow:hidden;\"><p style=\"font-weight:bold; margin:0 0 9px 0\">This email was sent to xxx@xxxxxxx.xxx by:<\/p><span style=\"margin:0;\">WAHP<\/span>, <span style=\"margin:0;\">575 6th Ave<\/span>, <span style=\"margin:0;\">San Diego<\/span>, <span style=\"margin:0;\">CA<\/span>, <span style=\"margin:0;\">United States<\/span>, <span style=\"margin:0;\">92101<\/span><\/div><img src=\"http:\/\/tracking.icbounce25.com\/getimage\/51f4efbfb3e18f4ea053c4d3d282c4e2$$$607\/binaryfx\" width=\"0\" height=\"0\" border=\"0\" \/>\n",
         "headers":"X-Spam-Flag: YES\nX-Spam-Score: 11.351\nX-Spam-Status: Yes, score=11.351 required=4 tests=[IVM_BL=2, RDNS_NONE=2,\n\tZ_RDNS_BL=2, RCVD_IN_IVMSIP24=1, URIBL_IVMURI=1, Z_JANUS_S_SCORE=1,\n\tZ_RECIPS_READ_BROWSER=1, Z_GENERIC_HI_UNSUB=0.5, Z_IMPERSONAL_RECIPS=0.5, Z_RCPT_DOM_NOT_IN_HEADERS=0.5,\n\tZ_URI_MISMATCH=0.5, HTML_IMAGE_ONLY_32=0.3, HTML_IMG_ONLY=0.1, Z_DIFF_FROM_TO_DOMAIN=0.1,\n\tZ_GENERIC_HI=0.1, Z_RCPT_NOT_IN_HEADERS=0.1, T_Z_HAS_DKIM_HEAD=0.01, Z_DIFF_FROM_TRIM_DOMAIN=0.01,\n\tZ_MED_EML=0.01, Z_MED_SUBJ=0.01, Z_MULTI_RECIPS=0.01, Z_SIZE_5k_10k=0.01,\n\tZ_WHITE_TEXT=0.01, HTML_MESSAGE=0.001, Z_JANUS_S=0.001, Z_MULTIBL=0.001,\n\tZS_LIST_HEAD=-1, LIST_UNSUB=-0.1, SPF_PASS=-0.1, Z_LIST_BODY_UNSUB=-0.1,\n\tZ_NL_UNSUB=-0.1, HAS_SENDER=-0.01, Z_ANY_UNSUB=-0.01, ZS_LIST_UNSUB2X=-0.001,\n\tZ_NL_HEADER=-0.001] autolearn=disabled\nReceived: from 127.0.0.1 (127.0.0.1:10024)\n\t(original ip: 63.143.37.175)\n\tby kitty.zerospam.ca (Themis) with ESMTP id tyJPSJsuwaU7Bq6u4Xi;\n\tMon, 30 Jan 2017 02:18:47 -0500\nX-ZEROSPAM-Spammy-Score: 2.222\nX-ZS-Size: 6262\nReceived-SPF: Pass (dp666@zerospam.ca: domain of mail37.dls130.vm1.icbounce25.com designates 63.143.37.175 as permitted sender) client-ip=63.143.37.175; envelope-from=\"bounce-eleanorestirk=xxxxx.xxx@mail37.dls130.vm1.icbounce25.com\"; helo=legendery.net; receiver=dp666@zerospam.ca; mechanism=\"ip4:63.143.37.0\/24\"; identity=mailfrom\nReceived: from legendery.net (unknown [63.143.37.175])\n        by kitty.zerospam.ca (Postfix) with ESMTP id 3vBgjg0NfLz10\n        for <dp666@zerospam.ca>; Mon, 30 Jan 2017 02:18:46 -0500 (EST)\nX-Original-To: phpfeed@zstest.com\nDelivered-To: phpfeed@sonya.zerospam.ca\nDKIM-Signature: v=1; a=rsa-sha1; c=relaxed\/relaxed; s=dikkim; d=icbounce25.com;\n h=Sender:Message-ID:Date:Subject:From:To:MIME-Version:Content-Type:List-Unsubscribe;\n i=binaryfx@icbounce25.com; bh=OThAX50NI9S8+BRrmuqX4MqtAtE=;\n b=dgUJyh2iiaYQ5c32wIo99GxhKWXTGL4NTogh1Piw2f9nJUaFTjaMYSAHO\/7ldyVN7lzrPOApCYJp\n QKyJoybOPdcCzEgBgq4rLHdE3jTkspZhZNZ\/Wcy2MYgUPy5lgAlYJPHJyDlZS2YHm0GhHrtMtcH2\n BJ4VH8ZzZN0sA7lF\/OE=\nDomainKey-Signature: a=rsa-sha1; c=nofws; q=dns; s=dikkim; d=icbounce25.com;\n b=o3liwn4PMaKlyyaqHl4Vy8oEYnXtV6BKInYK3GtjVF2SyU7K5ArD2sv1MZiO8ZWS0BsQtyCXpK0q\n kd4TvM0+BYdR5ri6qYwhsveTbA0xSyosNGiQQrvuJmPOExNe\/9sXAljg343\/duOxfGXJ70wEaNuf\n 2aoNNhoPWl8EyixJz6c=;\nSender: binaryfx@icbounce25.com\nMessage-ID: <0000014g3ylatfbs-0dc4w821-472d-2d5f-c85b-lnrx0dq1pk2i-000000@binaryfx.icbounce25.com>\nDate: Mon, 30 Jan 2017 02:18:21 -0500\nSubject: You have (1) new YouTube message. \nFrom: Monica <monica@binarypaycheck.com>\nTo: \" \" <xxx@xxxxxxx.xxx>\nMIME-Version: 1.0\nContent-Type: multipart\/alternative;\n boundary=148576070196bbf94306499e2d8f8a0cdd620dfde0\nX-Complaints-To: abuse@icbounce25.com\nX-Subscription: Subscribed on 10\/19\/2016, via web form, from import\nList-Unsubscribe: <http:\/\/tracking.icbounce25.com\/uml\/x8rQV0RLad\/51f4efbfb3e18f4ea053c4d3d282c4e2\/7f3b61ca89cacc525e7e39450d65495c\/binaryfx>\nX-Mailer: Sendlane 1.0",
         "status":{  
            "type":"quarantined",
            "changed":null,
            "user":{  
               "name":null,
               "organization":null
            }
         }
      }
   }
}';

        $config = $this->preSuccess($jsonResult);

        $request = (new ReadQuarantineRequest)
            ->setQuarantineType(QuarantineTypeEnum::USER())
            ->setOrganizationId(1)
            ->setMessageRecipientId(51473420);

        $client = new ProvulusClient($config);
        $client->processRequest($request);
    }


    /**
     * @test
     * @expectedException \Exception
     **/
    public function quarantineBindMessageTraitMessageIdNotSet()
    {

        $jsonResult
            = '{  
   "data":{  
      "type":"quarantineMessage",
      "id":"851213",
      "attributes":{  
         "organization_id":1,
         "received_at":"2017-01-30 07:18:47.000000 +00:00",
         "score":11.351
         ,
         "from":[  
            {  
               "local_part":"bounce-eleanorestirk=xxxxx.xxx",
               "domain_part":"mail37.dls130.vm1.icbounce25.com",
               "name":null
            },
            {  
               "local_part":"monica",
               "domain_part":"binarypaycheck.com",
               "name":"Monica"
            }
         ],
         "to":[  
            {  
               "local_part":"dp666",
               "domain_part":"zerospam.ca",
               "name":null
            }
         ],
         "subject":"You have (1) new YouTube message.",
         "attachments":[  

         ],
         "plainText":"{&quot;id&quot;:198793042,&quot;msg_id&quot;:50222297,&quot;msg_data&quot;:&quot;Hey,\\n\\nOne of your friends has sent you a video message.\\n[http:\\\/\\\/tracking.icbounce25.com\\\/tc\\\/x8rQV0RLad\\\/51f4efbfb3e18f4ea053c4d3d282c4e2\\\/fa09e3699873d1527aaf0facc335f535\\\/binaryfx\\n\\nThis video message can be viewed on your browser. Be sure\\nto turn your speakers on if you cannot hear it.\\n\\n [http:\\\/\\\/tracking.icbounce25.com\\\/tc\\\/x8rQV0RLad\\\/51f4efbfb3e18f4ea053c4d3d282c4e2\\\/91cb564f651c1effe578669509766f63\\\/binaryfx\\nhere to view your message [http:\\\/\\\/tracking.icbounce25.com\\\/tc\\\/x8rQV0RLad\\\/51f4efbfb3e18f4ea053c4d3d282c4e2\\\/fa09e3699873d1527aaf0facc335f535\\\/binaryfx\\n\\nTalk soon,\\n\\nMonica &amp; Dave\\n\\n\\n\\nThis email was sent to xxx@xxxxxxx.xxx by:\\nWAHP, 575 6th Ave, San Diego, CA, United States, 92101\\n\\nTo update or remove your contact information: \\nhttp:\\\/\\\/tracking.icbounce25.com\\\/uml\\\/x8rQV0RLad\\\/51f4efbfb3e18f4ea053c4d3d282c4e2\\\/7f3b61ca89cacc525e7e39450d65495c\\\/binaryfx\\n\\n\\n&quot;,&quot;data_type&quot;:{}}",
         "html":"<div class=\"repeatable ui-droppable\">     <!-- Start of Text -->       <table width=\"100%\" bgcolor=\"#ffffff\" cellpadding=\"0\" cellspacing=\"0\" border=\"0\" id=\"backgroundTable\" block=\"Text\"><tbody><tr><td>                     <table width=\"86.5%\" cellpadding=\"0\" cellspacing=\"0\" border=\"0\" align=\"center\" class=\"device-width full-width editingoutline\" modulebg=\"edit\" bgcolor=\"#ffffff\" style=\"border-top-color: rgb(255, 255, 255);\"><tbody class=\"\"><tr vblock=\"edit\"><td style=\"padding:10px;\"><\/td><\/tr><tr vblock=\"edit\"><td style=\"padding:10px;\">                                 <table width=\"100%\" cellpadding=\"0\" cellspacing=\"0\" border=\"0\" align=\"center\" class=\"device-width\" style=\"max-width:620px; margin:0 auto;\" contentmod=\"\"><tbody class=\"\"><tr><td  align=\"left\" valign=\"middle\" style=\"font-family: arial, sans-serif; line-height: 20px; color: rgb(51, 51, 51); text-align: justify; font-size: 15px;\" mgpara=\"preheadertext\" class=\"merge_tags\">Hey,<br><br>One of your friends has sent you a <u><a href=\"http:\/\/tracking.icbounce25.com\/tc\/x8rQV0RLad\/51f4efbfb3e18f4ea053c4d3d282c4e2\/fa09e3699873d1527aaf0facc335f535\/binaryfx\" data-tag=\"style=\'color:rgb(66,\">video message.<\/a><\/u><br><br>This video message can be viewed on your browser. Be sure<br>to turn your speakers on if you cannot hear it.<br><br><b><u><a href=\"http:\/\/tracking.icbounce25.com\/tc\/x8rQV0RLad\/51f4efbfb3e18f4ea053c4d3d282c4e2\/f2108f32896c23954a21539d386e2725\/binaryfx\"><\/a><a href=\"http:\/\/tracking.icbounce25.com\/tc\/x8rQV0RLad\/51f4efbfb3e18f4ea053c4d3d282c4e2\/fa09e3699873d1527aaf0facc335f535\/binaryfx\" style=\"color: rgb(66, 139, 202);\">Clicking here to view your message<\/a><\/u><\/b><br><br>Talk soon,<br><br>Monica &amp; Dave<br><br><br><br><br><br><br><br><br><\/td>                                       <\/tr><\/tbody><\/table><\/td>                           <\/tr><\/tbody><\/table><\/td>               <\/tr><\/tbody><\/table><!-- End of Text --><div style=\"display:none\" class=\"fn-hide ui-state-highlight ui-droppable\"><\/div><\/div><p style=\"margin:30px 0 20px 0; font-size:12px;\">To update or remove your contact information please <a href=\"http:\/\/tracking.icbounce25.com\/uml\/x8rQV0RLad\/51f4efbfb3e18f4ea053c4d3d282c4e2\/7f3b61ca89cacc525e7e39450d65495c\/binaryfx\" style=\"text-decoration:none; color:#08c;\">Manage Your Subscription<\/a><\/p><div style=\"color:#0b5875; background:#f3f5f5; padding:10px; font-size:12px; overflow:hidden;\"><p style=\"font-weight:bold; margin:0 0 9px 0\">This email was sent to xxx@xxxxxxx.xxx by:<\/p><span style=\"margin:0;\">WAHP<\/span>, <span style=\"margin:0;\">575 6th Ave<\/span>, <span style=\"margin:0;\">San Diego<\/span>, <span style=\"margin:0;\">CA<\/span>, <span style=\"margin:0;\">United States<\/span>, <span style=\"margin:0;\">92101<\/span><\/div><img src=\"http:\/\/tracking.icbounce25.com\/getimage\/51f4efbfb3e18f4ea053c4d3d282c4e2$$$607\/binaryfx\" width=\"0\" height=\"0\" border=\"0\" \/>\n",
         "headers":"X-Spam-Flag: YES\nX-Spam-Score: 11.351\nX-Spam-Status: Yes, score=11.351 required=4 tests=[IVM_BL=2, RDNS_NONE=2,\n\tZ_RDNS_BL=2, RCVD_IN_IVMSIP24=1, URIBL_IVMURI=1, Z_JANUS_S_SCORE=1,\n\tZ_RECIPS_READ_BROWSER=1, Z_GENERIC_HI_UNSUB=0.5, Z_IMPERSONAL_RECIPS=0.5, Z_RCPT_DOM_NOT_IN_HEADERS=0.5,\n\tZ_URI_MISMATCH=0.5, HTML_IMAGE_ONLY_32=0.3, HTML_IMG_ONLY=0.1, Z_DIFF_FROM_TO_DOMAIN=0.1,\n\tZ_GENERIC_HI=0.1, Z_RCPT_NOT_IN_HEADERS=0.1, T_Z_HAS_DKIM_HEAD=0.01, Z_DIFF_FROM_TRIM_DOMAIN=0.01,\n\tZ_MED_EML=0.01, Z_MED_SUBJ=0.01, Z_MULTI_RECIPS=0.01, Z_SIZE_5k_10k=0.01,\n\tZ_WHITE_TEXT=0.01, HTML_MESSAGE=0.001, Z_JANUS_S=0.001, Z_MULTIBL=0.001,\n\tZS_LIST_HEAD=-1, LIST_UNSUB=-0.1, SPF_PASS=-0.1, Z_LIST_BODY_UNSUB=-0.1,\n\tZ_NL_UNSUB=-0.1, HAS_SENDER=-0.01, Z_ANY_UNSUB=-0.01, ZS_LIST_UNSUB2X=-0.001,\n\tZ_NL_HEADER=-0.001] autolearn=disabled\nReceived: from 127.0.0.1 (127.0.0.1:10024)\n\t(original ip: 63.143.37.175)\n\tby kitty.zerospam.ca (Themis) with ESMTP id tyJPSJsuwaU7Bq6u4Xi;\n\tMon, 30 Jan 2017 02:18:47 -0500\nX-ZEROSPAM-Spammy-Score: 2.222\nX-ZS-Size: 6262\nReceived-SPF: Pass (dp666@zerospam.ca: domain of mail37.dls130.vm1.icbounce25.com designates 63.143.37.175 as permitted sender) client-ip=63.143.37.175; envelope-from=\"bounce-eleanorestirk=xxxxx.xxx@mail37.dls130.vm1.icbounce25.com\"; helo=legendery.net; receiver=dp666@zerospam.ca; mechanism=\"ip4:63.143.37.0\/24\"; identity=mailfrom\nReceived: from legendery.net (unknown [63.143.37.175])\n        by kitty.zerospam.ca (Postfix) with ESMTP id 3vBgjg0NfLz10\n        for <dp666@zerospam.ca>; Mon, 30 Jan 2017 02:18:46 -0500 (EST)\nX-Original-To: phpfeed@zstest.com\nDelivered-To: phpfeed@sonya.zerospam.ca\nDKIM-Signature: v=1; a=rsa-sha1; c=relaxed\/relaxed; s=dikkim; d=icbounce25.com;\n h=Sender:Message-ID:Date:Subject:From:To:MIME-Version:Content-Type:List-Unsubscribe;\n i=binaryfx@icbounce25.com; bh=OThAX50NI9S8+BRrmuqX4MqtAtE=;\n b=dgUJyh2iiaYQ5c32wIo99GxhKWXTGL4NTogh1Piw2f9nJUaFTjaMYSAHO\/7ldyVN7lzrPOApCYJp\n QKyJoybOPdcCzEgBgq4rLHdE3jTkspZhZNZ\/Wcy2MYgUPy5lgAlYJPHJyDlZS2YHm0GhHrtMtcH2\n BJ4VH8ZzZN0sA7lF\/OE=\nDomainKey-Signature: a=rsa-sha1; c=nofws; q=dns; s=dikkim; d=icbounce25.com;\n b=o3liwn4PMaKlyyaqHl4Vy8oEYnXtV6BKInYK3GtjVF2SyU7K5ArD2sv1MZiO8ZWS0BsQtyCXpK0q\n kd4TvM0+BYdR5ri6qYwhsveTbA0xSyosNGiQQrvuJmPOExNe\/9sXAljg343\/duOxfGXJ70wEaNuf\n 2aoNNhoPWl8EyixJz6c=;\nSender: binaryfx@icbounce25.com\nMessage-ID: <0000014g3ylatfbs-0dc4w821-472d-2d5f-c85b-lnrx0dq1pk2i-000000@binaryfx.icbounce25.com>\nDate: Mon, 30 Jan 2017 02:18:21 -0500\nSubject: You have (1) new YouTube message. \nFrom: Monica <monica@binarypaycheck.com>\nTo: \" \" <xxx@xxxxxxx.xxx>\nMIME-Version: 1.0\nContent-Type: multipart\/alternative;\n boundary=148576070196bbf94306499e2d8f8a0cdd620dfde0\nX-Complaints-To: abuse@icbounce25.com\nX-Subscription: Subscribed on 10\/19\/2016, via web form, from import\nList-Unsubscribe: <http:\/\/tracking.icbounce25.com\/uml\/x8rQV0RLad\/51f4efbfb3e18f4ea053c4d3d282c4e2\/7f3b61ca89cacc525e7e39450d65495c\/binaryfx>\nX-Mailer: Sendlane 1.0",
         "status":{  
            "type":"quarantined",
            "changed":null,
            "user":{  
               "name":null,
               "organization":null
            }
         }
      }
   }
}';

        $config = $this->preSuccess($jsonResult);

        $request = (new ReadQuarantineRequest)
            ->setQuarantineType(QuarantineTypeEnum::ORGANIZATION())
            ->setOrganizationId(1);

        $client = new ProvulusClient($config);
        $client->processRequest($request);
    }

    /**
     * @test
     */
    public function read_org_quar_greymail_tagged()
    {
        $jsonResult
            = '{
  "data": {
    "type": "quarantineMessage",
    "id": "51473504",
    "attributes": {
      "organization_id": 1234,
      "received_at": "2017-08-17 21:23:12.000000 +00:00",
      "score": 7.631,
      "from": {
        "envelope": {
          "local_part": "bounce-3308-1631840-656-248",
          "domain_part": "fastfbcommissions.com",
          "name": null
        },
        "header": [
          {
            "local_part": "meghan",
            "domain_part": "fastfbcommissions.com",
            "name": "Meghan"
          }
        ]
      },
      "to": {
        "envelope": {
          "local_part": "test",
          "domain_part": "zerospam.ca",
          "name": null
        },
        "header": []
      },
      "subject": "Good news - Your application has been accepted.",
      "body": {
        "plainText": "Hey,\n\nGood news! Your application has been accepted!\n\nPlease review the details of the position Here\nhttp://www.clkmg.com/poweraff/tdata\n\nWhen I last checked there was only 2 positions available in your area\nso I recommend you apply now before all the positions are gone.\n\nIt\'s all explained RIGHT HERE..\nhttp://www.clkmg.com/poweraff/tdata\n\nCheers,\n\nMeghan\n\n-\n\n--\n\n\n\n\n--\n\n\n\n\n---\n\n5536 Carson St, Orlando, FL, United States, 97218\n\nIf you no longer want our emails, please Unsubscribe\nhttp://app.fastfbcommissions.com/u.php?p=s8/rs/6xz/rv/s8/rs/rt\n",
        "html": "<!DOCTYPE html PUBLIC \"-//W3C//DTD XHTML 1.0 Transitional//EN\" \"http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd\">\n<html>\n<head>\n</head>\n<body>\n<p><span style=\"font-size: medium;\">Hey,</span></p>\n<p><span style=\"font-size: medium;\">Good news! Your application has been accepted!</span></p>\n<p><span style=\"font-size: medium;\"><a href=\"http://app.fastfbcommissions.com/tl.php?p=s8/s8/rs/6xz/rv/rs//http%3A%2F%2Fwww.clkmg.com%2Fpoweraff%2Ftdata\"><strong>Please review the details of the position Here</strong></a></span></p>\n<p><span style=\"font-size: medium;\">When I last checked there was only 2 positions available in your area</span><br /><span style=\"font-size: medium;\">so I recommend you apply now before all the positions are gone.</span></p>\n<p><span style=\"font-size: medium;\"><a href=\"http://app.fastfbcommissions.com/tl.php?p=s8/s8/rs/6xz/rv/rs//http%3A%2F%2Fwww.clkmg.com%2Fpoweraff%2Ftdata\"><strong>It\'s all explained RIGHT HERE..</strong></a></span></p>\n<p><span style=\"font-size: medium;\">Cheers,</span></p>\n<p><span style=\"font-size: medium;\">Meghan</span></p>\n<p>-</p>\n<p>--</p>\n<p></p>\n<p><br />--</p>\n<p></p>\n<p><br />---</p>\n<p>5536 Carson St, Orlando, FL, United States, 97218</p>\n<p>If you no longer want our emails, please <a href=\"http://app.fastfbcommissions.com/u.php?p=s8/rs/6xz/rv/s8/rs/rt\">Unsubscribe</a></p>\n\n<img src=\"http://app.fastfbcommissions.com/to.php?p=s8/s8/rs/6xz/rv/rs\" width=\"5\" height=\"2\" alt=\".\">\n\n</body>\n</html>\n\n"
      },
      "headers": "X-Spam-Flag: YES\nX-Spam-Score: 7.631\nX-Spam-Status: Yes, score=7.631 required=4 tests=[RDNS_NONE=2, ZS_SHORT_LNK=1,\n\tZ_CLICKBAIT_DUO=1, Z_JANUS_S_RSCORE=1, Z_SHORT_CLICKBAIT=1, Z_SMALL_SIZE_CLICKBAIT=1,\n\tZ_GENERIC_HI_UNSUB=0.5, Z_IMPERSONAL_RECIPS=0.5, Z_RCPT_DOM_NOT_IN_HEADERS=0.5, Z_URI_MISMATCH=0.5,\n\tHTML_IMAGE_ONLY_16=0.3, HTML_IMG_ONLY=0.1, ZS_OPENME=0.1, Z_GENERIC_HI=0.1,\n\tZ_RCPT_NOT_IN_HEADERS=0.1, T_Z_HAS_DKIM_HEAD=0.01, Z_MED_SUBJ=0.01, Z_MULTI_RECIPS=0.01,\n\tZ_SHORT_EML=0.01, Z_SIZE_3k_5k=0.01, HTML_MESSAGE=0.001, Z_JANUS_S=0.001,\n\tZ_JANUS_SPAM_REDUX=0.001, ZS_LIST_HEAD=-1, Z_HAM_CAREER=-0.5, Z_UNSUB_SPF=-0.2,\n\tLIST_UNSUB=-0.1, SPF_PASS=-0.1, Z_LIST_BODY_UNSUB=-0.1, Z_NL_UNSUB=-0.1,\n\tHAS_SENDER=-0.01, Z_ANY_UNSUB=-0.01, ZS_LIST_UNSUB2X=-0.001, Z_NL_HEADER=-0.001] autolearn=disabled\nReceived: from 127.0.0.1 (127.0.0.1:10024)\n\t(original ip: 192.99.113.196)\n\tby kitty.zerospam.ca (Themis) with ESMTP id KpSZH3iFXni4Am9j8vJ;\n\tMon, 30 Jan 2017 02:23:11 -0500\nX-ZEROSPAM-Spammy-Score: 2.593\nX-ZS-Size: 4842\nReceived-SPF: Pass (test@zerospam.ca: domain of fastfbcommissions.com designates 192.99.113.196 as permitted sender) client-ip=192.99.113.196; envelope-from=\"bounce-3308-1631840-656-248@fastfbcommissions.com\"; helo=legendery.net; receiver=test@zerospam.ca; mechanism=\"ip4:192.99.113.196/30\"; identity=mailfrom\nReceived: from legendery.net (unknown [192.99.113.196])\n        by kitty.zerospam.ca (Postfix) with ESMTP id 3vBgpl6XfYz15\n        for <test@zerospam.ca>; Mon, 30 Jan 2017 02:23:11 -0500 (EST)\nX-Original-To: phpfeed@zstest.com\nDelivered-To: phpfeed@sonya.zerospam.ca\nDKIM-Signature: v=1; a=rsa-sha1; c=relaxed/relaxed; s=comfb;\n d=fastfbcommissions.com; \n h=Date:To:From:Reply-to:Subject:Message-ID:Sender:List-Unsubscribe:MIME-Version:Content-Type;\n i=user-rt@fastfbcommissions.com; \n bh=YmibNVurfTahHHmQnRY/tGVuchg=;\n b=CNWuyspON8EfJcES6J+Ske9t/W8UJBbLTeP/+FH8wc5Wq+50HBxeQKogoD9UvQqaqe28Vi5gvsyb\n +J1o6+TOpri4lmbVJB1TCj4JucDSlvCkaLREru/ezDJvEY9YOt7FLIil9t3vOqq0NWPL4JHmpnCK\n pdz+JgaefJB0QiVlEdY=\nDomainKey-Signature: a=rsa-sha1; c=nofws; q=dns; s=comfb;\n d=fastfbcommissions.com; \n b=ZL6Ou8vJxV0P79lRPmVa31Oe93+syybiiNhIiTel21Ekyb1pe6QdPR+ldbCxVEZOJWnI6mWGod6p\n D1EJR7R7/DykZ9FnTs2Bv/1MALAOw6oZqYnhboHuxuvSr4sEImYcfL1bPmrSQO0gkm59BiFb29ob\n I6oXXcTxZ7I/88wSe3w=;\nDate: Mon, 30 Jan 2017 07:26:07 +0000\nTo: \"xxx@xxxxxxx.xxx\" <xxx@xxxxxxx.xxx>\nFrom: Meghan <meghan@fastfbcommissions.com>\nReply-to: Meghan <meghan@fastfbcommissions.com>\nSubject: Good news - Your application has been accepted.\nMessage-ID: <180b895335c530a03d61fd0b1ecc4653@app.fastfbcommissions.com>\nX-Priority: 3\nSender: <user-rt@fastfbcommissions.com>\nX-Mailer: Email Sending System\nX-Complaints-To: abuse@fastfbcommissions.com\nList-Unsubscribe: <http://app.fastfbcommissions.com/u.php?p=s8/rs/6xz/rv/s8/rs>,\n <mailto:abuse@fastfbcommissions.com?subject=unsubscribe:s8-6xz-YmFvX2Jlc2FyZXNAbWFpbHUuYnJha2tldHJvZXAubmw%3D-rv-rt-rs>\nX-MessageID: s8-6xz-YmFvX2Jlc2FyZXNAbWFpbHUuYnJha2tldHJvZXAubmw%3D-rv-rt-rs\nX-Report-Abuse: <http://app.fastfbcommissions.com/report_abuse.php?mid=s8-6xz-YmFvX2Jlc2FyZXNAbWFpbHUuYnJha2tldHJvZXAubmw%3D-rv-rt-rs>\nX-SMTPAPI: {\"unique_args\":{\"abuse-id\":\"s8-6xz-YmFvX2Jlc2FyZXNAbWFpbHUuYnJha2tldHJvZXAubmw%3D-rv-rt-rs\"},\n \"category\":\"campaign\"}\nMIME-Version: 1.0\nContent-Type: multipart/alternative;\n boundary=\"b1_180b895335c530a03d61fd0b1ecc4653\"",
      "attachments": [],
      "tags": [
        "greymail"
      ],
      "status": {
        "type": "deleted",
        "changed": "2017-11-22 21:06:27.830594 +00:00",
        "user": {
          "name": "Test User",
          "organization": "ZEROSPAM"
        }
      },
      "related": []
    }
  }
}';

        $requestBody = [];
        $config      = $this->preSuccess($jsonResult);

        $request = (new ReadQuarantineRequest)
            ->setQuarantineType(QuarantineTypeEnum::ORGANIZATION())
            ->setOrganizationId(1234)
            ->setMessageRecipientId(51348230);

        $client = new ProvulusClient($config);
        $client->processRequest($request);
        $response = $request->getResponse();

        $this->validateRequest($config, $requestBody);
        $this->validateUrl($config, '/orgs/1234/quar/51348230');
        self::assertTrue($response->tags->first()->is(StatusTagsEnum::GREYMAIL()));
    }

    /**
     * @test
     **/
    public function delete_message()
    {

        $jsonResult  = [];
        $jsonRequest = [];

        $config = $this->preSuccess($jsonResult);

        $request = (new QuarantineActionRequest())
            ->setQuarantineAction(QuarantineActionEnum::DELETE())
            ->setQuarantineType(QuarantineTypeEnum::ORGANIZATION())
            ->setOrganizationId(1)
            ->setMessageRecipientId(51473420);

        $client = new ProvulusClient($config);
        $client->processRequest($request);

        $this->validateRequestUrl($config, ['/orgs/1/quar/51473420/delete']);
        $this->validateRequest($config, $jsonRequest);

        self::assertTrue($request->getResponse() instanceof EmptyResponse);
    }

    /**
     * @test
     **/
    public function release_message()
    {
        $jsonResult  = [];
        $jsonRequest = [];

        $config = $this->preSuccess($jsonResult);

        $request = (new QuarantineActionRequest())
            ->setQuarantineAction(QuarantineActionEnum::RELEASE())
            ->setQuarantineType(QuarantineTypeEnum::ORGANIZATION())
            ->setOrganizationId(1)
            ->setMessageRecipientId(51473420);

        $client = new ProvulusClient($config);
        $client->processRequest($request);

        $this->validateRequestUrl($config, ['/orgs/1/quar/51473420/release']);
        $this->validateRequest($config, $jsonRequest);

        self::assertTrue($request->getResponse() instanceof EmptyResponse);
    }

    /**
     * @test
     **/
    public function allow_message()
    {
        $jsonResult  = [];
        $jsonRequest = [];

        $config = $this->preSuccess($jsonResult);

        $request = (new QuarantineActionRequest())
            ->setQuarantineAction(QuarantineActionEnum::RELEASE_AND_ALLOW())
            ->setQuarantineType(QuarantineTypeEnum::ORGANIZATION())
            ->setOrganizationId(1)
            ->setMessageRecipientId(51473420);

        $client = new ProvulusClient($config);
        $client->processRequest($request);

        $this->validateRequestUrl($config, ['/orgs/1/quar/51473420/allow']);
        $this->validateRequest($config, $jsonRequest);
    }

    /**
     * @test
     **/
    public function full_content_message()
    {
        $jsonResult
                     = '{  
           "data":{  
              "type":"messageBlob",
              "id":"198769311",
              "attributes":{  
                 "msg_id":50217186,
                 "msg_data":"X-Spam-Flag: YES\nX-Spam-Score: 12.988\nX-Spam-Status: Yes, score=12.988 required=4 tests=[Z_NETFLIX_SPOOF=4, Z_PH_COMPANY_ACCT=3,\n\tZ_MISMATCH_ACCT=2, BODY_URI_ONLY=1, Z_TWIN_MISMATCH=1, Z_UPDATE_ACCT=1,\n\tSPF_SOFTFAIL=0.5, ZS_WE_OUR=0.3, MIME_HTML_ONLY=0.1, ZS_OPENME=0.1,\n\tZ_BODY_ACC_SUSP=0.1, Z_SPOOFED_FM=0.1, Z_SUBJ_ACC_SUSP=0.1, FISHY_SPF=0.01,\n\tZS_BADHOTMAIL=0.01, ZS_BADLIVE=0.01, Z_BAD_FM=0.01, Z_LONG_EML=0.01,\n\tZ_MED_SUBJ=0.01, Z_SAY_HI_PLZ=0.01, Z_SIZE_10k_20k=0.01, FREEMAIL_FROM=0.001,\n\tHTML_MESSAGE=0.001, Z_ANY_SPOOF=0.001, Z_JANUS_HAM_CANCEL=0.001, Z_JANUS_VH=0.001,\n",
                 "data_type":"full_content"
              }
           }
        }';
        $jsonRequest = [];

        $config = $this->preSuccess($jsonResult);

        $request = (new FullMessageQuarantineRequest)
            ->setQuarantineType(QuarantineTypeEnum::ORGANIZATION())
            ->setOrganizationId(1)
            ->setMessageRecipientId(51473420);

        $client = new ProvulusClient($config);
        $client->processRequest($request);

        $this->validateRequestUrl($config, ['/orgs/1/quar/51473420/content']);
        $this->validateRequest($config, $jsonRequest);

        $response = $request->getResponse();
        $this->assertEquals(50217186, $response->get('msg_id'));
    }

    /**
     * @test
     **/
    public function restore_message()
    {
        $jsonResult  = [];
        $jsonRequest = [];

        $config = $this->preSuccess($jsonResult);

        $request = (new QuarantineActionRequest())
            ->setQuarantineAction(QuarantineActionEnum::RESTORE())
            ->setQuarantineType(QuarantineTypeEnum::ORGANIZATION())
            ->setOrganizationId(1)
            ->setMessageRecipientId(51473420);

        $client = new ProvulusClient($config);
        $client->processRequest($request);

        $this->validateRequestUrl($config, ['/orgs/1/quar/51473420/restore']);
        $this->validateRequest($config, $jsonRequest);

        $response = $request->getResponse();
        self::assertInstanceOf(EmptyResponse::class, $response);
    }
}
