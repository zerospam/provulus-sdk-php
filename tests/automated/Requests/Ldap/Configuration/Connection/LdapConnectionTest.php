<?php
/**
 * Created by PhpStorm.
 * User: ycoutu
 * Date: 29/08/18
 * Time: 3:53 PM
 */

namespace automated\Requests\Ldap\Configuration\Connection;

use ProvulusSDK\Client\ProvulusClient;
use ProvulusSDK\Client\Request\Ldap\Configuration\Connection\LdapConnectionBulkDeleteRequest;
use ProvulusSDK\Client\Request\Ldap\Configuration\Connection\LdapConnectionCreateRequest;
use ProvulusSDK\Client\Request\Ldap\Configuration\Connection\LdapConnectionDeleteRequest;
use ProvulusSDK\Client\Request\Ldap\Configuration\Connection\LdapConnectionIndexRequest;
use ProvulusSDK\Client\Request\Ldap\Configuration\Connection\LdapConnectionReadRequest;
use ProvulusSDK\Client\Request\Ldap\Configuration\Connection\LdapConnectionTestRequest;
use ProvulusSDK\Client\Request\Ldap\Configuration\Connection\LdapConnectionUpdateRequest;
use ProvulusSDK\Client\Response\EmptyResponse;
use ProvulusSDK\Client\Response\Ldap\Configuration\Connection\LdapConnectionResponse;
use ProvulusTest\TestCaseApiKey;

class LdapConnectionTest extends TestCaseApiKey
{
    /** @test */
    public function create_ldap_connection_success()
    {
        $jsonRequest = '{
	"hostname": "ldap.hostname.com",
	"port": 391,
	"is_ssl_enabled": false,
	"user_dn": "cn=admin,dc=example,dc=org",
	"password": "admin",
	"search_dn": "dc=users,dc=example,dc=org",
	"email_binding": "mail",
    "alias_binding": "mailAlternateAddress",
    "first_name_binding": "cn",
    "last_name_binding": "sn",
    "language_binding": "preferredLanguage",
    "sso_binding": "dn",
    "filter": "(description=GR1)",
    "pagination": 100
}';
        $jsonResponse = '{
    "data": {
        "type": "ldap-connection",
        "id": "21",
        "attributes": {
            "ldap_config_id": 123,
            "hostname": "ldap.hostname.com",
            "port": 391,
            "is_ssl_enabled": false,
            "user_dn": "cn=admin,dc=example,dc=org",
            "search_dn": "dc=users,dc=example,dc=org",
            "email_binding": "mail",
            "alias_binding": "mailAlternateAddress",
            "first_name_binding": "cn",
            "last_name_binding": "sn",
            "language_binding": "preferredLanguage",
            "sso_binding": "dn",
            "filter": "(description=GR1)",
            "pagination": 100,
            "updated_at": "2018-08-29 14:29:00.304168 +00:00",
            "created_at": "2018-08-29 14:29:00.304168 +00:00"
        }
    }
}';

        $config = $this->preSuccess($jsonResponse);

        $client = new ProvulusClient($config);

        $request = new LdapConnectionCreateRequest();

        $request
            ->setOrganizationId(123)
            ->setHostname("ldap.hostname.com")
            ->setPort(391)
            ->setIsSslEnabled(false)
            ->setUserDn("cn=admin,dc=example,dc=org")
            ->setPassword("admin")
            ->setSearchDn("dc=users,dc=example,dc=org")
            ->setEmailBinding("mail")
            ->setAliasBinding("mailAlternateAddress")
            ->setFirstNameBinding("cn")
            ->setLastNameBinding("sn")
            ->setLanguageBinding("preferredLanguage")
            ->setSsoBinding("dn")
            ->setFilter("(description=GR1)")
            ->setPagination(100);

        $client->processRequest($request);

        $this->validateRequest($config, $jsonRequest);
        $this->validateUrl($config, 'orgs/123/ldap/connections');

        $response = $request->getResponse();

        $this->assertEquals(123, $response->ldap_config_id);
        $this->assertEquals("ldap.hostname.com", $response->hostname);
        $this->assertEquals(391, $response->port);
        $this->assertFalse($response->is_ssl_enabled);
        $this->assertEquals("cn=admin,dc=example,dc=org", $response->user_dn);
        $this->assertEquals("dc=users,dc=example,dc=org", $response->search_dn);
        $this->assertEquals("mail", $response->email_binding);
        $this->assertEquals("mailAlternateAddress", $response->alias_binding);
        $this->assertEquals("cn", $response->first_name_binding);
        $this->assertEquals("sn", $response->last_name_binding);
        $this->assertEquals("preferredLanguage", $response->language_binding);
        $this->assertEquals("dn", $response->sso_binding);
        $this->assertEquals("(description=GR1)", $response->filter);
        $this->assertEquals(100, $response->pagination);
    }

    /** @test */
    public function index_ldap_connection_success()
    {
        $jsonRequest = '{}';
        $jsonResponse = '{
    "data": [
        {
            "type": "ldap-connection",
            "id": "19",
            "attributes": {
                "ldap_config_id": 7,
                "hostname": "ldap.net",
                "is_ssl_enabled": false,
                "port": 389,
                "user_dn": "cn=admin,dc=example,dc=org",
                "search_dn": "dc=users,dc=example,dc=org",
                "email_binding": "mail",
                "alias_binding": null,
                "first_name_binding": null,
                "last_name_binding": null,
                "language_binding": null,
                "sso_binding": null,
                "last_sync_id": 27018,
                "last_sync_changed_id": 27018,
                "filter": null,
                "pagination": 50,
                "created_at": null,
                "updated_at": "2018-08-29 15:02:13.000000 +00:00"
            }
        },
        {
            "type": "ldap-connection",
            "id": "20",
            "attributes": {
                "ldap_config_id": 7,
                "hostname": "ldap.com",
                "is_ssl_enabled": true,
                "port": 636,
                "user_dn": "cn=admin,dc=domain,dc=com",
                "search_dn": "dc=users,dc=domain,dc=com",
                "email_binding": "email",
                "alias_binding": "mailAlternateAddress",
                "first_name_binding": "cn",
                "last_name_binding": "sn",
                "language_binding": "preferredLanguage",
                "sso_binding": null,
                "last_sync_id": 27019,
                "last_sync_changed_id": 27019,
                "filter": null,
                "pagination": 500,
                "created_at": "2018-08-29 14:28:46.000000 +00:00",
                "updated_at": "2018-08-29 15:02:13.000000 +00:00"
            }
        },
        {
            "type": "ldap-connection",
            "id": "21",
            "attributes": {
                "ldap_config_id": 7,
                "hostname": "ldap.ca",
                "is_ssl_enabled": false,
                "port": 333,
                "user_dn": "cn=admin,dc=last,dc=net",
                "search_dn": "dc=users,dc=last,dc=net",
                "email_binding": "mail",
                "alias_binding": null,
                "first_name_binding": null,
                "last_name_binding": null,
                "language_binding": null,
                "sso_binding": null,
                "last_sync_id": 27020,
                "last_sync_changed_id": 27020,
                "filter": "(description=SYNC)",
                "pagination": 200,
                "created_at": "2018-08-29 14:29:00.000000 +00:00",
                "updated_at": "2018-08-29 15:02:14.000000 +00:00"
            }
        }
    ],
    "meta": {
        "pagination": {
            "total": 3,
            "count": 3,
            "per_page": 15,
            "current_page": 1,
            "total_pages": 1
        }
    },
    "links": {
        "self": "http://cumulus.local/api/v1/orgs/7/ldap/connections?page=1",
        "first": "http://cumulus.local/api/v1/orgs/7/ldap/connections?page=1",
        "last": "http://cumulus.local/api/v1/orgs/7/ldap/connections?page=1"
    }
}';

        $config = $this->preSuccess($jsonResponse);

        $client = new ProvulusClient($config);

        $request = new LdapConnectionIndexRequest();

        $request->setOrganizationId(7);

        $client->processRequest($request);

        $this->validateRequest($config, $jsonRequest);
        $this->validateUrl($config, 'orgs/7/ldap/connections');

        $response = $request->getResponse();

        $data = $response->data()->toArray();

        $this->assertEquals(3, count($data));

        /** @var LdapConnectionResponse $second */
        $second = $data[1];

        $this->assertEquals(7, $second->ldap_config_id);
        $this->assertEquals("ldap.com", $second->hostname);
        $this->assertEquals(636, $second->port);
        $this->assertTrue($second->is_ssl_enabled);
        $this->assertEquals("cn=admin,dc=domain,dc=com", $second->user_dn);
        $this->assertEquals("dc=users,dc=domain,dc=com", $second->search_dn);
        $this->assertEquals("email", $second->email_binding);
        $this->assertEquals("mailAlternateAddress", $second->alias_binding);
        $this->assertEquals("cn", $second->first_name_binding);
        $this->assertEquals("sn", $second->last_name_binding);
        $this->assertEquals("preferredLanguage", $second->language_binding);
        $this->assertNull($second->sso_binding);
        $this->assertNull($second->filter);
        $this->assertEquals(500, $second->pagination);
    }

    /** @test */
    public function read_ldap_connection_success()
    {
        $jsonRequest = '{}';
        $jsonResponse = '{
    "data": {
        "type": "ldap-connection",
        "id": "21",
        "attributes": {
            "ldap_config_id": 777,
            "hostname": "ldap.host.ca",
            "port": 636,
            "is_ssl_enabled": true,
            "user_dn": "cn=admin,dc=example,dc=org",
            "search_dn": "dc=users,dc=example,dc=org",
            "email_binding": "email",
            "alias_binding": null,
            "first_name_binding": null,
            "last_name_binding": "name",
            "language_binding": "lang",
            "sso_binding": null,
            "filter": null,
            "pagination": 500,
            "updated_at": "2018-08-29 14:29:00.304168 +00:00",
            "created_at": "2018-08-29 14:29:00.304168 +00:00"
        }
    }
}';

        $config = $this->preSuccess($jsonResponse);

        $client = new ProvulusClient($config);

        $request = new LdapConnectionReadRequest();

        $request->setOrganizationId(777)
            ->setLdapConnectionId(21);

        $client->processRequest($request);

        $this->validateRequest($config, $jsonRequest);
        $this->validateUrl($config, 'orgs/777/ldap/connections/21');

        $response = $request->getResponse();

        $this->assertEquals(777, $response->ldap_config_id);
        $this->assertEquals("ldap.host.ca", $response->hostname);
        $this->assertEquals(636, $response->port);
        $this->assertTrue($response->is_ssl_enabled);
        $this->assertEquals("cn=admin,dc=example,dc=org", $response->user_dn);
        $this->assertEquals("dc=users,dc=example,dc=org", $response->search_dn);
        $this->assertEquals("email", $response->email_binding);
        $this->assertNull($response->alias_binding);
        $this->assertNull($response->first_name_binding);
        $this->assertEquals("name", $response->last_name_binding);
        $this->assertEquals("lang", $response->language_binding);
        $this->assertNull($response->sso_binding);
        $this->assertNull($response->filter);
        $this->assertEquals(500, $response->pagination);
    }

    /** @test */
    public function update_ldap_connection_success()
    {
        $jsonRequest = '{
	"hostname": "ldap.hostname.com",
	"port": 391,
	"is_ssl_enabled": false,
	"user_dn": "cn=admin,dc=example,dc=org",
	"password": "admin",
	"search_dn": "dc=users,dc=example,dc=org",
	"email_binding": "mail",
    "alias_binding": "mailAlternateAddress",
    "first_name_binding": "cn",
    "last_name_binding": "sn",
    "language_binding": "preferredLanguage",
    "sso_binding": null,
    "filter": null,
    "pagination": 100
}';
        $jsonResponse = '{
    "data": {
        "type": "ldap-connection",
        "id": "21",
        "attributes": {
            "ldap_config_id": 123,
            "hostname": "ldap.hostname.com",
            "port": 391,
            "is_ssl_enabled": false,
            "user_dn": "cn=admin,dc=example,dc=org",
            "search_dn": "dc=users,dc=example,dc=org",
            "email_binding": "mail",
            "alias_binding": "mailAlternateAddress",
            "first_name_binding": "cn",
            "last_name_binding": "sn",
            "language_binding": "preferredLanguage",
            "sso_binding": null,
            "filter": null,
            "pagination": 100,
            "updated_at": "2018-08-29 14:29:00.304168 +00:00",
            "created_at": "2018-08-29 14:29:00.304168 +00:00"
        }
    }
}';

        $config = $this->preSuccess($jsonResponse);

        $client = new ProvulusClient($config);

        $request = new LdapConnectionUpdateRequest();

        $request
            ->setOrganizationId(123)
            ->setLdapConnectionId(21)
            ->setHostname("ldap.hostname.com")
            ->setPort(391)
            ->setIsSslEnabled(false)
            ->setUserDn("cn=admin,dc=example,dc=org")
            ->setPassword("admin")
            ->setSearchDn("dc=users,dc=example,dc=org")
            ->setEmailBinding("mail")
            ->setAliasBinding("mailAlternateAddress")
            ->setFirstNameBinding("cn")
            ->setLastNameBinding("sn")
            ->setLanguageBinding("preferredLanguage")
            ->setSsoBinding(null)
            ->setFilter(null)
            ->setPagination(100);

        $client->processRequest($request);

        $this->validateRequest($config, $jsonRequest);
        $this->validateUrl($config, 'orgs/123/ldap/connections/21');

        $response = $request->getResponse();

        $this->assertEquals(123, $response->ldap_config_id);
        $this->assertEquals("ldap.hostname.com", $response->hostname);
        $this->assertEquals(391, $response->port);
        $this->assertFalse($response->is_ssl_enabled);
        $this->assertEquals("cn=admin,dc=example,dc=org", $response->user_dn);
        $this->assertEquals("dc=users,dc=example,dc=org", $response->search_dn);
        $this->assertEquals("mail", $response->email_binding);
        $this->assertEquals("mailAlternateAddress", $response->alias_binding);
        $this->assertEquals("cn", $response->first_name_binding);
        $this->assertEquals("sn", $response->last_name_binding);
        $this->assertEquals("preferredLanguage", $response->language_binding);
        $this->assertNull($response->sso_binding);
        $this->assertNull($response->filter);
        $this->assertEquals(100, $response->pagination);
    }

    /** @test */
    public function delete_ldap_connection_success()
    {
        $jsonRequest = '{}';
        $jsonResponse = '{}';

        $config = $this->preSuccess($jsonResponse);

        $client = new ProvulusClient($config);

        $request = new LdapConnectionDeleteRequest();

        $request->setOrganizationId(937)
            ->setLdapConnectionId(707);

        $client->processRequest($request);

        $this->validateRequest($config, $jsonRequest);
        $this->validateUrl($config, 'orgs/937/ldap/connections/707');

        $response = $request->getResponse();

        self::assertInstanceOf(EmptyResponse::class, $response);
    }

    /** @test */
    public function bulk_delete_ldap_connection_success()
    {
        $jsonRequest = '{"ids": [5, 10, 15, 22]}';
        $jsonResponse = '{}';

        $config = $this->preSuccess($jsonResponse);

        $client = new ProvulusClient($config);

        $request = new LdapConnectionBulkDeleteRequest();

        $request->setOrganizationId(6545)
                ->setIds([5, 10, 15, 22]);

        $client->processRequest($request);

        $this->validateRequest($config, $jsonRequest);
        $this->validateUrl($config, 'orgs/6545/ldap/connections');

        $response = $request->getResponse();

        self::assertInstanceOf(EmptyResponse::class, $response);
    }

    /** @test */
    public function test_ldap_connection_success()
    {
        $jsonRequest = '{
    "hostname": "host.name",
    "port": 389,
    "is_ssl_enabled": false,
    "user_dn": "cn=admin,dc=test,cd=com",
    "password": "abc123",
    "search_dn": "cn=users,dc=test,cd=com",
    "email_binding": "mail"
}';
        $jsonResponse = '{}';

        $config = $this->preSuccess($jsonResponse);

        $client = new ProvulusClient($config);

        $request = new LdapConnectionTestRequest();

        $request
            ->setOrganizationId(444)
            ->setHostname("host.name")
            ->setPort(389)
            ->setUserDn("cn=admin,dc=test,cd=com")
            ->setSearchDn("cn=users,dc=test,cd=com")
            ->setPassword("abc123")
            ->setEmailBinding("mail")
            ->setIsSslEnabled(false);

        $client->processRequest($request);

        $this->validateRequest($config, $jsonRequest);
        $this->validateUrl($config, 'orgs/444/ldap/test');

        $response = $request->getResponse();

        self::assertInstanceOf(EmptyResponse::class, $response);
    }
}
