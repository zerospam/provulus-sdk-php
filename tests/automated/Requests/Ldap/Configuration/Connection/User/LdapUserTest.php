<?php
/**
 * Created by PhpStorm.
 * User: ycoutu
 * Date: 14/06/17
 * Time: 11:46 AM
 */

namespace automated\Requests\Ldap\Configuration\Connection\User;

use ProvulusSDK\Client\ProvulusClient;
use ProvulusSDK\Client\Request\Ldap\Configuration\Connection\User\LdapUserIndexRequest;
use ProvulusSDK\Client\Request\Ldap\Configuration\Connection\User\LdapUserReadRequest;
use ProvulusSDK\Client\Response\Ldap\Configuration\Connection\User\Address\LdapAddressResponse;
use ProvulusSDK\Client\Response\Ldap\Configuration\Connection\User\LdapUserCollectionResponse;
use ProvulusSDK\Client\Response\Ldap\Configuration\Connection\User\LdapUserResponse;
use ProvulusSDK\Enum\Ldap\LdapAddressTypeEnum;
use ProvulusSDK\Enum\Locale\LocaleEnum;
use ProvulusTest\TestCaseApiKey;

class LdapUserTest extends TestCaseApiKey
{
    /** @test */
    public function index_ldap_user_success()
    {
        $jsonResponse =
            '{
                "data": [{
                    "type": "ldap-user",
                    "id": "18170",
                    "attributes": {
                        "ldap_connection_id": 3,
                        "dn_path": "mail=louise.lalonde@zerospam.ca,dc=example,dc=org",
                        "first_name": "Louise",
                        "last_name": "Lalonde",
                        "sso_id": "louise",
                        "language": null,
                        "is_declared_as_user": true
                    }
                }, {
                    "type": "ldap-user",
                    "id": "18172",
                    "attributes": {
                        "ldap_connection_id": 3,
                        "dn_path": "mail=philippe.poirier@zerospam.ca,dc=example,dc=org",
                        "first_name": "Philippe",
                        "last_name": "Poirier",
                        "sso_id": "phil",
                        "language": "fr_ca",
                        "is_declared_as_user": true
                    }
                }, {
                    "type": "ldap-user",
                    "id": "18169",
                    "attributes": {
                        "ldap_connection_id": 3,
                        "dn_path": "mail=natalie.nadeau@zerospam.ca,dc=example,dc=org",
                        "first_name": "Nataly",
                        "last_name": "Nadeau",
                        "sso_id": "loljj",
                        "language": "en_us",
                        "is_declared_as_user": true
                    }
                }],
                "meta": {
                    "pagination": {
                        "total": 3,
                        "count": 3,
                        "per_page": 15,
                        "current_page": 1,
                        "total_pages": 1
                    }
                }
            }';

        $jsonRequest = '{}';

        $config = $this->preSuccess($jsonResponse);

        $request = new LdapUserIndexRequest();
        $request->setOrganizationId(123)
                ->setLdapConnectionId(3)
                ->setLastName('i');

        $client = new ProvulusClient($config);

        $client->processRequest($request);

        /** @var LdapUserCollectionResponse $collectionResponse */
        $collectionResponse = $request->getResponse();

        /** @var LdapUserResponse $ldapUserResponse */
        $ldapUserResponse = $collectionResponse->data()->first();

        $this->validateRequest($config, $jsonRequest);
        $this->validateUrl($config, 'orgs/123/ldap/connections/3/users?last_name=i');

        $this->assertEquals(18170, $ldapUserResponse->id());
        $this->assertEquals('Louise', $ldapUserResponse->first_name);
        $this->assertEquals('mail=louise.lalonde@zerospam.ca,dc=example,dc=org', $ldapUserResponse->dn_path);
        $this->assertEquals(true, $ldapUserResponse->is_declared_as_user);
        $this->assertNull($ldapUserResponse->language);

        $this->assertEquals(3, $collectionResponse->getMetaData()->count);
    }

    /** @test */
    public function read_ldap_user_success()
    {
        $jsonResponse =
            '{
                "data": {
                    "type": "ldap-user",
                    "id": "18170",
                    "attributes": {
                        "ldap_connection_id": 55,
                        "dn_path": "mail=louise.lalonde@zerospam.ca,dc=example,dc=org",
                        "first_name": "Louise",
                        "last_name": "Lalonde",
                        "sso_id": "louise",
                        "language": "en_us",
                        "is_declared_as_user": true
                    },
                    "relationships": {
                        "ldapAddresses": {
                            "data": [{
                                "type": "ldap-address",
                                "id": "103306"
                            }, {
                                "type": "ldap-address",
                                "id": "103522"
                            }]
                        }
                    }
                },
                "included": [{
                    "type": "ldap-address",
                    "id": "103306",
                    "attributes": {
                        "ldap_user_id": 18170,
                        "address_type_code": "primary_address",
                        "address": "louise.lalonde@zerospam.ca"
                    }
                }, {
                    "type": "ldap-address",
                    "id": "103522",
                    "attributes": {
                        "ldap_user_id": 18170,
                        "address_type_code": "alias_address",
                        "address": "ll@zerospam.ca"
                    }   
                }]
            }';

        $jsonRequest = '{}';

        $config = $this->preSuccess($jsonResponse);

        $request = new LdapUserReadRequest();
        $request->setOrganizationId(87)
                ->setLdapConnectionId(55)
                ->setLdapUserId(18170)
                ->withLdapAddresses();

        $client = new ProvulusClient($config);

        $client->processRequest($request);

        /** @var LdapUserResponse $response */
        $response = $request->getResponse();

        $this->validateRequest($config, $jsonRequest);
        $this->validateUrl($config, 'orgs/87/ldap/connections/55/users/18170');

        $addresses = $response->ldap_addresses;
        /** @var LdapAddressResponse $first_address */
        $first_address = $addresses->first();

        $this->assertEquals(18170, $response->id());
        $this->assertEquals('Louise', $response->first_name);
        $this->assertEquals('mail=louise.lalonde@zerospam.ca,dc=example,dc=org', $response->dn_path);
        $this->assertEquals(true, $response->is_declared_as_user);
        $this->assertTrue($response->language->is(LocaleEnum::EN_US()));

        $this->assertEquals(2, count($addresses->toArray()));
        $this->assertEquals(103306, $first_address->id());
        $this->assertEquals('louise.lalonde@zerospam.ca', $first_address->address);
        $this->assertTrue($first_address->address_type_code->is(LdapAddressTypeEnum::PRIMARY_ADDRESS()),
            'The address type code of not of the expected type');
    }
}
