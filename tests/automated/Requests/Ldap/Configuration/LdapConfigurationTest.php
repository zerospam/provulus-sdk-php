<?php
/**
 * Created by PhpStorm.
 * User: pbb
 * Date: 06/06/17
 * Time: 4:08 PM
 */

namespace automated\Requests\Ldap\Configuration;


use ProvulusSDK\Client\ProvulusClient;
use ProvulusSDK\Client\Request\Ldap\Configuration\LdapConfigurationDeleteRequest;
use ProvulusSDK\Client\Request\Ldap\Configuration\LdapConfigurationSynchronizeRequest;
use ProvulusSDK\Client\Request\Ldap\Configuration\LdapConfigurationUpdateRequest;
use ProvulusSDK\Client\Request\Ldap\Configuration\LdapConfigurationCreateRequest;
use ProvulusSDK\Client\Request\Ldap\Configuration\LdapConfigurationReadRequest;
use ProvulusSDK\Client\Response\EmptyResponse;
use ProvulusSDK\Client\Response\Ldap\Configuration\LdapConfigurationResponse;
use ProvulusSDK\Enum\Ldap\LdapConfigSyncFrequencyEnum;
use ProvulusSDK\Enum\Ldap\LdapConfigurationStatusEnum;
use ProvulusSDK\Utils\Ldap\Synchronization;
use ProvulusSDK\Utils\Ldap\UserCreation;
use ProvulusTest\TestCaseApiKey;

/**
 * Class LdapConfigurationTest
 *
 * @package automated\Requests\LDAP
 */
class LdapConfigurationTest extends TestCaseApiKey
{
    /**
     * @test
     */
    public function ldap_config_creation_success()
    {
        $jsonResult = '{
           "data":{  
              "type":"ldap-config",
              "id":"1",
              "attributes":{
                 "report_email":"report@zerospam.ca",
                 "updated_at":"2017-06-16 18:41:10.552897 +00:00",
                 "created_at":"2017-06-16 18:41:10.552897 +00:00",
                 "synchronization_status": "done",
                 "synchronization":{  
                    "is_automatic":true,
                    "frequency":"every_two_hours"
                 },
                 "user_creation":{  
                    "is_create_users":false,
                    "created_at":null,
                    "has_digest":false,
                    "send_email_on_creation":false,
                    "is_ldap_auth_enabled": true
                 }
              }
           }
        }';

        $jsonRequest = '{
            "report_email":"report@zerospam.ca",
            "user_creation": {
                "is_create_users": false,
                "has_digest": false,
                "send_email_on_creation": false,
                "is_ldap_auth_enabled": true
            },
            "synchronization": {
                "is_automatic": true,
                "frequency": "every_two_hours"
            }
        }';

        $config  = $this->preSuccess($jsonResult);
        $request = new LdapConfigurationCreateRequest();

        $userCreation = new UserCreation(
            false,
            false,
            false,
            true
        );

        $synchronization = new Synchronization(
            true,
            LdapConfigSyncFrequencyEnum::EVERY_TWO_HOURS()
        );

        $request->setReportEmail('report@zerospam.ca')
                ->setUserCreation($userCreation)
                ->setSynchronization($synchronization)
                ->setOrganizationId(1);

        $client = new ProvulusClient($config);
        $client->processRequest($request);

        $this->validateRequest($config, $jsonRequest);
        $this->validateUrl($config, 'orgs/1/ldap');

        /** @var LdapConfigurationResponse $response */
        $response = $request->getResponse();
        $synchro = $response->synchronization;

        self::assertTrue($response->synchronization_status->is(LdapConfigurationStatusEnum::DONE()));
    }

    /**
     * @test
     **/
    public function delete_ldap_configuration_request_success()
    {
        $jsonResult  = [];
        $jsonRequest = [];

        $config = $this->preSuccess($jsonResult);
        $client = new ProvulusClient($config);

        $request = new LdapConfigurationDeleteRequest();
        $request->setOrganizationId(1);

        $client->processRequest($request);

        $this->validateRequest($config, $jsonRequest);
        $this->validateUrl($config, 'orgs/1/ldap');

        $response = $request->getResponse();

        self::assertInstanceOf(EmptyResponse::class, $response);
    }

    /**
     * @test
     **/
    public function read_ldap_configuration_request_success()
    {
        $requestBody = '{}';
        $responseBody = '{
           "data":{  
              "type":"ldap-config",
              "id":"565",
              "attributes":{  
                 "report_email":"report@zerospam.ca",
                 "updated_at":"2017-06-08 20:47:18.776088 +00:00",
                 "created_at":"2017-06-08 20:47:18.776088 +00:00",
                 "synchronization_status": "synchronizing",
                 "synchronization":{  
                    "is_automatic":true,
                    "frequency":"every_day"
                 },
                 "user_creation":{  
                    "is_enabled":true,
                    "created_at":"2017-06-08 20:47:18.775471 +00:00",
                    "has_digest":true,
                    "send_email_on_creation":true,
                    "is_ldap_auth_enabled": false
                 }
              }
           }
        }';

        $config = $this->preSuccess($responseBody);

        $request = new LdapConfigurationReadRequest();
        $request->setOrganizationId(565);

        $client = new ProvulusClient($config);
        $client->processRequest($request);

        $this->validateRequest($config, $requestBody);
        $this->validateUrl($config, 'orgs/565/ldap');

        $response = $request->getResponse();
        self::assertFalse($response->userCreation->isLdapAuthEnabled());

        self::assertTrue($response->synchronization_status->is(LdapConfigurationStatusEnum::SYNCHRONIZING()));
    }

    /**
     * @test
     **/
    public function update_ldap_configuration_request_success()
    {
        $requestBody = '{
            "report_email":"report@zerospam.ca",
            "user_creation": {
                "is_create_users": false,
                "has_digest": false,
                "send_email_on_creation": false
            },
            "synchronization": {
                "is_automatic": true,
                "frequency": "every_two_hours"
            }
        }';

        $responseBody = '{
           "data":{  
              "type":"ldap-config",
              "id":"121",
              "attributes":{  
                 "report_email":"report@zerospam.ca",
                 "created_at":"2017-06-16 18:41:11.000000 +00:00",
                 "updated_at":"2017-06-16 19:29:18.894863 +00:00",
                 "synchronization_status": "validate",
                 "synchronization":{  
                    "is_automatic":true,
                    "frequency":"every_two_hours"
                 },
                 "user_creation":{  
                    "is_create_users":false,
                    "created_at":null,
                    "has_digest":false,
                    "send_email_on_creation":false
                 }
              }
           }
        }';

        $config = $this->preSuccess($responseBody);

        $userCreation = new UserCreation(
            false,
            false,
            false
        );

        $synchronization = new Synchronization(
            true,
            LdapConfigSyncFrequencyEnum::EVERY_TWO_HOURS()
        );

        $request = new LdapConfigurationUpdateRequest();
        $request->setOrganizationId(121)
                ->setReportEmail('report@zerospam.ca')
                ->setUserCreation($userCreation)
                ->setSynchronization($synchronization);

        $client = new ProvulusClient($config);
        $client->processRequest($request);

        $this->validateRequest($config, $requestBody);
        $this->validateUrl($config, 'orgs/121/ldap');

        $response = $request->getResponse();

        self::assertContains('every_eight_hours', $response->synchronization->toArray());
        self::assertTrue($response->synchronization_status->is(LdapConfigurationStatusEnum::VALIDATE()));
    }

    /**
     * @test
     **/
    public function ldap_manual_synchronization_success()
    {
        $requestBody = [];

        $responseBody = [];

        $config = $this->preSuccess($responseBody);

        $request = new LdapConfigurationSynchronizeRequest();
        $request->setOrganizationId(567);

        $client = new ProvulusClient($config);
        $client->processRequest($request);

        $this->validateRequest($config, $requestBody);
        $this->validateUrl($config, 'orgs/567/ldap/synchronization/synchronize');

        $response = $request->getResponse();

        self::assertInstanceOf(EmptyResponse::class, $response);
    }
}