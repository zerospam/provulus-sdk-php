<?php
/**
 * Created by PhpStorm.
 * User: ycoutu
 * Date: 29/06/17
 * Time: 10:15 AM
 */

namespace automated\Requests\Ldap\Synchronization\User;

use ProvulusSDK\Client\ProvulusClient;
use ProvulusSDK\Client\Request\Ldap\Synchronization\User\LdapUserSyncIndexRequest;
use ProvulusSDK\Client\Response\Ldap\Synchronization\User\LdapUserSyncCollectionResponse;
use ProvulusSDK\Client\Response\Ldap\Synchronization\User\LdapUserSyncResponse;
use ProvulusSDK\Enum\Ldap\LdapDifferentialEnum;
use ProvulusSDK\Enum\Locale\LocaleEnum;
use ProvulusTest\TestCaseApiKey;

class LdapUserSyncTest extends TestCaseApiKey
{
    /** @test */
    public function index_ldap_user_sync_success()
    {
        $jsonResponse
                     = '{
            "data": [{
                "type": "ldap-user-sync",
                "id": "54697",
                "attributes": {
                    "ldap_connection_id": 444,
                    "dn_path": "mail=louise.lalonde@zerospam.ca,dc=example,dc=org",
                    "diff_code": "modified",
                    "main_address": "louise.lalonde@zerospam.ca",
                    "name": "Louise",
                    "surname": "Lalonde",
                    "sso_id": "louise",
                    "language": "es_es",
                    "is_declared_as_user": true
                },
                "relationships": {
                    "ldapAddressesSync": {
                        "data": [{
                            "type": "ldap-address-sync",
                            "id": "104436"
                        }]
                    }
                }
            }, {
                "type": "ldap-user-sync",
                "id": "54698",
                "attributes": {
                    "organization_id": 444,
                    "dn_path": "mail=philippe.poirier@zerospam.ca,dc=example,dc=org",
                    "diff_code": "modified",
                    "main_address": "philippe.poirier@zerospam.ca",
                    "name": "Philippe",
                    "surname": "Poirier",
                    "sso_id": "phil",
                    "language": "en_us",
                    "is_declared_as_user": true
                },
                "relationships": {
                    "ldapAddressesSync": {
                        "data": [{
                            "type": "ldap-address-sync",
                            "id": "104437"
                        },{
                            "type": "ldap-address-sync",
                            "id": "104438"
                        }]
                    }
                }
            }],
            "included": [{
                "type": "ldap-address-sync",
                "id": "104436",
                "attributes": {
                    "ldap_user_sync_id": 54697,
                    "address_type_code": "alias_address",
                    "address": "l.lalonde@zerospam.ca",
                    "diff_code": "deleted"
                }
            },{
                "type": "ldap-address-sync",
                "id": "104437",
                "attributes": {
                    "ldap_user_sync_id": 54698,
                    "address_type_code": "alias_address",
                    "address": "pp@zerospam.ca",
                    "diff_code": "added"
                }
            },{
                "type": "ldap-address-sync",
                "id": "104438",
                "attributes": {
                    "ldap_user_sync_id": 54698,
                    "address_type_code": "alias_address",
                    "address": "p.poirier@zerospam.ca",
                    "diff_code": "added"
                }
            }],
            "meta": {
                "pagination": {
                    "total": 2,
                    "count": 2,
                    "per_page": 15,
                    "current_page": 1,
                    "total_pages": 1
                }
            }
        }';
        $jsonRequest = '{}';

        $config = $this->preSuccess($jsonResponse);

        $request = new LdapUserSyncIndexRequest();
        $request->setOrganizationId(22)
                ->setDiffCode(null)
                ->setDiffCode(LdapDifferentialEnum::MODIFIED())
                ->withLdapAddressesSync();

        $client = new ProvulusClient($config);
        $client->processRequest($request);
        $this->validateRequest($config, $jsonRequest);

        /** @var LdapUserSyncCollectionResponse $collectionResponse */
        $collectionResponse = $request->getResponse();

        $this->validateRequestUrl($config,
            ['orgs/22/ldap/synchronization/details?', 'include=ldapAddressesSync', 'diff_code=modified']);

        /** @var LdapUserSyncResponse $ldapUserSync */
        $ldapUserSync = $collectionResponse->data()->first();

        $this->assertEquals('mail=louise.lalonde@zerospam.ca,dc=example,dc=org', $ldapUserSync->dn_path);
        $this->assertTrue($ldapUserSync->diff_code->is(LdapDifferentialEnum::MODIFIED()));
        $this->assertTrue($ldapUserSync->language->is(LocaleEnum::ES_ES()));
    }
}
