<?php
/**
 * Created by PhpStorm.
 * User: pbb
 * Date: 13/06/17
 * Time: 2:28 PM
 */

namespace automated\Requests\Ldap\Synchronization;

use ProvulusSDK\Client\ProvulusClient;
use ProvulusSDK\Client\Request\Ldap\Synchronization\LdapSynchronizationIndexRequest;
use ProvulusSDK\Client\Request\Ldap\Synchronization\User\LdapUserSyncIndexRequest;
use ProvulusSDK\Client\Request\Ldap\Synchronization\LdapSynchronizationAcceptRequest;
use ProvulusSDK\Client\Request\Ldap\Synchronization\LdapSynchronizationDenyRequest;
use ProvulusSDK\Client\Response\EmptyResponse;
use ProvulusSDK\Client\Response\Ldap\Synchronization\Address\LdapAddressSyncResponse;
use ProvulusSDK\Client\Response\Ldap\Synchronization\LdapSynchronizationCollectionResponse;
use ProvulusSDK\Client\Response\Ldap\Synchronization\LdapSynchronizationResponse;
use ProvulusSDK\Client\Response\Ldap\Synchronization\User\LdapUserSyncResponse;
use ProvulusSDK\Enum\Ldap\LdapSyncStatusEnum;
use ProvulusTest\TestCaseApiKey;

/**
 * Class LdapSynchronizationTest
 *
 * @package automated\Requests\LDAP\Synchronization
 */
class LdapSynchronizationTest extends TestCaseApiKey
{

    /**
     * @test
     **/
    public function ldap_user_synchronisation_with_address_sync_success()
    {
        $responseBody
            = '{  
   "data":[  
      {  
         "type":"ldap-user-sync",
         "id":"54605",
         "attributes":{  
            "ldap_connection_id": 65,
            "dn_path":"cn=super zerospam,dc=zerospam,dc=ca",
            "diff_code":"added",
            "main_address":"cumulus@zerospam.ca",
            "name":"cumlus",
            "surname":"zerospam",
            "sso_id":null,
            "is_declared_as_user":true
         },
         "relationships":{  
            "ldapAddressesSync":{  
               "data":[  
                  {  
                     "type":"ldap-address-sync",
                     "id":"104349"
                  }
               ]
            }
         }
      }
   ],
   "included":[  
      {  
         "type":"ldap-address-sync",
         "id":"104349",
         "attributes":{  
            "ldap_user_sync_id":54605,
            "address_type_code":"primary_address",
            "address":"cumulus@zerospam.ca",
            "diff_code":"added"
         }
      }
   ],
   "meta":{  
      "pagination":{  
         "total":1,
         "count":1,
         "per_page":15,
         "current_page":1,
         "total_pages":1
      }
   },
   "links":{  
      "self":"http:\/\/cumulus.local\/api\/v1\/orgs\/567\/ldap\/syncDetails?page=1",
      "first":"http:\/\/cumulus.local\/api\/v1\/orgs\/567\/ldap\/syncDetails?page=1",
      "last":"http:\/\/cumulus.local\/api\/v1\/orgs\/567\/ldap\/syncDetails?page=1"
   }
}';

        $requestBody = [];

        $config = $this->preSuccess($responseBody);

        $request = new LdapUserSyncIndexRequest();
        $request->setOrganizationId(1)
            ->withLdapAddressesSync();

        $client = new ProvulusClient($config);
        $client->processRequest($request);

        $this->validateRequest($config, $requestBody);
        $this->validateUrl($config, 'orgs/1/ldap/synchronization/details');

        $response = $request->getResponse();

        $response->data()->each(function ($user) {

            self::assertInstanceOf(LdapUserSyncResponse::class, $user);

            /** @var LdapUserSyncResponse $user */
            self::assertEquals('added', $user->diff_code->getValue());

            /** @var LdapUserSyncResponse $user */
            $addressesSyncs = $user->ldap_addresses_sync;

            $addressesSyncs->each(function ($address) {
                self::assertInstanceOf(LdapAddressSyncResponse::class, $address);
            });

            /** @var LdapAddressSyncResponse $address */
            $address = $addressesSyncs->first();
            self::assertEquals('primary_address', $address->address_type_code->getValue());
            self::assertEquals('added', $address->diff_code->getValue());
        });
    }

    /**
     * @test
     **/
    public function manual_synchronization_accept_success()
    {
        $requestBody = [];

        $responseBody = [];

        $config = $this->preSuccess($responseBody);
        $request = new LdapSynchronizationAcceptRequest();
        $request->setOrganizationId(545);

        $client = new ProvulusClient($config);
        $client->processRequest($request);

        $this->validateRequest($config, $requestBody);
        $this->validateUrl($config, 'orgs/545/ldap/synchronization/accept');

        $response = $request->getResponse();
        self::assertInstanceOf(EmptyResponse::class, $response);
    }

    /**
     * @test
     **/
    public function manual_synchronization_deny_success()
    {
        $requestBody = [];

        $responseBody = [];

        $config = $this->preSuccess($responseBody);
        $request = new LdapSynchronizationDenyRequest();
        $request->setOrganizationId(656);

        $client = new ProvulusClient($config);
        $client->processRequest($request);

        $this->validateRequest($config, $requestBody);
        $this->validateUrl($config, 'orgs/656/ldap/synchronization/deny');

        $response = $request->getResponse();
        self::assertInstanceOf(EmptyResponse::class, $response);

    }

    /**
     * @test
     **/
    public function index_synchronization_request_success()
    {
        $requestBody = [];
        $responseBody
            = '
        {  
           "data":[  
              {  
                 "type":"ldap-sync",
                 "id":"26696",
                 "attributes":{  
                    "ldap_connection_id":567,
                    "nb_new_entries":0,
                    "nb_modified_entries":0,
                    "nb_deleted_entries":0,
                    "nb_entries_total":0,
                    "sync_status":"canceled",
                    "sync_at":"2017-06-22 18:11:28.105412 +00:00",
                    "ldap_config_id":142
                 }
              }
           ],
           "meta":{  
              "pagination":{  
                 "total":1,
                 "count":1,
                 "per_page":15,
                 "current_page":1,
                 "total_pages":1
              }
           },
           "links":{  
              "self":"http://cumulus.local/api/v1/orgs/567/ldapsyncs?page=1",
              "first":"http://cumulus.local/api/v1/orgs/567/ldapsyncs?page=1",
              "last":"http://cumulus.local/api/v1/orgs/567/ldapsyncs?page=1"
           }
        }';

        $config = $this->preSuccess($responseBody);
        $request = new LdapSynchronizationIndexRequest();
        $request->setOrganizationId(1234)
            ->setLdapConnectionId(567);

        $client = new ProvulusClient($config);
        $client->processRequest($request);

        $this->validateRequest($config, $requestBody);
        $this->validateUrl($config, 'orgs/1234/ldap/connections/567/synchronizations');

        $response = $request->getResponse();
        self::assertInstanceOf(LdapSynchronizationCollectionResponse::class, $response);
    }


    /**
     * @test
     **/
    public function indexLdapSynchronizationFilteredByStatus()
    {
        $requestBody = [];
        $responseBody
            = '{  
    "data":[  
      {  
         "type":"ldap-sync",
         "id":"14052",
         "attributes":{  
            "ldap_connection_id":155,
            "nb_new_entries":1,
            "nb_modified_entries":0,
            "nb_deleted_entries":2,
            "nb_entries_total":98,
            "sync_status":"awaiting_confirmation",
            "sync_at":"2016-07-13 13:17:13.192314 +00:00",
            "ldap_config_id":16
         }
      }
   ],
   "meta":{  
      "pagination":{  
         "total":1,
         "count":1,
         "per_page":15,
         "current_page":1,
         "total_pages":1
      }
   },
   "links":{  
      "self":"http:\/\/cumulus.local\/api\/v1\/orgs\/2220\/ldapsyncs?page=1",
      "first":"http:\/\/cumulus.local\/api\/v1\/orgs\/2220\/ldapsyncs?page=1",
      "last":"http:\/\/cumulus.local\/api\/v1\/orgs\/2220\/ldapsyncs?page=1"
   }
}';

        $config = $this->preSuccess($responseBody);

        $request = new LdapSynchronizationIndexRequest();
        $request->setOrganizationId(767)
            ->setLdapConnectionId(155)
            ->filterByStatuses([
                LdapSyncStatusEnum::REQ(),
                LdapSyncStatusEnum::AWAITING_CONFIRMATION(),
            ]);

        $client = new ProvulusClient($config);
        $client->processRequest($request);

        $this->validateRequest($config, $requestBody);
        $this->validateUrl($config, 'orgs/767/ldap/connections/155/synchronizations?status=req;awaiting_confirmation');

        $response = $request->getResponse();

        $response->data()->each(function (LdapSynchronizationResponse $sync) {
            self::assertTrue(
                $sync->sync_status->is(LdapSyncStatusEnum::AWAITING_CONFIRMATION())
                || $sync->sync_status->is(LdapSyncStatusEnum::REQ())
            );
        });

    }
}