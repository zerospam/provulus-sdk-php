<?php
/**
 * Created by PhpStorm.
 * User: aaflalo
 * Date: 16/12/16
 * Time: 2:16 PM
 */

namespace automated\Requests\Resource;


use ProvulusSDK\Client\Request\Resource\User\TypedUsers\UserRequestType;
use ProvulusTest\Request\Contact\ContactTestRequest;
use ProvulusTest\Request\Domain\DomainTestRequest;
use ProvulusTest\Request\User\UserTestRequest;
use ProvulusTest\TestCase;

class ResourceTest extends TestCase
{

    /**
     * @test
     */
    public function user_request() {

        $test = new UserTestRequest();
        $test->setOrganizationId(4)->setUserId(5)->setUserRequestType(UserRequestType::ADMIN());

        $this->assertEquals('orgs/4/admins/5', $test->routeUrl());
    }

    /**
     * @test
     * @expectedException \InvalidArgumentException
     */
    public function user_request_no_type() {

        $test = new UserTestRequest();
        $test->setOrganizationId(4)->setUserId(5);
        return $test->routeUrl();
    }

    /**
     * @test
     */
    public function domain_request() {

        $test = new DomainTestRequest();
        $test->setOrganizationId(4)->setDomainId(10);

        $this->assertEquals('orgs/4/domains/10', $test->routeUrl());
    }



    /**
     * @test
     */
    public function contact_request() {

        $test = new ContactTestRequest();
        $test->setOrganizationId(4)->setContactPersonId(110);

        $this->assertEquals('orgs/4/contacts/110', $test->routeUrl());
    }

}