<?php
/**
 * Created by PhpStorm.
 * User: pbb
 * Date: 27/01/17
 * Time: 3:12 PM
 */

namespace automated\Requests\Resource\Domain\Filtering\Item;

use GuzzleHttp\Handler\MockHandler;
use ProvulusSDK\Client\ProvulusClient;
use ProvulusSDK\Client\Request\Domain\Filtering\Item\ReadFilteringPolicyItemRequest;
use ProvulusSDK\Client\Response\Domain\Filtering\Item\FilteringPolicyItemResponse;
use ProvulusTest\TestCase;

/**
 * Class FilteringPolicyItemRequestTest
 *
 * Tests for FilteringPolicyItemRequest Resource
 *
 * @package automated\Requests\Resource\Domain
 */
class FilteringPolicyItemRequestTest extends TestCase
{

    /**
     * @test
     * @expectedException \Exception
     * @expectedExceptionMessage request Type
     **/
    public function failure_type_not_set()
    {
        $config = $this->getConfig(new MockHandler())->setApiKey('test');
        $request = new ReadFilteringPolicyItemRequest();
        $request->setOrganizationId(2865)
                ->setFilteringId(3122)
                ->setFilterItemId(403);

        $client = new ProvulusClient($config);

        /**
         * @var $response FilteringPolicyItemResponse
         */
        $client->processRequest($request);
    }
}