<?php
/**
 * Created by PhpStorm.
 * User: ycoutu
 * Date: 28/02/19
 * Time: 2:14 PM
 */

namespace automated\Requests\Domain\Mailbox;

use Carbon\Carbon;
use ProvulusSDK\Client\ProvulusClient;
use ProvulusSDK\Client\Request\Domain\Mailbox\IndexMailboxRequest;
use ProvulusSDK\Client\Request\Domain\Mailbox\ReadMailboxRequest;
use ProvulusSDK\Client\Response\Domain\Mailbox\MailboxCollectionResponse;
use ProvulusSDK\Client\Response\Domain\Mailbox\MailboxResponse;
use ProvulusTest\TestCaseApiKey;

class MailboxTest extends TestCaseApiKey
{
    /** @test */
    public function index_mailbox_test()
    {
        $jsonResponse = <<<JSON
{
  "data": [
    {
      "type": "mailbox",
      "id": "214414409",
      "attributes": {
        "domain_id": 7676,
        "email_address": "000@zerospam.ca",
        "message_count": 1811,
        "last_message_date": "2013-07-23 18:36:57.562562 +00:00",
        "last_queue_tag": "fake/WCKQHVoADkIc8X1",
        "last_log_entry": "fake postfix/smtp[1365]: WCKQHVoADkIc8X1: to=<000@zerospam.ca>, relay=server.com[8.8.8.8]:25, delay=0.3, delays=0.01/0/0.16/0.13, dsn=2.0.0, status=sent (250 2.0.0 Ok: queued as E5C82D4264A)",
        "version": 0,
        "created_at": "2013-07-23 18:36:57.562562 +00:00",
        "updated_at": null
      }
    },
    {
      "type": "mailbox",
      "id": "846961896",
      "attributes": {
        "domain_id": 7676,
        "email_address": "jd@zerospam.ca",
        "message_count": 424,
        "last_message_date": "2017-12-08 11:22:17.077157 +00:00",
        "last_queue_tag": "fake/LyfoR8H9d08n6ed",
        "last_log_entry": "fake postfix/smtp[32215]: LyfoR8H9d08n6ed: to=<aa@zerospam.ca>, relay=server.com[8.8.8.8]:25, delay=0.17, delays=0.05/0/0.1/0.02, dsn=2.0.0, status=sent (250 2.0.0 Ok: queued as 4599DB2073C)",
        "version": 0,
        "created_at": "2017-12-08 11:22:17.077157 +00:00",
        "updated_at": null
      }
    },
    {
      "type": "mailbox",
      "id": "260887899",
      "attributes": {
        "domain_id": 7676,
        "email_address": "my-hammy@zerospam.ca",
        "message_count": 254304,
        "last_message_date": "2013-09-04 06:17:08.983003 +00:00",
        "last_queue_tag": "fake/oIw9w9FgYuGnRtR",
        "last_log_entry": "fake postfix/smtp[6603]: oIw9w9FgYuGnRtR: to=<my-hammy@zerospam.ca>, relay=server.com[8.8.8.8]:25, delay=0.35, delays=0.03/0/0.18/0.14, dsn=2.0.0, status=sent (250 2.0.0 Ok: queued as 2556A5B0E03)",
        "version": 0,
        "created_at": "2013-09-04 06:17:08.983003 +00:00",
        "updated_at": null
      }
    },
    {
      "type": "mailbox",
      "id": "567957659",
      "attributes": {
        "domain_id": 7676,
        "email_address": "buy-stuff@zerospam.ca",
        "message_count": 221,
        "last_message_date": "2013-12-15 01:58:21.339434 +00:00",
        "last_queue_tag": "fake/Xeo5uTRyiQxpqta",
        "last_log_entry": "fake postfix/smtp[23160]: Xeo5uTRyiQxpqta: to=<buy-stuff@zerospam.ca>, relay=server.com[8.8.8.8]:25, delay=0.17, delays=0.05/0/0.11/0.02, dsn=2.0.0, status=sent (250 2.0.0 Ok: queued as D3AA2CF5816)",
        "version": 0,
        "created_at": "2013-12-15 01:58:21.339434 +00:00",
        "updated_at": null
      }
    },
    {
      "type": "mailbox",
      "id": "101518058",
      "attributes": {
        "domain_id": 7676,
        "email_address": "john@zerospam.ca",
        "message_count": 10,
        "last_message_date": "2015-11-03 19:36:45.947819 +00:00",
        "last_queue_tag": "fake/4ejZuYEtA2adnSP",
        "last_log_entry": "fake postfix/smtp[17811]: 4ejZuYEtA2adnSP: to=<john@zerospam.ca>, relay=server.com[8.8.8.8]:25, delay=0.13, delays=0.01/0/0.1/0.01, dsn=2.0.0, status=sent (250 2.0.0 Ok: queued as D5936718887)",
        "version": 0,
        "created_at": "2015-11-03 19:36:45.947819 +00:00",
        "updated_at": null
      }
    },
    {
      "type": "mailbox",
      "id": "834386928",
      "attributes": {
        "domain_id": 7676,
        "email_address": "john.doe@zerospam.ca",
        "message_count": 8730,
        "last_message_date": "2015-04-24 21:28:06.083859 +00:00",
        "last_queue_tag": "fake/uKgqnMsOHpXPfN4",
        "last_log_entry": "fake postfix/smtp[17006]: uKgqnMsOHpXPfN4: to=<john.doe@zerospam.ca>, relay=server.com[8.8.8.8]:25, delay=0.25, delays=0.05/0/0.16/0.04, dsn=2.0.0, status=sent (250 2.0.0 Ok: queued as BC06D7B6B81)",
        "version": 0,
        "created_at": "2015-04-24 21:28:06.083859 +00:00",
        "updated_at": null
      }
    },
    {
      "type": "mailbox",
      "id": "90163472",
      "attributes": {
        "domain_id": 7676,
        "email_address": "apple@zerospam.ca",
        "message_count": 49657,
        "last_message_date": "2018-02-14 16:11:16.241603 +00:00",
        "last_queue_tag": "fake/wEwlCOZL65Zxv5w",
        "last_log_entry": "fake postfix/smtp[9580]: wEwlCOZL65Zxv5w: to=<apple@zerospam.ca>, relay=server.com[8.8.8.8]:25, delay=0.21, delays=0/0/0.16/0.04, dsn=2.0.0, status=sent (250 2.0.0 Ok: queued as 43BFA35E1DE)",
        "version": 0,
        "created_at": "2018-02-14 16:11:16.241603 +00:00",
        "updated_at": null
      }
    },
    {
      "type": "mailbox",
      "id": "143853481",
      "attributes": {
        "domain_id": 7676,
        "email_address": "steve.smith@zerospam.ca",
        "message_count": 30,
        "last_message_date": "2017-10-12 18:57:38.655983 +00:00",
        "last_queue_tag": "fake/fURz1crB3jTk74O",
        "last_log_entry": "fake postfix/smtp[17811]: fURz1crB3jTk74O: to=<steve.smith@zerospam.ca>, relay=server.com[8.8.8.8]:25, delay=0.11, delays=0.01/0/0.09/0.01, dsn=2.0.0, status=sent (250 2.0.0 Ok: queued as 4951CC4A9FE)",
        "version": 0,
        "created_at": "2017-10-12 18:57:38.655983 +00:00",
        "updated_at": null
      }
    },
    {
      "type": "mailbox",
      "id": "419474445",
      "attributes": {
        "domain_id": 7676,
        "email_address": "pug@zerospam.ca",
        "message_count": 62,
        "last_message_date": "2015-09-18 12:38:28.929196 +00:00",
        "last_queue_tag": "fake/XRkbMQOGZIoN3DU",
        "last_log_entry": "fake postfix/smtp[15909]: XRkbMQOGZIoN3DU: to=<pug@zerospam.ca>, relay=server.com[8.8.8.8]:25, delay=0.16, delays=0.04/0/0.11/0.01, dsn=2.0.0, status=sent (250 2.0.0 Ok: queued as 9C05F58706B)",
        "version": 0,
        "created_at": "2015-09-18 12:38:28.929196 +00:00",
        "updated_at": null
      }
    },
    {
      "type": "mailbox",
      "id": "229306508",
      "attributes": {
        "domain_id": 7676,
        "email_address": "rob.moore@zerospam.ca",
        "message_count": 2541,
        "last_message_date": "2013-09-18 07:53:14.425942 +00:00",
        "last_queue_tag": "fake/RE9xUvzVaEaZ7LZ",
        "last_log_entry": "fake postfix/smtp[29194]: RE9xUvzVaEaZ7LZ: to=<rob.moore@zerospam.ca>, relay=server.com[8.8.8.8]:25, delay=0.55, delays=0.04/0/0.38/0.13, dsn=2.0.0, status=sent (250 2.0.0 Ok: queued as A537C39B27C)",
        "version": 0,
        "created_at": "2013-09-18 07:53:14.425942 +00:00",
        "updated_at": null
      }
    },
    {
      "type": "mailbox",
      "id": "430221931",
      "attributes": {
        "domain_id": 7676,
        "email_address": "nope@zerospam.ca",
        "message_count": 284,
        "last_message_date": "2016-10-28 16:21:05.137515 +00:00",
        "last_queue_tag": "fake/Otl2OJlFukE8WRZ",
        "last_log_entry": "fake postfix/smtp[32617]: Otl2OJlFukE8WRZ: to=<nope@zerospam.ca>, relay=server.com[8.8.8.8]:25, delay=0.23, delays=0.01/0/0.18/0.04, dsn=2.0.0, status=sent (250 2.0.0 Ok: queued as BCA486963F3)",
        "version": 0,
        "created_at": "2016-10-28 16:21:05.137515 +00:00",
        "updated_at": null
      }
    },
    {
      "type": "mailbox",
      "id": "535192236",
      "attributes": {
        "domain_id": 7676,
        "email_address": "email@zerospam.ca",
        "message_count": 724,
        "last_message_date": "2010-08-26 14:37:52.253575 +00:00",
        "last_queue_tag": "fake/v4T814pMWsPomhC",
        "last_log_entry": "fake postfix/smtp[28817]: v4T814pMWsPomhC: to=<email@zerospam.ca>, relay=server.com[8.8.8.8]:25, delay=0.22, delays=0.05/0/0.16/0.02, dsn=2.0.0, status=sent (250 2.0.0 Ok: queued as C383AD5D2C2)",
        "version": 0,
        "created_at": "2010-08-26 14:37:52.253575 +00:00",
        "updated_at": null
      }
    },
    {
      "type": "mailbox",
      "id": "945501417",
      "attributes": {
        "domain_id": 7676,
        "email_address": "biz@zerospam.ca",
        "message_count": 32,
        "last_message_date": "2011-02-20 11:22:20.233683 +00:00",
        "last_queue_tag": "fake/mp2B4B4AZUnvfpL",
        "last_log_entry": "fake postfix/smtp[10051]: mp2B4B4AZUnvfpL: to=<biz@zerospam.ca>, relay=server.com[8.8.8.8]:25, delay=0.13, delays=0.01/0/0.1/0.01, dsn=2.0.0, status=sent (250 2.0.0 Ok: queued as 58EBC457AA9)",
        "version": 0,
        "created_at": "2011-02-20 11:22:20.233683 +00:00",
        "updated_at": null
      }
    },
    {
      "type": "mailbox",
      "id": "589198952",
      "attributes": {
        "domain_id": 7676,
        "email_address": "com@zerospam.ca",
        "message_count": 10702,
        "last_message_date": "2011-11-13 12:11:45.775852 +00:00",
        "last_queue_tag": "fake/tukfKA57d89hzaO",
        "last_log_entry": "fake postfix/smtp[24040]: tukfKA57d89hzaO: to=<com@zerospam.ca>, relay=server.com[8.8.8.8]:25, delay=0.24, delays=0.01/0/0.18/0.04, dsn=2.0.0, status=sent (250 2.0.0 Ok: queued as C95DFBEDDD2)",
        "version": 0,
        "created_at": "2011-11-13 12:11:45.775852 +00:00",
        "updated_at": null
      }
    },
    {
      "type": "mailbox",
      "id": "881385655",
      "attributes": {
        "domain_id": 7676,
        "email_address": "number@zerospam.ca",
        "message_count": 13458,
        "last_message_date": "2012-11-13 16:38:25.469266 +00:00",
        "last_queue_tag": "fake/ngU98DQYPBI5m3F",
        "last_log_entry": "fake postfix/smtp[1210]: ngU98DQYPBI5m3F: to=<number@zerospam.ca>, relay=server.com[8.8.8.8]:25, delay=0.27, delays=0.05/0/0.17/0.05, dsn=2.0.0, status=sent (250 2.0.0 Ok: queued as 6CA18B91DAA)",
        "version": 0,
        "created_at": "2012-11-13 16:38:25.469266 +00:00",
        "updated_at": null
      }
    }
  ],
  "meta": {
    "pagination": {
      "total": 99,
      "count": 15,
      "per_page": 15,
      "current_page": 1,
      "total_pages": 7
    }
  }
}
JSON;
        $jsonRequest = '{}';

        $config = $this->preSuccess($jsonResponse);

        $request = new IndexMailboxRequest();
        $request->setOrganizationId(1212)
            ->setDomainId(7676)
            ->setEmailAddress('email')
            ->setMinimumMessageCount(10)
            ->setMaximumMessageCount(100)
            ->setStartLastMessageDate(Carbon::parse('2018-01-01'));

        $client = new ProvulusClient($config);

        /** @var $response MailboxCollectionResponse */
        $response = $client->processRequest($request);

        $this->validateRequest($config, $jsonRequest);
        $this->validateRequestUrl($config, ['/orgs/1212/domains/7676']);
        $this->validateRequestUrl($config, ['email_address=email']);
        $this->validateRequestUrl($config, ['min_msg_count=10']);
        $this->validateRequestUrl($config, ['max_msg_count=100']);
        $this->validateRequestUrl($config, ['start_last_message=2018-01-01']);

        /** @var MailboxResponse $mailboxResponse */
        $mailboxResponse = $response->data()->first();

        self::assertEquals("214414409", $mailboxResponse->id());
        self::assertEquals(7676, $mailboxResponse->domain_id);
        self::assertEquals("000@zerospam.ca", $mailboxResponse->email_address);
        self::assertEquals(1811, $mailboxResponse->message_count);
        self::assertEquals("fake/WCKQHVoADkIc8X1", $mailboxResponse->last_queue_tag);
        self::assertEquals(
            "fake postfix/smtp[1365]: WCKQHVoADkIc8X1: to=<000@zerospam.ca>, relay=server.com[8.8.8.8]:25, delay=0.3, delays=0.01/0/0.16/0.13, dsn=2.0.0, status=sent (250 2.0.0 Ok: queued as E5C82D4264A)",
            $mailboxResponse->last_log_entry
        );
        self::assertEquals(0, $mailboxResponse->version);
        self::assertNull($mailboxResponse->updated_at);

        self::assertEquals('2013-07-23', $mailboxResponse->created_at->toDateString());
        self::assertEquals('2013-07-23', $mailboxResponse->last_message_date->toDateString());

        $this->assertEquals(99, $response->getMetaData()->total);
        $this->assertEquals(15, $response->getMetaData()->count);
        $this->assertEquals(15, $response->getMetaData()->perPage);
        $this->assertEquals(1, $response->getMetaData()->currentPage);
        $this->assertEquals(7, $response->getMetaData()->totalPages);
    }

    /** @test */
    public function read_mailbox_test()
    {
        $jsonResponse = <<<JSON
{
    "data": {
        "type": "mailbox",
        "id": "789",
        "attributes": {
            "domain_id": 456,
            "email_address": "000@zerospam.ca",
            "message_count": 1811,
            "last_message_date": "2013-07-23 18:36:57.562562 +00:00",
            "last_queue_tag": "fake/WCKQHVoADkIc8X1",
            "last_log_entry": "fake postfix/smtp[1365]: WCKQHVoADkIc8X1: to=<000@zerospam.ca>, relay=server.com[8.8.8.8]:25, delay=0.3, delays=0.01/0/0.16/0.13, dsn=2.0.0, status=sent (250 2.0.0 Ok: queued as E5C82D4264A)",
            "version": 0,
            "created_at": "2013-07-23 18:36:57.562562 +00:00",
            "updated_at": null
        }
    }
}
JSON;
        $jsonRequest = '{}';

        $config = $this->preSuccess($jsonResponse);

        $request = new ReadMailboxRequest();
        $request->setOrganizationId(123)
            ->setDomainId(456)
            ->setMailboxId(789);

        $client = new ProvulusClient($config);

        /** @var $response MailboxResponse */
        $response = $client->processRequest($request);

        $this->validateRequest($config, $jsonRequest);
        $this->validateRequestUrl($config, ['/orgs/123/domains/456/mailboxes/789']);

        self::assertEquals("789", $response->id());
        self::assertEquals(456, $response->domain_id);
        self::assertEquals("000@zerospam.ca", $response->email_address);
        self::assertEquals(1811, $response->message_count);
        self::assertEquals("fake/WCKQHVoADkIc8X1", $response->last_queue_tag);
        self::assertEquals(
            "fake postfix/smtp[1365]: WCKQHVoADkIc8X1: to=<000@zerospam.ca>, relay=server.com[8.8.8.8]:25, delay=0.3, delays=0.01/0/0.16/0.13, dsn=2.0.0, status=sent (250 2.0.0 Ok: queued as E5C82D4264A)",
            $response->last_log_entry
        );
        self::assertEquals(0, $response->version);
        self::assertNull($response->updated_at);

        self::assertEquals('2013-07-23', $response->created_at->toDateString());
        self::assertEquals('2013-07-23', $response->last_message_date->toDateString());
    }
}
