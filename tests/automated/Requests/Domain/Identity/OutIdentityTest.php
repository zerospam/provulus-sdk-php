<?php
/**
 * Created by PhpStorm.
 * User: ycoutu
 * Date: 27/10/17
 * Time: 3:47 PM
 */

namespace automated\Requests\Domain\Identity;

use Cake\Collection\Collection;
use ProvulusSDK\Client\ProvulusClient;
use ProvulusSDK\Client\Request\Domain\Identity\BulkDeleteOutIdentityRequest;
use ProvulusSDK\Client\Request\Domain\Identity\CreateOutIdentityRequest;
use ProvulusSDK\Client\Request\Domain\Identity\DeleteOutIdentityRequest;
use ProvulusSDK\Client\Request\Domain\Identity\IndexOutIdentityRequest;
use ProvulusSDK\Client\Request\Domain\Identity\ReadOutIdentityRequest;
use ProvulusSDK\Client\Request\Domain\Identity\Type\ResetPasswordOutIdentityRequest;
use ProvulusSDK\Client\Request\Domain\Identity\Type\UpdateOutIdentityIpRequest;
use ProvulusSDK\Client\Request\Domain\Identity\UpdateOutIdentityRequest;
use ProvulusSDK\Client\Response\Domain\Identity\OutIdentityCollectionResponse;
use ProvulusSDK\Client\Response\Domain\Identity\OutIdentityResponse;
use ProvulusSDK\Client\Response\Domain\Identity\Type\OutIdentityIpResponse;
use ProvulusSDK\Client\Response\Domain\Identity\Type\OutIdentityLoginResponse;
use ProvulusSDK\Client\Response\Domain\Identity\Type\OutIdentitySharedIpResponse;
use ProvulusSDK\Enum\Domain\Identity\IdentityAuthenticationTypeEnum;
use ProvulusTest\TestCaseApiKey;

class OutIdentityTest extends TestCaseApiKey
{
    /** @test */
    public function out_identity_index_success()
    {
        $jsonResult = '
        {
            "data": [{
                "type": "out-identity",
                "id": "526",
                "attributes": {
                    "organization_id": 2016,
                    "name": "WithIp",
                    "forced_dg_id": 3727,
                    "authentication_type_code": "identity_ip",
                    "allow_extras": true
                },
                "relationships": {
                    "details": {
                        "data": [{
                            "type": "out-identity-ip",
                            "id": "757"
                        },{
                            "type": "out-identity-ip",
                            "id": "758"
                        },{
                            "type": "out-identity-ip",
                            "id": "759"
                        }]
                    }
                }
            }, {
                "type": "out-identity",
                "id": "527",
                "attributes": {
                    "organization_id": 2016,
                    "name": "Sharing",
                    "forced_dg_id": 1719,
                    "authentication_type_code": "identity_shared_ip",
                    "allow_extras": false
                },
                "relationships": {
                    "details": {
                        "data": {
                            "type": "out-identity-shared-ip",
                            "id": "5"
                        }
                    }
                }
            }, {
                "type": "out-identity",
                "id": "524",
                "attributes": {
                    "organization_id": 2016,
                    "name": "Authentication",
                    "forced_dg_id": null,
                    "authentication_type_code": "identity_login",
                    "allow_extras": true
                },
                "relationships": {
                    "details": {
                        "data": {
                            "type": "out-identity-login",
                            "id": "91"
                        }
                    }
                }
            }],
            "included": [{
                "type": "out-identity-ip",
                "id": "757",
                "attributes": {
                    "value": "6.6.6.6/32",
                    "out_identity_id": 526,
                    "outbound_ip_id": 614
                }
            }, {
                "type": "out-identity-ip",
                "id": "758",
                "attributes": {
                    "value": "4.9.0.0/16",
                    "out_identity_id": 526,
                    "outbound_ip_id": 615
                }
            }, {
                "type": "out-identity-ip",
                "id": "759",
                "attributes": {
                    "value": "36.49.64.0/20",
                    "out_identity_id": 526,
                    "outbound_ip_id": 616
                }
            }, {
                "type": "out-identity-shared-ip",
                "id": "5",
                "attributes": {
                    "out_identity_id": 527,
                    "reseller_id": 2016,
                    "created_at": "2017-10-27 20:28:15.000000 +00:00",
                    "updated_at": "2017-10-27 20:28:15.000000 +00:00",
                    "reseller_name": "Corporate Inc.",
                    "ips": [
                        "34.56.78.90/32",
                        "37.73.0.0/16"
                    ]
                }
            }, {
                "type": "out-identity-login",
                "id": "91",
                "attributes": {
                    "username": "410be3e8-e787-4554-804e-ced93f368055",
                    "out_identity_id": 524
                }
            }],
            "meta": {
                "pagination": {
                    "total": 3,
                    "count": 3,
                    "per_page": 15,
                    "current_page": 1,
                    "total_pages": 1
                }
            }
        }';

        $jsonRequest = '{}';

        $config = $this->preSuccess($jsonResult);

        $request = new IndexOutIdentityRequest();
        $request->setOrganizationId('2016');

        $client = new ProvulusClient($config);

        /**
         * @var $response OutIdentityCollectionResponse
         */
        $response = $client->processRequest($request);

        $this->validateRequest($config, $jsonRequest);
        $this->validateRequestUrl($config, ['orgs/2016/identities']);

        self::assertCount(3, $response->data()->toArray());

        list($ipResponse, $sharedIpResponse, $loginResponse) = $response->data()->toArray();

        // Tons of check to make sure that every authentication type is taken care of
        self::assertTrue($ipResponse->details instanceof  Collection);
        self::assertTrue($ipResponse->details->first() instanceof OutIdentityIpResponse);
        self::assertCount(3, $ipResponse->details->toArray());
        self::assertEquals('6.6.6.6/32', $ipResponse->details->first()->value);
        self::assertTrue($ipResponse->authentication_type_code->is(IdentityAuthenticationTypeEnum::IDENTITY_IP()));
        self::assertEquals('WithIp', $ipResponse->name);
        self::assertEquals(3727, $ipResponse->forced_dg_id);
        self::assertTrue($ipResponse->allow_extras);

        self::assertTrue($sharedIpResponse->details instanceof OutIdentitySharedIpResponse);
        self::assertCount(2, $sharedIpResponse->details->ips);
        self::assertEquals('34.56.78.90/32', $sharedIpResponse->details->ips[0]);
        self::assertEquals('Corporate Inc.', $sharedIpResponse->details->reseller_name);
        self::assertTrue($sharedIpResponse->authentication_type_code->is(IdentityAuthenticationTypeEnum::IDENTITY_SHARED_IP()));
        self::assertEquals('Sharing', $sharedIpResponse->name);
        self::assertEquals(1719, $sharedIpResponse->forced_dg_id);
        self::assertFalse($sharedIpResponse->allow_extras);

        self::assertTrue($loginResponse->details instanceof OutIdentityLoginResponse);
        self::assertEquals('410be3e8-e787-4554-804e-ced93f368055', $loginResponse->details->username);
        self::assertTrue($loginResponse->authentication_type_code->is(IdentityAuthenticationTypeEnum::IDENTITY_LOGIN()));
        self::assertEquals('Authentication', $loginResponse->name);
        self::assertNull($loginResponse->forced_dg_id);
        self::assertTrue($loginResponse->allow_extras);
    }

    /** @test */
    public function out_identity_read_success()
    {
        $jsonResult = '{
        "data": {
            "type": "out-identity",
            "id": "420",
            "attributes": {
                "organization_id": 42,
                "name": "Identity",
                "forced_dg_id": null,
                "authentication_type_code": "identity_ip",
                "allow_extras": false
            },
            "relationships": {
                "details": {
                    "data": [{
                        "type": "out-identity-ip",
                        "id": "595"
                    },{
                        "type": "out-identity-ip",
                        "id": "598"
                    },{
                        "type": "out-identity-ip",
                        "id": "800"
                    }]
                }
            }
        },
        "included": [{
            "type": "out-identity-ip",
            "id": "595",
            "attributes": {
                "value": "9.9.9.9/32",
                "out_identity_id": 420,
                "outbound_ip_id": 600
            }
        },{
            "type": "out-identity-ip",
            "id": "598",
            "attributes": {
                "value": "44.55.0.0/16",
                "out_identity_id": 420,
                "outbound_ip_id": 700
            }
        },{
            "type": "out-identity-ip",
            "id": "800",
            "attributes": {
                "value": "36.49.64.0/24",
                "out_identity_id": 420,
                "outbound_ip_id": 900
            }
        }]
    }';

        $jsonRequest = '{}';

        $config = $this->preSuccess($jsonResult);

        $request = new ReadOutIdentityRequest();
        $request->setOrganizationId(42)
                ->setIdentityId(420);

        $client = new ProvulusClient($config);

        /**
         * @var $response OutIdentityResponse
         */
        $response = $client->processRequest($request);

        $this->validateRequest($config, $jsonRequest);
        $this->validateRequestUrl($config, ['orgs/42/identities/420']);

        self::assertTrue($response->details instanceof  Collection);
        self::assertTrue($response->details->first() instanceof OutIdentityIpResponse);
        self::assertCount(3, $response->details->toArray());
        self::assertEquals('9.9.9.9/32', $response->details->first()->value);
        self::assertTrue($response->authentication_type_code->is(IdentityAuthenticationTypeEnum::IDENTITY_IP()));
        self::assertEquals('Identity', $response->name);
        self::assertNull($response->forced_dg_id);
        self::assertFalse($response->allow_extras);
    }

    /** @test */
    public function out_identity_create_success()
    {
        $jsonResult = '{
        "data": {
            "type": "out-identity",
            "id": "1234",
            "attributes": {
                "organization_id": 900,
                "name": "New login",
                "forced_dg_id": 443,
                "authentication_type_code": "identity_login",
                "allow_extras": true
            },
            "relationships": {
                "details": {
                    "data": {
                        "type": "out-identity-login",
                        "id": "91"
                    }
                }
            }
        },
        "included": [{
            "type": "out-identity-login",
            "id": "91",
            "attributes": {
                "username": "5f8174f9-0db5-4a94-93b0-4c418c3ddaad",
                "out_identity_id": 1234
            }
        }]
    }';

        $jsonRequest = '{
            "name": "New login",
            "forced_dg_id": 443,
            "authentication_type_code": "identity_login",
            "allow_extras": true
        }';

        $config = $this->preSuccess($jsonResult);

        $request = new CreateOutIdentityRequest();
        $request->setOrganizationId(900)
            ->setName("New login")
            ->setForcedDgId(443)
            ->setAuthenticationTypeCode(IdentityAuthenticationTypeEnum::IDENTITY_LOGIN())
            ->setAllowExtras(true);

        $client = new ProvulusClient($config);

        /**
         * @var $response OutIdentityResponse
         */
        $response = $client->processRequest($request);

        $this->validateRequest($config, $jsonRequest);
        $this->validateRequestUrl($config, ['orgs/900/identities']);

        self::assertTrue($response->details instanceof OutIdentityLoginResponse);
        self::assertEquals('5f8174f9-0db5-4a94-93b0-4c418c3ddaad', $response->details->username);
        self::assertTrue($response->authentication_type_code->is(IdentityAuthenticationTypeEnum::IDENTITY_LOGIN()));
        self::assertEquals('New login', $response->name);
        self::assertEquals(443, $response->forced_dg_id);
        self::assertTrue($response->allow_extras);
    }

    /** @test */
    public function out_identity_update_success()
    {
        $jsonResult = '{
        "data": {
            "type": "out-identity",
            "id": "527",
            "attributes": {
                "organization_id": 4444,
                "name": "Update shared",
                "forced_dg_id": 9999,
                "authentication_type_code": "identity_shared_ip",
                "allow_extras": false
            },
            "relationships": {
                "details": {
                    "data": {
                        "type": "out-identity-shared-ip",
                        "id": "5"
                    }
                }
            }
        },
        "included": [{
            "type": "out-identity-shared-ip",
            "id": "5",
            "attributes": {
                "out_identity_id": 527,
                "reseller_id": 3232,
                "created_at": "2017-10-27 20:28:15.000000 +00:00",
                "updated_at": "2017-10-27 20:28:15.000000 +00:00",
                "reseller_name": "Corporate Cie.",
                "ips": [
                    "55.66.77.88/32",
                    "12.21.0.0/16",
                    "20.17.11.0/24"
                ]
            }
         }]
    }';

        $jsonRequest = '{
            "name": "Update shared",
            "forced_dg_id": 9999,
            "allow_extras": false
        }';

        $config = $this->preSuccess($jsonResult);

        $request = new UpdateOutIdentityRequest();
        $request->setOrganizationId(4444)
                ->setIdentityId(3212)
                ->setName("Update shared")
                ->setForcedDgId(9999)
                ->setAllowExtras(false);

        $client = new ProvulusClient($config);

        /**
         * @var $response OutIdentityResponse
         */
        $response = $client->processRequest($request);

        $this->validateRequest($config, $jsonRequest);
        $this->validateRequestUrl($config, ['orgs/4444/identities/3212']);

        self::assertTrue($response->details instanceof OutIdentitySharedIpResponse);
        self::assertCount(3, $response->details->ips);
        self::assertEquals('55.66.77.88/32', $response->details->ips[0]);
        self::assertEquals('Corporate Cie.', $response->details->reseller_name);
        self::assertTrue($response->authentication_type_code->is(IdentityAuthenticationTypeEnum::IDENTITY_SHARED_IP()));
        self::assertEquals('Update shared', $response->name);
        self::assertEquals(9999, $response->forced_dg_id);
        self::assertFalse($response->allow_extras);
    }

    /** @test */
    public function out_identity_delete_success()
    {
        $jsonResult = '{}';
        $jsonRequest = '{}';

        $config = $this->preSuccess($jsonResult);

        $request = new DeleteOutIdentityRequest();
        $request->setOrganizationId(543)
                ->setIdentityId(795);

        $client = new ProvulusClient($config);
        $client->processRequest($request);

        $this->validateRequest($config, $jsonRequest);
        $this->validateRequestUrl($config, ['orgs/543/identities/795']);
    }

    /** @test */
    public function out_identity_bulk_delete_success()
    {
        $jsonResult = '{}';
        $jsonRequest = '{
            "ids": [33, 55, 99, 100]
        }';

        $config = $this->preSuccess($jsonResult);

        $request = new BulkDeleteOutIdentityRequest();
        $request->setOrganizationId(875)
                ->setIds([33, 55, 99, 100]);

        $client = new ProvulusClient($config);
        $client->processRequest($request);

        $this->validateRequest($config, $jsonRequest);
        $this->validateRequestUrl($config, ['orgs/875/identities']);
    }

    /** @test */
    public function out_identity_ip_update_success()
    {
        $jsonResult = '{
        "data": {
            "type": "out-identity",
            "id": "7071",
            "attributes": {
                "organization_id": 4142,
                "name": "Updated ips",
                "forced_dg_id": null,
                "authentication_type_code": "identity_ip",
                "allow_extras": true
            },
            "relationships": {
                "details": {
                    "data": [{
                        "type": "out-identity-ip",
                        "id": "765"
                    },{
                        "type": "out-identity-ip",
                        "id": "666"
                    },{
                        "type": "out-identity-ip",
                        "id": "804"
                    }]
                }
            }
        },
        "included": [{
            "type": "out-identity-ip",
            "id": "765",
            "attributes": {
                "value": "16.25.36.49/32",
                "out_identity_id": 7071,
                "outbound_ip_id": 600
            }
        },{
            "type": "out-identity-ip",
            "id": "666",
            "attributes": {
                "value": "69.8.0.0/16",
                "out_identity_id": 7071,
                "outbound_ip_id": 700
            }
        },{
            "type": "out-identity-ip",
            "id": "804",
            "attributes": {
                "value": "7.7.7.7/32",
                "out_identity_id": 7071,
                "outbound_ip_id": 900
            }
        }]
    }';

        $jsonRequest = '{
            "ips": ["16.25.36.49", "69.8.0.0/16", "7.7.7.7/32"]
        }';

        $config = $this->preSuccess($jsonResult);

        $request = new UpdateOutIdentityIpRequest();
        $request->setOrganizationId(4142)
                ->setIdentityId(7071)
                ->setIps(['16.25.36.49', '69.8.0.0/16', '7.7.7.7/32']);

        $client = new ProvulusClient($config);

        $client->processRequest($request);

        $this->validateRequest($config, $jsonRequest);
        $this->validateRequestUrl($config, ['orgs/4142/identities/7071/identity-ips']);
    }

    /** @test */
    public function out_identity_login_password_reset_success()
    {
        $jsonResult = '{
        "data": {
            "type": "out-identity-login",
            "id": "91",
            "attributes": {
                "username": "5f8174f9-0db5-4a94-93b0-4c418c3ddaad",
                "out_identity_id": 445,
                "new_password": "490rhjg3"
            }
        }
    }';

        $jsonRequest = '{}';

        $config = $this->preSuccess($jsonResult);

        $request = new ResetPasswordOutIdentityRequest();
        $request->setOrganizationId(554)
                ->setIdentityId(445);

        $client = new ProvulusClient($config);

        /** @var OutIdentityLoginResponse $response */
        $response = $client->processRequest($request);

        $this->validateRequest($config, $jsonRequest);
        $this->validateRequestUrl($config, ['orgs/554/identities/445/identity-logins/reset-password']);
        self::assertEquals("490rhjg3", $response->new_password);
    }
}
