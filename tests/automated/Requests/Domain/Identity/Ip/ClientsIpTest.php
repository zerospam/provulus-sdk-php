<?php
/**
 * Created by PhpStorm.
 * User: ycoutu
 * Date: 30/10/17
 * Time: 1:23 PM
 */

namespace automated\Requests\Domain\Identity\Ip;

use ProvulusSDK\Client\ProvulusClient;
use ProvulusSDK\Client\Request\Domain\Identity\Ip\IndexOutClientsIpRequest;
use ProvulusSDK\Client\Request\Domain\Identity\Ip\UpdateOutClientsIpRequest;
use ProvulusSDK\Client\Response\Domain\Identity\Ip\OutClientsIpCollectionResponse;
use ProvulusTest\TestCaseApiKey;

class ClientsIpTest extends TestCaseApiKey
{
    /** @test */
    public function clients_ip_index_success()
    {
        $jsonResult = '{
        "data": [{
            "type": "out-clients-ip",
            "id": "10",
            "attributes": {
                "reseller_id": 1994,
                "outbound_ip_id": 606,
                "created_at": "2017-10-23 19:21:15.000000 +00:00",
                "updated_at": "2017-10-23 19:21:15.000000 +00:00",
                "value": "244.244.244.24/32"
            }
        },{
            "type": "out-clients-ip",
            "id": "11",
            "attributes": {
                "reseller_id": 1994,
                "outbound_ip_id": 607,
                "created_at": "2017-10-23 19:21:15.000000 +00:00",
                "updated_at": "2017-10-23 19:21:15.000000 +00:00",
                "value": "25.25.26.0/24"
            }
        }]
    }';
        $jsonRequest = '{}';

        $config = $this->preSuccess($jsonResult);

        $request = new IndexOutClientsIpRequest();
        $request->setOrganizationId(1994);

        $client = new ProvulusClient($config);

        /**
         * @var $response OutClientsIpCollectionResponse
         */
        $response = $client->processRequest($request);

        $this->validateRequest($config, $jsonRequest);
        $this->validateRequestUrl($config, ['orgs/1994/clients-ip']);

        $clientIp = $response->data()->first();

        self::assertCount(2, $response->data()->toArray());
        self::assertArraySubset(['244.244.244.24/32', '25.25.26.0/24'], $response->ips);
        self::assertEquals('244.244.244.24/32', $clientIp->value);
        self::assertEquals(1994, $clientIp->reseller_id);
    }

    /** @test */
    public function clients_ip_update_ips_success()
    {
        $jsonResult = '{
        "data": [{
            "type": "out-clients-ip",
            "id": "10",
            "attributes": {
                "reseller_id": 1867,
                "outbound_ip_id": 556,
                "created_at": "2017-10-23 19:21:15.000000 +00:00",
                "updated_at": "2017-10-23 19:21:15.000000 +00:00",
                "value": "16.25.36.49/32"
            }
        },{
            "type": "out-clients-ip",
            "id": "11",
            "attributes": {
                "reseller_id": 1867,
                "outbound_ip_id": 557,
                "created_at": "2017-10-23 19:21:15.000000 +00:00",
                "updated_at": "2017-10-23 19:21:15.000000 +00:00",
                "value": "69.8.0.0/16"
            }
        },{
            "type": "out-clients-ip",
            "id": "11",
            "attributes": {
                "reseller_id": 1867,
                "outbound_ip_id": 559,
                "created_at": "2017-10-23 19:21:15.000000 +00:00",
                "updated_at": "2017-10-23 19:21:15.000000 +00:00",
                "value": "7.7.7.7/32"
            }
        }]
    }';

        $jsonRequest = '{
            "ips": ["16.25.36.49", "69.8.0.0/16", "7.7.7.7/32"]
        }';

        $config = $this->preSuccess($jsonResult);

        $request = new UpdateOutClientsIpRequest();
        $request->setOrganizationId(1867)
                ->setIps(['16.25.36.49', '69.8.0.0/16', '7.7.7.7/32']);

        $client = new ProvulusClient($config);

        /**
         * @var $response OutClientsIpCollectionResponse
         */
        $response = $client->processRequest($request);

        $this->validateRequest($config, $jsonRequest);
        $this->validateRequestUrl($config, ['orgs/1867/clients-ip']);

        $clientIp = $response->data()->first();

        self::assertCount(3, $response->data()->toArray());
        self::assertArraySubset(['16.25.36.49/32', '69.8.0.0/16', '7.7.7.7/32'], $response->ips);
        self::assertEquals('16.25.36.49/32', $clientIp->value);
        self::assertEquals(1867, $clientIp->reseller_id);
    }
}
