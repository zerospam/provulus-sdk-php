<?php
/**
 * Created by PhpStorm.
 * User: pbb
 * Date: 19/12/16
 * Time: 10:43 AM
 */

namespace automated\Requests\Domain;


use GuzzleHttp\Handler\MockHandler;
use ProvulusSDK\Client\ProvulusClient;
use ProvulusSDK\Client\Request\Domain\BulkDeleteDomainRequest;
use ProvulusSDK\Client\Request\Domain\CreateDomainRequest;
use ProvulusSDK\Client\Request\Domain\DeleteDomainRequest;
use ProvulusSDK\Client\Request\Domain\IndexDomainRequest;
use ProvulusSDK\Client\Request\Domain\MoveToFilteringPolicyDomainRequest;
use ProvulusSDK\Client\Request\Domain\MoveToOrganizationDomainRequest;
use ProvulusSDK\Client\Request\Domain\ReadDomainRequest;
use ProvulusSDK\Client\Request\Domain\Student\CreateFakeStudentDomainRequest;
use ProvulusSDK\Client\Request\Domain\Student\IndexAvailableFakeStudentRequest;
use ProvulusSDK\Client\Request\Domain\UpdateDomainRequest;
use ProvulusSDK\Client\Response\Domain\DomainResponse;
use ProvulusSDK\Client\Response\EmptyResponse;
use ProvulusSDK\Config\ProvulusConfiguration;
use ProvulusSDK\Enum\Domain\DomainTransportProtocolEnum;
use ProvulusSDK\Enum\Domain\DomainTypeEnum;
use ProvulusSDK\Enum\Mail\MailSMTPServerTypeEnum;
use ProvulusTest\TestCase;

/**
 * Class CreateDomainTest
 *
 * Tests for Domain Requests
 *
 * @package automated\Requests\Domain
 */
class DomainTest extends TestCase
{
    /**
     * @test
     **/
    public function create_domain_test()
    {
        $jsonResult
            = '{
                "data":{
                    "type":"domains",
                    "id":"4820",
                    "attributes":{
                        "organization_id":2888,
                        "domain_group_id":3141,
                        "transport_port":25,
                        "transport_use_mx":false,
                        "transport_hold":true,
                        "is_unconditional_delete":false,
                        "transport_protocol":"smtp",
                        "domain_type":"standard",
                        "name":"munchkin.com",
                        "transport":"mailcatcher",
                        "nb_declared_mailboxes":1,
                        "email":"test@munchkin.com",
                        "description":"This is a new domain.",
                        "updated_at":"2016-12-19 16:12:08.000000 +00:00",
                        "created_at":"2016-12-19 16:12:08.000000 +00:00",
                        "annual_price":600,
                        "organization_name":"hodor.com Client"
                    }
                }
            }';

        $jsonRequest
            = '{
                "name": "munchkin.com",
                "transport": "mailcatcher",
                "email": "test@munchkin.com",
                "description": "This is a new domain.",
                "domain_type": "standard",
                "nb_mailboxes": 1,
                "transport_port": 25,
                "domain_group_id": 3141,
                "transport_hold": true
            }';

        $config = $this->preSuccess($jsonResult);

        $request = new CreateDomainRequest();
        $request->setName('munchkin.com')
                ->setDomainType(DomainTypeEnum::STANDARD())
                ->setDescription('This is a new domain.')
                ->setEmail('test@munchkin.com')
                ->setNbMailboxes(1)
                ->setTransportPort(25)
                ->setTransport('mailcatcher')
                ->setTransportHold(true)
                ->setDomainGroupId(3141)
                ->setOrganizationId(2888);

        $client = new ProvulusClient($config);

        /**
         * @var $response DomainResponse
         */
        $response = $client->processRequest($request);

        $this->validateRequest($config, $jsonRequest);

        self::assertEquals('test@munchkin.com', $response->email, 'The email is not as expected.');
        self::assertNotEquals(28, $response->domain_group_id, 'The domain group id is not as expected.');
        self::assertEquals(3141, $response->domain_group_id, 'The domain group id is not as expected.');
        self::assertTrue($response->transport_hold, 'The transport hold is not as expected');
    }


    /**
     * @test
     **/
    public function domain_delete_success()
    {
        $jsonRequest = '{}';
        $jsonResult  = '{}';

        $config = $this->preSuccess($jsonResult);

        $request = new DeleteDomainRequest();
        $request->setOrganizationId(2888)
                ->setDomainId(4820);

        $client = new ProvulusClient($config);

        $response = $client->processRequest($request);

        $this->validateRequest($config, $jsonRequest);

        self::assertTrue($response instanceof EmptyResponse, 'The response class is not as expected.');
    }


    /**
     * @test
     **/
    public function index_domain_success()
    {
        $jsonResponse
            = '{
  "data": [
    {
      "type": "domains",
      "id": "4466",
      "attributes": {
        "domain_group_id": 1,
        "organization_id": 1,
        "name": "westworld.com",
        "description": "dsfsd",
        "transport": "12.12.12.12",
        "transport_protocol": "smtp",
        "transport_port": 25,
        "transport_use_mx": false,
        "transport_hold": false,
        "is_active": true,
        "created_at": "2016-12-15 15:23:44.000000 +00:00",
        "updated_at": null,
        "is_active_backup": null,
        "is_unconditional_delete": false,
        "count_start_date": null,
        "mailbox_count": null,
        "rejected_mailbox_count": null,
        "nb_declared_mailboxes": 546,
        "domain_type": "standard",
        "dmbcount_updated_at": null,
        "last_nb_declared_mailboxes": null,
        "is_dmbcount_declared": true,
        "email": "sdflkhjgred@westworld.com",
        "is_check_transport_success": false,
        "is_check_mx_success": false,
        "transport_server_type": "BASIC",
        "activation_date": null,
        "annual_price": 6027.839999999999,
        "organization_name": "ZEROSPAM"
      }
    }
  ],
  "meta": {
    "pagination": {
      "total": 1,
      "count": 1,
      "per_page": 15,
      "current_page": 1,
      "total_pages": 1,
      "links": []
    }
  }
}';
        $jsonRequest
            = '{}';

        $config = $this->preSuccess($jsonResponse);

        $request = new IndexDomainRequest();
        $request->setOrganizationId(2888);

        $client = new ProvulusClient($config);

        $client->processRequest($request);

        $collectionResponse = $request->getResponse();

        /**
         * @var $domainResponse DomainResponse
         */
        $domainResponse = $collectionResponse->data()->first();

        $this->validateRequest($config, $jsonRequest);

        $this->assertEquals(4466, $domainResponse->id());
        $this->assertEquals('westworld.com', $domainResponse->name);
        $this->assertEquals('domains', $collectionResponse->type());

        $this->assertEquals(1, $collectionResponse->getMetaData()->total);
        $this->assertEquals(1, $collectionResponse->getMetaData()->count);
        $this->assertEquals(15, $collectionResponse->getMetaData()->perPage);
        $this->assertEquals(1, $collectionResponse->getMetaData()->currentPage);
        $this->assertEquals(1, $collectionResponse->getMetaData()->totalPages);
    }

    /**
     * @test
     */
    public function index_domain_search_success()
    {
        $jsonResponse
            = '{
  "data": [
    {
      "type": "domains",
      "id": "4466",
      "attributes": {
        "domain_group_id": 1,
        "organization_id": 1,
        "name": "westworld.com",
        "description": "dsfsd",
        "transport": "12.12.12.12",
        "transport_protocol": "smtp",
        "transport_port": 25,
        "transport_use_mx": false,
        "transport_hold": false,
        "is_active": true,
        "created_at": "2016-12-15 15:23:44.000000 +00:00",
        "updated_at": null,
        "is_active_backup": null,
        "is_unconditional_delete": false,
        "count_start_date": null,
        "mailbox_count": null,
        "rejected_mailbox_count": null,
        "domain_type": "standard",
        "dmbcount_updated_at": null,
        "is_dmbcount_declared": true,
        "email": "sdflkhjgred@westworld.com",
        "is_check_transport_success": false,
        "is_check_mx_success": false,
        "transport_server_type": "BASIC",
        "activation_date": null,
        "annual_price": 6027.839999999999,
        "organization_name": "ZEROSPAM",
        "nb_mailboxes":546
      }
    }
  ],
  "meta": {
    "pagination": {
      "total": 1,
      "count": 1,
      "per_page": 15,
      "current_page": 1,
      "total_pages": 1,
      "links": []
    }
  }
}';
        $jsonRequest
            = '{}';

        $config = $this->preSuccess($jsonResponse);

        $request = new IndexDomainRequest();
        $request->setOrganizationId(2888)
                ->setFilteringId(1);

        $client = new ProvulusClient($config);

        $client->processRequest($request);

        $this->validateRequest($config, $jsonRequest);

        $this->validateRequestUrl($config, ['filtering_policy=1']);
    }


    /**
     * @test
     */
    public function index_domain_search_with_deletable_success()
    {
        $jsonResponse
            = '{"data":[{"type":"domains","id":"123","attributes":{"domain_group_id":632,"organization_id":612,"name":"test.com","description":null,"transport":"1.1.1.1","transport_protocol":"smtp","transport_port":25,"transport_use_mx":false,"transport_hold":false,"is_active":true,"created_at":"2011-02-14 16:06:28.000000 +00:00","updated_at":"2016-11-05 14:15:56.000000 +00:00","is_active_backup":null,"is_unconditional_delete":false,"count_start_date":"2012-12-06 14:35:05.336215 +00:00","mailbox_count":128,"rejected_mailbox_count":10492,"domain_type":"standard","dmbcount_updated_at":"2016-02-22 23:16:00.000000 +00:00","is_dmbcount_declared":true,"email":null,"is_check_transport_success":false,"is_check_mx_success":false,"transport_server_type":"basic","activation_date":null,"annual_price":3907.7999999999997,"nb_mailboxes":167,"is_deletable":false}}],"meta":{"pagination":{"total":1,"count":1,"per_page":15,"current_page":1,"total_pages":1}}}';
        $jsonRequest
            = '{}';

        $config = $this->preSuccess($jsonResponse);

        $request = new IndexDomainRequest();
        $request->setOrganizationId(2888)->withDeletable();

        $client = new ProvulusClient($config);

        $client->processRequest($request);

        $this->validateRequest($config, $jsonRequest);

        $this->validateRequestUrl($config, ['with_deletable=1']);

        /** @var DomainResponse $domain */
        $domain = $request->getResponse()->data()->first();
        self::assertEquals(false, $domain->is_deletable);
    }


    /**
     * @test
     **/
    public function read_domain_success()
    {
        $jsonResult
                     = '{
                "data":{
                    "type":"domains",
                    "id":"4820",
                    "attributes":{
                        "domain_group_id":3141,
                        "organization_id":2888,
                        "name":"munchkin.com",
                        "description":"This is an updated domain.",
                        "transport":"mailcatcher",
                        "transport_protocol":"smtp",
                        "transport_port":25,
                        "transport_use_mx":false,
                        "transport_hold":false,
                        "is_active":true,
                        "created_at":"2016-12-19 16:12:08.000000 +00:00",
                        "updated_at":"2016-12-20 19:58:49.000000 +00:00",
                        "is_active_backup":null,
                        "is_unconditional_delete":false,
                        "count_start_date":null,
                        "mailbox_count":null,
                        "rejected_mailbox_count":null,
                        "domain_type":"mirror",
                        "dmbcount_updated_at":null,
                        "is_dmbcount_declared":true,
                        "email":"munchies@munchkin.com",
                        "is_check_transport_success":false,
                        "is_check_mx_success":false,
                        "transport_server_type":"BASIC",
                        "activation_date":null,
                        "annual_price":600,
                        "organization_name":"hodor.com Client",
                        "nb_mailboxes":28
                    }
                }
            }';
        $jsonRequest = '{}';

        $config = $this->preSuccess($jsonResult);

        $request = new ReadDomainRequest();
        $request->setDomainId(4820)
                ->setOrganizationId(2888);

        $client = new ProvulusClient($config);

        /**
         * @var $response DomainResponse
         */
        $response = $client->processRequest($request);

        $this->validateRequest($config, $jsonRequest);

        self::assertEquals('munchies@munchkin.com', $response->email, 'The email is not as expected.');
        self::assertNotEquals(28, $response->domain_group_id, 'The domain group id is not as expected.');
        self::assertEquals(3141, $response->domain_group_id, 'The domain group id is not as expected.');
        self::assertEquals(28, $response->nb_mailboxes, 'The mailbox count is not as expected.');
        self::assertEquals(MailSMTPServerTypeEnum::BASIC()
                                                 ->getValue(), strtolower($response->transport_server_type),
            'The mail server type is not as expected.');
        self::assertTrue($response->transport_protocol->is(DomainTransportProtocolEnum::SMTP()));
    }


    /**
     * @test
     **/
    public function update_domain_success()
    {
        $jsonResult
            = '{
            "data":{
                "type":"domains",
                "id":"4820",
                "attributes":{
                    "domain_group_id":3141,
                    "organization_id":2888,
                    "name":"munchkin.com",
                    "description":"This is an updated domain.",
                    "transport":"mailcatcher",
                    "transport_protocol":"notransport",
                    "transport_port":25,
                    "transport_use_mx":false,
                    "transport_hold":false,
                    "is_active":true,
                    "created_at":"2016-12-19 16:12:08.000000 +00:00",
                    "updated_at":"2016-12-20 19:58:49.000000 +00:00",
                    "is_active_backup":null,
                    "is_unconditional_delete":true,
                    "count_start_date":null,
                    "mailbox_count":null,
                    "rejected_mailbox_count":null,
                    "domain_type":"mirror",
                    "dmbcount_updated_at":null,
                    "is_dmbcount_declared":true,
                    "email":"munchies@munchkin.com",
                    "is_check_transport_success":false,
                    "is_check_mx_success":false,
                    "transport_server_type":"BASIC",
                    "activation_date":null,
                    "annual_price":600,
                    "organization_name":"hodor.com Client",
                    "nb_mailboxes":28
                }
            }
        }';

        $jsonRequest
            = '{
                "transport":"mailcatcher",
                "email":"munchies@munchkin.com",
                "description":"This is an updated domain.",
                "domain_type":"mirror",
                "transport_port":25,
                "domain_group_id":3141,
                "transport_hold":false,
                "is_unconditional_delete": true,
                "transport_protocol": "notransport"
            }';

        $config = $this->preSuccess($jsonResult);

        $request = new UpdateDomainRequest();
        $request->setDomainType(DomainTypeEnum::MIRROR())
                ->setDescription('This is an updated domain.')
                ->setEmail('munchies@munchkin.com')
                ->setTransportPort(25)
                ->setTransport('mailcatcher')
                ->setTransportHold(false)
                ->setDomainId(4820)
                ->setDomainGroupId(3141)
                ->setOrganizationId(2888)
                ->setIsUnconditionalDelete(true)
                ->setTransportProtocol(DomainTransportProtocolEnum::NOTRANSPORT());

        $client = new ProvulusClient($config);

        /**
         * @var $response DomainResponse
         */
        $response = $client->processRequest($request);

        $this->validateRequest($config, $jsonRequest);

        self::assertEquals('munchies@munchkin.com', $response->email, 'The email not as expected.');
        self::assertNotEquals(28, $response->domain_group_id, 'The domain group id is not as expected.');
        self::assertEquals(3141, $response->domain_group_id, 'The domain group id is not as expected.');
        self::assertTrue($response->transport_hold === false, 'The transport hold is not as expected');
        self::assertEquals(4820, $response->id(), 'The domain id is not as expected.');
        self::assertTrue($response->is_unconditional_delete, "The is unconditional delete is not as expected");
        self::assertTrue($response->transport_protocol->is(DomainTransportProtocolEnum::NOTRANSPORT()),
            "The transport protocol is not as expected");
    }

    /**
     * @test
     **/
    public function domain_bulk_delete()
    {
        $jsonResult = [];

        $jsonRequest
            = '{
            "ids" :[
            3748, 
            3747
            ]
        }';

        $config = $this->preSuccess($jsonResult);

        $request = new BulkDeleteDomainRequest();
        $request->setOrganizationId(1)
                ->setIds([
                    3748,
                    3747,
                ]);

        $client = new ProvulusClient($config);
        $client->processRequest($request);

        $this->validateRequest($config, $jsonRequest);
        $this->validateUrl($config, 'orgs/1/domains');

        $response = $request->getResponse();
        $this->assertInstanceOf(EmptyResponse::class, $response);
    }

    /** @test */
    public function move_to_org_domain_success()
    {
        $jsonResult = '{}';

        $jsonRequest = '{
            "organization_id": 739,
            "filtering_policy_name": "Policy name",
            "ids": [135, 246, 789]
        }';

        $config = $this->preSuccess($jsonResult);

        $request = new MoveToOrganizationDomainRequest();

        $request->setOrganizationId(937);
        $request->setTargetOrganizationId(739)
                ->setFilteringPolicyName("Policy name")
                ->setIds([135, 246, 789]);

        $client = new ProvulusClient($config);
        $client->processRequest($request);

        $this->validateRequest($config, $jsonRequest);
        $this->validateUrl($config, 'orgs/937/domains/movetoorg');

        $response = $request->getResponse();
        $this->assertInstanceOf(EmptyResponse::class, $response);
    }

    /** @test */
    public function move_to_policy_domain_success()
    {
        $jsonResult = [];

        $jsonRequest = '{
            "ids": [505, 606, 707],
            "filtering_policy_id": 999
        }';

        $config = $this->preSuccess($jsonResult);

        $request = new MoveToFilteringPolicyDomainRequest();
        $request->setOrganizationId(404);
        $request->setIds([505, 606, 707])
                ->setTargetFilteringPolicyId(999);

        $client = new ProvulusClient($config);
        $client->processRequest($request);

        $this->validateRequest($config, $jsonRequest);
        $this->validateUrl($config, 'orgs/404/domains/movetopolicy');

        $response = $request->getResponse();
        $this->assertInstanceOf(EmptyResponse::class, $response);
    }

    /**
     * @test
     */
    public function createFakeStudentDomainTest()
    {
        $responseBody = <<<JSON
{
  "data": {
    "type": "domains",
    "id": "7224",
    "attributes": {
      "created_at": "2018-10-04 20:06:59.487715 +00:00",
      "updated_at": "2018-10-04 20:06:59.487715 +00:00",
      "domain_group_id": 1,
      "organization_id": 1,
      "name": "edufictif.zerospam.tester.com",
      "description": "Domaine fictif - boîtes étudiantes",
      "transport": "fake.zerospam.tester.com",
      "transport_protocol": "smtp",
      "transport_port": 25,
      "transport_use_mx": false,
      "transport_hold": false,
      "is_unconditional_delete": false,
      "domain_type": "student_fake",
      "dmbcount_updated_at": "2018-10-04 20:06:59.486807 +00:00",
      "transport_server_type": "basic",
      "delete_requested_at": null,
      "annual_price": 600,
      "nb_mailboxes": 1
    }
  }
}
JSON;

        $requestBody = <<<JSON
{
	"domain_id" : 7223,
	"nb_mailboxes": 1
}
JSON;

        $config = $this->preSuccess($responseBody);

        $request = new CreateFakeStudentDomainRequest();
        $request->setOrganizationId(1)
                ->setDomainId(7223)
                ->setNbMailboxes(1);

        $client = new ProvulusClient($config);
        $client->processRequest($request);

        $this->validateRequest($config, $requestBody);
        $this->validateUrl($config, 'orgs/1/domains/fake');

        $response = $request->getResponse();
        $this->assertInstanceOf(DomainResponse::class, $response);
        $this->assertTrue($response->domain_type->is(DomainTypeEnum::STUDENT_FAKE()));
    }

    /**
     * @test
     */
    public function indexDomainsAvailableForFake()
    {
        $responseBody = <<<JSON
{
  "data": [
    {
      "type": "domains",
      "id": "1234",
      "attributes": {
        "domain_group_id": 1,
        "organization_id": 1,
        "name": "test.zerospam.ca",
        "description": "Domaine de test",
        "transport": "test.zerospam.ca",
        "transport_protocol": "smtp",
        "transport_port": 1234,
        "transport_use_mx": false,
        "transport_hold": false,
        "is_active": true,
        "created_at": "2018-03-26 14:28:54.858859 +00:00",
        "updated_at": "2018-03-26 14:28:54.858859 +00:00",
        "is_active_backup": null,
        "is_unconditional_delete": false,
        "count_start_date": null,
        "mailbox_count": 0,
        "rejected_mailbox_count": null,
        "domain_type": "standard",
        "dmbcount_updated_at": "2018-03-26 14:28:54.000000 +00:00",
        "last_nb_declared_mailboxes": null,
        "is_dmbcount_declared": true,
        "email": "fake@filter.zerospam.ca",
        "is_check_transport_success": false,
        "is_check_mx_success": false,
        "transport_server_type": "basic",
        "activation_date": null,
        "delete_requested_at": null,
        "deleted_at": null,
        "annual_price": 600,
        "nb_mailboxes": 1
      }
    }
  ]
}        
JSON;

        $requestBody = [];

        $config = $this->preSuccess($responseBody);

        $request = new IndexAvailableFakeStudentRequest();
        $request->setOrganizationId(1)
                ->setFilteringId(1);

        $client = new ProvulusClient($config);
        $client->processRequest($request);

        $this->validateRequest($config, $requestBody);
        $this->validateUrl($config, 'orgs/1/policies/1/standards_for_fake');

        $response = $request->getResponse();
        $first    = $response->data()->first();
        $this->assertInstanceOf(DomainResponse::class, $first);
        $this->assertTrue($first->domain_type->is(DomainTypeEnum::STANDARD()));
    }

    /**
     * Gets config for tests
     *
     * @param MockHandler $handler
     *
     * @return ProvulusConfiguration
     */
    protected function getConfig(MockHandler $handler)
    {
        return parent::getConfig($handler)->setApiKey('test');
    }

}