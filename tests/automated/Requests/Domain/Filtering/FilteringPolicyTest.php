<?php
/**
 * Created by PhpStorm.
 * User: pbb
 * Date: 21/12/16
 * Time: 3:12 PM
 */

namespace automated\Requests\Domain\Filtering;

use GuzzleHttp\Handler\MockHandler;
use ProvulusSDK\Client\ProvulusClient;
use ProvulusSDK\Client\Request\Domain\Filtering\Bulk\BulkDeleteFilteringPolicyRequest;
use ProvulusSDK\Client\Request\Domain\Filtering\CreateFilteringPolicyRequest;
use ProvulusSDK\Client\Request\Domain\Filtering\DeleteFilteringPolicyRequest;
use ProvulusSDK\Client\Request\Domain\Filtering\IndexFilteringPolicyRequest;
use ProvulusSDK\Client\Request\Domain\Filtering\ReadFilteringPolicyRequest;
use ProvulusSDK\Client\Request\Domain\Filtering\UpdateFilteringPolicyRequest;
use ProvulusSDK\Client\Response\Domain\Filtering\FilteringPolicyResponse;
use ProvulusSDK\Config\ProvulusConfiguration;
use ProvulusSDK\Enum\Filtering\FilteringPolicyEnum;
use ProvulusSDK\Enum\Filtering\GreymailSensitivityEnum;
use ProvulusTest\TestCase;

/**
 * Class CreateFilteringPolicyTest
 *
 * Test for Filtering Policy Requests
 *
 * @package automated\Requests\Domain\Filtering
 */
class FilteringPolicyTest extends TestCase
{
    /**
     * @test
     **/
    public function create_filtering_policy_success()
    {
        $jsonResult = '{
            "data": {
                "type": "filtering-policy",
                "id": "3145",
                "attributes": {
                    "is_active": true,
                    "filter_type_code": "inbound",
                    "name":"Main Inbound Filtering policy",
                    "is_active_backup": false,
                    "is_ldap_filtering_enabled": false,
                    "max_message_size_in_mb": 50,
                    "is_using_greylist": true,
                    "is_content_filtering_enabled": true,
                    "organization_id": 2888,
                    "greymail_sensitivity": "strict",
                    "updated_at": "2016-12-21 20:20:24.000000 +00:00",
                    "created_at": "2016-12-21 20:20:24.000000 +00:00"
                }
            }
        }';

        $jsonRequest = '{
            "name": "Main Inbound Filtering policy",
            "filter_type_code": "inbound",
            "greymail_sensitivity": "strict",
            "base_filtering_policy": 765
        }';

        $config = $this->preSuccess($jsonResult);

        $request = new CreateFilteringPolicyRequest();
        $request->setName('Main Inbound Filtering policy')
            ->setFilterTypeCode(FilteringPolicyEnum::INBOUND())
            ->setOrganizationId(2888)
            ->setGreymailSensitivity(GreymailSensitivityEnum::STRICT())
            ->setBaseFilteringPolicy(765);

        $client = new ProvulusClient($config);
        /**
         * @var $response FilteringPolicyResponse
         */
        $response = $client->processRequest($request);

        $this->validateRequest($config, $jsonRequest);

        $this->assertEquals(2888, $response->organization_id, 'Organization ID incorrect.');
        $this->assertEquals(3145, $response->id(), 'Filtering Policy id is incorrect.');
        $this->assertTrue($response->filter_type_code->is(FilteringPolicyEnum::INBOUND()), 'Filtering Policy code is incorrect.');
        $this->assertTrue($response->greymail_sensitivity->is(GreymailSensitivityEnum::STRICT()), 'Filtering Policy code is incorrect.');
    }

    /**
     * @test
     **/
    public function read_filtering_policy_request()
    {
        $jsonResult = '{
                "data": {
                    "type":"filtering-policy",
                    "id":"3142",
                    "attributes": {
                        "organization_id":2888,
                        "name":"Secondary inbound filtering policy.",
                        "description":"This is the secondary filtering policy for inbound filtering.",
                        "is_using_greylist":true,
                        "is_content_filtering_enabled":true,
                        "created_at":"2016-12-02 14:02:01.000000 +00:00",
                        "updated_at":"2016-12-21 20:55:22.000000 +00:00",
                        "is_active":true,"is_active_backup":false,
                        "max_message_size_in_mb":15,
                        "is_ldap_filtering_enabled":false,
                        "filter_type_code": "inbound",
                        "is_default": false,
                        "greymail_sensitivity": "normal",
                        "items": {
                            "zerospam.ca": {
                                "isTransportEnabled": true,
                                "isDomain": true
                            },"fake.zerospam.ca": {
                                "isTransportEnabled": true,
                                "isDomain": true
                            },"test@errata.zerospam.ca": {
                                "isTransportEnabled": true,
                                    "isDomain": false
                            }
                        }
                    }, "relationships": {
                        "filteringPolicyItems": {
                            "data": [{
                                "type": "filtering-policy-item",
                                "id": "467"
                            }]
                        }
                    }
                },
                "included": [{
                    "type": "filtering-policy-item",
                    "id": "467",
                    "attributes": {
                        "domain_group_id": 1,
                        "value_type_code": "address",
                        "value": "test@zerospam.ca",
                        "domain_id": 1118,
                        "created_at": "2017-07-06 14:37:19.000000 +00:00",
                     "updated_at": null
                    }
                }]
            }';
        $jsonRequest = '{}';

        $config = $this->preSuccess($jsonResult);

        $request = new ReadFilteringPolicyRequest();
        $request->setOrganizationId(2888)
            ->setFilteringId(3142)
            ->withFilteringPolicyItems();

        $client = new ProvulusClient($config);

        /**
         * @var $response FilteringPolicyResponse
         */
        $response = $client->processRequest($request);

        $this->validateRequest($config, $jsonRequest);

        $this->validateRequestUrl($config, ['include=filteringPolicyItems']);

        self::assertEquals(2888, $response->organization_id, 'Organization ID incorrect.');
        self::assertEquals(3142, $response->id(), 'Filtering Policy id is incorrect.');
        self::assertTrue($response->is_active, 'Filtering policy is active is not correct.');
        self::assertTrue(
            $response->greymail_sensitivity->is(GreymailSensitivityEnum::NORMAL()),
            'Filtering policy greymail sensitivity is not correct.'
        );
        self::assertGreaterThan(14, $response->max_message_size_in_mb, 'Max message size in MB should be greater than 14.');
        self::assertEquals(1, count($response->filtering_policy_items->toArray()));
    }

    /**
     * @test
     **/
    public function update_filtering_policy_success()
    {
        $jsonResult = '{
            "data": {
                "type": "filtering-policy",
                "id": "3142",
                "attributes": {
                    "organization_id": 2888,
                    "name": "Secondary inbound filtering policy.",
                    "description": "This is the secondary filtering policy for inbound filtering.",
                    "is_using_greylist": true,
                    "is_content_filtering_enabled": true,
                    "created_at": "2016-12-02 14:02:01.000000 +00:00",
                    "updated_at": "2016-12-21 20:55:22.000000 +00:00",
                    "is_active": true,
                    "is_active_backup": false,
                    "max_message_size_in_mb": 15,
                    "is_ldap_filtering_enabled": false,
                    "filter_type_code": "outbound",
                    "greymail_sensitivity": "permissive"
                }
            }
        }';
        $jsonRequest = '{
            "name": "Secondary inbound filtering policy.",
            "description": "This is the secondary filtering policy for inbound filtering.",
            "filter_type_code": "outbound",
            "is_using_greylist": true,
            "max_message_size_in_mb": 15,
            "greymail_sensitivity": "permissive"
        }';

        $config = $this->preSuccess($jsonResult);

        $request = new UpdateFilteringPolicyRequest();
        $request->setName('Secondary inbound filtering policy.')
            ->setDescription('This is the secondary filtering policy for inbound filtering.')
            ->setIsUsingGreylist(true)
            ->setMaxMessageSizeInMb(15)
            ->setFilterTypeCode(FilteringPolicyEnum::OUTBOUND())
            ->setOrganizationId(2888)
            ->setFilteringId(3142)
            ->setGreymailSensitivity(GreymailSensitivityEnum::PERMISSIVE());

        $client = new ProvulusClient($config);

        /**
         * @var $response FilteringPolicyResponse
         */
        $response = $client->processRequest($request);

        $this->validateRequest($config, $jsonRequest);

        $this->assertEquals(2888, $response->organization_id, 'Organization ID incorrect.');
        $this->assertEquals(3142, $response->id(), 'Filtering Policy id is incorrect.');
        self::assertTrue($response->is_active, 'Filtering policy is active is not correct.');
        self::assertTrue(
            $response->greymail_sensitivity->is(GreymailSensitivityEnum::PERMISSIVE()),
            'Filtering policy greymail sensitivity is not correct.'
        );
    }

    /**
     * @test
     **/
    public function index_filtering_policy_success()
    {
        $jsonResponse = '{
            "data":[{
                "type":"filtering-policy",
                "id":"3141",
                "attributes": {
                    "organization_id":2888,
                    "name":"GRP_1",
                    "description":null,
                    "is_using_greylist":true,
                    "is_content_filtering_enabled":true,
                    "created_at":"2016-12-02 14:01:58.000000 +00:00",
                    "updated_at":"2016-12-02 14:01:58.000000 +00:00",
                    "is_active":true,
                    "is_active_backup":false,
                    "max_message_size_in_mb":50,
                    "is_ldap_filtering_enabled":false,
                    "filter_type_code":2,
                    "greymail_sensitivity": "strict"
                }
            }, {
                "type":"filtering-policy",
                "id":"3142",
                "attributes": {
                    "organization_id":2888,
                    "name":"testing",
                    "description":null,
                    "is_using_greylist":true,
                    "is_content_filtering_enabled":true,
                    "created_at":"2016-12-02 14:02:01.000000 +00:00",
                    "updated_at":"2016-12-02 14:02:01.000000 +00:00",
                    "is_active":true,"is_active_backup":false,
                    "max_message_size_in_mb":50,"is_ldap_filtering_enabled":false,
                    "filter_type_code":2,
                    "greymail_sensitivity": "normal"
                }
            }],
            "meta": {
                "pagination": {
                    "total": 2,
                    "count": 2,
                    "per_page": 15,
                    "current_page": 1,
                    "total_pages": 1,
                    "links": []
                }
            }
        }';

        $jsonRequest = '{}';

        $config = $this->preSuccess($jsonResponse);

        $request = new IndexFilteringPolicyRequest();
        $request->setOrganizationId(2888);

        $client = new ProvulusClient($config);
        $client->processRequest($request);

        $collectionResponse = $request->getResponse();

        $this->validateRequest($config, $jsonRequest);

        /**
         * @var FilteringPolicyResponse $filteringPolicyResponse
         */
        $filteringPolicyResponse = $collectionResponse->data()->first();
        self::assertEquals('GRP_1', $filteringPolicyResponse->get('name'));
        self::assertEquals(50, $filteringPolicyResponse->get('max_message_size_in_mb'));
        self::assertTrue($filteringPolicyResponse->get('is_using_greylist'));
        self::assertTrue($filteringPolicyResponse->greymail_sensitivity->is(GreymailSensitivityEnum::STRICT()));

    }

    /** @test */
    public function index_filtering_policy_search_type_success()
    {
        $jsonResponse = '{
            "data":[{
                "type":"filtering-policy",
                "id":"3141",
                "attributes": {
                    "organization_id":2888,
                    "name":"GRP_1",
                    "description":null,
                    "is_using_greylist":true,
                    "is_content_filtering_enabled":true,
                    "created_at":"2016-12-02 14:01:58.000000 +00:00",
                    "updated_at":"2016-12-02 14:01:58.000000 +00:00",
                    "is_active":true,
                    "is_active_backup":false,
                    "max_message_size_in_mb":50,
                    "is_ldap_filtering_enabled":false,
                    "filter_type_code": "inbound",
                    "greymail_sensitivity": "normal"
                }
            }, {
                "type":"filtering-policy",
                "id":"3142",
                "attributes": {
                    "organization_id":2888,
                    "name":"testing",
                    "description":null,
                    "is_using_greylist":true,
                    "is_content_filtering_enabled":true,
                    "created_at":"2016-12-02 14:02:01.000000 +00:00",
                    "updated_at":"2016-12-02 14:02:01.000000 +00:00",
                    "is_active":true,"is_active_backup":false,
                    "max_message_size_in_mb":50,"is_ldap_filtering_enabled":false,
                    "filter_type_code": "inbound",
                    "greymail_sensitivity": "permissive"
                }
            }],
            "meta": {
                "pagination": {
                      "total": 2,
                      "count": 2,
                      "per_page": 15,
                      "current_page": 1,
                      "total_pages": 1,
                      "links": []
                }
            }
        }';

        $jsonRequest = '{}';

        $config = $this->preSuccess($jsonResponse);

        $request = new IndexFilteringPolicyRequest();
        $request->setOrganizationId(2888)
            ->setFilterType(FilteringPolicyEnum::INBOUND());

        $client = new ProvulusClient($config);
        $client->processRequest($request);

        $this->validateRequest($config, $jsonRequest);

        $this->validateRequestUrl($config, ['filter_type=inbound']);
    }

    /**
     * @test
     **/
    public function delete_filtering_policy_success()
    {
        $jsonRequest = '{}';
        $jsonResult = '{}';

        $config = $this->preSuccess($jsonResult);

        $request = new DeleteFilteringPolicyRequest();
        $request->setOrganizationId(2888)
            ->setFilteringId(3142);

        $client = new ProvulusClient($config);

        $client->processRequest($request);

        $this->validateRequest($config, $jsonRequest);
    }

    /** @test */
    public function bulk_delete_filtering_policy_success()
    {
        $jsonRequest = '{
            "ids": [2, 3, 5, 7, 11]
        }';
        $jsonResult = '{}';

        $config = $this->preSuccess($jsonResult);

        $request = new BulkDeleteFilteringPolicyRequest();
        $request->setOrganizationId(2888)
            ->setIds([2, 3, 5, 7, 11]);

        $client = new ProvulusClient($config);

        $client->processRequest($request);

        $this->validateRequest($config, $jsonRequest);
    }

    /**
     * Gets config for tests
     *
     * @param MockHandler $handler
     *
     * @return ProvulusConfiguration
     */
    protected function getConfig(MockHandler $handler)
    {
        return parent::getConfig($handler)->setApiKey('test');
    }
}
