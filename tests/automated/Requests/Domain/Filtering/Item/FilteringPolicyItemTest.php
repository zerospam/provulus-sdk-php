<?php
/**
 * Created by PhpStorm.
 * User: pbb
 * Date: 22/12/16
 * Time: 2:08 PM
 */

namespace automated\Requests\Domain\Filtering\Item;

use GuzzleHttp\Handler\MockHandler;
use ProvulusSDK\Client\ProvulusClient;
use ProvulusSDK\Client\Request\Domain\Filtering\Item\AllFilteringPolicyItemRequest;
use ProvulusSDK\Client\Request\Domain\Filtering\Item\Bulk\BulkDeleteFilteringPolicyItemRequest;
use ProvulusSDK\Client\Request\Domain\Filtering\Item\DeleteFilteringPolicyItemRequest;
use ProvulusSDK\Client\Request\Domain\Filtering\Item\IndexFilteringPolicyItemRequest;
use ProvulusSDK\Client\Request\Domain\Filtering\Item\MoveToOrganizationFilteringPolicyItemRequest;
use ProvulusSDK\Client\Request\Domain\Filtering\Item\MoveToPolicyFilteringPolicyItemRequest;
use ProvulusSDK\Client\Request\Domain\Filtering\Item\ReadFilteringPolicyItemRequest;
use ProvulusSDK\Client\Request\Resource\Domain\Filtering\Item\FilteringPolicyItemRequestTypeEnum;
use ProvulusSDK\Client\Response\Domain\Filtering\FilteringPolicyResponse;
use ProvulusSDK\Client\Response\Domain\Filtering\Item\FilteringPolicyItemCollectionResponse;
use ProvulusSDK\Client\Response\Domain\Filtering\Item\FilteringPolicyItemResponse;
use ProvulusSDK\Config\ProvulusConfiguration;
use ProvulusSDK\Enum\Filtering\FilteringPolicyEnum;
use ProvulusSDK\Enum\Filtering\FilteringPolicyItemEnum;
use ProvulusTest\TestCase;

/**
 * Class ReadFilteringPolicyItemTest
 *
 * Tests for FilteringPolicyItems of type all
 *
 * @package automated\Requests\Domain\Filtering\Item
 */
class FilteringPolicyItemTest extends TestCase
{

    /**
     * @test
     **/
    public function read_policy_item_success()
    {
        $jsonResult
            = '{"data":{"type":"filtering-policy-item","id":"403","attributes":{"domain_group_id":3122,"value_type_code":"address","value":"allo@zozo.com","is_active":true,"is_active_backup":null,"created_at":"2016-11-30 20:31:00.000000 +00:00","updated_at":"2016-11-30 20:31:00.000000 +00:00","domain_id":4794}}}';
        $jsonRequest
            = '{}';

        $config = $this->preSuccess($jsonResult);

        $request = new ReadFilteringPolicyItemRequest();
        $request->setOrganizationId(2865)
                ->setFilteringId(3122)
                ->setFilterItemId(403)
                ->setRequestType(FilteringPolicyItemRequestTypeEnum::ALL());

        $client = new ProvulusClient($config);

        /**
         * @var $response FilteringPolicyItemResponse
         */
        $response = $client->processRequest($request);

        $this->validateRequest($config, $jsonRequest);

        self::assertEquals(403, $response->id(), 'Filtering policy item id is incorrect.');
        self::assertTrue($response->is_active, 'Filtering policy is active is not correct.');
        self::assertEquals('allo@zozo.com', $response->value, 'Filtering policy item value is not correct.');
        self::assertEquals(FilteringPolicyItemEnum::ADDRESS(), $response->value_type_code, 'Filtering policy item type is not correct.');
    }

    /**
     * @test
     **/
    public function filtering_policy_item_index_success()
    {
        $jsonRequest = '{}';
        $jsonResponse
                     = ' {
 	"data": [{
 		"type": "filtering-policy-item",
 		"id": "414",
 		"attributes": {
 			"domain_group_id": 3157,
 			"value_type_code": "address",
 			"value": "nono@nono.com",
 			"is_active": true,
 			"is_active_backup": null,
 			"created_at": "2017-01-10 16:03:55.000000 +00:00",
 			"updated_at": null,
 			"domain_id": null
 		}
 	}, {
 		"type": "filtering-policy-item",
 		"id": "415",
 		"attributes": {
 			"domain_group_id": 3157,
 			"value_type_code": "address",
 			"value": "blu@blublu.com",
 			"is_active": true,
 			"is_active_backup": null,
 			"created_at": "2017-01-10 16:04:20.000000 +00:00",
 			"updated_at": null,
 			"domain_id": null
 		}
 	}],
 	"meta": {
 		"pagination": {
 			"total": 2,
 			"count": 2,
 			"per_page": 15,
 			"current_page": 1,
 			"total_pages": 1,
 			"links": []
 		}
 	}
 }';

        $config = $this->preSuccess($jsonResponse);

        $request = new IndexFilteringPolicyItemRequest();
        $request->setOrganizationId(2865)
                ->setFilteringId(3122)
                ->setRequestType(FilteringPolicyItemRequestTypeEnum::ALL());

        $client = new ProvulusClient($config);
        $client->processRequest($request);

        $collectionResponse = $request->getResponse();

        /**
         * @var FilteringPolicyResponse $filteringPolicyItemResponse
         */
        $filteringPolicyItemResponse = $collectionResponse->data()->first();

        $this->validateRequest($config, $jsonRequest);
        self::assertEquals(3157, $filteringPolicyItemResponse->get('domain_group_id'));
        self::assertEquals('nono@nono.com', $filteringPolicyItemResponse->get('value'));
    }

    /**
     * @test
     **/
    public function delete_filtering_policy_item_success()
    {
        $jsonRequest = '{}';
        $jsonResult  = '{}';

        $config = $this->preSuccess($jsonResult);

        $request = new DeleteFilteringPolicyItemRequest();
        $request->setOrganizationId(2865)
                ->setFilteringId(3122)
                ->setFilterItemId(403)
                ->setRequestType(FilteringPolicyItemRequestTypeEnum::ALL());

        $client = new ProvulusClient($config);

        $client->processRequest($request);

        $this->validateRequest($config, $jsonRequest);
    }

    /** @test */
    public function bulk_delete_policy_item_success()
    {
        $jsonResult = '{}';
        $jsonRequest = '{"ids": [3, 14, 15, 926, 535]}';

        $config = $this->preSuccess($jsonResult);

        $request = new BulkDeleteFilteringPolicyItemRequest();
        $request->setOrganizationId(2865)
                ->setFilteringId(3122)
                ->setRequestType(FilteringPolicyItemRequestTypeEnum::ALL())
                ->setIds([3, 14, 15, 926, 535]);

        $client = new ProvulusClient($config);

        /**
         * @var $response FilteringPolicyItemResponse
         */
        $client->processRequest($request);
        $this->validateRequest($config, $jsonRequest);
    }

    /** @test */
    public function all_filtering_policy_item_success()
    {
        $jsonResult = '{
            "data": [{
                    "type": "filtering-policy-item",
                    "id": "1232",
                    "attributes": {
                        "domain_group_id": 1719,
                        "value_type_code": "domain",
                        "value": "google.es",
                        "domain_id": 5598,
                        "created_at": "2017-10-24 20:01:15.000000 +00:00",
                        "updated_at": "2017-10-24 20:01:15.000000 +00:00"
                    }
                },{
                    "type": "filtering-policy-item",
                    "id": "1243",
                    "attributes": {
                        "domain_group_id": 3717,
                        "value_type_code": "address",
                        "value": "tre@zerospam.ca",
                        "domain_id": 1118,
                        "created_at": "2018-01-02 16:42:43.000000 +00:00",
                        "updated_at": "2018-01-02 16:42:43.000000 +00:00"
                    }
                },{
                    "type": "filtering-policy-item",
                    "id": "993",
                    "attributes": {
                        "domain_group_id": 3717,
                        "value_type_code": "domain",
                        "value": "demo.zerospam.ca",
                        "domain_id": 1176,
                        "created_at": "2017-07-18 14:27:54.000000 +00:00",
                        "updated_at": "2018-01-02 18:45:00.000000 +00:00"
                    }
                },{
                    "type": "filtering-policy-item",
                    "id": "1231",
                    "attributes": {
                        "domain_group_id": 3717,
                        "value_type_code": "domain",
                        "value": "banana.ca",
                        "domain_id": null,
                        "created_at": "2017-10-24 19:58:58.000000 +00:00",
                        "updated_at": "2018-01-02 18:45:00.000000 +00:00"
                    }
                },{
                    "type": "filtering-policy-item",
                    "id": "995",
                    "attributes": {
                        "domain_group_id": 1121,
                        "value_type_code": "domain",
                        "value": "emailsecurity.ca",
                        "domain_id": 1503,
                        "created_at": "2017-07-18 14:27:54.000000 +00:00",
                        "updated_at": "2017-07-18 14:27:54.000000 +00:00"
                    }
                },{
                    "type": "filtering-policy-item",
                    "id": "1",
                    "attributes": {
                        "domain_group_id": 1121,
                        "value_type_code": "domain",
                        "value": "zerospam.ca",
                        "domain_id": 5537,
                        "created_at": null,
                        "updated_at": "2017-08-18 15:41:11.000000 +00:00"
                    }
                }],
            "meta": {
                "pagination": {
                    "total": 6,
                    "count": 6,
                    "per_page": 15,
                    "current_page": 1,
                    "total_pages": 1
                }
            }
        }';
        $jsonRequest = '{}';

        $config = $this->preSuccess($jsonResult);

        $request = (new AllFilteringPolicyItemRequest)
            ->setOrganizationId(7895)
            ->setFilterType(FilteringPolicyEnum::OUTBOUND())
            ->setRequestType(FilteringPolicyItemRequestTypeEnum::DOMAIN());

        
        $client = new ProvulusClient($config);

        /** @var FilteringPolicyItemCollectionResponse $response */
        $response = $client->processRequest($request);

        $this->validateRequest($config, $jsonRequest);

        $this->validateRequestUrl($config, ['/orgs/7895/policyitemsdomain']);

        $this->assertEquals(6, $response->getMetaData()->total);

        /** @var FilteringPolicyItemResponse $fpi */
        $fpi = $response->data()->first();

        $this->assertEquals("google.es", $fpi->value);
        $this->assertEquals(5598, $fpi->domain_id);
    }

    /** @test */
    public function move_to_org_filtering_policy_items_success()
    {
        $jsonResult = '{}';
        $jsonRequest = '{
            "organization_id": 1566,
            "filtering_policy_name": "Moved filtering policy items",
            "filter_type_code": "inbound",
            "ids": [4901, 5328, 6060, 6512]
        }';

        $config = $this->preSuccess($jsonResult);

        $request = (new MoveToOrganizationFilteringPolicyItemRequest)
            ->setOrganizationId(5772)
            ->setTargetOrganizationId(1566)
            ->setFilterTypeCode(FilteringPolicyEnum::INBOUND())
            ->setFilteringPolicyName("Moved filtering policy items")
            ->setIds([4901, 5328, 6060, 6512]);


        $client = new ProvulusClient($config);

        $client->processRequest($request);

        $this->validateRequest($config, $jsonRequest);

        $this->validateRequestUrl($config, ['/orgs/5772/policyitems/movetoorg']);
    }

    /** @test */
    public function move_to_policy_filtering_policy_items_success()
    {
        $jsonResult = '{}';
        $jsonRequest = '{
            "filtering_policy_id": 5080,
            "ids": [7568, 8772, 9352]
        }';

        $config = $this->preSuccess($jsonResult);

        $request = (new MoveToPolicyFilteringPolicyItemRequest)
            ->setOrganizationId(7320)
            ->setTargetFilteringPolicyId(5080)
            ->setIds([7568, 8772, 9352]);


        $client = new ProvulusClient($config);

        $client->processRequest($request);

        $this->validateRequest($config, $jsonRequest);

        $this->validateRequestUrl($config, ['/orgs/7320/policyitems/movetopolicy']);
    }

    /**
     * Gets config for tests
     *
     * @param MockHandler $handler
     *
     * @return ProvulusConfiguration
     */
    protected function getConfig(MockHandler $handler)
    {
        return parent::getConfig($handler)->setApiKey('test');
    }
}