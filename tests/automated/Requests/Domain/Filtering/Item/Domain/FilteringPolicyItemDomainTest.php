<?php
/**
 * Created by PhpStorm.
 * User: pbb
 * Date: 23/12/16
 * Time: 8:54 AM
 */

namespace automated\Requests\Domain\Filtering\Item\Domain;

use GuzzleHttp\Handler\MockHandler;
use ProvulusSDK\Client\ProvulusClient;
use ProvulusSDK\Client\Request\Domain\Filtering\Item\CreateFilteringPolicyItemRequest;
use ProvulusSDK\Client\Request\Domain\Filtering\Item\DeleteFilteringPolicyItemRequest;
use ProvulusSDK\Client\Request\Domain\Filtering\Item\IndexFilteringPolicyItemRequest;
use ProvulusSDK\Client\Request\Domain\Filtering\Item\ReadFilteringPolicyItemRequest;
use ProvulusSDK\Client\Request\Resource\Domain\Filtering\Item\FilteringPolicyItemRequestTypeEnum;
use ProvulusSDK\Client\Response\Domain\Filtering\Item\FilteringPolicyItemResponse;
use ProvulusSDK\Config\ProvulusConfiguration;
use ProvulusSDK\Enum\Filtering\FilteringPolicyItemEnum;
use ProvulusTest\TestCase;

/**
 * Class CreateFilteringPolicyItemDomainTest
 *
 * Tests for FilteringPolicyItems of type Domain
 *
 * @package automated\Requests\Domain\Filtering\Item\Domain
 */
class FilteringPolicyItemDomainTest extends TestCase
{

    /**
     * @test
     **/
    public function create_filtering_policy_item_domain_success()
    {
        $jsonResult
            = '{"data":{"type":"filtering-policy-item","id":"414","attributes":{"value_type_code":"domain","value":"hodor1.com","domain_group_id":3148,"updated_at":"2016-12-23 15:52:49.000000 +00:00","created_at":"2016-12-23 15:52:49.000000 +00:00"}}}';
        $jsonRequest
            = '{"value":"hodor1.com"}';

        $config = $this->preSuccess($jsonResult);

        $request = new CreateFilteringPolicyItemRequest();
        $request->setValue('hodor1.com')
                ->setOrganizationId(2888)
                ->setFilteringId(3148)
                ->setRequestType(FilteringPolicyItemRequestTypeEnum::DOMAIN());

        $client = new ProvulusClient($config);
        /**
         * @var $response FilteringPolicyItemResponse
         */
        $response = $client->processRequest($request);

        $this->validateRequest($config, $jsonRequest);

        $this->validateRequestUrl($config, ['/orgs/2888/policies/3148/policyitemsdomain']);

        self::assertEquals(414, $response->id(), 'Filtering Policy item address id is incorrect.');
        self::assertEquals('hodor1.com', $response->value, 'Filtering policy item address value is not correct.');
    }


    /**
     * @test
     **/
    public function read_filtering_policy_item_domain()
    {
        $jsonResult
            = '{"data":{"type":"filtering-policy-item","id":"46","attributes":{"domain_group_id":1735,"value_type_code":"domain","value":"zozo.com","is_active":true,"is_active_backup":null,"created_at":null,"updated_at":null,"domain_id":null}}}';
        $jsonRequest
            = '{}';

        $config = $this->preSuccess($jsonResult);

        $request = new ReadFilteringPolicyItemRequest();
        $request->setOrganizationId(836)
                ->setFilteringId(1735)
                ->setFilterItemId(46)
                ->setRequestType(FilteringPolicyItemRequestTypeEnum::DOMAIN());

        $client = new ProvulusClient($config);

        /**
         * @var $response FilteringPolicyItemResponse
         */
        $response = $client->processRequest($request);

        $this->validateRequest($config, $jsonRequest);
        self::assertEquals(46, $response->id(), 'Filtering policy item id is incorrect.');
        self::assertTrue($response->is_active, 'Filtering policy is active is not correct.');
        self::assertEquals('zozo.com', $response->value, 'Filtering policy item value is not correct.');
        self::assertEquals(FilteringPolicyItemEnum::DOMAIN(), $response->value_type_code, 'Filtering policy item type is not correct.');
    }


    /**
     * @test
     **/
    public function index_filtering_policy_item_domain_success()
    {
        $jsonRequest = '{}';
        $jsonResponse
                     = '
        {  
           "data":[  
              {  
                 "type":"filtering-policy-item",
                 "id":"416",
                 "attributes":{  
                    "domain_group_id":3157,
                    "value_type_code":"domain",
                    "value":"asdf.nono.com",
                    "is_active":true,
                    "is_active_backup":null,
                    "created_at":"2017-01-26 18:34:19.000000 +00:00",
                    "updated_at":"2017-01-26 18:34:19.000000 +00:00",
                    "domain_id":null
                 }
              }
           ],
           "meta":{  
              "pagination":{  
                 "total":1,
                 "count":1,
                 "per_page":15,
                 "current_page":1,
                 "total_pages":1,
                 "links":[  
        
                 ]
              }
           }
        }';

        $config = $this->preSuccess($jsonResponse);

        $request = new IndexFilteringPolicyItemRequest();
        $request->setOrganizationId(2865)
                ->setFilteringId(3122)
                ->setRequestType(FilteringPolicyItemRequestTypeEnum::DOMAIN());

        $client = new ProvulusClient($config);
        $client->processRequest($request);

        $collectionResponse = $request->getResponse();

        /**
         * @var FilteringPolicyItemResponse $filteringPolicyItemResponse
         */
        $filteringPolicyItemResponse = $collectionResponse->data()->first();

        $this->validateRequest($config, $jsonRequest);

        self::assertEquals(1, $collectionResponse->getMetaData()->count);
        self::assertTrue(is_null($filteringPolicyItemResponse->get('domain_id')));
        self::assertEquals(FilteringPolicyItemEnum::DOMAIN(), $filteringPolicyItemResponse->get('value_type_code'));
        self::assertEquals('asdf.nono.com', $filteringPolicyItemResponse->get('value'));
    }

    /**
     * @test
     **/
    public function delete_filtering_policy_item_domain_success()
    {
        $jsonRequest = '{}';
        $jsonResult  = '{}';

        $config = $this->preSuccess($jsonResult);

        $request = new DeleteFilteringPolicyItemRequest();
        $request->setOrganizationId(2865)
                ->setFilteringId(3122)
                ->setFilterItemId(413)
                ->setRequestType(FilteringPolicyItemRequestTypeEnum::DOMAIN());

        $client = new ProvulusClient($config);

        $client->processRequest($request);

        $this->validateRequest($config, $jsonRequest);
    }

    /**
     * Gets config for tests
     *
     * @param MockHandler $handler
     *
     * @return ProvulusConfiguration
     */
    protected function getConfig(MockHandler $handler)
    {
        return parent::getConfig($handler)->setApiKey('test');
    }
}