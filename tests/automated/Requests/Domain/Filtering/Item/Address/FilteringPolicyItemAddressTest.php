<?php
/**
 * Created by PhpStorm.
 * User: pbb
 * Date: 22/12/16
 * Time: 3:41 PM
 */

namespace automated\Requests\Domain\Filtering\Item\Address;

use GuzzleHttp\Handler\MockHandler;
use ProvulusSDK\Client\ProvulusClient;
use ProvulusSDK\Client\Request\Domain\Filtering\Item\CreateFilteringPolicyItemRequest;
use ProvulusSDK\Client\Request\Domain\Filtering\Item\DeleteFilteringPolicyItemRequest;
use ProvulusSDK\Client\Request\Domain\Filtering\Item\IndexFilteringPolicyItemRequest;
use ProvulusSDK\Client\Request\Domain\Filtering\Item\ReadFilteringPolicyItemRequest;
use ProvulusSDK\Client\Request\Resource\Domain\Filtering\Item\FilteringPolicyItemRequestTypeEnum;
use ProvulusSDK\Client\Response\Domain\Filtering\Item\FilteringPolicyItemResponse;
use ProvulusSDK\Config\ProvulusConfiguration;
use ProvulusSDK\Enum\Filtering\FilteringPolicyItemEnum;
use ProvulusTest\TestCase;

/**
 * Class CreateFilteringPolicyItemAddressTest
 *
 * Tests for Filtering Policy Items of type address
 *
 * @package automated\Requests\Domain\Filtering\Item\Address
 */
class FilteringPolicyItemAddressTest extends TestCase
{
    /**
     * @test
     **/
    public function create_filtering_policy_item_address_success()
    {
        $jsonResponse
            = '
            {
                  "data":{
                  "type":"filtering-policy-item",
                  "id":"413",
                  "attributes":{
                      "value_type_code":"address",
                      "value":"allo@zozo.com",
                      "domain_id":4794,
                      "domain_group_id":3122,
                      "updated_at":"2016-12-22 20:50:32.000000 +00:00",
                      "created_at":"2016-12-22 20:50:32.000000 +00:00"
                      }
                  }
            }';

        $jsonRequest = '{"value":"allo@zozo.com"}';

        $config = $this->preSuccess($jsonResponse);

        $request = new CreateFilteringPolicyItemRequest();
        $request->setValue('allo@zozo.com')
                ->setOrganizationId(2865)
                ->setFilteringId(3122)
                ->setRequestType(FilteringPolicyItemRequestTypeEnum::ADDRESS());

        $client = new ProvulusClient($config);
        /**
         * @var $response FilteringPolicyItemResponse
         */
        $response = $client->processRequest($request);

        $this->validateRequest($config, $jsonRequest);

        self::assertEquals(413, $response->id(), 'Filtering Policy item address id is incorrect.');
        self::assertEquals('allo@zozo.com', $response->value, 'Filtering policy item address value is not correct.');
        self::assertTrue(4794 === $response->domain_id, 'Filtering policy item address domain_id is not correct.');
    }


    /**
     * @test
     **/
    public function read_filtering_policy_item_address_success()
    {
        $jsonResult
            = '{"data":{"type":"filtering-policy-item","id":"413","attributes":{"domain_group_id":3122,"value_type_code":"address","value":"allo@zozo.com","is_active":true,"is_active_backup":null,"created_at":"2016-12-22 20:50:32.000000 +00:00","updated_at":"2016-12-22 20:50:32.000000 +00:00","domain_id":4794}}}';
        $jsonRequest
            = '{}';

        $config = $this->preSuccess($jsonResult);

        $request = new ReadFilteringPolicyItemRequest();
        $request->setOrganizationId(2865)
                ->setFilteringId(3122)
                ->setFilterItemId(413)
                ->setRequestType(FilteringPolicyItemRequestTypeEnum::ADDRESS());

        $client = new ProvulusClient($config);

        /**
         * @var $response FilteringPolicyItemResponse
         */
        $response = $client->processRequest($request);

        $this->validateRequest($config, $jsonRequest);

        self::assertEquals(413, $response->id(), 'Filtering policy item id is incorrect.');
        self::assertTrue($response->is_active, 'Filtering policy is active is not correct.');
        self::assertEquals('allo@zozo.com', $response->value, 'Filtering policy item value is not correct.');
        self::assertEquals(FilteringPolicyItemEnum::ADDRESS(), $response->value_type_code, 'Filtering policy item type is not correct.');
    }


    /**
     * @test
     **/
    public function index_filtering_policy_item_address_success()
    {
        $jsonRequest = '{}';
        $jsonResponse
                     = ' {
                "data": [{
                    "type": "filtering-policy-item",
                    "id": "414",
                    "attributes": {
                        "domain_group_id": 3157,
                        "value_type_code": "address",
                        "value": "nono@nono.com",
                        "is_active": true,
                        "is_active_backup": null,
                        "created_at": "2017-01-10 16:03:55.000000 +00:00",
                        "updated_at": null,
                        "domain_id": null
                    }
                }, {
                    "type": "filtering-policy-item",
                    "id": "415",
                    "attributes": {
                        "domain_group_id": 3157,
                        "value_type_code": "address",
                        "value": "blu@blublu.com",
                        "is_active": true,
                        "is_active_backup": null,
                        "created_at": "2017-01-10 16:04:20.000000 +00:00",
                        "updated_at": null,
                        "domain_id": null
                    }
                }],
                "meta": {
                    "pagination": {
                        "total": 2,
                        "count": 2,
                        "per_page": 15,
                        "current_page": 1,
                        "total_pages": 1,
                        "links": []
                    }
                }
             }';

        $config = $this->preSuccess($jsonResponse);

        $request = new IndexFilteringPolicyItemRequest();
        $request->setOrganizationId(2865)
                ->setFilteringId(3122)
                ->setRequestType(FilteringPolicyItemRequestTypeEnum::ADDRESS());

        $client = new ProvulusClient($config);
        $client->processRequest($request);

        $collectionResponse = $request->getResponse();

        /**
         * @var FilteringPolicyItemResponse $filteringPolicyItemResponse
         */
        $filteringPolicyItemResponse = $collectionResponse->data()->first();
        $this->validateRequest($config, $jsonRequest);

        self::assertEquals(2, $collectionResponse->getMetaData()->count);
        self::assertTrue(is_null($filteringPolicyItemResponse->get('updated_at')));
    }


    /**
     * @test
     **/
    public function delete_filtering_policy_item_address()
    {
        $jsonRequest = '{}';
        $jsonResult  = '{}';

        $config = $this->preSuccess($jsonResult);

        $request = new DeleteFilteringPolicyItemRequest();
        $request->setOrganizationId(2865)
                ->setFilteringId(3122)
                ->setFilterItemId(413)
                ->setRequestType(FilteringPolicyItemRequestTypeEnum::ADDRESS());

        $client = new ProvulusClient($config);

        $client->processRequest($request);
        $this->validateRequest($config, $jsonRequest);
    }

    /**
     * Gets config for tests
     *
     * @param MockHandler $handler
     *
     * @return ProvulusConfiguration
     */
    protected function getConfig(MockHandler $handler)
    {
        return parent::getConfig($handler)->setApiKey('test');
    }

}