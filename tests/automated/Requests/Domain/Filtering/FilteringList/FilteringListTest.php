<?php
/**
 * Created by PhpStorm.
 * User: ycoutu
 * Date: 30/08/17
 * Time: 10:45 AM
 */

namespace automated\Requests\Domain\Filtering\FilteringList;

use ProvulusSDK\Client\ProvulusClient;
use ProvulusSDK\Client\Request\Domain\Filtering\AllFilteringListsRequest;
use ProvulusSDK\Client\Request\Domain\Filtering\FilteringList\Bulk\BulkCreateFilteringListRequest;
use ProvulusSDK\Client\Request\Domain\Filtering\FilteringList\Bulk\BulkDeleteFilteringListRequest;
use ProvulusSDK\Client\Request\Domain\Filtering\FilteringList\CreateFilteringListRequest;
use ProvulusSDK\Client\Request\Domain\Filtering\FilteringList\DeleteFilteringListRequest;
use ProvulusSDK\Client\Request\Domain\Filtering\FilteringList\IndexFilteringListRequest;
use ProvulusSDK\Client\Request\Domain\Filtering\FilteringList\ReadFilteringListRequest;
use ProvulusSDK\Client\Request\Domain\Filtering\FilteringList\UpdateFilteringListRequest;
use ProvulusSDK\Client\Request\Resource\Domain\Filtering\FilteringList\FilteringListRequestTypeEnum;
use ProvulusSDK\Client\Response\Domain\Filtering\FilteringList\AllFilteringListsResponse;
use ProvulusSDK\Client\Response\Domain\Filtering\FilteringList\FilteringListCollectionResponse;
use ProvulusSDK\Client\Response\Domain\Filtering\FilteringList\FilteringListResponse;
use ProvulusSDK\Enum\Filtering\FilteringListActionEnum;
use ProvulusSDK\Utils\FilteringList\FilteringListObject;
use ProvulusTest\TestCaseApiKey;

class FilteringListTest extends TestCaseApiKey
{
    /** @test */
    public function create_filtering_list_success()
    {
        $jsonRequest = '{
	        "value": "10.0.0.0/8",
	        "description": "Block this range",
	        "action": "block"
        }';
        $jsonResult = '{
            "data": {
                "type": "filtering-list-ip",
                "id": "120500",
                "attributes": {
                    "is_enabled": true,
                    "organization_id": 27182,
                    "domain_group_id": 81828,
                    "value": "10.0.0.0/8",
                    "description": "Block this range",
                    "updated_at": "2017-08-30 15:04:31.144935 +00:00",
                    "created_at": "2017-08-30 15:04:31.144935 +00:00",
                    "action": "block"
                }
            }
        }';

        $config = $this->preSuccess($jsonResult);
        $request = new CreateFilteringListRequest();

        $request->setOrganizationId(27182)
                ->setFilteringId(81828)
                ->setFilteringListType(FilteringListRequestTypeEnum::IP())
                ->setValue("10.0.0.0/8")
                ->setAction(FilteringListActionEnum::BLOCK())
                ->setDescription("Block this range");

        $client = new ProvulusClient($config);

        /** @var FilteringListResponse $response */
        $response = $client->processRequest($request);

        $this->validateRequest($config, $jsonRequest);

        self::assertEquals("10.0.0.0/8", $response->value, "The value is not as expected");
        self::assertTrue($response->is_enabled, "The filtering list is not enabled");
        self::assertTrue($response->action->is(FilteringListActionEnum::BLOCK()), "The list type is not as expected");
        self::assertEquals("Block this range", $response->description, "The description is not as expected");

        $this->validateUrl($config, 'orgs/27182/policies/81828/ips');
    }

    /** @test */
    public function delete_filtering_list_success()
    {
        $jsonRequest = '{}';
        $jsonResult  = '{}';

        $config = $this->preSuccess($jsonResult);

        $request = new DeleteFilteringListRequest();
        $request->setOrganizationId(12020)
            ->setFilteringId(5690)
            ->setFilteringListId(31595)
            ->setFilteringListType(FilteringListRequestTypeEnum::INBOUND_RECIPIENT());

        $client = new ProvulusClient($config);
        $client->processRequest($request);
        $this->validateRequest($config, $jsonRequest);

        $this->validateUrl($config, 'orgs/12020/policies/5690/in-rcpts/31595');
    }

    /** @test */
    public function index_filtering_list_success()
    {
        $jsonRequest = '{}';
        $jsonResult = '{
            "data": [{
                "type": "filtering-list-file-extension",
                "id": "120477",
                "attributes": {
                    "domain_group_id": 926535,
                    "organization_id": 31415,
                    "is_enabled": true,
                    "value": "lwl",
                    "description": "my description is good",
                    "created_at": "2017-08-23 18:58:39.381050 +00:00",
                    "updated_at": "2017-08-23 18:58:39.381050 +00:00",
                    "action": "allow"
                }
            }, {
                "type": "filtering-list-file-extension",
                "id": "120476",
                "attributes": {
                    "domain_group_id": 926535,
                    "organization_id": 31415,
                    "is_enabled": true,
                    "value": "gif",
                    "description": null,
                    "created_at": "2017-08-23 18:12:05.062769 +00:00",
                    "updated_at": "2017-08-23 18:12:05.062769 +00:00",
                    "action": "allow"
                }
            }, {
                "type": "filtering-list-file-extension",
                "id": "120475",
                "attributes": {
                    "domain_group_id": 926535,
                    "organization_id": 31415,
                    "is_enabled": true,
                    "value": "jpg",
                    "description": null,
                    "created_at": "2017-08-23 17:41:21.924446 +00:00",
                    "updated_at": "2017-08-23 17:41:21.924446 +00:00",
                    "action": "allow"
                }
            }, {
                "type": "filtering-list-file-extension",
                "id": "120474",
                "attributes": {
                    "domain_group_id": 926535,
                    "organization_id": 31415,
                    "is_enabled": true,
                    "value": "png",
                    "description": null,
                    "created_at": "2017-08-23 15:44:47.628938 +00:00",
                    "updated_at": "2017-08-23 15:44:47.628938 +00:00",
                    "action": "allow"
                }
            }, {
                "type": "filtering-list-file-extension",
                "id": "120446",
                "attributes": {
                    "domain_group_id": 926535,
                    "organization_id": 31415,
                    "is_enabled": true,
                    "value": "test00",
                    "description": null,
                    "created_at": "2017-08-22 14:22:33.096685 +00:00",
                    "updated_at": "2017-08-22 14:22:33.096685 +00:00",
                    "action": "allow"
                }
            }, {
                "type": "filtering-list-file-extension",
                "id": "120424",
                "attributes": {
                    "domain_group_id": 926535,
                    "organization_id": 31415,
                    "is_enabled": true,
                    "value": "ok",
                    "description": null,
                    "created_at": "2017-08-22 13:03:24.936442 +00:00",
                    "updated_at": "2017-08-22 13:03:24.936442 +00:00",
                    "action": "allow"
                }
            }, {
                "type": "filtering-list-file-extension",
                "id": "120338",
                "attributes": {
                    "domain_group_id": 926535,
                    "organization_id": 31415,
                    "is_enabled": true,
                    "value": "exe",
                    "description": null,
                    "created_at": "2017-08-17 19:24:05.298903 +00:00",
                    "updated_at": "2017-08-18 18:22:47.063153 +00:00",
                    "action": "allow"
                }
            }]
        }';

        $config = $this->preSuccess($jsonResult);

        $request = new IndexFilteringListRequest();

        $request->setOrganizationId(31415)
            ->setFilteringId(926535)
            ->setFilteringListType(FilteringListRequestTypeEnum::MIME())
            ->setFilteringListAction(FilteringListActionEnum::ALLOW());

        $client = new ProvulusClient($config);

        /** @var FilteringListCollectionResponse $response */
        $response = $client->processRequest($request);
        /** @var FilteringListResponse $wlResponse */
        $wlResponse = $response->data()->first();
        $this->validateRequest($config, $jsonRequest);

        $this->validateRequestUrl($config, ['orgs/31415/policies/926535/mimes', 'filtering_list_action=allow']);
        self::assertEquals("lwl", $wlResponse->value);
    }

    /** @test */
    public function read_filtering_list_success()
    {
        $jsonRequest = '{}';
        $jsonResult = '{
            "data": {
                "type": "filtering-list-outbound-sender",
                "id": "749894",
                "attributes": {
                    "domain_group_id": 33988,
                    "organization_id": 16180,
                    "is_enabled": false,
                    "value": "test@domaine.com",
                    "description": "test",
                    "created_at": "2017-08-22 13:57:54.386071 +00:00",
                    "updated_at": "2017-08-22 13:57:54.386071 +00:00",
                    "action": "allow"
                }
            }
        }';

        $config = $this->preSuccess($jsonResult);

        $request = new ReadFilteringListRequest();
        $request->setOrganizationId(16180)
                ->setFilteringId(33988)
                ->setFilteringListId(749894)
                ->setFilteringListType(FilteringListRequestTypeEnum::OUTBOUND_SENDER());

        $client = new ProvulusClient($config);
        /** @var FilteringListResponse $response */
        $response = $client->processRequest($request);
        $this->validateRequest($config, $jsonRequest);

        $this->validateUrl($config, 'orgs/16180/policies/33988/out-snds/749894');

        self::assertEquals("test", $response->description);
    }

    /** @test */
    public function update_filtering_list_success()
    {
        $jsonRequest = '{
            "is_enabled": false,
            "description": null
        }';
        $jsonResult = '{
            "data": {
                "type": "filtering-list-authorized-address",
                "id": "6237",
                "attributes": {
                    "domain_group_id": 2135,
                    "organization_id": 1414,
                    "is_enabled": false,
                    "value": "test@domaine.com",
                    "description": null,
                    "created_at": "2017-08-22 13:57:54.386071 +00:00",
                    "updated_at": "2017-08-22 13:57:54.386071 +00:00",
                    "action": "restrict"
                }
            }
        }';

        $config = $this->preSuccess($jsonResult);

        $request = new UpdateFilteringListRequest();

        $request->setOrganizationId(1414)
                ->setFilteringId(2135)
                ->setFilteringListId(6237)
                ->setFilteringListType(FilteringListRequestTypeEnum::AUTHORIZED_ADDRESS())
                ->setIsEnabled(false)
                ->setDescription(null);

        $client = new ProvulusClient($config);
        /** @var FilteringListResponse $response */
        $response = $client->processRequest($request);
        $this->validateRequest($config, $jsonRequest);

        $this->validateUrl($config, 'orgs/1414/policies/2135/auth-addrs/6237');

        self::assertNull($response->description);
        self::assertFalse($response->is_enabled);
        self::assertTrue($response->action->is(FilteringListActionEnum::RESTRICT()));
    }

    /** @test */
    public function bulk_delete_filtering_list_success()
    {
        $jsonRequest = '{
            "ids": [577, 2156, 649]
        }';
        $jsonResult = '{}';

        $config = $this->preSuccess($jsonResult);

        $request = new BulkDeleteFilteringListRequest();

        $request->setOrganizationId(1970)
                ->setFilteringId(1231)
                ->setFilteringListType(FilteringListRequestTypeEnum::FILE_EXTENSION())
                ->setIds([577, 2156, 649]);

        $client = new ProvulusClient($config);
        $client->processRequest($request);
        $this->validateRequest($config, $jsonRequest);

        $this->validateUrl($config, 'orgs/1970/policies/1231/file-exts');
    }

    /** @test */
    public function bulk_create_filtering_list_success()
    {
        $jsonRequest = '{
            "filtering_list": [{
                    "value": "domain.com",
                    "is_enabled": false,
                    "action": "allow"
                }, {
                    "value": "email@address.com",
                    "description": "This is in the block list",
                    "action": "block"
                }]
        }';
        $jsonResult  = '{
            "data": [{
                "type": "filtering-list-inbound-sender",
                "id": "120501",
                "attributes": {
                    "is_enabled": false,
                    "organization_id": 567,
                    "domain_group_id": 1432,
                    "value": "domain.com",
                    "description": null,
                    "updated_at": "2017-08-30 19:08:15.513383 +00:00",
                    "created_at": "2017-08-30 19:08:15.513383 +00:00",
                    "action": "allow"
                }
            }, {
                "type": "filtering-list-inbound-sender",
                "id": "120502",
                "attributes": {
                    "is_enabled": true,
                    "organization_id": 567,
                    "domain_group_id": 1432,
                    "value": "email@address.com",
                    "description": "This is in the block list",
                    "updated_at": "2017-08-30 19:08:15.521828 +00:00",
                    "created_at": "2017-08-30 19:08:15.521828 +00:00",
                    "action": "block"
                }
            }]
        }';

        $config = $this->preSuccess($jsonResult);

        $request = new BulkCreateFilteringListRequest();
        $request->setOrganizationId(567)
                ->setFilteringId(1432)
                ->setFilteringListType(FilteringListRequestTypeEnum::INBOUND_SENDER());

        $wl1 = new FilteringListObject();
        $wl1->setValue("domain.com")
            ->setIsEnabled(false)
            ->setAction(FilteringListActionEnum::ALLOW());

        $wl2 = new FilteringListObject();
        $wl2->setValue("email@address.com")
            ->setAction(FilteringListActionEnum::BLOCK())
            ->setDescription("This is in the block list");

        $request->addFilteringList($wl1);
        $request->addFilteringList($wl2);

        $client = new ProvulusClient($config);
        /** @var FilteringListCollectionResponse $response */
        $response = $client->processRequest($request);
        $wlResponse = $response->data()->first();
        $this->validateRequest($config, $jsonRequest);

        $this->validateUrl($config, 'orgs/567/policies/1432/in-snds/bulkcreate');

        self::assertNull($wlResponse->description);
        self::assertEquals('domain.com', $wlResponse->value);
    }

    /** @test */
    public function show_all_filtering_lists()
    {
        $jsonRequest = '{}';

        $jsonResult = '{
  "data": {
    "type": "filtering-lists",
    "id": "",
    "attributes": {
      "in-snds": {
        "allow": {
          "data": [
            {
              "type": "filtering-list-inbound-sender",
              "id": "117946",
              "attributes": {
                "domain_group_id": 1,
                "organization_id": 1,
                "is_enabled": true,
                "value": "web-site.com",
                "description": "Auto added by xxxxx.",
                "created_at": "2017-01-26 16:18:56.000000 +00:00",
                "updated_at": "2017-01-26 16:18:56.000000 +00:00",
                "action": "allow"
              }
            },
            {
              "type": "filtering-list-inbound-sender",
              "id": "111984",
              "attributes": {
                "domain_group_id": 1,
                "organization_id": 1,
                "is_enabled": true,
                "value": "some.site.com",
                "description": "Auto added by xxxx.",
                "created_at": "2016-09-30 12:49:16.000000 +00:00",
                "updated_at": "2016-09-30 12:49:16.000000 +00:00",
                "action": "allow"
              }
            },
            {
              "type": "filtering-list-inbound-sender",
              "id": "111478",
              "attributes": {
                "domain_group_id": 1,
                "organization_id": 1,
                "is_enabled": true,
                "value": "spamassassin.apache.org",
                "description": "ML SpamAssassin",
                "created_at": "2016-09-12 12:50:37.000000 +00:00",
                "updated_at": "2016-09-12 12:50:37.000000 +00:00",
                "action": "allow"
              }
            },
            {
              "type": "filtering-list-inbound-sender",
              "id": "91486",
              "attributes": {
                "domain_group_id": 1,
                "organization_id": 1,
                "is_enabled": true,
                "value": "send.email.org",
                "description": "Auto added by xxxx.",
                "created_at": "2016-01-15 19:05:53.000000 +00:00",
                "updated_at": "2016-01-15 19:05:53.000000 +00:00",
                "action": "allow"
              }
            },
            {
              "type": "filtering-list-inbound-sender",
              "id": "91354",
              "attributes": {
                "domain_group_id": 1,
                "organization_id": 1,
                "is_enabled": true,
                "value": "international.ca",
                "description": "Blocage de xxxx non désiré",
                "created_at": "2016-01-11 15:22:53.000000 +00:00",
                "updated_at": "2016-01-11 15:22:53.000000 +00:00",
                "action": "allow"
              }
            },
            {
              "type": "filtering-list-inbound-sender",
              "id": "91353",
              "attributes": {
                "domain_group_id": 1,
                "organization_id": 1,
                "is_enabled": true,
                "value": "international.com",
                "description": "Blocage de xxxx non désiré",
                "created_at": "2016-01-11 15:22:52.000000 +00:00",
                "updated_at": "2016-01-11 15:22:52.000000 +00:00",
                "action": "allow"
              }
            },
            {
              "type": "filtering-list-inbound-sender",
              "id": "79984",
              "attributes": {
                "domain_group_id": 1,
                "organization_id": 1,
                "is_enabled": true,
                "value": "atlassian.net",
                "description": "Auto added by xxxx.",
                "created_at": "2015-03-05 20:06:43.000000 +00:00",
                "updated_at": "2015-03-05 20:06:43.000000 +00:00",
                "action": "allow"
              }
            },
            {
              "type": "filtering-list-inbound-sender",
              "id": "72395",
              "attributes": {
                "domain_group_id": 1,
                "organization_id": 1,
                "is_enabled": true,
                "value": "giason@zerospam.ca",
                "description": null,
                "created_at": "2014-07-28 18:47:14.000000 +00:00",
                "updated_at": "2014-07-28 18:47:14.000000 +00:00",
                "action": "allow"
              }
            },
            {
              "type": "filtering-list-inbound-sender",
              "id": "50581",
              "attributes": {
                "domain_group_id": 1,
                "organization_id": 1,
                "is_enabled": true,
                "value": "address.com",
                "description": "Ajout automatique par xxxx.",
                "created_at": "2013-03-01 16:23:50.000000 +00:00",
                "updated_at": "2013-03-01 16:23:50.000000 +00:00",
                "action": "allow"
              }
            },
            {
              "type": "filtering-list-inbound-sender",
              "id": "49997",
              "attributes": {
                "domain_group_id": 1,
                "organization_id": 1,
                "is_enabled": true,
                "value": "otterpop.fr",
                "description": "ajouté par xxxx - forum.",
                "created_at": "2013-02-13 15:16:32.000000 +00:00",
                "updated_at": "2013-02-13 15:16:32.000000 +00:00",
                "action": "allow"
              }
            }
          ]
        },
        "block": {
          "data": [
            {
              "type": "filtering-list-inbound-sender",
              "id": "111192",
              "attributes": {
                "domain_group_id": 1,
                "organization_id": 1,
                "is_enabled": true,
                "value": "corp-vus.com",
                "description": "award",
                "created_at": "2016-09-02 12:16:50.000000 +00:00",
                "updated_at": "2016-09-02 12:16:50.000000 +00:00",
                "action": "block"
              }
            },
            {
              "type": "filtering-list-inbound-sender",
              "id": "93688",
              "attributes": {
                "domain_group_id": 1,
                "organization_id": 1,
                "is_enabled": true,
                "value": "my.id",
                "description": "NDR storm",
                "created_at": "2016-03-25 15:12:59.000000 +00:00",
                "updated_at": "2016-03-25 15:12:59.000000 +00:00",
                "action": "block"
              }
            },
            {
              "type": "filtering-list-inbound-sender",
              "id": "89465",
              "attributes": {
                "domain_group_id": 1,
                "organization_id": 1,
                "is_enabled": true,
                "value": "spammer.com",
                "description": "Unsub",
                "created_at": "2015-10-23 21:36:53.000000 +00:00",
                "updated_at": "2015-10-23 21:36:53.000000 +00:00",
                "action": "block"
              }
            },
            {
              "type": "filtering-list-inbound-sender",
              "id": "82665",
              "attributes": {
                "domain_group_id": 1,
                "organization_id": 1,
                "is_enabled": true,
                "value": "4.com",
                "description": null,
                "created_at": "2015-05-19 17:30:36.000000 +00:00",
                "updated_at": "2015-05-19 17:30:36.000000 +00:00",
                "action": "block"
              }
            },
            {
              "type": "filtering-list-inbound-sender",
              "id": "62811",
              "attributes": {
                "domain_group_id": 1,
                "organization_id": 1,
                "is_enabled": true,
                "value": "xxxx@abcd.com",
                "description": "Spam LinkedIn vers xxxx",
                "created_at": "2014-05-30 14:53:10.000000 +00:00",
                "updated_at": "2014-05-30 14:53:10.000000 +00:00",
                "action": "block"
              }
            },
            {
              "type": "filtering-list-inbound-sender",
              "id": "62810",
              "attributes": {
                "domain_group_id": 1,
                "organization_id": 1,
                "is_enabled": true,
                "value": "architecture.com",
                "description": "Spam LinkedIn vers xxxx",
                "created_at": "2014-05-30 14:37:30.000000 +00:00",
                "updated_at": "2014-05-30 14:37:30.000000 +00:00",
                "action": "block"
              }
            },
            {
              "type": "filtering-list-inbound-sender",
              "id": "56858",
              "attributes": {
                "domain_group_id": 1,
                "organization_id": 1,
                "is_enabled": true,
                "value": "professional.com",
                "description": "Abus <xxxx>",
                "created_at": "2013-11-29 16:19:19.000000 +00:00",
                "updated_at": "2013-11-29 16:19:19.000000 +00:00",
                "action": "block"
              }
            },
            {
              "type": "filtering-list-inbound-sender",
              "id": "49106",
              "attributes": {
                "domain_group_id": 1,
                "organization_id": 1,
                "is_enabled": true,
                "value": "giason@solstice.org",
                "description": "xxxx a repris l\'alias xx",
                "created_at": "2013-01-17 13:08:00.000000 +00:00",
                "updated_at": "2013-01-17 13:08:00.000000 +00:00",
                "action": "block"
              }
            },
            {
              "type": "filtering-list-inbound-sender",
              "id": "46332",
              "attributes": {
                "domain_group_id": 1,
                "organization_id": 1,
                "is_enabled": true,
                "value": "une.adresse@drole.fr",
                "description": "Envoyeur de jokes",
                "created_at": "2012-09-26 20:38:19.000000 +00:00",
                "updated_at": "2012-09-26 20:38:19.000000 +00:00",
                "action": "block"
              }
            },
            {
              "type": "filtering-list-inbound-sender",
              "id": "38758",
              "attributes": {
                "domain_group_id": 1,
                "organization_id": 1,
                "is_enabled": true,
                "value": "expedition.com",
                "description": "Ajout par ZEROSPAM (Demande de xxxx)",
                "created_at": "2011-12-20 18:46:58.000000 +00:00",
                "updated_at": "2011-12-20 18:46:58.000000 +00:00",
                "action": "block"
              }
            }
          ]
        }
      },
      "ips": {
        "block": {
          "data": [
            {
              "type": "filtering-list-ip",
              "id": "120500",
              "attributes": {
                "domain_group_id": 1,
                "organization_id": 1,
                "is_enabled": true,
                "value": "10.0.0.0/8",
                "description": "Block this range",
                "created_at": "2017-08-30 15:04:31.144935 +00:00",
                "updated_at": "2017-08-30 15:04:31.144935 +00:00",
                "action": "block"
              }
            },
            {
              "type": "filtering-list-ip",
              "id": "120454",
              "attributes": {
                "domain_group_id": 1,
                "organization_id": 1,
                "is_enabled": true,
                "value": "3.3.3.3",
                "description": null,
                "created_at": "2017-08-22 14:31:15.860529 +00:00",
                "updated_at": "2017-08-22 14:31:15.860529 +00:00",
                "action": "block"
              }
            }
          ]
        },
        "allow": {
          "data": [
            {
              "type": "filtering-list-ip",
              "id": "98197",
              "attributes": {
                "domain_group_id": 1,
                "organization_id": 1,
                "is_enabled": true,
                "value": "69.68.62.115",
                "description": "IP de xxxx",
                "created_at": "2016-06-14 17:13:22.000000 +00:00",
                "updated_at": "2016-06-14 17:13:22.000000 +00:00",
                "action": "allow"
              }
            },
            {
              "type": "filtering-list-ip",
              "id": "57274",
              "attributes": {
                "domain_group_id": 1,
                "organization_id": 1,
                "is_enabled": true,
                "value": "30.100.60.10",
                "description": "Cpanel",
                "created_at": "2013-12-13 16:26:25.000000 +00:00",
                "updated_at": "2013-12-13 16:26:25.000000 +00:00",
                "action": "allow"
              }
            },
            {
              "type": "filtering-list-ip",
              "id": "44360",
              "attributes": {
                "domain_group_id": 1,
                "organization_id": 1,
                "is_enabled": true,
                "value": "10.2.3.4",
                "description": "ip de test",
                "created_at": "2012-07-04 13:21:17.000000 +00:00",
                "updated_at": "2012-07-04 13:21:17.000000 +00:00",
                "action": "allow"
              }
            },
            {
              "type": "filtering-list-ip",
              "id": "38475",
              "attributes": {
                "domain_group_id": 1,
                "organization_id": 1,
                "is_enabled": true,
                "value": "75.195.5.5",
                "description": "xxxx gw",
                "created_at": "2011-12-13 15:16:54.000000 +00:00",
                "updated_at": "2011-12-13 15:16:54.000000 +00:00",
                "action": "allow"
              }
            },
            {
              "type": "filtering-list-ip",
              "id": "38105",
              "attributes": {
                "domain_group_id": 1,
                "organization_id": 1,
                "is_enabled": true,
                "value": "61.211.221.241",
                "description": "postman@hosting.com",
                "created_at": "2011-11-28 14:46:00.000000 +00:00",
                "updated_at": "2011-11-28 14:46:00.000000 +00:00",
                "action": "allow"
              }
            },
            {
              "type": "filtering-list-ip",
              "id": "38104",
              "attributes": {
                "domain_group_id": 1,
                "organization_id": 1,
                "is_enabled": true,
                "value": "62.42.22.182",
                "description": "postman@hosting.ca",
                "created_at": "2011-11-28 14:41:40.000000 +00:00",
                "updated_at": "2011-11-28 14:41:40.000000 +00:00",
                "action": "allow"
              }
            },
            {
              "type": "filtering-list-ip",
              "id": "37965",
              "attributes": {
                "domain_group_id": 1,
                "organization_id": 1,
                "is_enabled": true,
                "value": "102.10.107.106",
                "description": "aol",
                "created_at": "2011-11-18 20:40:18.000000 +00:00",
                "updated_at": "2011-11-18 20:40:18.000000 +00:00",
                "action": "allow"
              }
            },
            {
              "type": "filtering-list-ip",
              "id": "37964",
              "attributes": {
                "domain_group_id": 1,
                "organization_id": 1,
                "is_enabled": true,
                "value": "105.88.149.29",
                "description": "aol",
                "created_at": "2011-11-18 20:37:31.000000 +00:00",
                "updated_at": "2011-11-18 20:37:31.000000 +00:00",
                "action": "allow"
              }
            },
            {
              "type": "filtering-list-ip",
              "id": "37505",
              "attributes": {
                "domain_group_id": 1,
                "organization_id": 1,
                "is_enabled": true,
                "value": "68.208.88.248",
                "description": "ListServ pour xxxx",
                "created_at": "2011-10-24 02:09:39.000000 +00:00",
                "updated_at": "2011-10-24 02:09:39.000000 +00:00",
                "action": "allow"
              }
            },
            {
              "type": "filtering-list-ip",
              "id": "19",
              "attributes": {
                "domain_group_id": 1,
                "organization_id": 1,
                "is_enabled": true,
                "value": "69.19.129.89",
                "description": null,
                "created_at": "2010-12-30 20:21:34.000000 +00:00",
                "updated_at": "2010-12-30 20:21:34.000000 +00:00",
                "action": "allow"
              }
            }
          ]
        }
      },
      "spfs": {
        "allow": {
          "data": [
            {
              "type": "filtering-list-spf",
              "id": "120459",
              "attributes": {
                "domain_group_id": 1,
                "organization_id": 1,
                "is_enabled": false,
                "value": "test2.com",
                "description": "ma description",
                "created_at": "2017-08-22 14:41:43.138217 +00:00",
                "updated_at": "2017-08-22 14:41:43.138217 +00:00",
                "action": "allow"
              }
            },
            {
              "type": "filtering-list-spf",
              "id": "120458",
              "attributes": {
                "domain_group_id": 1,
                "organization_id": 1,
                "is_enabled": true,
                "value": "test1.com",
                "description": null,
                "created_at": "2017-08-22 14:41:43.133843 +00:00",
                "updated_at": "2017-08-22 14:41:43.133843 +00:00",
                "action": "allow"
              }
            },
            {
              "type": "filtering-list-spf",
              "id": "120436",
              "attributes": {
                "domain_group_id": 1,
                "organization_id": 1,
                "is_enabled": true,
                "value": "domain.ca",
                "description": null,
                "created_at": "2017-08-22 13:54:29.198641 +00:00",
                "updated_at": "2017-08-23 19:09:34.346101 +00:00",
                "action": "allow"
              }
            }
          ]
        }
      },
      "in-rcpts": {
        "allow": {
          "data": [
            {
              "type": "filtering-list-inbound-recipient",
              "id": "120426",
              "attributes": {
                "domain_group_id": 1,
                "organization_id": 1,
                "is_enabled": false,
                "value": "testy",
                "description": "this is test",
                "created_at": "2017-08-22 13:13:50.391807 +00:00",
                "updated_at": "2017-08-22 13:13:50.391807 +00:00",
                "action": "allow"
              }
            },
            {
              "type": "filtering-list-inbound-recipient",
              "id": "120425",
              "attributes": {
                "domain_group_id": 1,
                "organization_id": 1,
                "is_enabled": true,
                "value": "test",
                "description": null,
                "created_at": "2017-08-22 13:13:07.294813 +00:00",
                "updated_at": "2017-08-22 13:13:07.294813 +00:00",
                "action": "allow"
              }
            },
            {
              "type": "filtering-list-inbound-recipient",
              "id": "120409",
              "attributes": {
                "domain_group_id": 1,
                "organization_id": 1,
                "is_enabled": true,
                "value": "iogreig",
                "description": null,
                "created_at": "2017-08-18 19:22:01.000000 +00:00",
                "updated_at": "2017-08-23 19:05:43.993220 +00:00",
                "action": "allow"
              }
            },
            {
              "type": "filtering-list-inbound-recipient",
              "id": "120380",
              "attributes": {
                "domain_group_id": 1,
                "organization_id": 1,
                "is_enabled": true,
                "value": "pop",
                "description": null,
                "created_at": "2017-08-18 15:05:45.000000 +00:00",
                "updated_at": "2017-08-18 18:44:48.899969 +00:00",
                "action": "allow"
              }
            }
          ]
        }
      },
      "auth-addrs": {
        "restrict": {
          "data": [
            {
              "type": "filtering-list-authorized-address",
              "id": "120445",
              "attributes": {
                "domain_group_id": 1,
                "organization_id": 1,
                "is_enabled": false,
                "value": "test000",
                "description": "ma description",
                "created_at": "2017-08-22 14:18:17.114286 +00:00",
                "updated_at": "2017-08-22 14:18:17.114286 +00:00",
                "action": "restrict"
              }
            },
            {
              "type": "filtering-list-authorized-address",
              "id": "120444",
              "attributes": {
                "domain_group_id": 1,
                "organization_id": 1,
                "is_enabled": true,
                "value": "test00",
                "description": null,
                "created_at": "2017-08-22 14:18:17.109911 +00:00",
                "updated_at": "2017-08-22 14:18:17.109911 +00:00",
                "action": "restrict"
              }
            },
            {
              "type": "filtering-list-authorized-address",
              "id": "120443",
              "attributes": {
                "domain_group_id": 1,
                "organization_id": 1,
                "is_enabled": false,
                "value": "test6",
                "description": "ma description",
                "created_at": "2017-08-22 13:59:11.939214 +00:00",
                "updated_at": "2017-08-22 13:59:11.939214 +00:00",
                "action": "restrict"
              }
            },
            {
              "type": "filtering-list-authorized-address",
              "id": "120442",
              "attributes": {
                "domain_group_id": 1,
                "organization_id": 1,
                "is_enabled": true,
                "value": "test9",
                "description": null,
                "created_at": "2017-08-22 13:59:11.935400 +00:00",
                "updated_at": "2017-08-22 13:59:11.935400 +00:00",
                "action": "restrict"
              }
            },
            {
              "type": "filtering-list-authorized-address",
              "id": "120441",
              "attributes": {
                "domain_group_id": 1,
                "organization_id": 1,
                "is_enabled": true,
                "value": "test5",
                "description": null,
                "created_at": "2017-08-22 13:58:51.209618 +00:00",
                "updated_at": "2017-08-22 13:58:51.209618 +00:00",
                "action": "restrict"
              }
            },
            {
              "type": "filtering-list-authorized-address",
              "id": "120422",
              "attributes": {
                "domain_group_id": 1,
                "organization_id": 1,
                "is_enabled": false,
                "value": "testy",
                "description": "this is a test!",
                "created_at": "2017-08-22 13:01:02.341114 +00:00",
                "updated_at": "2017-08-22 13:01:02.341114 +00:00",
                "action": "restrict"
              }
            },
            {
              "type": "filtering-list-authorized-address",
              "id": "120421",
              "attributes": {
                "domain_group_id": 1,
                "organization_id": 1,
                "is_enabled": true,
                "value": "iogreig",
                "description": null,
                "created_at": "2017-08-22 13:00:14.152144 +00:00",
                "updated_at": "2017-08-23 19:04:07.568893 +00:00",
                "action": "restrict"
              }
            },
            {
              "type": "filtering-list-authorized-address",
              "id": "120420",
              "attributes": {
                "domain_group_id": 1,
                "organization_id": 1,
                "is_enabled": true,
                "value": "pony",
                "description": null,
                "created_at": "2017-08-22 12:59:43.977400 +00:00",
                "updated_at": "2017-08-22 12:59:43.977400 +00:00",
                "action": "restrict"
              }
            },
            {
              "type": "filtering-list-authorized-address",
              "id": "120419",
              "attributes": {
                "domain_group_id": 1,
                "organization_id": 1,
                "is_enabled": true,
                "value": "test3",
                "description": null,
                "created_at": "2017-08-21 18:38:16.965130 +00:00",
                "updated_at": "2017-08-21 18:38:16.965130 +00:00",
                "action": "restrict"
              }
            },
            {
              "type": "filtering-list-authorized-address",
              "id": "120416",
              "attributes": {
                "domain_group_id": 1,
                "organization_id": 1,
                "is_enabled": false,
                "value": "test2",
                "description": "ma description",
                "created_at": "2017-08-21 18:30:12.129954 +00:00",
                "updated_at": "2017-08-21 18:30:12.129954 +00:00",
                "action": "restrict"
              }
            },
            {
              "type": "filtering-list-authorized-address",
              "id": "120415",
              "attributes": {
                "domain_group_id": 1,
                "organization_id": 1,
                "is_enabled": true,
                "value": "test",
                "description": null,
                "created_at": "2017-08-21 18:30:12.123522 +00:00",
                "updated_at": "2017-08-21 18:30:12.123522 +00:00",
                "action": "restrict"
              }
            }
          ]
        }
      },
      "file-exts": {
        "block": {
          "data": [
            {
              "type": "filtering-list-file-extension",
              "id": "120408",
              "attributes": {
                "domain_group_id": 1,
                "organization_id": 1,
                "is_enabled": true,
                "value": "rer",
                "description": null,
                "created_at": "2017-08-18 19:19:01.000000 +00:00",
                "updated_at": "2017-08-18 19:19:01.000000 +00:00",
                "action": "block"
              }
            },
            {
              "type": "filtering-list-file-extension",
              "id": "120407",
              "attributes": {
                "domain_group_id": 1,
                "organization_id": 1,
                "is_enabled": true,
                "value": "mom",
                "description": null,
                "created_at": "2017-08-18 19:19:01.000000 +00:00",
                "updated_at": "2017-08-18 19:19:01.000000 +00:00",
                "action": "block"
              }
            },
            {
              "type": "filtering-list-file-extension",
              "id": "120405",
              "attributes": {
                "domain_group_id": 1,
                "organization_id": 1,
                "is_enabled": true,
                "value": "pop",
                "description": null,
                "created_at": "2017-08-18 19:19:01.000000 +00:00",
                "updated_at": "2017-08-18 19:19:01.000000 +00:00",
                "action": "block"
              }
            },
            {
              "type": "filtering-list-file-extension",
              "id": "120447",
              "attributes": {
                "domain_group_id": 1,
                "organization_id": 1,
                "is_enabled": false,
                "value": "test000",
                "description": "ma description",
                "created_at": "2017-08-22 14:22:33.100575 +00:00",
                "updated_at": "2017-08-22 14:22:33.100575 +00:00",
                "action": "block"
              }
            },
            {
              "type": "filtering-list-file-extension",
              "id": "120423",
              "attributes": {
                "domain_group_id": 1,
                "organization_id": 1,
                "is_enabled": false,
                "value": "testy",
                "description": "this is a test!",
                "created_at": "2017-08-22 13:03:13.190002 +00:00",
                "updated_at": "2017-08-22 13:03:13.190002 +00:00",
                "action": "block"
              }
            }
          ]
        },
        "allow": {
          "data": [
            {
              "type": "filtering-list-file-extension",
              "id": "120505",
              "attributes": {
                "domain_group_id": 1,
                "organization_id": 1,
                "is_enabled": true,
                "value": "test4",
                "description": "this comment",
                "created_at": "2017-08-31 20:59:05.880183 +00:00",
                "updated_at": "2017-08-31 20:59:05.880183 +00:00",
                "action": "allow"
              }
            },
            {
              "type": "filtering-list-file-extension",
              "id": "120504",
              "attributes": {
                "domain_group_id": 1,
                "organization_id": 1,
                "is_enabled": true,
                "value": "test2",
                "description": "this comment",
                "created_at": "2017-08-31 20:59:05.872604 +00:00",
                "updated_at": "2017-08-31 20:59:05.872604 +00:00",
                "action": "allow"
              }
            },
            {
              "type": "filtering-list-file-extension",
              "id": "120503",
              "attributes": {
                "domain_group_id": 1,
                "organization_id": 1,
                "is_enabled": true,
                "value": "test1",
                "description": "this comment",
                "created_at": "2017-08-31 20:59:05.862574 +00:00",
                "updated_at": "2017-08-31 20:59:05.862574 +00:00",
                "action": "allow"
              }
            },
            {
              "type": "filtering-list-file-extension",
              "id": "120476",
              "attributes": {
                "domain_group_id": 1,
                "organization_id": 1,
                "is_enabled": true,
                "value": "gif",
                "description": null,
                "created_at": "2017-08-23 18:12:05.062769 +00:00",
                "updated_at": "2017-08-23 18:12:05.062769 +00:00",
                "action": "allow"
              }
            },
            {
              "type": "filtering-list-file-extension",
              "id": "120475",
              "attributes": {
                "domain_group_id": 1,
                "organization_id": 1,
                "is_enabled": true,
                "value": "jpg",
                "description": null,
                "created_at": "2017-08-23 17:41:21.924446 +00:00",
                "updated_at": "2017-08-23 17:41:21.924446 +00:00",
                "action": "allow"
              }
            },
            {
              "type": "filtering-list-file-extension",
              "id": "120474",
              "attributes": {
                "domain_group_id": 1,
                "organization_id": 1,
                "is_enabled": true,
                "value": "png",
                "description": null,
                "created_at": "2017-08-23 15:44:47.628938 +00:00",
                "updated_at": "2017-08-23 15:44:47.628938 +00:00",
                "action": "allow"
              }
            },
            {
              "type": "filtering-list-file-extension",
              "id": "120424",
              "attributes": {
                "domain_group_id": 1,
                "organization_id": 1,
                "is_enabled": true,
                "value": "ok",
                "description": null,
                "created_at": "2017-08-22 13:03:24.936442 +00:00",
                "updated_at": "2017-08-22 13:03:24.936442 +00:00",
                "action": "allow"
              }
            }
          ]
        }
      },
      "mimes": {
        "allow": {
          "data": [
            {
              "type": "filtering-list-mime",
              "id": "120457",
              "attributes": {
                "domain_group_id": 1,
                "organization_id": 1,
                "is_enabled": false,
                "value": "test2/test2",
                "description": "ma description",
                "created_at": "2017-08-22 14:33:11.373902 +00:00",
                "updated_at": "2017-08-22 14:33:11.373902 +00:00",
                "action": "allow"
              }
            },
            {
              "type": "filtering-list-mime",
              "id": "120434",
              "attributes": {
                "domain_group_id": 1,
                "organization_id": 1,
                "is_enabled": false,
                "value": "test/test",
                "description": "testing",
                "created_at": "2017-08-22 13:52:57.929309 +00:00",
                "updated_at": "2017-08-22 13:52:57.929309 +00:00",
                "action": "allow"
              }
            }
          ]
        },
        "block": {
          "data": [
            {
              "type": "filtering-list-mime",
              "id": "120478",
              "attributes": {
                "domain_group_id": 1,
                "organization_id": 1,
                "is_enabled": true,
                "value": "mime/typo",
                "description": null,
                "created_at": "2017-08-23 19:00:15.204717 +00:00",
                "updated_at": "2017-08-23 19:08:57.916108 +00:00",
                "action": "block"
              }
            },
            {
              "type": "filtering-list-mime",
              "id": "120456",
              "attributes": {
                "domain_group_id": 1,
                "organization_id": 1,
                "is_enabled": true,
                "value": "test1/test1",
                "description": null,
                "created_at": "2017-08-22 14:33:11.369805 +00:00",
                "updated_at": "2017-08-22 14:33:11.369805 +00:00",
                "action": "block"
              }
            },
            {
              "type": "filtering-list-mime",
              "id": "120435",
              "attributes": {
                "domain_group_id": 1,
                "organization_id": 1,
                "is_enabled": true,
                "value": "test/testy",
                "description": null,
                "created_at": "2017-08-22 13:53:33.493804 +00:00",
                "updated_at": "2017-08-22 13:53:33.493804 +00:00",
                "action": "block"
              }
            }
          ]
        }
      }
    }
  }
}';

        $config = $this->preSuccess($jsonResult);

        $request = new AllFilteringListsRequest();

        $request->setOrganizationId(2546011)
                ->setFilteringId(9370707);

        $client = new ProvulusClient($config);

        /** @var AllFilteringListsResponse $response */
        $response = $client->processRequest($request);
        $this->validateRequest($config, $jsonRequest);
        $this->validateRequestUrl($config, ['orgs/2546011/policies/9370707/all-items']);

        $mimes = $response->getFilteringList(FilteringListRequestTypeEnum::MIME(), FilteringListActionEnum::BLOCK());
        $filesExts = $response->getFilteringList(FilteringListRequestTypeEnum::FILE_EXTENSION(), FilteringListActionEnum::ALLOW());


        self::assertEquals(3, count($mimes->toArray()));
        self::assertEquals(7, count($filesExts->toArray()));

        /** @var FilteringListResponse $firstMime */
        $firstMime = $mimes->first();

        self::assertEquals('mime/typo', $firstMime->value);
    }
}
