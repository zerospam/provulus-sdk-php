<?php
/**
 * Created by PhpStorm.
 * User: ycoutu
 * Date: 18/04/18
 * Time: 11:50 AM
 */

namespace automated\Requests\Organization\Password;

use ProvulusSDK\Client\ProvulusClient;
use ProvulusSDK\Client\Request\Organization\Password\ReadPasswordConfigurationRequest;
use ProvulusSDK\Client\Request\Organization\Password\UpdatePasswordConfigurationRequest;
use ProvulusTest\TestCaseApiKey;

class PasswordConfigurationTest extends TestCaseApiKey
{
    /** @test */
    public function read_password_configuration()
    {
        $jsonResponse = '{
            "data": {
                "type": "password-configuration",
                "id": "111",
                "attributes": {
                    "min_length": 12,
                    "min_lower": 1,
                    "min_upper": 2,
                    "min_digits": 0,
                    "min_specials": 4,
                    "life_in_days": 30,
                    "history_count": 13,
                    "created_at": "2014-01-30 15:12:12.000000 +00:00",
                    "updated_at": "2017-08-28 18:55:53.000000 +00:00"
                }
            }
        }';

        $config  = $this->preSuccess($jsonResponse);
        $request = (new ReadPasswordConfigurationRequest)
            ->setOrganizationId(111);

        $client = new ProvulusClient($config);
        $client->processRequest($request);

        $response = $request->getResponse();

        self::assertEquals(12, $response->min_length);
        self::assertEquals(1, $response->min_lower);
        self::assertEquals(2, $response->min_upper);
        self::assertEquals(0, $response->min_digits);
        self::assertEquals(4, $response->min_specials);
        self::assertEquals(13, $response->history_count);
        self::assertEquals(30, $response->life_in_days);
    }

    /** @test */
    public function update_password_configuration()
    {
        $jsonResponse = '{
            "data": {
                "type": "password-configuration",
                "id": "443",
                "attributes": {
                    "min_length": 18,
                    "min_lower": 2,
                    "min_upper": 1,
                    "min_digits": 3,
                    "min_specials": 0,
                    "life_in_days": null,
                    "history_count": null,
                    "created_at": "2014-01-30 15:12:12.000000 +00:00",
                    "updated_at": "2017-08-28 18:55:53.000000 +00:00"
                }
            }
        }';
        $jsonRequest = '{
            "min_length": 18,
            "min_lower": 2,
            "min_upper": 1,
            "min_digits": 3,
            "min_specials": 0,
            "life_in_days": null,
            "history_count": null
        }';

        $config  = $this->preSuccess($jsonResponse);
        $request = (new UpdatePasswordConfigurationRequest)
            ->setOrganizationId(443)
            ->setMinLength(18)
            ->setMinLower(2)
            ->setMinUpper(1)
            ->setMinDigits(3)
            ->setMinSpecials(0)
            ->setLifeInDays(null)
            ->setHistoryCount(null);

        $client = new ProvulusClient($config);
        $client->processRequest($request);
        $this->validateRequest($config, $jsonRequest);


        $response = $request->getResponse();

        self::assertEquals(18, $response->min_length);
        self::assertEquals(2, $response->min_lower);
        self::assertEquals(1, $response->min_upper);
        self::assertEquals(3, $response->min_digits);
        self::assertEquals(0, $response->min_specials);
        self::assertNull($response->history_count);
        self::assertNull($response->life_in_days);
    }
}
