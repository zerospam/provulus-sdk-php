<?php
/**
 * Created by PhpStorm.
 * User: pbb
 * Date: 14/12/16
 * Time: 4:16 PM
 */

namespace automated\Requests\Organization\Contact\Person;

use GuzzleHttp\Handler\MockHandler;
use libphonenumber\PhoneNumberUtil;
use ProvulusSDK\Client\ProvulusClient;
use ProvulusSDK\Client\Request\Organization\Contact\Bulk\BulkDeleteContactPersonRequest;
use ProvulusSDK\Client\Request\Organization\Contact\CreateContactPersonRequest;
use ProvulusSDK\Client\Request\Organization\Contact\DeleteContactPersonRequest;
use ProvulusSDK\Client\Request\Organization\Contact\IndexContactPersonRequest;
use ProvulusSDK\Client\Request\Organization\Contact\Mailgroups\MailGroup;
use ProvulusSDK\Client\Request\Organization\Contact\Mailgroups\MailgroupsCategoriesIndexRequest;
use ProvulusSDK\Client\Request\Organization\Contact\Mailgroups\MailgroupsIndexRequest;
use ProvulusSDK\Client\Request\Organization\Contact\ReadContactPersonRequest;
use ProvulusSDK\Client\Request\Organization\Contact\SetDefaultContactPersonRequest;
use ProvulusSDK\Client\Request\Organization\Contact\UpdateContactPersonRequest;
use ProvulusSDK\Client\Response\Organization\Contact\ContactPersonResponse;
use ProvulusSDK\Client\Response\Organization\Contact\Mailgroups\MailGroupCategoryResponse;
use ProvulusSDK\Client\Response\Organization\Contact\Mailgroups\MailGroupResponse;
use ProvulusSDK\Config\ProvulusConfiguration;
use ProvulusSDK\Enum\Contact\Mailgroups\MailGroupCategoriesEnum;
use ProvulusSDK\Enum\Contact\Mailgroups\MailGroupEnum;
use ProvulusSDK\Enum\Locale\LocaleEnum;
use ProvulusTest\TestCase;


/**
 * Class ContactPersonTest
 *
 * Tests for ContactPerson Requests
 *
 * @package automated\Requests\Organization\Contact\Person
 */
class ContactPersonTest extends TestCase
{

    /**
     * @test
     */
    public function create_contact_person_success()
    {
        $jsonResult
            = '{
  "data": {
    "type": "contact-person",
    "id": "1802",
    "attributes": {
      "is_default": false,
      "organization_id": 3681,
      "phone": "+1 514-527-3232",
      "title": "Administration",
      "email": "support@zerospam.ca",
      "updated_at": "2018-09-10 20:36:50.840154 +00:00",
      "created_at": "2018-09-10 20:36:50.840154 +00:00",
      "language": "en_us",
      "mailgroups": [
  	     "service_activation",
  	     "new_client_created"
   	  ]
    }
  }
}';
        $jsonRequest
            = '
            {
  "title" : "Administration",
  "email" : "support@zerospam.ca",
  "phone" : "15145273232",
  "mailgroups": [
  	"service_activation",
  	"new_client_created"
  	]
}';

        $config = $this->preSuccess($jsonResult);

        $request = new CreateContactPersonRequest();
        $request->setTitle('Administration')
                ->setEmail('support@zerospam.ca')
                ->setPhone('15145273232')
                ->setMailgroups([
                    MailGroupEnum::SERVICE_ACTIVATION(),
                    MailGroupEnum::NEW_CLIENT_CREATED()
                ])
                ->setOrganizationId(2776);

        $client = new ProvulusClient($config);
        /**
         * @var $response ContactPersonResponse
         */
        $response = $client->processRequest($request);

        $this->validateRequest($config, $jsonRequest);

        $this->assertEquals(3681, $response->organization_id);
        $this->assertEquals(1802, $response->id());
        $this->assertTrue(
            PhoneNumberUtil::getInstance()->parse($response->phone, 'US')
                           ->equals(PhoneNumberUtil::getInstance()->parse('15145273232', 'US'))
        );
    }

    /**
     * @test
     */
    public function delete_contact_person_success()
    {
        $jsonRequest = '{}';
        $config      = $this->preSuccess('{}');

        $request = new DeleteContactPersonRequest();
        $request->setContactPersonId(209)->setOrganizationId(2776);

        $client = new ProvulusClient($config);
        /**
         * @var $response ContactPersonResponse
         */
        $client->processRequest($request);

        $this->validateRequest($config, $jsonRequest);
    }

    /**
     * @test
     */
    public function index_contact_person()
    {
        $jsonResponse
            = '{
  "data": [
    {
      "type": "contact-person",
      "id": "1837",
      "attributes": {
        "title": "Administration",
        "email": "support@zerospam.ca",
        "phone": "+1 514-527-3232",
        "organization_id": 3711,
        "created_at": "2018-09-13 13:48:27.000000 +00:00",
        "updated_at": "2018-09-13 13:48:27.000000 +00:00",
        "deleted_at": null,
        "language": "en_us",
        "is_default": false,
        "mailgroups": [
          "service_activation",
          "service_deconfiguration"
        ]
      }
    },
    {
      "type": "contact-person",
      "id": "1836",
      "attributes": {
        "title": "Administration",
        "email": "support@zerospam.ca",
        "phone": "+1 514-527-3232",
        "organization_id": 3711,
        "created_at": "2018-09-12 20:59:44.000000 +00:00",
        "updated_at": "2018-09-12 20:59:44.000000 +00:00",
        "deleted_at": null,
        "language": "en_us",
        "is_default": false,
        "mailgroups": []
      }
    }
  ],
  "meta": {
    "pagination": {
      "total": 2,
      "count": 2,
      "per_page": 15,
      "current_page": 1,
      "total_pages": 1
    }
  },
  "links": {
    "self": "http://cumulus.local/api/v1/orgs/3711/contacts?page=1",
    "first": "http://cumulus.local/api/v1/orgs/3711/contacts?page=1",
    "last": "http://cumulus.local/api/v1/orgs/3711/contacts?page=1"
  }
}';

        $jsonRequest = '{}';

        $config = $this->preSuccess($jsonResponse);

        $request = new IndexContactPersonRequest();
        $request->setOrganizationId(3711);

        $client = new ProvulusClient($config);
        $client->processRequest($request);
        $collectionResponse = $request->getResponse();

        $contactPersonResponse = $collectionResponse->data()->first();
        $this->assertEquals(1837, $contactPersonResponse->id());
        $this->assertEquals('Administration', $contactPersonResponse->title);
        $this->validateRequest($config, $jsonRequest);
    }

    /**
     * @test
     */
    public function read_contact_success()
    {
        $jsonResult
            = '{
  "data": {
    "type": "contact-person",
    "id": "1837",
    "attributes": {
      "title": "Administration",
      "email": "support@zerospam.ca",
      "phone": "+1 514-527-3232",
      "organization_id": 3711,
      "created_at": "2018-09-13 13:48:27.000000 +00:00",
      "updated_at": "2018-09-13 13:48:27.000000 +00:00",
      "deleted_at": null,
      "language": "en_us",
      "is_default": false,
      "mailgroups": [
        "service_activation",
        "service_deconfiguration"
      ]
    }
  }
}';

        $jsonRequest = '{}';

        $config = $this->preSuccess($jsonResult);

        $request = new ReadContactPersonRequest();
        $request->setOrganizationId(3711)
                ->setContactPersonId(1837);

        $client = new ProvulusClient($config);
        $client->processRequest($request);
        $response = $request->getResponse();

        $this->assertNotEquals(3000, $response->organization_id);
        $this->assertEquals(3711, $response->organization_id);
        $this->assertCount(2, $response->mailgroups);
        $this->validateRequest($config, $jsonRequest);
        $this->validateRequestUrl($config, ['orgs/3711/contacts/1837']);
    }


    /**
     * @test
     */
    public function updateContactPersonSuccess()
    {
        $jsonResult
            = '{
  "data": {
    "type": "contact-person",
    "id": "1804",
    "attributes": {
      "is_default": false,
      "organization_id": 3681,
      "phone": "+1 514-527-3232",
      "title": "Administration",
      "email": "support@zerospam.ca",
      "updated_at": "2018-09-11 14:59:17.000000 +00:00",
      "created_at": "2018-09-11 14:45:41.000000 +00:00",
      "language": "fr_ca",
      "deleted_at": null,
      "mailgroups": [
  	    "service_activation",
  	    "other"
  	  ]
    }
  }
}';
        $jsonRequest
            = '{
  "title": "Administration",
  "email": "support@zerospam.ca",
  "phone" : "15145273232",
  "language" : "fr_CA",
  "mailgroups": [
  	"service_activation",
  	"other"
  	]
}';

        $config = $this->preSuccess($jsonResult);

        $request = new UpdateContactPersonRequest();
        $request->setTitle('Administration')
                ->setEmail('support@zerospam.ca')
                ->setMailgroups([
                    MailGroupEnum::SERVICE_ACTIVATION(),
                    MailGroupEnum::OTHER(),
                ])
                ->setLanguage(LocaleEnum::FR_CA())
                ->setPhone('15145273232')
                ->setOrganizationId(3681)
                ->setContactPersonId(1804);

        $client = new ProvulusClient($config);
        /**
         * @var $response \ProvulusSDK\Client\Response\Organization\Contact\ContactPersonResponse
         */
        $response = $client->processRequest($request);

        $this->validateRequest($config, $jsonRequest);

        $this->assertEquals(3681, $response->organization_id);
        $this->assertEquals(1804, $response->id());
        $this->assertEquals('support@zerospam.ca', $response->email);
    }

    /**
     * @test
     */
    public function index_mailgroups_by_category()
    {
        $jsonRequest = [];
        $jsonResult  = '
{
  "data": [
    {
      "type": "mailgroup",
      "id": "technical_support",
      "attributes": {
        "description": "Technical support related inquiries from clients.",
        "value": "technical_support",
        "label": "Technical support"
      },
      "relationships": {
        "category": {
          "data": {
            "type": "mailgroup-category",
            "id": "technical"
          }
        }
      }
    },
    {
      "type": "mailgroup",
      "id": "sales_support",
      "attributes": {
        "value": "sales_support",
        "label": "Sales support",
        "description": "Sales related inquiries from clients."
      },
      "relationships": {
        "category": {
          "data": {
            "type": "mailgroup-category",
            "id": "sales"
          }
        }
      }
    },
    {
      "type": "mailgroup",
      "id": "billing_support",
      "attributes": {
        "value": "billing_support",
        "label": "Billing support",
        "description": "Billing related inquiries from clients."
      },
      "relationships": {
        "category": {
          "data": {
            "type": "mailgroup-category",
            "id": "billing"
          }
        }
      }
    },
    {
      "type": "mailgroup",
      "id": "new_client_created",
      "attributes": {
        "value": "new_client_created",
        "label": "New client created",
        "description": "Sent at client creation."
      },
      "relationships": {
        "category": {
          "data": {
            "type": "mailgroup-category",
            "id": "sales"
          }
        }
      }
    },
    {
      "type": "mailgroup",
      "id": "mail_delivery_issues",
      "attributes": {
        "value": "mail_delivery_issues",
        "label": "Mail delivery issues",
        "description": "Sent when your mail servers are not accepting emails."
      },
      "relationships": {
        "category": {
          "data": {
            "type": "mailgroup-category",
            "id": "technical"
          }
        }
      }
    },
    {
      "type": "mailgroup",
      "id": "outbound_filtering_configuration",
      "attributes": {
        "value": "outbound_filtering_configuration",
        "label": "Outbound filtering configuration",
        "description": "Outbound filtering activation and configuration instructions."
      },
      "relationships": {
        "category": {
          "data": {
            "type": "mailgroup-category",
            "id": "technical"
          }
        }
      }
    },
    {
      "type": "mailgroup",
      "id": "service_configuration",
      "attributes": {
        "value": "service_configuration",
        "label": "Service configuration",
        "description": "Service configuration technical instructions and tips."
      },
      "relationships": {
        "category": {
          "data": {
            "type": "mailgroup-category",
            "id": "technical"
          }
        }
      }
    },
    {
      "type": "mailgroup",
      "id": "service_activation",
      "attributes": {
        "value": "service_activation",
        "label": "Service activation",
        "description": "Sent when a domain is properly configured and activated."
      },
      "relationships": {
        "category": {
          "data": {
            "type": "mailgroup-category",
            "id": "sales"
          }
        }
      }
    },
    {
      "type": "mailgroup",
      "id": "service_deconfiguration",
      "attributes": {
        "value": "service_deconfiguration",
        "label": "Service Deconfiguration",
        "description": "Domain or account deconfiguration instructions and acknowledgments."
      },
      "relationships": {
        "category": {
          "data": {
            "type": "mailgroup-category",
            "id": "billing"
          }
        }
      }
    },
    {
      "type": "mailgroup",
      "id": "trial_expiry",
      "attributes": {
        "value": "trial_expiry",
        "label": "Trial expiry",
        "description": "Sent when your service trial is about to end."
      },
      "relationships": {
        "category": {
          "data": {
            "type": "mailgroup-category",
            "id": "sales"
          }
        }
      }
    },
    {
      "type": "mailgroup",
      "id": "yearly_mailbox_count_update",
      "attributes": {
        "value": "yearly_mailbox_count_update",
        "label": "Yearly mailbox count update",
        "description": "Yearly mailbox count instructions and reminders."
      },
      "relationships": {
        "category": {
          "data": {
            "type": "mailgroup-category",
            "id": "billing"
          }
        }
      }
    }
  ],
  "included": [
    {
      "type": "mailgroup-category",
      "id": "technical",
      "attributes": {
        "category": "technical",
        "label": "Technical"
      }
    },
    {
      "type": "mailgroup-category",
      "id": "sales",
      "attributes": {
        "category": "sales",
        "label": "Sales"
      }
    },
    {
      "type": "mailgroup-category",
      "id": "billing",
      "attributes": {
        "category": "billing",
        "label": "Billing"
      }
    }
  ]
}';

        $config  = $this->preSuccess($jsonResult);
        $client  = new ProvulusClient($config);
        $request = new MailgroupsIndexRequest();
        $client->processRequest($request->setOrganizationId(1234));

        $this->validateRequest($config, $jsonRequest);
        $this->validateRequestUrl($config, ['orgs/1234/mailgroups']);

        $data       = $request->getResponse()->data();
        $mailgroups = $data->reduce(
            function (array $carry, MailGroupResponse $response) {
                return array_merge($carry, [$response->value->getValue()]);
            }, []
        );

        self::assertArraySubset(
            [
                0 => MailGroupEnum::TECHNICAL_SUPPORT,
                1 => MailGroupEnum::SALES_SUPPORT
            ],
            $mailgroups
        );

        /** @var MailGroupResponse $first */
        $first = $data->first();
        self::assertTrue($first->value->is(MailGroupEnum::TECHNICAL_SUPPORT()));

        /** @var MailGroupCategoryResponse $category */
        $category = $first->category;
        self::assertTrue($category->category->is(MailGroupCategoriesEnum::TECHNICAL()));
    }

    /**
     * @test
     */
    public function index_mailgroup_categories()
    {
        $jsonRequest = [];
        $jsonResult  = '
{
  "data": [
    {
      "type": "mailgroup-categories",
      "id": "-1",
      "attributes": {
        "category": "technical",
        "label": "Technical"
      }
    },
    {
      "type": "mailgroup-categories",
      "id": "-1",
      "attributes": {
        "category": "sales",
        "label": "Sales"
      }
    },
    {
      "type": "mailgroup-categories",
      "id": "-1",
      "attributes": {
        "category": "billing",
        "label": "Billing"
      }
    }
  ]
}';

        $config  = $this->preSuccess($jsonResult);
        $client  = new ProvulusClient($config);
        $request = new MailgroupsCategoriesIndexRequest();
        $client->processRequest($request);

        $this->validateRequest($config, $jsonRequest);
        $this->validateRequestUrl($config, ['mailgroupcategories']);

        $response = $request->getResponse();

        $data = $response->data();

        /** @var $array */
        $array = $data->reduce(function (array $carry, MailGroupCategoryResponse $response) {
            return array_merge($carry, [$response->category]);
        }, []);

        self::assertArraySubset([
            0 => MailGroupCategoriesEnum::TECHNICAL(),
            1 => MailGroupCategoriesEnum::SALES(),
            2 => MailGroupCategoriesEnum::BILLING()
        ], $array);
    }

    /**
     * @test
     * @expectedException \InvalidArgumentException
     */
    public function create_contact_person_repeat_enum_value()
    {
        $request = new CreateContactPersonRequest();
        $request->setTitle('Administration')
                ->setEmail('support@zerospam.ca')
                ->setPhone('15145273232')
                ->setMailgroups([MailGroupEnum::NEW_CLIENT_CREATED(), MailGroupEnum::NEW_CLIENT_CREATED()])
                ->setOrganizationId(2776);
    }

    /** @test */
    public function bulk_delete_contact_person_success()
    {
        $jsonRequest = '{"ids": [3, 4, 5]}';

        $config = $this->preSuccess('{}');

        $request = new BulkDeleteContactPersonRequest();
        $request->setOrganizationId(777)
                ->setIds([3, 4, 5]);

        $client = new ProvulusClient($config);
        $client->processRequest($request);

        $this->validateRequest($config, $jsonRequest);
        $this->validateRequestUrl($config, ['/orgs/777/contacts']);
    }

    /** @test
     * @expectedException \InvalidArgumentException
     */
    public function bulk_delete_contact_person_failure()
    {
        $request = new BulkDeleteContactPersonRequest();

        $request->setOrganizationId(777)
                ->setIds(["3", 4, 5]);
    }

    /**
     * @test
     */
    public function setContactAsDefault()
    {
        $jsonResult
                     = '{
  "data": {
    "type": "contact-person",
    "id": "1837",
    "attributes": {
      "title": "Administration",
      "email": "support@zerospam.ca",
      "phone": "+1 514-527-3232",
      "organization_id": 3711,
      "created_at": "2018-09-13 13:48:27.000000 +00:00",
      "updated_at": "2018-09-18 15:43:03.699902 +00:00",
      "deleted_at": null,
      "language": "fr_ca",
      "is_default": true,
      "mailgroups": [
        "other"
      ]
    }
  }
}';
        $jsonRequest = [];

        $config = $this->preSuccess($jsonResult);

        $request = new SetDefaultContactPersonRequest();
        $request->setOrganizationId(3711)
                ->setContactPersonId(1837);

        $client = new ProvulusClient($config);
        $client->processRequest($request);
        $this->validateRequest($config, $jsonRequest);
        $this->validateRequestUrl($config, ['orgs/3711/contacts/1837/default']);

        $response = $request->getResponse();

        $this->assertEquals(3711, $response->organization_id);
        $this->assertEquals(1837, $response->id());
        $this->assertEquals(true, $response->is_default);
    }

    /**
     * Gets config for tests
     *
     * @param MockHandler $handler
     *
     * @return ProvulusConfiguration
     */
    protected function getConfig(MockHandler $handler)
    {
        return parent::getConfig($handler)->setApiKey('test');
    }
}