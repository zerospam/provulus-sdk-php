<?php
/**
 * Created by PhpStorm.
 * User: ycoutu
 * Date: 21/06/17
 * Time: 3:39 PM
 */

namespace automated\Requests\Organization\Contact\ContactUs;

use GuzzleHttp\Handler\MockHandler;
use ProvulusSDK\Client\ProvulusClient;
use ProvulusSDK\Client\Request\Organization\Contact\ContactUs\CreateContactUsPortalRequest;
use ProvulusSDK\Client\Request\Organization\Contact\ContactUs\CreateContactUsSupportRequest;
use ProvulusSDK\Config\ProvulusConfiguration;
use ProvulusSDK\Enum\Contact\Mailgroups\MailGroupCategoriesEnum;
use ProvulusTest\TestCase;

class ContactUsTest extends TestCase
{
    /**
     * @test
     */
    public function create_contact_us_support_success()
    {
        $jsonResult = '{}';
        $jsonRequest
                    = '
        {
            "help_topic": "billing",
            "subject": "Hello subject!",
            "body": "Hello body!"
        }';

        $config = $this->preSuccess($jsonResult);

        $request = new CreateContactUsSupportRequest();

        $request->setOrganizationId(567)
                ->setHelpTopic(MailGroupCategoriesEnum::BILLING())
                ->setSubject('Hello subject!')
                ->setBody('Hello body!');

        $client = new ProvulusClient($config);
        $client->processRequest($request);

        $this->validateRequest($config, $jsonRequest);
        $this->validateRequestUrl($config, ['/orgs/567/contact/support']);
    }

    /**
     * @test
     */
    public function create_contact_us_portal_success()
    {
        $jsonResult = '{}';
        $jsonRequest
                    = '
        {
            "first_name": "John",
            "last_name": "Doe",
            "email": "john.doe@email.com",
            "company_name": "Company Inc.",
            "help_topic": "billing",
            "subject": "Hello subject!",
            "body": "Hello body!"
        }';

        $config = $this->preSuccess($jsonResult);

        $request = new CreateContactUsPortalRequest();

        $request->setOrganizationId(567)
                ->setFirstName("John")
                ->setLastName("Doe")
                ->setCompanyName("Company Inc.")
                ->setEmail("john.doe@email.com")
                ->setHelpTopic(MailGroupCategoriesEnum::BILLING())
                ->setSubject('Hello subject!')
                ->setBody('Hello body!');

        $client = new ProvulusClient($config);
        $client->processRequest($request);

        $this->validateRequest($config, $jsonRequest);
        $this->validateRequestUrl($config, ['/orgs/567/contact/portal']);
    }

    /**
     * Gets config for tests
     *
     * @param MockHandler $handler
     *
     * @return ProvulusConfiguration
     */
    protected function getConfig(MockHandler $handler)
    {
        return parent::getConfig($handler)->setApiKey('test');
    }
}
