<?php
/**
 * Created by PhpStorm.
 * User: pbb
 * Date: 10/02/17
 * Time: 2:05 PM
 */

namespace automated\Requests\Organization;

use Carbon\Carbon;
use DateTimeZone;
use GuzzleHttp\RequestOptions;
use ProvulusSDK\Client\ProvulusClient;
use ProvulusSDK\Client\Request\Organization\Distributor\IndexDistributorRequest;
use ProvulusSDK\Client\Request\Organization\IndexOrganizationRequest;
use ProvulusSDK\Client\Request\Organization\ReadOrganizationRequest;
use ProvulusSDK\Client\Request\Organization\Trial\PostTrialConfirmRequest;
use ProvulusSDK\Client\Request\Organization\UpdateOrganizationRequest;
use ProvulusSDK\Client\Request\Resource\Organization\Logo\PortalRequest;
use ProvulusSDK\Client\Response\Organization\Billing\BillingResponse;
use ProvulusSDK\Client\Response\Organization\Cancellation\CancellationInformationResponse;
use ProvulusSDK\Client\Response\Organization\Cidr\LoginCidrResponse;
use ProvulusSDK\Client\Response\Organization\Logo\PortalResponse;
use ProvulusSDK\Client\Response\Organization\OrganizationResponse;
use ProvulusSDK\Client\Response\Organization\Password\PasswordConfigurationResponse;
use ProvulusSDK\Client\Response\Organization\Trial\PostTrialActionResponse;
use ProvulusSDK\Enum\Billing\BillingPeriodEnum;
use ProvulusSDK\Enum\Locale\LocaleEnum;
use ProvulusSDK\Enum\Login\TwoFactorAuthUsersEnum;
use ProvulusSDK\Enum\Organization\Type\OrganizationTypeEnum;
use ProvulusSDK\Enum\PaymentCurrency\PaymentCurrencyEnum;
use ProvulusSDK\Enum\SearchCriteria\ActiveCriteriaEnum;
use ProvulusSDK\Enum\Trial\PostTrialStateEnum;
use ProvulusSDK\Utils\Date\DateUtil;
use ProvulusTest\TestCaseApiKey;

/**
 * Class OrganizationTest
 *
 * Tests for Organizations requests
 *
 * @package automated\Requests\Organization
 */
class OrganizationTest extends TestCaseApiKey
{

    /**
     * @test
     **/
    public function read_organization_test_success()
    {
        $jsonResult
            = '{  
                   "data":{  
                      "type":"orgs",
                      "id":"3288",
                      "attributes":{  
                         "support_organization_id":1,
                         "billing_organization_id":1,
                         "is_access_allowed":true,
                         "is_billing_organization":false,
                         "is_support_organization":false,
                         "corporate_name":"tarla.com Inc",
                         "corporate_name_normalized":"tarla.com inc",
                         "portal_prefix":null,
                         "default_language_code":"fr_ca",
                         "timezone_code":{  
                            "timezone_type":3,
                            "timezone":"America\/Montreal"
                         },
                         "note_shared":null,
                         "quar_expiry_in_days":14,
                         "note_for_zs_only":null,
                         "logo_reference":null,
                         "is_active":true,
                         "version":0,
                         "created_at":"2017-02-09 18:46:16.000000 +00:00",
                         "updated_at":"2017-02-09 19:21:35.947487 +00:00",
                         "dsbl_portal_selfprov":false,
                         "dsbl_org_selfprov":false,
                         "currency":"cad",
                         "renewal_messages_allowed":true,
                         "renewal_note":null,
                         "yearly_renewal_date":"2018-02-09 00:00:00.000000 +00:00",
                         "renewal_state":"ok",
                         "renewal_msg_sent_at":null,
                         "next_renewal_msg":null,
                         "billing_period":null,
                         "billing_base_date":null,
                         "salesforce_id":"0012C00000BE8CqQAL",
                         "organization_type_enum": "NORMAL",
                         "shows_powered_by_logo":true,
                         "is_distributor_organization":false,
                         "distributor_id":null,
                         "external_distributor_id":null,
                         "activation_date":null
                      }
                   }
                }';

        $config = $this->preSuccess($jsonResult);
        $request = new ReadOrganizationRequest();
        $request->setOrganizationId(5);

        $client = new ProvulusClient($config);
        $client->processRequest($request);


        $response = $request->getResponse();

        $this->assertInstanceOf(DateTimeZone::class, $response->timezone_code);
        $this->assertEquals($response->timezone_code->getName(), 'America/Montreal');
        $this->assertEquals($response->created_at->format(DateUtil::getZerospamDateFormat()), '2017-02-09 18:46:16.000000 +00:00');
        $this->assertTrue(LocaleEnum::FR_CA()->is($response->default_language_code));
        $this->assertTrue($response->organization_type_enum->is(OrganizationTypeEnum::NORMAL()));
    }

    /**
     * @test
     **/
    public function read_organization_with_cancellation_test_success()
    {
        $jsonResult = '{
            "data": {
                "type": "orgs",
                "id": "23",
                "attributes": {
                    "support_organization_id": 1565,
                    "billing_organization_id": 1565,
                    "is_access_allowed": true,
                    "is_billing_organization": false,
                    "is_support_organization": false,
                    "corporate_name": "Corporate inc.",
                    "corporate_name_normalized": "Corporate inc.",
                    "portal_prefix": null,
                    "default_language_code": "en_us",
                    "timezone_code": {
                        "timezone_type": 3,
                        "timezone": "America/Montreal"
                    },
                    "note_shared": null,
                    "quar_expiry_in_days": 14,
                    "note_for_zs_only": null,
                    "lockdown_value": 3,
                    "logo_reference": null,
                    "is_active": true,
                    "created_at": "2016-07-25 15:05:43.000000 +00:00",
                    "updated_at": "2018-02-05 21:39:29.289141 +00:00",
                    "dsbl_portal_selfprov": false,
                    "dsbl_org_selfprov": false,
                    "currency": "cad",
                    "renewal_messages_allowed": true,
                    "renewal_note": null,
                    "yearly_renewal_date": "2018-02-01 05:00:00.000000 +00:00",
                    "renewal_state": "renewing",
                    "renewal_msg_sent_at": "2017-01-03 08:32:42.581890 +00:00",
                    "next_renewal_msg": "2017-01-10 05:00:00.000000 +00:00",
                    "billing_period": "monthly",
                    "billing_base_date": "2016-08-01 00:00:00.000000 +00:00",
                    "salesforce_id": null,
                    "shows_powered_by_logo": true,
                    "is_distributor_organization": false,
                    "distributor_id": null,
                    "external_distributor_id": null,
                    "activation_date": null,
                    "trial_end_date": "2017-09-25 21:16:13.000000 +00:00",
                    "trial_notification_sent_at": null,
                    "organization_type_enum": "RESELLER_CLIENT",
                    "person_of_interest_match_rule": "strict",
                    "can_grant_trial": true,
                    "has_trial": true,
                    "is_clients_notified_end_trial": true,
                    "is_notified_end_trial": true,
                    "two_factor_auth_users": "optional",
                    "is_two_factor_auth_mandatory": false,
                    "next_billing_date": "2018-08-01 00:00:00.000000 +00:00"
                },
                "relationships": {
                    "cancellationInformation": {
                        "data": {
                            "type": "cancellation-information",
                            "id": "11"
                        }
                    }
                }
            },
            "included": [
                {
                    "type": "cancellation-information",
                    "id": "11",
                    "attributes": {
                        "organization_id": 2651,
                        "reason": "Bankrupty",
                        "target_date": "2018-02-28 05:00:00.000000 +00:00",
                        "from_display_name": "Doe, John (j.doe@company.com)",
                        "from_corporate_name": "Company",
                        "is_active": true,
                        "executed_at": null,
                        "created_at": "2018-02-06 15:28:32.000000 +00:00",
                        "updated_at": "2018-02-06 15:28:32.000000 +00:00"
                    }
                }
            ]}';

        $config = $this->preSuccess($jsonResult);
        $request = new ReadOrganizationRequest();
        $request->setOrganizationId(23)
            ->withCancellationInformation();

        $client = new ProvulusClient($config);
        $client->processRequest($request);


        $response = $request->getResponse();

        $cancellationInformation = $response->cancellation_information;

        $this->assertInstanceOf(CancellationInformationResponse::class, $cancellationInformation);
        $this->assertEquals("Bankrupty", $cancellationInformation->reason);
        $this->assertEquals("Doe, John (j.doe@company.com)", $cancellationInformation->from_display_name);
        $this->assertEquals("Company", $cancellationInformation->from_corporate_name);
        $this->assertNull($cancellationInformation->executed_at);
        $this->assertEquals("2018-02-06 15:28:32.000000 +00:00", $cancellationInformation->created_at->format(DateUtil::getZerospamDateFormat()));
        $this->assertEquals("2018-02-06 15:28:32.000000 +00:00", $cancellationInformation->updated_at->format(DateUtil::getZerospamDateFormat()));

        $this->validateRequestUrl($config, ['/orgs/23?include=cancellationInformation']);
    }

    /** @test */
    public function read_organization_with_billing()
    {
        $jsonResponse = <<<RESPONSE
{
    "data": {
        "type": "orgs",
        "id": "4",
        "attributes": {
            "support_organization_id": 1,
            "billing_organization_id": 1,
            "is_access_allowed": true,
            "is_billing_organization": false,
            "is_support_organization": false,
            "corporate_name": "Organization Inc.",
            "corporate_name_normalized": "Organization Inc",
            "portal_prefix": null,
            "default_language_code": "fr_ca",
            "timezone_code": {
                "timezone_type": 3,
                "timezone": "America/Montreal"
            },
            "note_shared": null,
            "quar_expiry_in_days": 14,
            "note_for_zs_only": null,
            "lockdown_value": 10,
            "logo_reference": null,
            "is_active": true,
            "created_at": "2011-11-11 11:11:11.000000 +00:00",
            "updated_at": "2018-08-18 18:18:18.181818 +00:00",
            "dsbl_portal_selfprov": false,
            "dsbl_org_selfprov": false,
            "renewal_messages_allowed": true,
            "renewal_note": null,
            "yearly_renewal_date": "2016-03-01 04:00:00.000000 +00:00",
            "renewal_state": "ok",
            "renewal_msg_sent_at": "2015-02-18 10:00:00.000000 +00:00",
            "next_renewal_msg": "2016-05-18 10:00:00.000000 +00:00",
            "billing_period": "monthly",
            "billing_base_date": "2000-01-01 00:00:00.000000 +00:00",
            "salesforce_id": "id",
            "shows_powered_by_logo": true,
            "is_distributor_organization": false,
            "distributor_id": null,
            "external_distributor_id": null,
            "activation_date": "2011-11-23 00:00:00.000000 +00:00",
            "trial_end_date": "2011-10-01 16:00:00.000000 +00:00",
            "trial_notification_sent_at": "2011-09-27 16:00:00.000000 +00:00",
            "organization_type_enum": "NORMAL",
            "logo_reference_old": null,
            "can_grant_trial": true,
            "has_trial": true,
            "is_clients_notified_end_trial": true,
            "is_notified_end_trial": true,
            "person_of_interest_match_rule": "strict",
            "two_factor_auth_users": "optional",
            "is_two_factor_auth_mandatory": false,
            "currency": "cad",
            "next_billing_date": "2018-10-01 00:00:00.000000 +00:00",
            "end_billing_period": "2018-10-01 00:00:00.000000 +00:00",
            "payment_state_type": "paying"
        },
        "relationships": {
            "billing": {
                "data": {
                    "type": "billing",
                    "id": "4"
                }
            }
        }
    },
    "included": [
        {
            "type": "billing",
            "id": "4",
            "attributes": {
                "currency": "cad",
                "city": "Montréal",
                "country": "CA",
                "postal_code": "H0H 0H0",
                "state": "QC",
                "street1": "8 rue Principale",
                "street2": "",
                "created_at": "2018-09-24 18:41:56.000000 +00:00",
                "updated_at": "2018-10-25 18:41:56.000000 +00:00",
                "email": "email@email.com",
                "last_name": "Smith",
                "first_name": "John"
            }
        }
    ]
}
RESPONSE;

        $config = $this->preSuccess($jsonResponse);
        $request = new ReadOrganizationRequest();
        $request->setOrganizationId(4)
            ->withBilling();

        $client = new ProvulusClient($config);
        $client->processRequest($request);

        $response = $response = $request->getResponse();

        $billingResponse = $response->billing;

        self::assertInstanceOf(BillingResponse::class, $billingResponse);
        self::assertTrue($billingResponse->currency->is(PaymentCurrencyEnum::CAD()));
        self::assertEquals("Montréal", $billingResponse->city);
        self::assertEquals("CA", $billingResponse->country);
        self::assertEquals("H0H 0H0", $billingResponse->postal_code);
        self::assertEquals("QC", $billingResponse->state);
        self::assertEquals("8 rue Principale", $billingResponse->street1);
        self::assertEquals("", $billingResponse->street2);
        self::assertTrue($billingResponse->created_at->eq(Carbon::parse("2018-09-24 18:41:56.000000 +00:00")));
        self::assertTrue($billingResponse->updated_at->eq(Carbon::parse("2018-10-25 18:41:56.000000 +00:00")));
        self::assertEquals("email@email.com", $billingResponse->email);
        self::assertEquals("Smith", $billingResponse->last_name);
        self::assertEquals("John", $billingResponse->first_name);

        $this->validateRequestUrl($config, ['/orgs/4?include=billing']);
    }

    /** @test */
    public function read_organization_with_post_trial_action()
    {
        $jsonResponse = <<<RESPONSE
{
    "data": {
        "type": "orgs",
        "id": "3944",
        "attributes": {
            "support_organization_id": 1234,
            "billing_organization_id": 1234,
            "is_access_allowed": true,
            "is_billing_organization": false,
            "is_support_organization": false,
            "corporate_name": "Client Inc.",
            "corporate_name_normalized": "Client Inc.",
            "portal_prefix": null,
            "default_language_code": "fr_ca",
            "timezone_code": {
                "timezone_type": 3,
                "timezone": "America/Montreal"
            },
            "note_shared": null,
            "quar_expiry_in_days": 14,
            "note_for_zs_only": null,
            "lockdown_value": 3,
            "logo_reference": null,
            "is_active": true,
            "created_at": "2018-04-01 16:00:00.000000 +00:00",
            "updated_at": "2018-05-01 19:00:00.000000 +00:00",
            "dsbl_portal_selfprov": false,
            "dsbl_org_selfprov": false,
            "renewal_messages_allowed": true,
            "renewal_note": null,
            "yearly_renewal_date": "2019-01-01 05:00:00.000000 +00:00",
            "renewal_state": "ok",
            "renewal_msg_sent_at": null,
            "next_renewal_msg": null,
            "billing_period": "monthly",
            "billing_base_date": "2018-06-01 00:00:00.000000 +00:00",
            "salesforce_id": "id",
            "shows_powered_by_logo": true,
            "is_distributor_organization": false,
            "distributor_id": null,
            "external_distributor_id": null,
            "activation_date": "2018-04-02 00:00:00.000000 +00:00",
            "trial_end_date": "2018-05-02 16:00:00.000000 +00:00",
            "trial_notification_sent_at": "2018-04-20 09:00:00.000000 +00:00",
            "organization_type_enum": "RESELLER_CLIENT",
            "logo_reference_old": null,
            "can_grant_trial": true,
            "has_trial": true,
            "is_clients_notified_end_trial": true,
            "is_notified_end_trial": true,
            "person_of_interest_match_rule": "strict",
            "two_factor_auth_users": "optional",
            "is_two_factor_auth_mandatory": false,
            "currency": "cad",
            "next_billing_date": "2018-10-01 00:00:00.000000 +00:00",
            "end_billing_period": "2018-10-01 00:00:00.000000 +00:00",
            "payment_state_type": "paying"
        },
        "relationships": {
            "postTrialAction": {
                "data": {
                    "type": "post-trial-action",
                    "id": "556"
                }
            }
        }
    },
    "included": [
        {
            "type": "post-trial-action",
            "id": "556",
            "attributes": {
                "organization_id": 3944,
                "comment": "C'est bien ça",
                "state": "confirm",
                "created_at": "2018-05-06 11:00:00.000000 +00:00",
                "updated_at": "2018-05-07 11:00:00.000000 +00:00",
                "notification_sent_at": "2018-05-08 11:00:00.000000 +00:00"
            }
        }
    ]
}
RESPONSE;
        $config = $this->preSuccess($jsonResponse);
        $request = new ReadOrganizationRequest();
        $request->setOrganizationId(3944)
            ->withPostTrialAction();

        $client = new ProvulusClient($config);
        $client->processRequest($request);

        $response = $response = $request->getResponse();

        $postTrialActionResponse = $response->post_trial_action;

        self::assertInstanceOf(PostTrialActionResponse::class, $postTrialActionResponse);
        self::assertEquals(3944, $postTrialActionResponse->organization_id);
        self::assertEquals("C'est bien ça", $postTrialActionResponse->comment);
        self::assertTrue($postTrialActionResponse->state->is(PostTrialStateEnum::CONFIRM()));
        self::assertTrue($postTrialActionResponse->created_at->eq(Carbon::parse("2018-05-06 11:00:00.000000 +00:00")));
        self::assertTrue($postTrialActionResponse->updated_at->eq(Carbon::parse("2018-05-07 11:00:00.000000 +00:00")));
        self::assertTrue($postTrialActionResponse->notification_sent_at->eq(Carbon::parse("2018-05-08 11:00:00.000000 +00:00")));

        $this->validateRequestUrl($config, ['/orgs/3944?include=postTrialAction']);
    }

    /** @test */
    public function read_organization_with_password_configuration()
    {
        $jsonResponse = '{
            "data": {
                "type": "orgs",
                "id": "1984",
                "attributes": {
                    "support_organization_id": 1,
                    "billing_organization_id": 1,
                    "is_access_allowed": true,
                    "is_billing_organization": false,
                    "is_support_organization": false,
                    "corporate_name": "Corporate Inc.",
                    "corporate_name_normalized": "Corporate Inc",
                    "portal_prefix": null,
                    "default_language_code": "fr_ca",
                    "timezone_code": {
                        "timezone_type": 3,
                        "timezone": "America/Montreal"
                    },
                    "note_shared": null,
                    "quar_expiry_in_days": 14,
                    "note_for_zs_only": null,
                    "lockdown_value": 10,
                    "logo_reference": null,
                    "is_active": true,
                    "created_at": "2014-01-30 15:12:12.000000 +00:00",
                    "updated_at": "2017-08-28 18:55:53.072303 +00:00",
                    "dsbl_portal_selfprov": false,
                    "dsbl_org_selfprov": true,
                    "portal_currency": "cad",
                    "renewal_messages_allowed": true,
                    "renewal_note": null,
                    "yearly_renewal_date": "2017-03-01 05:00:00.000000 +00:00",
                    "renewal_state": "blocked",
                    "renewal_msg_sent_at": "2016-02-23 08:32:05.475102 +00:00",
                    "next_renewal_msg": null,
                    "billing_period": "annual",
                    "billing_base_date": "2014-04-01 00:00:00.000000 +00:00",
                    "salesforce_id": null,
                    "shows_powered_by_logo": true,
                    "is_distributor_organization": false,
                    "distributor_id": null,
                    "external_distributor_id": null,
                    "activation_date": null,
                    "trial_end_date": "2014-03-01 15:12:12.000000 +00:00",
                    "trial_notification_sent_at": "2014-02-22 15:12:12.000000 +00:00",
                    "organization_type_enum": "NORMAL",
                    "person_of_interest_match_rule": "strict",
                    "can_grant_trial": true,
                    "has_trial": true,
                    "is_clients_notified_end_trial": true,
                    "is_notified_end_trial": true,
                    "two_factor_auth_users": "optional",
                    "is_two_factor_auth_mandatory": false,
                    "next_billing_date": "2019-04-01 00:00:00.000000 +00:00",
                    "end_billing_period": "2019-04-01 00:00:00.000000 +00:00",
                    "payment_state": "paying"
                },
                "relationships": {
                    "passwordConfiguration": {
                        "data": {
                            "type": "password-configuration",
                            "id": "1984"
                        }
                    }
                }
            },
            "included": [{
                "type": "password-configuration",
                "id": "1984",
                "attributes": {
                    "min_length": 12,
                    "min_lower": 2,
                    "min_upper": 3,
                    "min_digits": 0,
                    "min_specials": 1,
                    "life_in_days": 30,
                    "history_count": 13,
                    "created_at": "2014-01-30 15:12:12.000000 +00:00",
                    "updated_at": "2017-08-28 18:55:53.000000 +00:00"
                }
            }]
        }';

        $config = $this->preSuccess($jsonResponse);
        $request = new ReadOrganizationRequest();
        $request->setOrganizationId(1984)
            ->withPasswordConfiguration();

        $client = new ProvulusClient($config);
        $client->processRequest($request);

        $response = $response = $request->getResponse();

        $passwordConfiguration = $response->password_configuration;

        self::assertInstanceOf(PasswordConfigurationResponse::class, $passwordConfiguration);
        self::assertEquals(12, $passwordConfiguration->min_length);
        self::assertEquals(2, $passwordConfiguration->min_lower);
        self::assertEquals(3, $passwordConfiguration->min_upper);
        self::assertEquals(0, $passwordConfiguration->min_digits);
        self::assertEquals(1, $passwordConfiguration->min_specials);
        self::assertEquals(30, $passwordConfiguration->life_in_days);
        self::assertEquals(13, $passwordConfiguration->history_count);

        $this->validateRequestUrl($config, ['/orgs/1984?include=passwordConfiguration']);
    }

    /** @test */
    public function read_organization_with_login_cidr()
    {
        $jsonResponse = '{
            "data": {
                "type": "orgs",
                "id": "34",
                "attributes": {
                    "support_organization_id": 1,
                    "billing_organization_id": 1,
                    "is_access_allowed": true,
                    "is_billing_organization": false,
                    "is_support_organization": false,
                    "corporate_name": "Company co.",
                    "corporate_name_normalized": "Company co.",
                    "portal_prefix": null,
                    "default_language_code": "fr_ca",
                    "timezone_code": {
                        "timezone_type": 3,
                        "timezone": "America/Montreal"
                    },
                    "note_shared": null,
                    "quar_expiry_in_days": 14,
                    "note_for_zs_only": null,
                    "lockdown_value": 10,
                    "logo_reference": null,
                    "is_active": true,
                    "created_at": "2014-01-30 15:12:12.000000 +00:00",
                    "updated_at": "2017-08-28 18:55:53.072303 +00:00",
                    "dsbl_portal_selfprov": false,
                    "dsbl_org_selfprov": true,
                    "portal_currency": "cad",
                    "renewal_messages_allowed": true,
                    "renewal_note": null,
                    "yearly_renewal_date": "2017-03-01 05:00:00.000000 +00:00",
                    "renewal_state": "blocked",
                    "renewal_msg_sent_at": "2016-02-23 08:32:05.475102 +00:00",
                    "next_renewal_msg": null,
                    "billing_period": "annual",
                    "billing_base_date": "2014-04-01 00:00:00.000000 +00:00",
                    "salesforce_id": null,
                    "shows_powered_by_logo": true,
                    "is_distributor_organization": false,
                    "distributor_id": null,
                    "external_distributor_id": null,
                    "activation_date": null,
                    "trial_end_date": "2014-03-01 15:12:12.000000 +00:00",
                    "trial_notification_sent_at": "2014-02-22 15:12:12.000000 +00:00",
                    "organization_type_enum": "NORMAL",
                    "person_of_interest_match_rule": "strict",
                    "can_grant_trial": true,
                    "has_trial": true,
                    "is_clients_notified_end_trial": true,
                    "is_notified_end_trial": true,
                    "two_factor_auth_users": "optional",
                    "is_two_factor_auth_mandatory": false,
                    "next_billing_date": "2019-04-01 00:00:00.000000 +00:00",
                    "end_billing_period": "2019-04-01 00:00:00.000000 +00:00",
                    "payment_state": "paying"
                },
                "relationships": {
                    "loginCidrs": {
                        "data": [{
                            "type": "login-cidr",
                            "id": "8"
                        },{
                            "type": "login-cidr",
                            "id": "9"
                        },{
                            "type": "login-cidr",
                            "id": "10"
                        }]
                    }
                }
            },
            "included": [{
                "type": "login-cidr",
                "id": "8",
                "attributes": {
                    "organization_id": 34,
                    "cidr": "199.199.199.199/32",
                    "created_at": "2017-08-28 18:55:53.000000 +00:00",
                    "updated_at": "2017-08-28 18:55:53.000000 +00:00"
                }
            },{
                "type": "login-cidr",
                "id": "9",
                "attributes": {
                    "organization_id": 34,
                    "cidr": "10.0.0.0/24",
                    "created_at": "2017-08-28 18:55:53.000000 +00:00",
                    "updated_at": "2017-08-28 18:55:53.000000 +00:00"
                }
            },{
                "type": "login-cidr",
                "id": "10",
                "attributes": {
                    "organization_id": 34,
                    "cidr": "2.2.2.2/32",
                    "created_at": "2017-08-28 18:55:53.000000 +00:00",
                    "updated_at": "2017-08-28 18:55:53.000000 +00:00"
                }
            }]
        }';
        $config = $this->preSuccess($jsonResponse);
        $request = new ReadOrganizationRequest();
        $request->setOrganizationId(34)
            ->withLoginCidr();

        $client = new ProvulusClient($config);
        $client->processRequest($request);

        $response = $response = $request->getResponse();

        $loginCidrs = $response->login_cidrs;
        /** @var LoginCidrResponse $first */
        $first = $loginCidrs->first();

        $this->validateRequestUrl($config, ['/orgs/34?include=loginCidrs']);

        self::assertEquals("199.199.199.199/32", $first->cidr);
        self::assertEquals(34, $first->organization_id);
    }

    /**
     * @test
     **/
    public function indexOrganizationsWithCorporateNameArgument()
    {
        $jsonResult
            = '{  
               "data":[  
                  {  
                     "type":"orgs",
                     "id":"2865",
                     "attributes":{  
                        "support_organization_id":2864,
                        "billing_organization_id":2864,
                        "is_access_allowed":true,
                        "is_billing_organization":false,
                        "is_support_organization":false,
                        "corporate_name":"zozo.com Client",
                        "corporate_name_normalized":"zozo.com client",
                        "portal_prefix":null,
                        "default_language_code":"en_us",
                        "timezone_code":{  
                           "timezone_type":3,
                           "timezone":"America\/Montreal"
                        },
                        "note_shared":null,
                        "quar_expiry_in_days":14,
                        "note_for_zs_only":null,
                        "logo_reference":null,
                        "is_active":true,
                        "version":0,
                        "created_at":"2016-11-30 20:30:58.000000 +00:00",
                        "updated_at":"2017-01-31 16:12:37.758969 +00:00",
                        "dsbl_portal_selfprov":false,
                        "dsbl_org_selfprov":false,
                        "currency":"cad",
                        "renewal_messages_allowed":true,
                        "renewal_note":null,
                        "yearly_renewal_date":"2017-02-08 16:12:32.166527 +00:00",
                        "renewal_state":"renewing",
                        "renewal_msg_sent_at":"2017-01-25 19:14:58.000000 +00:00",
                        "next_renewal_msg":null,
                        "billing_period":null,
                        "billing_base_date":null,
                        "salesforce_id":"0012C000006ptZbQAI",
                        "shows_powered_by_logo":true,
                        "is_distributor_organization":false,
                        "distributor_id":null,
                        "external_distributor_id":null,
                        "activation_date":null
                     }
                  },
                  {  
                     "type":"orgs",
                     "id":"2864",
                     "attributes":{  
                        "support_organization_id":1,
                        "billing_organization_id":1,
                        "is_access_allowed":true,
                        "is_billing_organization":true,
                        "is_support_organization":true,
                        "corporate_name":"zozo.com Reseller",
                        "corporate_name_normalized":"zozo.com reseller",
                        "portal_prefix":null,
                        "default_language_code":"en_us",
                        "timezone_code":{  
                           "timezone_type":3,
                           "timezone":"America\/Montreal"
                        },
                        "note_shared":null,
                        "quar_expiry_in_days":14,
                        "note_for_zs_only":null,
                        "logo_reference":null,
                        "is_active":true,
                        "version":0,
                        "created_at":"2016-11-30 20:30:42.000000 +00:00",
                        "updated_at":"2017-01-31 16:12:37.681101 +00:00",
                        "dsbl_portal_selfprov":false,
                        "dsbl_org_selfprov":false,
                        "currency":"cad",
                        "renewal_messages_allowed":true,
                        "renewal_note":null,
                        "yearly_renewal_date":null,
                        "renewal_state":"no_domain",
                        "renewal_msg_sent_at":null,
                        "next_renewal_msg":null,
                        "billing_period":null,
                        "billing_base_date":null,
                        "salesforce_id":"0012C000006ptYxQAI",
                        "shows_powered_by_logo":true,
                        "outbound_dg_bounce":null,
                        "outbound_dg_default":null,
                        "outbound_global_domains":true,
                        "outbound_filter_active":false,
                        "outbound_relay":null,
                        "is_distributor_organization":false,
                        "distributor_id":null,
                        "external_distributor_id":null,
                        "activation_date":null
                     }
                  }
               ],
               "meta":{  
                  "pagination":{  
                     "total":2,
                     "count":2,
                     "per_page":15,
                     "current_page":1,
                     "total_pages":1
                  }
               }
            }';


        $config = $this->preSuccess($jsonResult);
        $request = new IndexOrganizationRequest();
        $request->setCorporateName('zozo')
            ->setCorporateName(null)
            ->setCorporateName('zozo');

        $client = new ProvulusClient($config);
        $client->processRequest($request);


        $response = $request->getResponse();

        $this->assertEquals(2864, $response->data()->first()->support_organization_id);
        $response->data()->each(function (OrganizationResponse $response) {
            self::assertContains('zozo', $response->corporate_name_normalized);
        });
    }


    /**
     * @test
     **/
    public function indexOrganizationWithSupportedByArgument()
    {
        $jsonResult
            = '{  
                   "data":[  
                      {  
                         "type":"orgs",
                         "id":"2901",
                         "attributes":{  
                            "support_organization_id":2864,
                            "billing_organization_id":2864,
                            "is_access_allowed":true,
                            "is_billing_organization":false,
                            "is_support_organization":false,
                            "corporate_name":"zozo2.com Inc",
                            "corporate_name_normalized":"zozo2.com inc",
                            "portal_prefix":null,
                            "default_language_code":"fr_ca",
                            "timezone_code":{  
                               "timezone_type":3,
                               "timezone":"America\/Montreal"
                            },
                            "note_shared":null,
                            "quar_expiry_in_days":14,
                            "note_for_zs_only":null,
                            "logo_reference":null,
                            "is_active":true,
                            "version":0,
                            "created_at":"2017-02-13 14:42:02.000000 +00:00",
                            "updated_at":"2017-02-13 14:42:02.000000 +00:00",
                            "dsbl_portal_selfprov":false,
                            "dsbl_org_selfprov":false,
                            "currency":"cad",
                            "renewal_messages_allowed":true,
                            "renewal_note":null,
                            "yearly_renewal_date":"2018-02-13 00:00:00.000000 +00:00",
                            "renewal_state":"ok",
                            "renewal_msg_sent_at":null,
                            "next_renewal_msg":null,
                            "billing_period":null,
                            "billing_base_date":null,
                            "salesforce_id":null,
                            "shows_powered_by_logo":true,
                            "is_distributor_organization":false,
                            "distributor_id":null,
                            "external_distributor_id":null,
                            "activation_date":null
                         }
                      },
                      {  
                         "type":"orgs",
                         "id":"2865",
                         "attributes":{  
                            "support_organization_id":2864,
                            "billing_organization_id":2864,
                            "is_access_allowed":true,
                            "is_billing_organization":false,
                            "is_support_organization":false,
                            "corporate_name":"zozo.com Client",
                            "corporate_name_normalized":"zozo.com client",
                            "portal_prefix":null,
                            "default_language_code":"en_us",
                            "timezone_code":{  
                               "timezone_type":3,
                               "timezone":"America\/Montreal"
                            },
                            "note_shared":null,
                            "quar_expiry_in_days":14,
                            "note_for_zs_only":null,
                            "logo_reference":null,
                            "is_active":true,
                            "version":0,
                            "created_at":"2016-11-30 20:30:58.000000 +00:00",
                            "updated_at":"2017-01-31 16:12:37.758969 +00:00",
                            "dsbl_portal_selfprov":false,
                            "dsbl_org_selfprov":false,
                            "currency":"cad",
                            "renewal_messages_allowed":true,
                            "renewal_note":null,
                            "yearly_renewal_date":"2017-02-08 16:12:32.166527 +00:00",
                            "renewal_state":"renewing",
                            "renewal_msg_sent_at":"2017-01-25 19:14:58.000000 +00:00",
                            "next_renewal_msg":null,
                            "billing_period":null,
                            "billing_base_date":null,
                            "salesforce_id":"0012C000006ptZbQAI",
                            "shows_powered_by_logo":true,
                            "is_distributor_organization":false,
                            "distributor_id":null,
                            "external_distributor_id":null,
                            "activation_date":null
                         }
                      }
                   ],
                   "meta":{  
                      "pagination":{  
                         "total":2,
                         "count":2,
                         "per_page":15,
                         "current_page":1,
                         "total_pages":1
                      }
                   }
                }';

        $config = $this->preSuccess($jsonResult);
        $request = new IndexOrganizationRequest();
        $request->setSupportedBy(2864)
            ->setSupportedBy(null)
            ->setSupportedBy(2864);

        $client = new ProvulusClient($config);
        $client->processRequest($request);

        $response = $request->getResponse();

        $response->data()->each(function (OrganizationResponse $response) {
            self::assertEquals(2864, $response->support_organization_id);
        });
    }

    /**
     * @test
     **/
    public function indexDistributors()
    {
        $jsonResult
            = '{
  "data": [
    {
      "type": "orgs",
      "id": "3313",
      "attributes": {
        "support_organization_id": 1,
        "billing_organization_id": 1,
        "is_access_allowed": true,
        "is_billing_organization": false,
        "is_support_organization": false,
        "corporate_name": "StreamOne epic.ca Distributor",
        "corporate_name_normalized": "streamone epic.ca distributor",
        "portal_prefix": null,
        "default_language_code": "fr_ca",
        "timezone_code": {
          "timezone_type": 3,
          "timezone": "America/Montreal"
        },
        "note_shared": null,
        "quar_expiry_in_days": 14,
        "note_for_zs_only": null,
        "lockdown_value": 3,
        "logo_reference": null,
        "is_active": true,
        "created_at": "2017-03-20 17:17:25.504642 +00:00",
        "updated_at": "2017-07-24 13:31:39.183490 +00:00",
        "dsbl_portal_selfprov": false,
        "dsbl_org_selfprov": false,
        "currency": "cad",
        "renewal_messages_allowed": true,
        "renewal_note": null,
        "yearly_renewal_date": null,
        "renewal_state": "no_domain",
        "renewal_msg_sent_at": null,
        "next_renewal_msg": null,
        "billing_period": "annual",
        "billing_base_date": "2017-05-01 00:00:00.000000 +00:00",
        "salesforce_id": "0012C00000C2QESQA3",
        "shows_powered_by_logo": true,
        "is_distributor_organization": true,
        "distributor_id": null,
        "external_distributor_id": null,
        "activation_date": null
      }
    }
  ],
  "meta": {
    "pagination": {
      "total": 1,
      "count": 1,
      "per_page": 15,
      "current_page": 1,
      "total_pages": 1
    }
  },
  "links": {
    "self": "http://cumulus.local/api/v1/distributors?page=1",
    "first": "http://cumulus.local/api/v1/distributors?page=1",
    "last": "http://cumulus.local/api/v1/distributors?page=1"
  }
}';

        $config = $this->preSuccess($jsonResult);
        $request = new IndexDistributorRequest();

        $client = new ProvulusClient($config);
        $client->processRequest($request);

        $response = $request->getResponse();

        $response->data()->each(function (OrganizationResponse $response) {
            self::assertTrue($response->is_distributor_organization);
        });
    }

    /**
     * @test
     **/
    public function indexOrganizationWithDistributedByArgument()
    {
        $jsonResult
            = '{
  "data": [
    {
      "type": "orgs",
      "id": "3332",
      "attributes": {
        "support_organization_id": 1,
        "billing_organization_id": 1,
        "is_access_allowed": true,
        "is_billing_organization": true,
        "is_support_organization": true,
        "corporate_name": "Cloud Innovations",
        "corporate_name_normalized": "cloud innovations",
        "portal_prefix": null,
        "default_language_code": "en_us",
        "timezone_code": {
          "timezone_type": 3,
          "timezone": "America/Montreal"
        },
        "note_shared": null,
        "quar_expiry_in_days": 14,
        "note_for_zs_only": null,
        "lockdown_value": 3,
        "logo_reference": null,
        "is_active": true,
        "created_at": "2017-03-20 18:45:50.459631 +00:00",
        "updated_at": "2017-07-24 13:31:39.183490 +00:00",
        "dsbl_portal_selfprov": false,
        "dsbl_org_selfprov": false,
        "currency": "cad",
        "renewal_messages_allowed": true,
        "renewal_note": null,
        "yearly_renewal_date": null,
        "renewal_state": "no_domain",
        "renewal_msg_sent_at": null,
        "next_renewal_msg": null,
        "billing_period": "annual",
        "billing_base_date": "2017-05-01 00:00:00.000000 +00:00",
        "salesforce_id": "qsdqsd",
        "shows_powered_by_logo": true,
        "is_distributor_organization": false,
        "distributor_id": 3313,
        "external_distributor_id": "qsdqsdqsd",
        "activation_date": null
      }
    },
    {
      "type": "orgs",
      "id": "3362",
      "attributes": {
        "support_organization_id": 1,
        "billing_organization_id": 1,
        "is_access_allowed": true,
        "is_billing_organization": true,
        "is_support_organization": true,
        "corporate_name": "Micro test",
        "corporate_name_normalized": "micro test",
        "portal_prefix": null,
        "default_language_code": "en_us",
        "timezone_code": {
          "timezone_type": 3,
          "timezone": "America/Montreal"
        },
        "note_shared": null,
        "quar_expiry_in_days": 14,
        "note_for_zs_only": null,
        "lockdown_value": 3,
        "logo_reference": null,
        "is_active": true,
        "created_at": "2017-04-26 13:34:19.937729 +00:00",
        "updated_at": "2017-07-24 13:31:39.183490 +00:00",
        "dsbl_portal_selfprov": false,
        "dsbl_org_selfprov": false,
        "currency": "cad",
        "renewal_messages_allowed": true,
        "renewal_note": null,
        "yearly_renewal_date": null,
        "renewal_state": "no_domain",
        "renewal_msg_sent_at": null,
        "next_renewal_msg": null,
        "billing_period": "annual",
        "billing_base_date": "2017-06-01 00:00:00.000000 +00:00",
        "salesforce_id": "qsdqsdsq",
        "shows_powered_by_logo": true,
        "is_distributor_organization": false,
        "distributor_id": 3313,
        "external_distributor_id": "sdqsdqsdqsd",
        "activation_date": null
      }
    },
    {
      "type": "orgs",
      "id": "3385",
      "attributes": {
        "support_organization_id": 1,
        "billing_organization_id": 1,
        "is_access_allowed": true,
        "is_billing_organization": true,
        "is_support_organization": true,
        "corporate_name": "Super Com",
        "corporate_name_normalized": "Super Com",
        "portal_prefix": null,
        "default_language_code": "en_us",
        "timezone_code": {
          "timezone_type": 3,
          "timezone": "America/Montreal"
        },
        "note_shared": null,
        "quar_expiry_in_days": 14,
        "note_for_zs_only": null,
        "lockdown_value": 3,
        "logo_reference": null,
        "is_active": true,
        "created_at": "2017-05-12 18:55:45.656507 +00:00",
        "updated_at": "2017-07-24 13:31:39.183490 +00:00",
        "dsbl_portal_selfprov": false,
        "dsbl_org_selfprov": false,
        "currency": "cad",
        "renewal_messages_allowed": true,
        "renewal_note": null,
        "yearly_renewal_date": null,
        "renewal_state": "no_domain",
        "renewal_msg_sent_at": null,
        "next_renewal_msg": null,
        "billing_period": "annual",
        "billing_base_date": "2017-06-01 00:00:00.000000 +00:00",
        "salesforce_id": "sqdqsdqsd",
        "shows_powered_by_logo": true,
        "is_distributor_organization": false,
        "distributor_id": 3313,
        "external_distributor_id": "qsdqsd",
        "activation_date": null
      }
    }
  ],
  "meta": {
    "pagination": {
      "total": 3,
      "count": 3,
      "per_page": 15,
      "current_page": 1,
      "total_pages": 1
    }
  },
  "links": {
    "self": "http://cumulus.local/api/v1/orgs?page=1",
    "first": "http://cumulus.local/api/v1/orgs?page=1",
    "last": "http://cumulus.local/api/v1/orgs?page=1"
  }
}';

        $config = $this->preSuccess($jsonResult);
        $request = new IndexOrganizationRequest();
        $request->setDistributedBy(3313)
            ->setDistributedBy(null)
            ->setDistributedBy(3313);

        $client = new ProvulusClient($config);
        $client->processRequest($request);

        $response = $request->getResponse();

        $response->data()->each(function (OrganizationResponse $response) {
            self::assertEquals(3313, $response->distributor_org);
        });
    }

    /**
     * @test
     **/
    public function indexOrganizationsWithBilledByArguments()
    {
        $jsonResult
            = '{  
               "data":[  
                  {  
                     "type":"orgs",
                     "id":"2901",
                     "attributes":{  
                        "support_organization_id":2864,
                        "billing_organization_id":2864,
                        "is_access_allowed":true,
                        "is_billing_organization":false,
                        "is_support_organization":false,
                        "corporate_name":"zozo2.com Inc",
                        "corporate_name_normalized":"zozo2.com inc",
                        "portal_prefix":null,
                        "default_language_code":"fr_ca",
                        "timezone_code":{  
                           "timezone_type":3,
                           "timezone":"America\/Montreal"
                        },
                        "note_shared":null,
                        "quar_expiry_in_days":14,
                        "note_for_zs_only":null,
                        "logo_reference":null,
                        "is_active":true,
                        "version":0,
                        "created_at":"2017-02-13 14:42:02.000000 +00:00",
                        "updated_at":"2017-02-13 14:42:02.000000 +00:00",
                        "dsbl_portal_selfprov":false,
                        "dsbl_org_selfprov":false,
                        "currency":"cad",
                        "renewal_messages_allowed":true,
                        "renewal_note":null,
                        "yearly_renewal_date":"2018-02-13 00:00:00.000000 +00:00",
                        "renewal_state":"ok",
                        "renewal_msg_sent_at":null,
                        "next_renewal_msg":null,
                        "billing_period":null,
                        "billing_base_date":null,
                        "salesforce_id":null,
                        "shows_powered_by_logo":true,
                        "outbound_dg_bounce":null,
                        "is_distributor_organization":false,
                        "distributor_id":null,
                        "external_distributor_id":null,
                        "activation_date":null
                     }
                  },
                  {  
                     "type":"orgs",
                     "id":"2865",
                     "attributes":{  
                        "support_organization_id":2864,
                        "billing_organization_id":2864,
                        "is_access_allowed":true,
                        "is_billing_organization":false,
                        "is_support_organization":false,
                        "corporate_name":"zozo.com Client",
                        "corporate_name_normalized":"zozo.com client",
                        "portal_prefix":null,
                        "default_language_code":"en_us",
                        "timezone_code":{  
                           "timezone_type":3,
                           "timezone":"America\/Montreal"
                        },
                        "note_shared":null,
                        "quar_expiry_in_days":14,
                        "note_for_zs_only":null,
                        "logo_reference":null,
                        "is_active":true,
                        "version":0,
                        "created_at":"2016-11-30 20:30:58.000000 +00:00",
                        "updated_at":"2017-01-31 16:12:37.758969 +00:00",
                        "dsbl_portal_selfprov":false,
                        "dsbl_org_selfprov":false,
                        "currency":"cad",
                        "renewal_messages_allowed":true,
                        "renewal_note":null,
                        "yearly_renewal_date":"2017-02-08 16:12:32.166527 +00:00",
                        "renewal_state":"renewing",
                        "renewal_msg_sent_at":"2017-01-25 19:14:58.000000 +00:00",
                        "next_renewal_msg":null,
                        "billing_period":null,
                        "billing_base_date":null,
                        "salesforce_id":"0012C000006ptZbQAI",
                        "shows_powered_by_logo":true,
                        "outbound_dg_bounce":null,
                        "outbound_dg_default":null,
                        "outbound_global_domains":true,
                        "outbound_filter_active":false,
                        "outbound_relay":null,
                        "is_distributor_organization":false,
                        "distributor_id":null,
                        "external_distributor_id":null,
                        "activation_date":null
                     }
                  }
               ],
               "meta":{  
                  "pagination":{  
                     "total":2,
                     "count":2,
                     "per_page":15,
                     "current_page":1,
                     "total_pages":1
                  }
               }
            }';

        $config = $this->preSuccess($jsonResult);
        $request = new IndexOrganizationRequest();
        $request->setBilledBy(2864)
            ->setBilledBy(null)
            ->setBilledBy(2864);

        $client = new ProvulusClient($config);
        $client->processRequest($request);

        $response = $request->getResponse();

        $response->data()->each(function (OrganizationResponse $response) {
            self::assertEquals(2864, $response->billing_organization_id);
        });
    }

    /**
     * @test
     **/
    public function indexOrganizationsWithBilledByAndActiveArgument()
    {
        $jsonResult
            = '{  
               "data":[  
                  {  
                     "type":"orgs",
                     "id":"2865",
                     "attributes":{  
                        "support_organization_id":2864,
                        "billing_organization_id":2864,
                        "is_access_allowed":true,
                        "is_billing_organization":false,
                        "is_support_organization":false,
                        "corporate_name":"zozo.com Client",
                        "corporate_name_normalized":"zozo.com client",
                        "portal_prefix":null,
                        "default_language_code":"en_us",
                        "timezone_code":{  
                           "timezone_type":3,
                           "timezone":"America\/Montreal"
                        },
                        "note_shared":null,
                        "quar_expiry_in_days":14,
                        "note_for_zs_only":null,
                        "logo_reference":null,
                        "is_active":false,
                        "version":0,
                        "created_at":"2016-11-30 20:30:58.000000 +00:00",
                        "updated_at":"2017-01-31 16:12:37.758969 +00:00",
                        "dsbl_portal_selfprov":false,
                        "dsbl_org_selfprov":false,
                        "currency":"cad",
                        "renewal_messages_allowed":true,
                        "renewal_note":null,
                        "yearly_renewal_date":"2017-02-08 16:12:32.166527 +00:00",
                        "renewal_state":"renewing",
                        "renewal_msg_sent_at":"2017-01-25 19:14:58.000000 +00:00",
                        "next_renewal_msg":null,
                        "billing_period":null,
                        "billing_base_date":null,
                        "salesforce_id":"0012C000006ptZbQAI",
                        "shows_powered_by_logo":true,
                        "is_distributor_organization":false,
                        "distributor_id":null,
                        "external_distributor_id":null,
                        "activation_date":null
                     }
                  }
               ],
               "meta":{  
                  "pagination":{  
                     "total":1,
                     "count":1,
                     "per_page":15,
                     "current_page":1,
                     "total_pages":1
                  }
               }
            }';

        $config = $this->preSuccess($jsonResult);
        $request = new IndexOrganizationRequest();

        $request->setBilledBy(2864)
            ->setActive(ActiveCriteriaEnum::INACTIVE());

        $client = new ProvulusClient($config);
        $client->processRequest($request);

        $response = $request->getResponse();

        $response->data()->each(function (OrganizationResponse $response) {
            self::assertEquals(2864, $response->billing_organization_id);
            self::assertTrue($response->is_active === false);
        });
    }

    /**
     * @test
     */
    public function index_remove_arg()
    {
        $request = new IndexOrganizationRequest();
        $request->setActive(ActiveCriteriaEnum::ALL());

        $this->assertEquals('all', $request->requestOptions()[RequestOptions::QUERY]['active']);

        $request->setActive(null);

        $this->assertArrayNotHasKey('active', $request->requestOptions()[RequestOptions::QUERY]);
    }

    /**
     * @test
     **/
    public function indexOrganizationsWithDomainNameArgument()
    {
        $jsonResult
            = '{  
               "data":[  
                  {  
                     "type":"orgs",
                     "id":"2865",
                     "attributes":{  
                        "support_organization_id":2864,
                        "billing_organization_id":2864,
                        "is_access_allowed":true,
                        "is_billing_organization":false,
                        "is_support_organization":false,
                        "corporate_name":"zozo.com Client",
                        "corporate_name_normalized":"zozo.com client",
                        "portal_prefix":null,
                        "default_language_code":"en_us",
                        "timezone_code":{  
                           "timezone_type":3,
                           "timezone":"America\/Montreal"
                        },
                        "note_shared":null,
                        "quar_expiry_in_days":14,
                        "note_for_zs_only":null,
                        "logo_reference":null,
                        "is_active":true,
                        "version":0,
                        "created_at":"2016-11-30 20:30:58.000000 +00:00",
                        "updated_at":"2017-01-31 16:12:37.758969 +00:00",
                        "dsbl_portal_selfprov":false,
                        "dsbl_org_selfprov":false,
                        "currency":"cad",
                        "renewal_messages_allowed":true,
                        "renewal_note":null,
                        "yearly_renewal_date":"2017-02-08 16:12:32.166527 +00:00",
                        "renewal_state":"renewing",
                        "renewal_msg_sent_at":"2017-01-25 19:14:58.000000 +00:00",
                        "next_renewal_msg":null,
                        "billing_period":null,
                        "billing_base_date":null,
                        "salesforce_id":"0012C000006ptZbQAI",
                        "shows_powered_by_logo":true,
                        "is_distributor_organization":false,
                        "distributor_id":null,
                        "external_distributor_id":null,
                        "activation_date":null
                     }
                  },
                  {  
                     "type":"orgs",
                     "id":"2901",
                     "attributes":{  
                        "support_organization_id":2864,
                        "billing_organization_id":2864,
                        "is_access_allowed":true,
                        "is_billing_organization":false,
                        "is_support_organization":false,
                        "corporate_name":"zozo2.com Inc",
                        "corporate_name_normalized":"zozo2.com inc",
                        "portal_prefix":null,
                        "default_language_code":"fr_ca",
                        "timezone_code":{  
                           "timezone_type":3,
                           "timezone":"America\/Montreal"
                        },
                        "note_shared":null,
                        "quar_expiry_in_days":14,
                        "note_for_zs_only":null,
                        "logo_reference":null,
                        "is_active":true,
                        "version":0,
                        "created_at":"2017-02-13 14:42:02.000000 +00:00",
                        "updated_at":"2017-02-13 14:42:02.000000 +00:00",
                        "dsbl_portal_selfprov":false,
                        "dsbl_org_selfprov":false,
                        "currency":"cad",
                        "renewal_messages_allowed":true,
                        "renewal_note":null,
                        "yearly_renewal_date":"2018-02-13 00:00:00.000000 +00:00",
                        "renewal_state":"ok",
                        "renewal_msg_sent_at":null,
                        "next_renewal_msg":null,
                        "billing_period":null,
                        "billing_base_date":null,
                        "salesforce_id":null,
                        "shows_powered_by_logo":true,
                        "is_distributor_organization":false,
                        "distributor_id":null,
                        "external_distributor_id":null,
                        "activation_date":null
                     }
                  }
               ],
               "meta":{  
                  "pagination":{  
                     "total":3,
                     "count":2,
                     "per_page":15,
                     "current_page":1,
                     "total_pages":1
                  }
               }
            }';

        $config = $this->preSuccess($jsonResult);
        $request = new IndexOrganizationRequest();
        $request->setDomainName('zozo')
            ->setDomainName(null)
            ->setDomainName('zozo');

        $client = new ProvulusClient($config);
        $client->processRequest($request);

        $response = $request->getResponse();

        self::assertEquals(2, $response->getMetaData()->count);

        $response->data()->each(function (OrganizationResponse $response) {
            self::assertTrue($response->is_active === true);
        });
    }

    /**
     * @test
     */
    public function index_org_with_support_org()
    {
        $jsonResult
            = '{
  "data": [
    {
      "type": "orgs",
      "id": "2",
      "attributes": {
        "support_organization_id": 637,
        "billing_organization_id": 637,
        "is_access_allowed": true,
        "is_billing_organization": false,
        "is_support_organization": false,
        "corporate_name": "Famille test",
        "corporate_name_normalized": "famille test",
        "portal_prefix": null,
        "default_language_code": "fr_ca",
        "timezone_code": {
          "timezone_type": 3,
          "timezone": "America/Montreal"
        },
        "note_shared": null,
        "quar_expiry_in_days": 14,
        "note_for_zs_only": null,
        "logo_reference": null,
        "is_active": true,
        "version": 1,
        "created_at": "2010-12-30 21:45:32.000000 +00:00",
        "updated_at": "2015-11-18 14:44:19.000000 +00:00",
        "dsbl_portal_selfprov": false,
        "dsbl_org_selfprov": false,
        "currency": "cad",
        "renewal_messages_allowed": true,
        "renewal_note": null,
        "yearly_renewal_date": "2017-01-25 14:16:38.798597 +00:00",
        "renewal_state": "manual",
        "renewal_msg_sent_at": null,
        "next_renewal_msg": null,
        "billing_period": null,
        "billing_base_date": null,
        "salesforce_id": "0013000000NPFmx",
        "shows_powered_by_logo": true,
        "is_distributor_organization": false,
        "distributor_id": null,
        "external_distributor_id": null,
        "activation_date": null
      },
      "relationships": {
        "supportOrganization": {
          "data": {
            "type": "orgs",
            "id": "637"
          }
        }
      }
    }
  ],
  "included": [
    {
      "type": "orgs",
      "id": "637",
      "attributes": {
        "support_organization_id": 1,
        "billing_organization_id": 1,
        "is_access_allowed": true,
        "is_billing_organization": true,
        "is_support_organization": true,
        "corporate_name": "ZEROSPAM Demo",
        "corporate_name_normalized": "zerospam demo",
        "portal_prefix": null,
        "default_language_code": "en_us",
        "timezone_code": {
          "timezone_type": 3,
          "timezone": "America/Montreal"
        },
        "note_shared": "Demo ACME",
        "quar_expiry_in_days": 31,
        "note_for_zs_only": null,
        "logo_reference": null,
        "is_active": true,
        "version": 35,
        "created_at": "2011-03-01 22:09:11.000000 +00:00",
        "updated_at": "2016-04-21 13:05:03.000000 +00:00",
        "dsbl_portal_selfprov": false,
        "dsbl_org_selfprov": true,
        "currency": "cad",
        "renewal_messages_allowed": true,
        "renewal_note": "2014-08-11 SMF: Francis me sort la liste des clients qu\'on héberge et elaine me demande de les passer à Manuel",
        "yearly_renewal_date": "2017-01-25 14:16:38.798597 +00:00",
        "renewal_state": "manual",
        "renewal_msg_sent_at": null,
        "next_renewal_msg": null,
        "billing_period": null,
        "billing_base_date": null,
        "salesforce_id": null,
        "shows_powered_by_logo": true,
        "is_distributor_organization": false,
        "distributor_id": null,
        "external_distributor_id": null,
        "activation_date": null
      }
    }   
  ],
  "meta": {
    "pagination": {
      "total": 1,
      "count": 1,
      "per_page": 15,
      "current_page": 1,
      "total_pages": 1
    }
  },
  "links": {
    "self": "http://cumulus.local/api/v1/orgs?page=1",
    "first": "http://cumulus.local/api/v1/orgs?page=1",
    "next": "http://cumulus.local/api/v1/orgs?page=2",
    "last": "http://cumulus.local/api/v1/orgs?page=102"
  }
}';
        $config = $this->preSuccess($jsonResult);

        $request = new IndexOrganizationRequest();
        $request->withSupportOrganization();

        $client = new ProvulusClient($config);
        $client->processRequest($request);

        $response = $request->getResponse();

        $this->assertEquals(1, $response->getMetaData()->count);

        /**
         * @var $org OrganizationResponse
         */
        $org = $response->data()->first();

        $this->assertInstanceOf(OrganizationResponse::class, $org->support_organization);
        $this->assertEquals('ZEROSPAM Demo', $org->support_organization->corporate_name);

    }

    /**
     * @tests
     */
    public function update_org_general_info_test()
    {

        $jsonResult
            =
            '{
                  "data": {
                      "type": "orgs",
                      "id": "3000",
                      "attributes": {
                          "support_organization_id": 314,
                          "billing_organization_id": 159,
                          "is_access_allowed": true,
                          "is_billing_organization": false,
                          "is_support_organization": true,
                          "corporate_name": "ZeRoSpAm Testing",
                          "corporate_name_normalized": "zerospam testing",
                          "portal_prefix": null,
                          "default_language_code": "en_us",
                          "timezone_code": {
                              "timezone_type": 3,
                              "timezone": "America/Moncton"
                          },
                          "note_shared": "This is a note",
                          "quar_expiry_in_days": 15,
                          "note_for_zs_only": null,
                          "lockdown_value": 3,
                          "logo_reference": null,
                          "is_active": true,
                          "version": 7,
                          "created_at": "2016-11-01 15:19:18.000000 +00:00",
                          "updated_at": "2017-05-05 13:19:15.000000 +00:00",
                          "dsbl_portal_selfprov": false,
                          "dsbl_org_selfprov": false,
                          "currency": "eur",
                          "renewal_messages_allowed": false,
                          "renewal_note": "Note",
                          "yearly_renewal_date": "2018-01-01 05:00:00.000000 +00:00",
                          "renewal_state": "ok",
                          "renewal_msg_sent_at": null,
                          "next_renewal_msg": null,
                          "billing_period": "annual",
                          "billing_base_date": "2015-01-01 13:19:15.000000 +00:00",
                          "salesforce_id": "12345",
                          "shows_powered_by_logo": true,
                          "is_distributor_organization": false,
                          "distributor_id": 1,
                          "external_distributor_id": null,
                          "activation_date": null,
                          "is_two_factor_auth_mandatory": true,
                          "two_factor_auth_users": "admin",
                          "is_school_organization" : true
                      }
                  }
            }';

        $jsonRequest = '{
                          "corporate_name": "ZeRoSpAm Testing",
                          "portal_prefix": null,
                          "currency": "eur",
                          "default_language_code": "en_US",
                          "timezone_code": "America/Moncton",
                          "note_shared": "This is a note",
                          "note_for_zs_only": "Only ZS can see this",
                          "billing_period": "annual",
                          "billing_base_date": "2015-01-01T00:00:00+00:00",
                          "dsbl_portal_selfprov": false,
                          "dsbl_org_selfprov": false,
                          "is_access_allowed": true,
                          "is_billing_organization": false,
                          "is_distributor_organization": false,
                          "is_support_organization": true,
                          "shows_powered_by_logo": true,
                          "support_org": 1,
                          "distributor_org": 1,
                          "quar_expiry_in_days": 15,
                          "salesforce_id": "12345",
                          "yearly_renewal_date": "2018-01-01T00:00:00+00:00",
                          "is_two_factor_auth_mandatory": true,
                          "two_factor_auth_users": "administrator",
                          "renewal_messages_allowed": false,
                          "renewal_note" : "Note",
                          "is_school_organization": true
                        }';

        $config = $this->preSuccess($jsonResult);

        $request = new UpdateOrganizationRequest();
        $request->setOrganizationId(3279);
        $request
            ->setCorporateName("ZeRoSpAm Testing")
            ->setPortalPrefix(null)
            ->setCurrency(PaymentCurrencyEnum::EUR())
            ->setDefaultLanguageCode(LocaleEnum::EN_US())
            ->setTimezoneCode(new DateTimeZone('America/Moncton'))
            ->setNoteShared("This is a note")
            ->setNoteForZsOnly("Only ZS can see this")
            ->setBillingPeriod(BillingPeriodEnum::ANNUAL())
            ->setBillingBaseDate(Carbon::create(2015, 1, 1, 0, 0, 0, 'GMT'))
            ->setDsblOrgSelfprov(false)
            ->setDsblPortalSelfprov(false)
            ->setIsAccessAllowed(true)
            ->setIsBillingOrganization(false)
            ->setIsDistributorOrganization(false)
            ->setIsSupportOrganization(true)
            ->setShowsPoweredByLogo(true)
            ->setSupportOrg(1)
            ->setDistributorOrg(1)
            ->setQuarExpiryInDays(15)
            ->setSalesforceId("12345")
            ->setYearlyRenewalDate(Carbon::create(2018, 1, 1, 0, 0, 0, 'GMT'))
            ->setTwoFactorAuthUsers(TwoFactorAuthUsersEnum::ADMINISTRATOR())
            ->setIsTwoFactorAuthMandatory(true)
            ->setRenewalMessagesAllowed(false)
            ->setRenewalNote('Note')
            ->setIsSchoolOrganization(true);

        $client = new ProvulusClient($config);
        /**
         * @var $response OrganizationResponse
         */
        $response = $client->processRequest($request);
        $this->validateRequest($config, $jsonRequest);


        self::assertTrue($response->billing_period->is(BillingPeriodEnum::ANNUAL()),
            "Organization billing period is not correct");
        self::assertTrue($response->currency->is(PaymentCurrencyEnum::EUR()),
            "Organization portal currency is not correct");
        self::assertTrue($response->default_language_code->is(LocaleEnum::EN_US()),
            "Organization default language code is not correct");
        self::assertEquals(314, $response->support_org,
            "Organization support org is not correct");
        self::assertEquals(159, $response->billing_org,
            "Organization billing org is not correct");

    }

    /**
     * @tests
     */
    public function update_org_nullable()
    {
        $jsonResult
            =
            '{
                  "data": {
                      "type": "orgs",
                      "id": "3000",
                      "attributes": {
                          "support_organization_id": 1,
                          "billing_organization_id": 1,
                          "is_access_allowed": true,
                          "is_billing_organization": false,
                          "is_support_organization": true,
                          "corporate_name": "ZeRoSpAm Testing",
                          "corporate_name_normalized": "zerospam testing",
                          "portal_prefix": null,
                          "default_language_code": "en_us",
                          "timezone_code": {
                              "timezone_type": 3,
                              "timezone": "America/Moncton"
                          },
                          "note_shared": null,
                          "quar_expiry_in_days": 15,
                          "note_for_zs_only": null,
                          "lockdown_value": 3,
                          "logo_reference": null,
                          "is_active": true,
                          "version": 7,
                          "created_at": "2016-11-01 15:19:18.000000 +00:00",
                          "updated_at": "2017-05-05 13:19:15.000000 +00:00",
                          "dsbl_portal_selfprov": false,
                          "dsbl_org_selfprov": false,
                          "currency": "eur",
                          "renewal_messages_allowed": true,
                          "renewal_note": null,
                          "yearly_renewal_date": null,
                          "renewal_state": "ok",
                          "renewal_msg_sent_at": null,
                          "next_renewal_msg": null,
                          "billing_period": null,
                          "billing_base_date": null,
                          "salesforce_id": null,
                          "shows_powered_by_logo": true,
                          "is_distributor_organization": false,
                          "distributor_id": 1,
                          "external_distributor_id": null,
                          "activation_date": null,
                          "cidr_restrictions": [
                              "8.8.8.8",
                              "8.8.8.9",
                              "5.5.5.5"
                          ]
                      }
                  }
            }';

        $jsonRequest = '{
           "yearly_renewal_date": null,
           "salesforce_id": null,
           "note_for_zs_only": null,
           "note_shared": null,
           "billing_base_date": null,
           "billing_period": null,
           "portal_prefix": null
        }';

        $config = $this->preSuccess($jsonResult);

        $request = new UpdateOrganizationRequest();
        $request->setOrganizationId(3279);

        $request
            ->setYearlyRenewalDate(null)
            ->setSalesforceId(null)
            ->setNoteForZsOnly(null)
            ->setNoteShared(null)
            ->setBillingBaseDate(null)
            ->setBillingPeriod(null)
            ->setPortalPrefix(null);

        $client = new ProvulusClient($config);
        /**
         * @var $response OrganizationResponse
         */
        $response = $client->processRequest($request);
        $this->validateRequest($config, $jsonRequest);

        self::assertNull($response->yearly_renewal_date);
        self::assertNull($response->salesforce_id);
        self::assertNull($response->note_for_zs_only);
        self::assertNull($response->note_shared);
        self::assertNull($response->billing_base_date);
        self::assertNull($response->billing_period);
        self::assertNull($response->portal_prefix);
    }

    /**
     * @test
     */
    public function test_portal_no_incidents()
    {
        $jsonSuccess = <<<JSON
{
    "logo": "https://test.url/logo.png",
    "name": "test portal",
    "maintenance": null,
    "incidents": [],
    "id": 1234,
    "shows_powered_by_logo": true,
    "is_portal_selfprov_enabled": false
}
JSON;
        $config      = $this->preSuccess($jsonSuccess);
        $request     = (new PortalRequest())->setPortalName('test');

        $client = new ProvulusClient($config);
        /**
         * @var $response PortalResponse
         */
        $response = $client->processRequest($request);

        $this->assertEquals('https://test.url/logo.png', $response->logo);
        $this->assertEquals('test portal', $response->name);
        $this->assertNull($response->maintenance);
        $this->assertEquals(1234, $response->id());
        $this->assertTrue($response->shows_powered_by_logo);
        $this->assertFalse($response->is_portal_selfprov_enabled);
        $this->assertEmpty($response->incidents->toList());
    }

    /**
     * @test
     */
    public function test_portal_maintenance()
    {
        $jsonSuccess = <<<JSON
{
    "id": 987,
    "shows_powered_by_logo": false,
    "is_portal_selfprov_enabled": true,
    "logo": "https://test.url/logo.png",
    "name": "test portal",
    "maintenance": {
        "name": "Quarantine actions issues",
        "scheduled_for": "2018-01-09 10:00:39.822000 +00:00",
        "scheduled_until": "2018-01-09 11:00:39.822000 +00:00",
        "link": "http://stspg.io/94cd5dab3",
        "created_at": "2018-01-08 19:54:39.822000 +00:00",
        "impact": "maintenance"
    },
    "incidents": [{
        "name": "Test incidents",
        "scheduled_for": null,
        "scheduled_until": null,
        "link": "http://stspg.io/123456",
        "created_at": "2018-10-18 18:18:18.822000 +00:00",
        "impact": "minor"
    }]
}
JSON;
        $config      = $this->preSuccess($jsonSuccess);
        $request     = (new PortalRequest())->setPortalName('test');

        $client = new ProvulusClient($config);
        /**
         * @var $response PortalResponse
         */
        $response = $client->processRequest($request);

        $this->assertEquals('https://test.url/logo.png', $response->logo);
        $this->assertEquals('test portal', $response->name);
        $this->assertEquals(987, $response->id());
        $this->assertFalse($response->shows_powered_by_logo);
        $this->assertTrue($response->is_portal_selfprov_enabled);

        $this->assertEquals('Quarantine actions issues', $response->maintenance->name());
        $this->assertEquals('http://stspg.io/94cd5dab3', $response->maintenance->link()->__toString());
        $this->assertEquals('2018-01-09 10:00:39', $response->maintenance->scheduledFor()->toDateTimeString());
        $this->assertEquals('2018-01-09 11:00:39', $response->maintenance->scheduledUntil()->toDateTimeString());
        $this->assertEquals("maintenance", $response->maintenance->impact());

        $this->assertEquals(1, count($response->incidents->toList()));

        $incident = $response->incidents->first();
        $this->assertEquals('Test incidents', $incident->name());
        $this->assertEquals('http://stspg.io/123456', $incident->link()->__toString());
        $this->assertNull($incident->scheduledFor());
        $this->assertNull($incident->scheduledUntil());
        $this->assertEquals("minor", $incident->impact());
    }

    /**
     * @test
     **/
    public function test_end_trial_direct_client_confirm()
    {
        $requestBody = [];

        $request = new PostTrialConfirmRequest();
        $request->setToken('ASDFASDFASDFASDFASDFASDFASDFASDF')
            ->setState(PostTrialStateEnum::CONFIRM());

        $config = $this->preSuccess($requestBody);
        $client = new ProvulusClient($config);

        $client->processRequest($request);
        $this->validateRequest($config, $requestBody);
        $this->validateUrl($config, 'posttrial/confirm');
    }
}
