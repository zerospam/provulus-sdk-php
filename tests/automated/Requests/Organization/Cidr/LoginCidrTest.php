<?php
/**
 * Created by PhpStorm.
 * User: ycoutu
 * Date: 18/04/18
 * Time: 11:50 AM
 */

namespace automated\Requests\Organization\Cidr;

use ProvulusSDK\Client\ProvulusClient;
use ProvulusSDK\Client\Request\Organization\Cidr\Bulk\BulkDeleteLoginCidrRequest;
use ProvulusSDK\Client\Request\Organization\Cidr\Bulk\UpdateCidrsLoginCidrRequest;
use ProvulusSDK\Client\Request\Organization\Cidr\CreateLoginCidrRequest;
use ProvulusSDK\Client\Request\Organization\Cidr\DeleteLoginCidrRequest;
use ProvulusSDK\Client\Request\Organization\Cidr\IndexLoginCidrRequest;
use ProvulusSDK\Client\Request\Organization\Cidr\ReadLoginCidrRequest;
use ProvulusSDK\Client\Request\Organization\Cidr\UpdateLoginCidrRequest;
use ProvulusSDK\Client\Response\Organization\Cidr\LoginCidrResponse;
use ProvulusTest\TestCaseApiKey;

class LoginCidrTest extends TestCaseApiKey
{
    /** @test */
    public function read_login_cidr()
    {
        $jsonResponse = '{
            "data": {
                "type": "login-cidr",
                "id": "11",
                "attributes": {
                    "organization_id": 111,
                    "cidr": "10.0.0.0/24",
                    "created_at": "2017-08-28 18:55:53.000000 +00:00",
                    "updated_at": "2017-08-28 18:55:53.000000 +00:00"
                }
            }
        }';

        $config  = $this->preSuccess($jsonResponse);
        $request = (new ReadLoginCidrRequest)
            ->setOrganizationId(111)
            ->setLoginCidrId(11);

        $client = new ProvulusClient($config);
        $client->processRequest($request);

        $response = $request->getResponse();

        self::assertEquals("10.0.0.0/24", $response->cidr);
    }

    /** @test */
    public function create_login_cidr()
    {
        $jsonResponse = '{
            "data": {
                "type": "login-cidr",
                "id": "1337",
                "attributes": {
                    "organization_id": 222,
                    "cidr": "9.9.1.2/32",
                    "created_at": "2017-08-28 18:55:53.000000 +00:00",
                    "updated_at": "2017-08-28 18:55:53.000000 +00:00"
                }
            }
        }';
        $jsonRequest = '{
            "cidr": "9.9.1.2"
        }';

        $config  = $this->preSuccess($jsonResponse);
        $request = (new CreateLoginCidrRequest)
            ->setOrganizationId(222)
            ->setCidr("9.9.1.2");

        $client = new ProvulusClient($config);
        $client->processRequest($request);
        $this->validateRequest($config, $jsonRequest);

        $response = $request->getResponse();

        self::assertEquals("9.9.1.2/32", $response->cidr);
    }

    /** @test */
    public function delete_login_cidr()
    {
        $config  = $this->preSuccess('{}');
        $request = (new DeleteLoginCidrRequest)
            ->setOrganizationId(231)
            ->setLoginCidrId(3);

        $client = new ProvulusClient($config);
        $client->processRequest($request);
    }

    /** @test */
    public function update_login_cidr()
    {
        $jsonResponse = '{
            "data": {
                "type": "login-cidr",
                "id": "5",
                "attributes": {
                    "organization_id": 555,
                    "cidr": "6.5.0.0/16",
                    "created_at": "2017-08-28 18:55:53.000000 +00:00",
                    "updated_at": "2017-08-28 18:55:53.000000 +00:00"
                }
            }
        }';
        $jsonRequest = '{
            "cidr": "6.5.0.0/16"
        }';

        $config  = $this->preSuccess($jsonResponse);
        $request = (new UpdateLoginCidrRequest)
            ->setOrganizationId(555)
            ->setLoginCidrId(5)
            ->setCidr("6.5.0.0/16");

        $client = new ProvulusClient($config);
        $client->processRequest($request);
        $this->validateRequest($config, $jsonRequest);

        $response = $request->getResponse();

        self::assertEquals("6.5.0.0/16", $response->cidr);
    }

    /** @test */
    public function index_login_cidr()
    {
        $jsonResponse = '{
            "data": [{
                "type": "login-cidr",
                "id": "8",
                "attributes": {
                    "organization_id": 5434,
                    "cidr": "199.199.199.199/32",
                    "created_at": "2017-08-28 18:55:53.000000 +00:00",
                    "updated_at": "2017-08-28 18:55:53.000000 +00:00"
                }
            },{
                "type": "login-cidr",
                "id": "9",
                "attributes": {
                    "organization_id": 5434,
                    "cidr": "10.0.0.0/24",
                    "created_at": "2017-08-28 18:55:53.000000 +00:00",
                    "updated_at": "2017-08-28 18:55:53.000000 +00:00"
                }
            },{
                "type": "login-cidr",
                "id": "10",
                "attributes": {
                    "organization_id": 5434,
                    "cidr": "2.2.2.2/32",
                    "created_at": "2017-08-28 18:55:53.000000 +00:00",
                    "updated_at": "2017-08-28 18:55:53.000000 +00:00"
                }
            }]
        }';

        $config  = $this->preSuccess($jsonResponse);
        $request = (new IndexLoginCidrRequest)
            ->setOrganizationId(5434);

        $client = new ProvulusClient($config);
        $client->processRequest($request);

        $response = $request->getResponse();

        /** @var LoginCidrResponse $first */
        $first = $response->data()->first();

        self::assertEquals("199.199.199.199/32", $first->cidr);
    }

    /** @test */
    public function bulk_delete_login_cidr()
    {
        $jsonResponse = '{}';
        $jsonRequest = '{
            "ids": [4, 7, 9, 15]
        }';

        $config  = $this->preSuccess($jsonResponse);
        $request = (new BulkDeleteLoginCidrRequest)
            ->setOrganizationId(111)
            ->setIds([4, 7, 9, 15]);

        $client = new ProvulusClient($config);
        $client->processRequest($request);
        $this->validateRequest($config, $jsonRequest);
        }

    /** @test */
    public function bulk_update_login_cidr()
    {
        $jsonResponse = '{
            "data": [{
                "type": "login-cidr",
                "id": "8",
                "attributes": {
                    "organization_id": 90,
                    "cidr": "6.7.1.5/32",
                    "created_at": "2017-08-28 18:55:53.000000 +00:00",
                    "updated_at": "2017-08-28 18:55:53.000000 +00:00"
                }
            },{
                "type": "login-cidr",
                "id": "9",
                "attributes": {
                    "organization_id": 90,
                    "cidr": "10.0.0.0/24",
                    "created_at": "2017-08-28 18:55:53.000000 +00:00",
                    "updated_at": "2017-08-28 18:55:53.000000 +00:00"
                }
            }]
        }';
        $jsonRequest = '{
            "cidrs": ["6.7.1.5", "10.0.0.0/24"]
        }';

        $config  = $this->preSuccess($jsonResponse);
        $request = (new UpdateCidrsLoginCidrRequest)
            ->setOrganizationId(90)
            ->setCidrs(["6.7.1.5", "10.0.0.0/24"]);

        $client = new ProvulusClient($config);
        $client->processRequest($request);
        $this->validateRequest($config, $jsonRequest);

        $response = $request->getResponse();

        /** @var LoginCidrResponse $first */
        $first = $response->data()->first();

        self::assertEquals("6.7.1.5/32", $first->cidr);
    }
}
