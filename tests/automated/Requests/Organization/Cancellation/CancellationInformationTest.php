<?php
/**
 * Created by PhpStorm.
 * User: ycoutu
 * Date: 06/02/18
 * Time: 11:56 AM
 */

namespace automated\Requests\Organization\Cancellation;

use Carbon\Carbon;
use ProvulusSDK\Client\ProvulusClient;
use ProvulusSDK\Client\Request\Organization\Cancellation\CreateCancellationRequest;
use ProvulusSDK\Client\Request\Organization\Cancellation\DeleteCancellationRequest;
use ProvulusSDK\Utils\Date\DateUtil;
use ProvulusTest\TestCaseApiKey;

class CancellationInformationTest extends TestCaseApiKey
{
    /** @test */
    public function create_cancellation_information_request()
    {
        $jsonResponse = '{
            "data": {
                "type": "cancellation-information",
                "id": "11",
                "attributes": {
                    "organization_id": 654,
                    "reason": "The client likes to buy dubious pills.",
                    "target_date": "2018-02-28 00:00:00.000000 +00:00",
                    "from_display_name": "Cena, Jonathan (john.cena@company.com)",
                    "from_corporate_name": "Company Co.",
                    "is_active": true,
                    "executed_at": null,
                    "created_at": "2018-02-06 15:28:32.000000 +00:00",
                    "updated_at": "2018-02-06 15:28:32.000000 +00:00"
                }
            }
        }';

        $jsonRequest = '{
            "reason": "The client likes to buy dubious pills.",
            "requested_date": "2018-02-28T00:00:00+00:00"
        }';

        $config  = $this->preSuccess($jsonResponse);
        $request = (new CreateCancellationRequest)
            ->setOrganizationId(654)
            ->setReason("The client likes to buy dubious pills.")
            ->setRequestedDate(Carbon::create(2018, 2, 28, 0, 0, 0, 'GMT'));

        $client = new ProvulusClient($config);
        $client->processRequest($request);

        $response = $request->getResponse();
        $this->validateRequest($config, $jsonRequest);
        $this->validateRequestUrl($config, ['orgs/654/cancellation']);

        $this->assertEquals("The client likes to buy dubious pills.", $response->reason);
        $this->assertEquals("2018-02-28 00:00:00.000000 +00:00", $response->target_date->format(DateUtil::getZerospamDateFormat()));
    }

    /** @test */
    public function delete_cancellation_information_request()
    {
        $jsonResponse = '{}';

        $jsonRequest = '{}';

        $config  = $this->preSuccess($jsonResponse);

        $request = (new DeleteCancellationRequest)
            ->setOrganizationId(443);

        $client = new ProvulusClient($config);
        $client->processRequest($request);
        $this->validateRequestUrl($config, ['orgs/443/cancellation']);
        
        $request->getResponse();
        $this->validateRequest($config, $jsonRequest);
    }
}
