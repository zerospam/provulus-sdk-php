<?php
/**
 * Created by PhpStorm.
 * User: pbb
 * Date: 16/05/17
 * Time: 3:53 PM
 */

namespace automated\Requests\Organization\User\SelfProvision;

use ProvulusSDK\Client\ProvulusClient;
use ProvulusSDK\Client\Request\User\SelfProvision\CreateRegularUserSelfProvisionConfirmRequest;
use ProvulusSDK\Client\Request\User\SelfProvision\CreateRegularUserSelfProvisionRequest;
use ProvulusSDK\Client\Response\EmptyResponse;
use ProvulusSDK\Client\Response\User\UserResponse;
use ProvulusSDK\Enum\Locale\LocaleEnum;
use ProvulusTest\TestCaseApiKey;

/**
 * Class UserRegularSelfProvisionTest
 *
 * @package automated\Requests\Organization\User\SelfProvision
 */
class UserRegularSelfProvisionTest extends TestCaseApiKey
{

    /**
     * @test
     **/
    public function self_provision_request_success()
    {
        $jsonRequestBody
            = '{
  "email_address": "cumulus@zerospam.ca",
  "name" : "Zerospam",
  "surname" : "Cumulus",
  "language_code": "en_US",
  "timezone_code" : "America/Montreal",
  "is_daily_digest" : true,
  "phone_number" : "4186674206"
}';

        $jsonResponse = [];

        $config = $this->preSuccess($jsonResponse);

        $request = new CreateRegularUserSelfProvisionRequest();
        $request->setSurname('Cumulus')
                ->setName('Zerospam')
                ->setEmailAddress('cumulus@zerospam.ca')
                ->setLanguageCode(LocaleEnum::EN_US())
                ->setTimezoneCode(new \DateTimeZone('America/Montreal'))
                ->setIsDailyDigest(true)
                ->setPhoneNumber('4186674206')
                ->setPassword('abcdefghi12345')
                ->setOrganizationId(1);

        $client = new ProvulusClient($config);

        $response = $client->processRequest($request);

        $this->validateRequest($config, $jsonRequestBody);
        $this->validateRequestUrl($config, ['orgs', 1, 'users', 'selfprov', 'regular']);

        $this->assertInstanceOf(EmptyResponse::class, $response);
    }

    public function self_provision_confirm()
    {
        $jsonRequestBody
            = '{
  "token": "H4sIAAAAAAAAA8WUy07DMBBFfwV5DcV2Xq7ZAC1LRNXSnSU0SSbFwklKnCBK1X_HTgMCITYsSKTIludm5sTz2BOrNxWRJMmKQkRsGvAomgo-ZakQAJxnRSDyDFJBC8wAiiSIIkbjVAATMcQpEymP04CTU9LWT-hd3clpKBWZdWVnOqvUdWd1hdbt7r1CqQXsTA25UjevW92gUmuLzQpNsWjqF2117TRL3HQGmh-WJT53aNsPF0QKubeSUeYiqo6651_i9pGwBG0UubCS-x8Gmxdf30vc6mySQa9gNBiB0HZNBSX2BB6xdYIBh46A8xsLH4HF1BmYI03kwmP1sF4NOOEIOK0u8a2ujkAsdgRXJTbaVc_5bV21DYIZ6JIR6HJX6Lu53nymzDGwgWeMzts-fl6Vr-PQzaI4CTmN-7PkT7Xdr5Aa_N7j_WnrOObQ-pB3kvlOnkGTeuphJTLwc8hXTn7UuaHgs8gpS85odMbECRMyFJJFE8HDJByuz_v6yP1Du9v6T7UMnEl8sfRaL13fz9z-cCCHdxJe9lS6BQAA"
}';
        $jsonResponse
            = '{  
   "data":{  
      "type":"users",
      "id":"11884",
      "attributes":{  
         "organization_id":1,
         "surname":"Cumulus",
         "name":"Zerospam",
         "display_name":"Cumulus, Zerospam",
         "email_address":"cumulus@zerospam.ca",
         "username":"cumulus@zerospam.ca",
         "quar_search_page_size":25,
         "is_access_allowed":true,
         "timezone_code":{  
            "timezone_type":3,
            "timezone":"America\/Montreal"
         },
         "language_code":"en_us",
         "usr_type_code":"regular",
         "is_ldap":false,
         "is_admin_stats_push":false,
         "is_adminquar_only":false,
         "phone_number":"+1 418-667-4206",
         "updated_at":"2017-05-16 18:49:40.187075 +00:00",
         "created_at":"2017-05-16 18:49:19.322909 +00:00",
         "daily_digest_email":"cumulus@zerospam.ca",
         "is_locked_down":false
      }
   }
}';

        $config = $this->preSuccess($jsonResponse);

        $request = new CreateRegularUserSelfProvisionConfirmRequest();
        $request->setToken('H4sIAAAAAAAAA8WUy07DMBBFfwV5DcV2Xq7ZAC1LRNXSnSU0SSbFwklKnCBK1X_HTgMCITYsSKTIludm5sTz2BOrNxWRJMmKQkRsGvAomgo-ZakQAJxnRSDyDFJBC8wAiiSIIkbjVAATMcQpEymP04CTU9LWT-hd3clpKBWZdWVnOqvUdWd1hdbt7r1CqQXsTA25UjevW92gUmuLzQpNsWjqF2117TRL3HQGmh-WJT53aNsPF0QKubeSUeYiqo6651_i9pGwBG0UubCS-x8Gmxdf30vc6mySQa9gNBiB0HZNBSX2BB6xdYIBh46A8xsLH4HF1BmYI03kwmP1sF4NOOEIOK0u8a2ujkAsdgRXJTbaVc_5bV21DYIZ6JIR6HJX6Lu53nymzDGwgWeMzts-fl6Vr-PQzaI4CTmN-7PkT7Xdr5Aa_N7j_WnrOObQ-pB3kvlOnkGTeuphJTLwc8hXTn7UuaHgs8gpS85odMbECRMyFJJFE8HDJByuz_v6yP1Du9v6T7UMnEl8sfRaL13fz9z-cCCHdxJe9lS6BQAA');

        $client = new ProvulusClient($config);

        $client->processRequest($request);

        /** @var UserResponse $response */
        $response = $request->getResponse();

        $this->validateRequest($config, $jsonRequestBody);
        $this->validateRequestUrl($config, ['/confirm/selfprov/regular']);

        $this->assertEquals('Zerospam', $response->name);

    }
}