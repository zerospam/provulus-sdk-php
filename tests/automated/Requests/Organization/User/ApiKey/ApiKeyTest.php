<?php
/**
 * Created by PhpStorm.
 * User: pbb
 * Date: 30/01/17
 * Time: 3:10 PM
 */

namespace automated\Requests\Organization\User\ApiKey;

use GuzzleHttp\Handler\MockHandler;
use ProvulusSDK\Client\Errors\Validation\ValidationError;
use ProvulusSDK\Client\ProvulusClient;
use ProvulusSDK\Client\Request\User\ApiKeys\CreateApiKeyRequest;
use ProvulusSDK\Client\Request\User\ApiKeys\DeleteApiKeyRequest;
use ProvulusSDK\Client\Request\User\ApiKeys\IndexApiKeyRequest;
use ProvulusSDK\Client\Request\User\ApiKeys\UpdateApiKeyRequest;
use ProvulusSDK\Client\Response\User\ApiKey\ApiKeyResponse;
use ProvulusSDK\Exception\API\ValidationException;
use ProvulusTest\TestCaseApiKey;

/**
 * Class ApiKeyTest
 *
 * Tests for ApiKeys requests
 *
 * @package automated\Requests\User
 */
class ApiKeyTest extends TestCaseApiKey
{

    /**
     * @test
     **/
    public function create_api_key_success()
    {



        $jsonRequest = '{"permissions":["create","read","update","delete"],"comment":"This is a new api key"}';

        $jsonResponse
            = '{  
                   "data":{  
                      "type":"apikey",
                      "id":"128",
                      "attributes":{  
                         "api_key":"KXn1NQwU-7KCKAk13-dzx6KWpG-lNXon3Xz-fS3Mr4yy",
                         "comment":"This is a new api key",
                         "updated_at":"2017-01-30 20:21:43.000000 +00:00",
                         "created_at":"2017-01-30 20:21:43.000000 +00:00",
                         "permissions":[  
                            "create",
                            "read",
                            "update",
                            "delete"
                         ]
                      }
                   }
                }';

        $config = $this->preSuccess($jsonResponse);



        $request = new CreateApiKeyRequest();
        $request->setOrganizationId(2888)
                ->setUserId(10075)
                ->setComment('This is a new api key')
                ->setPermissions([
                    "create",
                    "read",
                    "update",
                    "delete",
                ]);

        $client = new ProvulusClient($config);

        /**
         * @var ApiKeyResponse $response
         */
        $response = $client->processRequest($request);
        self::assertContains('delete', $response->get('permissions'));
        self::assertEquals('This is a new api key', $response->get('comment'));

        $this->validateRequest($config, $jsonRequest);
    }

    /**
     * @test
     **/
    public function index_api_keys_success()
    {

        $jsonRequest = '{}';
        $jsonResponse
                     = '{  
               "data":[  
                  {  
                     "type":"apikey",
                     "id":"128",
                     "attributes":{  
                        "api_key":"KXn1N\u2022\u2022\u2022\u2022\u2022\u2022\u2022\u2022\u2022\u2022\u2022\u2022\u2022\u2022\u2022\u2022\u2022\u2022\u2022\u2022\u2022\u2022\u2022\u2022\u2022\u2022\u2022\u2022\u2022\u2022\u2022\u2022\u2022\u2022\u2022\u2022",
                        "created_at":"2017-01-30 20:21:43.000000 +00:00",
                        "updated_at":"2017-01-30 20:21:43.000000 +00:00",
                        "deleted_at":null,
                        "ip_access":null,
                        "comment":"This is a new api key",
                        "permissions":[  
                           "create",
                           "read",
                           "update",
                           "delete"
                        ]
                     }
                  },
                  {  
                     "type":"apikey",
                     "id":"114",
                     "attributes":{  
                        "api_key":"10aBC\u2022\u2022\u2022\u2022\u2022\u2022\u2022\u2022\u2022\u2022\u2022\u2022\u2022\u2022\u2022\u2022\u2022\u2022\u2022\u2022\u2022\u2022\u2022\u2022\u2022\u2022\u2022\u2022\u2022\u2022\u2022\u2022\u2022\u2022\u2022\u2022",
                        "created_at":"2016-12-02 14:02:00.000000 +00:00",
                        "updated_at":"2016-12-02 14:02:00.000000 +00:00",
                        "deleted_at":null,
                        "ip_access":null,
                        "comment":"Key for server",
                        "permissions":[  
                           "create",
                           "read",
                           "update",
                           "delete"
                        ]
                     }
                  }
               ],
               "meta":{  
                  "pagination":{  
                     "total":2,
                     "count":2,
                     "per_page":15,
                     "current_page":1,
                     "total_pages":1
                  }
               },
               "links":{  
                  "self":"http:\/\/cumulus.local\/api\/v1\/orgs\/2888\/users\/10075\/apikeys?page=1",
                  "first":"http:\/\/cumulus.local\/api\/v1\/orgs\/2888\/users\/10075\/apikeys?page=1",
                  "last":"http:\/\/cumulus.local\/api\/v1\/orgs\/2888\/users\/10075\/apikeys?page=1"
               }
            }';

        $config = $this->preSuccess($jsonResponse);


        $request = new IndexApiKeyRequest();
        $request->setOrganizationId(2888)
                ->setUserId(10075);

        $client = new ProvulusClient($config);
        $client->processRequest($request);

        $collectionResponse = $request->getResponse();

        /**
         * @var ApiKeyResponse $apikeyResponse
         */
        $apikeyResponse = $collectionResponse->data()->first();
        self::assertContains('create', $apikeyResponse->get('permissions'));
        self::assertCount(4, $apikeyResponse->get('permissions'));
        self::assertTrue(is_null($apikeyResponse->get('ip_access')));

        $this->validateRequest($config, $jsonRequest);
    }


    /**
     * @test
     **/
    public function delete_apikey_success()
    {

        $config = $this->preSuccess([]);

        $request = new DeleteApiKeyRequest();
        $request->setOrganizationId(2888)
                ->setUserId(10075)
                ->setApiKeyId(114);

        $client = new ProvulusClient($config);
        $client->processRequest($request);

        $this->validateRequestUrl($config, [2888, 10075, 114]);
    }

    /**
     * @test
     **/
    public function update_api_key_success()
    {
        $jsonRequest
            = '{"comment":"New Comment","cidr":"1.2.3.4"}';

        $jsonResponse
            = '{  
                       "data":{  
                          "type":"apikey",
                          "id":"114",
                          "attributes":{  
                             "api_key":"10aBC\u2022\u2022\u2022\u2022\u2022\u2022\u2022\u2022\u2022\u2022\u2022\u2022\u2022\u2022\u2022\u2022\u2022\u2022\u2022\u2022\u2022\u2022\u2022\u2022\u2022\u2022\u2022\u2022\u2022\u2022\u2022\u2022\u2022\u2022\u2022\u2022",
                             "created_at":"2016-12-02 14:02:00.000000 +00:00",
                             "updated_at":"2017-01-30 21:09:11.000000 +00:00",
                             "deleted_at":null,
                             "ip_access":"1.2.3.4\/32",
                             "comment":"New Comment",
                             "permissions":[  
                                "create",
                                "read",
                                "update",
                                "delete"
                             ]
                          }
                       }
                    }';

        $config = $this->preSuccess($jsonResponse);


        $request = new UpdateApiKeyRequest();
        $request->setOrganizationId(2888)
                ->setUserId(10075)
                ->setApiKeyId(114)
                ->setComment('New Comment')
                ->setCidr('1.2.3.4');

        $client = new ProvulusClient($config);

        /**
         * @var ApiKeyResponse $response
         */
        $response = $client->processRequest($request);
        self::assertEquals('1.2.3.4/32', $response->get('ip_access'));

        $this->validateRequest($config, $jsonRequest);
    }


    /**
     * @test
     * @expectedException \Exception
     * @expectedExceptionMessage ApiKeyId needs to be set.
     **/
    public function apiKey_not_set_failure()
    {
        $config = $this->getConfig(new MockHandler());

        $request = new UpdateApiKeyRequest();
        $request->setOrganizationId(2888)
                ->setUserId(10075)
                ->setComment('New Comment')
                ->setCidr('1.2.3.4');

        $client = new ProvulusClient($config);
        $client->processRequest($request);
    }


    /**
     * @test
     * @expectedException \ProvulusSDK\Exception\API\ValidationException
     **/
    public function update_apiKey_invalid_cidr_failure()
    {

        $jsonRequest
            = '{"comment":"New Comment","cidr":"test.com"}';

        $jsonResponse
            = '{  
               "errors":{  
                  "cidr":[  
                     {  
                        "code":"cidr_error",
                        "message":"The CIDR\/IP given is not valid. (XXX.XXX.XXX.XXX\/XX)",
                        "value":"test.com"
                     }
                  ]
               }
            }';

        $config = $this->preFailure($jsonResponse);

        $request = new UpdateApiKeyRequest();
        $request->setOrganizationId(2888)
                ->setUserId(10075)
                ->setApiKeyId(114)
                ->setComment('New Comment')
                ->setCidr('test.com');

        $client = new ProvulusClient($config);

        try {
            $client->processRequest($request);
        } catch (ValidationException $validationException) {

            $this->validateRequest($config, $jsonRequest);

            $validationData = $validationException->getValidationData();
            $this->assertArrayHasKey('cidr', $validationData);

            /**
             * @var $first ValidationError
             */
            $first = $validationData['cidr']->getValidationErrorCollection()->first();
            $this->assertInstanceOf(ValidationError::class, $first);
            $this->assertEquals('The CIDR/IP given is not valid. (XXX.XXX.XXX.XXX/XX)', $first->getMessage());
            self::assertEquals('cidr_error', $first->getCode());
            self::assertEquals('test.com', $first->getValue());
            throw $validationException;
        }
    }
}