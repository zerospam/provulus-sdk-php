<?php
/**
 * Created by PhpStorm.
 * User: pbb
 * Date: 13/03/17
 * Time: 10:21 AM
 */

namespace automated\Requests\Organization\User\RequestAddress;


use ProvulusSDK\Client\ProvulusClient;
use ProvulusSDK\Client\Request\User\AddressRequest\RequestAddressRequest;
use ProvulusTest\TestCaseApiKey;

/**
 * Class ConfirmAddressTest
 *
 * @package automated\Requests\Organization\User\ConfirmAddress
 */
class RequestAddressTest extends TestCaseApiKey
{

    /**
     * @test
     **/
    public function create_confirm_address_confirm()
    {
        $jsonResult
            = '{}';

        $jsonRequest
            = '{"email_address":"newUser@zerospam.ca"}';

        $config = $this->preSuccess($jsonResult);

        $request = new RequestAddressRequest();
        $request->setOrganizationId(1)
                ->setUserId(9885)
                ->setEmailAddress('newUser@zerospam.ca');

        $client = new ProvulusClient($config);
        /**
         * @var $response RequestAddressResponse
         */
        $client->processRequest($request);

        $this->validateRequest($config, $jsonRequest);
        $this->validateRequestUrl($config, ['/orgs/1/users/9885/requestAddress']);
    }
}