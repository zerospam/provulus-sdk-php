<?php
/**
 * Created by PhpStorm.
 * User: pbb
 * Date: 16/03/17
 * Time: 11:29 AM
 */

namespace automated\Requests\Organization\User\RequestAddress;

use ProvulusSDK\Client\Errors\Precondition\PreconditionError;
use ProvulusSDK\Client\ProvulusClient;
use ProvulusSDK\Client\Request\User\AddressRequest\ConfirmAddressRequestRequest;
use ProvulusSDK\Client\Response\EmptyResponse;
use ProvulusSDK\Exception\API\PreconditionException;
use ProvulusTest\TestCaseApiKey;

/**
 * Class ConfirmAddressTest
 *
 * @package automated\Requests\Organization\User\RequestAddress
 */
class ConfirmAddressTest extends TestCaseApiKey
{

    /**
     * @test
     **/
    public function confirmEmailAddress()
    {
        $jsonResult
            = '{  
               "data":{  
                  "type":"addresses",
                  "id":"2000000000009833",
                  "attributes":{  
                     "usr_id":9885,
                     "domain_id":2,
                     "local_part":"CumulusAddress",
                     "updated_at":"2017-03-20 19:01:36.000000 +00:00",
                     "created_at":"2017-03-20 19:01:36.000000 +00:00",
                     "domain_name":"zerospam.ca"
                  }
               }
            }';

        $jsonRequest
            = '{"token": "H4sIAAAAAAAAA7WRwU7DMAyGXwXlzKZkyZI2uwCDA6chNG6RUNp6rNA2o0kFY9q744RNAokTYr7YiT_9vxPviK-fOqJJJjPBQSqrKIUV5LmQIG3JVopTWqykLWhpra0Un_JyCjkomheitEgLiiw5J8G9QJRaaKm0IfOhHZrBG3M1-LoDj9UyEsbc2W3jbGXMzfum7sGYy6rqE3Ao7uF1AB-OHNFC77xWU1Q1A8X4P-0kN3job9FnVmvGMs5maCZOZQatrRv08noyQY-A_eh_4deuhzc8jUvXpn52sgf3X3dQHeE0Dke7tnl268677pd5FP3LPCnbooGfv5BuQ-26axsA5ReaRf-57QuHModMNI-rj8uovjicU-JpQpkaUT5i2RkTWkhN8zFNkZikFeoWPlwHj2G7gbRdHn_1WyexEX1YzrHe78n-E7rsnFcSAwAA"}';

        $config = $this->preSuccess($jsonResult);

        $request = new ConfirmAddressRequestRequest();
        $request->setToken('H4sIAAAAAAAAA7WRwU7DMAyGXwXlzKZkyZI2uwCDA6chNG6RUNp6rNA2o0kFY9q744RNAokTYr7YiT_9vxPviK-fOqJJJjPBQSqrKIUV5LmQIG3JVopTWqykLWhpra0Un_JyCjkomheitEgLiiw5J8G9QJRaaKm0IfOhHZrBG3M1-LoDj9UyEsbc2W3jbGXMzfum7sGYy6rqE3Ao7uF1AB-OHNFC77xWU1Q1A8X4P-0kN3job9FnVmvGMs5maCZOZQatrRv08noyQY-A_eh_4deuhzc8jUvXpn52sgf3X3dQHeE0Dke7tnl268677pd5FP3LPCnbooGfv5BuQ-26axsA5ReaRf-57QuHModMNI-rj8uovjicU-JpQpkaUT5i2RkTWkhN8zFNkZikFeoWPlwHj2G7gbRdHn_1WyexEX1YzrHe78n-E7rsnFcSAwAA');

        $client = new ProvulusClient($config);

        $client->processRequest($request);
        $this->validateRequest($config, $jsonRequest);
        $this->validateRequestUrl($config, ['/confirm/email']);

        self::assertInstanceOf(EmptyResponse::class, $request->getResponse());
    }

    /**
     * @test
     * @expectedException \ProvulusSDK\Exception\API\PreconditionException
     */
    public function tokenExpired()
    {
        $jsonResult
            = '{  
               "errors":[  
                  {  
                     "code":"user_controller_token_invalid_error",
                     "obj_id":-1,
                     "message":"Email address confirmation token is either invalid or has expired."
                  }
               ]
            }';

        $jsonRequest
            = '{"token":"H4sIAAAAAAAAA7WRwU7DMAyGXwXlzKZkyZI2uwCDA6chNG6RUNp6rNA2o0kFY9q744RNAokTYr7YiT_9vxPviK-fOqJJJjPBQSqrKIUV5LmQIG3JVopTWqykLWhpra0Un_JyCjkomheitEgLiiw5J8G9QJRaaKm0IfOhHZrBG3M1-LoDj9UyEsbc2W3jbGXMzfum7sGYy6rqE3Ao7uF1AB-OHNFC77xWU1Q1A8X4P-0kN3job9FnVmvGMs5maCZOZQatrRv08noyQY-A_eh_4deuhzc8jUvXpn52sgf3X3dQHeE0Dke7tnl268677pd5FP3LPCnbooGfv5BuQ-26axsA5ReaRf-57QuHModMNI-rj8uovjicU-JpQpkaUT5i2RkTWkhN8zFNkZikFeoWPlwHj2G7gbRdHn_1WyexEX1YzrHe78n-E7rsnFcSAwAA"}';

        $config = $this->preFailure($jsonResult, 412);

        $request = new ConfirmAddressRequestRequest();
        $request->setToken('H4sIAAAAAAAAA7WRwU7DMAyGXwXlzKZkyZI2uwCDA6chNG6RUNp6rNA2o0kFY9q744RNAokTYr7YiT_9vxPviK-fOqJJJjPBQSqrKIUV5LmQIG3JVopTWqykLWhpra0Un_JyCjkomheitEgLiiw5J8G9QJRaaKm0IfOhHZrBG3M1-LoDj9UyEsbc2W3jbGXMzfum7sGYy6rqE3Ao7uF1AB-OHNFC77xWU1Q1A8X4P-0kN3job9FnVmvGMs5maCZOZQatrRv08noyQY-A_eh_4deuhzc8jUvXpn52sgf3X3dQHeE0Dke7tnl268677pd5FP3LPCnbooGfv5BuQ-26axsA5ReaRf-57QuHModMNI-rj8uovjicU-JpQpkaUT5i2RkTWkhN8zFNkZikFeoWPlwHj2G7gbRdHn_1WyexEX1YzrHe78n-E7rsnFcSAwAA');

        $client = new ProvulusClient($config);
        try {
            $client->processRequest($request);
        } catch (PreconditionException $exception) {
            $this->validateRequest($config, $jsonRequest);
            $errors = $exception->getErrors();

            /**
             * @var PreconditionError $error
             */
            $error = $errors->first();
            self::assertEquals('Email address confirmation token is either invalid or has expired.', $error->getMessage());

            throw $exception;
        }
    }

}