<?php
/**
 * Created by PhpStorm.
 * User: ycoutu
 * Date: 28/12/17
 * Time: 11:36 AM
 */

namespace automated\Requests\Organization\User\TwoFactorAuth;

use ProvulusSDK\Client\ProvulusClient;
use ProvulusSDK\Client\Request\User\TwoFactorAuth\ConfirmTwoFactorAuthRequest;
use ProvulusSDK\Client\Request\User\TwoFactorAuth\CreateTwoFactorAuthRequest;
use ProvulusSDK\Client\Request\User\TwoFactorAuth\DeleteTwoFactorAuthRequest;
use ProvulusSDK\Client\Request\User\TwoFactorAuth\ReadTwoFactorAuthRequest;
use ProvulusSDK\Client\Response\User\TwoFactorAuth\TwoFactorAuthResponse;
use ProvulusTest\TestCaseApiKey;

class TwoFactorAuthTest extends TestCaseApiKey
{
    /** @test */
    public function create_tfa_success()
    {
        $jsonRequest = '{}';

        $jsonResult = '{
            "data": {
                "type": "two-factor-auth",
                "id": "4",
                "attributes": {
                    "is_confirmed": false,
                    "usr_id": 258,
                    "key": "BZ3CAXJR45UNZBXTCVJSHFRU5S5OX2AI",
                    "updated_at": "2017-12-28 16:30:27.162114 +00:00",
                    "created_at": "2017-12-28 16:30:27.162114 +00:00",
                    "qr": "data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAAMgAAADICAIAAAAiOjnJAAAABmJLR0QA/wD/AP+gvaeTAAAFhElEQVR4nO3dQY7kNhBFwWnD97/yeOFNAYYGZDGfSmVELAdqiW1/sBMUlfz5/fv3L5j216cHwP+TYJEQLBKCRUKwSAgWCcEiIVgkBIuEYJEQLBKCRUKwSAgWCcEiIVgkBIvE3+/92M/Pz+w4fv36dbWX9fVZK9cU47nz/ru/453jXGfGIiFYJASLxJs11quT73yuaoKrmqOoIV5d/S679d9JzTRVSxX/X9aZsUgIFgnBIjFQY72a+ttfrPGsXH9SW0zVf8W36XVN9l9mLBKCRUKwSAzXWFN217de7dZnK7XF7nrV67/v1jf1Wt09zFgkBIuEYJF4aI11su/qpB66GsPufXbXhHbX6p7fh9GMRUKwSAgWieEaa+pv/0k9sbsutTueq38/eW7x/nT3/rPMWCQEi4RgkRiosZ78bmtqTevVyvVT7zSn1sbuZ8YiIVgkBIvEmzVWvS4yVX9M1Sv1fVau373/Z5mxSAgWCcEi8YH+WPW60dXP7o7t6v5FH4eTmrL+9/eYsUgIFgnBIvFz/te0qLem9qpPrRUVfbDufHd5df3VONVYPJRgkRAsEgM11tJjhmqUoq/mVD139az6Ps/8DtGMRUKwSAgWiYF3hSd9FnbXYOpeU0Xvq5VnnTj5RrKrvcxYJASLhGCRGH5XOHWeYLFmU6z9TP1eJz87tbY3e38zFgnBIiFYJN6ssabehV3d8+pnixroU7XUVL31aqpmUmPxUIJFQrBIfHjP+656P/vJc4s97CeKOnidGYuEYJEQLBJhn/epb9x2n7ti971hPZ5i7/xUXfgeMxYJwSIhWCSG97xPfTe38twVU3XDyd783Z5YV9esjG3lmqva0ToWX0CwSAgWiZvOhC72JK08a6q+uXN/+u7aXr2PzX4sHkSwSAgWieH+WMU+pHqta2UMK/ef2o+1Mp6pMXhXyJcRLBKCRSLsj3Vlqq968aw794kX9dwU61g8lGCRECwS4ZnQdS/yO3s3XF1/Ut9MrSfVve/fY8YiIVgkBIvEQH+sV/fs9XnvnsU+/StT/avqvhjdOTxmLBKCRUKwSHygP9b9e4P+e8+T8Vy5s1fCne9S32PGIiFYJASLxMB3hcV5hVNn6Uy9C1u5Z/2O785er9axeCjBIiFYJIbP0lm5ZqpWeNpZOnd+k3hnL3g1Fg8iWCQEi0TYH6t+dzbVB2v3nlf337X7u0/1Zbi65ywzFgnBIiFYJAa+K7yzP2fXl/zPpr7d+1TNN9WrbJ0Zi4RgkRAsEgP9sVas/O2/uv/UGXwn47wa28pzi3r0+cxYJASLhGCRGO6PdXKe4O71xbvI3bOWC7vvDU/u47xCvoxgkRAsEh/Y817fs167qs/GKXpGnLDnnQcRLBKCReKmPe+vpvZfF/cp+q3vnuV88g60/m++zoxFQrBICBaJ4Rqr6HEwVd+c7Jc6qWlOvhO8Unxj+ErvBh5KsEgIFokH9ceqe5mujG33uStjuLrmyqf2tuuPxRcQLBKCRWLgLJ3Cnb06p+rFk/d0xTpZPYY/M2ORECwSgkXipt4NK4p3akVP+WKf+8n1V9fsfpPou0K+gGCRECwS4XmFK+7c/75bo+yOZ/dni+8Zn9PH1YxFQrBICBaJL9vzPvWebnc8J9e/qs8UWlGfgfgvMxYJwSIhWCTC3g1TTuqSYi/8ytiuxnnS32G3D+pnzzQ0Y5EQLBKCReILaqyTXgZXin1aK8+qzw2881l/ZsYiIVgkBIvEcI31qXNmin4NJ3XJyRpSsfdrxex7QzMWCcEiIVgkBvpjTfnUGTL1/vqpbxK/a/+7GYuEYJEQLBIP7Y/FtzNjkRAsEoJFQrBICBYJwSIhWCQEi4RgkRAsEoJFQrBICBYJwSIhWCQEi4RgkfgHjbjeeACEyXEAAAAASUVORK5CYII="
                }
            }
        }';

        $config = $this->preSuccess($jsonResult);

        $request = (new CreateTwoFactorAuthRequest)
            ->setOrganizationId(147)
            ->setUserId(258);

        $client = new ProvulusClient($config);

        /** @var $response TwoFactorAuthResponse */
        $response = $client->processRequest($request);

        $this->validateRequest($config, $jsonRequest);

        $this->validateRequestUrl($config, ['/orgs/147/users/258/tfa']);

        $this->assertFalse($response->is_confirmed);
        $this->assertEquals(258, $response->usr_id);
        $this->assertEquals('BZ3CAXJR45UNZBXTCVJSHFRU5S5OX2AI', $response->key);
        $qr_start = "data:image/png;base64,";
        $this->assertEquals($qr_start, substr($response->qr, 0, strlen($qr_start)));
    }

    /** @test */
    public function read_tfa_success()
    {
        $jsonRequest = '{}';

        $jsonResult = '{
            "data": {
                "type": "two-factor-auth",
                "id": "4",
                "attributes": {
                    "usr_id": 9876,
                    "is_confirmed": true,
                    "created_at": "2017-12-28 16:30:27.000000 +00:00",
                    "updated_at": "2017-12-28 16:55:05.000000 +00:00"
                }
            }
        }';

        $config = $this->preSuccess($jsonResult);

        $request = (new ReadTwoFactorAuthRequest)
            ->setOrganizationId(876)
            ->setUserId(9876);

        $client = new ProvulusClient($config);

        /** @var $response TwoFactorAuthResponse */
        $response = $client->processRequest($request);

        $this->validateRequest($config, $jsonRequest);

        $this->validateRequestUrl($config, ['/orgs/876/users/9876/tfa']);

        $this->assertTrue($response->is_confirmed);
        $this->assertEquals(9876, $response->usr_id);
        $this->assertNull($response->key);
        $this->assertNull($response->qr);
    }

    /** @test */
    public function delete_tfa_success()
    {
        $jsonResult = '{}';
        $jsonRequest = '{}';

        $config = $this->preSuccess($jsonResult);

        $request = (new DeleteTwoFactorAuthRequest)
            ->setOrganizationId(93)
            ->setUserId(70707);

        $client = new ProvulusClient($config);

        /** @var $response TwoFactorAuthResponse */
        $response = $client->processRequest($request);

        $this->validateRequest($config, $jsonRequest);

        $this->validateRequestUrl($config, ['/orgs/93/users/70707/tfa']);
    }

    /** @test */
    public function confirm_tfa_success()
    {
        $jsonResult = '{
            "data": {
                "type": "two-factor-auth",
                "id": "54",
                "attributes": {
                    "usr_id": 777,
                    "is_confirmed": true,
                    "created_at": "2017-12-28 16:30:27.000000 +00:00",
                    "updated_at": "2017-12-28 16:55:05.000000 +00:00"
                }
            }
        }';

        $jsonRequest = '{
            "code": "142857"
        }';

        $config = $this->preSuccess($jsonResult);

        $request = (new ConfirmTwoFactorAuthRequest)
            ->setOrganizationId(555)
            ->setUserId(777)
            ->setCode("142857");

        $client = new ProvulusClient($config);

        /** @var $response TwoFactorAuthResponse */
        $response = $client->processRequest($request);

        $this->validateRequest($config, $jsonRequest);

        $this->validateRequestUrl($config, ['/orgs/555/users/777/tfa/confirm']);
    }
}
