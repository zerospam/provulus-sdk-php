<?php
/**
 * Created by PhpStorm.
 * User: ycoutu
 * Date: 31/10/18
 * Time: 4:58 PM
 */

namespace automated\Requests\Organization\User\Summary;

use ProvulusSDK\Client\ProvulusClient;
use ProvulusSDK\Client\Request\User\Summary\ReadAnonymousSummaryRequest;
use ProvulusSDK\Client\Request\User\Summary\ReadUserSummaryRequest;
use ProvulusSDK\Client\Response\User\Summary\UserSummaryResponse;
use ProvulusSDK\Enum\Locale\LocaleEnum;
use ProvulusSDK\Enum\Organization\Type\OrganizationTypeEnum;
use ProvulusSDK\Enum\User\UserTypeEnum;
use ProvulusTest\TestCaseApiKey;

class UserSummaryTest extends TestCaseApiKey
{
    /** @test */
    public function read_user_summary_request()
    {
        $jsonRequest = '{}';
        $jsonResponse = <<<JSON
{
    "data": {
        "type": "user-connection",
        "id": "554",
        "attributes": {
            "username": "jsmith@domain.com",
            "email_address": "jsmith@domain.com",
            "display_name": "Smith, John",
            "quar_search_page_size": 100,
            "is_two_factor_auth_enabled": true,
            "should_configure_two_factor_auth": false,
            "is_anonymous": false,
            "timezone": {
                "timezone_type": 3,
                "timezone": "America/Montreal"
            },
            "language": "en_us",
            "type": "admin"
        },
        "relationships": {
            "organizationSummary": {
                "data": {
                    "type": "org-connection",
                    "id": "6"
                }
            }
        }
    },
    "included": [{
        "type": "org-connection",
        "id": "6",
        "attributes": {
            "is_billing_organization": false,
            "is_support_organization": false,
            "corporate_name": "Company Inc.",
            "organization_type_enum": "NORMAL",
            "needs_to_fill_billing": false,
            "is_owner": false,
            "is_direct": true,
            "is_distribution": false,
            "can_update_dmb_count": false,
            "has_ldap_configuration": false,
            "has_cancellation_request": false,
            "has_outbound_filtering": true,
            "has_billing": false
        }
    }]
}
JSON;

        $config = $this->preSuccess($jsonResponse);

        $request = (new ReadUserSummaryRequest())
            ->setUserId(554);

        $client = new ProvulusClient($config);

        /** @var $response UserSummaryResponse */
        $response = $client->processRequest($request);

        $this->validateRequest($config, $jsonRequest);
        $this->validateUrl($config, 'users/554/summary');

        $this->assertEquals("jsmith@domain.com", $response->username);
        $this->assertEquals("jsmith@domain.com", $response->email_address);
        $this->assertEquals("Smith, John", $response->display_name);
        $this->assertEquals(100, $response->quar_search_page_size);
        $this->assertTrue($response->is_two_factor_auth_enabled);
        $this->assertFalse($response->should_configure_two_factor_auth);
        $this->assertFalse($response->is_anonymous);
        $this->assertTrue($response->language->is(LocaleEnum::EN_US()));
        $this->assertTrue($response->type->is(UserTypeEnum::ADMIN()));

        $organization = $response->organization_summary;

        $this->assertFalse($organization->is_billing_organization);
        $this->assertFalse($organization->is_support_organization);
        $this->assertEquals("Company Inc.", $organization->corporate_name);
        $this->assertTrue($organization->organization_type_enum->is(OrganizationTypeEnum::NORMAL()));
        $this->assertFalse($organization->needs_to_fill_billing);
        $this->assertFalse($organization->is_owner);
        $this->assertTrue($organization->is_direct);
        $this->assertFalse($organization->is_distribution);
        $this->assertFalse($organization->can_update_dmb_count);
        $this->assertFalse($organization->has_ldap_configuration);
        $this->assertFalse($organization->has_cancellation_request);
        $this->assertTrue($organization->has_outbound_filtering);
        $this->assertFalse($organization->has_billing);
    }

    /** @test */
    public function read_anonymous_summary_request()
    {
        $jsonRequest = '{}';
        $jsonResponse = <<<JSON
{
    "data": {
        "type": "user-connection",
        "id": "1",
        "attributes": {
            "username": "anonymous",
            "email_address": null,
            "display_name": "Anonymous user",
            "quar_search_page_size": 50,
            "is_two_factor_auth_enabled": false,
            "should_configure_two_factor_auth": false,
            "is_anonymous": true,
            "timezone": {
                "timezone_type": 3,
                "timezone": "America/Montreal"
            },
            "language": "fr_ca",
            "type": "system"
        },
        "relationships": {
            "organizationSummary": {
                "data": {
                    "type": "org-connection",
                    "id": "0"
                }
            }
        }
    },
    "included": [{
        "type": "org-connection",
        "id": "0",
        "attributes": {
            "is_billing_organization": false,
            "is_support_organization": false,
            "organization_type_enum": "NORMAL",
            "corporate_name": "Anonymous",
            "needs_to_fill_billing": false,
            "is_owner": false,
            "is_direct": true,
            "is_distribution": false,
            "can_update_dmb_count": false,
            "has_ldap_configuration": false,
            "has_cancellation_request": false,
            "has_outbound_filtering": false,
            "has_billing": true
        }
    }]
}
JSON;
        $config = $this->preSuccess($jsonResponse);

        $request = new ReadAnonymousSummaryRequest();

        $client = new ProvulusClient($config);

        /** @var $response UserSummaryResponse */
        $response = $client->processRequest($request);

        $this->validateRequest($config, $jsonRequest);
        $this->validateUrl($config, 'anonymous');

        $this->assertEquals("anonymous", $response->username);
        $this->assertNull($response->email_address);
        $this->assertEquals("Anonymous user", $response->display_name);
        $this->assertEquals(50, $response->quar_search_page_size);
        $this->assertFalse($response->is_two_factor_auth_enabled);
        $this->assertFalse($response->should_configure_two_factor_auth);
        $this->assertTrue($response->is_anonymous);
        $this->assertTrue($response->language->is(LocaleEnum::FR_CA()));
        $this->assertTrue($response->type->is(UserTypeEnum::SYSTEM()));

        $organization = $response->organization_summary;

        $this->assertFalse($organization->is_billing_organization);
        $this->assertFalse($organization->is_support_organization);
        $this->assertEquals("Anonymous", $organization->corporate_name);
        $this->assertTrue($organization->organization_type_enum->is(OrganizationTypeEnum::NORMAL()));
        $this->assertFalse($organization->needs_to_fill_billing);
        $this->assertFalse($organization->is_owner);
        $this->assertTrue($organization->is_direct);
        $this->assertFalse($organization->is_distribution);
        $this->assertFalse($organization->can_update_dmb_count);
        $this->assertFalse($organization->has_ldap_configuration);
        $this->assertFalse($organization->has_cancellation_request);
        $this->assertFalse($organization->has_outbound_filtering);
        $this->assertTrue($organization->has_billing);
    }
}
