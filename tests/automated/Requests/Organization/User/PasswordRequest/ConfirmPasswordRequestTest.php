<?php
/**
 * Created by PhpStorm.
 * User: pbb
 * Date: 26/04/17
 * Time: 2:50 PM
 */

namespace automated\Requests\Organization\User\PasswordRequest;

use ProvulusSDK\Client\ProvulusClient;
use ProvulusSDK\Client\Request\User\PasswordRequest\ConfirmPasswordRequestRequest;
use ProvulusSDK\Client\Response\User\Summary\UserSummaryResponse;
use ProvulusTest\TestCaseApiKey;

/**
 * Class ConfirmPasswordRequestTest
 *
 * @package automated\Requests\Organization\User\PasswordRequest
 */
class ConfirmPasswordRequestTest extends TestCaseApiKey
{

    /**
     * @test
     */
    public function confirmPasswordRequest()
    {
        $jsonResult = <<<JSON
{
    "data": {
        "type": "user-connection",
        "id": "11861",
        "attributes": {
            "username": "cumulus@zerospam.ca",
            "email_address": "cumulus@zerospam.ca",
            "display_name": "Cumulus, Zerospam",
            "quar_search_page_size": 25,
            "is_two_factor_auth_enabled": false,
            "should_configure_two_factor_auth": true,
            "is_anonymous": false,
            "timezone": {
                "timezone_type": 3,
                "timezone": "America/Montreal"
            },
            "language": "fr_ca",
            "type": "admin"
        },
        "relationships": {
            "organizationSummary": {
                "data": {
                    "type": "org-connection",
                    "id": "663"
                }
            }
        }
    },
    "included": [{
        "type": "org-connection",
        "id": "663",
        "attributes": {
            "is_billing_organization": true,
            "is_support_organization": true,
            "corporate_name": "ZERO",
            "organization_type_enum": "SUPPORT_BILLING",
            "needs_to_fill_billing": false,
            "is_owner": false,
            "is_direct": true,
            "is_distribution": false,
            "can_update_dmb_count": false,
            "has_ldap_configuration": true,
            "has_cancellation_request": false,
            "has_outbound_filtering": true
        }
    }]
}
JSON;

        $jsonRequest
            = '{"token": "H4sIAAAAAAAAA7WRwU7DMAyGXwXlzKZkyZI2uwCDA6chNG6RUNp6rNA2o0kFY9q744RNAokTYr7YiT_9vxPviK-fOqJJJjPBQSqrKIUV5LmQIG3JVopTWqykLWhpra0Un_JyCjkomheitEgLiiw5J8G9QJRaaKm0IfOhHZrBG3M1-LoDj9UyEsbc2W3jbGXMzfum7sGYy6rqE3Ao7uF1AB-OHNFC77xWU1Q1A8X4P-0kN3job9FnVmvGMs5maCZOZQatrRv08noyQY-A_eh_4deuhzc8jUvXpn52sgf3X3dQHeE0Dke7tnl268677pd5FP3LPCnbooGfv5BuQ-26axsA5ReaRf-57QuHModMNI-rj8uovjicU-JpQpkaUT5i2RkTWkhN8zFNkZikFeoWPlwHj2G7gbRdHn_1WyexEX1YzrHe78n-E7rsnFcSAwAA",
            "password": "Super secure long passphrase"
            }';

        $config = $this->preSuccess($jsonResult);

        $request = new ConfirmPasswordRequestRequest();
        $request->setToken('H4sIAAAAAAAAA7WRwU7DMAyGXwXlzKZkyZI2uwCDA6chNG6RUNp6rNA2o0kFY9q744RNAokTYr7YiT_9vxPviK-fOqJJJjPBQSqrKIUV5LmQIG3JVopTWqykLWhpra0Un_JyCjkomheitEgLiiw5J8G9QJRaaKm0IfOhHZrBG3M1-LoDj9UyEsbc2W3jbGXMzfum7sGYy6rqE3Ao7uF1AB-OHNFC77xWU1Q1A8X4P-0kN3job9FnVmvGMs5maCZOZQatrRv08noyQY-A_eh_4deuhzc8jUvXpn52sgf3X3dQHeE0Dke7tnl268677pd5FP3LPCnbooGfv5BuQ-26axsA5ReaRf-57QuHModMNI-rj8uovjicU-JpQpkaUT5i2RkTWkhN8zFNkZikFeoWPlwHj2G7gbRdHn_1WyexEX1YzrHe78n-E7rsnFcSAwAA')
                ->setPassword('Super secure long passphrase');

        $client = new ProvulusClient($config);

        $client->processRequest($request);
        $this->validateRequest($config, $jsonRequest);
        $this->validateRequestUrl($config, ['/confirm/password']);

        $response = $request->getResponse();

        self::assertInstanceOf(UserSummaryResponse::class, $response);
    }
}
