<?php
/**
 * Created by PhpStorm.
 * User: pbb
 * Date: 19/04/17
 * Time: 4:23 PM
 */

namespace automated\Requests\Organization\User\PasswordRequest;


use ProvulusSDK\Client\ProvulusClient;
use ProvulusSDK\Client\Request\User\PasswordRequest\ResetPasswordRequestRequest;
use ProvulusTest\TestCaseApiKey;

/**
 * Class PasswordRequestTest
 *
 * @package automated\Requests\Organization\User\PasswordRequest
 */
class PasswordRequestTest extends TestCaseApiKey
{

    /**
     * @test
     **/
    public function reset_password_success()
    {
        $jsonResult  = [];
        $jsonRequest = [];

        $config  = $this->preSuccess($jsonResult);
        $request = new ResetPasswordRequestRequest();
        $request->setUsername('cumulus@zerospam.ca');

        $client = new ProvulusClient($config);
        $client->processRequest($request);

        $this->validateRequest($config, $jsonRequest);
        $this->validateRequestUrl($config, ['reset/password']);
    }
}