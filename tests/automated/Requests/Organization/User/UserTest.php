<?php
/**
 * Created by PhpStorm.
 * User: pbb
 * Date: 04/01/17
 * Time: 11:14 AM
 */

namespace automated\Requests\Organization\User;

use InvalidArgumentException;
use libphonenumber\PhoneNumberUtil;
use ProvulusSDK\Client\Errors\Validation\ValidationError;
use ProvulusSDK\Client\ProvulusClient;
use ProvulusSDK\Client\Request\Resource\User\Contact\UserToContactRequest;
use ProvulusSDK\Client\Request\Resource\User\TypedUsers\UserRequestType;
use ProvulusSDK\Client\Request\User\BulkDeleteUserRequest;
use ProvulusSDK\Client\Request\User\CreateUserTypedRequest;
use ProvulusSDK\Client\Request\User\DeleteUserRequest;
use ProvulusSDK\Client\Request\User\IndexUserTypedRequest;
use ProvulusSDK\Client\Request\User\PasswordRequest\ResetPasswordAuthenticatedRequest;
use ProvulusSDK\Client\Request\User\PromoteUserRequest;
use ProvulusSDK\Client\Request\User\ReadUserRequest;
use ProvulusSDK\Client\Request\User\UpdateUserPasswordRequest;
use ProvulusSDK\Client\Request\User\UpdateUserRequest;
use ProvulusSDK\Client\Response\EmptyResponse;
use ProvulusSDK\Client\Response\User\Session\UserSessionResponse;
use ProvulusSDK\Client\Response\User\UserEmailAddress\UserEmailAddressResponse;
use ProvulusSDK\Client\Response\User\UserResponse;
use ProvulusSDK\Enum\Contact\Mailgroups\MailGroupEnum;
use ProvulusSDK\Enum\Locale\LocaleEnum;
use ProvulusSDK\Enum\User\UserTypeEnum;
use ProvulusSDK\Exception\API\ValidationException;
use ProvulusTest\TestCaseApiKey;

/**
 * Class CreateUserTest
 *
 * Tests for CreateUserRequests
 *
 * @package automated\Requests\Resource\User
 */
class UserTest extends TestCaseApiKey
{

    /**
     * @test
     **/
    public function create_user_success_admin()
    {
        $jsonResult
            = '
                {  
                   "data":{  
                      "type":"users",
                      "id":"11848",
                      "attributes":{  
                         "organization_id":1,
                         "surname":"zerospam",
                         "name":"cumulus",
                         "display_name":"zerospam, cumulus",
                         "display_name_normalized":"zerospam, cumulus",
                         "is_access_allowed":true,
                         "is_admin_stats_push":true,
                         "is_adminquar_only":false,
                         "sso_id":"zerospam",
                         "is_ldap":false,
                         "login_failures":0,
                         "quar_search_page_size":200,
                         "timezone_code":{  
                            "timezone_type":3,
                            "timezone":"America\/Toronto"
                         },
                         "language_code":"fr_ca",
                         "phone_number":"+1 418-667-4206",
                         "usr_type_code":"admin",
                         "email_address":"cumulus_new@zerospam.ca",
                         "username":"cumulus_new@zerospam.ca",
                         "updated_at":"2017-04-03 15:42:05.000000 +00:00",
                         "created_at":"2017-04-03 15:42:04.000000 +00:00",
                         "daily_digest_email":"cumulus_new@zerospam.ca",
                         "is_locked_down":false
                      }
                   }
                }';

        $jsonRequest
            = '
            {
              "email_address": "cumulus_new@zerospam.ca",
              "name" : "cumulus",
              "surname" : "zerospam",
              "phone_number" : "14186674206",
              "language_code": "fr_CA",
              "is_daily_digest" : true,
              "is_adminquar_only":false,
              "is_admin_stats_push":true,
              "sso_id" : "zerospam",
              "is_access_allowed" : true,
              "timezone_code" : "America/Montreal",
              "quar_search_page_size": 200            }';

        $config = $this->preSuccess($jsonResult);

        $request = new CreateUserTypedRequest();
        $request->setOrganizationId(1)
                ->setUserRequestType(UserRequestType::ADMIN())
                ->setEmailAddress('cumulus_new@zerospam.ca')
                ->setName('cumulus')
                ->setSurname('zerospam')
                ->setPhoneNumber('14186674206')
                ->setLanguageCode(LocaleEnum::FR_CA())
                ->setIsDailyDigest(true)
                ->setIsAdminquarOnly(false)
                ->setIsAdminStatsPush(true)
                ->setSsoId('zerospam')
                ->setIsAccessAllowed(true)
                ->setTimezoneCode(new \DateTimeZone('America/Montreal'))
                ->setQuarSearchPageSize(200);


        $client = new ProvulusClient($config);
        /**
         * @var $response UserResponse
         */
        $response = $client->processRequest($request);

        $this->assertEquals(1, $response->organization_id, 'Organization ID incorrect.');
        self::assertTrue($response->usr_type_code->is(UserTypeEnum::ADMIN()), 'user type is not correct.');
        self::assertEquals('zerospam', $response->surname, 'user surname is not correct.');
        self::assertEquals('cumulus', $response->name, 'user name is not correct.');
        self::assertEquals('cumulus_new@zerospam.ca', $response->email_address, 'user emailaddress is not correct.');
        self::assertEquals(200, $response->quar_search_page_size, 'quar search page size is not correct.');

        $this->validateRequest($config, $jsonRequest);
    }

    /**
     * @test
     * @expectedException \ProvulusSDK\Exception\API\ValidationException
     **/
    public function create_user_failure_admin_require_email()
    {
        $jsonResult
            = '{"errors":{"email_address":[{"code":"required_error","message":"The email address is required.","value":null}]}}';
        $jsonRequest
            = '{"name":"admin","surname":"test","is_daily_digest":true,"phone_number":"14186674206","language_code":"en_US"}';

        $config = $this->preFailure($jsonResult);

        $request = new CreateUserTypedRequest();
        $request->setOrganizationId(2832)
                ->setUserRequestType(UserRequestType::ADMIN())
                ->setSurname('test')
                ->setName('admin')
                ->setPhoneNumber('14186674206')
                ->setIsDailyDigest(true)
                ->setLanguageCode(LocaleEnum::EN_US());

        $client = new ProvulusClient($config);

        try {
            $client->processRequest($request);
        } catch (ValidationException $validationException) {

            $this->validateRequest($config, $jsonRequest);

            $validationData = $validationException->getValidationData();
            $this->assertArrayHasKey('email_address', $validationData);
            /**
             * @var $first ValidationError
             */
            $first = $validationData['email_address']->getValidationErrorCollection()->first();
            $this->assertInstanceOf(ValidationError::class, $first);
            $this->assertNull($first->getValue());
            $this->assertEquals('required_error', $first->getCode());
            $this->assertEquals('The email address is required.', $first->getMessage());

            throw $validationException;
        }
    }

    /**
     * @test
     **/
    public function create_user_success_regular()
    {

        $jsonResult
            = '{  
               "data":{  
                  "type":"users",
                  "id":"10085",
                  "attributes":{  
                     "organization_id":1,
                     "is_access_allowed":true,
                     "language_code":"en_us",
                     "email_address":"cumulus@zerospam.ca",
                     "surname":"test",
                     "name":"test",
                     "display_name":"test, test",
                     "phone_number":{  
            
                     },
                     "usr_type_code":"regular"
                  }
               }
            }';
        $jsonRequest
            = '{  
                   "name":"test",
                   "surname":"test",
                   "email_address":"cumulus@zerospam.ca",
                   "is_daily_digest":true,
                   "language_code":"en_US"
                }';

        $config = $this->preSuccess($jsonResult);


        $request = new CreateUserTypedRequest();
        $request->setOrganizationId(1)
                ->setUserRequestType(UserRequestType::REGULAR())
                ->setEmailAddress('cumulus@zerospam.ca')
                ->setSurname('test')
                ->setName('test')
                ->setIsDailyDigest(true)
                ->setLanguageCode(LocaleEnum::EN_US());

        $client = new ProvulusClient($config);
        /**
         * @var $response UserResponse
         */
        $response = $client->processRequest($request);

        $this->validateRequest($config, $jsonRequest);

        $this->assertEquals(1, $response->organization_id, 'Organization ID incorrect.');
        self::assertTrue($response->usr_type_code->is(UserTypeEnum::REGULAR()), 'user type is not correct.');
        self::assertEquals('test', $response->surname, 'user surname is not correct.');
        self::assertEquals('test', $response->name, 'user name is not correct.');
        self::assertEquals('cumulus@zerospam.ca', $response->email_address, 'user emailaddress is not correct.');
    }

    /**
     * @test
     * @expectedException \ProvulusSDK\Exception\API\ValidationException
     **/
    public function create_user_failure_regular_email_already_used()
    {
        $jsonResult
            = '{"errors":{"email_address":[{"code":"unique_error","message":"There is already a user using this email address.","value":"test6@wawa.com"}]}}';
        $jsonRequest
            = '{"name":"test","surname":"test","email_address":"test6@wawa.com","is_daily_digest":false ,"phone_number":"14186674206","language_code":"en_US"}';

        $config = $this->preFailure($jsonResult);


        $request = new CreateUserTypedRequest();
        $request->setOrganizationId(2832)
                ->setUserRequestType(UserRequestType::REGULAR())
                ->setSurname('test')
                ->setName('test')
                ->setPhoneNumber('14186674206')
                ->setEmailAddress('test6@wawa.com')
                ->setIsDailyDigest(false)
                ->setLanguageCode(LocaleEnum::EN_US());

        $client = new ProvulusClient($config);

        try {
            $client->processRequest($request);
        } catch (ValidationException $validationException) {

            $this->validateRequest($config, $jsonRequest);

            $validationData = $validationException->getValidationData();
            $this->assertArrayHasKey('email_address', $validationData);
            /**
             * @var $first ValidationError
             */
            $first = $validationData['email_address']->getValidationErrorCollection()->first();
            $this->assertInstanceOf(ValidationError::class, $first);
            $this->assertEquals('test6@wawa.com', $first->getValue());
            $this->assertEquals('unique_error', $first->getCode());
            $this->assertEquals('There is already a user using this email address.', $first->getMessage());

            throw $validationException;
        }
    }


    /**
     * @test
     * @expectedException InvalidArgumentException
     **/
    public function create_user_failure_create_on_all_collection()
    {
        $request = new CreateUserTypedRequest();
        $request->setOrganizationId(2888)
                ->setUserRequestType(UserRequestType::ALL());
    }

    /**
     * @test
     * @expectedException InvalidArgumentException
     **/
    public function update_user_on_all_routes()
    {
        $request = new UpdateUserRequest();
        $request->setUserRequestType(UserRequestType::ALL());
    }


    /**
     * @test
     **/
    public function update_user_success_admin()
    {
        $jsonResult
            = '{  
               "data":{  
                  "type":"users",
                  "id":"10025",
                  "attributes":{  
                     "organization_id":2832,
                     "is_access_allowed":false,
                     "usr_type_code":"admin_quar",
                     "email_address":"test111@wawa.com",
                     "display_name":"test, test",
                     "language_code":"en_us",
                     "name":"test",
                     "surname":"test"
                  }
               }
            }';

        $jsonRequest
            = '{  
               "name":"test",
               "surname":"test",
               "email_address":"test111@wawa.com",
               "sso_id":"1234asdaf1234",
               "daily_digest_email":"test111@wawa.com",
               "phone_number":"14186674206",
               "timezone_code":"America\/Toronto",
               "is_access_allowed":false,
               "is_admin_stats_push":true,
               "is_adminquar_only":true,
               "language_code":"en_US"
            }';

        $config = $this->preSuccess($jsonResult);

        $request = new UpdateUserRequest();
        $request->setOrganizationId(2832)
                ->setUserId(10025)
                ->setUserRequestType(UserRequestType::ADMIN())
                ->setEmailAddress('test111@wawa.com')
                ->setSurname('test')
                ->setName('test')
                ->setPhoneNumber('14186674206')
                ->setDailyDigestEmail('test111@wawa.com')
                ->setLanguageCode(LocaleEnum::EN_US())
                ->setIsAccessAllowed(false)
                ->setIsAdminquarOnly(true)
                ->setIsAdminStatsPush(true)
                ->setTimezoneCode(new \DateTimeZone('America/Toronto'))
                ->setSsoId('1234asdaf1234');

        $client = new ProvulusClient($config);
        /**
         * @var $response UserResponse
         */
        $response = $client->processRequest($request);

        $this->validateRequest($config, $jsonRequest);

        $this->assertEquals(2832, $response->organization_id, 'Organization ID incorrect.');
        self::assertTrue($response->usr_type_code->is(UserTypeEnum::ADMIN_QUAR()), 'user type is not correct.');
        self::assertEquals('test', $response->surname, 'user surname is not correct.');
        self::assertEquals('test', $response->name, 'user name is not correct.');
        self::assertEquals('test111@wawa.com', $response->email_address, 'user emailaddress is not correct.');
    }

    /**
     * @test
     **/
    public function update_user_success_regular()
    {
        $jsonResult
            = '{"data":{"type":"users","id":"10079","attributes":{"organization_id":2832,"is_access_allowed":true,"usr_type_code":"regular","email_address":"test121@wawa.com","display_name":"test, test","language_code":"en_us","name":"test","surname":"test"}}}';
        $jsonRequest
            = '{  
               "name":"test",
               "surname":"test",
               "email_address":"test121@wawa.com",
               "sso_id":"asdf1234asdf",
               "daily_digest_email":"test121@wawa.com",
               "phone_number":"14186674206",
               "timezone_code":"America\/Montreal",
               "is_access_allowed":true,
               "language_code":"en_US"
            }';

        $config = $this->preSuccess($jsonResult);


        $request = new UpdateUserRequest();
        $request->setOrganizationId(2832)
                ->setUserId(10025)
                ->setUserRequestType(UserRequestType::REGULAR())
                ->setEmailAddress('test121@wawa.com')
                ->setSurname('test')
                ->setName('test')
                ->setPhoneNumber('14186674206')
                ->setDailyDigestEmail('test121@wawa.com')
                ->setLanguageCode(LocaleEnum::EN_US())
                ->setIsAccessAllowed(true)
                ->setTimezoneCode(new \DateTimeZone('America/Montreal'))
                ->setSsoId('asdf1234asdf');

        $client = new ProvulusClient($config);
        /**
         * @var $response UserResponse
         */
        $response = $client->processRequest($request);

        $this->validateRequest($config, $jsonRequest);

        $this->assertEquals(2832, $response->organization_id, 'Organization ID incorrect.');
        self::assertTrue($response->usr_type_code->is(UserTypeEnum::REGULAR()), 'user type is not correct.');
        self::assertEquals('test', $response->surname, 'user surname is not correct.');
        self::assertEquals('test', $response->name, 'user name is not correct.');
        self::assertEquals('test121@wawa.com', $response->email_address, 'user emailaddress is not correct.');
    }

    /**
     * @test
     * @expectedException \ProvulusSDK\Exception\API\ValidationException
     **/
    public function failure_daily_digest_address_domain_not_registered_within_organization()
    {
        $jsonResult
            = '{  
               "errors":{  
                  "daily_digest_address":[  
                     {  
                        "code":"domain_or_email_belong_to_organization_error",
                        "message":"The daily digest address needs to belong to the user organization.",
                        "value":"lolo@jambon.com"
                     }
                  ]
               }
            }';
        $jsonRequest
            = '{  
                   "name":"test",
                   "surname":"test",
                   "email_address":"test121@wawa.com",
                   "sso_id":"asdf1234asdf",
                   "daily_digest_email":"lolo@jambon.com",
                   "phone_number":"14186674206",
                   "timezone_code":"America\/Montreal",
                   "language_code":"en_US"
                }';

        $config = $this->preFailure($jsonResult);


        $request = new UpdateUserRequest();
        $request->setOrganizationId(2832)
                ->setUserId(10079)
                ->setUserRequestType(UserRequestType::REGULAR())
                ->setSurname('test')
                ->setName('test')
                ->setPhoneNumber('14186674206')
                ->setEmailAddress('test121@wawa.com')
                ->setDailyDigestEmail('lolo@jambon.com')
                ->setLanguageCode(LocaleEnum::EN_US())
                ->setSsoId('asdf1234asdf')
                ->setTimezoneCode(new \DateTimeZone('America/Montreal'));

        $client = new ProvulusClient($config);

        try {
            $client->processRequest($request);
        } catch (ValidationException $validationException) {

            $this->validateRequest($config, $jsonRequest);

            $validationData = $validationException->getValidationData();
            $this->assertArrayHasKey('daily_digest_address', $validationData);
            /**
             * @var $first ValidationError
             */
            $first = $validationData['daily_digest_address']->getValidationErrorCollection()->first();
            $this->assertInstanceOf(ValidationError::class, $first);
            $this->assertEquals('lolo@jambon.com', $first->getValue());
            $this->assertEquals('domain_or_email_belong_to_organization_error', $first->getCode());

            throw $validationException;
        }
    }


    /**
     * @test
     **/
    public function read_user_success_all()
    {
        $jsonResult = <<<JSON
{
    "data":{
        "type":"users",
        "id":"10075",
        "attributes":{
            "organization_id":2888,
            "daily_digest_address_id":null,
            "is_access_allowed":true,
            "usr_type_code":"admin",
            "username":"testk@zerospam.ca",
            "email_address":"testk@zerospam.ca",
            "is_admin_stats_push":false,
            "is_adminquar_only":false,
            "login_failures":0,
            "display_name":"Perdu, Super",
            "display_name_normalized":"perdu, super",
            "language_code":"en_us",
            "timezone_code":{"timezone_type":3,"timezone":"America\/Montreal"},
            "quar_search_page_size":25,
            "last_daily_digest_date":null,
            "is_active":true,
            "version":0,
            "is_active_backup":null,
            "name":"Super","surname":
            "Perdu","note":null,
            "password_changed_at":null,
            "alternative_daily_digest_address":null,
            "sso_id":null,
            "is_ldap":false,
            "phone_number":"+1 514-972-4022",
            "daily_digest_email":null
        }
    }
}
JSON;
        $jsonRequest = '{}';

        $config = $this->preSuccess($jsonResult);


        $request = new ReadUserRequest();
        $request->setOrganizationId(2888)
                ->setUserId(10075)
                ->setUserRequestType(UserRequestType::ALL());

        $client = new ProvulusClient($config);
        /**
         * @var $response UserResponse
         */
        $response = $client->processRequest($request);

        $this->assertEquals(2888, $response->organization_id, 'Organization ID incorrect');
        self::assertEquals(10075, $response->id(), 'User ID is incorrect.');
        self::assertEquals('testk@zerospam.ca', $response->username, 'Username is not correct.');

        $this->validateRequest($config, $jsonRequest);

    }

    /**
     * @test
     **/
    public function read_user_success_admin()
    {
        $jsonResult = <<<JSON
{
    "data":{
        "type":"users",
        "id":"10075",
        "attributes":{
            "organization_id":2888,
            "daily_digest_address_id":null,
            "is_access_allowed":true,
            "usr_type_code":"admin",
            "username":"testk@zerospam.ca",
            "email_address":"testk@zerospam.ca",
            "is_admin_stats_push":false,
            "is_adminquar_only":false,
            "login_failures":0,
            "display_name":"Perdu, Super",
            "display_name_normalized":"perdu, super",
            "language_code":"en_us",
            "timezone_code":{"timezone_type":3,"timezone":"America\/Montreal"},
            "quar_search_page_size":25,
            "last_daily_digest_date":null,
            "is_active":true,
            "version":0,
            "is_active_backup":null,
            "name":"Super",
            "surname":"Perdu",
            "note":null,
            "password_changed_at":null,
            "alternative_daily_digest_address":null,
            "sso_id":null,
            "is_ldap":false,
            "phone_number":"+1 514-972-4022",
            "daily_digest_email":null
        }
    }
}
JSON;
        $jsonRequest = '{}';

        $config = $this->preSuccess($jsonResult);

        $request = new ReadUserRequest();
        $request->setOrganizationId(2888)
                ->setUserId(10075)
                ->setUserRequestType(UserRequestType::ADMIN());

        $client = new ProvulusClient($config);
        /**
         * @var $response UserResponse
         */
        $response = $client->processRequest($request);

        $this->validateRequest($config, $jsonRequest);

        $this->assertEquals(2888, $response->organization_id, 'Organization ID incorrect.');
        self::assertEquals(10075, $response->id(), 'User ID is incorrect.');
        self::assertEquals('testk@zerospam.ca', $response->username, 'Username is not correct.');
        self::assertTrue($response->usr_type_code->is(UserTypeEnum::ADMIN()), 'user type is not correct.');
    }

    /**
     * @test
     **/
    public function read_user_success_regular()
    {
        $jsonResult = <<<JSON
{
    "data":{
        "type":"users",
        "id":"10026",
        "attributes":{
            "organization_id":2832,
            "daily_digest_address_id":null,
            "is_access_allowed":true,
            "usr_type_code":"regular",
            "username":"mama@wawa.com",
            "email_address":"mama@wawa.com",
            "is_admin_stats_push":false,
            "is_adminquar_only":false,
            "login_failures":0,
            "display_name":"bybye, allo",
            "display_name_normalized":"bybye, allo",
            "language_code":"fr_ca",
            "timezone_code":{"timezone_type":3,"timezone":"America\/Montreal"},
            "quar_search_page_size":25,
            "last_daily_digest_date":null,
            "is_active":true,
            "version":0,
            "is_active_backup":null,
            "name":"allo",
            "surname":"bybye",
            "note":null,
            "password_changed_at":"2016-11-23 14:08:47+00",
            "alternative_daily_digest_address":"mama@dudu.com",
            "sso_id":null,
            "is_ldap":false,
            "phone_number":null,
            "daily_digest_email":
            "mama@dudu.com"
        }
    }
}
JSON;

        $config = $this->preSuccess($jsonResult);


        $request = new ReadUserRequest();
        $request->setOrganizationId(2832)
                ->setUserId(10026)
                ->setUserRequestType(UserRequestType::REGULAR());

        $client = new ProvulusClient($config);
        /**
         * @var $response UserResponse
         */
        $response = $client->processRequest($request);

        $this->assertEquals(2832, $response->organization_id, 'Organization ID incorrect.');
        self::assertEquals(10026, $response->id(), 'User ID is incorrect.');
        self::assertEquals('mama@wawa.com', $response->username, 'Username is not correct.');
        self::assertTrue($response->usr_type_code->is(UserTypeEnum::REGULAR()), 'user type is not correct.');

        $this->validateRequestUrl($config, [
            'orgs',
            2832,
            UserRequestType::REGULAR,
            10026,
        ]);
    }

    /**
     * @test
     **/
    public function read_user_success_with_addresses_relation()
    {
        $jsonResult
            = '
        {  
           "data":{  
              "type":"users",
              "id":"11748",
              "attributes":{  
                 "organization_id":1,
                 "daily_digest_address_id":2000000000009730,
                 "is_access_allowed":true,
                 "usr_type_code":"admin",
                 "username":"cumulus@zerospam.ca",
                 "email_address":"cumulus@zerospam.ca",
                 "is_admin_stats_push":true,
                 "is_adminquar_only":false,
                 "login_failures":0,
                 "display_name":"zerospam, cumulus",
                 "display_name_normalized":"zerospam, cumulus",
                 "language_code":"en_us",
                 "timezone_code":{  
                    "timezone_type":3,
                    "timezone":"America\/Montreal"
                 },
                 "quar_search_page_size":25,
                 "last_daily_digest_date":null,
                 "is_active":true,
                 "version":0,
                 "is_active_backup":null,
                 "name":"cumulus",
                 "surname":"zerospam",
                 "note":"Cumulus user of organization Zerospam",
                 "password_changed_at":null,
                 "alternative_daily_digest_address":"cumulus1@zerospam.ca",
                 "sso_id":"zerospam",
                 "is_ldap":false,
                 "phone_number":"+1 418-667-4206",
                 "daily_digest_email":"cumulus@zerospam.ca"
              },
              "relationships":{  
                 "addresses":{  
                    "data":[  
                       {  
                          "type":"emails",
                          "id":"2000000000009730"
                       }
                    ]
                 }
              }
           },
           "included":[  
              {  
                 "type":"emails",
                 "id":"2000000000009730",
                 "attributes":{  
                    "usr_id":11748,
                    "domain_id":2,
                    "local_part":"cumulus",
                    "created_at":"2017-03-01 13:55:32.000000 +00:00",
                    "updated_at":"2017-03-01 13:55:32.000000 +00:00",
                    "domain_name":"zerospam.ca"
                 }
              }
           ]
        }';

        $config = $this->preSuccess($jsonResult);

        $request = new ReadUserRequest();
        $request->setOrganizationId(1)
                ->setUserId(11748)
                ->setUserRequestType(UserRequestType::ALL())
                ->withEmailAddresses();

        $client = new ProvulusClient($config);
        /**
         * @var $response UserResponse
         */
        $response = $client->processRequest($request);

        // type assertion
        $response->addresses->each(function ($response) {
            self::assertInstanceOf(UserEmailAddressResponse::class, $response);
        });

        $this->assertEquals(2000000000009730, $response->addresses->first()->id, 'UserEmailAddress id is incorrect.');

        $this->validateRequestUrl($config, [
            'orgs',
            1,
            UserRequestType::ALL,
            11748,
        ]);
    }

    /**
     * @test
     **/
    public function index_user_success_all()
    {

        $jsonResponse
            = '
        {  
           "data":[  
              {  
                 "type":"users",
                 "id":"10075",
                 "attributes":{  
                    "organization_id":2888,
                    "daily_digest_address_id":null,
                    "is_access_allowed":true,
                    "usr_type_code":"admin",
                    "username":"testk@hodor.com",
                    "email_address":"testk@hodor.com",
                    "is_admin_stats_push":false,
                    "is_adminquar_only":false,
                    "login_failures":0,
                    "display_name":"Perdu, Super",
                    "display_name_normalized":"perdu, super",
                    "language_code":"en_us",
                    "timezone_code":{  
                       "timezone_type":3,
                       "timezone":"America\/Montreal"
                    },
                    "quar_search_page_size":25,
                    "last_daily_digest_date":null,
                    "is_active":true,
                    "version":0,
                    "is_active_backup":null,
                    "name":"Super",
                    "surname":"Perdu",
                    "note":null,
                    "password_changed_at":null,
                    "alternative_daily_digest_address":null,
                    "sso_id":null,
                    "is_ldap":false,
                    "phone_number":"+1 418-667-4206",
                    "daily_digest_email":null
                 }
              }
           ],
           "meta":{  
              "pagination":{  
                 "total":1,
                 "count":1,
                 "per_page":15,
                 "current_page":1,
                 "total_pages":1,
                 "links":[  
        
                 ]
              }
           }
        }';

        $config = $this->preSuccess($jsonResponse);

        $request = new IndexUserTypedRequest();
        $request->setOrganizationId(2888)
                ->setUserRequestType(UserRequestType::ALL());

        $client = new ProvulusClient($config);
        $client->processRequest($request);

        $collectionResponse = $request->getResponse();

        /**
         * @var UserResponse $userResponse
         */
        $userResponse = $collectionResponse->data()->first();
        self::assertEquals('Super', $userResponse->get('name'));
        self::assertEquals('America/Montreal', $userResponse->timezone_code->getName());

        $this->validateRequestUrl($config, [
            'orgs',
            2888,
            UserRequestType::ALL,
        ]);
    }

    /**
     * @test
     **/
    public function index_user_success_admins()
    {
        $jsonResponse
            = '{  
               "data":[  
                  {  
                     "type":"users",
                     "id":"10075",
                     "attributes":{  
                        "organization_id":2888,
                        "daily_digest_address_id":null,
                        "is_access_allowed":true,
                        "usr_type_code":"admin",
                        "username":"testk@hodor.com",
                        "email_address":"testk@hodor.com",
                        "is_admin_stats_push":false,
                        "is_adminquar_only":false,
                        "login_failures":0,
                        "display_name":"Perdu, Super",
                        "display_name_normalized":"perdu, super",
                        "language_code":"en_us",
                        "timezone_code":{  
                           "timezone_type":3,
                           "timezone":"America\/Montreal"
                        },
                        "quar_search_page_size":25,
                        "last_daily_digest_date":null,
                        "is_active":true,
                        "version":0,
                        "is_active_backup":null,
                        "name":"Super",
                        "surname":"Perdu",
                        "note":null,
                        "password_changed_at":null,
                        "alternative_daily_digest_address":null,
                        "sso_id":null,
                        "is_ldap":false,
                        "phone_number":"+1 418-667-4206",
                        "daily_digest_email":null
                     }
                  }
               ],
               "meta":{  
                  "pagination":{  
                     "total":1,
                     "count":1,
                     "per_page":15,
                     "current_page":1,
                     "total_pages":1,
                     "links":[  
            
                     ]
                  }
               }
            }';

        $config = $this->preSuccess($jsonResponse);


        $request = new IndexUserTypedRequest();
        $request->setOrganizationId(2888)
                ->setUserRequestType(UserRequestType::ADMIN());

        $client = new ProvulusClient($config);
        $client->processRequest($request);

        $collectionResponse = $request->getResponse();

        /**
         * @var UserResponse $userResponse
         */
        $userResponse = $collectionResponse->data()->first();
        self::assertEquals('+1 418-667-4206', $userResponse->get('phone_number'));
        self::assertEquals('testk@hodor.com', $userResponse->get('username'));

        $this->validateRequestUrl($config, [
            'orgs',
            2888,
            UserRequestType::ADMIN,
        ]);
    }

    /**
     * @test
     **/
    public function index_user_success_regulars()
    {
        $jsonResponse
            = '{  
               "data":[  
                  {  
                     "type":"users",
                     "id":"10094",
                     "attributes":{  
                        "organization_id":2888,
                        "daily_digest_address_id":2000000000006737,
                        "is_access_allowed":true,
                        "usr_type_code":"regular",
                        "username":"testuser@hodor.com",
                        "email_address":"testuser@hodor.com",
                        "is_admin_stats_push":false,
                        "is_adminquar_only":false,
                        "login_failures":0,
                        "display_name":"test, test",
                        "display_name_normalized":"test, test",
                        "language_code":"en_us",
                        "timezone_code":{  
                           "timezone_type":3,
                           "timezone":"America\/Montreal"
                        },
                        "quar_search_page_size":25,
                        "last_daily_digest_date":null,
                        "is_active":true,
                        "version":0,
                        "is_active_backup":null,
                        "name":"test",
                        "surname":"test",
                        "note":"this is a new regular user",
                        "password_changed_at":null,
                        "alternative_daily_digest_address":"testuser@hodor.com",
                        "sso_id":"asdf1234asdf",
                        "is_ldap":false,
                        "phone_number":"+1 418-667-4206",
                        "daily_digest_email":"testuser@hodor.com"
                     }
                  }
               ],
               "meta":{  
                  "pagination":{  
                     "total":1,
                     "count":1,
                     "per_page":15,
                     "current_page":1,
                     "total_pages":1,
                     "links":[  
            
                     ]
                  }
               }
            }';

        $config = $this->preSuccess($jsonResponse);

        $request = new IndexUserTypedRequest();
        $request->setOrganizationId(2888)
                ->setUserRequestType(UserRequestType::REGULAR());

        $client = new ProvulusClient($config);
        $client->processRequest($request);

        $collectionResponse = $request->getResponse();

        /**
         * @var UserResponse $userResponse
         */
        $userResponse = $collectionResponse->data()->first();
        self::assertEquals('+1 418-667-4206', $userResponse->get('phone_number'));
        self::assertEquals('testuser@hodor.com', $userResponse->get('username'));

        $this->validateRequestUrl($config, [
            'orgs',
            2888,
            UserRequestType::REGULAR,
        ]);
    }


    /**
     * @test
     **/
    public function delete_user_success_all()
    {

        $config = $this->preSuccess([]);


        $request = new DeleteUserRequest();
        $request->setOrganizationId(2888)
                ->setUserId(10075)
                ->setUserRequestType(UserRequestType::ALL());

        $client = new ProvulusClient($config);

        $client->processRequest($request);

        $this->validateRequestUrl($config, [
            'orgs',
            2888,
            UserRequestType::ALL,
            10075,
        ]);
    }

    /**
     * @test
     **/
    public function delete_user_success_admin()
    {
        $config = $this->preSuccess([]);


        $request = new DeleteUserRequest();
        $request->setOrganizationId(2888)
                ->setUserId(10075)
                ->setUserRequestType(UserRequestType::ADMIN());

        $client = new ProvulusClient($config);

        $client->processRequest($request);

        $this->validateRequestUrl($config, [
            'orgs',
            2888,
            UserRequestType::ADMIN,
            10075,
        ]);
    }

    /**
     * @test
     **/
    public function delete_user_success_regular()
    {
        $config = $this->preSuccess([]);


        $request = new DeleteUserRequest();
        $request->setOrganizationId(2832)
                ->setUserId(10026)
                ->setUserRequestType(UserRequestType::REGULAR());

        $client = new ProvulusClient($config);

        $client->processRequest($request);

        $this->validateRequestUrl($config, [
            'orgs',
            2832,
            UserRequestType::REGULAR,
            10026,
        ]);
    }

    /**
     * @test
     */
    public function update_user_password()
    {
        $jsonBody     = '{
            "current_password": "Password1",
            "new_password": "passworD2",
            "new_password_confirm": "passworD2",
            "tfa_code": "123456"
        }';
        $jsonResponse = [];

        $config = $this->preSuccess($jsonResponse);

        $request = new UpdateUserPasswordRequest();
        $request->setOrganizationId(2832)
                ->setUserId(10026)
                ->setCurrentPassword("Password1")
                ->setNewPassword("passworD2")
                ->setNewPasswordConfirm("passworD2")
                ->setTfaCode("123456");

        $client = new ProvulusClient($config);
        $client->processRequest($request);

        $this->validateRequestUrl($config, ['orgs', 2832, 'users', 10026, 'updatePassword']);
        $this->validateRequest($config, $jsonBody);
    }

    /**
     * @test
     * @expectedException \ProvulusSDK\Exception\API\ValidationException
     */
    public function update_password_under_min_length()
    {
        $jsonBody = '{
            "current_password": "Password1",
            "new_password": "l",
            "new_password_confirm": "l"
        }';
        $jsonResponse
                  = '{  
   "errors":{  
      "password":[  
         {  
            "code":"password_validator_error",
            "message":"The password does not comply with the following rules : min. length [12].",
            "value":"short"
         }
      ]
   }
}';

        $config = $this->preFailure($jsonResponse);

        $request = new UpdateUserPasswordRequest();
        $request->setOrganizationId(2832)
                ->setUserId(10026)
                ->setCurrentPassword("Password1")
                ->setNewPassword("l")
                ->setNewPasswordConfirm("l");

        $client = new ProvulusClient($config);

        try {
            $client->processRequest($request);
        } catch (ValidationException $validationException) {

            $this->validateRequest($config, $jsonBody);

            $validationData = $validationException->getValidationData();
            $this->assertArrayHasKey('password', $validationData);
            /**
             * @var $first ValidationError
             */
            $first = $validationData['password']->getValidationErrorCollection()->first();
            $this->assertInstanceOf(ValidationError::class, $first);
            $this->assertEquals('password_validator_error', $first->getCode());
            $this->assertEquals('The password does not comply with the following rules : min. length [12].',
                $first->getMessage());

            throw $validationException;
        }

        $this->validateRequestUrl($config, ['orgs', 2832, 'users', 10026, 'updatePassword']);
        $this->validateRequest($config, $jsonBody);
    }

    /**
     * @test
     */
    public function reset_password_authenticated_user()
    {
        $config = $this->preSuccess([]);

        $passwordRequest = new ResetPasswordAuthenticatedRequest();
        $passwordRequest->setOrganizationId(1)->setUserId(5);

        $client = new ProvulusClient($config);
        $client->processRequest($passwordRequest);

        $this->validateRequestUrl($config, [1, 5, 'reset']);
    }

    /**
     * @test
     */
    public function user_to_contact_create_request()
    {
        $jsonResult
            = '{
  "data": {
    "type": "contact-person",
    "id": "1808",
    "attributes": {
      "is_default": false,
      "organization_id": 3681,
      "phone": "+1 514-527-3232",
      "title": "Administration",
      "email": "support@zerospam.ca",
      "updated_at": "2018-09-11 15:41:44.350927 +00:00",
      "created_at": "2018-09-11 15:41:44.350927 +00:00",
      "language": "en_us"
    }
  }
}';

        $jsonRequest
            = '{
   "mailgroups" : [
   	  "service_activation"
   	  ],
  "phone": "15145273232"
}';

        $config = $this->preSuccess($jsonResult);


        $request = new UserToContactRequest();

        $request->setOrganizationId(3681)
                ->setEmail('support@zerospam.ca')
                ->setPhone('15145273232')
                ->setUserId(1234)
                ->setTitle('Administration')
                ->setMailgroups([MailGroupEnum::SERVICE_ACTIVATION()]);

        $client = new ProvulusClient($config);
        $client->processRequest($request);

        $this->validateRequest($config, $jsonRequest);

        $response = $request->getResponse();
        $this->assertEquals(3681, $response->organization_id);
        $this->assertEquals(1808, $response->id());
        $this->assertTrue(
            PhoneNumberUtil::getInstance()->parse('15145273232', 'US')
                           ->equals(PhoneNumberUtil::getInstance()->parse($response->phone, 'US'))
        );
    }

    /**
     * @test
     **/
    public function userIndexWithCriterion()
    {
        $jsonResult
            = '{
  "data": [
    {
      "type": "users",
      "id": "173233",
      "attributes": {
        "organization_id": 1,
        "is_access_allowed": true,
        "usr_type_code": "admin",
        "username": "provulus@zerospam.ca",
        "email_address": "provulus@zerospam.ca",
        "is_admin_stats_push": false,
        "is_adminquar_only": false,
        "login_failures": 0,
        "display_name": "Zerospam, Provulus",
        "display_name_normalized": "zerospam, provulus",
        "language_code": "en_us",
        "timezone_code": {
          "timezone_type": 3,
          "timezone": "America/Montreal"
        },
        "quar_search_page_size": 25,
        "last_daily_digest_date": null,
        "is_active": true,
        "created_at": "2017-08-17 20:44:46.991992 +00:00",
        "updated_at": "2017-08-21 18:50:51.000000 +00:00",
        "is_active_backup": null,
        "name": "Provulus",
        "surname": "Zerospam",
        "password_changed_at": "2017-08-21 18:46:25.449601 +00:00",
        "sso_id": null,
        "is_ldap": false,
        "phone_number": null,
        "daily_digest_email": null,
        "is_locked_down": false,
        "full_name": "Provulus Zerospam"
      },
      "relationships": {
        "lastSession": {
          "data": {
            "type": "user_session",
            "id": "12419724"
          }
        }
      }
    }
  ],
  "included": [
    {
      "type": "user_session",
      "id": "12419724",
      "attributes": {
        "usr_id": 173233,
        "is_expired": true,
        "php_web_session_id": "6m7q61t98ffqmv5sg67e09k0s2",
        "started_at": "2017-08-21 18:50:50.940458 +00:00",
        "last_accessed_at": "2017-08-21 18:50:57.514289 +00:00",
        "mode_code": 2,
        "ip_address": "10.0.0.224",
        "browser": "Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/60.0.3112.101 Safari/537.36",
        "created_at": "2017-08-21 18:50:51.000000 +00:00",
        "updated_at": "2017-08-21 18:50:57.000000 +00:00",
        "portal_prefix": "zerospam"
      }
    }
  ],
  "meta": {
    "pagination": {
      "total": 1,
      "count": 1,
      "per_page": 15,
      "current_page": 1,
      "total_pages": 1
    }
  },
  "links": {
    "self": "http://cumulus.local/api/v1/orgs/1/users?page=1",
    "first": "http://cumulus.local/api/v1/orgs/1/users?page=1",
    "last": "http://cumulus.local/api/v1/orgs/1/users?page=1"
  }
}';

        $jsonRequest = [];

        $config = $this->preSuccess($jsonResult);


        $request = new IndexUserTypedRequest();

        $request->setOrganizationId(1)
                ->setUserRequestType(UserRequestType::ALL())
                ->setUserTypeArgument(UserTypeEnum::ADMIN())
                ->setAddressArgument('provulus')
                ->setUsernameArgument('prov')
                ->withLastSession()
                ->setIncludeLdapArgument(true);

        $client = new ProvulusClient($config);

        $client->processRequest($request);
        $response = $request->getResponse();

        $this->validateRequest($config, $jsonRequest);
        $this->validateUrl($config,
            '?user_type=admin&address=provulus&username=prov&include_ldap=1&include=lastSession');

        /** @var UserResponse $response */
        $response = $response->data()->first();

        $this->assertEquals('1', $response->organization_id);
        $this->assertEquals(173233, $response->id());
        $this->assertTrue($response->usr_type_code->is(UserTypeEnum::ADMIN()));
        $this->assertContains('provulus', $response->email_address);
        $this->assertContains('prov', $response->username);

        $this->assertInstanceOf(UserSessionResponse::class, $response->last_session);

    }

    /**
     * @test
     **/
    public function bulkUsersDelete()
    {
        $jsonResult = [];

        $jsonRequest
            = '{
            "ids" :[
            3748, 
            3747
            ]
        }';

        $config = $this->preSuccess($jsonResult);

        $request = new BulkDeleteUserRequest();
        $request->setOrganizationId(1)
                ->setIds([
                    3748,
                    3747,
                ]);

        $client = new ProvulusClient($config);
        $client->processRequest($request);

        $this->validateRequest($config, $jsonRequest);
        $this->validateUrl($config, 'orgs/1/users');

        $response = $request->getResponse();
        $this->assertInstanceOf(EmptyResponse::class, $response);
    }

    /** @test */
    public function promote_user_success()
    {
        $jsonResult = '{
            "data": {
                "type": "users",
                "id": "37",
                "attributes": {
                    "organization_id": 13,
                    "is_access_allowed": true,
                    "usr_type_code": "admin",
                    "username": "yves.yak@zerospam.ca",
                    "email_address": "yves.yak@zerospam.ca",
                    "is_admin_stats_push": false,
                    "is_adminquar_only": true,
                    "login_failures": 0,
                    "display_name": "",
                    "display_name_normalized": "",
                    "language_code": "fr_ca",
                    "timezone_code": {
                        "timezone_type": 3,
                        "timezone": "America/Montreal"
                    },
                    "quar_search_page_size": 25,
                    "last_daily_digest_date": null,
                    "is_active": true,
                    "created_at": "2017-11-01 17:48:14.017197 +00:00",
                    "updated_at": "2017-11-28 15:14:26.404162 +00:00",
                    "is_active_backup": null,
                    "name": null,
                    "surname": null,
                    "password_changed_at": null,
                    "sso_id": null,
                    "is_ldap": false,
                    "phone_number": null,
                    "daily_digest_email": null,
                    "is_locked_down": false,
                    "full_name": "",
                    "user_type": "admin_quar"
                }
            }
        }';

        $jsonRequest = '{"is_quarantine": true}';

        $config = $this->preSuccess($jsonResult);

        $request = (new PromoteUserRequest)
            ->setUserRequestType(UserRequestType::REGULAR())
            ->setOrganizationId(13)
            ->setUserId(37)
            ->setIsQuarantine(true);

        $client = new ProvulusClient($config);
        $client->processRequest($request);

        $this->validateRequest($config, $jsonRequest);
        $this->validateUrl($config, 'orgs/13/regulars/37/promote');
    }
}