<?php
/**
 * Created by PhpStorm.
 * User: pbb
 * Date: 01/03/17
 * Time: 8:49 AM
 */

namespace automated\Requests\Organization\User\UserEmailAddress;

use ProvulusSDK\Client\ProvulusClient;
use ProvulusSDK\Client\Request\User\UserEmailAddress\DeleteBulkUserEmailAddressRequest;
use ProvulusSDK\Client\Request\User\UserEmailAddress\IndexUserEmailAddressRequest;
use ProvulusSDK\Client\Request\User\UserEmailAddress\ReadUserEmailAddressRequest;
use ProvulusSDK\Client\Response\EmptyResponse;
use ProvulusSDK\Client\Response\User\UserEmailAddress\UserEmailAddressResponse;
use ProvulusTest\TestCaseApiKey;

/**
 * Class UserEmailAddressTest
 *
 * @package automated\Requests\Organization\User\UserEmailAddress
 */
class UserEmailAddressTest extends TestCaseApiKey
{

    public function index_user_email_addresses()
    {
        $jsonResult
            = '  
            {  
               "data":[  
                  {  
                     "type":"addresses",
                     "id":"2000000000009731",
                     "attributes":{  
                        "usr_id":11748,
                        "domain_id":2,
                        "local_part":"cumulus2",
                        "created_at":"2017-03-01 15:54:55.000000 +00:00",
                        "updated_at":"2017-03-01 15:54:55.000000 +00:00",
                        "domain_name":"zerospam.ca"
                     }
                  },
                  {  
                     "type":"addresses",
                     "id":"2000000000009730",
                     "attributes":{  
                        "usr_id":11748,
                        "domain_id":2,
                        "local_part":"cumulus",
                        "created_at":"2017-03-01 13:55:32.000000 +00:00",
                        "updated_at":"2017-03-01 13:55:32.000000 +00:00",
                        "domain_name":"zerospam.ca"
                     }
                  }
               ],
               "meta":{  
                  "pagination":{  
                     "total":2,
                     "count":2,
                     "per_page":15,
                     "current_page":1,
                     "total_pages":1
                  }
               }
            }';

        $jsonRequest = [];

        $config = $this->preSuccess($jsonResult);


        $request = new IndexUserEmailAddressRequest();
        $request->setOrganizationId(1)
                ->setUserId(11748);

        $client = new ProvulusClient($config);
        $client->processRequest($request);

        $collectionResponse = $request->getResponse();

        $response = $collectionResponse->data()->first();
        self::assertEquals(2000000000009731, $response->id());
        self::assertCount(2, $collectionResponse->data());

        $this->validateRequest($config, $jsonRequest);
        $this->validateRequestUrl($config, ['orgs/1/users/11748/emails']);
    }

    /**
     * @test
     **/
    public function read_user_email_address()
    {
        $jsonResult
            = '
                {  
           "data":{  
              "type":"addresses",
              "id":"2000000000009731",
              "attributes":{  
                 "usr_id":11748,
                 "domain_id":2,
                 "local_part":"cumulus2",
                 "created_at":"2017-03-01 15:54:55.000000 +00:00",
                 "updated_at":"2017-03-01 15:54:55.000000 +00:00",
                 "domain_name":"zerospam.ca"
              }
           }
        }';

        $jsonRequest = [];

        $config = $this->preSuccess($jsonResult);

        $request = new ReadUserEmailAddressRequest();
        $request->setOrganizationId(1)
                ->setUserId(11748)
                ->setEmailId(2000000000009731);

        $client = new ProvulusClient($config);
        $client->processRequest($request);

        /**
         * @var UserEmailAddressResponse $response
         */
        $response = $request->getResponse();
        self::assertEquals(2000000000009731, $response->id());
        self::assertEquals(2, $response->domain_id);

        $this->validateRequest($config, $jsonRequest);
        $this->validateRequestUrl($config, ['orgs/1/users/11748/emails/2000000000009731']);
    }

    /**
     * @test
     **/
    public function bulk_email_delete_success()
    {

        $jsonResult = [];

        $jsonRequest = [];

        $config = $this->preSuccess($jsonResult);

        $request = new DeleteBulkUserEmailAddressRequest();
        $request->setOrganizationId(1)
                ->setUserId(11748)
                ->setIds([
                    1,
                    2,
                    3,
                ]);

        $client = new ProvulusClient($config);
        $client->processRequest($request);

        /**
         * @var EmptyResponse $response
         */
        $response = $request->getResponse();
        self::assertInstanceOf(EmptyResponse::class, $response);

        $this->validateRequest($config, $jsonRequest);
        $this->validateRequestUrl($config, ['orgs/1/users/11748/emails']);
    }

    /**
     * @test
     * @expectedException \InvalidArgumentException
     **/
    public function bulk_addresses_delete_invalid_argument()
    {
        $request = new DeleteBulkUserEmailAddressRequest();
        $request->setIds([
            1,
            2,
            'allo',
        ]);
    }
}