<?php
/**
 * Created by PhpStorm.
 * User: pbb
 * Date: 21/02/17
 * Time: 3:28 PM
 */

namespace automated\Requests\Organization;


use ProvulusSDK\Client\ProvulusClient;
use ProvulusSDK\Client\Request\Resource\RenewalOrganizationsIndexRequest;
use ProvulusSDK\Client\Response\Organization\OrganizationResponse;
use ProvulusSDK\Enum\Renewal\PaymentRenewalStateEnum;
use ProvulusTest\TestCaseApiKey;

/**
 * Class RenewalStateOrganizationTest
 *
 * tests RenewalStateOrganizationRequest
 *
 * @package automated\Requests\Organization
 */
class RenewalStateOrganizationTest extends TestCaseApiKey
{

    /**
     * @test
     **/
    public function index_organizations_state_no_domain()
    {
        $jsonResult
            = '{  
           "data":[  
              {   
                  "type":"orgs",
                  "id":"2805",
                  "attributes":{  
                     "support_organization_id":1,
                     "billing_organization_id":1,
                     "is_access_allowed":true,
                     "is_billing_organization":true,
                     "is_support_organization":true,
                     "corporate_name":"blah5.com Reseller",
                     "corporate_name_normalized":"blah5.com reseller",
                     "portal_prefix":null,
                     "default_language_code":"en_us",
                     "timezone_code":{  
                        "timezone_type":3,
                        "timezone":"America\/Montreal"
                     },
                     "note_shared":null,
                     "quar_expiry_in_days":14,
                     "note_for_zs_only":null,
                     "logo_reference":null,
                     "is_deact_req":false,
                     "deact_req_reason":null,
                     "deact_req_from_display_name":null,
                     "deact_req_from_corporate_name":null,
                     "deact_req_issued_at":null,
                     "deact_req_target_date":null,
                     "deact_req_executed_at":null,
                     "is_active":true,
                     "version":0,
                     "created_at":"2016-11-18 19:47:05.000000 +00:00",
                     "updated_at":"2017-01-31 16:12:37.681101 +00:00",
                     "dsbl_portal_selfprov":false,
                     "dsbl_org_selfprov":false,
                     "currency":"cad",
                     "renewal_messages_allowed":true,
                     "renewal_note":null,
                     "yearly_renewal_date":null,
                     "renewal_state":"no_domain",
                     "renewal_msg_sent_at":null,
                     "next_renewal_msg":null,
                     "billing_period":null,
                     "billing_base_date":null,
                     "salesforce_id":"0012C000006ew5cQAA",
                     "shows_powered_by_logo":true,
                     "outbound_dg_bounce":null,
                     "outbound_dg_default":null,
                     "outbound_global_domains":true,
                     "outbound_filter_active":false,
                     "outbound_relay":null,
                     "is_distributor_organization":false,
                     "distributor_id":null,
                     "external_distributor_id":null,
                     "activation_date":null
                  }
               },
               {  
                  "type":"orgs",
                  "id":"2806",
                  "attributes":{  
                     "support_organization_id":1,
                     "billing_organization_id":1,
                     "is_access_allowed":true,
                     "is_billing_organization":true,
                     "is_support_organization":true,
                     "corporate_name":"blah6.com Reseller",
                     "corporate_name_normalized":"blah6.com reseller",
                     "portal_prefix":null,
                     "default_language_code":"en_us",
                     "timezone_code":{  
                        "timezone_type":3,
                        "timezone":"America\/Montreal"
                     },
                     "note_shared":null,
                     "quar_expiry_in_days":14,
                     "note_for_zs_only":null,
                     "logo_reference":null,
                     "is_deact_req":false,
                     "deact_req_reason":null,
                     "deact_req_from_display_name":null,
                     "deact_req_from_corporate_name":null,
                     "deact_req_issued_at":null,
                     "deact_req_target_date":null,
                     "deact_req_executed_at":null,
                     "is_active":true,
                     "version":0,
                     "created_at":"2016-11-18 19:49:48.000000 +00:00",
                     "updated_at":"2017-01-31 16:12:37.681101 +00:00",
                     "dsbl_portal_selfprov":false,
                     "dsbl_org_selfprov":false,
                     "currency":"cad",
                     "renewal_messages_allowed":true,
                     "renewal_note":null,
                     "yearly_renewal_date":null,
                     "renewal_state":"no_domain",
                     "renewal_msg_sent_at":null,
                     "next_renewal_msg":null,
                     "billing_period":null,
                     "billing_base_date":null,
                     "salesforce_id":"0012C000006ew5mQAA",
                     "shows_powered_by_logo":true,
                     "outbound_dg_bounce":null,
                     "outbound_dg_default":null,
                     "outbound_global_domains":true,
                     "outbound_filter_active":false,
                     "outbound_relay":null,
                     "is_distributor_organization":false,
                     "distributor_id":null,
                     "external_distributor_id":null,
                     "activation_date":null
                  }
               }
            ],
            "meta":{
                  "pagination":{
                     "total":2,
                     "count":2,
                     "per_page":15,
                     "current_page":1,
                     "total_pages":1
                  }
               }
            }';

        $config  = $this->preSuccess($jsonResult);
        $request = new RenewalOrganizationsIndexRequest();
        $request->setRenewalStateParameter(PaymentRenewalStateEnum::NO_DOMAIN());

        $client = new ProvulusClient($config);
        $client->processRequest($request);

        $response = $request->getResponse();

        $response->data()->each(function (OrganizationResponse $response) {
            self::assertTrue($response->renewal_state->is(PaymentRenewalStateEnum::NO_DOMAIN()));
        });
    }

    /**
     * @test
     **/
    public function unset_renewal_state_argument()
    {
        $request = new RenewalOrganizationsIndexRequest();

        $request->setRenewalStateParameter(null);
    }
}
