<?php
/**
 * Created by PhpStorm.
 * User: ycoutu
 * Date: 19/09/18
 * Time: 11:33 AM
 */

namespace automated\Requests\Organization\Billing;

use Carbon\Carbon;
use ProvulusSDK\Client\ProvulusClient;
use ProvulusSDK\Client\Request\Organization\Billing\CreateBillingRequest;
use ProvulusSDK\Client\Request\Organization\Billing\IndexBillingRequest;
use ProvulusSDK\Client\Request\Organization\Billing\ReadBillingRequest;
use ProvulusSDK\Client\Request\Organization\Billing\UpdateBillingRequest;
use ProvulusSDK\Client\Response\Organization\Billing\BillingResponse;
use ProvulusSDK\Enum\PaymentCurrency\PaymentCurrencyEnum;
use ProvulusSDK\Utils\Organization\Billing\Rebate;
use ProvulusTest\TestCaseApiKey;

class BillingTest extends TestCaseApiKey
{
    /** @test */
    public function read_billing_request()
    {
        $jsonRequest = '{}';
        $jsonResponse = <<<RESPONSE
{
    "data": {
        "type": "billing",
        "id": "2357",
        "attributes": {
            "external_id": "def456",
            "currency": "usd",
            "city": "New York",
            "country": "US",
            "postal_code": "H0H 0H0",
            "state": "NY",
            "street1": "2 Street",
            "street2": "app. 12",
            "created_at": "2018-11-15 18:46:16.000000 +00:00",
            "updated_at": "2019-02-27 14:33:05.000000 +00:00",
            "price_list_id": 1,
            "email": "jp@email.com",
            "last_name": "Paul",
            "first_name": "Joan",
            "external_url": "https://my.freshbooks.com/#/client/def456"
        }
    }
}
RESPONSE;

        $config = $this->preSuccess($jsonResponse);

        $client = new ProvulusClient($config);

        $request = (new ReadBillingRequest)
            ->setOrganizationId(2357);

        $client->processRequest($request);

        $this->validateRequestUrl($config, ['/orgs/2357/billing']);

        $this->validateRequest($config, $jsonRequest);

        $response = $request->getResponse();

        $this->assertEquals("def456", $response->external_id);
        $this->assertTrue($response->currency->is(PaymentCurrencyEnum::USD()));
        $this->assertEquals("New York", $response->city);
        $this->assertEquals("US", $response->country);
        $this->assertEquals("H0H 0H0", $response->postal_code);
        $this->assertEquals("NY", $response->state);
        $this->assertEquals("2 Street", $response->street1);
        $this->assertEquals("app. 12", $response->street2);
        $this->assertTrue($response->created_at->eq(
            Carbon::createSafe(2018, 11, 15, 18, 46, 16, '+00:00'))
        );
        $this->assertTrue($response->updated_at->eq(
            Carbon::createSafe(2019, 2, 27, 14, 33, 5, '+00:00'))
        );
        $this->assertEquals(1, $response->price_list_id);
        $this->assertEquals("jp@email.com", $response->email);
        $this->assertEquals("Paul", $response->last_name);
        $this->assertEquals("Joan", $response->first_name);
        $this->assertEquals("https://my.freshbooks.com/#/client/def456", $response->external_url);
    }

    /** @test */
    public function index_billing_request()
    {
        $jsonRequest = '{}';
        $jsonResponse = <<<RESPONSE
{
    "data": [{
        "type": "billing",
        "id": "4",
        "attributes": {
            "external_id": "abc123",
            "currency": "cad",
            "city": "Montréal",
            "country": "CA",
            "postal_code": "H0H 0H0",
            "state": "QC",
            "street1": "1 Main Street",
            "street2": "",
            "created_at": "2017-08-09 18:46:16.000000 +00:00",
            "updated_at": null,
            "price_list_id": 1,
            "email": "js@email.com",
            "last_name": "Smith",
            "first_name": "John",
            "external_url": "https://my.freshbooks.com/#/client/abc123"
        }
    },{
        "type": "billing",
        "id": "6",
        "attributes": {
            "external_id": "def456",
            "currency": "usd",
            "city": "New York",
            "country": "US",
            "postal_code": "H0H 0H0",
            "state": "NY",
            "street1": "2 Street",
            "street2": "app. 12",
            "created_at": null,
            "updated_at": null,
            "price_list_id": 1,
            "email": "jp@email.com",
            "last_name": "Paul",
            "first_name": "Joan",
            "external_url": "https://my.freshbooks.com/#/client/def456"
        }
    }],
    "meta": {
        "pagination": {
            "total": 2,
            "count": 2,
            "per_page": 15,
            "current_page": 1,
            "total_pages": 1
        }
    }
}
RESPONSE;

        $config = $this->preSuccess($jsonResponse);

        $client = new ProvulusClient($config);

        $request = new IndexBillingRequest();

        $client->processRequest($request);

        $this->validateRequest($config, $jsonRequest);

        $response = $request->getResponse();

        $this->validateRequestUrl($config, ['/orgs/billings']);

        $this->assertEquals(2, $response->getMetaData()->count);

        /** @var BillingResponse $billingResponse */
        $billingResponse = $response->data()->first();

        $this->assertEquals("abc123", $billingResponse->external_id);
        $this->assertTrue($billingResponse->currency->is(PaymentCurrencyEnum::CAD()));
        $this->assertEquals("Montréal", $billingResponse->city);
        $this->assertEquals("CA", $billingResponse->country);
        $this->assertEquals("H0H 0H0", $billingResponse->postal_code);
        $this->assertEquals("QC", $billingResponse->state);
        $this->assertEquals("1 Main Street", $billingResponse->street1);
        $this->assertEquals("", $billingResponse->street2);
        $this->assertTrue($billingResponse->created_at->eq(
            Carbon::createSafe(2017, 8, 9, 18, 46, 16, '+00:00'))
        );
        $this->assertNull($billingResponse->updated_at);
        $this->assertEquals(1, $billingResponse->price_list_id);
        $this->assertEquals("js@email.com", $billingResponse->email);
        $this->assertEquals("Smith", $billingResponse->last_name);
        $this->assertEquals("John", $billingResponse->first_name);
        $this->assertEquals("https://my.freshbooks.com/#/client/abc123", $billingResponse->external_url);
    }

    /** @test */
    public function create_billing_request()
    {
        $jsonRequest = <<<REQUEST
{
    "currency": "eur",
    "city": "Paris",
    "country": "FR",
    "postal_code": "55443",
    "state": "PP",
    "street1": "5 rue Principale",
    "street2": "Suite 545",
    "email": "jean.cool@mail.fr",
    "last_name": "Cool",
    "first_name": "Jean"
}
REQUEST;

        $jsonResponse = <<<RESPONSE
{
    "data": {
        "type": "billing",
        "id": "4142",
        "attributes": {
            "external_id": "gogo9090",
            "currency": "eur",
            "city": "Paris",
            "country": "FR",
            "postal_code": "55443",
            "state": "PP",
            "street1": "5 rue Principale",
            "street2": "Suite 545",
            "created_at": "2018-11-15 18:46:16.000000 +00:00",
            "updated_at": null,
            "price_list_id": 7,
            "email": "jean.cool@mail.fr",
            "last_name": "Cool",
            "first_name": "Jean",
            "external_url": "https://my.freshbooks.com/#/client/gogo9090"
        }
    }
}
RESPONSE;

        $config = $this->preSuccess($jsonResponse);

        $client = new ProvulusClient($config);

        $request = (new CreateBillingRequest)
            ->setOrganizationId(4142)
            ->setCurrency(PaymentCurrencyEnum::EUR())
            ->setCity("Paris")
            ->setCountry("FR")
            ->setEmail("jean.cool@mail.fr")
            ->setFirstName("Jean")
            ->setLastName("Cool")
            ->setPostalCode("55443")
            ->setState("PP")
            ->setStreet1("5 rue Principale")
            ->setStreet2("Suite 545");

        $client->processRequest($request);

        $this->validateRequest($config, $jsonRequest);

        $response = $request->getResponse();

        $this->assertEquals("gogo9090", $response->external_id);
        $this->assertTrue($response->currency->is(PaymentCurrencyEnum::EUR()));
        $this->assertEquals("Paris", $response->city);
        $this->assertEquals("FR", $response->country);
        $this->assertEquals("55443", $response->postal_code);
        $this->assertEquals("PP", $response->state);
        $this->assertEquals("5 rue Principale", $response->street1);
        $this->assertEquals("Suite 545", $response->street2);
        $this->assertTrue($response->created_at->eq(
            Carbon::createSafe(2018, 11, 15, 18, 46, 16, '+00:00'))
        );
        $this->assertNull($response->updated_at);
        $this->assertEquals(7, $response->price_list_id);
        $this->assertEquals("jean.cool@mail.fr", $response->email);
        $this->assertEquals("Cool", $response->last_name);
        $this->assertEquals("Jean", $response->first_name);
        $this->assertEquals("https://my.freshbooks.com/#/client/gogo9090", $response->external_url);
    }

    /** @test */
    public function update_billing_request()
    {
        $jsonRequest = <<<REQUEST
{
    "currency": "eur",
    "city": "Paris",
    "country": "FR",
    "postal_code": "55443",
    "state": "PP",
    "street1": "5 rue Principale",
    "street2": null,
    "email": "jean.cool@mail.fr",
    "last_name": "Cool",
    "first_name": "Jean",
    "external_id": "extid",
    "pricelist": 4,
    "rebates": [{
        "id": 3
    }, {
        "id": 5,
        "expiry": "2020-01-01T00:00:00+00:00"
    },{
        "id": 11,
        "expiry": null
    }]
}
REQUEST;

        $jsonResponse = <<<RESPONSE
{
    "data": {
        "type": "billing",
        "id": "4142",
        "attributes": {
            "external_id": "extid",
            "currency": "eur",
            "city": "Paris",
            "country": "FR",
            "postal_code": "55443",
            "state": "PP",
            "street1": "5 rue Principale",
            "street2": "",
            "created_at": "2018-11-15 18:46:16.000000 +00:00",
            "updated_at": null,
            "price_list_id": 4,
            "email": "jean.cool@mail.fr",
            "last_name": "Cool",
            "first_name": "Jean",
            "external_url": "https://my.freshbooks.com/#/client/extid"
        }
    }
}
RESPONSE;

        $config = $this->preSuccess($jsonResponse);

        $client = new ProvulusClient($config);

        $rebates = [
            (new Rebate)
                ->setId(3),
            (new Rebate)
                ->setId(5)
                ->setExpiry(Carbon::create(2020, 1, 1, 0, 0, 0, 'GMT')),
            (new Rebate)
                ->setId(11)
                ->setExpiry(null),
        ];

        $request = (new UpdateBillingRequest)
            ->setOrganizationId(4142)
            ->setCurrency(PaymentCurrencyEnum::EUR())
            ->setCity("Paris")
            ->setCountry("FR")
            ->setEmail("jean.cool@mail.fr")
            ->setFirstName("Jean")
            ->setLastName("Cool")
            ->setPostalCode("55443")
            ->setState("PP")
            ->setStreet1("5 rue Principale")
            ->setStreet2(null)
            ->setPricelist(4)
            ->setRebates($rebates)
            ->setExternalId("extid");

        $client->processRequest($request);

        $this->validateRequest($config, $jsonRequest);

        $response = $request->getResponse();

        $this->assertEquals("extid", $response->external_id);
        $this->assertTrue($response->currency->is(PaymentCurrencyEnum::EUR()));
        $this->assertEquals("Paris", $response->city);
        $this->assertEquals("FR", $response->country);
        $this->assertEquals("55443", $response->postal_code);
        $this->assertEquals("PP", $response->state);
        $this->assertEquals("5 rue Principale", $response->street1);
        $this->assertEquals("", $response->street2);
        $this->assertTrue($response->created_at->eq(
            Carbon::createSafe(2018, 11, 15, 18, 46, 16, '+00:00'))
        );
        $this->assertNull($response->updated_at);
        $this->assertEquals(4, $response->price_list_id);
        $this->assertEquals("jean.cool@mail.fr", $response->email);
        $this->assertEquals("Cool", $response->last_name);
        $this->assertEquals("Jean", $response->first_name);
        $this->assertEquals("https://my.freshbooks.com/#/client/extid", $response->external_url);
    }
}
