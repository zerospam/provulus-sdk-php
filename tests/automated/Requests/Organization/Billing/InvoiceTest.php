<?php
/**
 * Created by PhpStorm.
 * User: ycoutu
 * Date: 11/12/18
 * Time: 4:48 PM
 */

namespace automated\Requests\Organization\Billing;

use ProvulusSDK\Client\ProvulusClient;
use ProvulusSDK\Client\Request\Organization\Billing\Invoice\IndexInvoiceRequest;
use ProvulusSDK\Client\Response\Organization\Billing\Invoice\InvoiceResponse;
use ProvulusSDK\Enum\PaymentCurrency\PaymentCurrencyEnum;
use ProvulusTest\TestCaseApiKey;

class InvoiceTest extends TestCaseApiKey
{
    /** @test */
    public function index_invoice_request()
    {
        $jsonRequest = '{}';
        $jsonResponse = <<<JSON
{
    "data": [{
        "type": "invoice",
        "id": "11",
        "attributes": {
            "billing_id": 434,
            "customer_external_id": "1132555",
            "external_id": "3332123",
            "note": "Billing Period: 2020-01-01 - 2020-12-31",
            "terms": "The terms",
            "currency": "usd",
            "created_at": "2018-12-05 16:03:38.000000 +00:00",
            "updated_at": "2018-12-05 16:05:24.000000 +00:00",
            "invoice_number": "4342012",
            "total_in_cad": 1234.56,
            "billing_start": "2020-01-01 00:00:00.000000 +00:00",
            "billing_end": "2020-12-31 00:00:00.000000 +00:00",
            "sent_at": "2018-12-05 16:05:24.000000 +00:00",
            "is_sent": true,
            "external_url": "https://url.example",
            "total": 1001.01
        }
    }],
    "meta": {
        "pagination": {
            "total": 1,
            "count": 1,
            "per_page": 15,
            "current_page": 1,
            "total_pages": 1
        }
    }
}
JSON;
        $config = $this->preSuccess($jsonResponse);
        $client = new ProvulusClient($config);
        $request = new IndexInvoiceRequest();
        $request->setOrganizationId(434);
        $client->processRequest($request);
        $this->validateRequest($config, $jsonRequest);

        $response = $request->getResponse();

        $this->validateRequestUrl($config, ['/orgs/434/billing/invoices']);

        $this->assertEquals(1, $response->getMetaData()->count);

        /** @var InvoiceResponse $invoice */
        $invoice = $response->data()->first();

        $this->assertEquals(434, $invoice->billing_id);
        $this->assertEquals("1132555", $invoice->customer_external_id);
        $this->assertEquals("3332123", $invoice->external_id);
        $this->assertEquals("Billing Period: 2020-01-01 - 2020-12-31", $invoice->note);
        $this->assertEquals("The terms", $invoice->terms);
        $this->assertTrue($invoice->currency->is(PaymentCurrencyEnum::USD()));
        $this->assertEquals("2018-12-05 16:03:38", $invoice->created_at->toDateTimeString());
        $this->assertEquals("2018-12-05 16:05:24", $invoice->updated_at->toDateTimeString());
        $this->assertEquals("4342012", $invoice->invoice_number);
        $this->assertEquals(1234.56, $invoice->total_in_cad, null, 0.001);
        $this->assertEquals("2020-01-01", $invoice->billing_start->toDateString());
        $this->assertEquals("2020-12-31", $invoice->billing_end->toDateString());
        $this->assertEquals("2018-12-05 16:05:24", $invoice->sent_at->toDateTimeString());
        $this->assertTrue($invoice->is_sent);
        $this->assertEquals("https://url.example", $invoice->external_url);
        $this->assertEquals(1001.01, $invoice->total, null, 0.001);
    }
}
