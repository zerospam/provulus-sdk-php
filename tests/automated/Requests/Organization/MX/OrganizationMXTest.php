<?php
/**
 * Created by PhpStorm.
 * User: pbb
 * Date: 23/12/16
 * Time: 11:44 AM
 */

namespace automated\Requests\Organization\MX;

use GuzzleHttp\Handler\MockHandler;
use ProvulusSDK\Client\ProvulusClient;
use ProvulusSDK\Client\Request\Organization\MX\CreateMXRequest;
use ProvulusSDK\Client\Request\Organization\MX\DeleteMXRequest;
use ProvulusSDK\Client\Request\Organization\MX\IndexMXRequest;
use ProvulusSDK\Client\Request\Organization\MX\ReadMXRequest;
use ProvulusSDK\Client\Request\Organization\MX\UpdateMXRequest;
use ProvulusSDK\Client\Response\Organization\MX\OrganizationMXResponse;
use ProvulusSDK\Config\ProvulusConfiguration;
use ProvulusSDK\Exception\API\ValidationException;
use ProvulusTest\TestCase;

/**
 * Class CreateMXTest
 *
 * Test for CreateMXRequest
 *
 * @package automated\Requests\Organization\MX
 */
class OrganizationMXTest extends TestCase
{
    /**
     * @test
     **/
    public function create_mx_success()
    {
        $jsonResult
            = '{  
               "data":[  
                  {  
                     "type":"mxs",
                     "id":"5446",
                     "attributes":{  
                        "name":"2888.mx.zerospam.ca",
                        "priority":10,
                        "organization_id":2888,
                        "updated_at":"2016-12-23 18:41:16.000000 +00:00",
                        "created_at":"2016-12-23 18:41:16.000000 +00:00"
                     }
                  },
                  {  
                     "type":"mxs",
                     "id":"5447",
                     "attributes":{  
                        "name":"2888.mx1.zerospam.ca",
                        "priority":20,
                        "organization_id":2888,
                        "updated_at":"2016-12-23 18:41:16.000000 +00:00",
                        "created_at":"2016-12-23 18:41:16.000000 +00:00"
                     }
                  }
               ],
               "meta":{  
                  "pagination":{  
                     "total":2,
                     "count":2,
                     "per_page":10,
                     "current_page":1,
                     "total_pages":1,
                     "links":[  
            
                     ]
                  }
               }
            }';

        $jsonRequest
            = '{"mxs":[{"name":"2888.mx.zerospam.ca","priority":10},{"name":"2888.mx1.zerospam.ca","priority":20}]}';

        $config = $this->preSuccess($jsonResult);

        $request = new CreateMXRequest();
        $request->addMXRecord('2888.mx.zerospam.ca', 10)
                ->addMXRecord('2888.mx1.zerospam.ca', 20)
                ->setOrganizationId(2888);

        $client = new ProvulusClient($config);
        $client->processRequest($request);

        $collectionResponse = $request->getResponse();

        $this->validateRequest($config, $jsonRequest);

        /**
         * @var OrganizationMXResponse $response
         */
        $response = $collectionResponse->data()->first();
        self::assertCount(2, $collectionResponse->data());
        self::assertEquals('2888.mx.zerospam.ca', $response->get('name'));
    }

    /**
     * @test
     * @expectedException \ProvulusSDK\Exception\API\ValidationException
     **/
    public function create_mx_failure_unique()
    {
        $jsonResult
            = '{  
               "errors":{  
                  "mxs.0.name":[  
                     {  
                        "code":"required_error",
                        "message":"The MX record name is required.",
                        "value":null
                     }
                  ],
                  "mxs.1.name":[  
                     {  
                        "code":"unique_error",
                        "message":"The mxs.1.name has already been taken.",
                        "value":"1467.bb.zerospam.ca"
                     }
                  ]
               }
            }';

        $jsonRequest
            = '{
                "mxs": [
                  {
                    "name": "1467.bb.zerospam.ca",
                    "priority": 10
                  }
                ]
            }';

        $config = $this->preFailure($jsonResult);

        $request = new CreateMXRequest();
        $request->addMXRecord('1467.bb.zerospam.ca', 10)
                ->setOrganizationId(2888);

        $client = new ProvulusClient($config);

        try {
            $client->processRequest($request);
        } catch (ValidationException $validationException) {
            $this->validateRequest($config, $jsonRequest);
            throw $validationException;
        }
    }

    /**
     * @test
     **/
    public function read_mx_success()
    {

        $jsonResult
            = '{"data":{"type":"mxs","id":"5447","attributes":{"organization_id":2888,"priority":20,"name":"2888.mx1.zerospam.ca","created_at":"2016-12-23 18:41:16.000000 +00:00","updated_at":"2016-12-23 18:41:16.000000 +00:00"}}}';
        $jsonRequest
            = '{}';

        $config = $this->preSuccess($jsonResult);

        $request = new ReadMXRequest();
        $request->setOrganizationId(2888)
                ->setMXId(5447);

        $client = new ProvulusClient($config);
        /**
         * @var $response OrganizationMXResponse
         */
        $response = $client->processRequest($request);

        $this->validateRequest($config, $jsonRequest);

        self::assertEquals(20, $response->priority, 'MX priority is not correct.');
        self::assertEquals('2888.mx1.zerospam.ca', $response->name, 'MX record name is not correct.');
    }


    /**
     * @test
     **/
    public function index_mx_success()
    {
        $jsonRequest
            = '{}';

        $jsonResponse
            = '{  
                   "data":[  
                      {  
                         "type":"mxs",
                         "id":"5449",
                         "attributes":{  
                            "organization_id":2888,
                            "priority":10,
                            "name":"2888.bb.zerospam.ca",
                            "created_at":"2017-01-27 14:30:19.000000 +00:00",
                            "updated_at":"2017-01-27 14:30:19.000000 +00:00"
                         }
                      },
                      {  
                         "type":"mxs",
                         "id":"5448",
                         "attributes":{  
                            "organization_id":2888,
                            "priority":20,
                            "name":"2888.aa.zerospam.ca",
                            "created_at":"2017-01-27 14:30:19.000000 +00:00",
                            "updated_at":"2017-01-27 14:30:19.000000 +00:00"
                         }
                      }
                   ],
                   "meta":{  
                      "pagination":{  
                         "total":2,
                         "count":2,
                         "per_page":15,
                         "current_page":1,
                         "total_pages":1,
                         "links":[  
                
                         ]
                      }
                   }
                }';

        $config = $this->preSuccess($jsonResponse);

        $request = new IndexMXRequest();
        $request->setOrganizationId(2888);

        $client = new ProvulusClient($config);
        $client->processRequest($request);

        $collectionResponse = $request->getResponse();

        $this->validateRequest($config, $jsonRequest);

        /**
         * @var OrganizationMXResponse $organizationMXResponse
         */
        $organizationMXResponse = $collectionResponse->data()->first();
        $this->assertEquals(5449, $organizationMXResponse->id());

        $this->assertEquals(2, $collectionResponse->getMetaData()->total);
        $this->assertEquals(2, $collectionResponse->getMetaData()->count);
        $this->assertEquals(15, $collectionResponse->getMetaData()->perPage);
        $this->assertEquals(1, $collectionResponse->getMetaData()->currentPage);
        $this->assertEquals(1, $collectionResponse->getMetaData()->totalPages);
    }


    /**
     * @test
     **/
    public function update_mx_success()
    {
        $jsonResult
            = '{"data":{"type":"mxs","id":"5447","attributes":{"organization_id":2888,"priority":32,"name":"2888.mx2.zerospam.ca","created_at":"2016-12-23 18:41:16.000000 +00:00","updated_at":"2016-12-23 19:36:02.000000 +00:00"}}}';

        $jsonRequest
            = '{"name":"2888.mx2.zerospam.ca","priority":32}';

        $config = $this->preSuccess($jsonResult);

        $request = new UpdateMXRequest();
        $request->setName('2888.mx2.zerospam.ca')
                ->setPriority(32)
                ->setOrganizationId(2888)
                ->setMXId(5447);

        $client = new ProvulusClient($config);

        /**
         * @var OrganizationMXResponse $response
         */
        $response = $client->processRequest($request);

        $this->validateRequest($config, $jsonRequest);

        self::assertEquals(32, $response->priority, 'MX priority is not correct.');
        self::assertEquals('2888.mx2.zerospam.ca', $response->name, 'MX record name is not correct.');

    }

    /**
     * @test
     **/
    public function delete_mx_success()
    {
        $jsonRequest = '{}';
        $jsonResult  = '{}';

        $config = $this->preSuccess($jsonResult);

        $request = new DeleteMXRequest();
        $request->setOrganizationId(2888)
                ->setMXId(5447);

        $client = new ProvulusClient($config);
        $client->processRequest($request);

        $this->validateRequest($config, $jsonRequest);
    }

    /**
     * Gets config for tests
     *
     * @param MockHandler $handler
     *
     * @return ProvulusConfiguration
     */
    protected function getConfig(MockHandler $handler)
    {
        return parent::getConfig($handler)->setApiKey('test');
    }
}