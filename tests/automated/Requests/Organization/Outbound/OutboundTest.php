<?php


namespace automated\Requests\Organization\Outbound;

use ProvulusSDK\Client\ProvulusClient;
use ProvulusSDK\Client\Request\Organization\Outbound\CancelOutboundCancellationRequest;
use ProvulusSDK\Client\Request\Organization\Outbound\CreateOutboundCancellationRequest;
use ProvulusSDK\Client\Request\Organization\Outbound\CreateOutboundRequest;
use ProvulusSDK\Client\Request\Organization\Outbound\ReadOutboundRequest;
use ProvulusSDK\Client\Request\Organization\Outbound\UpdateOutboundRequest;
use ProvulusSDK\Client\Response\Organization\Outbound\OutboundResponse;
use ProvulusTest\TestCaseApiKey;

class OutboundTest extends TestCaseApiKey
{
    /** @test */
    public function create_outbound_request()
    {
        $jsonRequest = '{}';

        $jsonResponse = <<<JSON
{
    "data": {
        "type": "outbound",
        "id": "654",
        "attributes": {
            "relay": "example.zerospamoutbound.ca",
            "default_filtering_policy_id": 7767,
            "bounce_filtering_policy_id": 7767,
            "is_filter_inbound_domains": true
        }
    }
}
JSON;
        $config       = $this->preSuccess($jsonResponse);
        $request      = new CreateOutboundRequest();
        $request->setOrganizationId(654);

        $client = new ProvulusClient($config);


        /** @var $response OutboundResponse */
        $response = $client->processRequest($request);

        $this->validateRequest($config, $jsonRequest);
        $this->validateUrl($config, 'orgs/654/outbound');

        $this->assertEquals("example.zerospamoutbound.ca", $response->relay);
        $this->assertEquals(7767, $response->default_filtering_policy_id);
        $this->assertEquals(7767, $response->bounce_filtering_policy_id);
        $this->assertTrue($response->is_filter_inbound_domains);
    }

    /** @test */
    public function read_outbound_request()
    {
        $jsonRequest = '{}';

        $jsonResponse = <<<JSON
{
    "data": {
        "type": "outbound",
        "id": "1121",
        "attributes": {
            "relay": "read.zerospamoutbound.ca",
            "default_filtering_policy_id": 9998,
            "bounce_filtering_policy_id": 1112,
            "is_filter_inbound_domains": true
        }
    }
}
JSON;
        $config       = $this->preSuccess($jsonResponse);
        $request      = new ReadOutboundRequest();
        $request->setOrganizationId(1121);

        $client = new ProvulusClient($config);


        /** @var $response OutboundResponse */
        $response = $client->processRequest($request);

        $this->validateRequest($config, $jsonRequest);
        $this->validateUrl($config, 'orgs/1121/outbound');

        $this->assertEquals("read.zerospamoutbound.ca", $response->relay);
        $this->assertEquals(9998, $response->default_filtering_policy_id);
        $this->assertEquals(1112, $response->bounce_filtering_policy_id);
        $this->assertTrue($response->is_filter_inbound_domains);
    }

    /** @test */
    public function update_outbound_request()
    {
        $jsonRequest = <<<JSON
{
    "default_filtering_policy_id": 31416,
    "bounce_filtering_policy_id": 27183
}
JSON;


        $jsonResponse = <<<JSON
{
    "data": {
        "type": "outbound",
        "id": "2515",
        "attributes": {
            "relay": "update.zerospamoutbound.ca",
            "default_filtering_policy_id": 31416,
            "bounce_filtering_policy_id": 27183,
            "is_filter_inbound_domains": true
        }
    }
}
JSON;
        $config       = $this->preSuccess($jsonResponse);
        $request      = new UpdateOutboundRequest();
        $request->setOrganizationId(2515)
                ->setBounceFilteringPolicyId(27183)
                ->setDefaultFilteringPolicyId(31416);

        $client = new ProvulusClient($config);


        /** @var $response OutboundResponse */
        $response = $client->processRequest($request);

        $this->validateRequest($config, $jsonRequest);
        $this->validateUrl($config, 'orgs/2515/outbound');

        $this->assertEquals("update.zerospamoutbound.ca", $response->relay);
        $this->assertEquals(31416, $response->default_filtering_policy_id);
        $this->assertEquals(27183, $response->bounce_filtering_policy_id);
        $this->assertTrue($response->is_filter_inbound_domains);
    }

    /** @test */
    public function create_outbound_cancellation_request()
    {
        $jsonRequest = '{}';
        $jsonResult  = '{}';

        $config = $this->preSuccess($jsonResult);

        $request = new CreateOutboundCancellationRequest();
        $request->setOrganizationId(2888);

        $client = new ProvulusClient($config);

        $client->processRequest($request);

        $this->validateUrl($config, 'orgs/2888/outbound/create-cancellation');
        $this->validateRequest($config, $jsonRequest);
    }

    /** @test */
    public function cancel_outbound_cancellation_request()
    {
        $jsonRequest = '{}';
        $jsonResult  = '{}';

        $config = $this->preSuccess($jsonResult);

        $request = new CancelOutboundCancellationRequest();
        $request->setOrganizationId(2888);

        $client = new ProvulusClient($config);

        $client->processRequest($request);

        $this->validateUrl($config, 'orgs/2888/outbound/cancel-cancellation');
        $this->validateRequest($config, $jsonRequest);
    }
}
