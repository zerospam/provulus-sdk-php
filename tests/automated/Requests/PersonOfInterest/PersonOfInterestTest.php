<?php
/**
 * Created by PhpStorm.
 * User: pbb
 * Date: 18/10/17
 * Time: 9:26 AM
 */

namespace automated\Requests\PersonOfInterest;

use ProvulusSDK\Client\ProvulusClient;
use ProvulusSDK\Client\Request\PersonOfInterest\BulkDeletePersonOfInterestRequest;
use ProvulusSDK\Client\Request\PersonOfInterest\CreatePersonOfInterestRequest;
use ProvulusSDK\Client\Request\PersonOfInterest\DeletePersonOfInterestRequest;
use ProvulusSDK\Client\Request\PersonOfInterest\IndexPersonOfInterestRequest;
use ProvulusSDK\Client\Request\PersonOfInterest\ReadPersonOfInterestRequest;
use ProvulusSDK\Client\Request\PersonOfInterest\UpdatePersonOfInterestRequest;
use ProvulusSDK\Client\Response\EmptyResponse;
use ProvulusSDK\Client\Response\PersonOfInterest\PersonOfInterestResponse;
use ProvulusSDK\Enum\PersonOfInterest\ActionUponMatchEnum;
use ProvulusTest\TestCaseApiKey;

/**
 * Class PersonOfInterestTest
 *
 * @package automated\Requests\PersonOfInterest
 */
class PersonOfInterestTest extends TestCaseApiKey
{

    /**
     * @test
     **/
    public function person_of_interest_create_success()
    {
        $responseBody
            = '{
                  "data": {
                    "type": "person_of_interest",
                    "id": "2",
                    "attributes": {
                      "full_name": "Björk Guðmundsson",
                      "action_upon_match": "block",
                      "organization_id": 12345,
                      "updated_at": "2017-11-10 20:53:16.759997 +00:00",
                      "created_at": "2017-11-10 20:53:16.759997 +00:00"
                    }
                  }
                }';

        $requestBody
            = '{
                  "full_name" : "Björk Guðmundsson"
                }';

        $config  = $this->preSuccess($responseBody);
        $request = new CreatePersonOfInterestRequest();

        $request->setOrganizationId(12345)
                ->setFullName('Björk Guðmundsson')
                ->setActionUponMatch(ActionUponMatchEnum::ADD_HEADER());

        $client = new ProvulusClient($config);
        $client->processRequest($request);

        $this->validateRequest($config, $requestBody);
        $this->validateUrl($config, 'orgs/12345/personinterests');

        $response = $request->getResponse();
        self::assertEquals($response->organization_id, 12345);
        self::assertTrue($response->action_upon_match->is(ActionUponMatchEnum::BLOCK()));
    }

    /**
     * @test
     **/
    public function read_person_of_interest_success()
    {
        $responseBody
            = '{
                  "data": {
                    "type": "person_of_interest",
                    "id": "11",
                    "attributes": {
                      "organization_id": 12345,
                      "full_name": "Björk Guðmundsdóttir",
                      "full_name_normalized": "bjork gudmundsdottir",
                      "action_upon_match": "pass_through",
                      "created_at": "2017-10-19 14:18:32.000000 +00:00",
                      "updated_at": "2017-10-19 14:21:51.000000 +00:00"
                    }
                  }
                }';

        $requestBody = [];

        $config = $this->preSuccess($responseBody);
        $client = new ProvulusClient($config);

        $request = new ReadPersonOfInterestRequest();
        $request->setOrganizationId(12345)
                ->setPersonOfInterestId(1);

        $client->processRequest($request);
        $this->validateRequest($config, $requestBody);
        $this->validateUrl($config, 'orgs/12345/personinterests/1');

        $response = $request->getResponse();

        self::assertEquals('Björk Guðmundsdóttir', $response->full_name);
    }

    /**
     * @test
     **/
    public function index_person_of_interest_success()
    {
        $responseBody
            = '{
                  "data": [
                    {
                      "type": "person_of_interest",
                      "id": "1",
                      "attributes": {
                        "organization_id": 12345,
                        "full_name": "Björk Guðmundsdóttir",
                        "full_name_normalized": "bjork gudmundsdottir",
                        "action_upon_match": "pass_through",
                        "created_at": "2017-10-19 15:58:56.000000 +00:00",
                        "updated_at": "2017-10-19 15:58:56.000000 +00:00"
                      }
                    },
                    {
                      "type": "person_of_interest",
                      "id": "2",
                      "attributes": {
                        "organization_id": 12345,
                        "full_name": "Björk Guðmundsson",
                        "full_name_normalized": "bjork gudmundsson",
                        "action_upon_match": "block",
                        "created_at": "2017-10-19 15:59:12.000000 +00:00",
                        "updated_at": "2017-10-19 15:59:12.000000 +00:00"
                      }
                    }
                  ],
                  "meta": {
                    "pagination": {
                      "total": 2,
                      "count": 2,
                      "per_page": 15,
                      "current_page": 1,
                      "total_pages": 1
                    }
                  },
                  "links": {
                    "self": "http://cumulus.local/api/v1/orgs/12345/personinterests?page=1",
                    "first": "http://cumulus.local/api/v1/orgs/12345/personinterests?page=1",
                    "last": "http://cumulus.local/api/v1/orgs/12345/personinterests?page=1"
                  }
                }';

        $requestBody = [];

        $config = $this->preSuccess($responseBody);
        $client = new ProvulusClient($config);

        $request = new IndexPersonOfInterestRequest();
        $request->setOrganizationId(12345);

        $client->processRequest($request);
        $this->validateRequest($config, $requestBody);
        $this->validateUrl($config, 'orgs/12345/personinterests');

        $response = $request->getResponse();

        $sliced = $response->data()->take(1, 1);
        /** @var PersonOfInterestResponse $secondElement */
        $secondElement = $sliced->first();

        self::assertTrue($secondElement->action_upon_match->is(ActionUponMatchEnum::BLOCK()));
    }

    /**
     * @test
     **/
    public function update_person_of_interest_success()
    {
        $responseBody
            = '{
                  "data": {
                    "type": "person_of_interest",
                    "id": "12",
                    "attributes": {
                      "organization_id": 12345,
                        "full_name": "zerospam cumulus",
                        "full_name_normalized": "zerospam cumulus",
                      "action_upon_match": "add_header",
                      "created_at": "2017-10-20 14:53:28.000000 +00:00",
                      "updated_at": "2017-10-20 14:54:59.000000 +00:00"
                    }
                  }
                }';

        $requestBody
            = '{
                  "full_name" : "zerospam cumulus",
                  "action_upon_match" : "add_header"
                }';

        $config = $this->preSuccess($responseBody);
        $client = new ProvulusClient($config);

        $request = new UpdatePersonOfInterestRequest();
        $request->setOrganizationId(12345)
                ->setPersonOfInterestId(12)
                ->setFullName('zerospam cumulus')
                ->setActionUponMatch(ActionUponMatchEnum::ADD_HEADER());

        $client->processRequest($request);

        $this->validateRequest($config, $requestBody);
        $this->validateUrl($config, 'orgs/12345/personinterests/12');

        $response = $request->getResponse();
        self::assertEquals('zerospam cumulus', $response->full_name);
        self::assertTrue($response->action_upon_match->is(ActionUponMatchEnum::ADD_HEADER()));
    }

    /**
     * @test
     **/
    public function delete_person_of_interest_success()
    {
        $requestBody = [];

        $responseBody = [];

        $config = $this->preSuccess($responseBody);
        $client = new ProvulusClient($config);

        $request = new DeletePersonOfInterestRequest();
        $request->setOrganizationId(12345)
                ->setPersonOfInterestId(12);

        $client->processRequest($request);
        $this->validateRequest($config, $requestBody);
        $this->validateUrl($config, 'orgs/12345/personinterests/12');

        $response = $request->getResponse();

        $this->assertInstanceOf(EmptyResponse::class, $response);
    }

    /**
     * @test
     **/
    public function bulk_delete_person_of_interest_success()
    {
        $requestBody
            = '{
                "ids": [12, 13]
            }';

        $responseBody = [];

        $config = $this->preSuccess($responseBody);
        $client = new ProvulusClient($config);

        $request = new BulkDeletePersonOfInterestRequest();
        $request->setOrganizationId(12345)
                ->setIds([12, 13]);

        $client->processRequest($request);
        $this->validateRequest($config, $requestBody);
        $this->validateUrl($config, 'orgs/12345/personinterests');

        $response = $request->getResponse();

        $this->assertInstanceOf(EmptyResponse::class, $response);
    }
}