<?php
/**
 * Created by PhpStorm.
 * User: aaflalo
 * Date: 12/12/16
 * Time: 10:07 AM
 */

namespace automated\Client;


use Carbon\Carbon;
use GuzzleHttp\Exception\BadResponseException;
use GuzzleHttp\Exception\RequestException;
use GuzzleHttp\Handler\MockHandler;
use GuzzleHttp\Psr7\Request;
use GuzzleHttp\Psr7\Response;
use ProvulusSDK\Client\Args\Search\Organization\ActiveArgument;
use ProvulusSDK\Client\Args\Search\Organization\CorporateNameArgument;
use ProvulusSDK\Client\Errors\Precondition\PreconditionError;
use ProvulusSDK\Client\ProvulusClient;
use ProvulusSDK\Enum\Locale\LocaleEnum;
use ProvulusSDK\Enum\SearchCriteria\ActiveCriteriaEnum;
use ProvulusSDK\Exception\API\HitRateLimitException;
use ProvulusSDK\Exception\API\NotFoundException;
use ProvulusSDK\Exception\API\PreconditionException;
use ProvulusSDK\Exception\Http\GuzzleException;
use ProvulusTest\Request\BindableTestRequest;
use ProvulusTest\Request\DeleteRequest;
use ProvulusTest\Request\InheritedTestRequest;
use ProvulusTest\Request\NullableRequest;
use ProvulusTest\Request\TestRequest;
use ProvulusTest\TestCase;
use ProvulusTest\TestResponse;

class ClientTest extends TestCase
{


    /**
     * @test
     * @expectedException \InvalidArgumentException
     */
    public function client_throw_exception_if_no_api_key_or_jwt_storage()
    {
        $config = $this->getConfig(new MockHandler());
        new ProvulusClient($config);
    }


    /**
     * @test
     * @expectedException \InvalidArgumentException
     */
    public function client_throw_exception_if_no_jwt_storage_and_use_login()
    {
        $config = $this->getConfig(new MockHandler());
        $client = new ProvulusClient($config);
        $client->login('test', 'test');
    }

    /**
     * @test
     * @expectedException \InvalidArgumentException
     */
    public function client_throw_exception_if_no_jwt_storage_and_use_login2()
    {
        $config = $this->getConfig(new MockHandler())->setApiKey('test');

        $client = new ProvulusClient($config);
        $client->login('test', 'test');
    }

    /**
     * @test
     */
    public function client_set_json_header_content_type()
    {
        $config = $this->getConfig(new MockHandler([new Response(201)]))->setApiKey('test');


        $client = new ProvulusClient($config);
        $client->processRequest(new TestRequest());

        $trans  = $this->lastTransaction($config);
        $header = $trans->request()->getHeader('Content-Type');
        $this->assertEquals('application/json', $header[0]);
    }

    /**
     * @test
     */
    public function client_uses_set_org_id()
    {
        $config = $this->getConfig(new MockHandler([new Response(201)]))->setOrganizationId(10)->setApiKey('test');


        $client = new ProvulusClient($config);
        $client->processRequest(new TestRequest());

        $trans = $this->lastTransaction($config);
        $this->assertTrue($trans->request()->hasHeader('X-Cumulus-Org'), 'Has Org Header');
        $this->assertArraySubset([10], $trans->request()->getHeader('X-Cumulus-Org'), false, 'Org Header == 10');
    }

    /**
     * @test
     */
    public function client_uses_set_locale()
    {
        $config = $this->getConfig(new MockHandler([new Response(201)]))->setLocale(LocaleEnum::EN_US())
                       ->setApiKey('test');


        $client = new ProvulusClient($config);
        $client->processRequest(new TestRequest());

        $trans = $this->lastTransaction($config);
        $this->assertTrue($trans->request()->hasHeader('X-Cumulus-Locale'), 'Has Locale header');
        $this->assertArraySubset([LocaleEnum::EN_US()->getValue()], $trans->request()->getHeader('X-Cumulus-Locale'),
            false, 'Locale header == en_US');
    }


    /**
     * @test
     */
    public function client_response_has_request()
    {
        $config = $this->getConfig(new MockHandler([new Response(201)]))->setApiKey('test');


        $client   = new ProvulusClient($config);
        $request  = new TestRequest();
        $response = $client->processRequest($request);

        $this->assertEquals($request, $response->getParentRequest());
    }


    /**
     * @test
     */
    public function client_response_set_response()
    {
        $config = $this->getConfig(new MockHandler([new Response(201)]))->setApiKey('test');

        $client   = new ProvulusClient($config);
        $request  = new TestRequest();
        $response = $client->processRequest($request);

        $this->assertEquals($response, $request->getResponse());
    }

    /**
     * @test
     * @expectedException \InvalidArgumentException
     * @expectedExceptionMessage find related object
     */
    public function client_cant_create_response_alone()
    {
        new TestResponse([]);
    }


    /**
     * @test
     * @expectedException  \ProvulusSDK\Exception\API\NotFoundException
     */
    public function client_not_found_exception()
    {
        $mockHandler = new MockHandler([
            new BadResponseException('Not Found', new Request('GET', 'test'), new Response(404)),
        ]);

        $config = $this->getConfig($mockHandler)->setApiKey('test');

        $client = new ProvulusClient($config);
        $client->processRequest(new TestRequest());
    }

    /**
     * @test
     * @expectedException  \ProvulusSDK\Exception\API\NotFoundException
     */
    public function client_not_found_exception_keep_request()
    {
        $mockHandler = new MockHandler([
            new Response(404),
        ]);

        $config = $this->getConfig($mockHandler)->setApiKey('test');

        $client  = new ProvulusClient($config);
        $request = new TestRequest();
        try {
            $client->processRequest($request);
        } catch (NotFoundException $e) {
            $this->assertEquals($request, $e->getRequest());
            throw $e;
        }
    }

    /**
     * @test
     */
    public function client_throttle_update()
    {
        $mockHandler = new MockHandler([
            new Response(200, [
                'X-RateLimit-Remaining' => 50,
                'X-RateLimit-Limit'     => 60,
            ]),
        ]);

        $config = $this->getConfig($mockHandler)->setApiKey('test');

        $client   = new ProvulusClient($config);
        $response = $client->processRequest(new TestRequest());


        $this->assertEquals(50, $response->getRateLimit()->getRemaining());
        $this->assertEquals(60, $response->getRateLimit()->getMaxPerMinute());

    }

    /**
     * @test
     * @expectedException  \ProvulusSDK\Exception\API\HitRateLimitException
     */
    public function client_throttle_limit_reached()
    {
        $now = time();


        $mockHandler = new MockHandler([
            new Response(429, [
                'X-RateLimit-Remaining' => 0,
                'X-RateLimit-Limit'     => 60,
                'X-RateLimit-Reset'     => $now,
            ]),
        ]);

        $config = $this->getConfig($mockHandler)->setApiKey('test');

        $client = new ProvulusClient($config);
        try {
            $client->processRequest(new TestRequest());
        } catch (HitRateLimitException $e) {
            $rate = $e->getRateLimit();
            $this->assertEquals(0, $rate->getRemaining());
            $this->assertEquals(Carbon::createFromTimestamp($now), $rate->getEndOfThrottle());
            throw $e;
        }

    }

    /**
     * @test
     * @expectedException  \ProvulusSDK\Exception\Http\GuzzleException
     */
    public function client_guzzle_exception()
    {
        $mockHandler = new MockHandler([
            new Response(405),
        ]);

        $config = $this->getConfig($mockHandler)->setApiKey('test');

        $client = new ProvulusClient($config);
        $client->processRequest(new TestRequest());


    }

    /**
     * @test
     * @expectedException \ProvulusSDK\Exception\API\PreconditionException
     **/
    public function delete_request_failure()
    {
        $jsonResponse
            = '{
  "errors": [
    {
      "code": "domain_already_scheduled_error",
      "obj_id": 9999,
      "message": "There is already a Once Check Mx scheduled on test.ca"
    }
  ]
}';

        $mockHandler = new MockHandler([
            new BadResponseException('Not Found', new Request('DELETE', 'test'), new Response(412, [], $jsonResponse)),
        ]);

        $config = $this->getConfig($mockHandler)->setApiKey('test');

        $request = new DeleteRequest();

        $client = new ProvulusClient($config);

        $client->processRequest($request);
    }


    /**
     * @test
     * @expectedException \ProvulusSDK\Exception\API\PreconditionException
     **/
    public function delete_request_failure_content_match()
    {
        $jsonResponse
            = '{
  "errors": [
    {
      "code": "domain_already_scheduled_error",
      "obj_id": 9999,
      "message": "There is already a Once Check Mx scheduled on test.ca"
    }
  ]
}';

        $mockHandler = new MockHandler([
            new BadResponseException('Not Found', new Request('DELETE', 'test'), new Response(412, [], $jsonResponse)),
        ]);

        $config = $this->getConfig($mockHandler)->setApiKey('test');

        $request = new DeleteRequest();

        $client = new ProvulusClient($config);

        try {
            $client->processRequest($request);
        } catch (PreconditionException $exception) {
            /**
             * @var $error PreconditionError
             */
            $error = $exception->getErrors()->first();
            $this->assertEquals('domain_already_scheduled_error', $error->getCode());
            $this->assertEquals('9999', $error->getObjectId());
            $this->assertEquals('There is already a Once Check Mx scheduled on test.ca', $error->getMessage());
            throw $exception;
        }

    }

    /**
     * @test
     * @expectedException \InvalidArgumentException
     */
    public function bindable_request_need_to_be_bound()
    {
        $request = new BindableTestRequest();
        $request->routeUrl();
    }

    /**
     * @test
     */
    public function get_value_request()
    {
        $request = new TestRequest();
        $this->assertEquals('bar', $request->getValue('test'));
    }

    /**
     * @test
     */
    public function get_value_inherited_request()
    {
        $request = new InheritedTestRequest();
        $this->assertEquals('bar', $request->getValue('test'));
    }


    /**
     * @test
     */
    public function get_value_inherited_request_cached()
    {
        $request = new InheritedTestRequest();
        $this->assertEquals('bar', $request->getValue('test'));
        $this->assertEquals('bar', $request->getValue('test'));
    }

    /**
     * @test
     */
    public function get_value_inherited_request_own_value()
    {
        $request = new InheritedTestRequest();
        $this->assertEquals('foo', $request->getValue('super'));
    }

    /**
     * @test
     */
    public function get_value_inherited_request_own_value_cached()
    {
        $request = new InheritedTestRequest();
        $this->assertEquals('foo', $request->getValue('super'));
        $this->assertEquals('foo', $request->getValue('super'));
    }


    /**
     * @test
     */
    public function get_value_request_null()
    {
        $request = new TestRequest();
        $this->assertNull($request->getValue('nope'));
    }

    /**
     * @test
     */
    public function nullable_field_not_set()
    {
        $request = new NullableRequest();

        $jsonResult  = '{}';
        $jsonRequest = '{"normal_field" : "test"}';
        $config      = $this->preSuccess($jsonResult)->setApiKey('s');;

        $request->setNormalField('test');

        $client = new ProvulusClient($config);

        $client->processRequest($request);

        $this->validateRequest($config, $jsonRequest);
    }

    /**
     * @test
     */
    public function nullable_field_set_null()
    {
        $request = new NullableRequest();

        $jsonResult  = '{}';
        $jsonRequest = '{"null_field" : null}';
        $config      = $this->preSuccess($jsonResult)->setApiKey('s');;

        $request->setNullField(null);

        $client = new ProvulusClient($config);

        $client->processRequest($request);

        $this->validateRequest($config, $jsonRequest);
    }

    /**
     * @test
     */
    public function nullable_field_no_change()
    {
        $request = new NullableRequest();

        $jsonResult  = '{}';
        $jsonRequest = '{}';
        $config      = $this->preSuccess($jsonResult)->setApiKey('s');;

        $client = new ProvulusClient($config);

        $client->processRequest($request);

        $this->validateRequest($config, $jsonRequest);
    }


    /**
     * @test
     */
    public function date_format_test()
    {
        $request = new NullableRequest();

        $jsonResult  = '{}';
        $jsonRequest = '{"date_test" : "2017-05-05T05:05:05+00:00"}';
        $config      = $this->preSuccess($jsonResult)->setApiKey('s');

        $request->setDateTest(Carbon::createFromTimestamp(1493960705, 0));

        $client = new ProvulusClient($config);

        $client->processRequest($request);

        $this->validateRequest($config, $jsonRequest);
    }

    /**
     * @test
     * @expectedException \ProvulusSDK\Exception\Http\GuzzleException
     */
    public function retry_on_failure()
    {
        $request = new NullableRequest();
        $config  = $this->prepareQueue([
            new RequestException('test', new Request('GET', '')),
            new RequestException('test', new Request('GET', '')),
            new RequestException('test', new Request('GET', '')),
        ])->setApiKey('s');

        $client = new ProvulusClient($config);
        try {
            $client->processRequest($request);
        } catch (GuzzleException $e) {
            $this->assertEquals($config->getRetries(), $request->tries());
            throw $e;
        }
    }

    /**
     * @test
     * @expectedException  \ProvulusSDK\Exception\API\Success\PartialSuccessException
     */
    public function test_partial_success()
    {
        $request = new TestRequest();

        $jsonResult
                     = '{
  "errors": [
    {
      "code": "domain_mx_points_zs_error",
      "obj_id": 4523,
      "message": "test.ca point zs."
    },
    {
      "code": "domain_not_found_error",
      "obj_id": 2,
      "message": "Domain id [2] does\'t exists."
    }
  ]
}';
        $jsonRequest = '{}';
        $config      = $this->preSuccess($jsonResult, 206)->setApiKey('s');

        $client = new ProvulusClient($config);

        try {
            $client->processRequest($request);
        } catch (PreconditionException $e) {
            $first  = $e->getErrors()->first();
            $second = $e->getErrors()->take(1, 1)->first();
            $this->assertEquals(4523, $first->getObjectId());
            $this->assertEquals('domain_mx_points_zs_error', $first->getCode());

            $this->assertEquals(2, $second->getObjectId());
            $this->assertEquals('domain_not_found_error', $second->getCode());
            $errorMsg
                = <<<EOF
test.ca point zs.
Domain id [2] does't exists.
EOF;

            $this->assertEquals($errorMsg, $e->getMessage());
            throw $e;
        }

        $this->validateRequest($config, $jsonRequest);
    }

    /**
     * @test
     */
    public function test_url_generator_request()
    {
        $request = new TestRequest();

        $jsonResult = '{}';
        $config     = $this->preSuccess($jsonResult)->setApiKey('s');

        $client = new ProvulusClient($config);

        $this->assertEquals('http://127.0.0.1/api/v1/test/request', $client->uriOf($request));
    }

    /**
     * @test
     */
    public function test_url_generator_request_with_argument()
    {
        $request = new TestRequest();
        $request->addArgument(new CorporateNameArgument('test'));

        $jsonResult = '{}';
        $config     = $this->preSuccess($jsonResult)->setApiKey('s');

        $client = new ProvulusClient($config);

        $this->assertEquals('http://127.0.0.1/api/v1/test/request?corporate_name=test', $client->uriOf($request));
    }



    /**
     * @test
     */
    public function test_url_generator_request_with_multiple_arguments()
    {
        $request = new TestRequest();
        $request->addArgument(new CorporateNameArgument('test'))->addArgument(new ActiveArgument(ActiveCriteriaEnum::ACTIVE()));

        $jsonResult = '{}';
        $config     = $this->preSuccess($jsonResult)->setApiKey('s');

        $client = new ProvulusClient($config);

        $this->assertEquals('http://127.0.0.1/api/v1/test/request?corporate_name=test&active=active', $client->uriOf($request));
    }

}