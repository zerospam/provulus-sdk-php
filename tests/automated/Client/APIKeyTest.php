<?php
/**
 * Created by PhpStorm.
 * User: aaflalo
 * Date: 07/12/16
 * Time: 9:44 AM
 */

namespace automated\Auth;



use GuzzleHttp\Exception\BadResponseException;
use GuzzleHttp\Handler\MockHandler;
use GuzzleHttp\Psr7\Request;
use GuzzleHttp\Psr7\Response;
use Mockery as m;
use ProvulusSDK\Client\ProvulusClient;
use ProvulusTest\Request\TestRequest;
use ProvulusTest\TestCase;

class APIKeyTest extends TestCase
{

    /**
     * @test
     */
    public function auth_api_key_success()
    {
        $mockHandler = new MockHandler([
            new Response(201)
        ]);
        $config = $this->getConfig($mockHandler);


        $client = new ProvulusClient($config);
        $client->processRequest(new TestRequest());

        $trans = $this->lastTransaction($config);

        $this->assertTrue($trans->request()->hasHeader('X-Cumulus-Api'), 'Has API Header');
        $this->assertArraySubset(['test'], $trans->request()->getHeader('X-Cumulus-Api'), false, 'API Header == test');
    }

    /**
     * @test
     * @expectedException \ProvulusSDK\Exception\API\UnauthorizedException
     */
    public function auth_api_key_failure()
    {
        $mockHandler = new MockHandler([
           new BadResponseException('Unauthorized', new Request('GET', 'test'), new Response(401))
        ]);
        $config = $this->getConfig($mockHandler);


        $client = new ProvulusClient($config);
        $client->processRequest(new TestRequest());

    }


    protected function getConfig(MockHandler $handler)
    {
        $config=  parent::getConfig($handler);
        return $config->setApiKey('test');
    }


}