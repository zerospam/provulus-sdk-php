<?php
/**
 * Created by PhpStorm.
 * User: aaflalo
 * Date: 01/02/17
 * Time: 2:24 PM
 */

namespace automated\Client\Collection;


use ProvulusSDK\Client\Args\Sort\SortEnum;
use ProvulusTest\Request\IndexTestRequest;
use ProvulusTest\TestCase;

class CollectionTest extends TestCase
{

    /**
     * @test
     */
    function set_per_page()
    {
        $request = new IndexTestRequest();
        $request->setPerPage(250);

        $this->assertEquals(250, $request->getArguments()['perPage']->toPrimitive());

    }

    /**
     * @test
     */
    function remove_per_page()
    {
        $request = new IndexTestRequest();
        $request->setPerPage(250);

        $this->assertEquals(250, $request->getArguments()['perPage']->toPrimitive());

        $request->setPerPage(null);
        $this->assertArrayNotHasKey('perPage', $request->getArguments());

    }

    /**
     * @test
     */
    function set_page()
    {
        $request = new IndexTestRequest();
        $request->setPage(250);

        $this->assertEquals(250, $request->getArguments()['page']->toPrimitive());

    }

    /**
     * @test
     */
    function remove_page()
    {
        $request = new IndexTestRequest();
        $request->setPage(250);

        $this->assertEquals(250, $request->getArguments()['page']->toPrimitive());

        $request->setPage(null);
        $this->assertArrayNotHasKey('page', $request->getArguments());

    }

    /**
     * @test
     */
    function set_search()
    {
        $request = new IndexTestRequest();
        $request->setSearch('test');

        $this->assertEquals('test', $request->getArguments()['search']->toPrimitive());

    }

    /**
     * @test
     */
    function remove_search()
    {
        $request = new IndexTestRequest();
        $request->setSearch('test');

        $this->assertEquals('test', $request->getArguments()['search']->toPrimitive());

        $request->setSearch(null);
        $this->assertArrayNotHasKey('search', $request->getArguments());

    }

    /**
     * @test
     */
    function set_order()
    {
        $request = new IndexTestRequest();
        $request->setOrder('test', SortEnum::ASCENDING());

        $this->assertEquals('test', $request->getArguments()['orderBy']->toPrimitive());
        $this->assertEquals('ASC', $request->getArguments()['sortedBy']->toPrimitive());

    }

    /**
     * @test
     */
    function remove_order()
    {
        $request = new IndexTestRequest();
        $request->setOrder('test', SortEnum::ASCENDING());

        $this->assertEquals('test', $request->getArguments()['orderBy']->toPrimitive());
        $this->assertEquals('ASC', $request->getArguments()['sortedBy']->toPrimitive());

        $request->setOrder(null, SortEnum::ASCENDING());
        $this->assertArrayNotHasKey('orderBy', $request->getArguments());
        $this->assertArrayNotHasKey('sortedBy', $request->getArguments());

    }
}