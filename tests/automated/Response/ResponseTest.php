<?php
/**
 * Created by PhpStorm.
 * User: aaflalo
 * Date: 10/02/17
 * Time: 9:14 AM
 */

namespace automated\Response;


use Carbon\Carbon;
use GuzzleHttp\Handler\MockHandler;
use GuzzleHttp\Psr7\Response;
use ProvulusSDK\Client\ProvulusClient;
use ProvulusTest\Request\TestRequest;
use ProvulusTest\TestCaseApiKey;

class ResponseTest extends TestCaseApiKey
{


    /**
     * @test
     */
    public function response_test_dynamic_field()
    {
        $mockHandler = new MockHandler([
            new Response(200),
        ]);

        $config = $this->getConfig($mockHandler);

        $client  = new ProvulusClient($config);
        $request = new TestRequest();
        $client->processRequest($request);

        $response = $request->getResponse();

        $this->assertEquals(1486736482, $response->date_epoch);

    }

    /**
     * @test
     */
    public function response_test_dynamic_field_chained()
    {
        $mockHandler = new MockHandler([
            new Response(200),
        ]);

        $config = $this->getConfig($mockHandler);

        $client  = new ProvulusClient($config);
        $request = new TestRequest();
        $client->processRequest($request);

        $response = $request->getResponse();

        $this->assertInstanceOf(Carbon::class, $response->date_test);
        $this->assertEquals(1486736482, $response->date_test->timestamp);

    }

    /**
     * @test
     */
    public function response_test_dynamic_field_chained_cached()
    {
        $mockHandler = new MockHandler([
            new Response(200),
        ]);

        $config = $this->getConfig($mockHandler);

        $client  = new ProvulusClient($config);
        $request = new TestRequest();
        $client->processRequest($request);

        $response = $request->getResponse();

        $dateTest = $response->date_test;

        $this->assertSame($dateTest, $response->date_test);

    }

    /**
     * @test
     **/
    public function response_test_dates_transTyping()
    {
        $mockHandler = new MockHandler([
            new Response(200, [], json_encode([
                'data' => [
                    'attributes' =>
                        ['created_at' => '1975-12-25 14:15:16.000000 -05:00'],
                ],
            ])),
        ]);

        $config = $this->getConfig($mockHandler);

        $client  = new ProvulusClient($config);
        $request = new TestRequest();
        $client->processRequest($request);

        $response = $request->getResponse();

        $dateTest = $response->created_at;

        $this->assertInstanceOf(Carbon::class, $dateTest);
        $this->assertEquals('1975-12-25 14:15:16.000000 -05:00', $dateTest->format('Y-m-d H:i:s.u P'));
    }


    /**
     * @test
     **/
    public function response_test_dates_transTyping_null()
    {
        $mockHandler = new MockHandler([
            new Response(200),
        ]);

        $config = $this->getConfig($mockHandler);

        $client  = new ProvulusClient($config);
        $request = new TestRequest();
        $client->processRequest($request);

        $response = $request->getResponse();

        $this->assertNull($response->date_null);

    }


}