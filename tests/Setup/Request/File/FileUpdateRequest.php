<?php
/**
 * Created by PhpStorm.
 * User: aaflalo
 * Date: 17-07-25
 * Time: 13:27
 */

namespace ProvulusTest\Request\File;


use ProvulusSDK\Client\Request\BaseProvulusRequest;
use ProvulusSDK\Client\Request\RequestType;
use ProvulusSDK\Client\Response\EmptyResponse;
use ProvulusSDK\Client\Response\IProvulusResponse;

class FileUpdateRequest extends BaseProvulusRequest
{


    /**
     * @var string
     */
    private $testInfo;

    /**
     * @param string $testInfo
     *
     * @return $this
     */
    public function setTestInfo($testInfo)
    {
        $this->testInfo = $testInfo;

        return $this;
    }


    /**
     * Add the image
     *
     * @param string $filepath
     *
     * @return $this
     */
    public function setImage($filepath)
    {
        $this->addFile($filepath);
        return $this;
    }

    /**
     * The url of the route
     *
     * @return string
     */
    public function routeUrl()
    {
        return 'file';
    }

    /**
     * Type of request
     *
     * @return RequestType
     */
    public function httpType()
    {
        return RequestType::HTTP_PUT();
    }

    /**
     * Process the data that is in the response
     *
     * @param array $jsonResponse
     *
     * @return IProvulusResponse
     */
    public function processResponse(array $jsonResponse)
    {
        return new EmptyResponse();
    }
}