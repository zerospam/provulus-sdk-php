<?php
/**
 * Created by PhpStorm.
 * User: aaflalo
 * Date: 14/12/16
 * Time: 5:02 PM
 */

namespace ProvulusTest\Request;


class BindableMultiTestRequest extends BindableTestRequest
{
    function baseRoute()
    {
        return 'test/:testId/nice/:niceId/super/:niceId';
    }


}