<?php
/**
 * Created by PhpStorm.
 * User: aaflalo
 * Date: 20/01/17
 * Time: 4:00 PM
 */

namespace ProvulusTest\Request;


use ProvulusSDK\Client\Request\EmptyResponseTrait;
use ProvulusSDK\Client\Request\RequestType;

class DeleteRequest extends TestRequest
{

    use EmptyResponseTrait;

    /**
     * Type of request
     *
     * @return RequestType
     */
    public function httpType()
    {
        return RequestType::HTTP_DELETE();
    }
}