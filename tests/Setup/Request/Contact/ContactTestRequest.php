<?php
/**
 * Created by PhpStorm.
 * User: aaflalo
 * Date: 16/12/16
 * Time: 2:25 PM
 */

namespace ProvulusTest\Request\Contact;


use ProvulusSDK\Client\Request\RequestType;
use ProvulusSDK\Client\Request\Resource\Organization\Contact\ContactPersonRequest;
use ProvulusSDK\Client\Response\IProvulusObjectResponse;
use ProvulusTest\TestResponse;

class ContactTestRequest extends ContactPersonRequest
{

    /**
     * Type of request
     *
     * @return RequestType
     */
    public function httpType()
    {
        return RequestType::HTTP_GET();
    }

    /**
     * Process the data that is in the response
     *
     * @param array $jsonResponse
     *
     * @return IProvulusObjectResponse
     * @internal param Response $response
     */
    public function processResponse(array $jsonResponse)
    {
        return new TestResponse(array());
    }
}