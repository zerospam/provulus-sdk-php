<?php
/**
 * Created by PhpStorm.
 * User: aaflalo
 * Date: 16/12/16
 * Time: 2:18 PM
 */

namespace ProvulusTest\Request\User;


use ProvulusSDK\Client\Request\RequestType;
use ProvulusSDK\Client\Request\Resource\User\TypedUsers\UserTypedRequest;
use ProvulusSDK\Client\Response\IProvulusObjectResponse;
use ProvulusTest\TestResponse;

class UserTestRequest extends UserTypedRequest
{

    /**
     * Type of request
     *
     * @return RequestType
     */
    public function httpType()
    {
       return RequestType::HTTP_GET();
    }

    /**
     * Process the data that is in the response
     *
     * @param array $jsonResponse
     *
     * @return IProvulusObjectResponse
     * @internal param Response $response
     */
    public function processResponse(array $jsonResponse)
    {
        return new TestResponse(array());
    }
}