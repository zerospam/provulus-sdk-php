<?php
/**
 * Created by PhpStorm.
 * User: aaflalo
 * Date: 07/12/16
 * Time: 9:50 AM
 */

namespace ProvulusTest\Request;

use ProvulusSDK\Client\Args\Search\Audit\AuditLogsArgumentsTrait;
use ProvulusSDK\Client\Args\Search\Common\Date\DateRangeArgumentsTrait;
use ProvulusSDK\Client\Args\Search\Common\User\UserIdArgumentTrait;
use ProvulusSDK\Client\Args\Search\User\UserSearchTrait;
use ProvulusSDK\Client\Request\BaseProvulusRequest;
use ProvulusSDK\Client\Request\Organization\Traits\WithDomains;
use ProvulusSDK\Client\Request\Organization\Traits\WithSupportOrganization;
use ProvulusSDK\Client\Request\RequestType;
use ProvulusSDK\Client\Request\User\Traits\WithEmailAddresses;
use ProvulusSDK\Client\Response\IProvulusObjectResponse;
use ProvulusTest\TestResponse;

/**
 * Class TestRequest
 *
 * @method TestResponse getResponse()
 *
 * @package ProvulusTest\Request
 */
class TestRequest extends BaseProvulusRequest
{
    use WithDomains,
        WithSupportOrganization,
        WithEmailAddresses,
        DateRangeArgumentsTrait,
        UserIdArgumentTrait,
        AuditLogsArgumentsTrait,
        UserSearchTrait;

    private $test = 'bar';

    /**
     * Get the data used for the route
     *
     * @return array
     */
    public function toArray()
    {
        return [];
    }

    /**
     * The url of the route
     *
     * @return string
     */
    public function routeUrl()
    {
        return 'test/request';
    }

    /**
     * Type of request
     *
     * @return RequestType
     */
    public function httpType()
    {
        return RequestType::HTTP_POST();
    }

    /**
     * Process the data that is in the response
     *
     * @param array $jsonResponse
     *
     * @return IProvulusObjectResponse
     */
    public function processResponse(array $jsonResponse)
    {
        if (!isset($jsonResponse['data'])) {
            $jsonResponse['data'] = [];
        }

        return new TestResponse($jsonResponse['data']);
    }
}