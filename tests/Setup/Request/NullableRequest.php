<?php
/**
 * Created by PhpStorm.
 * User: aaflalo
 * Date: 05/05/17
 * Time: 11:10 AM
 */

namespace ProvulusTest\Request;


use Carbon\Carbon;
use ProvulusSDK\Client\Request\BaseProvulusRequest;
use ProvulusSDK\Client\Request\RequestType;
use ProvulusSDK\Client\Response\EmptyResponse;
use ProvulusSDK\Client\Response\IProvulusResponse;

class NullableRequest extends BaseProvulusRequest
{

    /**
     * @var string
     */
    private $nullField = -1;

    /**
     * @var string
     */
    private $normalField;

    /**
     * @var Carbon
     */
    private $dateTest;

    protected $nullableFields = ['nullField'];

    /**
     * @param int $nullField
     *
     * @return $this
     */
    public function setNullField($nullField)
    {
        $this->nullField = $nullField;
        $this->nullableChanged();

        return $this;
    }

    /**
     * @param string $normalField
     *
     * @return $this
     */
    public function setNormalField($normalField)
    {
        $this->normalField = $normalField;

        return $this;
    }

    /**
     * @param Carbon $dateTest
     *
     * @return $this
     */
    public function setDateTest($dateTest)
    {
        $this->dateTest = $dateTest;

        return $this;
    }




    /**
     * The url of the route
     *
     * @return string
     */
    public function routeUrl()
    {
        return 'null';
    }

    /**
     * Type of request
     *
     * @return RequestType
     */
    public function httpType()
    {
       return RequestType::HTTP_GET();
    }

    /**
     * Process the data that is in the response
     *
     * @param array $jsonResponse
     *
     * @return IProvulusResponse
     */
    public function processResponse(array $jsonResponse)
    {
        return new EmptyResponse();
    }
}