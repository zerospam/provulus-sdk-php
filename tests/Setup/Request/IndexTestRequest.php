<?php
/**
 * Created by PhpStorm.
 * User: aaflalo
 * Date: 01/02/17
 * Time: 2:21 PM
 */

namespace ProvulusTest\Request;


use ProvulusSDK\Client\Request\BindableRequest;
use ProvulusSDK\Client\Request\Collection\CollectionPaginatedTrait;
use ProvulusSDK\Client\Request\Collection\IProvulusCollectionPaginatedRequest;
use ProvulusSDK\Client\Request\Collection\SearchableTrait;
use ProvulusSDK\Client\Request\RequestType;
use ProvulusSDK\Client\Response\EmptyResponse;
use ProvulusSDK\Client\Response\IProvulusObjectResponse;
use ProvulusTest\Response\TestCollectionResponse;

class IndexTestRequest extends BindableRequest implements IProvulusCollectionPaginatedRequest
{

    use CollectionPaginatedTrait, SearchableTrait;

    /**
     * Base route without binding
     *
     * @return string
     */
    function baseRoute()
    {
        return 'collection/test';
    }

    /**
     * Type of request
     *
     * @return RequestType
     */
    public function httpType()
    {
        return RequestType::HTTP_GET();
    }

    /**
     * Used to parse each result of the collection
     *
     * Need to be overridden by the class
     *
     * @param array $array
     *
     * @return IProvulusObjectResponse
     */
    function toResponse(array $array)
    {
        return new EmptyResponse();
    }

    /**
     * Class to be used to build the collection
     *
     * @return string
     */
    function collectionClass()
    {
        return TestCollectionResponse::class;
    }
}