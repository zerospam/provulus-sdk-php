<?php
/**
 * Created by PhpStorm.
 * User: aaflalo
 * Date: 14/12/16
 * Time: 4:58 PM
 */

namespace ProvulusTest\Request;


use ProvulusSDK\Client\Request\BindableRequest;
use ProvulusSDK\Client\Request\RequestType;
use ProvulusSDK\Client\Response\IProvulusResponse;

class BindableTestRequest extends BindableRequest
{


    private $test;

    /**
     * @param mixed $test
     *
     * @return $this
     */
    public function setTest($test)
    {
        $this->test = $test;

        return $this;
    }


    public function setNiceId($id)
    {
        $this->addBinding('niceId', $id);

        return $this;
    }

    public function setTestId($id)
    {
        $this->addBinding('testId', $id);

        return $this;
    }

    /**
     * Base route without binding
     *
     * @return string
     */
    function baseRoute()
    {
        return 'test/:testId/nice/:niceId';
    }

    /**
     * Type of request
     *
     * @return RequestType
     */
    public function httpType()
    {
        // TODO: Implement httpType() method.
    }

    /**
     * Process the data that is in the response
     *
     * @param array $jsonResponse
     *
     * @return IProvulusResponse
     * @internal param ResponseInterface $response
     */
    public function processResponse(array $jsonResponse)
    {
        // TODO: Implement processResponse() method.
    }
}