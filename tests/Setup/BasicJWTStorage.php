<?php
namespace ProvulusTest;

use ProvulusSDK\Config\JWT\IProvulusJWTTokenStorage;

/**
 * Created by PhpStorm.
 * User: aaflalo
 * Date: 30/11/16
 * Time: 1:50 PM
 */
class BasicJWTStorage implements IProvulusJWTTokenStorage
{

    /**
     * @var string
     */
    private $token;

    /**
     * Get the current token
     *
     * @return string
     */
    public function getToken()
    {
        return $this->token;
    }

    /**
     * Update the token
     *
     * @param string $token
     */
    public function updateToken($token)
    {
        $this->token = $token;
    }
}