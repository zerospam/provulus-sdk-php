<?php
/**
 * Created by PhpStorm.
 * User: aaflalo
 * Date: 07/12/16
 * Time: 9:51 AM
 */

namespace ProvulusTest;


use Carbon\Carbon;
use ProvulusSDK\Client\Response\ProvulusBaseResponse;

/**
 * Class TestResponse
 *
 * @property-read Carbon $date_test
 * @property-read int    $date_epoch
 * @property-read Carbon $created_at
 *
 * @package ProvulusTest
 */
class TestResponse extends ProvulusBaseResponse
{

    /**
     * @var string[]
     */
    protected $dates
        = [
            'created_at',
            'date_null',
        ];

    /**
     * @return Carbon
     */
    public function getDateTestAttribute()
    {
        return Carbon::createFromTimestampUTC($this->date_epoch);
    }

    /**
     * @return int
     */
    public function getDateEpochAttribute()
    {
        return 1486736482;
    }


}