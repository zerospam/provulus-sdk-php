<?php
/**
 * Created by PhpStorm.
 * User: aaflalo
 * Date: 08/02/17
 * Time: 4:06 PM
 */

namespace ProvulusTest;


use GuzzleHttp\Handler\MockHandler;
use ProvulusTest\Obj\Conf\TestConf;

class TestCaseApiKey extends TestCase
{
    /**
     * @param MockHandler $handler
     *
     * @return TestConf
     */
    protected function getConfig(MockHandler $handler)
    {
        return parent::getConfig($handler)->setApiKey('test');
    }


}