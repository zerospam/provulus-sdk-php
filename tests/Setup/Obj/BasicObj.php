<?php
/**
 * Created by PhpStorm.
 * User: aaflalo
 * Date: 15/12/16
 * Time: 1:16 PM
 */

namespace ProvulusTest\Obj;


class BasicObj
{

    private $foo;

    /**
     * BasicClass constructor.
     *
     * @param $foo
     */
    public function __construct($foo) { $this->foo = $foo; }


}