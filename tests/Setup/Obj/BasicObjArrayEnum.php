<?php
/**
 * Created by PhpStorm.
 * User: aaflalo
 * Date: 15/12/16
 * Time: 1:19 PM
 */

namespace ProvulusTest\Obj;


class BasicObjArrayEnum
{

    /**
     * @var array|BasicEnum[]
     */
    private $enumArray = array();

    /**
     * BasicObjArrayEnum constructor.
     *
     * @param array|BasicEnum[] $enumArray
     */
    public function __construct($enumArray) { $this->enumArray = $enumArray; }


}