<?php
/**
 * Created by PhpStorm.
 * User: aaflalo
 * Date: 15/12/16
 * Time: 1:18 PM
 */

namespace ProvulusTest\Obj;


use MabeEnum\Enum;
use ProvulusSDK\Client\Request\Resource\PrimalValued;
use ProvulusSDK\Client\Request\Resource\PrimalValuedEnumTrait;

/**
 * Class BasicEnum
 *
 * @method static BasicEnum TEST()
 * @package ProvulusTest\Obj
 */
class BasicEnum extends Enum implements PrimalValued
{
    use PrimalValuedEnumTrait;

    const TEST = 'test';
}