<?php
/**
 * Created by PhpStorm.
 * User: aaflalo
 * Date: 15/12/16
 * Time: 1:17 PM
 */

namespace ProvulusTest\Obj;


class BasicObjEnum
{

    /**
     * @var BasicEnum
     */
    private $enum;

    /**
     * BasicObjEnum constructor.
     *
     * @param BasicEnum $enum
     */
    public function __construct(BasicEnum $enum) { $this->enum = $enum; }


}