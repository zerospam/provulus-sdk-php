<?php
/**
 * Created by PhpStorm.
 * User: aaflalo
 * Date: 08/02/17
 * Time: 10:40 AM
 */

namespace ProvulusTest\Obj\Conf;


use GuzzleHttp\Client;
use GuzzleHttp\ClientInterface;
use GuzzleHttp\Handler\MockHandler;
use GuzzleHttp\HandlerStack;
use GuzzleHttp\Middleware;
use ProvulusSDK\Config\Endpoint\ProvulusEndpoint;
use ProvulusSDK\Config\ProvulusConfiguration;
use ProvulusSDK\Config\Version\ProvulusApiVersion;

class TestConf extends ProvulusConfiguration
{


    /**
     * @var MockHandler
     */
    private $mockHandler;

    /**
     * @var array
     */
    private $container = [];

    /**
     * TestConf constructor.
     *
     * @param MockHandler $mockHandler
     */
    public function __construct(MockHandler $mockHandler)
    {
        $this->mockHandler = HandlerStack::create($mockHandler);
        $this->mockHandler->push(Middleware::history($this->container));
        parent::__construct(ProvulusEndpoint::TESTING(), ProvulusApiVersion::V1());
    }


    /**
     * Build the client for this configuration
     *
     * @return ClientInterface
     */
    public function buildClient()
    {
        return new Client(
            [
                'handler'  => $this->mockHandler,
                'base_uri' => $this->baseUrl(),
            ]);
    }

    /**
     * Containing the request and response done
     *
     * @see http://docs.guzzlephp.org/en/latest/testing.html#history-middleware
     *
     * @return array
     */
    public function getContainer()
    {
        return $this->container;
    }


}