<?php

namespace ProvulusTest;

use GuzzleHttp\Handler\MockHandler;
use GuzzleHttp\Psr7\Response;
use Mockery;
use ProvulusTest\Obj\Conf\TestConf;
use ProvulusTest\Obj\Container\Transaction;

/**
 * Created by PhpStorm.
 * User: aaflalo
 * Date: 30/11/16
 * Time: 1:45 PM
 */

/**
 * Class TestCase
 *
 * @see     http://docs.guzzlephp.org/en/latest/testing.html
 * @package ProvulusTest
 */
class TestCase extends \PHPUnit\Framework\TestCase
{
    public function tearDown()
    {
        Mockery::close();
    }

    /**
     * @param MockHandler $handler
     *
     * @see http://docs.guzzlephp.org/en/latest/testing.html
     * @return TestConf
     */
    protected function getConfig(MockHandler $handler)
    {
        return new TestConf($handler);
    }

    /**
     * The last transaction done (first if only one done)
     *
     * @param TestConf $conf
     *
     * @return Transaction|null null if no transaction done
     */
    protected function lastTransaction(TestConf $conf)
    {
        $last = count($conf->getContainer()) - 1;
        if ($last < 0) {
            return null;
        }
        $lastTrans = $conf->getContainer()[$last];

        return new Transaction($lastTrans['request'], $lastTrans['options'], $lastTrans['response'],
            $lastTrans['error']);
    }

    /**
     * All the transaction in order
     *
     * @param TestConf $conf
     *
     * @return Transaction[]
     */
    protected function allTransactions(TestConf $conf)
    {
        return array_map(function ($transaction) {
            return new Transaction($transaction['request'], $transaction['options'], $transaction['response'],
                $transaction['error']);
        }, $conf->getContainer());
    }

    /**
     * Run to set a success
     *
     * @param  string|string[] $responseBody
     *
     * @param int              $statusCode
     *
     * @return TestConf
     */
    protected function preSuccess($responseBody, $statusCode = 200)
    {
        if (is_array($responseBody)) {
            $responseBody = \GuzzleHttp\json_encode($responseBody, true);
        }

        $mockHandler = new MockHandler([
            new Response($statusCode, [], $responseBody),
        ]);

        $config = $this->getConfig($mockHandler);

        return $config;
    }

    /**
     * Validate that the request did contain the wanted arguments
     *
     * @param TestConf        $config
     * @param string|string[] $requestBody
     */
    protected function validateRequest(TestConf $config, $requestBody)
    {
        $trans  = $this->lastTransaction($config);
        $decode = \GuzzleHttp\json_decode($trans->request()->getBody()->getContents(), true);
        if (is_array($requestBody)) {
            $sent = $requestBody;
        } else {
            $sent = \GuzzleHttp\json_decode($requestBody, true);
        }

        $this->assertArraySubset($sent, $decode, 'Request contains all we want');
    }

    /**
     * Prepare for a failure
     *
     * @param  string|string[] $responseBody
     *
     * @param int              $statusCode Set the status code of the response
     *
     * @return TestConf
     */
    protected function preFailure($responseBody, $statusCode = 422)
    {
        if ($statusCode < 400) {
            throw new \InvalidArgumentException('The status code to prepare for failure need to be >=400');
        }

        if (is_array($responseBody)) {
            $responseBody = \GuzzleHttp\json_encode($responseBody, true);
        }

        $mockHandler = new MockHandler([
            new Response($statusCode, [], $responseBody),
        ]);

        $config = $this->getConfig($mockHandler);

        return $config;
    }

    /**
     * Used to queue an exception
     *
     * @param array $queue
     *
     * @return TestConf
     *
     */
    protected function prepareQueue(array $queue)
    {
        $mockHandler = new MockHandler($queue);

        $config = $this->getConfig($mockHandler);

        return $config;
    }

    /**
     * Validate that the request URI contains the following strings
     *
     * @param TestConf $config
     * @param string[] $contains
     */
    protected function validateRequestUrl(TestConf $config, array $contains)
    {
        $trans = $this->lastTransaction($config);

        $url = (string)$trans->request()->getUri();
        foreach ($contains as $contain) {
            $this->assertContains((string)$contain, $url, 'Url need to contain: ' . $contain);
        }
    }

    /**
     * Make sure url contains at least specified parts in contains
     *
     * @param TestConf      $config
     * @param array|\string $elements
     */
    protected function validateUrl(TestConf $config, $elements)
    {
        $trans = $this->lastTransaction($config);

        $url = urldecode((string)$trans->request()->getUri());

        if (is_string($elements)) {
            $this->assertContains($elements, $url);

            return;
        }

        $urlElements = explode('/', $url);

        foreach ($elements as $element) {
            $this->assertContains((string)$element, $urlElements, 'Url need to contain: ' . $element);
        }
    }
}